<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Nespresso Vietnam brings luxury coffee and espresso machine straight from the café and into your kitchen.">
    <meta name="author" content="Koodi">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="google-site-verification" content="Ihzx1tBV53gOkNIrs2JJCElahIqsxA6tiG6pMorjWdA" />
    <title>Nespresso Vietnam | Ispirazione Italiana</title>
    <link rel="icon" type="image/x-icon" href="https://www.nespresso.vn/wp-content/themes/storefront-child/images/favicon.ico">

    <!-- wp_head -->
    <title>Nespresso Vietnam &#8211; Nespresso Vietnam brings luxury coffee and espresso machine straight from the café and into your kitchen.</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Nespresso Vietnam &raquo; Feed" href="https://www.nespresso.vn/feed/" />
<link rel="alternate" type="application/rss+xml" title="Nespresso Vietnam &raquo; Comments Feed" href="https://www.nespresso.vn/comments/feed/" />
<link rel='stylesheet' id='axapta-api-css'  href='https://www.nespresso.vn/wp-content/plugins/axapta-api/public/css/axapta-api-public.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://www.nespresso.vn/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3' type='text/css' media='all' />
<link rel='stylesheet' id='nespresso-css'  href='https://www.nespresso.vn/wp-content/plugins/nespresso/public/css/nespresso-public.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='smart-search-css'  href='https://www.nespresso.vn/wp-content/plugins/smart-woocommerce-search/assets/css/general.css?ver=ysm-1.5.5' type='text/css' media='all' />
<style id='smart-search-inline-css' type='text/css'>
.widget_search.ysm-active .smart-search-suggestions .smart-search-post-icon{width:50px;}.widget_product_search.ysm-active .smart-search-suggestions .smart-search-post-icon{width:50px;}
</style>
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='fpf_front-css'  href='https://www.nespresso.vn/wp-content/plugins/flexible-product-fields/assets/css/front.min.css?ver=39' type='text/css' media='all' />
<link rel='stylesheet' id='trp-language-switcher-style-css'  href='https://www.nespresso.vn/wp-content/plugins/translatepress-multilingual/assets/css/trp-language-switcher.css?ver=1.3.8' type='text/css' media='all' />
<link rel='stylesheet' id='storefront-style-css'  href='https://www.nespresso.vn/wp-content/themes/storefront/style.css?ver=2.1.8' type='text/css' media='all' />
<style id='storefront-style-inline-css' type='text/css'>
@media (min-width: 768px){.woocommerce-checkout.yith-wcms #order_review, .woocommerce-checkout.yith-wcms #order_review_heading {width: 100%; float: none;}}

			.main-navigation ul li a,
			.site-title a,
			ul.menu li a,
			.site-branding h1 a,
			.site-footer .storefront-handheld-footer-bar a:not(.button),
			button.menu-toggle,
			button.menu-toggle:hover {
				color: #d5d9db;
			}

			button.menu-toggle,
			button.menu-toggle:hover {
				border-color: #d5d9db;
			}

			.main-navigation ul li a:hover,
			.main-navigation ul li:hover > a,
			.site-title a:hover,
			a.cart-contents:hover,
			.site-header-cart .widget_shopping_cart a:hover,
			.site-header-cart:hover > li > a,
			.site-header ul.menu li.current-menu-item > a {
				color: #ffffff;
			}

			table th {
				background-color: #f8f8f8;
			}

			table tbody td {
				background-color: #fdfdfd;
			}

			table tbody tr:nth-child(2n) td {
				background-color: #fbfbfb;
			}

			.site-header,
			.secondary-navigation ul ul,
			.main-navigation ul.menu > li.menu-item-has-children:after,
			.secondary-navigation ul.menu ul,
			.storefront-handheld-footer-bar,
			.storefront-handheld-footer-bar ul li > a,
			.storefront-handheld-footer-bar ul li.search .site-search,
			button.menu-toggle,
			button.menu-toggle:hover {
				background-color: #2c2d33;
			}

			p.site-description,
			.site-header,
			.storefront-handheld-footer-bar {
				color: #9aa0a7;
			}

			.storefront-handheld-footer-bar ul li.cart .count,
			button.menu-toggle:after,
			button.menu-toggle:before,
			button.menu-toggle span:before {
				background-color: #d5d9db;
			}

			.storefront-handheld-footer-bar ul li.cart .count {
				color: #2c2d33;
			}

			.storefront-handheld-footer-bar ul li.cart .count {
				border-color: #2c2d33;
			}

			h1, h2, h3, h4, h5, h6 {
				color: #484c51;
			}

			.widget h1 {
				border-bottom-color: #484c51;
			}

			body,
			.secondary-navigation a,
			.onsale,
			.pagination .page-numbers li .page-numbers:not(.current), .woocommerce-pagination .page-numbers li .page-numbers:not(.current) {
				color: #43454b;
			}

			.widget-area .widget a,
			.hentry .entry-header .posted-on a,
			.hentry .entry-header .byline a {
				color: #75777d;
			}

			a  {
				color: #96588a;
			}

			a:focus,
			.button:focus,
			.button.alt:focus,
			.button.added_to_cart:focus,
			.button.wc-forward:focus,
			button:focus,
			input[type="button"]:focus,
			input[type="reset"]:focus,
			input[type="submit"]:focus {
				outline-color: #96588a;
			}

			button, input[type="button"], input[type="reset"], input[type="submit"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {
				background-color: #96588a;
				border-color: #96588a;
				color: #ffffff;
			}

			button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {
				background-color: #7d3f71;
				border-color: #7d3f71;
				color: #ffffff;
			}

			button.alt, input[type="button"].alt, input[type="reset"].alt, input[type="submit"].alt, .button.alt, .added_to_cart.alt, .widget-area .widget a.button.alt, .added_to_cart, .pagination .page-numbers li .page-numbers.current, .woocommerce-pagination .page-numbers li .page-numbers.current, .widget a.button.checkout {
				background-color: #2c2d33;
				border-color: #2c2d33;
				color: #ffffff;
			}

			button.alt:hover, input[type="button"].alt:hover, input[type="reset"].alt:hover, input[type="submit"].alt:hover, .button.alt:hover, .added_to_cart.alt:hover, .widget-area .widget a.button.alt:hover, .added_to_cart:hover, .widget a.button.checkout:hover {
				background-color: #13141a;
				border-color: #13141a;
				color: #ffffff;
			}

			#comments .comment-list .comment-content .comment-text {
				background-color: #f8f8f8;
			}

			.site-footer {
				background-color: #f0f0f0;
				color: #61656b;
			}

			.site-footer a:not(.button) {
				color: #2c2d33;
			}

			.site-footer h1, .site-footer h2, .site-footer h3, .site-footer h4, .site-footer h5, .site-footer h6 {
				color: #494c50;
			}

			#order_review,
			#payment .payment_methods > li .payment_box {
				background-color: #ffffff;
			}

			#payment .payment_methods > li {
				background-color: #fafafa;
			}

			#payment .payment_methods > li:hover {
				background-color: #f5f5f5;
			}

			@media screen and ( min-width: 768px ) {
				.secondary-navigation ul.menu a:hover {
					color: #b3b9c0;
				}

				.secondary-navigation ul.menu a {
					color: #9aa0a7;
				}

				.site-header-cart .widget_shopping_cart,
				.main-navigation ul.menu ul.sub-menu,
				.main-navigation ul.nav-menu ul.children {
					background-color: #24252b;
				}
			}
</style>
<link rel='stylesheet' id='storefront-fonts-css'  href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,700,900&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='woo-viet-provinces-style-css'  href='https://www.nespresso.vn/wp-content/plugins/woo-viet/assets/provinces.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='tawcvs-frontend-css'  href='https://www.nespresso.vn/wp-content/plugins/variation-swatches-for-woocommerce/assets/css/frontend.css?ver=20160615' type='text/css' media='all' />
<link rel='stylesheet' id='storefront-woocommerce-style-css'  href='https://www.nespresso.vn/wp-content/themes/storefront/assets/sass/woocommerce/woocommerce.css?ver=4.9.8' type='text/css' media='all' />
<style id='storefront-woocommerce-style-inline-css' type='text/css'>

			a.cart-contents,
			.site-header-cart .widget_shopping_cart a {
				color: #d5d9db;
			}

			table.cart td.product-remove,
			table.cart td.actions {
				border-top-color: #ffffff;
			}

			.woocommerce-tabs ul.tabs li.active a,
			ul.products li.product .price,
			.onsale,
			.widget_search form:before,
			.widget_product_search form:before {
				color: #43454b;
			}

			.woocommerce-breadcrumb a,
			a.woocommerce-review-link,
			.product_meta a {
				color: #75777d;
			}

			.onsale {
				border-color: #43454b;
			}

			.star-rating span:before,
			.quantity .plus, .quantity .minus,
			p.stars a:hover:after,
			p.stars a:after,
			.star-rating span:before,
			#payment .payment_methods li input[type=radio]:first-child:checked+label:before {
				color: #96588a;
			}

			.widget_price_filter .ui-slider .ui-slider-range,
			.widget_price_filter .ui-slider .ui-slider-handle {
				background-color: #96588a;
			}

			.woocommerce-breadcrumb,
			#reviews .commentlist li .comment_container {
				background-color: #f8f8f8;
			}

			.order_details {
				background-color: #f8f8f8;
			}

			.order_details > li {
				border-bottom: 1px dotted #e3e3e3;
			}

			.order_details:before,
			.order_details:after {
				background: -webkit-linear-gradient(transparent 0,transparent 0),-webkit-linear-gradient(135deg,#f8f8f8 33.33%,transparent 33.33%),-webkit-linear-gradient(45deg,#f8f8f8 33.33%,transparent 33.33%)
			}

			p.stars a:before,
			p.stars a:hover~a:before,
			p.stars.selected a.active~a:before {
				color: #43454b;
			}

			p.stars.selected a.active:before,
			p.stars:hover a:before,
			p.stars.selected a:not(.active):before,
			p.stars.selected a.active:before {
				color: #96588a;
			}

			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {
				background-color: #96588a;
				color: #ffffff;
			}

			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {
				background-color: #7d3f71;
				border-color: #7d3f71;
				color: #ffffff;
			}

			@media screen and ( min-width: 768px ) {
				.site-header-cart .widget_shopping_cart,
				.site-header .product_list_widget li .quantity {
					color: #9aa0a7;
				}
			}
</style>
<link rel='stylesheet' id='storefront-child-style-css'  href='https://www.nespresso.vn/wp-content/themes/storefront-child/style.css?ver=4.9.8' type='text/css' media='all' />
<script type='text/javascript' src='https://www.nespresso.vn/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://www.nespresso.vn/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/axapta-api/public/js/axapta-api-public.js?ver=1.0.0'></script>
<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/html5-responsive-faq/js/hrf-script.js?ver=4.9.8'></script>
<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/nespresso/public/js/nespresso-public.js?ver=1.0.0'></script>
<link rel='https://api.w.org/' href='https://www.nespresso.vn/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.nespresso.vn/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.nespresso.vn/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.8" />
<meta name="generator" content="WooCommerce 3.5.2" />
<link rel="canonical" href="https://www.nespresso.vn/" />
<link rel='shortlink' href='https://www.nespresso.vn/' />
<link rel="alternate" type="application/json+oembed" href="https://www.nespresso.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nespresso.vn%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.nespresso.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nespresso.vn%2F&#038;format=xml" />
<!-- start Simple Custom CSS and JS -->
<style type="text/css">
.select2-results__options li {
  color: #333 !important;
}

.select2-results__options li.select2-results__option--highlighted {
  color: #fff !important;
}</style>
<!-- end Simple Custom CSS and JS -->

<!-- This website runs the Product Feed PRO for WooCommerce by AdTribes.io plugin -->
        <!--noptimize-->
        <!-- Global site tag (gtag.js) - Google Ads: 770954263 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-770954263"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-770954263');
        </script>
        <!--/noptimize-->

		<link rel="alternate" hreflang="en-US" href="https://www.nespresso.vn/"/><link rel="alternate" hreflang="vi" href="https://www.nespresso.vn/vi/"/><script type="text/javascript">
           var ajaxurl = "https://www.nespresso.vn/wp-admin/admin-ajax.php";
           var login_url = "https://www.nespresso.vn/my-account/";
         </script>	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	
<!-- Facebook Pixel Code -->
<script type='text/javascript'>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
</script>
<!-- End Facebook Pixel Code -->
<script type='text/javascript'>
  fbq('init', '2387221091407358', [], {
    "agent": "wordpress-4.9.8-1.7.25"
});
</script><script type='text/javascript'>
  fbq('track', 'PageView', []);
</script>
<!-- Facebook Pixel Code -->
<noscript>
<img height="1" width="1" style="display:none" alt="fbpx"
src="https://www.facebook.com/tr?id=2387221091407358&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->
		<script type="text/javascript">
			var cli_flush_cache=2;
		</script>
		<style type="text/css" id="custom-background-css">
body.custom-background { background-color: ##ffffff; }
</style>
<link rel="alternate" type="application/rss+xml" title="RSS" href="https://www.nespresso.vn/rsslatest.xml" />		<style type="text/css" id="wp-custom-css">
			/*
You can add your own CSS here.

Click the help icon above to learn more.
*/

.site-control .btn {
  line-height: 40px !important;
}

.account-sidebar {
    padding: 20px 0 0;
    background-color: #f9f9f9;
    margin-right: 0 !important;
}

div.wpcf7-validation-errors {
    border: 2px solid #f7002e;
    color: #3D8705;
}


div.wpcf7-mail-sent-ok {
    border: 2px solid #398f14;
    color: red;
}

.contactus .contactus-main textarea {
    height: 200px !important;
}

.contactus .contactus-main {
    padding: 0 20px !important;
}

.contactus .contactus-main .col-md-6 .input-text, .contactus .contactus-main .col-md-6 input[type="text"], .contactus .contactus-main .col-md-6 input[type="email"], .contactus .contactus-main .col-md-6 input[type="url"], .contactus .contactus-main .col-md-6 input[type="password"], .contactus .contactus-main .col-md-6 input[type="search"], .contactus .contactus-main .col-md-6 textarea {
    width: 100%;
}

.input-group.input-group-generic input::-moz-placeholder {
  color: #666 !important;
}

.input-group.input-group-generic input::-webkit-placeholder {
  color: #666 !important;
}

/*
For contact-us desktop page
start
*/

.page-id-41 .wpcf7-form-control {
height:38px !important;
}

.page-id-41 .input-group.input-group-generic .desktop-label {
	padding-top: 21px !important;
}

.page-id-41 .desktop-label {
      width: 185px !important;
}

.page-id-41 .dropdown {
   margin-bottom: 0px !important;
}
/*
For contact-us desktop page
end
*/

		</style>
	    <!-- /wp_head -->

        <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/libs/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/libs/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/style.css">
    <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/css-helpers.css">
    <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/style.css">
    <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/custom.css">
    <link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/override.css">

    

    <script type="text/javascript">

        //Please execute the following dataLayer initialization on page load with the required site or page level parameters and attributes.
        // **NOTE** If any value is not obtainable or not relevant to the page, please leave the value blank/empty so nothing is passed into Google Analytics.
        var page_category = document.getElementById('product_category');
        gtmDataObject = [{
            'isEnvironmentProd': 'Yes',
            'pageName': 'Home | Nespresso',
            'pageType': 'Home', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
            'pageCategory': 'home', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
            'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
            'pageTechnology': 'OriginalLine', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
            'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
            'market': 'VN',
            'version': 'VN - WooCommerce V1',
            'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
            'segmentBusiness': 'B2C',
            'currency': 'VND',
            'clubMemberID': '', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
            'clubMemberStatus': 'false', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
            'clubMemberLevel': '', //market level name for user level that is a club member
            'clubMemberTierID': '', //Global HQ level ID for club membership level
            'clubMemberTitle': '',
            'clubMemberLoginStatus': 'false', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
            'machineOwner': '', //If user owns a machine or not, if known
            'machineOwned': '', //If multiple machines owned, separate with "|||" symbol
            'preferredTechnology': 'Espresso',
            'event': 'event_pageView',
            'persistentBasketLoaded': 'false', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
            'language': 'EN'
        }];
    </script>
    <script type="text/javascript">
    function removeFromCartGTM(cart_item_key) {
        var data = {
            action: 'gtm_get_basket',
            cart_item_key: cart_item_key
        };
        $.ajax({
            dataType: 'json',
            type: 'post',
            data: data,
            url: ajaxurl,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                // return false;
            },
            success: function (data) {
                if(data.status) {
                    gtmDataObject.push({
                        'event': 'removeFromCart',
                        'landscape': 'Wordpress',
                        'segmentBusiness': 'B2C',
                        'currencyCode' : 'VND',
                        'ecommerce': {
                            'remove': {
                                'actionField': {

                                },
                                'products': data.products
                            },
                        },
                    });
                }

            } // success()

        });
    }

    function addToCartGTM(item) {
        var data = {
            action: 'gtm_get_basket',
        };
        var metric10 = ''
        var metric11 = ''
        var metric12 = ''

        var dimension57 = '';

        switch (item.type)
        {
            case 'Coffee':
                dimension57 = 'Single Product - Capsule';
                metric10 = item.quantity;
            break;
            case 'Machine':
                dimension57 = 'Variable Product - Machine';
                metric11 = item.quantity;
            break;
            case 'Accessory':
                dimension57 = 'Single Product - Accessory';
                metric12 = item.quantity;
            break;

        }

        // var data = {
        //     action: 'gtm_add_to_cart',
        //     id: item.id
        // };
        // $.ajax({
        //     dataType: 'json',
        //     type: 'post',
        //     data: data,
        //     url: ajaxurl,
        //     dataType: 'json',
        //     error: function (jqXHR, textStatus, errorThrown) {
        //         console.log(errorThrown);
        //         // return false;
        //     },
        //     success: function (data) {
        //         if(data.status) {
        //             gtmDataObject.push({
        //                 'event': 'removeFromCart',
        //                 'landscape': 'Wordpress',
        //                 'segmentBusiness': 'B2C',
        //                 'currencyCode' : 'VND',
        //                 'ecommerce': {
        //                     'remove': {
        //                         'actionField': {

        //                         },
        //                         'products': data.products
        //                     },
        //                 },
        //             });
        //         }

        //     } // success()

        // });

        gtmDataObject.push({
            'event': 'addToCart',
            'landscape': 'Wordpress',
            'segmentBusiness': 'B2C',
            'currencyCode' : 'VND',
            'ecommerce': {
                'add': {
                    'actionField': {

                    },
                    'products': [
                        {
                            'name': item.name,
                            'id': item.hqid,
                            'dimension54': item.name,
                            'dimension55': item.range,
                            'dimension53': item.sku,
                            'quantity': item.quantity,
                            'price': item.price,
                            'category': item.type.toLowerCase(),
                            'dimension56': item.technology,
                            'brand': 'Nespresso',
                            'metric10': metric10,
                            'metric11': metric11,
                            'metric12': metric12,
                            'dimension57': dimension57
                        }
                    ]
                },
            },
        });
    }
</script>
    <!--  Google Tag Manager  -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].unshift({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','gtmDataObject','GTM-KMPTP65');</script>
    <!-- End Google Tag Manager -->

    <script type="text/javascript">
        window.host_url = 'https://www.nespresso.vn';
        window.nespresso_token = '032da4cc0e26717ca42f91b2ab071669';
        window.currency_symbol = '&#8363;';
        window.currency = 'VND';
                    window.user =  null;
        
    </script>
<link rel="stylesheet" href="owl/assets/owl.carousel.min.css">
<link rel="stylesheet" href="owl/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="css/mycss.css?a=<?=time()?>">
</head>
<body class="home page-template-default page page-id-262 custom-background woocommerce-no-js translatepress-en_US yith-wcms show_checkout_login_reminder storefront-full-width-content right-sidebar woocommerce-active">
<?php include 'header.php' ?>
<main id="content">
    <div class="container-fluid">
        <div class="row">
			<div id="sliderbanner" class="owl-carousel">
			  <div class="slideitem" style="background-image: url(img/backgrounds/slide-1_L.jpg);" >
				<div class="sgv">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1115.68 173.48" class="injected-svg g_visual" src="img/hero/ispirazione-italiana_L.svg" aria-hidden="true"><path d="M642.48 77.69c-11.55 5.71-23.33 10.91-35.68 14.67-6.15 1.87-12.76 3.99-19.23 4.21-2.07.07-5.05.12-5.45-2.43-.44-2.87.17-6.17 1.03-8.9.78-2.48 2.05-5.66 3.89-7.57.99-1.03 3.82-2.38 6.08-3.16 5.39-1.84 10.5-5.18 15.17-8.51 2.34-1.67 6.08-4.53 8.28-6.39 2.09-1.77 4.54-3.53 6.06-5.86 2.61-4.01.63-8.64-2.82-11.37-3.2-2.53-7.78-3.82-11.78-2.77-5.69 1.49-10.47 6.33-14.87 9.97-4.53 3.74-9.38 7.03-13.8 10.91-4.75 4.18-9.16 8.73-13.4 13.42-8.1 8.97-15.29 19.41-24.79 26.98 1.93-6.28 4.16-12.36 6.51-18.49 2.46-6.41 4.05-13.07 6.41-19.52 1.05-2.88 2.37-6.04 2.69-9.11.28-2.7-2.87-4.1-5.2-2.89-2.8 1.45-5.43 4.31-7.89 6.3l-8.21 6.65c1.92-4.46 4.28-8.89 5.11-13.71 1.18-6.86 2.22-15.42-6.16-17.86-3.99-1.16-8.46-.36-12.44.46-3.98.82-9.77 1.75-12.66 4.94-2.72 3.01-1.11 6.87-2.48 10.25-1.96 4.84-4.81 9.48-7.6 13.87-5.42 8.54-11.23 17.1-19.17 23.49-3.76 3.03-7.4 6.23-11.46 8.86-1.92 1.25-3.97 2.44-6.11 3.27-2.05.8-4.44 2-6.7 1.76-.62-.07-1.24-.25-1.71-.65-.77-.65-1.04-1.73-1.04-2.74-.02-5.89 2.14-11.51 5.27-16.42 3.15-4.94 5.78-8.66 9.55-13.1 3.24-3.82 7.99-8.15 11.89-11.37 3.53-2.92 7.84-6.23 12.26-7.6 1.92-.6 11.8-1.74 7.17-5.06-4.43-3.18-10-.49-14.37 1.33-3.15 1.31-6.24 2.77-9.17 4.52-8.93 5.32-17.72 12.21-24.25 20.55-5.75 7.34-10.4 16.67-9.33 26.22.49 4.41 2.14 9.76 5.03 13.22 2.87 3.43 9.03 2.89 13.07 2.64 9.15-.58 17.63-6.73 24.63-12.3 6.96-5.54 13.02-12.17 17.71-19.73 5.55-8.94 10.17-18.72 11.3-29.3.27-2.55.34-5.13.2-7.69-.15-2.89.59-2.98 3.29-3.77 4.89-1.44 10.38-2.99 15.53-2.82 4.91.16 1.97 6.58.81 9.32-2.08 4.96-4.59 9.74-7.14 14.47-2.63 4.88-5.35 9.71-7.92 14.63-2.16 4.15-5.37 9-5.51 13.81-.08 2.69 2.66 10.43 5.68 5.98 2.84-4.18 5.15-8.69 8.4-12.6 3.07-3.7 6.64-6.94 9.78-10.56 1.77-2.04 3.33-4.2 5.29-6.07 1.88-1.78 3.91-3.4 5.79-5.18-.16 2.04-1.54 3.8-2.5 5.6-1.26 2.37-2.48 4.75-3.68 7.14-2.32 4.64-4.51 9.34-6.59 14.1-2.79 6.39-8.02 14.34-6.16 21.59.94 3.66 3.55 7.4 7.08 8.94 3.38 1.47 4.17-1.19 5.9-3.51 2.66-3.57 5.76-6.87 8.53-10.37 2.84-3.58 5.65-7.2 8.56-10.72 5.35-6.48 10.9-12.82 16.37-19.2-1.56 6.26-3.42 12.71-1.94 19.18 1.59 6.95 7.12 12.56 14.44 12.8 3.67.12 7.29-.89 10.75-2.15 14.41-5.27 28.49-11.41 41.2-20.15 1.57-1.08 3.07-2.28 4.66-3.33.79-.53 2.44-1.04 2.81-2 1.05-2.7-4.18.49-4.97.88zm-53.38-7.48c3.22-6.01 7.17-11.76 11.92-16.63 2.49-2.54 5.13-4.9 8.09-6.88 2.11-1.41 6.04-4.28 8.65-2.46 5.76 4.03-5.42 11.95-8.35 14.16-5.68 4.29-13.72 8.25-19.85 12.24-.27.18-.62-.13-.46-.43zm-147.01 2.05c-2.97 5.51-5.64 11.19-8.86 16.57-.81 1.35-1.66 2.66-2.52 3.96-.79 1.2-1.57 2.39-2.55 2.63s-3.57-1.95-4.81-3.76c-.83-1.21-.02-3.82.55-5.13 3.42-7.81 7.44-15.37 12.09-22.52 1.62-2.49 3.29-5.8 5.51-7.8 1.89-1.69 4.33-.76 5.95.9 1.97 2.01 1.32 4.19-.05 6.28-1.89 2.89-3.67 5.83-5.31 8.87zm-197.33-6.31c-4.21 9.37-12.51 17.06-20.93 22.91-8.93 6.21-18.95 12-29.32 15.84-5.23 1.94-11.08 4.35-16.81 4.58-4.7.19-7.85 2.61-9.63 6.69-4.86 11.15-11.34 21.25-16.79 32.14-5.32 10.62-4.38 11.61-11.76 20.95-1.24 1.57-2.07 4.2-4.45 4.4-2.73.23-3.97-2.16-4.43-4.36-1.21-5.66 2-11.2 4.1-16.12 4.97-11.61 5.88-13.89 10.99-25.44 9.93-22.44 20.38-44.5 32.83-65.67 2.51-4.28 4.99-10.05 9.09-13.13 4.64-3.49 6.56 1.87 7 5.9.59 5.46-2.84 9.76-5.23 14.36-2.87 5.52-5.68 11.07-8.46 16.63-1.32 2.63-2.64 5.27-3.95 7.91-1.23 2.46-1.67 5.85-3.83 9.47 11.2-3.09 24.1-9.56 33.92-15.34 9.58-5.64 19.25-12.2 25.21-21.54 2.3-3.6 4.83-8.04 4.31-12.54-.46-4.01-3.61-6.87-7.12-8.8-10.39-5.7-22.86-7.61-34.39-7.42-6.71.11-13.48.81-19.96 2.14-2.14.44-4.23 1.04-6.29 1.73-1.25.42-2.62.86-3.75 1.5-.94.53-1.45 1.41-2.3 2.02-3.06 2.21-9.68-3.1-7.37-6.26 1.04-1.42 4.14-2.43 5.67-3.16 2.21-1.05 4.53-1.89 6.92-2.53 11.84-3.17 24.95-3.77 37.37-1.91 10.72 1.61 22.63 5.02 31.69 11.31 8.07 5.62 11.55 15.11 7.67 23.74zM966.4 84.12c-3.12 9.26-6.28 18.5-9.91 27.58-.5 1.26-.9 4.07-2.93 3.45-1.8-.56-2.99-3.58-3.28-5.04-.85-4.17 1.43-8.63 2.74-12.39 1.8-5.19 3.81-10.3 5.79-15.42 1.71-4.43 3.23-8.95 5.75-13.02.05-.07-.07-.15-.13-.09-16.96 15.86-25.3 34.82-38.55 52.84-1.14 1.55-3.73 1.54-5.09-.05-2.19-2.57-4.16-5.28-4.7-8.47-.87-5.13 3.93-9.32 5.93-13.72 2.33-5.14 14.9-28.77 15.13-29.41 1.67-4.7-10.89 8.12-11.69 8.98-7.65 8.16-16.61 16.36-22.86 25.55-3.6 5.29-13.93 13.12-17.19 4.17-1.72-4.73 1.39-9.45 3.37-13.77 2.15-4.7 4.46-9.95 5.46-15.03-5.16 5.73-10.16 11.64-15.46 17.25-3.98 4.21-9.48 11.12-16.86 8.51-6.34-2.24-4.16-9.88-2.97-14.08 1.75-6.16 5.1-11.73 9.15-16.75 7.82-9.69 19.57-20.31 33.63-21.97 2.89-.34 6.03-.7 8.4 1.44.69.62 1.79 1.78 1.85 2.65.17 2.19-1.69 1.52-3.15.93-6.75-2.73-12.48 2.29-17.07 5.78-11.42 8.69-19.5 20.26-24.49 33.12 7.12-5.55 13.44-11.69 19.94-17.8 3.07-2.89 6.24-5.7 9.53-8.39 1.77-1.45 3.58-2.86 5.43-4.23 1.51-1.12 2.48-2.37 3.84-3.58 2.6-2.32 5.83-.95 6.28 2.3.49 3.59-1.15 6.88-2.89 9.84-4.07 6.95-8.12 14.27-10.25 22.04 12.34-13.72 24.05-27.75 37.67-40.47 1.71-1.59 3.12-2.94 4.16-4.99.66-1.31 1.34-2.57 3.18-2.65 1.63-.07 3.19 1.15 4.64 1.76 1.51.63 3.15 1.01 4.35 2.17 3.26 3.17-3.8 10.28-4.89 13.47-1.48 4.32-2.78 8.7-4.65 12.87 3.43-3.4 10.28-9.17 13.73-12.55 2.79-2.74 6.83-7.19 11.79-4.99 4.07 1.81 2.96 5.38 1.96 8.3-1.57 4.61-3.13 9.24-4.69 13.86zM107.34 73.93c-7.22 14.44-13.95 29.79-23.56 42.81-6.82 9.26-15.45 18.44-26.48 22.51-10.75 3.97-22.61 4.15-33.5.66-8.54-2.74-15.9-7.78-22.61-13.62-.4-.35-.81-.72-1.06-1.19-.12-.23-.2-.51-.08-.74.22-.44.88-.32 1.34-.12 4.75 2.05 9.07 4.9 13.94 6.66 4.91 1.78 10.3 1.98 15.42 1.17 10.86-1.7 21.3-6.71 29.7-13.75 3.87-3.24 7.33-6.97 10.38-10.98 7.83-10.31 14.3-21.73 20.59-33.02 6.72-12.07 13.1-24.32 19.7-36.45 2.97-5.46 5.94-11.28 8.99-16.84 1.57-2.86 3.19-5.69 4.9-8.47.73-1.18 4.71-5.29 1.82-5.86-1.99-.39-5.4 2.69-7.02 3.7a101.1 101.1 0 00-7.87 5.45c-9.79 7.5-18.04 16.83-24.8 27.11-3.33 5.05-6.44 10.56-7.06 16.7-.14 1.42-.16 2.87.14 4.27.27 1.25 1.3 2.5 1.32 3.77.05 2.62-3.67 3.55-5.31 2.01-4.23-3.96-4.77-10.35-3.17-15.57C77.2 40.67 86.55 29.37 96.6 19.82c4.84-4.6 9.88-9.08 15.56-12.6C117.61 3.85 125.1.03 131.68 0c13.97-.05-.15 23.06-2.48 28.17-7.01 15.38-14.3 30.64-21.86 45.76zm1008.21-2.15c-.74 1.71-4.37-.5-5.8-.74-5.4-.91-11.13.56-16.13 1.82-10.16 2.57-19.57 6.76-28.75 11.18-9.13 4.4-18.05 9.15-26.41 14.61-8.49 5.54-15.96 11.95-23.32 18.63-3.44 3.12-6.76 7.56-12.25 7.53-4.53-.02-9.9-2.09-12.06-6.1-2.22-4.12.61-8.5 1.98-12.2 1.97-5.33 3.58-10.8 5.97-15.97-5.11 5.32-9.91 11.18-15.84 15.77-3.89 3.02-11.03 8.16-15.46 1.93-3.6-5.07-1.75-9.83 1.03-14.33 3.33-5.4 7.83-10.35 12.4-14.89 8.76-8.7 20.2-18 33.34-21.02 2.33-.54 6.02-1.59 8.16.35 3.26 2.94-3.24 2.42-4.61 2.81-2.46.7-5.07 1.81-7.09 3.14-2.88 1.9-6.19 4.26-8.88 6.37-5.86 4.58-10.06 10.01-14.87 15.51-4.82 5.51-8.42 11.54-12.37 17.57-.11.18.14.39.32.27 7.85-5.31 14.09-12.2 20.75-18.63 2.22-2.14 4.38-4.33 6.52-6.54 3.06-3.16 6.75-6.21 8.86-10.06.64-1.16 1-3.16 2.26-3.92 1.24-.75 2.99-.12 4.38.24 2.37.62 4.34 1.38 4.23 3.87-.16 3.79-2.55 7.79-3.94 11.25-1.62 3.99-3.3 7.96-4.89 11.96-3.59 9.01-6.08 18.23-8.48 27.62 7.32-6.63 14.43-13.39 22.58-19.22 8.14-5.83 16.92-10.93 25.97-15.61 9.44-4.88 19.13-9.57 29.33-13.2 9.65-3.44 20.08-6.21 30.74-2.33 1.11.41 2.95.92 2.33 2.33zM302.5 69.6c-6.27 8.95-11.94 18.39-17.2 27.97-2.55 4.64-4.65 9.35-6.46 14.32-.69 1.9-1.2 3.67-2.93 4.89-1.15.81-3.45 2.23-4.85 1.6-1.23-.56-2.49-2.73-3.35-3.75-1.5-1.76-2.32-3.77-2.31-6.09.03-4.44 2.29-9.16 3.82-13.25 4.39-11.74 9.87-23.03 14.75-34.57-6.66 7.2-13.34 14.28-19.56 21.87-6.11 7.46-11.76 15.76-19.29 21.88-1.6 1.3-3.01 2.04-4.87.75-1.5-1.04-2.68-2.76-3.09-4.54-.98-4.24 2-9.39 3.76-13.08 2.21-4.64 4.51-9.24 6.72-13.88 1.47-3.09 2.87-8.87 7.33-6.44 4.94 2.7.43 8.52-1.16 12.13-2.39 5.43-4.68 10.9-7.04 16.34 6.82-6.86 12.24-14.91 18.61-22.17 6.77-7.72 13.95-15.16 21.06-22.57 1.49-1.55 2.13-2.75 3.04-4.62.8-1.64 2.14-2.06 3.94-1.76 2.34.39 5.68 1.33 7.38 3.1 1.5 1.56 1.37 3.63.91 5.6-.97 4.16-2.94 8.23-4.42 12.23 4.55-4.51 8.88-9.05 13.96-12.97 4.14-3.2 12.34-8.07 15.55-5.58 3.22 2.48-.32 7.38-1.4 8.51-2.35 2.45-7.24 6.79-8.49 5.63s1.34-2.81.22-4.5c-1.56-2.36-3.9.71-5.24 1.94-3.57 3.29-6.62 7.07-9.39 11.01zm421.17-38.1c-10.61 17.04-23.26 32.86-33.94 49.85-6.01 9.55-11.35 19.48-17.23 29.11-1.5 2.45-3.11 4.85-4.99 7.06-1.25 1.48-2.52 4.25-4.53 5.17-4.29 1.95-8.02-4.52-8.28-7.52.71-4.85 3.3-9.31 5.46-13.63 2.25-4.52 4.69-8.95 7.28-13.31 5.59-9.43 11.83-18.5 18.16-27.49 5.67-8.05 11.15-16.21 16.47-24.46 5.07-7.86 10.3-15.76 14.49-24.07.9-1.78 1.86-3.49 1.7-5.55-.16-2 .34-2.73 2.79-2.46 3.41.38 7.1 2.37 8.96 5.1 5.08 7.42-2.71 16.36-6.34 22.2zm-592.12 86.52c8.89-.91 13.9-11.84 15.57-19.44.4-1.81.72-4.09 2.44-5.17 1.32-.82 3.05-.78 4.53-.96 1.58-.2 3.12-.5 4.58-1.08.47-.19.48-.86.01-1.06-1.45-.61-2.83-.82-4.38-.87-1.23-.04-3.94-.13-4.34-.71-.57-.83.18-3.44.35-4.76.4-3.16 1.05-6.3 1.94-9.36 1.08-3.74 2.08-7.85 4.22-11.15 1.2-1.85 2.77-3.12 4.24-4.71 1.43-1.54.63-3.73-1.71-3.98-1.55-.17-5.63 3.62-6.01 3.91-.8.63-1.6 1.25-2.4 1.88-1.6 1.26-3.19 2.53-4.77 3.81-9.61 7.77-19.01 15.88-27.85 24.52a310.903 310.903 0 00-18.99 20.27c-1.56 1.82-3.1 3.67-4.62 5.53-1.24 1.52-3.09 3.21-3.78 5.07-.69 1.87.21 3.78 2.05 4.55.92.39 3.32.8 4.23.34 1.34-.68.88-2.28 1.28-3.48.66-1.93 2.18-3.87 3.35-5.52 1.34-1.88 2.8-3.67 4.32-5.4 1.03-1.18 2.49-3.47 4.19-3.75 2.43-.41 4 3.05 5.52 4.35 2.41 2.06 4.94 4.09 7.81 5.49 2.53 1.23 5.4 1.97 8.22 1.68zm-10.89-24.45c3.78-3.62 7.57-7.24 11.35-10.86 1.94-1.86 3.88-3.71 5.82-5.57.71-.68 2.82-3.19 4.32-4.2.48-.32 1.1.13.91.68-1 2.95-1.99 5.9-2.99 8.86-.66 1.95-1.33 6.49-3.03 7.65-.91.62-2.98.74-4.03 1l-4.14 1.02c-2.64.65-5.28 1.29-7.92 1.94-.31.07-.51-.31-.29-.52zm-3.52 12.35c-1.85-1.36-3.88-2.6-1.22-4.09 4.61-2.58 9.89-4.28 14.93-5.76 1.2-.35 5.1-1.91 5.31-.55.14.92-1.13 2.99-1.51 3.81-.62 1.32-1.27 2.55-1.94 3.85-2.03 3.9-4.68 5.26-9.12 5.24-2.41-.05-4.56-1.1-6.45-2.5zM351.8 88.87c2.28-8.14 5.34-16.03 8.69-23.78 1.68-3.88 3.43-7.73 5.21-11.57 1.29-2.78 4.48-6.45-1.03-8.01-3.82-1.08-5.76.64-7.2 3.05-1.02 1.72-1.56 1.8-3.87 1.51-2.02-.25-3.86.1-5.56.79-7.65 3.1-14.46 7.93-20.12 13.1-5.82 5.31-11.46 11.15-16.06 17.32-3.65 4.89-8.95 13.56-4.2 19.96.9 1.21 2.5 3.04 4.28 2.84 1.36-.15 2.36-1.56 3.37-2.19 3.67-2.27 6.72-5.08 9.86-7.87 6.71-5.95 13.17-12.1 19.21-18.57-1.17 4.62-2.78 9.11-3.79 13.78-.83 3.82-.66 3.29 1.34 7.3.59 1.19 2.68 4.48 4.44 4.41 1.61-.06 2.81-3.26 3.22-4.28 1.65-4.08 1.02-3.55 2.21-7.79zm-40.59 8.35c-.11.08-.3-.04-.25-.16 7.5-18.08 21.7-31.23 39.87-41.46.5-.28 1.2.29.9.73-10.63 15.73-24.4 29.16-40.52 40.89zm140.06-52.37c-.47-1.33 1.57-2.84 2.3-3.69 2.03-2.35 4.38-5.56 7.91-4.82 1.95.41 4.87 3.09 3.94 5.37-.63 1.54-3.37 1.74-4.7 2.15-1.89.58-3.75 1.25-5.64 1.85-1.09.16-3.29.6-3.81-.86zm417.87 11.21c-.51 1.41-3.26 1.59-4.56 1.97-1.86.53-3.68 1.14-5.53 1.69-1.1.14-3.28.54-3.92-.79-.58-1.21 1.35-2.6 2.02-3.37 1.85-2.15 3.97-5.09 7.59-4.4 2 .37 5.15 2.82 4.4 4.9zm-614.63-5.15c-.56-.89.88-2.54 1.29-3.25.61-1.07 1.06-3.15 1.9-3.96 1.04-1.01 4.02.08 6.4 1.23 2.04.99 2.32 3.83.47 5.14-.68.48-1.4.84-2.01 1.04-1.23.41-2.36.12-3.63.12-1.02-.01-3.75.73-4.42-.32zm168.72 46.37c-.02 1.13-1.88 1.43-2.65 1.54-3.48.5-12.57 2.14-16.94 3.06-1.83.43-3.69.76-5.53 1.14-8.27 1.68-16.59 3.29-24.7 5.63a92.07 92.07 0 00-11.39 4.11c-1.78.78-3.56 1.59-5.27 2.5-1.47.77-3.15 2.03-4.91 1.73-1.81-.3-2.82-2.31-3.35-4.02-.21-.69-.4-1.39-.46-2.11-.12-1.48.44-2.42 1.36-3.52 1.34-1.59 2.75-3.13 4.16-4.66 5.57-6.01 11.53-11.68 17.85-16.9 6.22-5.14 12.74-9.91 19.19-14.76 5.11-3.85 10.24-7.67 15.37-11.5 2.72-2.02 5.43-4.05 8.15-6.07 1.09-.8 2.36-1.53 3.59-2.3-6.85-.19-13.69-.47-20.53-.7-3.95-.14-7.96-.11-11.91-.39-2.01-.15-4.02-.48-5.91-1.1-1.38-.45-3.37-.68-4.57-1.56-2.42-1.77-3.6-8.63-.18-10.07 1.29-.54 3.62.4 5.03.51 2.12.17 4.24.05 6.35-.11 4.12-.32 8.21-.46 12.35-.53 8.53-.14 17.07-.13 25.59.3 6.15.31 13.77 1.82 15.9 7.6.71 1.94.99 4.56-.6 6.23-1.13 1.18-1.8.53-3.35.5-2.04-.04-3.53.7-3.89.9-6.92 3.84-13.4 8.7-19.88 13.35-15.05 10.8-29.41 22.47-44.02 33.95-.19.15-.06.48.17.42 9.61-2.37 19.24-4.96 29.03-6.35 5.23-.74 10.53-1.01 15.77-.3 2.62.36 5.15 1.01 7.69 1.79.57.2 2.52.54 2.49 1.69zM856.16 67.3c-1.84.14-2.55 1.69-3.59 2.79-1.55 1.65-3.56 3-5.31 4.47-16.49 13.87-30.61 29.84-45.69 44.93.66-4.81 1.19-9.55 2.68-14.17 1.54-4.74 3.74-9.19 5.71-13.76 1.75-4.06 3.31-8.23 5.81-11.93 2.86-4.23 7.13-7.09 11.13-10.36 7.7-6.29 14.9-13.07 21.82-20.06 6.9-6.97 13.95-13.98 19.87-21.69 2.77-3.61 5.07-7.5 7.07-11.53 1.23-2.46 2.27-5.55-.46-7.15-3.82-2.24-10.52-2.04-14.37-.9-4.46 1.33-8.45 3.58-12.06 6.17-7.55 5.42-14.27 11.92-20 18.92-3 3.66-5.77 7.47-8.48 11.32-.88 1.24-1.91 3.83-3.89 3.76-1.91-.07-4.38-2.1-6.23-2.76-10.12-3.64-22.07-2.65-32.16-1.28-10.47 1.42-20.64 3.72-30.87 5.91 1.98-2.86 3.94-5.71 6.05-8.49 1.65-2.18 5.52-4.61 5.54-7.47.02-2.45-2.29-6.12-5.27-6.41-3.27-.31-4.98 3.41-6.49 5.34-4.51 5.73-8.66 11.69-13.13 17.44-3.88 4.99-9.5 5.03-15.78 5.49-.79.06-1.2.86-.73 1.49.88 1.18 2.14 2.02 3.77 2.34 1.01.2 4.87.05 5.82.05-.19 0-.45.57-.53.69-6.72 10.21-13.61 20.3-19.85 30.84-3.13 5.29-6.39 10.57-9.01 16.14-2.06 4.39-3.47 9.49-.19 14.12 2.08 2.94 7.42 3.55 9.89 1.43 3.95-3.39 7.37-7.52 10.96-11.23 7.12-7.32 14.17-14.7 21.26-22.05-.24.24.47 2.59.56 3 .25 1.15.55 2.28 1.04 3.35 1.65 3.57 5.51 5.09 8.95 2.85 5.58-3.63 10.63-8.29 15.86-12.33-2.45 5.29-5.69 10.61-7.45 16.16-.28.87-.28 1.82-.12 2.71.47 2.64 2.32 5.13 4.72 6.33 3.07 1.54 7.12.84 10.12-.51 2.71-1.22 5.03-3.06 7.45-4.75 2.69-1.89 11.16-8.82 11.26-9.2-2.31 8.38-4.46 20.58 3.38 27.66 7.34 6.63 14.38-1.03 18.8-5.67 6.48-6.79 11.99-14.16 17.46-21.65 2.87-3.93 5.89-7.78 8.95-11.58 2.12-2.64 5.12-5.66 8.15-7.95-1.26 2.31-3.41 5.43-4.68 7.97-1.13 2.27-2.52 4.39-3.46 6.75-.9 2.24-1.92 4.74-.27 7.07.93 1.31 3.33 3.41 4.9 2.17 1.29-1.02 1.92-3.8 2.54-5.19.98-2.19 1.84-4.43 2.69-6.67 1.73-4.63 3.33-9.32 5.46-13.8.58-1.22 1.2-2.43 1.87-3.61.53-.93 1.68-2.09 1.92-3.1.33-1.59-1.59-2.54-3.39-2.41zm-19.68-28.14c5.92-8.18 13-16.72 21.63-22.68 1.99-1.37 4.51-3.15 7.09-3.53 1.52-.22 5.95.2 6.37 2.01.38 1.64-1.91 4.45-2.78 5.83-1.06 1.7-2.13 3.4-3.32 5.02-6.72 9.16-14.6 17.61-22.95 25.55-3.98 3.79-8.09 7.46-12.28 11.06-2.1 1.8-4.21 3.58-6.34 5.35-1.11.92-2.2 1.86-3.35 2.74-.83.64-2 1.84-2.77 1.16s11.32-22.33 18.7-32.51zm-39.65 45.3c-1.05 2.6-1.32 4.2-2.57 6.02-1.55 2.25-3.59 4.11-5.7 5.82-.1.08-.19.16-.29.23-1.6 1.3-3.16 2.64-4.78 3.92-1.29 1.07-2.72 1.98-4.04 3.01-.23.18-.47.35-.7.52-.15.1-.32.22-.5.34-5.51 3.88-3.64-2.78-3.49-3.28.9-2.76 1.56-5.67 2.99-8.64 2.04-4.22 4.03-8.46 5.96-12.72 1.3-2.87 4.73-7.51 3.14-10.84-.64-1.35-2.57-3.41-4.15-3.89-1.72-.53-2.5.46-3.58 1.44-3.27 2.97-6.66 5.84-10.03 8.72-3.29 2.8-6.62 5.57-10.03 8.26a210.9 210.9 0 01-5.58 4.27c-1.42 1.05-3.07 2.74-4.9 3.14-.92-4.63 3.87-8.59 6.83-11.37 3.63-3.41 7.49-6.55 11.38-9.64 3.44-2.74 7.25-5.11 11.48-6.83 1.11-.45 3.72-1 3.99-2.28.33-1.6-1.83-2.44-3.22-2.46-2.18-.02-4.34 1.19-6.19 1.98-2.24.97-4.43 2.03-6.57 3.17-4.4 2.34-8.57 5.01-12.57 7.85-8.47 6.01-16.25 12.76-23.69 19.77-3.69 3.48-7.3 7.04-10.87 10.63-1.85 1.85-3.68 3.71-5.52 5.57-1.43 1.45-2.95 3.81-5.14 4.42 7.7-13.82 15.2-27.6 24.01-40.84 2.14-3.22 4.26-6.47 6.41-9.68 1.45-2.17 3.13-3.55 5.92-4.18 6.84-1.55 14.11-2.42 21.09-3.52 7.98-1.26 16.01-2.53 24.16-3.07 4.03-.26 8.09-.35 12.15-.25 2.99.07 7.27-.29 10.13.81 2.4.93 1.04 2.86.21 4.31-1.67 2.89-3.41 5.75-5.12 8.62-3.91 6.63-7.73 13.49-10.62 20.67z" fill="#fff"></path></svg>
				</div>
				<p class="g_text">
					Introducing Ispirazione Italiana; a range of distinct coffees inspired by the culture and the coffee traditions of Italy’s cities and local life, each with its own distinct tastes and roasting expertise.
				</p>
			  </div>
			  <div class="slideitem" style="background-image: url(img/backgrounds/slide-2_L.jpg);">
						<div class="sgv">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1115.68 173.48" class="injected-svg g_visual" src="img/hero/ispirazione-italiana_L.svg" aria-hidden="true"><path d="M642.48 77.69c-11.55 5.71-23.33 10.91-35.68 14.67-6.15 1.87-12.76 3.99-19.23 4.21-2.07.07-5.05.12-5.45-2.43-.44-2.87.17-6.17 1.03-8.9.78-2.48 2.05-5.66 3.89-7.57.99-1.03 3.82-2.38 6.08-3.16 5.39-1.84 10.5-5.18 15.17-8.51 2.34-1.67 6.08-4.53 8.28-6.39 2.09-1.77 4.54-3.53 6.06-5.86 2.61-4.01.63-8.64-2.82-11.37-3.2-2.53-7.78-3.82-11.78-2.77-5.69 1.49-10.47 6.33-14.87 9.97-4.53 3.74-9.38 7.03-13.8 10.91-4.75 4.18-9.16 8.73-13.4 13.42-8.1 8.97-15.29 19.41-24.79 26.98 1.93-6.28 4.16-12.36 6.51-18.49 2.46-6.41 4.05-13.07 6.41-19.52 1.05-2.88 2.37-6.04 2.69-9.11.28-2.7-2.87-4.1-5.2-2.89-2.8 1.45-5.43 4.31-7.89 6.3l-8.21 6.65c1.92-4.46 4.28-8.89 5.11-13.71 1.18-6.86 2.22-15.42-6.16-17.86-3.99-1.16-8.46-.36-12.44.46-3.98.82-9.77 1.75-12.66 4.94-2.72 3.01-1.11 6.87-2.48 10.25-1.96 4.84-4.81 9.48-7.6 13.87-5.42 8.54-11.23 17.1-19.17 23.49-3.76 3.03-7.4 6.23-11.46 8.86-1.92 1.25-3.97 2.44-6.11 3.27-2.05.8-4.44 2-6.7 1.76-.62-.07-1.24-.25-1.71-.65-.77-.65-1.04-1.73-1.04-2.74-.02-5.89 2.14-11.51 5.27-16.42 3.15-4.94 5.78-8.66 9.55-13.1 3.24-3.82 7.99-8.15 11.89-11.37 3.53-2.92 7.84-6.23 12.26-7.6 1.92-.6 11.8-1.74 7.17-5.06-4.43-3.18-10-.49-14.37 1.33-3.15 1.31-6.24 2.77-9.17 4.52-8.93 5.32-17.72 12.21-24.25 20.55-5.75 7.34-10.4 16.67-9.33 26.22.49 4.41 2.14 9.76 5.03 13.22 2.87 3.43 9.03 2.89 13.07 2.64 9.15-.58 17.63-6.73 24.63-12.3 6.96-5.54 13.02-12.17 17.71-19.73 5.55-8.94 10.17-18.72 11.3-29.3.27-2.55.34-5.13.2-7.69-.15-2.89.59-2.98 3.29-3.77 4.89-1.44 10.38-2.99 15.53-2.82 4.91.16 1.97 6.58.81 9.32-2.08 4.96-4.59 9.74-7.14 14.47-2.63 4.88-5.35 9.71-7.92 14.63-2.16 4.15-5.37 9-5.51 13.81-.08 2.69 2.66 10.43 5.68 5.98 2.84-4.18 5.15-8.69 8.4-12.6 3.07-3.7 6.64-6.94 9.78-10.56 1.77-2.04 3.33-4.2 5.29-6.07 1.88-1.78 3.91-3.4 5.79-5.18-.16 2.04-1.54 3.8-2.5 5.6-1.26 2.37-2.48 4.75-3.68 7.14-2.32 4.64-4.51 9.34-6.59 14.1-2.79 6.39-8.02 14.34-6.16 21.59.94 3.66 3.55 7.4 7.08 8.94 3.38 1.47 4.17-1.19 5.9-3.51 2.66-3.57 5.76-6.87 8.53-10.37 2.84-3.58 5.65-7.2 8.56-10.72 5.35-6.48 10.9-12.82 16.37-19.2-1.56 6.26-3.42 12.71-1.94 19.18 1.59 6.95 7.12 12.56 14.44 12.8 3.67.12 7.29-.89 10.75-2.15 14.41-5.27 28.49-11.41 41.2-20.15 1.57-1.08 3.07-2.28 4.66-3.33.79-.53 2.44-1.04 2.81-2 1.05-2.7-4.18.49-4.97.88zm-53.38-7.48c3.22-6.01 7.17-11.76 11.92-16.63 2.49-2.54 5.13-4.9 8.09-6.88 2.11-1.41 6.04-4.28 8.65-2.46 5.76 4.03-5.42 11.95-8.35 14.16-5.68 4.29-13.72 8.25-19.85 12.24-.27.18-.62-.13-.46-.43zm-147.01 2.05c-2.97 5.51-5.64 11.19-8.86 16.57-.81 1.35-1.66 2.66-2.52 3.96-.79 1.2-1.57 2.39-2.55 2.63s-3.57-1.95-4.81-3.76c-.83-1.21-.02-3.82.55-5.13 3.42-7.81 7.44-15.37 12.09-22.52 1.62-2.49 3.29-5.8 5.51-7.8 1.89-1.69 4.33-.76 5.95.9 1.97 2.01 1.32 4.19-.05 6.28-1.89 2.89-3.67 5.83-5.31 8.87zm-197.33-6.31c-4.21 9.37-12.51 17.06-20.93 22.91-8.93 6.21-18.95 12-29.32 15.84-5.23 1.94-11.08 4.35-16.81 4.58-4.7.19-7.85 2.61-9.63 6.69-4.86 11.15-11.34 21.25-16.79 32.14-5.32 10.62-4.38 11.61-11.76 20.95-1.24 1.57-2.07 4.2-4.45 4.4-2.73.23-3.97-2.16-4.43-4.36-1.21-5.66 2-11.2 4.1-16.12 4.97-11.61 5.88-13.89 10.99-25.44 9.93-22.44 20.38-44.5 32.83-65.67 2.51-4.28 4.99-10.05 9.09-13.13 4.64-3.49 6.56 1.87 7 5.9.59 5.46-2.84 9.76-5.23 14.36-2.87 5.52-5.68 11.07-8.46 16.63-1.32 2.63-2.64 5.27-3.95 7.91-1.23 2.46-1.67 5.85-3.83 9.47 11.2-3.09 24.1-9.56 33.92-15.34 9.58-5.64 19.25-12.2 25.21-21.54 2.3-3.6 4.83-8.04 4.31-12.54-.46-4.01-3.61-6.87-7.12-8.8-10.39-5.7-22.86-7.61-34.39-7.42-6.71.11-13.48.81-19.96 2.14-2.14.44-4.23 1.04-6.29 1.73-1.25.42-2.62.86-3.75 1.5-.94.53-1.45 1.41-2.3 2.02-3.06 2.21-9.68-3.1-7.37-6.26 1.04-1.42 4.14-2.43 5.67-3.16 2.21-1.05 4.53-1.89 6.92-2.53 11.84-3.17 24.95-3.77 37.37-1.91 10.72 1.61 22.63 5.02 31.69 11.31 8.07 5.62 11.55 15.11 7.67 23.74zM966.4 84.12c-3.12 9.26-6.28 18.5-9.91 27.58-.5 1.26-.9 4.07-2.93 3.45-1.8-.56-2.99-3.58-3.28-5.04-.85-4.17 1.43-8.63 2.74-12.39 1.8-5.19 3.81-10.3 5.79-15.42 1.71-4.43 3.23-8.95 5.75-13.02.05-.07-.07-.15-.13-.09-16.96 15.86-25.3 34.82-38.55 52.84-1.14 1.55-3.73 1.54-5.09-.05-2.19-2.57-4.16-5.28-4.7-8.47-.87-5.13 3.93-9.32 5.93-13.72 2.33-5.14 14.9-28.77 15.13-29.41 1.67-4.7-10.89 8.12-11.69 8.98-7.65 8.16-16.61 16.36-22.86 25.55-3.6 5.29-13.93 13.12-17.19 4.17-1.72-4.73 1.39-9.45 3.37-13.77 2.15-4.7 4.46-9.95 5.46-15.03-5.16 5.73-10.16 11.64-15.46 17.25-3.98 4.21-9.48 11.12-16.86 8.51-6.34-2.24-4.16-9.88-2.97-14.08 1.75-6.16 5.1-11.73 9.15-16.75 7.82-9.69 19.57-20.31 33.63-21.97 2.89-.34 6.03-.7 8.4 1.44.69.62 1.79 1.78 1.85 2.65.17 2.19-1.69 1.52-3.15.93-6.75-2.73-12.48 2.29-17.07 5.78-11.42 8.69-19.5 20.26-24.49 33.12 7.12-5.55 13.44-11.69 19.94-17.8 3.07-2.89 6.24-5.7 9.53-8.39 1.77-1.45 3.58-2.86 5.43-4.23 1.51-1.12 2.48-2.37 3.84-3.58 2.6-2.32 5.83-.95 6.28 2.3.49 3.59-1.15 6.88-2.89 9.84-4.07 6.95-8.12 14.27-10.25 22.04 12.34-13.72 24.05-27.75 37.67-40.47 1.71-1.59 3.12-2.94 4.16-4.99.66-1.31 1.34-2.57 3.18-2.65 1.63-.07 3.19 1.15 4.64 1.76 1.51.63 3.15 1.01 4.35 2.17 3.26 3.17-3.8 10.28-4.89 13.47-1.48 4.32-2.78 8.7-4.65 12.87 3.43-3.4 10.28-9.17 13.73-12.55 2.79-2.74 6.83-7.19 11.79-4.99 4.07 1.81 2.96 5.38 1.96 8.3-1.57 4.61-3.13 9.24-4.69 13.86zM107.34 73.93c-7.22 14.44-13.95 29.79-23.56 42.81-6.82 9.26-15.45 18.44-26.48 22.51-10.75 3.97-22.61 4.15-33.5.66-8.54-2.74-15.9-7.78-22.61-13.62-.4-.35-.81-.72-1.06-1.19-.12-.23-.2-.51-.08-.74.22-.44.88-.32 1.34-.12 4.75 2.05 9.07 4.9 13.94 6.66 4.91 1.78 10.3 1.98 15.42 1.17 10.86-1.7 21.3-6.71 29.7-13.75 3.87-3.24 7.33-6.97 10.38-10.98 7.83-10.31 14.3-21.73 20.59-33.02 6.72-12.07 13.1-24.32 19.7-36.45 2.97-5.46 5.94-11.28 8.99-16.84 1.57-2.86 3.19-5.69 4.9-8.47.73-1.18 4.71-5.29 1.82-5.86-1.99-.39-5.4 2.69-7.02 3.7a101.1 101.1 0 00-7.87 5.45c-9.79 7.5-18.04 16.83-24.8 27.11-3.33 5.05-6.44 10.56-7.06 16.7-.14 1.42-.16 2.87.14 4.27.27 1.25 1.3 2.5 1.32 3.77.05 2.62-3.67 3.55-5.31 2.01-4.23-3.96-4.77-10.35-3.17-15.57C77.2 40.67 86.55 29.37 96.6 19.82c4.84-4.6 9.88-9.08 15.56-12.6C117.61 3.85 125.1.03 131.68 0c13.97-.05-.15 23.06-2.48 28.17-7.01 15.38-14.3 30.64-21.86 45.76zm1008.21-2.15c-.74 1.71-4.37-.5-5.8-.74-5.4-.91-11.13.56-16.13 1.82-10.16 2.57-19.57 6.76-28.75 11.18-9.13 4.4-18.05 9.15-26.41 14.61-8.49 5.54-15.96 11.95-23.32 18.63-3.44 3.12-6.76 7.56-12.25 7.53-4.53-.02-9.9-2.09-12.06-6.1-2.22-4.12.61-8.5 1.98-12.2 1.97-5.33 3.58-10.8 5.97-15.97-5.11 5.32-9.91 11.18-15.84 15.77-3.89 3.02-11.03 8.16-15.46 1.93-3.6-5.07-1.75-9.83 1.03-14.33 3.33-5.4 7.83-10.35 12.4-14.89 8.76-8.7 20.2-18 33.34-21.02 2.33-.54 6.02-1.59 8.16.35 3.26 2.94-3.24 2.42-4.61 2.81-2.46.7-5.07 1.81-7.09 3.14-2.88 1.9-6.19 4.26-8.88 6.37-5.86 4.58-10.06 10.01-14.87 15.51-4.82 5.51-8.42 11.54-12.37 17.57-.11.18.14.39.32.27 7.85-5.31 14.09-12.2 20.75-18.63 2.22-2.14 4.38-4.33 6.52-6.54 3.06-3.16 6.75-6.21 8.86-10.06.64-1.16 1-3.16 2.26-3.92 1.24-.75 2.99-.12 4.38.24 2.37.62 4.34 1.38 4.23 3.87-.16 3.79-2.55 7.79-3.94 11.25-1.62 3.99-3.3 7.96-4.89 11.96-3.59 9.01-6.08 18.23-8.48 27.62 7.32-6.63 14.43-13.39 22.58-19.22 8.14-5.83 16.92-10.93 25.97-15.61 9.44-4.88 19.13-9.57 29.33-13.2 9.65-3.44 20.08-6.21 30.74-2.33 1.11.41 2.95.92 2.33 2.33zM302.5 69.6c-6.27 8.95-11.94 18.39-17.2 27.97-2.55 4.64-4.65 9.35-6.46 14.32-.69 1.9-1.2 3.67-2.93 4.89-1.15.81-3.45 2.23-4.85 1.6-1.23-.56-2.49-2.73-3.35-3.75-1.5-1.76-2.32-3.77-2.31-6.09.03-4.44 2.29-9.16 3.82-13.25 4.39-11.74 9.87-23.03 14.75-34.57-6.66 7.2-13.34 14.28-19.56 21.87-6.11 7.46-11.76 15.76-19.29 21.88-1.6 1.3-3.01 2.04-4.87.75-1.5-1.04-2.68-2.76-3.09-4.54-.98-4.24 2-9.39 3.76-13.08 2.21-4.64 4.51-9.24 6.72-13.88 1.47-3.09 2.87-8.87 7.33-6.44 4.94 2.7.43 8.52-1.16 12.13-2.39 5.43-4.68 10.9-7.04 16.34 6.82-6.86 12.24-14.91 18.61-22.17 6.77-7.72 13.95-15.16 21.06-22.57 1.49-1.55 2.13-2.75 3.04-4.62.8-1.64 2.14-2.06 3.94-1.76 2.34.39 5.68 1.33 7.38 3.1 1.5 1.56 1.37 3.63.91 5.6-.97 4.16-2.94 8.23-4.42 12.23 4.55-4.51 8.88-9.05 13.96-12.97 4.14-3.2 12.34-8.07 15.55-5.58 3.22 2.48-.32 7.38-1.4 8.51-2.35 2.45-7.24 6.79-8.49 5.63s1.34-2.81.22-4.5c-1.56-2.36-3.9.71-5.24 1.94-3.57 3.29-6.62 7.07-9.39 11.01zm421.17-38.1c-10.61 17.04-23.26 32.86-33.94 49.85-6.01 9.55-11.35 19.48-17.23 29.11-1.5 2.45-3.11 4.85-4.99 7.06-1.25 1.48-2.52 4.25-4.53 5.17-4.29 1.95-8.02-4.52-8.28-7.52.71-4.85 3.3-9.31 5.46-13.63 2.25-4.52 4.69-8.95 7.28-13.31 5.59-9.43 11.83-18.5 18.16-27.49 5.67-8.05 11.15-16.21 16.47-24.46 5.07-7.86 10.3-15.76 14.49-24.07.9-1.78 1.86-3.49 1.7-5.55-.16-2 .34-2.73 2.79-2.46 3.41.38 7.1 2.37 8.96 5.1 5.08 7.42-2.71 16.36-6.34 22.2zm-592.12 86.52c8.89-.91 13.9-11.84 15.57-19.44.4-1.81.72-4.09 2.44-5.17 1.32-.82 3.05-.78 4.53-.96 1.58-.2 3.12-.5 4.58-1.08.47-.19.48-.86.01-1.06-1.45-.61-2.83-.82-4.38-.87-1.23-.04-3.94-.13-4.34-.71-.57-.83.18-3.44.35-4.76.4-3.16 1.05-6.3 1.94-9.36 1.08-3.74 2.08-7.85 4.22-11.15 1.2-1.85 2.77-3.12 4.24-4.71 1.43-1.54.63-3.73-1.71-3.98-1.55-.17-5.63 3.62-6.01 3.91-.8.63-1.6 1.25-2.4 1.88-1.6 1.26-3.19 2.53-4.77 3.81-9.61 7.77-19.01 15.88-27.85 24.52a310.903 310.903 0 00-18.99 20.27c-1.56 1.82-3.1 3.67-4.62 5.53-1.24 1.52-3.09 3.21-3.78 5.07-.69 1.87.21 3.78 2.05 4.55.92.39 3.32.8 4.23.34 1.34-.68.88-2.28 1.28-3.48.66-1.93 2.18-3.87 3.35-5.52 1.34-1.88 2.8-3.67 4.32-5.4 1.03-1.18 2.49-3.47 4.19-3.75 2.43-.41 4 3.05 5.52 4.35 2.41 2.06 4.94 4.09 7.81 5.49 2.53 1.23 5.4 1.97 8.22 1.68zm-10.89-24.45c3.78-3.62 7.57-7.24 11.35-10.86 1.94-1.86 3.88-3.71 5.82-5.57.71-.68 2.82-3.19 4.32-4.2.48-.32 1.1.13.91.68-1 2.95-1.99 5.9-2.99 8.86-.66 1.95-1.33 6.49-3.03 7.65-.91.62-2.98.74-4.03 1l-4.14 1.02c-2.64.65-5.28 1.29-7.92 1.94-.31.07-.51-.31-.29-.52zm-3.52 12.35c-1.85-1.36-3.88-2.6-1.22-4.09 4.61-2.58 9.89-4.28 14.93-5.76 1.2-.35 5.1-1.91 5.31-.55.14.92-1.13 2.99-1.51 3.81-.62 1.32-1.27 2.55-1.94 3.85-2.03 3.9-4.68 5.26-9.12 5.24-2.41-.05-4.56-1.1-6.45-2.5zM351.8 88.87c2.28-8.14 5.34-16.03 8.69-23.78 1.68-3.88 3.43-7.73 5.21-11.57 1.29-2.78 4.48-6.45-1.03-8.01-3.82-1.08-5.76.64-7.2 3.05-1.02 1.72-1.56 1.8-3.87 1.51-2.02-.25-3.86.1-5.56.79-7.65 3.1-14.46 7.93-20.12 13.1-5.82 5.31-11.46 11.15-16.06 17.32-3.65 4.89-8.95 13.56-4.2 19.96.9 1.21 2.5 3.04 4.28 2.84 1.36-.15 2.36-1.56 3.37-2.19 3.67-2.27 6.72-5.08 9.86-7.87 6.71-5.95 13.17-12.1 19.21-18.57-1.17 4.62-2.78 9.11-3.79 13.78-.83 3.82-.66 3.29 1.34 7.3.59 1.19 2.68 4.48 4.44 4.41 1.61-.06 2.81-3.26 3.22-4.28 1.65-4.08 1.02-3.55 2.21-7.79zm-40.59 8.35c-.11.08-.3-.04-.25-.16 7.5-18.08 21.7-31.23 39.87-41.46.5-.28 1.2.29.9.73-10.63 15.73-24.4 29.16-40.52 40.89zm140.06-52.37c-.47-1.33 1.57-2.84 2.3-3.69 2.03-2.35 4.38-5.56 7.91-4.82 1.95.41 4.87 3.09 3.94 5.37-.63 1.54-3.37 1.74-4.7 2.15-1.89.58-3.75 1.25-5.64 1.85-1.09.16-3.29.6-3.81-.86zm417.87 11.21c-.51 1.41-3.26 1.59-4.56 1.97-1.86.53-3.68 1.14-5.53 1.69-1.1.14-3.28.54-3.92-.79-.58-1.21 1.35-2.6 2.02-3.37 1.85-2.15 3.97-5.09 7.59-4.4 2 .37 5.15 2.82 4.4 4.9zm-614.63-5.15c-.56-.89.88-2.54 1.29-3.25.61-1.07 1.06-3.15 1.9-3.96 1.04-1.01 4.02.08 6.4 1.23 2.04.99 2.32 3.83.47 5.14-.68.48-1.4.84-2.01 1.04-1.23.41-2.36.12-3.63.12-1.02-.01-3.75.73-4.42-.32zm168.72 46.37c-.02 1.13-1.88 1.43-2.65 1.54-3.48.5-12.57 2.14-16.94 3.06-1.83.43-3.69.76-5.53 1.14-8.27 1.68-16.59 3.29-24.7 5.63a92.07 92.07 0 00-11.39 4.11c-1.78.78-3.56 1.59-5.27 2.5-1.47.77-3.15 2.03-4.91 1.73-1.81-.3-2.82-2.31-3.35-4.02-.21-.69-.4-1.39-.46-2.11-.12-1.48.44-2.42 1.36-3.52 1.34-1.59 2.75-3.13 4.16-4.66 5.57-6.01 11.53-11.68 17.85-16.9 6.22-5.14 12.74-9.91 19.19-14.76 5.11-3.85 10.24-7.67 15.37-11.5 2.72-2.02 5.43-4.05 8.15-6.07 1.09-.8 2.36-1.53 3.59-2.3-6.85-.19-13.69-.47-20.53-.7-3.95-.14-7.96-.11-11.91-.39-2.01-.15-4.02-.48-5.91-1.1-1.38-.45-3.37-.68-4.57-1.56-2.42-1.77-3.6-8.63-.18-10.07 1.29-.54 3.62.4 5.03.51 2.12.17 4.24.05 6.35-.11 4.12-.32 8.21-.46 12.35-.53 8.53-.14 17.07-.13 25.59.3 6.15.31 13.77 1.82 15.9 7.6.71 1.94.99 4.56-.6 6.23-1.13 1.18-1.8.53-3.35.5-2.04-.04-3.53.7-3.89.9-6.92 3.84-13.4 8.7-19.88 13.35-15.05 10.8-29.41 22.47-44.02 33.95-.19.15-.06.48.17.42 9.61-2.37 19.24-4.96 29.03-6.35 5.23-.74 10.53-1.01 15.77-.3 2.62.36 5.15 1.01 7.69 1.79.57.2 2.52.54 2.49 1.69zM856.16 67.3c-1.84.14-2.55 1.69-3.59 2.79-1.55 1.65-3.56 3-5.31 4.47-16.49 13.87-30.61 29.84-45.69 44.93.66-4.81 1.19-9.55 2.68-14.17 1.54-4.74 3.74-9.19 5.71-13.76 1.75-4.06 3.31-8.23 5.81-11.93 2.86-4.23 7.13-7.09 11.13-10.36 7.7-6.29 14.9-13.07 21.82-20.06 6.9-6.97 13.95-13.98 19.87-21.69 2.77-3.61 5.07-7.5 7.07-11.53 1.23-2.46 2.27-5.55-.46-7.15-3.82-2.24-10.52-2.04-14.37-.9-4.46 1.33-8.45 3.58-12.06 6.17-7.55 5.42-14.27 11.92-20 18.92-3 3.66-5.77 7.47-8.48 11.32-.88 1.24-1.91 3.83-3.89 3.76-1.91-.07-4.38-2.1-6.23-2.76-10.12-3.64-22.07-2.65-32.16-1.28-10.47 1.42-20.64 3.72-30.87 5.91 1.98-2.86 3.94-5.71 6.05-8.49 1.65-2.18 5.52-4.61 5.54-7.47.02-2.45-2.29-6.12-5.27-6.41-3.27-.31-4.98 3.41-6.49 5.34-4.51 5.73-8.66 11.69-13.13 17.44-3.88 4.99-9.5 5.03-15.78 5.49-.79.06-1.2.86-.73 1.49.88 1.18 2.14 2.02 3.77 2.34 1.01.2 4.87.05 5.82.05-.19 0-.45.57-.53.69-6.72 10.21-13.61 20.3-19.85 30.84-3.13 5.29-6.39 10.57-9.01 16.14-2.06 4.39-3.47 9.49-.19 14.12 2.08 2.94 7.42 3.55 9.89 1.43 3.95-3.39 7.37-7.52 10.96-11.23 7.12-7.32 14.17-14.7 21.26-22.05-.24.24.47 2.59.56 3 .25 1.15.55 2.28 1.04 3.35 1.65 3.57 5.51 5.09 8.95 2.85 5.58-3.63 10.63-8.29 15.86-12.33-2.45 5.29-5.69 10.61-7.45 16.16-.28.87-.28 1.82-.12 2.71.47 2.64 2.32 5.13 4.72 6.33 3.07 1.54 7.12.84 10.12-.51 2.71-1.22 5.03-3.06 7.45-4.75 2.69-1.89 11.16-8.82 11.26-9.2-2.31 8.38-4.46 20.58 3.38 27.66 7.34 6.63 14.38-1.03 18.8-5.67 6.48-6.79 11.99-14.16 17.46-21.65 2.87-3.93 5.89-7.78 8.95-11.58 2.12-2.64 5.12-5.66 8.15-7.95-1.26 2.31-3.41 5.43-4.68 7.97-1.13 2.27-2.52 4.39-3.46 6.75-.9 2.24-1.92 4.74-.27 7.07.93 1.31 3.33 3.41 4.9 2.17 1.29-1.02 1.92-3.8 2.54-5.19.98-2.19 1.84-4.43 2.69-6.67 1.73-4.63 3.33-9.32 5.46-13.8.58-1.22 1.2-2.43 1.87-3.61.53-.93 1.68-2.09 1.92-3.1.33-1.59-1.59-2.54-3.39-2.41zm-19.68-28.14c5.92-8.18 13-16.72 21.63-22.68 1.99-1.37 4.51-3.15 7.09-3.53 1.52-.22 5.95.2 6.37 2.01.38 1.64-1.91 4.45-2.78 5.83-1.06 1.7-2.13 3.4-3.32 5.02-6.72 9.16-14.6 17.61-22.95 25.55-3.98 3.79-8.09 7.46-12.28 11.06-2.1 1.8-4.21 3.58-6.34 5.35-1.11.92-2.2 1.86-3.35 2.74-.83.64-2 1.84-2.77 1.16s11.32-22.33 18.7-32.51zm-39.65 45.3c-1.05 2.6-1.32 4.2-2.57 6.02-1.55 2.25-3.59 4.11-5.7 5.82-.1.08-.19.16-.29.23-1.6 1.3-3.16 2.64-4.78 3.92-1.29 1.07-2.72 1.98-4.04 3.01-.23.18-.47.35-.7.52-.15.1-.32.22-.5.34-5.51 3.88-3.64-2.78-3.49-3.28.9-2.76 1.56-5.67 2.99-8.64 2.04-4.22 4.03-8.46 5.96-12.72 1.3-2.87 4.73-7.51 3.14-10.84-.64-1.35-2.57-3.41-4.15-3.89-1.72-.53-2.5.46-3.58 1.44-3.27 2.97-6.66 5.84-10.03 8.72-3.29 2.8-6.62 5.57-10.03 8.26a210.9 210.9 0 01-5.58 4.27c-1.42 1.05-3.07 2.74-4.9 3.14-.92-4.63 3.87-8.59 6.83-11.37 3.63-3.41 7.49-6.55 11.38-9.64 3.44-2.74 7.25-5.11 11.48-6.83 1.11-.45 3.72-1 3.99-2.28.33-1.6-1.83-2.44-3.22-2.46-2.18-.02-4.34 1.19-6.19 1.98-2.24.97-4.43 2.03-6.57 3.17-4.4 2.34-8.57 5.01-12.57 7.85-8.47 6.01-16.25 12.76-23.69 19.77-3.69 3.48-7.3 7.04-10.87 10.63-1.85 1.85-3.68 3.71-5.52 5.57-1.43 1.45-2.95 3.81-5.14 4.42 7.7-13.82 15.2-27.6 24.01-40.84 2.14-3.22 4.26-6.47 6.41-9.68 1.45-2.17 3.13-3.55 5.92-4.18 6.84-1.55 14.11-2.42 21.09-3.52 7.98-1.26 16.01-2.53 24.16-3.07 4.03-.26 8.09-.35 12.15-.25 2.99.07 7.27-.29 10.13.81 2.4.93 1.04 2.86.21 4.31-1.67 2.89-3.41 5.75-5.12 8.62-3.91 6.63-7.73 13.49-10.62 20.67z" fill="#fff"></path></svg>
				</div>
				<p class="g_text">
					We travelled high and low through Italy, to bring you this range that embodies each city's own unique approach to roasting coffees.
				</p>
			  </div>

			  <div class="slideitem" style="background-image: url(img/backgrounds/slide-3_L.jpg);">		
				<div class="sgv">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1115.68 173.48" class="injected-svg g_visual" src="img/hero/ispirazione-italiana_L.svg" aria-hidden="true"><path d="M642.48 77.69c-11.55 5.71-23.33 10.91-35.68 14.67-6.15 1.87-12.76 3.99-19.23 4.21-2.07.07-5.05.12-5.45-2.43-.44-2.87.17-6.17 1.03-8.9.78-2.48 2.05-5.66 3.89-7.57.99-1.03 3.82-2.38 6.08-3.16 5.39-1.84 10.5-5.18 15.17-8.51 2.34-1.67 6.08-4.53 8.28-6.39 2.09-1.77 4.54-3.53 6.06-5.86 2.61-4.01.63-8.64-2.82-11.37-3.2-2.53-7.78-3.82-11.78-2.77-5.69 1.49-10.47 6.33-14.87 9.97-4.53 3.74-9.38 7.03-13.8 10.91-4.75 4.18-9.16 8.73-13.4 13.42-8.1 8.97-15.29 19.41-24.79 26.98 1.93-6.28 4.16-12.36 6.51-18.49 2.46-6.41 4.05-13.07 6.41-19.52 1.05-2.88 2.37-6.04 2.69-9.11.28-2.7-2.87-4.1-5.2-2.89-2.8 1.45-5.43 4.31-7.89 6.3l-8.21 6.65c1.92-4.46 4.28-8.89 5.11-13.71 1.18-6.86 2.22-15.42-6.16-17.86-3.99-1.16-8.46-.36-12.44.46-3.98.82-9.77 1.75-12.66 4.94-2.72 3.01-1.11 6.87-2.48 10.25-1.96 4.84-4.81 9.48-7.6 13.87-5.42 8.54-11.23 17.1-19.17 23.49-3.76 3.03-7.4 6.23-11.46 8.86-1.92 1.25-3.97 2.44-6.11 3.27-2.05.8-4.44 2-6.7 1.76-.62-.07-1.24-.25-1.71-.65-.77-.65-1.04-1.73-1.04-2.74-.02-5.89 2.14-11.51 5.27-16.42 3.15-4.94 5.78-8.66 9.55-13.1 3.24-3.82 7.99-8.15 11.89-11.37 3.53-2.92 7.84-6.23 12.26-7.6 1.92-.6 11.8-1.74 7.17-5.06-4.43-3.18-10-.49-14.37 1.33-3.15 1.31-6.24 2.77-9.17 4.52-8.93 5.32-17.72 12.21-24.25 20.55-5.75 7.34-10.4 16.67-9.33 26.22.49 4.41 2.14 9.76 5.03 13.22 2.87 3.43 9.03 2.89 13.07 2.64 9.15-.58 17.63-6.73 24.63-12.3 6.96-5.54 13.02-12.17 17.71-19.73 5.55-8.94 10.17-18.72 11.3-29.3.27-2.55.34-5.13.2-7.69-.15-2.89.59-2.98 3.29-3.77 4.89-1.44 10.38-2.99 15.53-2.82 4.91.16 1.97 6.58.81 9.32-2.08 4.96-4.59 9.74-7.14 14.47-2.63 4.88-5.35 9.71-7.92 14.63-2.16 4.15-5.37 9-5.51 13.81-.08 2.69 2.66 10.43 5.68 5.98 2.84-4.18 5.15-8.69 8.4-12.6 3.07-3.7 6.64-6.94 9.78-10.56 1.77-2.04 3.33-4.2 5.29-6.07 1.88-1.78 3.91-3.4 5.79-5.18-.16 2.04-1.54 3.8-2.5 5.6-1.26 2.37-2.48 4.75-3.68 7.14-2.32 4.64-4.51 9.34-6.59 14.1-2.79 6.39-8.02 14.34-6.16 21.59.94 3.66 3.55 7.4 7.08 8.94 3.38 1.47 4.17-1.19 5.9-3.51 2.66-3.57 5.76-6.87 8.53-10.37 2.84-3.58 5.65-7.2 8.56-10.72 5.35-6.48 10.9-12.82 16.37-19.2-1.56 6.26-3.42 12.71-1.94 19.18 1.59 6.95 7.12 12.56 14.44 12.8 3.67.12 7.29-.89 10.75-2.15 14.41-5.27 28.49-11.41 41.2-20.15 1.57-1.08 3.07-2.28 4.66-3.33.79-.53 2.44-1.04 2.81-2 1.05-2.7-4.18.49-4.97.88zm-53.38-7.48c3.22-6.01 7.17-11.76 11.92-16.63 2.49-2.54 5.13-4.9 8.09-6.88 2.11-1.41 6.04-4.28 8.65-2.46 5.76 4.03-5.42 11.95-8.35 14.16-5.68 4.29-13.72 8.25-19.85 12.24-.27.18-.62-.13-.46-.43zm-147.01 2.05c-2.97 5.51-5.64 11.19-8.86 16.57-.81 1.35-1.66 2.66-2.52 3.96-.79 1.2-1.57 2.39-2.55 2.63s-3.57-1.95-4.81-3.76c-.83-1.21-.02-3.82.55-5.13 3.42-7.81 7.44-15.37 12.09-22.52 1.62-2.49 3.29-5.8 5.51-7.8 1.89-1.69 4.33-.76 5.95.9 1.97 2.01 1.32 4.19-.05 6.28-1.89 2.89-3.67 5.83-5.31 8.87zm-197.33-6.31c-4.21 9.37-12.51 17.06-20.93 22.91-8.93 6.21-18.95 12-29.32 15.84-5.23 1.94-11.08 4.35-16.81 4.58-4.7.19-7.85 2.61-9.63 6.69-4.86 11.15-11.34 21.25-16.79 32.14-5.32 10.62-4.38 11.61-11.76 20.95-1.24 1.57-2.07 4.2-4.45 4.4-2.73.23-3.97-2.16-4.43-4.36-1.21-5.66 2-11.2 4.1-16.12 4.97-11.61 5.88-13.89 10.99-25.44 9.93-22.44 20.38-44.5 32.83-65.67 2.51-4.28 4.99-10.05 9.09-13.13 4.64-3.49 6.56 1.87 7 5.9.59 5.46-2.84 9.76-5.23 14.36-2.87 5.52-5.68 11.07-8.46 16.63-1.32 2.63-2.64 5.27-3.95 7.91-1.23 2.46-1.67 5.85-3.83 9.47 11.2-3.09 24.1-9.56 33.92-15.34 9.58-5.64 19.25-12.2 25.21-21.54 2.3-3.6 4.83-8.04 4.31-12.54-.46-4.01-3.61-6.87-7.12-8.8-10.39-5.7-22.86-7.61-34.39-7.42-6.71.11-13.48.81-19.96 2.14-2.14.44-4.23 1.04-6.29 1.73-1.25.42-2.62.86-3.75 1.5-.94.53-1.45 1.41-2.3 2.02-3.06 2.21-9.68-3.1-7.37-6.26 1.04-1.42 4.14-2.43 5.67-3.16 2.21-1.05 4.53-1.89 6.92-2.53 11.84-3.17 24.95-3.77 37.37-1.91 10.72 1.61 22.63 5.02 31.69 11.31 8.07 5.62 11.55 15.11 7.67 23.74zM966.4 84.12c-3.12 9.26-6.28 18.5-9.91 27.58-.5 1.26-.9 4.07-2.93 3.45-1.8-.56-2.99-3.58-3.28-5.04-.85-4.17 1.43-8.63 2.74-12.39 1.8-5.19 3.81-10.3 5.79-15.42 1.71-4.43 3.23-8.95 5.75-13.02.05-.07-.07-.15-.13-.09-16.96 15.86-25.3 34.82-38.55 52.84-1.14 1.55-3.73 1.54-5.09-.05-2.19-2.57-4.16-5.28-4.7-8.47-.87-5.13 3.93-9.32 5.93-13.72 2.33-5.14 14.9-28.77 15.13-29.41 1.67-4.7-10.89 8.12-11.69 8.98-7.65 8.16-16.61 16.36-22.86 25.55-3.6 5.29-13.93 13.12-17.19 4.17-1.72-4.73 1.39-9.45 3.37-13.77 2.15-4.7 4.46-9.95 5.46-15.03-5.16 5.73-10.16 11.64-15.46 17.25-3.98 4.21-9.48 11.12-16.86 8.51-6.34-2.24-4.16-9.88-2.97-14.08 1.75-6.16 5.1-11.73 9.15-16.75 7.82-9.69 19.57-20.31 33.63-21.97 2.89-.34 6.03-.7 8.4 1.44.69.62 1.79 1.78 1.85 2.65.17 2.19-1.69 1.52-3.15.93-6.75-2.73-12.48 2.29-17.07 5.78-11.42 8.69-19.5 20.26-24.49 33.12 7.12-5.55 13.44-11.69 19.94-17.8 3.07-2.89 6.24-5.7 9.53-8.39 1.77-1.45 3.58-2.86 5.43-4.23 1.51-1.12 2.48-2.37 3.84-3.58 2.6-2.32 5.83-.95 6.28 2.3.49 3.59-1.15 6.88-2.89 9.84-4.07 6.95-8.12 14.27-10.25 22.04 12.34-13.72 24.05-27.75 37.67-40.47 1.71-1.59 3.12-2.94 4.16-4.99.66-1.31 1.34-2.57 3.18-2.65 1.63-.07 3.19 1.15 4.64 1.76 1.51.63 3.15 1.01 4.35 2.17 3.26 3.17-3.8 10.28-4.89 13.47-1.48 4.32-2.78 8.7-4.65 12.87 3.43-3.4 10.28-9.17 13.73-12.55 2.79-2.74 6.83-7.19 11.79-4.99 4.07 1.81 2.96 5.38 1.96 8.3-1.57 4.61-3.13 9.24-4.69 13.86zM107.34 73.93c-7.22 14.44-13.95 29.79-23.56 42.81-6.82 9.26-15.45 18.44-26.48 22.51-10.75 3.97-22.61 4.15-33.5.66-8.54-2.74-15.9-7.78-22.61-13.62-.4-.35-.81-.72-1.06-1.19-.12-.23-.2-.51-.08-.74.22-.44.88-.32 1.34-.12 4.75 2.05 9.07 4.9 13.94 6.66 4.91 1.78 10.3 1.98 15.42 1.17 10.86-1.7 21.3-6.71 29.7-13.75 3.87-3.24 7.33-6.97 10.38-10.98 7.83-10.31 14.3-21.73 20.59-33.02 6.72-12.07 13.1-24.32 19.7-36.45 2.97-5.46 5.94-11.28 8.99-16.84 1.57-2.86 3.19-5.69 4.9-8.47.73-1.18 4.71-5.29 1.82-5.86-1.99-.39-5.4 2.69-7.02 3.7a101.1 101.1 0 00-7.87 5.45c-9.79 7.5-18.04 16.83-24.8 27.11-3.33 5.05-6.44 10.56-7.06 16.7-.14 1.42-.16 2.87.14 4.27.27 1.25 1.3 2.5 1.32 3.77.05 2.62-3.67 3.55-5.31 2.01-4.23-3.96-4.77-10.35-3.17-15.57C77.2 40.67 86.55 29.37 96.6 19.82c4.84-4.6 9.88-9.08 15.56-12.6C117.61 3.85 125.1.03 131.68 0c13.97-.05-.15 23.06-2.48 28.17-7.01 15.38-14.3 30.64-21.86 45.76zm1008.21-2.15c-.74 1.71-4.37-.5-5.8-.74-5.4-.91-11.13.56-16.13 1.82-10.16 2.57-19.57 6.76-28.75 11.18-9.13 4.4-18.05 9.15-26.41 14.61-8.49 5.54-15.96 11.95-23.32 18.63-3.44 3.12-6.76 7.56-12.25 7.53-4.53-.02-9.9-2.09-12.06-6.1-2.22-4.12.61-8.5 1.98-12.2 1.97-5.33 3.58-10.8 5.97-15.97-5.11 5.32-9.91 11.18-15.84 15.77-3.89 3.02-11.03 8.16-15.46 1.93-3.6-5.07-1.75-9.83 1.03-14.33 3.33-5.4 7.83-10.35 12.4-14.89 8.76-8.7 20.2-18 33.34-21.02 2.33-.54 6.02-1.59 8.16.35 3.26 2.94-3.24 2.42-4.61 2.81-2.46.7-5.07 1.81-7.09 3.14-2.88 1.9-6.19 4.26-8.88 6.37-5.86 4.58-10.06 10.01-14.87 15.51-4.82 5.51-8.42 11.54-12.37 17.57-.11.18.14.39.32.27 7.85-5.31 14.09-12.2 20.75-18.63 2.22-2.14 4.38-4.33 6.52-6.54 3.06-3.16 6.75-6.21 8.86-10.06.64-1.16 1-3.16 2.26-3.92 1.24-.75 2.99-.12 4.38.24 2.37.62 4.34 1.38 4.23 3.87-.16 3.79-2.55 7.79-3.94 11.25-1.62 3.99-3.3 7.96-4.89 11.96-3.59 9.01-6.08 18.23-8.48 27.62 7.32-6.63 14.43-13.39 22.58-19.22 8.14-5.83 16.92-10.93 25.97-15.61 9.44-4.88 19.13-9.57 29.33-13.2 9.65-3.44 20.08-6.21 30.74-2.33 1.11.41 2.95.92 2.33 2.33zM302.5 69.6c-6.27 8.95-11.94 18.39-17.2 27.97-2.55 4.64-4.65 9.35-6.46 14.32-.69 1.9-1.2 3.67-2.93 4.89-1.15.81-3.45 2.23-4.85 1.6-1.23-.56-2.49-2.73-3.35-3.75-1.5-1.76-2.32-3.77-2.31-6.09.03-4.44 2.29-9.16 3.82-13.25 4.39-11.74 9.87-23.03 14.75-34.57-6.66 7.2-13.34 14.28-19.56 21.87-6.11 7.46-11.76 15.76-19.29 21.88-1.6 1.3-3.01 2.04-4.87.75-1.5-1.04-2.68-2.76-3.09-4.54-.98-4.24 2-9.39 3.76-13.08 2.21-4.64 4.51-9.24 6.72-13.88 1.47-3.09 2.87-8.87 7.33-6.44 4.94 2.7.43 8.52-1.16 12.13-2.39 5.43-4.68 10.9-7.04 16.34 6.82-6.86 12.24-14.91 18.61-22.17 6.77-7.72 13.95-15.16 21.06-22.57 1.49-1.55 2.13-2.75 3.04-4.62.8-1.64 2.14-2.06 3.94-1.76 2.34.39 5.68 1.33 7.38 3.1 1.5 1.56 1.37 3.63.91 5.6-.97 4.16-2.94 8.23-4.42 12.23 4.55-4.51 8.88-9.05 13.96-12.97 4.14-3.2 12.34-8.07 15.55-5.58 3.22 2.48-.32 7.38-1.4 8.51-2.35 2.45-7.24 6.79-8.49 5.63s1.34-2.81.22-4.5c-1.56-2.36-3.9.71-5.24 1.94-3.57 3.29-6.62 7.07-9.39 11.01zm421.17-38.1c-10.61 17.04-23.26 32.86-33.94 49.85-6.01 9.55-11.35 19.48-17.23 29.11-1.5 2.45-3.11 4.85-4.99 7.06-1.25 1.48-2.52 4.25-4.53 5.17-4.29 1.95-8.02-4.52-8.28-7.52.71-4.85 3.3-9.31 5.46-13.63 2.25-4.52 4.69-8.95 7.28-13.31 5.59-9.43 11.83-18.5 18.16-27.49 5.67-8.05 11.15-16.21 16.47-24.46 5.07-7.86 10.3-15.76 14.49-24.07.9-1.78 1.86-3.49 1.7-5.55-.16-2 .34-2.73 2.79-2.46 3.41.38 7.1 2.37 8.96 5.1 5.08 7.42-2.71 16.36-6.34 22.2zm-592.12 86.52c8.89-.91 13.9-11.84 15.57-19.44.4-1.81.72-4.09 2.44-5.17 1.32-.82 3.05-.78 4.53-.96 1.58-.2 3.12-.5 4.58-1.08.47-.19.48-.86.01-1.06-1.45-.61-2.83-.82-4.38-.87-1.23-.04-3.94-.13-4.34-.71-.57-.83.18-3.44.35-4.76.4-3.16 1.05-6.3 1.94-9.36 1.08-3.74 2.08-7.85 4.22-11.15 1.2-1.85 2.77-3.12 4.24-4.71 1.43-1.54.63-3.73-1.71-3.98-1.55-.17-5.63 3.62-6.01 3.91-.8.63-1.6 1.25-2.4 1.88-1.6 1.26-3.19 2.53-4.77 3.81-9.61 7.77-19.01 15.88-27.85 24.52a310.903 310.903 0 00-18.99 20.27c-1.56 1.82-3.1 3.67-4.62 5.53-1.24 1.52-3.09 3.21-3.78 5.07-.69 1.87.21 3.78 2.05 4.55.92.39 3.32.8 4.23.34 1.34-.68.88-2.28 1.28-3.48.66-1.93 2.18-3.87 3.35-5.52 1.34-1.88 2.8-3.67 4.32-5.4 1.03-1.18 2.49-3.47 4.19-3.75 2.43-.41 4 3.05 5.52 4.35 2.41 2.06 4.94 4.09 7.81 5.49 2.53 1.23 5.4 1.97 8.22 1.68zm-10.89-24.45c3.78-3.62 7.57-7.24 11.35-10.86 1.94-1.86 3.88-3.71 5.82-5.57.71-.68 2.82-3.19 4.32-4.2.48-.32 1.1.13.91.68-1 2.95-1.99 5.9-2.99 8.86-.66 1.95-1.33 6.49-3.03 7.65-.91.62-2.98.74-4.03 1l-4.14 1.02c-2.64.65-5.28 1.29-7.92 1.94-.31.07-.51-.31-.29-.52zm-3.52 12.35c-1.85-1.36-3.88-2.6-1.22-4.09 4.61-2.58 9.89-4.28 14.93-5.76 1.2-.35 5.1-1.91 5.31-.55.14.92-1.13 2.99-1.51 3.81-.62 1.32-1.27 2.55-1.94 3.85-2.03 3.9-4.68 5.26-9.12 5.24-2.41-.05-4.56-1.1-6.45-2.5zM351.8 88.87c2.28-8.14 5.34-16.03 8.69-23.78 1.68-3.88 3.43-7.73 5.21-11.57 1.29-2.78 4.48-6.45-1.03-8.01-3.82-1.08-5.76.64-7.2 3.05-1.02 1.72-1.56 1.8-3.87 1.51-2.02-.25-3.86.1-5.56.79-7.65 3.1-14.46 7.93-20.12 13.1-5.82 5.31-11.46 11.15-16.06 17.32-3.65 4.89-8.95 13.56-4.2 19.96.9 1.21 2.5 3.04 4.28 2.84 1.36-.15 2.36-1.56 3.37-2.19 3.67-2.27 6.72-5.08 9.86-7.87 6.71-5.95 13.17-12.1 19.21-18.57-1.17 4.62-2.78 9.11-3.79 13.78-.83 3.82-.66 3.29 1.34 7.3.59 1.19 2.68 4.48 4.44 4.41 1.61-.06 2.81-3.26 3.22-4.28 1.65-4.08 1.02-3.55 2.21-7.79zm-40.59 8.35c-.11.08-.3-.04-.25-.16 7.5-18.08 21.7-31.23 39.87-41.46.5-.28 1.2.29.9.73-10.63 15.73-24.4 29.16-40.52 40.89zm140.06-52.37c-.47-1.33 1.57-2.84 2.3-3.69 2.03-2.35 4.38-5.56 7.91-4.82 1.95.41 4.87 3.09 3.94 5.37-.63 1.54-3.37 1.74-4.7 2.15-1.89.58-3.75 1.25-5.64 1.85-1.09.16-3.29.6-3.81-.86zm417.87 11.21c-.51 1.41-3.26 1.59-4.56 1.97-1.86.53-3.68 1.14-5.53 1.69-1.1.14-3.28.54-3.92-.79-.58-1.21 1.35-2.6 2.02-3.37 1.85-2.15 3.97-5.09 7.59-4.4 2 .37 5.15 2.82 4.4 4.9zm-614.63-5.15c-.56-.89.88-2.54 1.29-3.25.61-1.07 1.06-3.15 1.9-3.96 1.04-1.01 4.02.08 6.4 1.23 2.04.99 2.32 3.83.47 5.14-.68.48-1.4.84-2.01 1.04-1.23.41-2.36.12-3.63.12-1.02-.01-3.75.73-4.42-.32zm168.72 46.37c-.02 1.13-1.88 1.43-2.65 1.54-3.48.5-12.57 2.14-16.94 3.06-1.83.43-3.69.76-5.53 1.14-8.27 1.68-16.59 3.29-24.7 5.63a92.07 92.07 0 00-11.39 4.11c-1.78.78-3.56 1.59-5.27 2.5-1.47.77-3.15 2.03-4.91 1.73-1.81-.3-2.82-2.31-3.35-4.02-.21-.69-.4-1.39-.46-2.11-.12-1.48.44-2.42 1.36-3.52 1.34-1.59 2.75-3.13 4.16-4.66 5.57-6.01 11.53-11.68 17.85-16.9 6.22-5.14 12.74-9.91 19.19-14.76 5.11-3.85 10.24-7.67 15.37-11.5 2.72-2.02 5.43-4.05 8.15-6.07 1.09-.8 2.36-1.53 3.59-2.3-6.85-.19-13.69-.47-20.53-.7-3.95-.14-7.96-.11-11.91-.39-2.01-.15-4.02-.48-5.91-1.1-1.38-.45-3.37-.68-4.57-1.56-2.42-1.77-3.6-8.63-.18-10.07 1.29-.54 3.62.4 5.03.51 2.12.17 4.24.05 6.35-.11 4.12-.32 8.21-.46 12.35-.53 8.53-.14 17.07-.13 25.59.3 6.15.31 13.77 1.82 15.9 7.6.71 1.94.99 4.56-.6 6.23-1.13 1.18-1.8.53-3.35.5-2.04-.04-3.53.7-3.89.9-6.92 3.84-13.4 8.7-19.88 13.35-15.05 10.8-29.41 22.47-44.02 33.95-.19.15-.06.48.17.42 9.61-2.37 19.24-4.96 29.03-6.35 5.23-.74 10.53-1.01 15.77-.3 2.62.36 5.15 1.01 7.69 1.79.57.2 2.52.54 2.49 1.69zM856.16 67.3c-1.84.14-2.55 1.69-3.59 2.79-1.55 1.65-3.56 3-5.31 4.47-16.49 13.87-30.61 29.84-45.69 44.93.66-4.81 1.19-9.55 2.68-14.17 1.54-4.74 3.74-9.19 5.71-13.76 1.75-4.06 3.31-8.23 5.81-11.93 2.86-4.23 7.13-7.09 11.13-10.36 7.7-6.29 14.9-13.07 21.82-20.06 6.9-6.97 13.95-13.98 19.87-21.69 2.77-3.61 5.07-7.5 7.07-11.53 1.23-2.46 2.27-5.55-.46-7.15-3.82-2.24-10.52-2.04-14.37-.9-4.46 1.33-8.45 3.58-12.06 6.17-7.55 5.42-14.27 11.92-20 18.92-3 3.66-5.77 7.47-8.48 11.32-.88 1.24-1.91 3.83-3.89 3.76-1.91-.07-4.38-2.1-6.23-2.76-10.12-3.64-22.07-2.65-32.16-1.28-10.47 1.42-20.64 3.72-30.87 5.91 1.98-2.86 3.94-5.71 6.05-8.49 1.65-2.18 5.52-4.61 5.54-7.47.02-2.45-2.29-6.12-5.27-6.41-3.27-.31-4.98 3.41-6.49 5.34-4.51 5.73-8.66 11.69-13.13 17.44-3.88 4.99-9.5 5.03-15.78 5.49-.79.06-1.2.86-.73 1.49.88 1.18 2.14 2.02 3.77 2.34 1.01.2 4.87.05 5.82.05-.19 0-.45.57-.53.69-6.72 10.21-13.61 20.3-19.85 30.84-3.13 5.29-6.39 10.57-9.01 16.14-2.06 4.39-3.47 9.49-.19 14.12 2.08 2.94 7.42 3.55 9.89 1.43 3.95-3.39 7.37-7.52 10.96-11.23 7.12-7.32 14.17-14.7 21.26-22.05-.24.24.47 2.59.56 3 .25 1.15.55 2.28 1.04 3.35 1.65 3.57 5.51 5.09 8.95 2.85 5.58-3.63 10.63-8.29 15.86-12.33-2.45 5.29-5.69 10.61-7.45 16.16-.28.87-.28 1.82-.12 2.71.47 2.64 2.32 5.13 4.72 6.33 3.07 1.54 7.12.84 10.12-.51 2.71-1.22 5.03-3.06 7.45-4.75 2.69-1.89 11.16-8.82 11.26-9.2-2.31 8.38-4.46 20.58 3.38 27.66 7.34 6.63 14.38-1.03 18.8-5.67 6.48-6.79 11.99-14.16 17.46-21.65 2.87-3.93 5.89-7.78 8.95-11.58 2.12-2.64 5.12-5.66 8.15-7.95-1.26 2.31-3.41 5.43-4.68 7.97-1.13 2.27-2.52 4.39-3.46 6.75-.9 2.24-1.92 4.74-.27 7.07.93 1.31 3.33 3.41 4.9 2.17 1.29-1.02 1.92-3.8 2.54-5.19.98-2.19 1.84-4.43 2.69-6.67 1.73-4.63 3.33-9.32 5.46-13.8.58-1.22 1.2-2.43 1.87-3.61.53-.93 1.68-2.09 1.92-3.1.33-1.59-1.59-2.54-3.39-2.41zm-19.68-28.14c5.92-8.18 13-16.72 21.63-22.68 1.99-1.37 4.51-3.15 7.09-3.53 1.52-.22 5.95.2 6.37 2.01.38 1.64-1.91 4.45-2.78 5.83-1.06 1.7-2.13 3.4-3.32 5.02-6.72 9.16-14.6 17.61-22.95 25.55-3.98 3.79-8.09 7.46-12.28 11.06-2.1 1.8-4.21 3.58-6.34 5.35-1.11.92-2.2 1.86-3.35 2.74-.83.64-2 1.84-2.77 1.16s11.32-22.33 18.7-32.51zm-39.65 45.3c-1.05 2.6-1.32 4.2-2.57 6.02-1.55 2.25-3.59 4.11-5.7 5.82-.1.08-.19.16-.29.23-1.6 1.3-3.16 2.64-4.78 3.92-1.29 1.07-2.72 1.98-4.04 3.01-.23.18-.47.35-.7.52-.15.1-.32.22-.5.34-5.51 3.88-3.64-2.78-3.49-3.28.9-2.76 1.56-5.67 2.99-8.64 2.04-4.22 4.03-8.46 5.96-12.72 1.3-2.87 4.73-7.51 3.14-10.84-.64-1.35-2.57-3.41-4.15-3.89-1.72-.53-2.5.46-3.58 1.44-3.27 2.97-6.66 5.84-10.03 8.72-3.29 2.8-6.62 5.57-10.03 8.26a210.9 210.9 0 01-5.58 4.27c-1.42 1.05-3.07 2.74-4.9 3.14-.92-4.63 3.87-8.59 6.83-11.37 3.63-3.41 7.49-6.55 11.38-9.64 3.44-2.74 7.25-5.11 11.48-6.83 1.11-.45 3.72-1 3.99-2.28.33-1.6-1.83-2.44-3.22-2.46-2.18-.02-4.34 1.19-6.19 1.98-2.24.97-4.43 2.03-6.57 3.17-4.4 2.34-8.57 5.01-12.57 7.85-8.47 6.01-16.25 12.76-23.69 19.77-3.69 3.48-7.3 7.04-10.87 10.63-1.85 1.85-3.68 3.71-5.52 5.57-1.43 1.45-2.95 3.81-5.14 4.42 7.7-13.82 15.2-27.6 24.01-40.84 2.14-3.22 4.26-6.47 6.41-9.68 1.45-2.17 3.13-3.55 5.92-4.18 6.84-1.55 14.11-2.42 21.09-3.52 7.98-1.26 16.01-2.53 24.16-3.07 4.03-.26 8.09-.35 12.15-.25 2.99.07 7.27-.29 10.13.81 2.4.93 1.04 2.86.21 4.31-1.67 2.89-3.41 5.75-5.12 8.62-3.91 6.63-7.73 13.49-10.62 20.67z" fill="#fff"></path></svg>
				</div>
				<p class="g_text">
					Fortunately, you won't have to travel far beyond your home to get a taste for yourself.
				</p></div>
			</div>
        </div>
		 <div class="row bgt" style="background-image:url(https://www.nespresso.com/shared_res/agility/ispirazioneItaliana/ispirazioneItaliana/ispirazioneItaliana/img/backgrounds/marble-coffees_L.jpg) ">
			<div class="items" >
				<div class="g_content" >
					<div class="g_textc">
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-napoli_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br>Napoli</p>
									<div class="g_desc"><p>The darkest roast in this range pays tribute to this Italian capital of coffee by creating a creamy coffee that intensifies with every mouthful.</p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 14,700</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>
												</div>
											</div>
										</div><a href="/ispirazione-napoli" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-napoli_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">13</span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-palermo-kazaar_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br>Palermo Kazaar</p>
									<div class="g_desc"><p>A long and dark roast inspired by the south that enhances the wildness and spiciness of this blend.<br>
Formerly: <strong class="v_brand" term="kazaar">Kazaar</strong></p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 18,000</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazione-palermo-kazaar" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-palermo-kazaar_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">12</span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-ristretto-italiano_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br>Ristretto Italiano</p>
									<div class="g_desc"><p>A medium dark roast that reveals its intensity with elegant fruity notes and a hint of acidity that dances through the blend.<br>
Formerly: <strong class="v_brand" term="ristretto">Ristretto</strong></p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 14,700</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazione-ristretto-italiano" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-ristretto-italiano_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">10</span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-firenze-arpeggio_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br>Firenze Arpeggio</p>
									<div class="g_desc"><p>This short and dark roast combines cocoa notes with sweeter blends to create a velvety masterpiece.<br>
Formerly: <strong class="v_brand" term="arpeggio">Arpeggio</strong></p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 14,700</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazione-firenze-arpeggio" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-firenze-arpeggio_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">9</span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-roma_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br> Roma</p>
									<div class="g_desc"><p>This short roast with elegant aromatics achieves woody and cereal notes with hints of acidity.<br>
Formerly: <strong class="v_brand" term="roma">Roma</strong></p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 14,700</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazione-roma" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-roma_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">8</span><span class="ProductListElement__intensity-block"></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-venezia_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br>Venezia</p>
									<div class="g_desc"><p>A long roast harmonising a hint of fruity and floral aromas, the distinct cereal fragrance, and luxurious notes of sweet caramel.</p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 14,700</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazione-venezia" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-venezia_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">8</span><span class="ProductListElement__intensity-block"></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-genova-livanto_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br>Genova Livanto</strong></p>
									<div class="g_desc"><p>This sweet medium roast delivers a beautiful balanced and aromatic complexity of cereal and caramel notes.<br>
Formerly: <strong class="v_brand" term="livanto">Livanto</strong></p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 14,700</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazione-genova-livanto" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-genova-livanto_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">6</span><span class="ProductListElement__intensity-block"></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block"></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(https://www.nespresso.com/shared_res/agility/ispirazioneItaliana/ispirazioneItaliana/ispirazioneItaliana/img/coffees/OL/textures/ispirazione-ristretto-italiano-decafeinatto_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br> Ristretto Italiano Decaffeinato</p>
									<div class="g_desc"><p>A medium dark roast that reveals its intensity with elegant fruity notes and a hint of acidity that dances through the blend.<br>
Formerly: <strong class="v_brand" term="ristretto decaffeinato">Ristretto Decaffeinato</strong></p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 17,000</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazione-ristretto-italiano-decaffeinato" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(https://www.nespresso.com/shared_res/agility/ispirazioneItaliana/ispirazioneItaliana/ispirazioneItaliana/img/coffees/OL/capsules/ispirazione-ristretto-italiano-decafeinatto_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">10</span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
						<div class="item" >
							<div class="g_bg" style="background-image: url(img/coffees/OL/textures/ispirazione-firenze-arpeggio-decaffeinato_L.jpg);"></div>
							<div class="g_data">
								<div class="g_header">
									<p class="g_h3">Ispirazione<br>Firenze Arpeggio Decaffeinato</p>
									<div class="g_desc"><p>This short and dark roast combines cocoa notes with sweeter blends to create a velvety masterpiece.<br>
Formerly: <strong class="v_brand" term="arpeggio decaffeinato">Arpeggio Decaffeinato</strong></p></div>
								</div>
								<div class="g_detail">
									<div class="g_card">
										<div class="g_wysiwyg g_txt_S">
											<p>(1 sleeve contains 10 capsules)</p>
										</div>
										<div class="g_addToCart">
											<!---->
											<div data-product-item-id="7545.40" data-product-section="Tiles" data-product-position="1" class="g_priceAndButton track-impression-product">
												<p class="g_productPrice">VNĐ 17,000</p>
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/coffee-list"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div><a href="/ispirazion-firenze-arpeggio-decaffeinato" class="g_txt_S g_link"><span class=""><span style="font-size: 15px;">Discover the full story here</span></span><i class="fn_angleRight"></i></a>
									</div>
									<div class="g_img" style="background-image:url(img/coffees/OL/capsules/ispirazione-firenze-arpeggio-decaffeinato_XL.png)"></div>
									<div class="g_intensity"><p class="g_h5_nomargin">intensity</p><div aria-hidden="true" class="ProductListElement__intensity"><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__intensity-block ProductListElement__intensity-block--filled"></span><span class="ProductListElement__current-intensity">9</span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span><span class="ProductListElement__intensity-block "></span></div></div>
								</div>
								<div class="g_button">
									<i class="fa fa-angle-down"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- bbb -->
				<div class="items3">
					<img alt="" aria-hidden="true" class="g_cup"  src="img/crossSell/coffee-cup_L.png"/>
					<h2 class="g_h2">ENJOY THE ISPIRAZIONE ITALIANA RANGE WITH OUR ACCESSORIES AND MACHINES</h2>					
				</div>
				<!-- sl-->
				<div class="item4">
					<div id="sl2" class="owl-carousel owl-theme">
						<div class="item2">
							<div class="itemcard">
								<div class="ic_head">
									<div class="ic_off">Free Shipping</div>
									<div class="ic_name">Pixie Red</div>
								</div>
								<div class="ic_body">
									<div class="ic_img"><a  style="background-image:url(img/public/h1.png)" href="/pixie"></a></div>
									<div class="ic_right">
										<div class="ic_desc">
											<p><strong class="v_brand" term="pixie">Pixie</strong> is the SMART model in our range, condensing a wide range of innovative, advanced features in a surprisingly small machine.</p>
										</div>
										<div class="ic_action">
											<div class="ic_price">
											<p class="g_from"><strong>VNĐ 5,900,000</strong></p>
											</div>
											<div class="ic_button g_addToCart">
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/pixie"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item2">
							<div class="itemcard">
								<div class="ic_head">
									<div class="ic_off">Free Shipping</div>
									<div class="ic_name">Inissia  Black</div>
								</div>
								<div class="ic_body">
									<div class="ic_img"><a  style="background-image:url(img/public/ht.png)" href="/inissia"></a></div>
									<div class="ic_right">
										<div class="ic_desc">
										<p>Compact and lightweight coffee machine that will fit into any kitchen interior</p>
										</div>
										<div class="ic_action">
											<div class="ic_price">
												<p class="g_from "><strong>VNĐ 4,100,000</strong></p>
											</div>
											<div class="ic_button g_addToCart">
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/inissia"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="item2">
							<div class="itemcard">
								<div class="ic_head">
									<div class="ic_off">Free Shipping</div>
									<div class="ic_name">Essenza Mini Red</div>
								</div>
								<div class="ic_body">
									<div class="ic_img"><a  style="background-image:url(img/public/h6.png)" href="/essenza-mini"></a></div>
									<div class="ic_right">
										<div class="ic_desc">
											<p>Our compact machine yet – without any compromise on taste. Essenza Mini opens up the whole world of Nespresso coffee.</p>
										</div>
										<div class="ic_action">
											<div class="ic_price">
												<p class="g_from "><strong>VNĐ 3,550,000</strong></p>
											</div>
											<div class="ic_button g_addToCart">
												<div class="g_addToCartCustom">
													<div class="g_btnaddtocart"><a style="color:#fff" href="/essenza-mini"><i class="icon-basket"></i> ADD <i class="icon-plus"></i></a></div>													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- form -->
				<div class="fm">
					<div class="formcode">						
						<p>Get a chance to win an Aeroccino 4</p>
						<form>
							<div class="msg"></div>
							<div class="inp">
								<input class="inputcode" placeholder="Enter your code for your chance to win!"/>							
								<a class="submitbtn">Submit ></a>
							</div>
						</form>
						<div class="howto">
							<h3>HOW TO PLAY</h3>
							<ul>
								<li>You will receive 1 entry ticket for every Ispirazione Italiana sleeve you purchase.</li>
								<li style="position: relative;
    padding-right: 100px;
    min-height: 100px;">Your boarding pass number (a 7-letter code) on each ticket will be your chance to win our latest milk frother Aerocino 4. <div class="gf_img1" style="background-image:url(img/public/gift.png)"></div></li>
								<li>Go to nespresso.vn/ispirazione-italiana or scan the QR then submit your code to find out if you are the lucky winner.</li>
								<li>If you are the congratulations, to redeem the prize, please contact our hotline at 1900 633 474. Our lines are open from Monday - Saturday, 10am to 6pm.
</li>
								<li>The event starts from March 2nd, 2020 – April 8th, 2020
</li>
								<li>After April 8th, we will stop receiving any code submission.
</li>							
								<li id="termshow">Click here to read more about <strong>TERMS AND CONDITIONS</strong>
</li>
							</ul>
						</div>
					</div>					
				</div>
				<!-- video -->
				<div class="video">
					<div class="vd_img" style="background-image: url(img/backgrounds/video_L.jpg);"></div>
					<div class="vd_content">
						<div class="vd_h3">
							<p>EXPLORE ISPIRAZIONE ITALIANA COFFEES</p>
						</div>
						<div class="vd_action">
							<a href="https://youtu.be/uDLvrTYJLd0" target="_new">WATCH THE VIDEO</a>
						</div>
					</div>
				</div>
				<!-- title -->
				<div class="items5">
					<h2 class="g_h2">GET A TASTE OF OUR ISPIRAZIONE ITALIANA COFFEES WITH MILK</h2>
					<p>Maybe you're a Cappuccino connoisseur who loves an espresso topped with delightfully textured milk. Or a Macchiato enthusiast who prefers their coffee with a dash of milk foam. Our range of Ispirazione Italiana coffees can be enjoyed with a splash of milk too, just don't tell your Italian friends if it's after 11am.</p>					
					<p>Rediscover the Ispirazione Italiana coffee range inspired by Italian roasting traditions at any <strong>Nespresso</strong> Boutiques.</p>					
					<p style="display:none"><a href="nespresso-events#ispirazione">Learn more ></a></p>
				</div>
				<!-- slide 3-->
				<div class="item6">
					<div id="sl3" class="owl-carousel owl-theme">
						<div class="item2">
							<div class="itemcard">								
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/machine-and-milk_L.jpg);"></div>
									<div class="sl3_text">								
									</div>
								</div>
							</div>
						</div>	
						<div class="item2">
							<div class="itemcard">
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/capsules-assortment_L.jpg);"></div>
									<div class="sl3_text">	
										<span>New Ispirazione Italiana coffee range</span>
									</div>
								</div>
							</div>
						</div>		
						<div class="item2">
							<div class="itemcard">
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/napoli_L.jpg);"></div>
									<div class="sl3_text">	
										<span>Ispirazione Napoli</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item2">
							<div class="itemcard">
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/palermo_L.jpg);"></div>
									<div class="sl3_text">	
										<span>Ispirazione Palermo Kazaar</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item2">
							<div class="itemcard">
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/roma_L.jpg);"></div>
									<div class="sl3_text">	
										<span>Ispirazione Roma</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item2">
							<div class="itemcard">
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/firenze_L.jpg);"></div>
									<div class="sl3_text">	
										<span>Ispirazione Firenze Arpeggio</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item2">
							<div class="itemcard">
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/venezia_L.jpg);"></div>
									<div class="sl3_text">	
										<span>Ispirazione Venezia</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item2">
							<div class="itemcard">
								<div class="bsl">
									<div class="sl3_img" style="background-image: url(img/gallery/napoli-venezia_L.jpg);"></div>
									<div class="sl3_text">	
										<span>Ispirazione Napoli</span>
										<span>Ispirazione Venezia</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end -->
				<div class="lastb">
					<div class="l_img" style="background-image: url(img/public/bgf.jpg);background-color:black;"></div>
					<div class="l_content" >
						<div class="l_h3">
							<p><strong>NESPRESSO</strong>. WHAT ELSE?</p>
						</div>
						<div class="l_c">
							<p>Get the most out of your Nespresso coffee experience knowing we're committed to making a positive impact on the environment and on sourcing the highest quality coffee to create a wide variety of authentic flavours - all at the touch of a button.</p>
						</div>
						<div class="l_action">
							<a target="_new" href="https://www.nespresso.com/us/en/sustainability">Read more about Nespresso ></a>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</main>
<div id="termcondition">
	<div class="ter_content">
		<span id="closeterm"><i class="fa fa-times"></i></span>
		<div class="ter_title">
			<h2><strong>TERMS AND CONDITIONS</strong></h2>
		</div>
		<div class="ter_text">
		<p>By participating in this competition, the participant is indicating his/her agreement to be bound by these terms and conditions. </p>
		<ul style="list-style: decimal">
			<li>Nespresso will not accept any responsibilities if contact details provided are incomplete or inaccurate. </li>
			<li>The prize is as stated and no cash or other alternatives will be offered. The prizes are not transferable. Prizes are subject to availability and we reserve the right to substitute any prize with another of equivalent value without giving any notice. </li>
			<li>Winners will be chosen by Nespresso. We will notify the winner when and where the prize can be collected / is delivered. </li>
			<li>Winners will be notified in our website www.nespresso.vn and/or by email and/or direct call from our customer service department within 28 days of the closing date. If the winner cannot be contacted or does not claim the prize within 14 days of notification, we reserve the right to withdraw the prize from the winner and to pick a replacement winner. </li>
			<li>The winner agrees to the use of his/her name and image in any publicity material, as well as their entry. Any personal data relating to the winner or any other entrants will be used solely in accordance with current data protection legislation and will not be disclosed to a third party without the entrant’s prior consent. </li>
			<li>Nespresso shall have the right, at its sole discretion and at any time, to change or modify these terms and conditions, such change shall be effective immediately upon posting to this webpage. </li>
			<li>Nespresso also reserves the right to cancel the competition, change, add and/or modify the Terms and Conditions at its sole discretion if circumstances arise outside of its control. </li>
			<li>Nespresso will responsible for all the shipping costs and transportation costs incurred in sending the prizes to the winner’s given address </li>
			<li>Nespresso will choose the carrier/shipping, but it will not be considered as being responsible for the shipment or the carrier is interpreted as Nespresso agent. The risk of loss or damage to all prizes under this agreement will switch from Nespresso to the Winner individually when Nespresso give and handle the prizes to the operator of the shipment </li>
			<li>The winner must fulfill and provide all the requested data and information from Nespresso (documents that need to be collected from the winner (ID proof, Address proof, Proof of purchase…etc. before the winner can legally be entitled to receive the prize</li>
		</ul>
		</div>

	</div>
</div>
<?php include 'footer.php'; ?>
<script src="owl/owl.carousel.min.js"></script>
<!-- nespresso scripts -->

																																																																																																																																												<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/withinviewport.js"></script>
																																																																																																																																										
																																																																																																																																												<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/jquery.withinviewport.js"></script>
																																																																																																																																										
																																																																																																																																												<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/gtm_function.js"></script>
						
<!-- /wp_footer -->
<script>
jQuery(document).ready(function(){
  jQuery("#sliderbanner").owlCarousel({
		loop:true,
		margin:0,
		nav:false,
		dots:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	});
  jQuery("#sl2").owlCarousel({
		loop:false,
		margin:20,
		nav:false,
		dots:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});
	jQuery("#sl3").owlCarousel({
		loop:false,
		margin:20,
		nav:true,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		dots:false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});
	jQuery('a.submitbtn').click(function(){
		var code = jQuery('.inputcode').val(),_msg = jQuery('.msg');
		_msg.empty();
		if(code)
		{
			if(code=='1010738')
				_msg.html('<div class="sccode"><div class="gf_img" style="background-image:url(img/public/gift.png)"></div> <div class="msgcs">Congratulations. You have won an Aeroccino 4. To claim the price, please contact our hotline <a style="color:#040432" href="tel:1900633474">1900633474</a> or email <a style="color:#040432" href="mailto:contact@nespresso.vn">contact@nespresso.vn</a></div></div>');
			else
				_msg.html('<div class="ercode">Wish you luck next time</div>');
		}else{
			_msg.html('<div class="ercode">Please enter your code!</div>');
		}
	});
	jQuery('span#closeterm').click(function(){
		jQuery('#termcondition').hide();
	});
	jQuery('#termshow').click(function(){
		jQuery('#termcondition').show();
		jQuery('.ter_text').css('height',jQuery(window).height()-100);
	});
});
</script>
</body>
</html>

					</div><!-- .entry-content -->
		</div><!-- #post-## -->

	<!--</main>--><!-- #main -->
<!--</div>--><!-- #primary -->


<!-- get_footer -->
