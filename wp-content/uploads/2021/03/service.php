<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * Template Name: Nespresso Services
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>

<?php get_header(); ?>

<!-- block 1-->
<div class="text-center blog1">
    <h1>NESPRESSO SERVICES</h1>

    <h3>Good afternoon, how can we help you?</h3>

    <p>As a Nespresso Member, you benefit from many personalized services, so that you can enjoy your coffee your way.</p>
</div>


<section id="order" class="main hvrbox" >
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-6 block1" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/ordering.webp');">
                
                <div class="boxtop1">
                    <h1> <i class="icon icon-accessories " ></i>  <br> Ordering <br> <div class="ordertitle">
                    Order your capsules 24/7 from our website
                    </div> </h1>
                   
                    

                </div>
                

                <div class="box" id="myBtn">
                        Online
                </div>

                <!-- The Modal -->
                <div id="myModal" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="boxtop1" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/ordering.webp');">
                                    <h1> 
                                        <i class="icon icon-accessories " ></i>  <br> 
                                        Ordering  <br> 
                                        Order your capsules 24/7 from our website <br>
                                     </h1>
                                   
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3 class="g_h3">Standard Delivery</h3>
                                <h4 class="g_h4">Enjoy your <strong>Nespresso</strong></h4>
                                <div class="">
                                    <p class="text_1" >
                                        <a href="#">Creating an account</a> 
                                        with us will give you full access to everything 
                                        <strong>Nespresso</strong>. 
                                    </p>
                                    <p class="text_1" >
                                        Enjoy free delivery when you spend a minimum of 1,500,000 VND. Delivery takes up to 3 working days for Ho Chi Minh City and up to 5 days for other provinces.
                                    </p>
                                </div>
                            </div>
                        
                        </div>


                        <span class="close">&times;</span>
                        
                    </div>

                    </div>

                </div>

            <div class="col-sm-6  text-center" >
                    
                    <h1> Order online</h1>

                   
                    <div class="boxleft" >
                            Receive your coffee, your way
                    </div>
                    <span class="boxleft2">
                       <a href="#"> Order coffee now</a>
                    </span>
            </div>
        </div>
        
    </div>
</section>


<section id="order2" class="main hvrbox" >
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-6 block2" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/delivery.webp');">
            
                <div class="boxtop2">
                    <h1 style="color: #FFF;font-size: 2rem;"> <div class="text-center order2-img"> <img class="text-center" style="width: 100px;" src="https://www.nespresso.vn/wp-content/uploads/2019/02/delivery-navi-229-x-229.jpg"> </div>  <br> Delivery <br>
                    
                    </h1>
                    <div class="order2title">
                    Being delivered has never been so simple. We adapt to your time and to your place to deliver you the best way you deserve...
                    
                    </div>
                </div>

                <div class="box2" id="myBtn2">
                    Delivery
                </div>

                <!-- The Modal -->
                <div id="myModal2" class="modal2">
                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="boxtop2" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/delivery.webp');">
                                    <h1> 
                                        Delivery  <br> 
                                     </h1>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3 class="modal_h3">Standard Delivery</h3>
                                <h4 class="modal_h4">Enjoy your <strong>Nespresso</strong></h4>
                                <div class="">
                                    <p class="text_modal2" >
                                        <a href="#">Creating an account</a> 
                                        with us will give you full access to everything 
                                        <strong>Nespresso</strong>. 
                                    </p>
                                    <p class="text_modal2" >
                                        Enjoy free delivery when you spend a minimum of 1,500,000 VND. Delivery takes up to 3 working days for Ho Chi Minh City and up to 5 days for other provinces.
                                    </p>
                                </div>
                            </div>
                        
                        </div>
                        <span class="close2">&times;</span>
                    </div>

                </div>

            </div>

                <div class="col-sm-6  text-center" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/customer-care.webp');"  >
                        
                    <h3 class="box2_h3">Standard Delivery</h3>
                    <h4 class="box2_h4">Enjoy your <strong>Nespresso</strong></h4>
                    <div class="">
                        <p class="box2_boxleft" >
                            Enjoy free delivery when you spend a minimum of 1,500,000 VND. Delivery takes up to 3 working days for Ho Chi Minh City and up to 5 days for other provinces.
                        </p>
                    </div>
        
                </div>
        </div>
        
    </div>
</section>


<script>
jQuery('.block1').hover(function()
{
    jQuery(".box").show();
    // jQuery(".boxtop1").hide();
    jQuery(".box").css("opacity", "1");
    jQuery(".block1").css("opacity", ".7");
    
}, function()
{ 
    jQuery(".box").hide();
});


jQuery('.block2').hover(function()
{
    jQuery(".box2").show();
    // jQuery(".boxtop1").hide();
    jQuery(".box2").css("opacity", "1");
    jQuery(".block2").css("opacity", ".7");
    
}, function()
{ 
    jQuery(".box2").hide();
});


// session 1
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close");
btn.onclick = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// session 2
var modal2 = document.getElementById("myModal2");
var btn2 = document.getElementById("myBtn2");
var span2 = document.getElementsByClassName("close2");
btn2.onclick = function() {
    modal2.style.display = "block";
}
span2.onclick = function() {
    modal2.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal2) {
    modal2.style.display = "none";
  }
}


</script>
<style>
p.text_modal2 {
    position: absolute;
    font-size: 1.1rem;
    top: 85px;
    padding: 20px;
    text-transform: lowercase;
}
h3.modal_h3 {
    position: absolute;
    font-size: 1.2rem;
    top: 20px;
}

h4.modal_h4 {
    position: absolute;
    font-size: 1.1rem;
    top: 50px;
}


p.box2_boxleft {
    position: absolute;
    top: 150px;
    left: 35px;
    font-size: 1.2rem;
    float: right;
}
h4.box2_h4 {
    position: absolute;
    top: 90px;
    left: 35px;
    font-size: 1.2rem;
    color: #FFF;
}
h3.box2_h3 {
    color: #ffff;
    position: absolute;
    top: 50px;
    left: 35px;
    font-size: 1.2rem;
}
.ordertitle{
    font-size: 1.2rem;
    color: #FFF;
    padding: 0px 30px;
    font-weight: 300;
    line-height: normal;
}
.order2title{
    font-size: 1.2rem;
    color: #FFF;
    padding: 0px 30px;
    font-weight: 300;
    line-height: normal;
}
.boxtop2 {
    padding-top: 120px;
    height: 350px;
}

.text-center.order2-img {
    position: absolute;
    left: 35%;
    top: 60px;
}
p.text_1 {
    font-size: 1rem;
    float: right;
}
h4.g_h4 {
    float: inherit;
}
h3.g_h3 {
    float: inherit;
    font-size: 1.2rem;
}
.text_1{
    color: #000 !important;
    font-size: 1.3rem;
}

body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal2 {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding: 20px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto; 
  margin: auto;
  color: #000;

}
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding: 20px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto; 
  margin: auto;
  color: #000;

}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
    top: 25%;
    position: absolute;
    left: 15%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.boxleft{
    color: #fff;
    position: absolute;
    font-size: 1rem;
    opacity: 1;
    padding-top: 30px;
    top: 210px;
    left: 25%;
}
.boxleft2{
    color: #f7fb42;
    position: absolute;
    font-size: 1rem;
    opacity: 1;
    padding-top: 30px;
    top: 240px;
    left: 35%;
    text-decoration: underline;
}
.box{
    display: none;
    background: white;
    margin: -5rem 10rem;
    padding: 25px 20px;
    color: #000;
    font-size: 1.3rem;
}
.box2{
    display: none;
    background: white;
    margin: 1rem 10rem;
    padding: 25px 20px;
    color: #000;
    font-size: 1.5rem;
}
.block1,.block2{
    height: 500px;
    opacity: 1;
}
.blog1{
    display: table;
    position: relative;
    width: 100%;
    height: 100%;
    max-width: 66.25em;
    padding: 0 2em;
    margin: 0 auto;
}
.blog1 h1{
    margin-bottom: 3rem;
    color: #FFF;
    font-size: 3.5rem;
    
}
.blog1 h3{
    margin-bottom: 1.33333em;
    font-size: 1.3rem;
    color: #fff;
}
.blog1 p{
    margin-bottom: 1.33333em;
    font-size: 1rem;
}

#order h1 {
    color: #fff;
    font-size: 2rem;
    padding: 80px 0px;
}
</style>


<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/libs/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/components/services.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
