<?php
 
/**
 * Plugin Name: express Shipping
 * Description: Custom Shipping Method for WooCommerce
 * Version: 1.0.0
 * Author:hieu nguyen
 * Author URI: http://www.ibenic.com
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Domain Path: /lang
 * Text Domain: express
 */
 
if ( ! defined( 'WPINC' ) ) {
 
    die;
 
}
 
/*
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
    function express_shipping_method() {
        if ( ! class_exists( 'Express_Shipping_Method' ) ) {
            class Express_Shipping_Method extends WC_Shipping_Method {
                /**
                 * Constructor for your shipping class
                 *
                 * @access public
                 * @return void
                 */
                public function __construct() {
                    $this->id                 = 'express'; 
                    $this->method_title       = __( 'Express Shipping', 'express' );  
                    $this->method_description = __( 'Custom Shipping Method for Express', 'express' ); 
					// Availability & Countries
					$this->availability = 'including';
					$this->countries = array(						
						'VN' // VietNam
					);

                    $this->init();
 
                    $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
                    $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'Express Shipping', 'express' );
                }
 
                /**
                 * Init your settings
                 *
                 * @access public
                 * @return void
                 */
                function init() {
                    // Load the settings API
                    $this->init_form_fields(); 
                    $this->init_settings(); 
 
                    // Save settings in admin if you have any defined
                    add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
                }
 
                /**
                 * Define settings field for this shipping
                 * @return void 
                 */
                function init_form_fields() { 

                   $this->form_fields = array(
 
					 'enabled' => array(
						  'title' => __( 'Enable', 'express' ),
						  'type' => 'checkbox',
						  'description' => __( 'Enable this shipping.', 'express' ),
						  'default' => 'yes'
						  ),
			 
					 'title' => array(
						'title' => __( 'Title', 'express' ),
						  'type' => 'text',
						  'description' => __( 'Title to be display on site', 'express' ),
						  'default' => __( 'Express Shipping', 'express' )
						  ),						  
					);	
					//prices
					for($i=1;$i<=12;$i++)
					{
						$this->form_fields['district'.$i]=
						 array(
							'title' => __( 'District '.$i ),
							'type' => 'number',
							'description' => __( 'Shipping fee for District '.$i, 'express' ),
							'default' => 100
						  )	;
					}
					$this->form_fields['govap']=
						 array(
							'title' => __( 'Gò Vấp' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Gò Vấp', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['tanbinh']=
						 array(
							'title' => __( 'Tân Bình' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Tân Bình', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['tanphu']=
						 array(
							'title' => __( 'Tân Phú' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Tân Phú', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['phunhuan']=
						 array(
							'title' => __( 'Phú Nhuận' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Phú Nhuận', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['binhtan']=
						 array(
							'title' => __( 'Bình Tân' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Bình Tân', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['binhthanh']=
						 array(
							'title' => __( 'Bình Thạnh' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Bình Thạnh', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['binhchanh']=
						 array(
							'title' => __( 'Bình Chánh' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Bình Chánh', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['thuduc']=
						 array(
							'title' => __( 'Thủ Đức' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Thủ Đức', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['cuchi']=
						 array(
							'title' => __( 'Củ Chi' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Củ Chi', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['hocmon']=
						 array(
							'title' => __( 'Hóc Môn' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Hóc Môn', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['nhabe']=
						 array(
							'title' => __( 'Nhà Bè' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Nhà Bè', 'express' ),
							'default' => 100
						  )	;
					$this->form_fields['cangio']=
						 array(
							'title' => __( 'Cần Giờ' ),
							'type' => 'number',
							'description' => __( 'Shipping fee for Cần Giờ', 'express' ),
							'default' => 100
						  )	;
                }
 
                /**
                 * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
                 *
                 * @access public
                 * @param mixed $package
                 * @return void
                 */
                public function calculate_shipping( $package = array()) {		
					//if(isset($_POST['delivery_option']) && $_POST['delivery_option']=='express' ){
						$city = $package["destination"]["city"];
						$disids = array(
							'Huyện Bình Chánh'=>'binhchanh',
							'Huyện Cần Giờ'=>'cangio',
							'Huyện Củ Chi'=>'cuchi',
							'Huyện Hóc Môn'=>'hocmon',
							'Huyện Nhà Bè'=>'nhabe',
							'Quận 1'=>'district1',
							'Quận 10'=>'district10',
							'Quận 11'=>'district11',
							'Quận 12'=>'district12',
							'Quận 2'=>'district2',
							'Quận 3'=>'district3',
							'Quận 4'=>'district4',
							'Quận 5'=>'district5',
							'Quận 6'=>'district6',
							'Quận 7'=>'district7',
							'Quận 8'=>'district8',
							'Quận 9'=>'district9',
							'Quận Bình Tân'=>'binhtan',
							'Quận Bình Thạnh'=>'binhthanh',
							'Quận Gò Vấp'=>'govap',
							'Quận Phú Nhuận'=>'phunhuan',
							'Quận Tân Bình'=>'tanbinh',
							'Quận Tân Phú'=>'tanphu',
							'Quận Thủ Đức'=>'thuduc',
						);
						$disid = $disids[$city];
						$cost = isset( $this->settings[$disid] ) ? $this->settings[$disid] : 0;
						//var_dump($package);
						$rate = array(
							'id' => $this->id,
							'label' => $this->title,
							'cost' =>$cost
						);
			
						$this->add_rate( $rate );
					//}	
                }
            }
        }
    }
 
    add_action( 'woocommerce_shipping_init', 'express_shipping_method' );
 
    function add_express_shipping_method( $methods ) {
        $methods[] = 'Express_Shipping_Method';
        return $methods;
    }
 
	add_filter( 'woocommerce_shipping_methods', 'add_express_shipping_method' );
	//add_filter('woocommerce_package_rates', 'set_express_shipping_method', 11, 2 );
	// function set_express_shipping_method( $rates, $package ) {
	// 	if(isset($_POST['delivery_option']) && $_POST['delivery_option']=='express' && $package["destination"]["state"]=='HO-CHI-MINH'){
			
	// 		$choosed = array();
	// 		foreach ( $rates as $rate_id => $rate ) {
	// 			if ( 'express' === $rate->method_id ) {
	// 				$choosed[ $rate_id ] = $rate;
	// 				break;
	// 			}
	// 		}
	// 		return ! empty( $choosed ) ? $choosed : $rates;
	// 	}
	// 	return  $rates;
	// }
}