<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://minionsolutions.ph/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin
 */

/**
 * option name for the nespresso slider
 */
global $nespresso_slider_option_name;
$nespresso_slider_option_name = 'nespresso_slider';

/**
 * option name for the nespresso positive cup
 */
global $nespresso_positive_cup_option_name;
$nespresso_positive_cup_option_name = 'nespresso_positive_cup';

/**
 * option name for the nespresso recipies
 */
global $nespresso_recipies_option_name;
$nespresso_recipies_option_name = 'nespresso_recipies';

/**
 * option name for the nespresso best sellers
 */
global $nespresso_best_sellers_option_name;
$nespresso_best_sellers_option_name = 'nespresso_best_sellers';

/**
 * option name for the nespresso product list
 */
global $nespresso_product_list_option_name;
$nespresso_product_list_option_name = 'nespresso_product_list';

/**
 * option name for the nespresso category banner
 */
global $nespresso_category_banner_option_name;
$nespresso_category_banner_option_name = 'nespresso_category_banner';

/**
 * option name for the nespresso navigation banner
 */
global $nespresso_navigation_banner_option_name;
$nespresso_navigation_banner_option_name = 'nespresso_navigation_banner';

/**
 * option name for the nespresso mobile slider
 */
global $nespresso_mobile_slider_option_name;
$nespresso_mobile_slider_option_name = 'nespresso_mobile_slider';

/**
 * option name for the nespresso left dynamic banner
 */
global $nespresso_left_dynamic_banner_option_name;
$nespresso_left_dynamic_banner_option_name = 'nespresso_left_dynamic_banner';
/**
 * option name for the nespresso right dynamic banner
 */
global $nespresso_right_dynamic_banner_option_name;
$nespresso_right_dynamic_banner_option_name = 'nespresso_right_dynamic_banner';
/**
 * option name for the nespresso other settings
 */
global $nespresso_other_settings_option_name;
$nespresso_other_settings_option_name = 'nespresso_other_settings';
/**
 * option name for the nespresso slider pop up
 */
global $nespresso_slider_pop_up_option_name;
$nespresso_slider_pop_up_option_name = 'nespresso_slider_pop_up';

/**
 * option name for the nespresso coys promo
 */
global $nespresso_coys_promo_option_name;
$nespresso_coys_promo_option_name = 'nespresso_coys_promo';

/**
 * option name for the nespresso coys discount
 */
global $nespresso_coys_discount_option_name;
$nespresso_coys_discount_option_name = 'nespresso_coys_discount';
/**
 * option name for the nespresso email header
 */
global $nespresso_email_header_option_name;
$nespresso_email_header_option_name = 'nespresso_email_header';

/**
 * option name for the nespresso email on hold order
 */
global $nespresso_email_on_hold_order_option_name;
$nespresso_email_on_hold_order_option_name = 'nespresso_email_on_hold_order';

/**
 * option name for the nespresso email processing order
 */
global $nespresso_email_processing_order_option_name;
$nespresso_email_processing_order_option_name = 'nespresso_email_processing_order';

/**
 * option name for the nespresso email completed order
 */
global $nespresso_email_completed_order_option_name;
$nespresso_email_completed_order_option_name = 'nespresso_email_completed_order';

/**
 * option name for the nespresso email reset password
 */
global $nespresso_email_reset_password_option_name;
$nespresso_email_reset_password_option_name = 'nespresso_email_reset_password';

/**
 * option name for the nespresso email new acount
 */
global $nespresso_email_new_account_option_name;
$nespresso_email_new_account_option_name = 'nespresso_email_new_account';

/**
 * option name for the nespresso pick up location
 */
global $nespresso_pickup_location_option_name;
$nespresso_pickup_location_option_name = 'nespresso_pickup_location';

/**
 * get the homepage products for best sellers
 *
 * @param array $product_ids
 * @return object
 */
function get_homepage_products($product_ids=[]) {

	global $wpdb;

	$products = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='product_type' AND (meta_value='%s' OR meta_value='Machine' OR meta_value='Accessory') ", 'Coffee') );

	foreach ( $products as $k => $product ) :

		$products[$k]->id = $product->post_id;

	    $products[$k]->name = get_the_title( $product->post_id );

	    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->post_id ), 'single-post-thumbnail' );

	    $product->image_url = $image ? $image[0] : '';

	endforeach;

	return $products;

} // get_homepage_products()

/**
 * save nespresso slider as an wp option
 *
 * @param  array  $post - post data
 * @return array
 */
function save_nespresso_slider(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_slider_option_name;

	$slide['id'] = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);
	$slide['content'] =  isset($post['content']) ? wp_kses_post($post['content']) : null;
	$slide['image_url']= sanitize_text_field( isset($post['image_url']) ? $post['image_url'] : null);
	$slide['link'] = sanitize_text_field( isset($post['link']) ? $post['link'] : null);
	$slide['type'] = sanitize_text_field( isset($post['type']) ? $post['type'] : null);

	$sliders = get_option($nespresso_slider_option_name);

	if ( !$sliders) :
		$slide['id'] = 1;
		$sliders[$slide['id']] = (object)$slide;
	else :
		if ( isset($sliders[$slide['id']]) ) :
			$sliders[$slide['id']] = (object)$slide;
		else :
			$slide['id'] = count($sliders)+1;
			$sliders[$slide['id']] = (object)$slide;
		endif;
	endif;

	ksort($sliders);

	try {
		update_option($nespresso_slider_option_name, $sliders);
		return $data;
	} catch (Exception $e) {
		return [];
	}

} // save_nespresso_slider()

/**
 * delete a slide from the options
 *
 * @param  array  $post - post data
 * @return array - what's left of the sliders
 */
function delete_nespresso_slider(array $post=[]) {

	global $nespresso_slider_option_name;

	$slide_id = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);

	$sliders = get_option($nespresso_slider_option_name);

	if ( !isset($sliders[$slide_id]) )
		return [];

	unset($sliders[$slide_id]);

	$updatedSliders = [];
	if ($sliders) :
		$i = 1;
		foreach ( $sliders as $id => $slide ) :
			$slide->id = $i++;
			$updatedSliders[$slide->id] = $slide;
		endforeach;
	endif;

	update_option($nespresso_slider_option_name, $updatedSliders);

	return $updatedSliders;
} // delete_nespresso_slider()

/**
 * get the nespresso slider
 *
 * @return array|false
 */
function get_nespresso_sliders() {

	global $nespresso_slider_option_name;

	return get_option($nespresso_slider_option_name);

} // get_nespresso_slider()

/**
 * save nespresso positive cup as an wp option
 *
 * @param  array  $post - post data
 * @return array
 */
function save_nespresso_positive_cup(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_positive_cup_option_name;

	$data['image_url'] = sanitize_text_field( isset($post['image_url']) ? $post['image_url'] : '');
	$data['link'] = sanitize_text_field( isset($post['link']) ? $post['link'] : '');
	$data['content'] =  isset($post['content']) ? $post['content'] : null;

	update_option($nespresso_positive_cup_option_name, (object)$data);

} //save_nespresso_positive_cup()

/**
 * get the nespresso positive_cup
 *
 * @return array|false
 */
function get_nespresso_positive_cup() {

	global $nespresso_positive_cup_option_name;

	return get_option($nespresso_positive_cup_option_name);

} // get_nespresso_positive_cup()


/**
 * save nespresso recipies as an wp option
 *
 * @param  array  $post - post data
 * @return array
 */
function save_nespresso_recipe(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_recipies_option_name;

	$recipe = [];
	$recipe['id'] = sanitize_text_field( isset($post['id']) ? $post['id'] : '');
	$recipe['title'] = sanitize_text_field( isset($post['title']) ? $post['title'] : '');
	$recipe['image_url'] = sanitize_text_field( isset($post['image_url']) ? $post['image_url'] : '');
	$recipe['link'] = sanitize_text_field( isset($post['link']) ? $post['link'] : '');
	$recipe['content'] =  isset($post['content']) ? $post['content'] : null;

	$recipies = get_option($nespresso_recipies_option_name);

	if ( !$recipies) :
		$recipe['id'] = 1;
		$recipies[$recipe['id']] = (object)$recipe;
	else :
		if ( isset($recipies[$recipe['id']]) ) :
			$recipies[$recipe['id']] = (object)$recipe;
		else :
			$recipe['id'] = count($recipies)+1;
			$recipies[$recipe['id']] = (object)$recipe;
		endif;
	endif;

	ksort($recipies);

	try {
		update_option($nespresso_recipies_option_name, $recipies);
		return $data;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_recipies()

/**
 * delete a slide from the options
 *
 * @param  array  $post - post data
 * @return array - what's left of the sliders
 */
function delete_nespresso_recipe(array $post=[]) {

	global $nespresso_recipies_option_name;

	$recipe_id = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);

	$recipies = get_option($nespresso_recipies_option_name);

	if ( !isset($recipies[$recipe_id]) )
		return [];

	unset($recipies[$recipe_id]);

	$updatedRecipies = [];
	if ($recipies) :
		$i = 1;
		foreach ( $recipies as $id => $recipe ) :
			$recipe->id = $i++;
			$updatedRecipies[$recipe->id] = $recipe;
		endforeach;
	endif;

	update_option($nespresso_recipies_option_name, $updatedRecipies);

	return $updatedRecipies;
} // delete_nespresso_recipe()

/**
 * get the nespresso recipies
 *
 * @return array|false
 */
function get_nespresso_recipies() {

	global $nespresso_recipies_option_name;

	return get_option($nespresso_recipies_option_name);

} // get_nespresso_recipies()


/**
 * save nespresso best sellers as an wp option
 *
 * @param  array  $post - post data
 * @return array
 */
function save_nespresso_best_sellers(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_best_sellers_option_name;

	$best_seller_ids = [];
	$best_seller_ids['top'] = ( isset($post['best_sellers_top_product_ids']) ? $post['best_sellers_top_product_ids'] : '');
	$best_seller_ids['middle'] = ( isset($post['best_sellers_middle_product_ids']) ? $post['best_sellers_middle_product_ids'] : '');
	$best_seller_ids['bottom'] = ( isset($post['best_sellers_bottom_product_ids']) ? $post['best_sellers_bottom_product_ids'] : '');

	try {
		update_option($nespresso_best_sellers_option_name, $best_seller_ids);
		return $data;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_best_sellers()

/**
 * get the nespresso best sellers
 *
 * @return array|false
 */
function get_nespresso_best_sellers() {

	global $nespresso_best_sellers_option_name;

	$best_sellers = get_option($nespresso_best_sellers_option_name);
	if ( !$best_sellers )
		return [];

	$return = (object)[];
	foreach ( $best_sellers as $position => $product_ids ) :

		if ( !$product_ids )
			continue;

		$products = [];
		foreach ( $product_ids as $k => $product_id ) :

			$product = (array)get_post($product_id);

			if ( !$product )
				continue;
			$pro= wc_get_product( $product_id );
			$products[$k] = $product = (object) array_merge($product,  get_post_meta($product_id) );
			if($pro && $pro->is_type( 'variable' ))
			{
				$sub  = $pro->get_available_variations();
				//dd($sub);
				//echo '<pre>',print_r($sub),'</pre>';
				//	echo $pro->variation_id; echo $pro->variation_sku;exit ;
				$products[$k]->_sku[0] = $sub[0]['sku'];
				$products[$k]->_price[0] = $sub[0]['display_price'];
				$products[$k]->_regular_price[0] = $sub[0]['display_regular_price'];
				$products[$k]->ID = $sub[0]['variation_id'];
			}
			else{
				$products[$k]->ID =$product_id;
				$products[$k]->_regular_price[0] = $pro->regular_price;
			}
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );

			$products[$k]->id = $product_id;
			$products[$k]->post_id = $product_id;
			$products[$k]->name = get_the_title($product_id);
			$products[$k]->image_url = $image ? $image[0] : '';
			$products[$k]->product_type = get_field( 'product_type', $product_id );;

		endforeach;

		$return->$position = $products;

	endforeach;

	return $return;

} // get_nespresso_recipies()


function save_nespresso_product_list(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_product_list_option_name;

	$product_list_ids = [];
	$product_list_ids['coffee'] = ( isset($post['product_list_coffe_categories']) ? $post['product_list_coffe_categories'] : '');
	$product_list_ids['accessory'] = ( isset($post['product_list_accessory_categories']) ? $post['product_list_accessory_categories'] : '');
	$product_list_ids['machine'] = ( isset($post['product_list_machine_product_ids']) ? $post['product_list_machine_product_ids'] : '');
	$product_list_ids['product'] = ( isset($post['product_list_product_ids']) ? $post['product_list_product_ids'] : '');

	try {
		update_option($nespresso_product_list_option_name, $product_list_ids);
		return $data;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_product_list()


/**
 * get the nespresso product list
 *
 * @return array|false
 */
function get_nespresso_product_list() {

	global $nespresso_product_list_option_name;

	$product_list = get_option($nespresso_product_list_option_name);

	return $product_list;

} // get_nespresso_product_list()


function save_nespresso_category_banner(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_category_banner_option_name;

	$category_banner = [];
	$category_banner['coffee_category_banner_image_url'] = sanitize_text_field( isset($post['coffee_category_banner_image_url_mobile']) ? $post['coffee_category_banner_image_url'] : '');
	$category_banner['coffee_category_banner_image_url_mobile'] = sanitize_text_field( isset($post['coffee_category_banner_image_url']) ? $post['coffee_category_banner_image_url_mobile'] : '');
	$category_banner['coffee_sidebar_image_url'] = sanitize_text_field( isset($post['coffee_sidebar_image_url']) ? $post['coffee_sidebar_image_url'] : '');
	$category_banner['coffee_sidebar_image_link'] = sanitize_text_field( isset($post['coffee_sidebar_image_link']) ? $post['coffee_sidebar_image_link'] : '');
	$category_banner['coffee_category_banner_link'] = sanitize_text_field( isset($post['coffee_category_banner_link']) ? $post['coffee_category_banner_link'] : '');

	$category_banner['machine_category_banner_image_url'] = sanitize_text_field( isset($post['machine_category_banner_image_url_mobile']) ? $post['machine_category_banner_image_url'] : '');
    $category_banner['machine_category_banner_image_url_mobile'] = sanitize_text_field( isset($post['machine_category_banner_image_url']) ? $post['machine_category_banner_image_url_mobile'] : '');
    $category_banner['machine_sidebar_image_url'] = sanitize_text_field( isset($post['machine_sidebar_image_url']) ? $post['machine_sidebar_image_url'] : '');
    $category_banner['machine_sidebar_image_link'] = sanitize_text_field( isset($post['machine_sidebar_image_link']) ? $post['machine_sidebar_image_link'] : '');
    $category_banner['machine_category_banner_link'] = sanitize_text_field( isset($post['machine_category_banner_link']) ? $post['machine_category_banner_link'] : '');

    $category_banner['accessory_category_banner_image_url'] = sanitize_text_field( isset($post['accessory_category_banner_image_url_mobile']) ? $post['accessory_category_banner_image_url'] : '');
    $category_banner['accessory_category_banner_image_url_mobile'] = sanitize_text_field( isset($post['accessory_category_banner_image_url']) ? $post['accessory_category_banner_image_url_mobile'] : '');
    $category_banner['accessory_sidebar_image_url'] = sanitize_text_field( isset($post['accessory_sidebar_image_url']) ? $post['accessory_sidebar_image_url'] : '');
    $category_banner['accessory_sidebar_image_link'] = sanitize_text_field( isset($post['accessory_sidebar_image_link']) ? $post['accessory_sidebar_image_link'] : '');
    $category_banner['accessory_category_banner_link'] = sanitize_text_field( isset($post['accessory_category_banner_link']) ? $post['accessory_category_banner_link'] : '');

    $category_banner['storelocator_category_banner_image_url'] = sanitize_text_field( isset($post['storelocator_category_banner_image_url_mobile']) ? $post['storelocator_category_banner_image_url'] : '');
    $category_banner['storelocator_category_banner_image_url_mobile'] = sanitize_text_field( isset($post['storelocator_category_banner_image_url']) ? $post['storelocator_category_banner_image_url_mobile'] : '');
    $category_banner['storelocator_category_banner_link'] = sanitize_text_field( isset($post['storelocator_category_banner_link']) ? $post['storelocator_category_banner_link'] : '');

    $category_banner['advisory_banner_image_url_mobile'] = sanitize_text_field( isset($post['advisory_banner_image_url_mobile']) ? $post['advisory_banner_image_url_mobile'] : '');
	$category_banner['advisory_banner_image_url'] = sanitize_text_field( isset($post['advisory_banner_image_url']) ? $post['advisory_banner_image_url'] : '');
	$category_banner['advisory_banner_status'] = sanitize_text_field( isset($post['advisory_banner_status']) ? $post['advisory_banner_status'] : '');
	$category_banner['advisory_banner_callback'] = sanitize_text_field( isset($post['advisory_banner_callback']) ? $post['advisory_banner_callback'] : '');

	//banner promotion name
	$category_banner['storelocator_category_banner_promo_name'] = sanitize_text_field( isset($post['storelocator_category_banner_promo_name']) ? $post['storelocator_category_banner_promo_name'] : '');
	$category_banner['coffee_category_banner_promo_name'] = sanitize_text_field( isset($post['coffee_category_banner_promo_name']) ? $post['coffee_category_banner_promo_name'] : '');
	$category_banner['machine_category_banner_promo_name'] = sanitize_text_field( isset($post['machine_category_banner_promo_name']) ? $post['machine_category_banner_promo_name'] : '');
	$category_banner['accessory_category_banner_promo_name'] = sanitize_text_field( isset($post['accessory_category_banner_promo_name']) ? $post['accessory_category_banner_promo_name'] : '');

	//sidebar banner promotion name
	$category_banner['coffee_sidebar_image_promo_name'] = sanitize_text_field( isset($post['coffee_sidebar_image_promo_name']) ? $post['coffee_sidebar_image_promo_name'] : '');
	$category_banner['machine_sidebar_image_promo_name'] = sanitize_text_field( isset($post['machine_sidebar_image_promo_name']) ? $post['machine_sidebar_image_promo_name'] : '');
	$category_banner['accessory_sidebar_image_promo_name'] = sanitize_text_field( isset($post['accessory_sidebar_image_promo_name']) ? $post['accessory_sidebar_image_promo_name'] : '');

	try {
		update_option($nespresso_category_banner_option_name, $category_banner);
		return $data;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_category_banner()


/**
 * get the nespresso category banner
 *
 * @return array|false
 */
function get_nespresso_category_banner() {

	global $nespresso_category_banner_option_name;

	$category_banner = get_option($nespresso_category_banner_option_name);

	return $category_banner;

} // get_nespresso_category_banner()


function save_nespresso_navigation_banner(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_navigation_banner_option_name;

	$navigation_banner = [];
	$navigation_banner['coffee_navigation_banner_image_url'] = sanitize_text_field( isset($post['coffee_navigation_banner_image_url']) ? $post['coffee_navigation_banner_image_url'] : '');
	$navigation_banner['coffee_navigation_banner_title'] = sanitize_text_field( isset($post['coffee_navigation_banner_title']) ? $post['coffee_navigation_banner_title'] : '');
	$navigation_banner['coffee_navigation_banner_link'] = sanitize_text_field( isset($post['coffee_navigation_banner_link']) ? $post['coffee_navigation_banner_link'] : '');
	$navigation_banner['machine_navigation_banner_image_url'] = sanitize_text_field( isset($post['machine_navigation_banner_image_url']) ? $post['machine_navigation_banner_image_url'] : '');
    $navigation_banner['machine_navigation_banner_title'] = sanitize_text_field( isset($post['machine_navigation_banner_title']) ? $post['machine_navigation_banner_title'] : '');
    $navigation_banner['machine_navigation_banner_link'] = sanitize_text_field( isset($post['machine_navigation_banner_link']) ? $post['machine_navigation_banner_link'] : '');
    $navigation_banner['accessory_navigation_banner_image_url'] = sanitize_text_field( isset($post['accessory_navigation_banner_image_url']) ? $post['accessory_navigation_banner_image_url'] : '');
    $navigation_banner['accessory_navigation_banner_title'] = sanitize_text_field( isset($post['accessory_navigation_banner_title']) ? $post['accessory_navigation_banner_title'] : '');
    $navigation_banner['accessory_navigation_banner_link'] = sanitize_text_field( isset($post['accessory_navigation_banner_link']) ? $post['accessory_navigation_banner_link'] : '');
    $navigation_banner['our_services_navigation_banner_image_url'] = sanitize_text_field( isset($post['our_services_navigation_banner_image_url']) ? $post['our_services_navigation_banner_image_url'] : '');
    $navigation_banner['our_services_navigation_banner_title'] = sanitize_text_field( isset($post['our_services_navigation_banner_title']) ? $post['our_services_navigation_banner_title'] : '');
    $navigation_banner['our_services_navigation_banner_link'] = sanitize_text_field( isset($post['our_services_navigation_banner_link']) ? $post['our_services_navigation_banner_link'] : '');
    $navigation_banner['contact_us_navigation_banner_image_url'] = sanitize_text_field( isset($post['contact_us_navigation_banner_image_url']) ? $post['contact_us_navigation_banner_image_url'] : '');
    $navigation_banner['contact_us_navigation_banner_title'] = sanitize_text_field( isset($post['contact_us_navigation_banner_title']) ? $post['contact_us_navigation_banner_title'] : '');
    $navigation_banner['contact_us_navigation_banner_link'] = sanitize_text_field( isset($post['contact_us_navigation_banner_link']) ? $post['contact_us_navigation_banner_link'] : '');

	try {
		update_option($nespresso_navigation_banner_option_name, $navigation_banner);
		return $data;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_navigation_banner()


/**
 * get the nespresso navigation banner
 *
 * @return array|false
 */
function get_nespresso_navigation_banner() {

	global $nespresso_navigation_banner_option_name;

	$navigation_banner = get_option($nespresso_navigation_banner_option_name);

	return $navigation_banner;

} // get_nespresso_navigation_banner()


function save_nespresso_mobile_slider(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_mobile_slider_option_name;

	$slide['id'] = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);
	$slide['content'] =  isset($post['content']) ? wp_kses_post($post['content']) : null;
	$slide['image_url']= sanitize_text_field( isset($post['image_url']) ? $post['image_url'] : null);
	$slide['link'] = sanitize_text_field( isset($post['link']) ? $post['link'] : null);
	$slide['type'] = sanitize_text_field( isset($post['type']) ? $post['type'] : null);

	$sliders = get_option($nespresso_mobile_slider_option_name);

	if ( !$sliders) :
		$slide['id'] = 1;
		$sliders[$slide['id']] = (object)$slide;
	else :
		if ( isset($sliders[$slide['id']]) ) :
			$sliders[$slide['id']] = (object)$slide;
		else :
			$slide['id'] = count($sliders)+1;
			$sliders[$slide['id']] = (object)$slide;
		endif;
	endif;

	ksort($sliders);

	try {
		update_option($nespresso_mobile_slider_option_name, $sliders);
		return $sliders;
	} catch (Exception $e) {
		return [];
	}

} // save_nespresso_slider()

/**
 * delete a slide from the options
 *
 * @param  array  $post - post data
 * @return array - what's left of the sliders
 */
function delete_nespresso_mobile_slider(array $post=[]) {

	global $nespresso_mobile_slider_option_name;

	$slide_id = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);

	$sliders = get_option($nespresso_mobile_slider_option_name);

	if ( !isset($sliders[$slide_id]) )
		return [];

	unset($sliders[$slide_id]);

	$updatedSliders = [];
	if ($sliders) :
		$i = 1;
		foreach ( $sliders as $id => $slide ) :
			$slide->id = $i++;
			$updatedSliders[$slide->id] = $slide;
		endforeach;
	endif;

	update_option($nespresso_mobile_slider_option_name, $updatedSliders);

	return $updatedSliders;
} // delete_nespresso_slider()

/**
 * get the nespresso slider
 *
 * @return array|false
 */
function get_nespresso_mobile_sliders() {

	global $nespresso_mobile_slider_option_name;

	return get_option($nespresso_mobile_slider_option_name);

} // get_nespresso_slider()

function save_nespresso_left_dynamic_banner(array $post=[]) {

	if (!$post || empty($post) )
		return [];
	global $nespresso_left_dynamic_banner_option_name;
	$slide['id'] = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);
	$slide['content'] =  isset($post['content']) ? wp_kses_post($post['content']) : null;
	$slide['image_url']= sanitize_text_field( isset($post['image_url']) ? $post['image_url'] : null);
	$slide['link'] = sanitize_text_field( isset($post['link']) ? $post['link'] : null);

	$sliders = get_option($nespresso_left_dynamic_banner_option_name);

	if ( !$sliders) :
		$slide['id'] = 1;
		$sliders[$slide['id']] = (object)$slide;
	else :
		if ( isset($sliders[$slide['id']]) ) :
			$sliders[$slide['id']] = (object)$slide;
		else :
			$slide['id'] = count($sliders)+1;
			$sliders[$slide['id']] = (object)$slide;
		endif;
	endif;

	ksort($sliders);

	try {
		update_option($nespresso_left_dynamic_banner_option_name, $sliders);
		return $sliders;
	} catch (Exception $e) {
		return [];
	}

} // save_nespresso_left_dynamic_banner()

/**
 * delete a slide from the options
 *
 * @param  array  $post - post data
 * @return array - what's left of the sliders
 */
function delete_nespresso_left_dynamic_banner(array $post=[]) {

	global $nespresso_left_dynamic_banner_option_name;

	$slide_id = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);

	$sliders = get_option($nespresso_left_dynamic_banner_option_name);

	if ( !isset($sliders[$slide_id]) )
		return [];

	unset($sliders[$slide_id]);

	$updatedSliders = [];
	if ($sliders) :
		$i = 1;
		foreach ( $sliders as $id => $slide ) :
			$slide->id = $i++;
			$updatedSliders[$slide->id] = $slide;
		endforeach;
	endif;

	update_option($nespresso_left_dynamic_banner_option_name, $updatedSliders);

	return $updatedSliders;
} // delete_nespresso_left_dynamic_banner()

/**
 * get the nespresso slider
 *
 * @return array|false
 */
function get_nespresso_left_dynamic_banners() {

	global $nespresso_left_dynamic_banner_option_name;

	return get_option($nespresso_left_dynamic_banner_option_name);

} // delete_nespresso_left_dynamic_banner()

function save_nespresso_right_dynamic_banner(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_right_dynamic_banner_option_name;

	$slide['id'] = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);
	$slide['content'] =  isset($post['content']) ? wp_kses_post($post['content']) : null;
	$slide['image_url']= sanitize_text_field( isset($post['image_url']) ? $post['image_url'] : null);
	$slide['link'] = sanitize_text_field( isset($post['link']) ? $post['link'] : null);

	$sliders = get_option($nespresso_left_dynamic_banner_option_name);

	if ( !$sliders) :
		$slide['id'] = 1;
		$sliders[$slide['id']] = (object)$slide;
	else :
		if ( isset($sliders[$slide['id']]) ) :
			$sliders[$slide['id']] = (object)$slide;
		else :
			$slide['id'] = count($sliders)+1;
			$sliders[$slide['id']] = (object)$slide;
		endif;
	endif;

	ksort($sliders);

	try {
		update_option($nespresso_right_dynamic_banner_option_name, $sliders);
		return $sliders;
	} catch (Exception $e) {
		return [];
	}
} // save_nespresso_left_dynamic_banner()

/**
 * save the nespresso othe settings
 *
 * @return array|false
 */

function save_nespresso_other_settings(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_other_settings_option_name;

	$other_settings = [];
	$other_settings['gwp_status'] = sanitize_text_field( isset($post['gwp_status']) ? $post['gwp_status'] : '');
	$other_settings['product_with_gwp'] = sanitize_text_field( isset($post['product_with_gwp']) ? $post['product_with_gwp'] : '');
	$other_settings['gwp_by_product_status'] = sanitize_text_field( isset($post['gwp_by_product_status']) ? $post['gwp_by_product_status'] : '');
	$other_settings['product_with_gwp_by_product'] = sanitize_text_field( isset($post['product_with_gwp_by_product']) ? $post['product_with_gwp_by_product'] : '');
	$other_settings['subtotal_tier'] = sanitize_text_field( isset($post['subtotal_tier']) ? $post['subtotal_tier'] : '');

	try {
		update_option($nespresso_other_settings_option_name, $other_settings);
		return $other_settings;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_other_settings()

/**
 * delete a slide from the options
 *
 * @param  array  $post - post data
 * @return array - what's left of the sliders
 */
function delete_nespresso_right_dynamic_banner(array $post=[]) {

	global $nespresso_right_dynamic_banner_option_name;

	$slide_id = sanitize_text_field( isset($post['id']) && $post['id'] ? $post['id'] : null);

	$sliders = get_option($nespresso_right_dynamic_banner_option_name);

	if ( !isset($sliders[$slide_id]) )
		return [];

	unset($sliders[$slide_id]);

	$updatedSliders = [];
	if ($sliders) :
		$i = 1;
		foreach ( $sliders as $id => $slide ) :
			$slide->id = $i++;
			$updatedSliders[$slide->id] = $slide;
		endforeach;
	endif;

	update_option($nespresso_right_dynamic_banner_option_name, $updatedSliders);

	return $updatedSliders;
} // delete_nespresso_left_dynamic_banner()

/**
 * get the nespresso slider
 *
 * @return array|false
 */
function get_nespresso_right_dynamic_banners() {

	global $nespresso_right_dynamic_banner_option_name;

	return get_option($nespresso_right_dynamic_banner_option_name);

} // delete_nespresso_right_dynamic_banner()

/**
 * get the nespresso other settings
 *
 * @return array|false
 */
function get_nespresso_other_settings() {

	global $nespresso_other_settings_option_name;

	$other_settings = get_option($nespresso_other_settings_option_name);

	return $other_settings;

} // get_nespresso_other_settings()

function save_nespresso_coys_promo(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_coys_promo_option_name;

	$coys_promo = [];
	$coys_promo['coys_promo_status'] = sanitize_text_field( isset($post['coys_promo_status']) ? $post['coys_promo_status'] : '');
	$coys_promo['main_product'] = sanitize_text_field( isset($post['main_product']) ? json_encode($post['main_product']) : '');
	$coys_promo['conditional_product'] = sanitize_text_field( isset($post['conditional_product']) ? json_encode($post['conditional_product']) : '');
	$coys_promo['conditional_product_quantity'] = sanitize_text_field( isset($post['conditional_product_quantity']) ? json_encode($post['conditional_product_quantity']) : '');
	$coys_promo['gift'] = sanitize_text_field( isset($post['gift']) ? json_encode($post['gift']): '');

	try {
		update_option($nespresso_coys_promo_option_name, $coys_promo);
		return $coys_promo;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_coys_promo()

function get_nespresso_coys_promo() {

	global $nespresso_coys_promo_option_name;

	$coys_promo = get_option($nespresso_coys_promo_option_name);

	return $coys_promo;

} // get_nespresso_coys_promo()

/**
 * save the nespresso othe settings
 *
 * @return array|false
 */

function save_nespresso_slider_pop_up(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_slider_pop_up_option_name;

	$slider_pop_up = [];
	$slider_pop_up['slider_pop_up_header'] = sanitize_text_field( isset($post['slider_pop_up_header']) ? $post['slider_pop_up_header'] : '');
	$slider_pop_up['slider_pop_up_content'] = sanitize_text_field( isset($post['slider_pop_up_content']) ? $post['slider_pop_up_content'] : '');

	try {
		update_option($nespresso_slider_pop_up_option_name, $slider_pop_up);
		return $slider_pop_up;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_slider_pop_up()

function get_nespresso_slider_pop_up() {

	global $nespresso_slider_pop_up_option_name;

	$slider_pop_up = get_option($nespresso_slider_pop_up_option_name);

	return $slider_pop_up;

} // get_nespresso_slider_pop_up()

function save_nespresso_coys_discount(array $post=[]) {

	if (!$post || empty($post) )
		return [];

	global $nespresso_coys_discount_option_name;

	$coys_discount = [];
	$coys_discount['coys_discount_status'] = sanitize_text_field( isset($post['coys_discount_status']) ? $post['coys_discount_status'] : '');
	$coys_discount['conditional_product'] = sanitize_text_field( isset($post['conditional_product']) ? $post['conditional_product'] : '');
	$coys_discount['discounted_product'] = sanitize_text_field( isset($post['discounted_product']) ? $post['discounted_product'] : '');
	$coys_discount['coys_discount_type'] = sanitize_text_field( isset($post['coys_discount_type']) ? $post['coys_discount_type'] : '');
	$coys_discount['coys_amount_percentage'] = sanitize_text_field( isset($post['coys_amount_percentage']) ? $post['coys_amount_percentage'] : '');
	$coys_discount['discount_label'] = sanitize_text_field( isset($post['discount_label']) ? $post['discount_label'] : '');

	try {
		update_option($nespresso_coys_discount_option_name, $coys_discount);
		return $coys_discount;
	} catch (Exception $e) {
		return [];
	}

} //save_nespresso_coys_discount()

function get_nespresso_coys_discount() {

	global $nespresso_coys_discount_option_name;

	$coys_discount = get_option($nespresso_coys_discount_option_name);

	return $coys_discount;

} // get_nespresso_coys_discount()

function save_nespresso_email_header(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $nespresso_email_header_option_name;

    $email_header = [];
    $email_header['email_header_image_url'] = sanitize_text_field( isset($post['email_header_image_url']) ? $post['email_header_image_url'] : '');
    $email_header['email_header_button_label'] = sanitize_text_field( isset($post['email_header_button_label']) ? $post['email_header_button_label'] : '');
    $email_header['email_header_button_link'] = sanitize_text_field( isset($post['email_header_button_link']) ? $post['email_header_button_link'] : '');


    try {
        update_option($nespresso_email_header_option_name, $email_header);
        return $email_header;
    } catch (Exception $e) {
        return [];
    }

} //save_nespresso_email_header()

function get_nespresso_email_header() {

    global $nespresso_email_header_option_name;

    $email_header = get_option($nespresso_email_header_option_name);

    return $email_header;

} // get_nespresso_email_header()

function save_nespresso_email_on_hold_order(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $nespresso_email_on_hold_order_option_name;

    $email = [];
    $email['email_headline'] = sanitize_text_field( isset($post['email_headline']) ? $post['email_headline'] : '');
    $email['email_content'] = isset($post['email_content']) ? json_encode($post['email_content']) : '';
    $email['email_footer'] =  isset($post['email_footer']) ? json_encode($post['email_footer']) : '';


    try {
        update_option($nespresso_email_on_hold_order_option_name, $email);
        return $email;
    } catch (Exception $e) {
        return [];
    }

} //save_nespresso_email_on_hold_order()

function get_nespresso_email_on_hold_order() {

    global $nespresso_email_on_hold_order_option_name;

    $email = get_option($nespresso_email_on_hold_order_option_name);

    return $email;

} // get_nespresso_email_on_hold_order()

function save_nespresso_email_processing_order(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $nespresso_email_processing_order_option_name;

    $email = [];
    $email['email_headline'] = sanitize_text_field( isset($post['email_headline']) ? $post['email_headline'] : '');
    $email['email_content'] = isset($post['email_content']) ? json_encode($post['email_content']) : '';
    $email['email_footer'] =  isset($post['email_footer']) ? json_encode($post['email_footer']) : '';


    try {
        update_option($nespresso_email_processing_order_option_name, $email);
        return $email;
    } catch (Exception $e) {
        return [];
    }

} //save_nespresso_email_processing_order()

function get_nespresso_email_processing_order() {

    global $nespresso_email_processing_order_option_name;

    $email = get_option($nespresso_email_processing_order_option_name);

    return $email;

} // get_nespresso_email_processing_order()

function save_nespresso_email_completed_order(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $nespresso_email_completed_order_option_name;

    $email = [];
    $email['email_headline'] = sanitize_text_field( isset($post['email_headline']) ? $post['email_headline'] : '');
    $email['email_content'] = isset($post['email_content']) ? json_encode($post['email_content']) : '';
    $email['email_footer'] =  isset($post['email_footer']) ? json_encode($post['email_footer']) : '';


    try {
        update_option($nespresso_email_completed_order_option_name, $email);
        return $email;
    } catch (Exception $e) {
        return [];
    }

} //save_nespresso_email_completed_order()

function get_nespresso_email_completed_order() {

    global $nespresso_email_completed_order_option_name;

    $email = get_option($nespresso_email_completed_order_option_name);

    return $email;

} // get_nespresso_email_completed_order()

function save_nespresso_email_reset_password(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $nespresso_email_reset_password_option_name;

    $email = [];
    $email['email_headline'] = sanitize_text_field( isset($post['email_headline']) ? $post['email_headline'] : '');
    $email['email_content'] = isset($post['email_content']) ? json_encode($post['email_content']) : '';
    $email['email_footer'] =  isset($post['email_footer']) ? json_encode($post['email_footer']) : '';


    try {
        update_option($nespresso_email_reset_password_option_name, $email);
        return $email;
    } catch (Exception $e) {
        return [];
    }

} //save_nespresso_email_reset_password()

function get_nespresso_email_reset_password() {

    global $nespresso_email_reset_password_option_name;

    $email = get_option($nespresso_email_reset_password_option_name);

    return $email;

} // get_nespresso_email_reset_password()

function save_nespresso_email_new_acount(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $nespresso_email_new_account_option_name;

    $email = [];
    $email['email_headline'] = sanitize_text_field( isset($post['email_headline']) ? $post['email_headline'] : '');
    $email['email_content'] = isset($post['email_content']) ? json_encode($post['email_content']) : '';
    $email['email_footer'] =  isset($post['email_footer']) ? json_encode($post['email_footer']) : '';


    try {
        update_option($nespresso_email_new_account_option_name, $email);
        return $email;
    } catch (Exception $e) {
        return [];
    }

} //save_nespresso_email_new_acount()

function get_nespresso_email_new_account() {

    global $nespresso_email_new_account_option_name;

    $email = get_option($nespresso_email_new_account_option_name);

    return $email;

} // get_nespresso_email_new_account()

function save_nespresso_pickup_location(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $nespresso_pickup_location_option_name;
    $location = [];
    $location['address'] = sanitize_text_field( isset($post['address']) ? json_encode($post['address']) : '');
    $location['city'] = sanitize_text_field( isset($post['city']) ? json_encode($post['city']) : '');
    $location['province'] = sanitize_text_field( isset($post['province']) ? json_encode($post['province']) : '');
    $location['contact'] = sanitize_text_field( isset($post['contact']) ? json_encode($post['contact']) : '');

    try {
        update_option($nespresso_pickup_location_option_name, $location);
        return $location;
    } catch (Exception $e) {
        return [];
    }

} //save_nespresso_pickup_location()

function get_nespresso_pickup_location() {

    global $nespresso_pickup_location_option_name;

    $location = get_option($nespresso_pickup_location_option_name);
    return $location;

} // get_nespresso_pickup_location()

function get_nespresso_show_price($old,$new) {
	if($new==$old)
	{
		return '<span class="pro-price">'.wc_price($new).'</span>';
	}else{
		return '<span class="pro-price">'.wc_price($new).'</span>
			 <span class="pro-price old-price">'.wc_price($old).'</span>';
	}
} 
function get_nespresso_show_off($label,$class='pro-off') {	
	if($label)
	{
		return '<span class="'.$class.'">'.$label.'</span>';
	}else{
		return '';
	}
}