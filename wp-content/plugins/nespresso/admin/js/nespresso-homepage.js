
/**
 * nespresso-slider.js
 */

(function($) {

	$(document).ready( function() {
		$('#btn-add-new-slide').on('click', function() {
			$().hideSliderTableListShowForm();
		});

		$('#btn-add-new-recipe').on('click', function() {
			$().hideRecipeTableListShowForm();
		});

		$('#btn-close').on('click', function() {
			$().showSliderTableListHideForm();
		});

		$('#btn-close-recipe').on('click', function() {
			$().showRecipeTableListHideFormRecipe();
		});

		$('.btn-edit').on( 'click', function() {
			$().hideSliderTableListShowForm();
			$(this).populateForm();
		});

		$('.btn-edit-recipe').on( 'click', function() {
			$().hideRecipeTableListShowForm();
			$(this).populateFormRecipe();
		});

		$('[id=btn-add-dynamic-banner]').on('click', function(){
			var id = $(this).data('id');
			$('#form-'+id+'-dynamic-banner').removeClass('hide');
			$('#'+id+'-dynamic-banner-list-table').hide();
			$(this).hide();
		});

		$('[id=btn-close-left-dynamic-banner]').on('click', function(){
			$('#form-left-dynamic-banner').addClass('hide');
			$('#image-thumbnail-left-dynamic-banner').attr('src',null).addClass('hide');
			$('#image-url-left-dynamic-banner').val(null);
			$('#link-left-dynamic-banner').val(null);
			$('#id-left-dynamic-banner').val(null);
			$('#left-dynamic-banner-content').val(null);
			$('button[data-id=left]').show();
			$('#left-dynamic-banner-list-table').show();
		});

		$('[id=btn-close-right-dynamic-banner]').on('click', function(){
			$('#form-right-dynamic-banner').addClass('hide');
			$('#image-thumbnail-right-dynamic-banner').attr('src',null).addClass('hide');
			$('#image-url-right-dynamic-banner').val(null);
			$('#link-right-dynamic-banner').val(null);
			$('#id-right-dynamic-banner').val(null);
			$('#right-dynamic-banner-content').val(null);
			$('button[data-id=right]').show();
			$('#right-dynamic-banner-list-table').show();
		});

		$('.btn-open-wp-modal-left-dynamic-banner').on( 'click', function() {
			$(this).showWPMediaModal();
		});

		$('.btn-open-wp-modal-right-dynamic-banner').on( 'click', function() {
			$(this).showWPMediaModal();
		});

		$('.btn-delete-left-dynamic-banner').on('click', function(){
			var yesno = confirm('Do you want to delete this selected item?');
			if (yesno) {
				var id = $(this).data('id');
				var form = $('#form-left-dynamic-banner');
				form.find('#type-left-dynamic-banner').val('delete');
				form.find('#id-left-dynamic-banner').val(id);
				form.submit();
			}
		});

		$('.btn-delete-right-dynamic-banner').on('click', function(){
			var yesno = confirm('Do you want to delete this selected item?');
			if (yesno) {
				var id = $(this).data('id');
				var form = $('#form-right-dynamic-banner');
				form.find('#type-right-dynamic-banner').val('delete');
				form.find('#id-right-dynamic-banner').val(id);
				form.submit();
			}
		});

		$('.btn-edit-left-dynamic-banner').on('click', function(){
			var id = $(this).data('id');
			var data = $left_image[id];
			$('#left-dynamic-banner-list-table').hide();
			$('button[data-id=left]').hide();
			$('#image-url-left-dynamic-banner').val(data.image_url);
			$('#id-left-dynamic-banner').val(id);
			$('#link-left-dynamic-banner').val(data.link);
			$('#image-thumbnail-left-dynamic-banner').attr('src',data.image_url).removeClass('hide');
			$('#left-dynamic-banner-content').val(data.content);
			$('#form-left-dynamic-banner').removeClass('hide');
		});


		$('.btn-edit-right-dynamic-banner').on('click', function(){
			var id = $(this).data('id');
			var data = $right_image[id];
			$('#right-dynamic-banner-list-table').hide();
			$('button[data-id=right]').hide();
			$('#image-url-right-dynamic-banner').val(data.image_url);
			$('#id-right-dynamic-banner').val(id);
			$('#link-right-dynamic-banner').val(data.link);
			$('#image-thumbnail-right-dynamic-banner').attr('src',data.image_url).removeClass('hide');
			$('#right-dynamic-banner-content').val(data.content);
			$('#form-right-dynamic-banner').removeClass('hide');
		});

		$('#btn-add-new-mobile-slide').on('click', function(){
			$('#form-slider-mobile').removeClass('hide');
			$('#mobile-list-table').hide();
			$(this).hide();
		});

		$('#btn-close-mobile').on('click', function(){
			$('#form-slider-mobile').addClass('hide');
			$('#image-thumbnail-mobile').attr('src',null).addClass('hide');
			$('#image-url-mobile').val(null);
			$('#link-mobile').val(null);
			$('#type-slide-mobile').val(null);
			$('#id-mobile').val(null);
			$('#mobile-content').val(null);
			$('#btn-add-new-mobile-slide').show();
			$('#mobile-list-table').show();
		});

		$('.btn-open-wp-modal-mobile').on( 'click', function() {
			$(this).showWPMediaModal();
		});

		$('.btn-delete-mobile').on('click', function(){
			var yesno = confirm('Do you want to delete this selected item?');
			if (yesno) {
				var id = $(this).data('id');
				var form = $('#form-slider-mobile');
				form.find('#type-mobile').val('delete');
				form.find('#id-mobile').val(id);
				form.submit();
			}
		});

		$('.btn-edit-mobile').on('click', function(){
			var id = $(this).data('id');
			var data = $mobile[id];
			$('#mobile-list-table').hide();
			$('#btn-add-new-mobile-slide').hide();
			$('#image-url-mobile').val(data.image_url);
			$('#id-mobile').val(id);
			$('#link-mobile').val(data.link);
			$('#type-slide-mobile').val(data.type);
			$('#image-thumbnail-mobile').attr('src',data.image_url).removeClass('hide');
			$('#mobile-content').val(data.content);
			$('#form-slider-mobile').removeClass('hide');
		});

		$('.btn-delete').on('click', function() {
			var r = confirm("Do you want to delete the selected item?");

			if (r) {
				$(this).deleteSlide();
			}
			return;
		});

		$('.btn-delete-recipe').on('click', function() {
			var r = confirm("Do you want to delete the selected item?");

			if (r) {
				$(this).deleteRecipe();
			}
			return;
		});

		$('.btn-open-wp-modal').on( 'click', function() {
			$(this).showWPMediaModal();
		});
        $('.best-seller-select2').select2({
        	allowClear: true,
            placeholder: "Select product",
            templateSelection: addProductImage,
            containerCssClass : "select2-best-seller-top-select"
        });

		$.fn.deleteSlide = function() {

			var deleteButton = $(this);
			var id = deleteButton.parents('.slide-row').attr('data-id');

			var form = $('#form-slider');

			form.find('#action_type').val('delete');
			form.find('#id-slide').val(id);
			form.submit();

		}; // deleteSlide()

		$.fn.deleteRecipe = function() {

			var deleteButton = $(this);
			var id = deleteButton.parents('.recipe-row').attr('data-id');

			var form = $('#form-recipe');

			form.find('#type').val('delete');
			form.find('#id-recipe').val(id);
			form.submit();

		}; // deleteRecipe()

		/**
		 * populate the form for edit
		 */
		$.fn.populateForm = function() {

			var editButton = $(this);

			var id = editButton.parents('.slide-row').attr('data-id');

			try {
				var slide = $slides[id];
			} catch (e) {
				var slide = null;
			}

			// tmce_setContent(slide.content, 'content-slide');
			$('#id-slide').val(slide.id);
			$('#content-slide').val(slide.content);
			$('#link-slide').val(slide.link);
			$('#type-slide').val(slide.type);
			$('#image-url-slide').val(slide.image_url);
			$('#image-thumbnail-slide').attr('src',slide.image_url).removeClass('hide');

		}; // populateForm()


		/**
		 * populate the form for edit
		 */
		$.fn.populateFormRecipe = function() {

			var editButton = $(this);

			var id = editButton.parents('.recipe-row').attr('data-id');

			try {
				var recipe = $recipies[id];
			} catch (e) {
				var recipe = null;
			}

			tmce_setContent(recipe.content, 'content-recipe');
			$('#id-recipe').val(recipe.id);
			$('#title-recipe').val(recipe.title);
			$('#link-recipe').val(recipe.link);
			$('#image-url-recipe').val(recipe.image_url);
			$('#image-thumbnail-recipe').attr('src',recipe.image_url).removeClass('hide');

		}; // populateFormRecipe()

		$.fn.hideSliderTableListShowForm = function() {
			$('#btn-add-new-slide').addClass('hide');
			$('#slider-list-table').addClass('hide');
			$('#form-slider').removeClass('hide');
		}; // hideSliderTableListShowForm()

		$.fn.hideRecipeTableListShowForm = function() {
			$('#btn-add-new-recipe').addClass('hide');
			$('#recipies-list-table').addClass('hide');
			$('#form-recipe').removeClass('hide');
		}; // hideRecipeTableListShowForm()

		$.fn.showSliderTableListHideForm = function() {
			$('#btn-add-new-slide').removeClass('hide');
			$('#slider-list-table').removeClass('hide');
			$('#form-slider').addClass('hide');
			$('#image-thumbnail-slide').addClass('hide');
			tmce_setContent('', 'content', 'content');
			$('#link-slide').val(null);
			$('#type-slide').val(null);
			$('#image-url-slide').val(null);
			$('#image-thumbnail-slide').attr('src', null);
		}; // showSliderTableListHideForm()

		$.fn.showRecipeTableListHideFormRecipe = function() {
			$('#btn-add-new-recipe').removeClass('hide');
			$('#recipies-list-table').removeClass('hide');
			$('#form-recipe').addClass('hide');
			$('#image-thumbnail-recipe').addClass('hide');
			tmce_setContent('', 'content-recipe');
			$('#title-recipe').val(null);
			$('#link-recipe').val(null);
			$('#image-url-recipe').val(null);
			$('#image-thumbnail-recipe').attr('src', null);
		}; // showRecipeTableListHideFormRecipe()

		$.fn.enableAddNewButton = function() {
			$('#btn-add-new-slide').attr('disabled', false);
		}; // enableAddNewButton()

		$.fn.disableAddNewButton = function() {
			$('#btn-add-new-slide').attr('disabled', true);
		}; // disableAddNewButton()

		/**
		 * show the wp media modal in selecting images
		 */
		$.fn.showWPMediaModal = function() {

			var button = $(this);

			// Accepts an optional object hash to override default values.
			var frame = new wp.media.view.MediaFrame.Select({
				// Modal title
				title: 'Select Image',

				// Enable/disable multiple select
				multiple: false,

				// Library WordPress query arguments.
				library: {
					order: 'ASC',

					// [ 'name', 'author', 'date', 'title', 'modified', 'uploadedTo',
					// 'id', 'post__in', 'menuOrder' ]
					orderby: 'title',

					// mime type. e.g. 'image', 'image/jpeg'
					type: 'image',

					// Searches the attachment title.
					search: null,

					// Attached to a specific post (ID).
					uploadedTo: null
				},

				button: {
					text: 'Select'
				}
			});

			// Fires after the frame markup has been built, but not appended to the DOM.
			// @see wp.media.view.Modal.attach()
			frame.on( 'ready', function() {} );

			// Fires when the frame's $el is appended to its DOM container.
			// @see media.view.Modal.attach()
			frame.on( 'attach', function() {} );

			// Fires when the modal opens (becomes visible).
			// @see media.view.Modal.open()
			frame.on( 'open', function() {} );

			// Fires when the modal closes via the escape key.
			// @see media.view.Modal.close()
			frame.on( 'escape', function() {} );

			// Fires when the modal closes.
			// @see media.view.Modal.close()
			frame.on( 'close', function() {} );

			// Fires when a user has selected attachment(s) and clicked the select button.
			// @see media.view.MediaFrame.Post.mainInsertToolbar()
			frame.on( 'select', function() {
				var selectionCollection = frame.state().get('selection');

				var attachments = selectionCollection.map( function( attachment ) {
				  attachment = attachment.toJSON();
				  return attachment;
				});

				var attachment = attachments ? attachments[0] : null;

				if ( !attachment )
					return;

				var section = "-" + button.attr('data-section');

				var imageInput = $('#image-url' + section);
				var imageDisplay = $('#image-thumbnail' + section);
				imageInput.val(attachment.url);
				imageDisplay.attr('src', attachment.url);
				imageDisplay.removeClass('hide');

			} );

			// Fires when a state activates.
			frame.on( 'activate', function() {} );

			// Fires when a mode is deactivated on a region.
			frame.on( '{region}:deactivate', function() {} );
			// and a more specific event including the mode.
			frame.on( '{region}:deactivate:{mode}', function() {} );

			// Fires when a region is ready for its view to be created.
			frame.on( '{region}:create', function() {} );
			// and a more specific event including the mode.
			frame.on( '{region}:create:{mode}', function() {} );

			// Fires when a region is ready for its view to be rendered.
			frame.on( '{region}:render', function() {} );
			// and a more specific event including the mode.
			frame.on( '{region}:render:{mode}', function() {} );

			// Fires when a new mode is activated (after it has been rendered) on a region.
			frame.on( '{region}:activate', function() {} );
			// and a more specific event including the mode.
			frame.on( '{region}:activate:{mode}', function() {} );

			// Get an object representing the current state.
			frame.state();

			// Get an object representing the previous state.
			frame.lastState();

			// Open the modal.
			frame.open();

		}; // showWPMediaModal()

		/**
		 * add product image to select2
		 */
		function addProductImage (opt) {
		    if (!opt.id) {
		        return opt.text;
		    }
		    var optimage = $(opt.element).data('image');
		    if(!optimage){
		        return opt.text;
		    } else {
		        var $opt = $(
		        '<span><img src="' + optimage + '" class="select2-product-image" /> ' + $(opt.element).text() + '</span>'
		        );
		        return $opt;
		    }
		} // addUserPic()


	}); // (document).ready()

	$(document).on('change','.coffee_product_order', function(){

		//Get select array
		var values = $("select[name='product_list_coffe_categories[]']").map(function(){return $(this).val();}).get();

		//get selected index
	    var initialid = $(this).data('id');

	    //get future value
	    var change = $(this).val();
	    //get previous value
	    var initial = $(this).data('name');
	    var strIntitial = initial;

	    //change previous attr to new attr
		$(this).data('name', change);

		//mapping
		$.each(values, function(ind, val){
			var id = "coffee_"+ind;
			//swap value
			if (change === val && id !== initialid) {
				$('[data-id='+id+']').val(strIntitial);
				$('[data-id='+id+']').data('name', strIntitial);
				console.log(id, initialid);
			}
		});

		//display new select array
		values = $("select[name='product_list_coffe_categories[]']")
             .map(function(){return $(this).val();}).get();

	});

	$(document).on('change','.accessory_product_list', function(){

		//Get select array
		var values = $("select[name='product_list_accessory_categories[]']").map(function(){return $(this).val();}).get();

		//get selected index
	    var initialid = $(this).data('id');

	    //get future value
	    var change = $(this).val();
	    //get previous value
	    var initial = $(this).data('name');
	    var strIntitial = initial;

	    //change previous attr to new attr
		$(this).data('name', change);

		//mapping
		$.each(values, function(ind, val){
			//swap value
			var id = "acc_"+ind;

			if (change === val && id !== initialid) {
				$('[data-id='+id+']').val(strIntitial);
				$('[data-id='+id+']').data('name', strIntitial);
			}
		});

		//display new select array
		values = $("select[name='product_list_accessory_categories[]']")
             .map(function(){return $(this).val();}).get();

	});

	$(document).on('change','.machine_product_lists', function(){

		//Get select array
		var values = $("select[name='product_list_machine_product_ids[]']").map(function(){return $(this).val();}).get();

		//get selected index
	    var initialid = $(this).data('id');

	    //get future value
	    var change = $(this).val();
	    //get previous value
	    var initial = $(this).data('name');
	    var strIntitial = initial;

	    //change previous attr to new attr
		$(this).data('name', change);
		//mapping
		$.each(values, function(ind, val){
			var id = "machine_"+ind;
			//swap value
			if (change === val && id !== initialid) {
				$('[data-id='+id+']').val(strIntitial);
				$('[data-id='+id+']').data('name', strIntitial);
			}
		});

		//display new select array
		values = $("select[name='product_list_machine_product_ids[]']")
             .map(function(){return $(this).val();}).get();

	});
})(jQuery);

function tmce_setContent(content, editor_id, textarea_id) {
  if ( typeof editor_id == 'undefined' ) editor_id = wpActiveEditor;
  if ( typeof textarea_id == 'undefined' ) textarea_id = editor_id;

  if ( jQuery('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id) ) {
    return tinyMCE.get(editor_id).setContent(content);
  }else{
    return jQuery('#'+textarea_id).val(content);
  }
} // tmce_setContent()

