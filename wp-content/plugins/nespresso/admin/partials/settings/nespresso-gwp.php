<form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
    method="post"
    role="form"
    id="form-other-settings-pages"
>

    <div class="panel panel-default">

        <div class="panel-body">

            <input type="hidden" name="action" value="nespresso_other_settings">

            <input type="hidden" name="type" value="update-or-create">

            <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

            <!-- <h4>GWP</h4> -->

            <div class="form-group">
                <h2>GWP Promo by Subtotal</h2>
                <label for="gwp_status">Status:</label>
                <br>
                <select name="gwp_status" class="form-control" required>
                    <option value="Inactive" <?php echo (isset($other_settings['gwp_status']) && $other_settings['gwp_status'] == 'Inactive') ? 'selected' : '' ?> >Inactive</option>
                    <option value="Active" <?php echo (isset($other_settings['gwp_status']) && $other_settings['gwp_status'] == 'Active') ? 'selected' : '' ?> >Active</option>
                </select>
            </div>
            <div class="form-group">
                <label for="product_with_gwp">Eligible product(s) for this promotion. SKU number should separated by comma: <small>e.g ITESSE001A1, ITESSE099A</small></label>
                <br>
                <textarea class="form-control" name="product_with_gwp" placeholder="ITESSE001A1, ITESSE099A"><?= @$other_settings['product_with_gwp'] ?></textarea>
            </div>
            <div class="form-group">
                <label for="subtotal_tier">Subtotal Tier. Tier amount should separated by comma: <small>e.g 50000, 100000</small></label>
                <br>
                <textarea class="form-control" required name="subtotal_tier" placeholder="50000, 100000"><?= @$other_settings['subtotal_tier'] ?></textarea>
            </div>
            <!-- <hr>
            <div class="form-group">
                <h2>GWP by Product</h2>
                <label for="gwp_by_product_status">Status:</label>
                <br>
                <select name="gwp_by_product_status" class="form-control" required>
                    <option value="Inactive" <?php echo (isset($other_settings['gwp_by_product_status']) && $other_settings['gwp_by_product_status'] == 'Inactive') ? 'selected' : '' ?> >Inactive</option>
                    <option value="Active" <?php echo (isset($other_settings['gwp_by_product_status']) && $other_settings['gwp_by_product_status'] == 'Active') ? 'selected' : '' ?> >Active</option>
                </select>
            </div>
            <div class="form-group">
                <label for="product_with_gwp_by_product">Eligible product(s) for this promotion. SKU number should separated by comma: <small>e.g ITESSE001A1, ITESSE099A</small></label>
                <br>
                <textarea class="form-control" name="product_with_gwp_by_product" placeholder="ITESSE001A1, ITESSE099A"><?= @$other_settings['product_with_gwp_by_product'] ?></textarea>
            </div> -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat">
                    Submit
                </button>
            </div>

        </div><!-- .panel-body -->

    </div><!-- .panel -->
</form>
