<?php

/**
 * Provide a admin slider area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));
$pickup_locations = get_nespresso_pickup_location();
$states = nespresso_get_states();
$cities = nespresso_get_cities();
?>

<div class="wrap">
    <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
    <?php if ( isset($_SESSION['nespresso_alert']) && isset($_SESSION['nespresso_alert']['message'])) : ?>
        <div class="alert alert-<?= $_SESSION['nespresso_alert']['type']; ?> alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?= $_SESSION['nespresso_alert']['header']; ?></strong> <?= $_SESSION['nespresso_alert']['message']; ?>
        </div>
    <?php unset($_SESSION['nespresso_alert']); endif; ?>
</div>



<div class="wrap pd-top-20 pd-bottom-20">


    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-other-settings-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_pickup_location">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">
                <button type="button" id="add_more_coy" class="btn btn-success btn-sm btn-flat pull-right">Add More</button>
                <br>
                <br>
                <div class="address-container">
                <?php
                    if($pickup_locations):
                        $pickup_locations['address'] = json_decode($pickup_locations['address']);
                        $pickup_locations['province'] = json_decode($pickup_locations['province']);
                        $pickup_locations['city'] = json_decode($pickup_locations['city']);
                        $pickup_locations['contact'] = json_decode(@$pickup_locations['contact']);
                        foreach ($pickup_locations['address'] as $key => $value): ?>
                            <div id="address-num-<?= $key ?>">
                                <hr>
                                <button type="button" data-id="<?= $key ?>" class="btn btn-danger btn-sm btn-flat pull-right remove_address">Remove</button>
                                <br>
                                <div class="form-group">
                                    <label for="address">Address<span class="required">*</span></label>
                                    <input required class="form-control" name="address[]" value="<?= $value ?>">
                                </div>
                                <div class="form-group">
                                    <label for="province">Province<span class="required">*</span></label>
                                    <select class="form-control select2" name="province[]">
                                        <?php foreach ( $states as $state ) : ?>
                                            <option value="<?= $state ?>" <?= $pickup_locations['province'][$key] == $state ? 'selected="selected"': '' ?>><?= $state ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="city">District<span class="required">*</span></label>
                                    <select class="form-control select2" name="city[]">
                                        <?php foreach ( $cities as $city ) : ?>
                                            <option value="<?= $city ?>" <?= $pickup_locations['city'][$key] == $city ? 'selected="selected"': '' ?>><?= $city ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="address">Contact<span class="required">*</span></label>
                                    <input required class="form-control" name="contact[]" value="<?= @$pickup_locations['contact'][$key] ?>">
                                </div>
                            </div>
                        <?php endforeach;
                    else: ?>
                        <div class="form-group">
                            <label for="address">Address<span class="required">*</span></label>
                            <input class="form-control" name="address[]">
                        </div>
                        <div class="form-group">
                            <label for="province">Province<span class="required">*</span></label>
                            <select class="form-control select2" name="province[]">
                                <?php foreach ( $states as $code => $name ) : ?>
                                    <option value="<?= $name ?>"><?= $name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="city">District<span class="required">*</span></label>
                            <select class="form-control select2" name="city[]">
                                <?php foreach ( $cities as $code => $name ) : ?>
                                    <option value="<?= $name ?>"><?= $name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="address">Contact<span class="required">*</span></label>
                            <input required class="form-control" name="contact[]">
                        </div>
                    <?php endif; ?>
                </div>
            </div><!-- .panel-body -->
        </div><!-- .panel -->
        <button type="submit" class="btn btn-primary btn-flat pull-right">
            Submit
        </button>
    </form>
</div>
<div style="display: none;">
    <div class="province-city">
        <div class="form-group">
            <label for="province">Province<span class="required">*</span></label>
            <select class="form-control additional_select" name="province[]" forcerequired>
                <option value="">Select Province</option>
                <?php foreach ( $states as $state ) : ?>
                    <option value="<?= $state ?>"><?= $state ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="city">District<span class="required">*</span></label>
            <select class="form-control additional_select" name="city[]" forcerequired>
                <option value="">Select District</option>
                <?php foreach ( $cities as $city ) : ?>
                    <option value="<?= $city ?>"><?= $city ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="address">Contact<span class="required">*</span></label>
            <input class="form-control" name="contact[]" forcerequired>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function () {
        jQuery('.select2').select2();
        var address_count = jQuery('.remove_address').length + 2;
        jQuery('#add_more_coy').on('click',function () {
            var province_city = $('.province-city').html();
            province_city = province_city.replace(/\additional_select/g, 'select2');
            province_city = province_city.replace(/\forcerequired/g, 'required');
            jQuery('.address-container').append(
                '<div id="address-num-'+address_count+'">'+
                '<hr>'+
                '<button type="button" data-id="'+address_count+'" class="btn btn-danger btn-sm btn-flat pull-right remove_address">Remove</button>'+
                '<br>'+
                '<div class="form-group">'+
                    '<label for="product_with_gwp">Address<span class="required">*</span></label>'+
                    '<input required class="form-control" name="address[]" >'+
                '</div>'+province_city+
                '</div>'
            );
            jQuery('.select2').select2();
        });
        jQuery(document).on('click','.remove_address', function(){
            var r = confirm("Are you sure you want to delete this?");
            if (r == true) {
                var data_id = jQuery(this).data('id');
                jQuery('#address-num-'+data_id).remove();
            }
        });
    })
</script>
