<?php

/**
 * Provide a admin slider area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.ph/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));
$email_header = get_nespresso_email_header();
$email_on_hold = get_nespresso_email_on_hold_order();
$email_processing = get_nespresso_email_processing_order();
$email_completed = get_nespresso_email_completed_order();
$email_password = get_nespresso_email_reset_password();
$email_account = get_nespresso_email_new_account();

?>

<div class="wrap">
    <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
</div>

<div class="wrap pd-top-20 pd-bottom-20">


    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-email-header-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_email_header">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4></h4>

                <div class="form-group">
                    <label for="email_header_image_url">Email Header</label>
                    <br>
                    <img class="<?= @$email_header['email_header_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" style="height: auto;" src="<?= @$email_header['email_header_image_url'] ;?>" id="image-thumbnail-email-header">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="email-header">Select Image</a>
                    <input type="hidden" name="email_header_image_url" required value="<?= @$email_header['email_header_image_url'] ;?>" id="image-url-email-header">
                </div>

                <div class="form-group">
                    <label for="email_header_button_label">Header Button Label</label>
                    <br>
                    <input type="text" class="form-control" name="email_header_button_label" required placeholder="Order Coffee" value="<?= @$email_header['email_header_button_label'] ?>">
                </div>

                <div class="form-group">
                    <label for="email_header_button_link">Header Button Link</label>
                    <br>
                    <input type="text" class="form-control" name="email_header_button_link" required  placeholder="http://www.nespresso.vn/coffee-list" value="<?= @$email_header['email_header_button_link'] ?>">
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-email-on-hold-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_email_on_hold_order">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4>Customer On Hold Order</h4>

                <div class="form-group">
                    <label for="email_headline">Headline</label>
                    <br>
                    <input type="text" class="form-control" name="email_headline" required placeholder="PAYMENT INSTRUCTIONS" value="<?= @$email_on_hold['email_headline'] ?>">
                </div>

                <div class="form-group">
                    <label for="email_content">Content</label>
                    <br>
                    <?php if (isset($email_on_hold['email_content']) && $email_on_hold['email_content']):
                        $on_hold_content = json_decode($email_on_hold['email_content']);
                    else:
                        $on_hold_content = "Please deposit your full payment within three (3) banking days from date of order confirmation to the account below.";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_content" required placeholder="Please deposit your full payment within three (3) banking days from date of order confirmation to the account below."><?= $on_hold_content ?></textarea>
                </div>

                <div class="form-group">
                    <label for="email_footer">Footer <small>You use can HTML Tags</small></label>
                    <br>
                    <?php if (isset($email_on_hold['email_footer']) && $email_on_hold['email_footer']):
                        $on_hold_footer = json_decode($email_on_hold['email_footer']);
                    else:
                        $on_hold_footer = "If you would like more information on your order, please contact our Coffee Specialists at 1900 633 474 from 9am to 7pm, Mondays to Saturdays. \n\n";
                        $$on_hold_footer .= "Thank you. \n";
                        $on_hold_footer  .= "Nespresso Club Vietnam";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_footer" required><?=$on_hold_footer?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-email-processing-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_email_processing_order">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4>Customer Processing Order</h4>

                <div class="form-group">
                    <label for="email_headline">Headline</label>
                    <br>
                    <input type="text" class="form-control" name="email_headline" required placeholder="ORDER CONFIRMATION" value="<?= @$email_processing['email_headline'] ?>">
                </div>

                <div class="form-group">
                    <label for="email_content">Content</label>
                    <br>
                    <?php if (isset($email_processing['email_content']) && $email_processing['email_content']):
                        $processing_content = json_decode($email_processing['email_content']);
                    else:
                        $processing_content = "Your order is now being processed.";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_content" required placeholder="Your order is now being processed."><?= $processing_content ?></textarea>
                </div>

                <div class="form-group">
                    <label for="email_footer">Footer <small>You use can HTML Tags</small></label>
                    <br>
                    <?php if (isset($email_processing['email_footer']) && $email_processing['email_footer']):
                        $processing_footer = json_decode($email_processing['email_footer']);
                    else:
                        $processing_footer = "If you would like more information on your order, please contact our Coffee Specialists at 1900 633 474 from 9am to 7pm, Mondays to Saturdays. \n\n";
                        $processing_footer .= "Thank you.\n\n";
                        $processing_footer .= "Nespresso Club Vietnam";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_footer" required ><?= $processing_footer ?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-email-completed-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_email_completed_order">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4>Customer Completed Order</h4>

                <div class="form-group">
                    <label for="email_headline">Headline</label>
                    <br>
                    <input type="text" class="form-control" name="email_headline" required placeholder="ORDER DELIVERED" value="<?= @$email_completed['email_headline'] ?>">
                </div>

                <div class="form-group">
                    <label for="email_content">Content</label>
                    <br>
                    <?php if (isset($email_completed['email_content']) && $email_completed['email_content']):
                        $completed_order_content = json_decode($email_completed['email_content']);
                    else:
                        $completed_order_content = "Your order has been delivered!";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_content" required placeholder="Your order has been delivered!"><?= $completed_order_content ?></textarea>
                </div>

                <div class="form-group">
                    <label for="email_footer">Footer <small>You use can HTML Tags</small></label>
                    <br>
                    <?php if (isset($email_completed['email_footer']) && $email_completed['email_footer']):
                        $completed_order_footer = json_decode($email_completed['email_footer']);
                    else:
                        $completed_order_footer = "If you would like more information on your order, please contact our Coffee Specialists at 1900 633 474 from 9am to 7pm, Mondays to Saturdays.\n\n";
                        $completed_order_footer = "Thank you.\n\n";
                        $completed_order_footer = "Nespresso Club Vietnam";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_footer" required ><?= $completed_order_footer ?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-email-reset-password-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_email_reset_password">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4>Reset Password</h4>

                <div class="form-group">
                    <label for="email_headline">Headline</label>
                    <br>
                    <input type="text" class="form-control" name="email_headline" required placeholder="Change Your Password" value="<?= @$email_password['email_headline'] ?>">
                </div>

                <div class="form-group">
                    <label for="email_content">Content</label>
                    <br>
                    <?php if (isset($email_password['email_content']) && $email_password['email_content']):
                        $reset_password_content =  json_decode($email_password['email_content']);
                    else:
                        $reset_password_content = "Your password request has been received.";
                     endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_content" required placeholder="Your password request has been received."><?= $reset_password_content ?></textarea>
                </div>

                <div class="form-group">
                    <label for="email_footer">Footer <small>You use can HTML Tags</small></label>
                    <br>
                    <?php if (isset($email_password['email_footer']) && $email_password['email_footer']):
                        $reset_password_footer = json_decode($email_password['email_footer']);
                    else:
                        $reset_password_footer = "If you need assistance, you may call our Coffee Specialists at 1900 633 474 from 9am to 7pm, Mondays to Saturdays.\n\n";
                        $reset_password_footer .= "Enjoy the ultimate coffee experience.\n\n";
                        $reset_password_footer .= "Nespresso Club Vietnam";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_footer" required ><?= $reset_password_footer ?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-email-new-account-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_email_new_account">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4>New Account</h4>

                <div class="form-group">
                    <label for="email_headline">Headline</label>
                    <br>
                    <input type="text" class="form-control" name="email_headline" required placeholder="REGISTRATION CONFIRMATION" value="<?= @$email_account['email_headline'] ?>">
                </div>

                <div class="form-group">
                    <label for="email_content">Content</label>
                    <br>
                    <?php if (isset($email_account['email_content']) && $email_account['email_content']):
                        $new_account_content = json_decode($email_account['email_content']);
                    else:
                        $new_account_content = "Welcome!\n\n";
                        $new_account_content = "You have just created your Nespresso Vietnam account.\n\n";
                        $new_account_content = "You can now login using your email address and password.\n\n";
                        $new_account_content = "Become a Nespresso Club Member when you purchase your first coffee, and take full advantage of exclusive offers, personalized services, and event invites.";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_content" required placeholder=""><?= $new_account_content ?></textarea>
                </div>

                <div class="form-group">
                    <label for="email_footer">Footer <small>You use can HTML Tags</small></label>
                    <br>
                    <?php if (isset($email_account['email_footer']) && $email_account['email_footer']):
                        $new_account_footer = json_decode($email_account['email_footer']);
                    else:
                        $new_account_footer = "Enjoy the ultimate coffee experience.\n\n";
                        $new_account_footer = "Nespresso Club Vietnam";
                    endif; ?>
                    <textarea style="height: 200px" class="form-control" name="email_footer" required ><?= $new_account_footer ?></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>
</div>
