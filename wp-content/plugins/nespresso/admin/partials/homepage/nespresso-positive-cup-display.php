<?php

/**
 * Provide a admin positive cup area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));

$positive_cup = get_nespresso_positive_cup();

if ( !$positive_cup ) :
    $positive_cup = (object)[];
    $positive_cup->image_url = '';
    $positive_cup->content = '';
    $positive_cup->link = '';
endif;

?>
<div class="wrap pd-top-20 pd-bottom-20" id="positive-cup">

    <h3>Positive Cup</h3>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        enctype="multipart/form-data"
        role="form"
        id="form-positive-cup"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_positive_cup">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <!-- image -->
                <div class="form-group">
                    <label for="image">Image:</label>
                    <br>
                    <img class="<?= $positive_cup->image_url ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= $positive_cup->image_url ;?>" id="image-thumbnail-positive-cup">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="positive-cup">Select Image</a>
                    <input type="hidden" name="image_url" value="<?= $positive_cup->image_url ;?>" id="image-url-positive-cup">
                </div>

                 <!-- content -->
                <div class="form-group">
                    <label for="content">Content:</label>
                    <?php wp_editor( str_replace('\\"', '"', $positive_cup->content) , 'content-positive-cup', $settings = array('textarea_name'=>'content') ); ?>
                </div>

                 <!-- link -->
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" name="link" id="link" value="<?= $positive_cup->link ?>" class="form-control">
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>
</div><!-- #positive-cup -->
