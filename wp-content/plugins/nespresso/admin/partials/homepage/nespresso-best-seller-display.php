<?php

/**
 * Provide a admin positive cup area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.ph/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));

$products = get_homepage_products();

// get the best sellers
$best_sellers = get_nespresso_best_sellers();

$best_sellers_top_ids = [];
$best_sellers_middle_ids = [];
$best_sellers_bottom_ids = [];

// foreach ( $products as $product ) : //why best sellers looped in products?

//     if ( !$product)
//         continue;

//     foreach ( $best_sellers as $position => $best_seller) :

//         if ( !$best_sellers )
//             continue;

//         switch($position) :

//             case 'top':
//                 foreach ( $best_seller as $best_seller_product ) :
//                     if ( $product->post_id == $best_seller_product->ID)
//                         $best_sellers_top_ids[] = $product->post_id;
//                 endforeach;

//                 break;

//             case 'middle':
//                 foreach ( $best_seller as $best_seller_product ) :
//                     if ( $product->post_id == $best_seller_product->ID)
//                         $best_sellers_middle_ids[] = $product->post_id;
//                 endforeach;

//                 break;

//             case 'bottom':
//                 foreach ( $best_seller as $best_seller_product ) :
//                     if ( $product->post_id == $best_seller_product->ID)
//                         $best_sellers_bottom_ids[] = $product->post_id;
//                 endforeach;

//                 break;

//         endswitch;

//     endforeach;
// endforeach;

foreach ($best_sellers as $position => $best_seller):

    switch ($position):
        case 'top':
            foreach ( $best_seller as $best_seller_product ) :
                $best_sellers_top_ids[] = $best_seller_product->ID;
            endforeach;

            break;
        case 'middle':
            foreach ( $best_seller as $best_seller_product ) :
                $best_sellers_middle_ids[] = $best_seller_product->ID;
            endforeach;

            break;
        case 'bottom':
            foreach ( $best_seller as $best_seller_product ) :
                $best_sellers_bottom_ids[] = $best_seller_product->ID;
            endforeach;

            break;
    endswitch;

endforeach;


?>

<script type="text/javascript">

</script>

<div class="wrap pd-top-20 pd-bottom-20" id="best-seller">

    <h3>Best Seller</h3>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-best-sellers"
    >

        <input type="hidden" name="action" value="nespresso_best_sellers">

        <input type="hidden" name="type" value="update-or-create" id="type">

        <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

        <div class="form-group">
            <?php //echo "<pre>"; print_r($best_sellers_top_ids);die; ?>
            <label for="best_sellers_top_product_ids">Top 1</label>
            <select name="best_sellers_top_product_ids[0]" class="best-seller-select2" id="best-seller-top-select">
                <option>Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_top_ids[0] ? 'selected' : '' ?>

                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="best_sellers_top_product_ids">Top 2</label>
            <select name="best_sellers_top_product_ids[1]" class="best-seller-select2" id="best-seller-top-select">
                <option>Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_top_ids[1] ? 'selected' : '' ?>
                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="best_sellers_top_product_ids">Top 3</label>
            <select name="best_sellers_top_product_ids[2]" class="best-seller-select2" id="best-seller-top-select">
                <option>Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_top_ids[2] ? 'selected' : '' ?>
                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <br><hr><br>
        <div class="form-group">
            <label for="best_sellers_middle_product_ids">Middle 1</label>
            <select name="best_sellers_middle_product_ids[0]" class="best-seller-select2" id="best-seller-top-select">
                <option>Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_middle_ids[0] ? 'selected' : '' ?>
                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <label for="best_sellers_middle_product_ids">Middle 2</label>
            <select name="best_sellers_middle_product_ids[1]" class="best-seller-select2" id="best-seller-top-select">
                <option>Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_middle_ids[1] ? 'selected' : '' ?>
                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="best_sellers_middle_product_ids">Middle 3</label>
            <select name="best_sellers_middle_product_ids[2]" class="best-seller-select2" id="best-seller-top-select">
                <option value="">Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_middle_ids[2] ? 'selected' : '' ?>
                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <br><hr><br>
        <div class="form-group">
            <label for="best_sellers_bottom_product_ids">Bottom 1</label>
            <select name="best_sellers_bottom_product_ids[0]" class="best-seller-select2" id="best-seller-top-select">
                <option>Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_bottom_ids[0] ? 'selected' : '' ?>
                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="best_sellers_bottom_product_ids">Bottom 2</label>
            <select name="best_sellers_bottom_product_ids[1]" class="best-seller-select2" id="best-seller-top-select">
                <option>Select Product</option>
                <?php foreach ( $products as $product ) : ?>
                    <option value="<?= $product->post_id ?>"
                        data-image="<?= $product->image_url ?>"
                        <?= $product->post_id == @$best_sellers_bottom_ids[1] ? 'selected' : '' ?>
                    ><?= $product->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-flat pull-right">Submit</button>
        </div>

    </form>

</div><!-- #best-seller -->
