<?php
	$products = get_coffee_products();
	krsort( $products );
	
	$product_list_id_order = "";
	if(get_option( 'product_list_id_order' ))
	{
		$product_list_id_order = get_option( 'product_list_id_order' );
	}
	
	if($_POST)
	{
		
		add_option( 'product_list_id_order', wp_kses_post(json_encode($_POST['product_list_id_order'] )) );
		update_option( 'product_list_id_order', wp_kses_post( json_encode($_POST['product_list_id_order'] )) );
		
		$product_list_id_order = get_option( 'product_list_id_order' );
		
		echo "<meta http-equiv='refresh' content='0'>";
	}
	
	
	$arrays = get_nespresso_product_list();
	if ( ! $arrays ) {
		$categories = custom_acf_get_data( 'Product Details', 'category' );
	} else {
		$categories['choices'] = $arrays['coffee'];
	}
	
	foreach ( $categories['choices'] as $key => $categ ) {
		foreach ( $products as $category => $product ) {
			if ( $categ === $category ) {
				foreach ( $product as $k => $product )
				{
					$list[$category][] = $product;
				}
			}
		}
	}
	
	$product_list_id_order = json_decode($product_list_id_order);

echo "<pre>";
print_r($product_list_id_order);
echo "</pre>";

?>
<h3>Order Product for Category Page</h3>

<form action="#"
      method="post"
      role="form"
      id="product_list_id_order"
      name="product_list_id_order"
>
	<?php
		foreach ( $list as $category => $pro )
		{
			$count = count( $pro );
			?>
            <h3><?= $category ?></h3>
            <div class="row">
				<?php
					for ( $i = 0; $i < $count; $i ++ )
					{
						?>
                        <p class="form-control">
							<?php echo $i + 1;  ?> -
                            <select class="product_list_product_ids" name="product_list_id_order[<?php echo $category ?>][]" >
								<?php
									foreach ( $pro as $key => $val )
									{
										?>
                                        <option  value="<?= $val->product_id . ','. $val->name ?>" <?= $key ==  $i ? "selected" : ""; ?>  ><?= $val->name ?></option>
										<?php
									}
								?>
                            </select>
                        </p>
						<?php
						
					}
				?>
            </div>
			<?php
		}
	?>
    <div>
        <button type="submit" class="btn btn-primary btn-flat pull-right">Submit</button>
    </div>
</form>



