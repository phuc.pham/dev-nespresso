<?php

 $body ="<p>Dear team kế toán,</p>
 <p> Nespresso có đơn hàng yêu cầu xuất hóa đơn. Phiền team check và hỗ trợ với thông tin sau đây:</p>
 <p><strong>Red Invoice:</strong></p>
 <p><strong>Billing name:</strong> {red_name}</p>
 <p><strong>Billing address:</strong>{red_address} </p>
 <p><strong>VAT number:</strong> {vat_number} </p>

 <p><strong>Recipient Email:</strong> {red_email_invoice}</p>
 <p> Best regards,  </p>";

$settings = array(
  'tinymce'       => array(
      'setup' => 'function (ed) {
          tinymce.documentBaseURL = "' . get_admin_url() . '";
      }',
      
  ),
  'quicktags'     => TRUE,
  'editor_class'  => 'frontend-article-editor',
  'textarea_rows' => 25,
  'media_buttons' => TRUE,
);

$content = get_option('red_email_invoice_template');

if(empty($content))
{
  $content = $body;
}

// status
$status = false;
if(get_option( 'red_email_invoice_status' ))
{
  $status = true;
}
// subject email
$red_email_invoice_subject_email = "HĐ";
if(get_option( 'red_email_invoice_subject_email' ))
{
  $red_email_invoice_subject_email = get_option( 'red_email_invoice_subject_email' );
}
//send to emailcc
$red_email_invoice_email_cc = 'ty.dv@annam-professional.com,hieu.nvt@jacobins-digital.com';
if(get_option( 'red_email_invoice_email_cc' ))
{
  $red_email_invoice_email_cc = get_option( 'red_email_invoice_email_cc' );
}
//email goi den ke toan
$red_email_invoice_email_to = 'ty.dv@annam-professional.com,hieu.nvt@jacobins-digital.com';
if(get_option( 'red_email_invoice_email_to' ))
{
  $red_email_invoice_email_to = get_option( 'red_email_invoice_email_to' );
}




// when save/update wp_kses_post()
if($_POST)
{
  
  add_option('red_email_invoice_template', wp_kses_post($_POST['red_email_invoice_template']) );
  update_option('red_email_invoice_template', wp_kses_post($_POST['red_email_invoice_template']) );
  
  add_option('red_email_invoice_status', $_POST['red_email_invoice_status']);
  update_option('red_email_invoice_status', $_POST['red_email_invoice_status']);
  
  add_option('red_email_invoice_subject_email', $_POST['red_email_invoice_subject_email']);
  update_option('red_email_invoice_subject_email', $_POST['red_email_invoice_subject_email']);

  add_option('red_email_invoice_email_cc', $_POST['red_email_invoice_email_cc']);
  update_option('red_email_invoice_email_cc', $_POST['red_email_invoice_email_cc']);

  add_option('red_email_invoice_email_to', $_POST['red_email_invoice_email_to']);
  update_option('red_email_invoice_email_to', $_POST['red_email_invoice_email_to']);

  $settings = array(
    'tinymce'       => array(
        'setup' => 'function (ed) {
            tinymce.documentBaseURL = "' . get_admin_url() . '";
        }',
        
    ),
    'quicktags'     => TRUE,
    'editor_class'  => 'frontend-article-editor',
    'textarea_rows' => 25,
    'media_buttons' => TRUE,
  );

  $body ="<p>Dear team kế toán,</p>
  <p> Nespresso có đơn hàng yêu cầu xuất hóa đơn. Phiền team check và hỗ trợ với thông tin sau đây:</p>
  <p><strong>Red Invoice:</strong></p>
  <p><strong>Billing name:</strong> {red_name}</p>
  <p><strong>Billing address:</strong>{red_address} </p>
  <p><strong>VAT number:</strong> {vat_number} </p>

  <p><strong>Recipient Email:</strong> {red_email_invoice}</p>
  <p> Best regards,  </p>";

  $settings = array(
    'tinymce'       => array(
        'setup' => 'function (ed) {
            tinymce.documentBaseURL = "' . get_admin_url() . '";
        }',
    ),
    'quicktags'     => false,
    'editor_class'  => 'frontend-article-editor',
    'textarea_rows' => 25,
    'media_buttons' => false,
    // 'editor_css' => '',
    // 'tabindex' => '',
  );

  $content = get_option('red_email_invoice_template');

  if(empty($content))
  {
    $content = $body;
  }
  // status
  $status = false;
  if(get_option( 'red_email_invoice_status' ))
  {
    $status = true;
  }
  // subject email
  $red_email_invoice_subject_email = "HĐ";
  if(get_option( 'red_email_invoice_subject_email' ))
  {
    $red_email_invoice_subject_email = get_option( 'red_email_invoice_subject_email' );
  }
  //send to emailcc
  $red_email_invoice_email_cc = 'ty.dv@annam-professional.com,hieu.nvt@jacobins-digital.com';
  if(get_option( 'red_email_invoice_email_cc' ))
  {
    $red_email_invoice_email_cc = get_option( 'red_email_invoice_email_cc' );
  }
  //email goi den ke toan
  $red_email_invoice_email_to = 'ty.dv@annam-professional.com,hieu.nvt@jacobins-digital.com';
  if(get_option( 'red_email_invoice_email_to' ))
  {
    $red_email_invoice_email_to = get_option( 'red_email_invoice_email_to' );
  }

}

?>
 <form action="" method="post">
 <fieldset>
    <p class="description">Enable will send email for customer if customer require invoice.</p>
    <label for="red_email_invoice_status" style="margin: 20px">
        <?= $status ? 'Enable' : 'Disable' ?>
        <input class="" type="checkbox" name="red_email_invoice_status" id="red_email_invoice_status" <?= $status ? 'checked=checked' : '' ?> >
    </label>
  </fieldset>
  <fieldset>
    <p class="description">Subject in Email</p>
    <label for="red_email_invoice_subject_email" style="margin: 20px">
        <input class="" type="text" name="red_email_invoice_subject_email" id="red_email_invoice_subject_email" value="<?= $red_email_invoice_subject_email ? $red_email_invoice_subject_email : "HĐ" ?>" >
    </label>
  </fieldset>
  <fieldset>
    <p class="description">CC Email</p>
    <label for="red_email_invoice_email_cc" style="margin: 20px">
    <input class="" size="100%" type="text" name="red_email_invoice_email_cc" id="red_email_invoice_email_cc" value="<?= $red_email_invoice_email_cc ?>" >
    </label>
  </fieldset>
  <fieldset>
    <p class="description">Email</p>
    <label for="red_email_invoice_email_to" style="margin: 20px">
    <input class="" size="100%" type="text" name="red_email_invoice_email_to" id="red_email_invoice_email_to" value="<?= $red_email_invoice_email_to ?>" >
    </label>
  </fieldset>
<?php
  wp_editor( $content , 'red_email_invoice_template', $settings ); 
  submit_button('Save', 'primary');

  
?>
</form>

