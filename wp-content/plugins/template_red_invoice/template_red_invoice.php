<?php
/**
* Plugin Name: Template Red Invoice
* Plugin URI: https://www.nespresso.vn/
* Description: This is edit template red invoice plugin.
* Version: 1.0
* Author: nespresso
* Author URI: https://www.nespresso.vn/
**/
add_action( 'admin_menu', 'mfp_Add_My_Admin_Link' );
 
// Add a new top level menu link to the ACP
function mfp_Add_My_Admin_Link()
{
      add_menu_page(
        'Template Red Invoice', // Title of the page
        'Template Red Invoice', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'template_red_invoice/includes/template_red_invoice_page.php' // The 'slug' - file to display when clicking the link
    );
}

function scanwp_buttons( $buttons ) {
  array_unshift( $buttons, 'fontselect' ); 
  array_unshift( $buttons, 'fontsizeselect' ); 
  return $buttons;
}
add_filter( 'mce_buttons_2', 'scanwp_buttons' );

// Add Formats Dropdown Menu To MCE
if ( ! function_exists( 'wpex_style_select' ) ) {
function wpex_style_select( $buttons ) {
  array_push( $buttons, 'styleselect' );
  return $buttons;
}
}
add_filter( 'mce_buttons', 'wpex_style_select' );

// status
$invoice_status = false;
if(get_option( 'red_email_invoice_status' ))
{
  $invoice_status = true;
}


if ( is_plugin_active( 'template_red_invoice/template_red_invoice.php' ) && $invoice_status ) 
{
  //email red vat
  function woocommerce_order_status_completed_send_mail_invoice($order_id)
  {
    //email goi den ke toan
    $email = 'hieu.nvt@jacobins-digital.com';
    if(get_option( 'red_email_invoice_email_to' ))
    {
      $email = get_option( 'red_email_invoice_email_to' );
      $listmailto = explode(",", $email);
      $email = $listmailto[0];
    }
  
    $name = get_post_meta($order_id,'red_name',true);
    $address = get_post_meta($order_id,'red_address',true);
    $vat = get_post_meta($order_id,'vat_number',true);

    $recipientemail = '';

    if(get_option( 'red_email_invoice' ))
    {
      $recipientemail = get_option( 'red_email_invoice' );
    }

      if( $email && $name && $address && $vat && $recipientemail && filter_var($recipientemail, FILTER_VALIDATE_EMAIL))
      { 
          // subject email
          $red_email_invoice_subject_email = "HĐ";
          if(get_option( 'red_email_invoice_subject_email' ))
          {
            $red_email_invoice_subject_email = get_option( 'red_email_invoice_subject_email' );
          }
          
          //send to emailcc
          $red_email_invoice_email_cc = 'ty.dv@annam-professional.com,hieu.nvt@jacobins-digital.com';
          if(get_option( 'red_email_invoice_email_cc' ))
          {
            $red_email_invoice_email_cc = get_option( 'red_email_invoice_email_cc' );
          }

          $listmailcc = explode(",", $red_email_invoice_email_cc);
          $headers = array('Content-Type: text/html; charset=UTF-8');
          foreach($listmailcc as $cc)
          {
            $subject = preg_split('/(.*)(@.*)$/', $cc , -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $headers[] = 'Cc: '. $subject[0] .' <'. $cc .'>';
            //Cc: ty.dv <ty.dv@annam-professional.com>
          }

          $subject ="{$red_email_invoice_subject_email} #{$order_id} - $name";
          // $headers[] = 'Cc: ty.dv <ty.dv@annam-professional.com>';
          // $headers[] = 'Cc: hieu.nv <hieu.nvt@jacobins-digital.com>';
          $body = get_option('red_email_invoice_template');

          if(empty($body))
          {
              $body = "<p>Dear team kế toán,</p>
              <p> Nespresso có đơn hàng yêu cầu xuất hóa đơn. Phiền team check và hỗ trợ với thông tin sau đây:</p>
              <p><strong>Red Invoice:</strong></p>
              <p><strong>Billing name:</strong> {$name}</p>
              <p><strong>Billing address:</strong>  {$address} </p>
              <p><strong>VAT number:</strong>  {$vat} </p>​
              <p><strong>Recipient Email:</strong> {$recipientemail}  </p>
              <p> Best regards,  </p>";
          }else
          {
              $body = str_replace("{red_name}", $name , $body);
              $body = str_replace("{red_address}", $address , $body);
              $body = str_replace("{vat_number}", $vat, $body);
              $body = str_replace("{red_email_invoice}", $recipientemail, $body);
          }

          $content_type = apply_filters( 'wp_mail_content_type', $content_type );

          add_filter('wp_mail_content_type', function( $content_type ) {
              return 'text/html';
          });

      wp_mail($email,$subject,$body,$headers);
      }  
  }

  add_action( 'woocommerce_order_status_completed', 'woocommerce_order_status_completed_send_mail_invoice');

} 


