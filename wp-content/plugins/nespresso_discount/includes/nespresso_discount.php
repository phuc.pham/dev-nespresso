<?php

	$nespresso_discount_status = "";
	if(get_option( 'nespresso_discount_status' ))
	{
		$nespresso_discount_status = get_option( 'nespresso_discount_status' );
	}
	
	$nespresso_discount_percent = "";
	if(get_option( 'nespresso_discount_percent' ))
	{
		$nespresso_discount_percent = get_option( 'nespresso_discount_percent' );
	}
	
	$nespresso_condition_discount = "";
	if(get_option( 'nespresso_condition_discount' ))
	{
		$nespresso_condition_discount = get_option( 'nespresso_condition_discount' );
	}
	
	$body ='<p style="text-transform: uppercase;color: red;margin: 20px;font-size: large;">giam 20% neu mua 30 vien do , vo mua di !!! .</p>';
	
	$settings = array(
		'tinymce'       => array(
			'setup' => 'function (ed) {
            tinymce.documentBaseURL = "' . get_admin_url() . '";
        }',
		),
		'quicktags'     => false,
		'editor_class'  => 'frontend-article-editor',
		'textarea_rows' => 25,
		'media_buttons' => false,
		// 'editor_css' => '',
		// 'tabindex' => '',
	);
	
	$content = get_option('nespresso_discount_template');
	
	if(empty($content))
	{
		$content = $body;
	}
	
    if($_POST) {
     
	    add_option( 'nespresso_discount_status', wp_kses_post( $_POST['nespresso_discount_status'] ) );
	    update_option( 'nespresso_discount_status', wp_kses_post( $_POST['nespresso_discount_status'] ) );
	    
	    add_option( 'nespresso_discount_percent', wp_kses_post( $_POST['nespresso_discount_percent'] ) );
	    update_option( 'nespresso_discount_percent', wp_kses_post( $_POST['nespresso_discount_percent'] ) );
	    
	    add_option( 'nespresso_condition_discount', wp_kses_post( $_POST['nespresso_condition_discount'] ) );
	    update_option( 'nespresso_condition_discount', wp_kses_post( $_POST['nespresso_condition_discount'] ) );
	    
	    add_option( 'nespresso_discount_template', wp_kses_post( $_POST['nespresso_discount_template'] ) );
	    update_option( 'nespresso_discount_template', wp_kses_post( $_POST['nespresso_discount_template'] ) );
	    
	    
	
	    echo "<meta http-equiv='refresh' content='0'>";
    }
    

?>
 <form action="" method="post" id="nespresso_discount_form" name="nespresso_discount_form">
   <h1>Nespresso Discount Status</h1>
     <fieldset>
         <p class="description">Enable to discount when customer checkout </p>
         <label for="nespresso_discount_status" style="margin: 20px">
			 <?= $nespresso_discount_status ? 'Enable' : 'Disable' ?>
             <input class="" type="checkbox" name="nespresso_discount_status" id="nespresso_discount_status" <?= $nespresso_discount_status ? 'checked=checked' : '' ?> >
         </label>
     </fieldset>
     <hr>
     <hr>
     <fieldset>
         <p class="description">Percent Discount</p>
         <label for="nespresso_discount_percent" style="margin: 20px">
             <input class=""  type="number" name="nespresso_discount_percent" id="nespresso_discount_percent" value="<?= $nespresso_discount_percent ? $nespresso_discount_percent : "No Data" ?>" >
         </label>
     </fieldset>
     <fieldset>
         <p class="description">Condition Discount</p>
         <label for="nespresso_condition_discount" style="margin: 20px">
             <input class=""  type="number" name="nespresso_condition_discount" id="nespresso_condition_discount" value="<?= $nespresso_condition_discount ? $nespresso_condition_discount : "No Data" ?>" >
         </label>
     </fieldset>
     <fieldset>
         <p class="description">Discount Template</p>
	     <?php
		     wp_editor( $content , 'nespresso_discount_template', $settings );
	     ?>
     </fieldset>
	
	 
     
     <hr>
 
<?php
  submit_button('Save', 'primary');
?>
</form>

