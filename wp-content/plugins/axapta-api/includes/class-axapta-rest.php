<?php

use \Httpful\Request;
Class AXAPTA_REST
{

    public function __construct()
    {
        $this->init();

        if(
            $this->uri &&
			$this->port &&
            $this->consumer_key &&
            $this->consumer_secret
        ) {

            add_action( 'admin_axapta_create_customer', array($this,'axapta_create_customer' ));
            add_action( 'admin_axapta_update_customer', array($this,'axapta_update_customer' ));
            add_action( 'admin_axapta_create_sales_invoice', array($this,'axapta_create_sales_invoice' ));
            add_action( 'woocommerce_order_status_changed', array($this,'axapta_post_order' ), 10, 4 );

        }

    }

    private function init()
    {

        $axapta_settings       = $this->axapta_settings();

        //set all credentials
        $this->uri              = @$axapta_settings['uri'];
		$this->port              = @$axapta_settings['port'];
        $this->consumer_key     = @$axapta_settings['consumer_key'];
        $this->consumer_secret  = @$axapta_settings['consumer_secret'];

        // $this->userid           = @base64_encode($axapta_settings['userid']);
        // $this->signature        = @base64_encode($axapta_settings['signature']);
        // $this->storeid          = @base64_encode($axapta_settings['storeid']);

    }

    private function url_param()
    {

        $axapta_settings        = $this->axapta_settings();
        unset($axapta_settings['uri']);

        $url_str = '';
        foreach ($axapta_settings as $key => $value) {
            $url_str .= "&$key=$value";
        }
        return $url_str;
    }

    private function new_url_param()
    {

    }


    public function axapta_create_customer($user)
    {
        $token      = $this->request_token();

        if ($token) {
            $gender     = '';
            $user_meta  = get_user_meta($user->ID);

            if (isset($user_meta['title'][0]) && $user_meta['title'][0]) {
                if ($user_meta['title'][0] == 'Mr')
                    $gender = 'Male';
                if ($user_meta['title'][0] == 'Ms')
                    $gender = 'Female';
            }

            //add customer
            $customer_data = json_encode([
                'Address1'              => @$user_meta['billing_address_1'][0],
                'Address2'              => @$user_meta['billing_address_2'][0],
                'Address3'              => @$user_meta['billing_city'][0],
                'Country'               => @$user_meta['billing_country'][0],
                'PostalCode'            => @$user_meta['billing_postcode'][0],
                'PostalCode2'           => @$user_meta['billing_postcode'][0],
                'ShippingPostalCode'    => @$user_meta['shipping_postcode'][0],
                'ShippingCountry'       => @$user_meta['shipping_country'][0],
                'ShippingAddress'       => @$user_meta['shipping_address_1'][0],
                'ShippingAddress2'      => @$user_meta['shipping_address_2'][0],
                'ShippingCity'          => @$user_meta['shipping_city'][0],
                'CustomerCode'          => $user->ID,
                'IDNumber'              => $user->ID,
                'Email'                 => $user->user_email,
                'Surname'               => $user_meta['last_name'][0],
                'Name'                  => $user_meta['first_name'][0],
                'Sex'                   => $gender,
                'HandPhone'             => $user_meta['mobile'][0],
                'HomePhone'             => $user_meta['phone'][0],
                'Location'              => $this->location

            ]);

            $url        = $this->uri . 'customer/add';

            $customer = Request::post($url)
                ->sendsJson()
                ->addHeader('ApiAuth', $this->header)
                ->addHeader('Token', $token)
                ->expectsHtml()
                ->body($customer_data)
                ->send();


            $data_response = json_decode($customer);

            if ($data_response && isset($data_response->MembershipCode)) {
                update_user_meta( $user->ID, 'has_club_membership_no', sanitize_text_field(1));
                update_user_meta( $user->ID, 'club_membership_no', sanitize_text_field($data_response->MembershipCode));

                //add my machine
                if(isset($user_meta['my_machines'][0]) && $user_meta['my_machines'][0]) {
                    $my_machines = unserialize($user_meta['my_machines'][0]);
                    if ($my_machines) {
                        foreach (unserialize($user_meta['my_machines'][0]) as $machine) {
                            $this->add_machine($machine, $user->user_email, $data_response->MembershipCode, $token);
                        }
                    }
                }
            }

            //For Checking
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction: Created Customer CUSTOMER_ID = ' . $user->ID,
                    'test_value' => 'Response => ' . $customer  . ' Data sent => ' . $customer,
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );

        } else {
            //For Checking
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction: Created Customer CUSTOMER_ID = ' . $user->ID,
                    'test_value' => 'Invalid Token',
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );
        }
    }

    public function axapta_update_customer($user)
    {
        $token      = $this->request_token();

        if ($token) {
            $gender     = '';
            $user_meta  = get_user_meta($user->ID);

            if (isset($user_meta['title'][0]) && $user_meta['title'][0]) {
                if ($user_meta['title'][0] == 'Mr')
                    $gender = 'Male';
                if ($user_meta['title'][0] == 'Ms')
                    $gender = 'Female';
            }

            //add customer
            $customer_data = json_encode([
                'Address1'              => @$user_meta['billing_address_1'][0],
                'Address2'              => @$user_meta['billing_address_2'][0],
                'Address3'              => @$user_meta['billing_city'][0],
                'Country'               => @$user_meta['billing_country'][0],
                'PostalCode'            => @$user_meta['billing_postcode'][0],
                'PostalCode2'           => @$user_meta['billing_postcode'][0],
                'ShippingPostalCode'    => @$user_meta['shipping_postcode'][0],
                'ShippingCountry'       => @$user_meta['shipping_country'][0],
                'ShippingAddress'       => @$user_meta['shipping_address_1'][0],
                'ShippingAddress2'      => @$user_meta['shipping_address_2'][0],
                'ShippingCity'          => @$user_meta['shipping_city'][0],
                'CustomerCode'          => $user->ID,
                'IDNumber'              => $user->ID,
                'Email'                 => $user->user_email,
                'Surname'               => $user_meta['last_name'][0],
                'Name'                  => $user_meta['first_name'][0],
                'Sex'                   => $gender,
                'HandPhone'             => $user_meta['mobile'][0],
                'HomePhone'             => $user_meta['phone'][0],
                'Location'              => $this->location

            ]);

            $url        = $this->uri . 'customer/add';

            $customer = Request::post($url)
                ->sendsJson()
                ->addHeader('ApiAuth', $this->header)
                ->addHeader('Token', $token)
                ->expectsHtml()
                ->body($customer_data)
                ->send();

            $data_response = json_decode($customer);

            if ($data_response && isset($data_response->MembershipCode)) {
                update_user_meta( $user->ID, 'has_club_membership_no', sanitize_text_field(1));
                update_user_meta( $user->ID, 'club_membership_no', sanitize_text_field($data_response->MembershipCode));
            }

            //For Checking
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction: Update Customer CUSTOMER_ID = ' . $user->ID,
                    'test_value' => 'Response => ' . $customer,
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );

        }  else {
            //For Checking
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction: Update Customer CUSTOMER_ID = ' . $user->ID,
                    'test_value' => 'Invalid Token',
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );
        }
    }

    public function old_axapta_woocommerce_order_status_processing($order_id)
    {
        try{
            // great, no exceptions where thrown while creating the object
            $order = wc_get_order($order_id);
            // The Order data
            $order_data                 = $order->get_data();
            $order_product              = [];

            $total_discount_amount      = $order_data['discount_total'];
            $computed_discount_amount   = 0;
            $counter                    = 1;
            $count                      = count($order->get_items());

            //tax amount
            $net_sale_amount = $order->get_subtotal() - $order_data['discount_total'];
            $taxAmount = $net_sale_amount-($net_sale_amount/1.12);
            $customer_data = get_user_meta($order_data['customer_id']);


            $order_product[0]['CustomerCode']       = $order_data['customer_id'];
            $order_product[0]['Email']              = $customer_data['email'][0];
            $order_product[0]['cellphone']          = $customer_data['mobile'][0];
            $order_product[0]['address']            = $order_data['billing']['address_1'] . ' ' . $order_data['billing']['address_2'] . ' ' . $order_data['billing']['city'];
            $order_product[0]['DeliveryName']       = $order_data['shipping']['first_name'] . ' ' . $order_data['shipping']['last_name'];
            $order_product[0]['DeliveryAddress']    = $order->get_meta('delivery_address');
            $order_product[0]['Delivery City']      = $order->get_meta('delivery_city');
            $order_product[0]['Region']             = $order->get_meta('delivery_region');
            $order_product[0]['Mobile']             = $customer_data['mobile'][0];
            $order_product[0]['Payment']            = $order->get_payment_method_title();
            $order_product[0]['store']              = $this->storeid;
            $order_product[0]['Refoid']             = '';
            $order_product[0]['Salesman']           = 'NA';
            $order_product[0]['GrandTotalOrder']    = $order->get_total();


            $x = 1;
            foreach ( $order->get_items() as $key => $item ) {

                //get product details

                if($item['variation_id']) {
                    $_product = wc_get_product( $item['variation_id'] );
                } else {
                    $_product = wc_get_product( $item['product_id'] );
                }


                $discount_per_item                          = $item->get_subtotal() - $item->get_total();

                //last loop override the last discount amount into remaining amount discount amount (due to round issue)
                if ($counter == $count) {
                   $discount_per_item = round($total_discount_amount - $computed_discount_amount,2);
                }

                $computed_discount_amount                   += $discount_per_item;
                $item_net_amount                            = $item['subtotal'] - $discount_per_item;
                $taxAmount                                  = $item_net_amount-($item_net_amount/1.12);
                $discount_percentage                        = ($discount_per_item / $item['subtotal']) * 100;

                $order_product[$x]['ItemNumber']          = $_product->get_sku();
                $order_product[$x]['Quantity']            = $item['quantity'];
                $order_product[$x]['SalesPrice']          = $_product->get_price();
                $order_product[$x]['PriceAfterDisc']      = $_product->get_price();
                $order_product[$x]['NetAmount']           = $item_net_amount;
                $order_product[$x]['Amount']              = $_product->get_price();
                $order_product[$x]['LineDisc']            = $item_net_amount;
                $order_product[$x]['LinePercent']         = round($discount_percentage,2);
                $order_product[$x]['Barcode']             = '';
                $order_product[$x]['Notes']               = '';
                $order_product[$x]['ItemSalesTaxGroup']   = '';
                $order_product[$x]['TaxAmount']           = round($taxAmount,2);
                $order_product[$x]['DiscountAmount']      = '-'+round($discount_per_item,2);
                $order_product[$x]['RowTotal']            = $item_net_amount;

                $counter++;
                $x++;

            }

            $invoice_data = json_encode($order_product);

            $url        = $this->uri . '?action=makeorder' . $this->url_param();

            $invoice = Request::post($url)
                ->sendsJson()
                ->expectsHtml()
                ->body($order_product)
                ->send();

            //For Checking
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction: Created Invoice ORDER_ID = ' . $order_id,
                    'test_value' => 'Response => '.$invoice . ' Data sent => ' . $invoice_data,
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );
        } catch (\Exception $ex) {
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction FAILED: Created Invoice ORDER_ID = ' . $order_id,
                    'test_value' => 'Response => ' . $ex->getMessage(),
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );
        }

    }


    public function axapta_post_order($order_id, $from_status, $to_status, $order)
    {
        try{

            if ($to_status != "manual-processing")
                return;

            // great, no exceptions where thrown while creating the object
            $order      = wc_get_order($order_id);
            $states     = dhl_get_states();

            $order_data = $order->get_data();
            $customer_data = get_user_meta($order_data['customer_id']);

            $sale = [];
            $sale["Currency"]           = $order_data['currency'];
            $sale["CustomerRef"]        = $order_data['customer_id'];
            $sale["DeliveryMethod"]     = $order->get_meta('delivery_option');
            $sale["PaymentMethod"]      = $order_data['payment_method'];
            $sale["PriceInclTax"]       = $order_data['prices_include_tax'];
            $sale["DiscAmount"]         = $order_data['discount_total'];
            $sale["DeliveryFee"]        = $order_data['shipping_total'];
            $sale["PaymAmount"]         = $order_data['total'];
            $sale["FirstName"]          = $order_data['billing']['first_name'];
            $sale["LastName"]           = $order_data['billing']['last_name'];
            $sale["BillingName"]        = $order_data['billing']['company'];
            $sale["BillingAddress"]     = $order_data['billing']['company'] . ' ' .$order_data['billing']['address_1'] . ' ' . $order_data['billing']['address_2'] . ' ' . $order_data['billing']['city'] . ' ' . $order_data['billing']['state'];
            $sale["Delivery"]           = [
                  "Street"              => $order->get_shipping_company() . ' ' . $order->get_shipping_address_1() . ' ' .$order->get_shipping_address_2(),
                  "District"            => $order->get_shipping_city(),
                  "PostalCode"          => $order_data['shipping']['postcode'],
                  "Receiver"            => $order->get_shipping_first_name() . ' ' .$order->get_shipping_last_name(),
            ];
            if ($order->get_meta('delivery_option') == 'pick_up') {
                $nespresso_states           = nespresso_get_states();
                $sale['Phone']              = $order->get_meta('_shipping_mobile');
                $sale["Delivery"]["City"]   = $states[array_search($order->get_meta('delivery_region'), $nespresso_states)];
            } else {
                $sale["Delivery"]["City"]   = $states[$order->get_shipping_state()];
                //$sale['Phone']              = $order->get_meta('_shipping_country_code')=='+84'?$order->get_meta('_shipping_mobile'):$order->get_meta('_shipping_country_code').substr($order->get_meta('_shipping_mobile'),1);//str_replace('+', '0', $order->get_meta('_shipping_country_code')) . substr($order->get_meta('_shipping_mobile'), 1);
				$sale['Phone']              = $order->get_meta('_shipping_mobile');
            }
			if($order->get_meta('vat_number'))
			{
				$sale['VATInvoice']         = array(
					'InvoiceAddress'=>$order->get_meta('red_address'),
					'InvoiceName'=>$order->get_meta('red_name'),
					'TaxNumber'=>$order->get_meta('vat_number'),
				);
			}
            $sale['Email']              = $customer_data['email'][0];
            $sale['DlvComment']         = $order_data['customer_note'];
            $sale['InventWarehouse']    = "";

            // Line items
            $x = 0;
            $order_product = [];
            foreach ( $order->get_items() as $key => $item ) {
                
                $product_id = $item['product_id'];
                $terms = get_the_terms( $product_id, 'product_cat' );
               
                foreach ( $terms as $term ) {
                    $product_cat_slug = $term->slug;
                }

                //get product details

                if($item['variation_id']) {
                    $_product = wc_get_product( $item['variation_id'] );
                } else {
                    $_product = wc_get_product( $item['product_id'] );
                }
               
                if(!$_product->get_sku())
                    continue;
                $bundleprice = $item->get_meta('_woosb_price');
                    //dd($item_meta_data);
                $order_product[$x]['ItemId']        = $_product->get_sku();
                $order_product[$x]['ItemName']      = $_product->get_title();  

				$item_sale_price                    = $_product->get_regular_price();
                if ($product_cat_slug == 'coffee') {

                    $order_product[$x]['SalesQty']      = $item['quantity']/10;
                    $order_product[$x]['SalesPrice']    = $item_sale_price*10;

                } else {
                    $order_product[$x]['SalesPrice']    = $item_sale_price;
                    $order_product[$x]['SalesQty']      = $item['quantity'];

                }

                $order_product[$x]['LineAmount']    = $item_sale_price*$item['quantity'];

                 //hieu fix bundle
                 if($bundleprice)
                 {
                     if($item_sale_price > $bundleprice)
                         $distotal = ($item_sale_price - $bundleprice)*$item['quantity'];
                     else{
                         $distotal = '';
                     }
                 }else{
                     $distotal = $_product->get_sale_price()?($_product->get_regular_price()-$_product->get_sale_price())*$item['quantity']:'';
                 }
                $order_product[$x]['LineDiscAmt']   = $distotal;
                $order_product[$x]['LineDiscPct']   = '';


                $order_product[$x]['TaxItemGroup']  = '';
                $order_product[$x]['TaxGroup']      = '';
                $x++;

            }

            $sale["SalesLineList"] = $order_product;
            $sale["FromOrderNumber"]    = $order_id;

            $invoice_data = json_encode($sale, JSON_UNESCAPED_UNICODE);
//echo $invoice_data;
        $url = $this->uri . '/salesorder/create?oauth_consumer_key='.$this->consumer_key.'&oauth_signature_method=HMAC-SHA1&oauth_timestamp='.time().'&oauth_nonce=APkl5DKOWyV&oauth_version=1.0&oauth_signature=Z/fm3F5/RAlU/lMJ8ZX6gR5sqW4=",';

            $curl = curl_init();
//exit;
if(isset($_POST['showfulllink']))
{
	$_SESSION['fulllink']=$url;
	$_SESSION['jsondata']=$invoice_data;
	$_SESSION['port']=$this->port;
}
            curl_setopt_array($curl, array(
              CURLOPT_PORT => $this->port,
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $invoice_data,
              CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Postman-Token: a00c8448-0453-444e-ac1d-09d5c7804aba",
                "cache-control: no-cache"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err)
              throw new Exception($err);

            // $invoice = Request::post($url)
            //     ->sendsJson()
            //     ->expectsHtml()
            //     ->body($order_product)
            //     ->send();

            //For Checking
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction: Created Invoice ORDER_ID = ' . $order_id,
                    'test_value' => 'Response => '.$response . ' Data sent => ' . $invoice_data,
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );
        } catch (\Exception $ex) {
            global $wpdb;
            $wpdb->insert(
                'axapta_logs',
                array(
                    'test_key' => 'Transaction FAILED: Created Invoice ORDER_ID = ' . $order_id,
                    'test_value' => 'Response => ' . $ex->getMessage(),
                    'date_created' => date('Y-m-d H:i:s')
                ),
                array(
                    '%s',
                    '%s',
                    '%s'
                )
            );
        }

    }

    private function axapta_settings()
    {


        $axapta_settings = get_option('axapta_settings_option_name');

        if ( !$axapta_settings )
            return [];

        return $axapta_settings;

    }


}
