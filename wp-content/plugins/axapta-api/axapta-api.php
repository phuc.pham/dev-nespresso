<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://minionsolutions.com
 * @since             1.0.0
 * @package           Axapta_Api
 *
 * @wordpress-plugin
 * Plugin Name:       AXAPTA API
 * Plugin URI:        http://minionsolutions.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Glenn Espejo
 * Author URI:        http://minionsolutions.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       axapta-api
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require 'vendor/autoload.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-axapta-rest.php';

if ( class_exists( 'AXAPTA_REST')) {
    $axapta_rest = new AXAPTA_REST();
}


/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-axapta-api-activator.php
 */
function activate_axapta_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-axapta-api-activator.php';
	Axapta_Api_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-axapta-api-deactivator.php
 */
function deactivate_axapta_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-axapta-api-deactivator.php';
	Axapta_Api_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_axapta_api' );
register_deactivation_hook( __FILE__, 'deactivate_axapta_api' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-axapta-api.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_axapta_api() {

	$plugin = new Axapta_Api();
	$plugin->run();

}
run_axapta_api();
