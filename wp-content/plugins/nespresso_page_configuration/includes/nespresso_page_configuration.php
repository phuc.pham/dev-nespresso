<?php
	//nespresso_boutique
	$nespresso_boutique = "";
	if(get_option( 'nespresso_boutique' ))
	{
		$nespresso_boutique = get_option( 'nespresso_boutique' );
	}
	
	$maps_nespresso_boutique = "";
	if(get_option( 'maps_nespresso_boutique' ))
	{
		$maps_nespresso_boutique = get_option( 'maps_nespresso_boutique' );
	}
	
	$store_hours_nespresso_boutique = "";
	if(get_option( 'store_hours_nespresso_boutique' ))
	{
		$store_hours_nespresso_boutique = get_option( 'store_hours_nespresso_boutique' );
	}
	
	$direct_pickup_nespresso_boutique = "";
	if(get_option( 'direct_pickup_nespresso_boutique' ))
	{
		$direct_pickup_nespresso_boutique = get_option( 'direct_pickup_nespresso_boutique' );
	}
	
	//nespresso_nano_boutique_1
	$nespresso_nano_boutique_1 = "";
	if(get_option( 'nespresso_nano_boutique_1' ))
    {
		$nespresso_nano_boutique_1 = get_option( 'nespresso_nano_boutique_1' );
	}
	
	$maps_nespresso_boutique_2 = "";
	if(get_option( 'maps_nespresso_boutique_2' ))
    {
	    $maps_nespresso_boutique_2 = get_option( 'maps_nespresso_boutique_2' );
	}
	
	$store_hours_nespresso_nano_boutique_1 = "";
	if(get_option( 'store_hours_nespresso_nano_boutique_1' ))
	{
		$store_hours_nespresso_nano_boutique_1 = get_option( 'store_hours_nespresso_nano_boutique_1' );
	}
	
	$direct_pickup_nespresso_nano_boutique_1 = "";
	if(get_option( 'direct_pickup_nespresso_nano_boutique_1' ))
	{
		$direct_pickup_nespresso_nano_boutique_1 = get_option( 'direct_pickup_nespresso_nano_boutique_1' );
	}
	
	//nespresso_nano_boutique_2
	$nespresso_nano_boutique_2 = "";
	if(get_option( 'nespresso_nano_boutique_2' ))
	{
		$nespresso_nano_boutique_2 = get_option( 'nespresso_nano_boutique_2' );
	}
	
	$store_hours_nespresso_nano_boutique_2 = "";
	if(get_option( 'store_hours_nespresso_nano_boutique_2' ))
	{
		$store_hours_nespresso_nano_boutique_2 = get_option( 'store_hours_nespresso_nano_boutique_2' );
	}
	
	$direct_pickup_nespresso_nano_boutique_2 = "";
	if(get_option( 'direct_pickup_nespresso_nano_boutique_2' ))
	{
		$direct_pickup_nespresso_nano_boutique_2 = get_option( 'direct_pickup_nespresso_nano_boutique_2' );
	}
	
	$on_off_nespresso_nano_boutique = false;
	if(get_option( 'on_off_nespresso_nano_boutique' ))
	{
		$on_off_nespresso_nano_boutique = true;
	}
	
	$on_off_nespresso_boutique = false;
	if(get_option( 'on_off_nespresso_boutique' ))
	{
		$on_off_nespresso_boutique = true;
	}
	
	$on_off_nespresso_boutique_tabs_1 = false;
	if(get_option( 'on_off_nespresso_boutique_tabs_1' ))
	{
		$on_off_nespresso_boutique_tabs_1 = true;
	}
	
	$on_off_nespresso_nano_boutique_tabs_1 = false;
	if(get_option( 'on_off_nespresso_nano_boutique_tabs_1' ))
	{
		$on_off_nespresso_nano_boutique_tabs_1 = true;
	}
	
	$on_off_nespresso_nano_boutique_tabs_2 = false;
	if(get_option( 'on_off_nespresso_nano_boutique_tabs_2' ))
	{
		$on_off_nespresso_nano_boutique_tabs_2 = true;
	}
	
	$banner_store_locator_2 = "";
	if(get_option( 'banner_store_locator_2' ))
	{
		$banner_store_locator_2 = get_option( 'banner_store_locator_2');
	}
	
	$link_banner_store_locator_2 = "";
	if(get_option( 'link_banner_store_locator_2' ))
	{
		$link_banner_store_locator_2 = get_option( 'link_banner_store_locator_2');
	}
	
	$mobile_banner_store_locator_2 = "";
	if(get_option( 'mobile_banner_store_locator_2' ))
	{
		$mobile_banner_store_locator_2 = get_option( 'mobile_banner_store_locator_2');
	}
	
	$link_mobile_banner_store_locator_2 = "";
	if(get_option( 'link_mobile_banner_store_locator_2' ))
	{
		$link_mobile_banner_store_locator_2 = get_option( 'link_mobile_banner_store_locator_2');
	}
	
	$custom_modal_banner_nespresso = "";
	if(get_option( 'custom_modal_banner_nespresso' ))
	{
		$custom_modal_banner_nespresso = get_option( 'custom_modal_banner_nespresso');
	}
	
    if($_POST) {
     
        //nespresso_boutique
	    add_option( 'nespresso_boutique', wp_kses_post( $_POST['nespresso_boutique'] ) );
	    update_option( 'nespresso_boutique', wp_kses_post( $_POST['nespresso_boutique'] ) );
	
	    add_option( 'maps_nespresso_boutique', wp_kses_post( $_POST['maps_nespresso_boutique'] ) );
	    update_option( 'maps_nespresso_boutique', wp_kses_post( $_POST['maps_nespresso_boutique'] ) );
	
	    add_option( 'store_hours_nespresso_boutique', wp_kses_post( $_POST['store_hours_nespresso_boutique'] ) );
	    update_option( 'store_hours_nespresso_boutique', wp_kses_post( $_POST['store_hours_nespresso_boutique'] ) );
	
	    add_option( 'direct_pickup_nespresso_boutique', wp_kses_post( $_POST['direct_pickup_nespresso_boutique'] ) );
	    update_option( 'direct_pickup_nespresso_boutique', wp_kses_post( $_POST['direct_pickup_nespresso_boutique'] ) );
	    
	    
	    //nespresso_nano_boutique_1
	    add_option( 'nespresso_nano_boutique_1', wp_kses_post( $_POST['nespresso_nano_boutique_1'] ) );
	    update_option( 'nespresso_nano_boutique_1', wp_kses_post( $_POST['nespresso_nano_boutique_1'] ) );
	
	    add_option( 'store_hours_nespresso_nano_boutique_1', wp_kses_post( $_POST['store_hours_nespresso_nano_boutique_1'] ) );
	    update_option( 'store_hours_nespresso_nano_boutique_1', wp_kses_post( $_POST['store_hours_nespresso_nano_boutique_1'] ) );
	
	    add_option( 'direct_pickup_nespresso_nano_boutique_1', wp_kses_post( $_POST['direct_pickup_nespresso_nano_boutique_1'] ) );
	    update_option( 'direct_pickup_nespresso_nano_boutique_1', wp_kses_post( $_POST['direct_pickup_nespresso_nano_boutique_1'] ) );
	    
	    
	    //nespresso_nano_boutique_2
	    add_option( 'nespresso_nano_boutique_2', wp_kses_post( $_POST['nespresso_nano_boutique_2'] ) );
	    update_option( 'nespresso_nano_boutique_2', wp_kses_post( $_POST['nespresso_nano_boutique_2'] ) );
	
	    add_option( 'maps_nespresso_boutique_2', wp_kses_post( $_POST['maps_nespresso_boutique_2'] ) );
	    update_option( 'maps_nespresso_boutique_2', wp_kses_post( $_POST['maps_nespresso_boutique_2'] ) );
	
	    add_option( 'store_hours_nespresso_nano_boutique_2', wp_kses_post( $_POST['store_hours_nespresso_nano_boutique_2'] ) );
	    update_option( 'store_hours_nespresso_nano_boutique_2', wp_kses_post( $_POST['store_hours_nespresso_nano_boutique_2'] ) );
	
	    add_option( 'direct_pickup_nespresso_nano_boutique_2', wp_kses_post( $_POST['direct_pickup_nespresso_nano_boutique_2'] ) );
	    update_option( 'direct_pickup_nespresso_nano_boutique_2', wp_kses_post( $_POST['direct_pickup_nespresso_nano_boutique_2'] ) );
	    
	    add_option( 'on_off_nespresso_nano_boutique', wp_kses_post( $_POST['on_off_nespresso_nano_boutique'] ) );
	    update_option( 'on_off_nespresso_nano_boutique', wp_kses_post( $_POST['on_off_nespresso_nano_boutique'] ) );
	
	    add_option( 'on_off_nespresso_boutique', wp_kses_post( $_POST['on_off_nespresso_boutique'] ) );
	    update_option( 'on_off_nespresso_boutique', wp_kses_post( $_POST['on_off_nespresso_boutique'] ) );
	
	    add_option( 'on_off_nespresso_boutique_tabs_1', wp_kses_post( $_POST['on_off_nespresso_boutique_tabs_1'] ) );
	    update_option( 'on_off_nespresso_boutique_tabs_1', wp_kses_post( $_POST['on_off_nespresso_boutique_tabs_1'] ) );
	    
	    add_option( 'on_off_nespresso_nano_boutique_tabs_1', wp_kses_post( $_POST['on_off_nespresso_nano_boutique_tabs_1'] ) );
	    update_option( 'on_off_nespresso_nano_boutique_tabs_1', wp_kses_post( $_POST['on_off_nespresso_nano_boutique_tabs_1'] ) );
	
	    add_option( 'on_off_nespresso_nano_boutique_tabs_2', wp_kses_post( $_POST['on_off_nespresso_nano_boutique_tabs_2'] ) );
	    update_option( 'on_off_nespresso_nano_boutique_tabs_2', wp_kses_post( $_POST['on_off_nespresso_nano_boutique_tabs_2'] ) );
	    
	    add_option( 'banner_store_locator_2', wp_kses_post( $_POST['banner_store_locator_2'] ) );
	    update_option( 'banner_store_locator_2', wp_kses_post( $_POST['banner_store_locator_2'] ) );
	
	    add_option( 'mobile_banner_store_locator_2', wp_kses_post( $_POST['mobile_banner_store_locator_2'] ) );
	    update_option( 'mobile_banner_store_locator_2', wp_kses_post( $_POST['mobile_banner_store_locator_2'] ) );
	    
	    add_option( 'link_mobile_banner_store_locator_2', wp_kses_post( $_POST['link_mobile_banner_store_locator_2'] ) );
	    update_option( 'link_mobile_banner_store_locator_2', wp_kses_post( $_POST['link_mobile_banner_store_locator_2'] ) );
	    
	    add_option( 'link_banner_store_locator_2', wp_kses_post( $_POST['link_banner_store_locator_2'] ) );
	    update_option( 'link_banner_store_locator_2', wp_kses_post( $_POST['link_banner_store_locator_2'] ) );
	    
	    add_option( 'custom_modal_banner_nespresso', wp_kses_post( $_POST['custom_modal_banner_nespresso'] ) );
	    update_option( 'custom_modal_banner_nespresso', wp_kses_post( $_POST['custom_modal_banner_nespresso'] ) );
	    
	
	    echo "<meta http-equiv='refresh' content='0'>";
    }
    

?>
 <form action="" method="post" id="nespresso_page_configuration_form" name="nespresso_page_configuration_form">
   <h1>Nespresso Boutique</h1>
     <fieldset>
         <p class="description">Enable to Show Tabs Nespresso Boutique.</p>
         <label for="on_off_nespresso_boutique" style="margin: 20px">
			 <?= $on_off_nespresso_boutique ? 'Enable' : 'Disable' ?>
             <input class="" type="checkbox" name="on_off_nespresso_boutique" id="on_off_nespresso_boutique" <?= $on_off_nespresso_boutique ? 'checked=checked' : '' ?> >
         </label>
     </fieldset>
     <hr>
     <hr>
     <fieldset>
         <p class="description">Enable to Show Branch Nespresso Boutique.</p>
         <label for="red_email_invoice_status" style="margin: 20px">
			 <?= $on_off_nespresso_boutique_tabs_1 ? 'Enable' : 'Disable' ?>
             <input class="" type="checkbox" name="on_off_nespresso_boutique_tabs_1" id="on_off_nespresso_boutique_tabs_1" <?= $on_off_nespresso_boutique_tabs_1 ? 'checked=checked' : '' ?> >
         </label>
     </fieldset>
      <fieldset>
        <p class="description">Nespresso Boutique</p>
        <label for="nespresso_boutique" style="margin: 20px">
            <input class=""  style="width: 100%" size="100%" type="text" name="nespresso_boutique" id="nespresso_boutique" value="<?= $nespresso_boutique ? $nespresso_boutique : "No Data" ?>" >
        </label>
      </fieldset>
      <fieldset>
         <p class="description">Store hours</p>
         <label for="store_hours_nespresso_boutique" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="store_hours_nespresso_boutique" id="store_hours_nespresso_boutique" value="<?= $store_hours_nespresso_boutique ? $store_hours_nespresso_boutique : "No Data" ?>" >
         </label>
      </fieldset>
      <fieldset>
         <p class="description">For direct order and pick-up please call</p>
         <label for="direct_pickup_nespresso_boutique" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="direct_pickup_nespresso_boutique" id="direct_pickup_nespresso_boutique" value="<?= $direct_pickup_nespresso_boutique ? $direct_pickup_nespresso_boutique : "No Data" ?>" >
         </label>
      </fieldset>
     <fieldset>
         <p class="description">Google Maps Nespresso Boutique</p>
         <label for="maps_nespresso_boutique" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="maps_nespresso_boutique" id="maps_nespresso_boutique" value="<?= $maps_nespresso_boutique ? $maps_nespresso_boutique : "No Data" ?>" >
         </label>
      </fieldset>
     
     <hr>
     
     <h1>Nespresso Nano Boutique</h1>
     <fieldset>
         <p class="description">Enable to Show Tabs Nespresso Nano Boutique.</p>
         <label for="on_off_nespresso_nano_boutique" style="margin: 20px">
			 <?= $on_off_nespresso_nano_boutique ? 'Enable' : 'Disable' ?>
             <input class="" type="checkbox" name="on_off_nespresso_nano_boutique" id="on_off_nespresso_nano_boutique" <?= $on_off_nespresso_nano_boutique ? 'checked=checked' : '' ?> >
         </label>
     </fieldset>
        <hr>
        <hr>
     <fieldset>
         <p class="description">Enable to Show Branch Nespresso Nano Boutique.</p>
         <label for="on_off_nespresso_nano_boutique_tabs_1" style="margin: 20px">
			 <?= $on_off_nespresso_nano_boutique_tabs_1 ? 'Enable' : 'Disable' ?>
             <input class="" type="checkbox" name="on_off_nespresso_nano_boutique_tabs_1" id="on_off_nespresso_nano_boutique_tabs_1" <?= $on_off_nespresso_nano_boutique_tabs_1 ? 'checked=checked' : '' ?> >
         </label>
     </fieldset>
      <fieldset>
        <p class="description">Nespresso Nano Boutique</p>
        <label for="nespresso_nano_boutique_1" style="margin: 20px">
        <input class=""  style="width: 100%" size="100%" type="text" name="nespresso_nano_boutique_1" id="nespresso_nano_boutique_1" value="<?= $nespresso_nano_boutique_1 ? $nespresso_nano_boutique_1 : "No Data"; ?>" >
        </label>
      </fieldset>
      <fieldset>
        <p class="description">Store hours</p>
        <label for="store_hours_nespresso_nano_boutique_1" style="margin: 20px">
        <input class=""  style="width: 100%" size="100%" type="text" name="store_hours_nespresso_nano_boutique_1" id="store_hours_nespresso_nano_boutique_1" value="<?= $store_hours_nespresso_nano_boutique_1 ? $store_hours_nespresso_nano_boutique_1 : "No Data" ?>" >
        </label>
      </fieldset>
      <fieldset>
        <p class="description">For direct order and pick-up please call</p>
        <label for="direct_pickup_nespresso_nano_boutique_1" style="margin: 20px">
        <input class=""  style="width: 100%" size="100%" type="text" name="direct_pickup_nespresso_nano_boutique_1" id="direct_pickup_nespresso_nano_boutique_1" value="<?= $direct_pickup_nespresso_nano_boutique_1 ? $direct_pickup_nespresso_nano_boutique_1 : "No Data"; ?>" >
        </label>
      </fieldset>
     <hr>
     <hr>
     <fieldset>
         <p class="description">Enable to Show Branch Nespresso Nano Boutique.</p>
         <label for="on_off_nespresso_nano_boutique_tabs_2" style="margin: 20px">
			 <?= $on_off_nespresso_nano_boutique_tabs_2 ? 'Enable' : 'Disable' ?>
             <input class="" type="checkbox" name="on_off_nespresso_nano_boutique_tabs_2" id="on_off_nespresso_nano_boutique_tabs_2" <?= $on_off_nespresso_nano_boutique_tabs_2 ? 'checked=checked' : '' ?> >
         </label>
     </fieldset>
     <fieldset>
         <p class="description">Nespresso Nano Boutique 2</p>
         <label for="on_off_nespresso_nano_boutique_tabs_2" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="nespresso_nano_boutique_2" id="on_off_nespresso_nano_boutique_tabs_2" value="<?= $nespresso_nano_boutique_2 ? $nespresso_nano_boutique_2 : "No Data"; ?>" >
         </label>
     </fieldset>
     <fieldset>
         <p class="description">Store hours</p>
         <label for="store_hours_nespresso_nano_boutique_2" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="store_hours_nespresso_nano_boutique_2" id="store_hours_nespresso_nano_boutique_2" value="<?= $store_hours_nespresso_nano_boutique_2 ? $store_hours_nespresso_nano_boutique_2 : "No Data" ?>" >
         </label>
     </fieldset>
     <fieldset>
         <p class="description">For direct order and pick-up please call</p>
         <label for="direct_pickup_nespresso_nano_boutique_2" style="margin: 20px">
             <input class="" s style="width: 100%" ize="100%" type="text" name="direct_pickup_nespresso_nano_boutique_2" id="direct_pickup_nespresso_nano_boutique_2" value="<?= $direct_pickup_nespresso_nano_boutique_2 ? $direct_pickup_nespresso_nano_boutique_2 : "No Data"; ?>" >
         </label>
     </fieldset>
     <fieldset>
         <p class="description">Google Maps Nespresso Nano Boutique</p>
         <label for="maps_nespresso_boutique_2" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="maps_nespresso_boutique_2" id="maps_nespresso_boutique_2" value="<?= $maps_nespresso_boutique_2 ? $maps_nespresso_boutique_2 : "No Data"; ?>" >
         </label>
     </fieldset>
     
     <hr>
     <h1>Banner Store Locator 2</h1>

     <fieldset>
         <p class="description">Banner Store Locator 2 </p>
         <label for="banner_store_locator_2" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="banner_store_locator_2" id="banner_store_locator_2" value="<?= $banner_store_locator_2 ? $banner_store_locator_2 : "No Data"; ?>" >
         </label>
     </fieldset>
     
     <fieldset>
         <p class="description">Link Banner Store Locator 2 </p>
         <label for="link_banner_store_locator_2" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="link_banner_store_locator_2" id="link_banner_store_locator_2" value="<?= $link_banner_store_locator_2 ? $link_banner_store_locator_2 : "No Data"; ?>" >
         </label>
     </fieldset>
     
     <fieldset>
         <p class="description">Mobile Banner Store Locator 2 </p>
         <label for="mobile_banner_store_locator_2" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="mobile_banner_store_locator_2" id="mobile_banner_store_locator_2" value="<?= $mobile_banner_store_locator_2 ? $mobile_banner_store_locator_2 : "No Data"; ?>" >
         </label>
     </fieldset>
     
     <fieldset>
         <p class="description">Link Mobile Banner Store Locator 2 </p>
         <label for="link_mobile_banner_store_locator_2" style="margin: 20px">
             <input class=""  style="width: 100%" size="100%" type="text" name="link_mobile_banner_store_locator_2" id="link_mobile_banner_store_locator_2" value="<?= $link_mobile_banner_store_locator_2 ? $link_mobile_banner_store_locator_2 : "No Data"; ?>" >
         </label>
     </fieldset>

     <h1>Custom Modal for Banner Nespresso</h1>

     <fieldset>
         <p class="description">custom_modal_banner_nespresso</p>
         <label for="custom_modal_banner_nespresso" style="margin: 20px">
             <?php wp_editor( $custom_modal_banner_nespresso , 'custom_modal_banner_nespresso' ); ?>
         </label>
     </fieldset>
     

 
<?php
  submit_button('Save', 'primary');
?>
</form>

