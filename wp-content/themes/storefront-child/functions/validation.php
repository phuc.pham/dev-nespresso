<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * list of custom and extendable js validations to all front-end forms
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * validation class for front-end
 *
 * @package Validation
 */
class Validation {

	/**
	 * type of request being submitted
	 * @var string
	 */
	public $request_type = 'post';

	/**
	 * the user
	 * @var object
	 */
	public $user;

	/**
	 * class constructor
	 *
	 * @param object $user
	 */
	public function __construct($user=null) {

		$this->user = $user;

		$this->request_type = 'post'; // by default
	} // __construct()

	/**
	 * check if class request_type is valid
	 *
	 * @param  string $request_type
	 * @return boolean
	 */
	private function valid_request_type($request_type='post') {

		if ( $request_type != 'post' && $request_type != 'get' ) {
			throw new Exception('Nespresso Validation Error:: Validation variable $request_type only accepts "post" or "get" value.');
			return false;
		}

		return true;
	} // valid_request_type()

	/**
	 * set request type
	 *
	 * @param string $request_type
	 * @return Class
	 */
	public function set_request_type($request_type='post') {

		$this->valid_request_type($request_type);

		$this->request_type = $request_type;

		return $this;
	} // set_request_type()

	/**
	 * get the class request_type var
	 *
	 * @return string
	 */
	public function get_request_type() {
		return $this->request_type;
	}

	/**
	 * validate first name
	 *
	 * @param  string $first_name
	 * @return boolean
	 */
	public function valid_first_name($first_name=null) {

		if ( $first_name == null )
			$first_name = $this->request_type == 'post' ? @$_POST['first_name'] : @$_GET['first_name'];

		if ( !$first_name )
			return false;

		return true;
	} // valid_first_name()

	/**
	 * validate middle name
	 *
	 * @param  string $middle_name
	 * @return boolean
	 */
	public function valid_middle_name($middle_name=null) {

		if ( $middle_name == null )
			$middle_name = $this->request_type == 'post' ? @$_POST['middle_name'] : @$_GET['middle_name'];

		return true; // middle name is not required by ingrid

		if ( !$middle_name )
			return false;

		return true;
	} // valid_middle_name()

	/**
	 * validate last name
	 *
	 * @param  string $last_name
	 * @return boolean
	 */
	public function valid_last_name($last_name=null) {

		if ( $last_name == null )
			$last_name = $this->request_type == 'post' ? @$_POST['last_name'] : @$_GET['last_name'];

		if ( !$last_name )
			return false;

		return true;
	} // valid_last_name()

	/**
	 * validate date_of_birth
	 *
	 * @param  string $date_of_birth
	 * @return boolean
	 */
	public function valid_date_of_birth($date_of_birth=null) {

		if ( $date_of_birth == null )
			$date_of_birth = $this->request_type == 'post' ? @$_POST['date_of_birth'] : @$_GET['date_of_birth'];

		$regex = "/^\d{4}\-\d{1,2}\-\d{1,2}$/";

		if ( !$date_of_birth || !preg_match($regex, $date_of_birth) )
			return false;

		return true;
	} // valid_date_of_birth()

	/**
	 * validate mobile
	 *
	 * @param  string $mobile
	 * @return boolean
	 */
	public function valid_mobile($mobile=null) {

		if ( $mobile == null )
			$mobile = $this->request_type == 'post' ? @$_POST['mobile'] : @$_GET['mobile'];

		if ( !$mobile )
			return false;

		return true;
	} // valid_mobile()

	/**
	 * validate email
	 *
	 * @param  string $email
	 * @return boolean
	 */
	public function valid_email($email=null) {

		if ( $email == null )
			$email = $this->request_type == 'post' ? @$_POST['email'] : @$_GET['email'];

		// $regex = "/^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$/";

		// if ( !$email || !preg_match($regex, $email) )
		// 	return false;

		return filter_var($email, FILTER_VALIDATE_EMAIL) ? true : false;

		// return true;
	} // valid_email()

	/**
	 * validate email
	 *
	 * @param  string $email
	 * @return boolean
	 */
	public function valid_email_is_unique($email=null) {

		if ( $email == null )
			$email = $this->request_type == 'post' ? @$_POST['email'] : @$_GET['email'];

		$user = nespresso_get_user_data();

		if ( !$email )
			return false;

		if ( $user && $user->user_email == $email)
			return true;

		$findUser = get_user_by('email', $email);

		if ( $findUser )
			return false;

		return true;
	} // valid_email()

	/**
	 * validate email confirmation
	 *
	 * @param  string $email
	 * @return boolean
	 */
	public function valid_email_confirmation($email=null, $email_confirmation=null) {

		if ( $email == null )
			$email = $this->request_type == 'post' ? @$_POST['email'] : @$_GET['email'];

		if ( $email_confirmation == null )
			$email_confirmation = $this->request_type == 'post' ? @$_POST['email_confirmation'] : @$_GET['email_confirmation'];

		if ( $email != $email_confirmation )
			return false;

		return true;
	} // valid_email()

	/**
	 * validate email confirmation
	 *
	 * @param  string $email
	 * @return boolean
	 */
	public function valid_email_exists($email=null) {

		if ( $email == null )
			$email = $this->request_type == 'post' ? @$_POST['email'] : @$_GET['email'];

		$findUser = get_user_by('email', $email);

		if ( !$findUser || empty($findUser) )
			return false;

		return true;
	} // valid_email()

	/**
	 * validate password
	 *
	 * @param  string $password
	 * @return boolean
	 */
	public function valid_password($password=null) {

		if ( $password == null )
			$password = $this->request_type == 'post' ? @$_POST['password'] : @$_GET['password'];

		$regex = "/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!~@$%^&*-]).{4,}$/";

		if ( !preg_match($regex, $password) )
			return false;

		return true;
	} // valid_password()

	/**
	 * validate password current
	 *
	 * @param  string $password_current
	 * @return boolean
	 */
	public function valid_password_current($password_current=null) {

		if ( $password_current == null )
			$password_current = $this->request_type == 'post' ? @$_POST['password_current'] : @$_GET['password_current'];

		$user = nespresso_get_user_data();

		if ( !$user )
			return false;

		if ( !wp_check_password($password_current, $user->user_pass, $user->ID) )
			return false;

		return true;
	} // valid_password_current()

	/**
	 * validate password that are the same as the old one
	 *
	 * @param  string $password
	 * @return boolean - if the same true, else false
	 */
	public function valid_password_same($password=null) {

		if ( $password == null )
			$password = $this->request_type == 'post' ? @$_POST['password'] : @$_GET['password'];

		$user = $this->user ? $this->user : nespresso_get_user_data();

		// @todo: more checking needed
		if ( wp_check_password($password, $user->user_pass, $user->ID) )
			return false;

		return true;
	} // valid_password()

	/**
	 * validate password confirmation
	 *
	 * @param  string $password
	 * @return boolean
	 */
	public function valid_password_confirmation($password=null, $password_confirmation=null) {

		if ( $password == null )
			$password = $this->request_type == 'post' ? @$_POST['password'] : @$_GET['password'];

		if ( $password == null )
			$password_confirmation = $this->request_type == 'post' ? @$_POST['password_confirmation'] : @$_GET['password_confirmation'];

		if ( !$password || !$password != $password_confirmation )
			return false;

		return true;
	} // valid_password()

	/**
	 * validate club membership nuber
	 *
	 * @param  string $club_membership_no
	 * @return boolean
	 */
	public function valid_club_membership_no($club_membership_no=null) {

		if ( $club_membership_no == null )
			$club_membership_no = $this->request_type == 'post' ? @$_POST['club_membership_no'] : @$_GET['club_membership_no'];

		if ( !$club_membership_no )
			return false;
		else
			return true;
	} // valid_club_membership_no()

	/**
	 * validate billing_address_type
	 *
	 * @param  string $billing_address_type
	 * @return boolean
	 */
	public function valid_billing_address_type($billing_address_type=null) {

		if ( $billing_address_type == null )
			$billing_address_type = $this->request_type == 'post' ? @$_POST['billing_address_type'] : @$_GET['billing_address_type'];

		if ( $billing_address_type != 'private' && $billing_address_type != 'company' )
			return false;

		return true;
	} // valid_billing_address_type()

	/**
	 * validate title
	 *
	 * @param  string $title
	 * @return boolean
	 */
	public function valid_title($title=null) {

		if ( $title == null )
			$title = $this->request_type == 'post' ? @$_POST['title'] : @$_GET['title'];

		if ( $title != 'Mr/Ms' && $title != 'Mr' && $title != 'Ms' )
			return false;

		return true;
	} // valid_title()

	/**
	 * validate billing_address_1
	 *
	 * @param  string $billing_address_1
	 * @return boolean
	 */
	public function valid_billing_address_1($billing_address_1=null) {

		if ( $billing_address_1 == null )
			$billing_address_1 = $this->request_type == 'post' ? @$_POST['billing_address_1'] : @$_GET['billing_address_1'];

		if ( !$billing_address_1 )
			return false;

		return true;
	} // valid_billing_address_1()

	/**
	 * validate billing_address_2
	 *
	 * @param  string $billing_address_2
	 * @return boolean
	 */
	public function valid_billing_address_2($billing_address_2=null) {

		if ( $billing_address_2 == null )
			$billing_address_2 = $this->request_type == 'post' ? @$_POST['billing_address_2'] : @$_GET['billing_address_2'];

		if ( !$billing_address_2 )
			return false;

		return true;
	} // valid_billing_address_2()

	/**
	 * validate city
	 *
	 * @param  string $city
	 * @return boolean
	 */
	public function valid_city($city=null) {
		return $this->valid_billing_city($city);
	}

	/**
	 * validate billing_city
	 *
	 * @param  string $billing_city
	 * @return boolean
	 */
	public function valid_billing_city($billing_city=null) {

		if ( $billing_city == null )
			$billing_city = $this->request_type == 'post' ? @$_POST['billing_city'] : @$_GET['billing_city'];

		if ( !$billing_city )
			return false;

		return true;
	} // valid_billing_city()

	/**
	 * validate state
	 *
	 * @param  string $state
	 * @return boolean
	 */
	public function valid_state($state=null) {
		return $this->valid_billing_state();
	}

	/**
	 * validate billing_state
	 *
	 * @param  string $billing_state
	 * @return boolean
	 */
	public function valid_billing_state($billing_state=null) {

		if ( $billing_state == null )
			$billing_state = $this->request_type == 'post' ? @$_POST['billing_state'] : @$_GET['billing_state'];

		if ( !$billing_state )
			return false;

		return true;
	} // valid_billing_state()

	/**
	 * validate billing coiuntry
	 *
	 * @param  string $billing_country
	 * @return boolean
	 */
	public function valid_billing_country($billing_country=null) {

		if ( $billing_country == null )
			$billing_country = $this->request_type == 'post' ? @$_POST['billing_country'] : @$_GET['billing_country'];

		if ( !$billing_country )
			return false;

		return true;
	} // valid_billing_country()

	/**
	 * validate billing_postcode
	 *
	 * @param  string $billing_postcode
	 * @return boolean
	 */
	public function valid_billing_postcode($billing_postcode=null) {

		if ( $billing_postcode == null )
			$billing_postcode = $this->request_type == 'post' ? @$_POST['billing_postcode'] : @$_GET['billing_postcode'];

		if ( !$billing_postcode )
			return false;

		return true;
	} // valid_billing_postcode()

	/**
	 * validate billing_mobile
	 *
	 * @param  string $billing_mobile
	 * @return boolean
	 */
	public function valid_billing_mobile($billing_mobile=null) {

		if ( $billing_mobile == null )
			$billing_mobile = $this->request_type == 'post' ? @$_POST['billing_mobile'] : @$_GET['billing_mobile'];

		if ( !$billing_mobile )
			return false;

		return true;
	} // valid_billing_mobile()

	/**
	 * validate billing_phone
	 *
	 * @param  string $billing_phone
	 * @return boolean
	 */
	public function valid_billing_phone($billing_phone=null) {

		if ( $billing_phone == null )
			$billing_phone = $this->request_type == 'post' ? @$_POST['billing_phone'] : @$_GET['billing_phone'];

		if ( !$billing_phone )
			return false;

		return true;
	} // valid_billing_phone()

	/**
	 * validate billing_home_office
	 *
	 * @param  string $billing_home_office
	 * @return boolean
	 */
	public function valid_billing_home_office($billing_home_office=null) {

		if ( $billing_home_office == null )
			$billing_home_office = $this->request_type == 'post' ? @$_POST['billing_home_office'] : @$_GET['billing_home_office'];

		if ( !$billing_home_office )
			return false;

		return true;
	} // valid_billing_home_office()

	/**
	 * validate postal code
	 *
	 * @param  string $postal_code
	 * @return boolean
	 */
	public function valid_postal_code($postal_code=null) {

		if ( $postal_code == null )
			$postal_code = $this->request_type == 'post' ? @$_POST['postal_code'] : @$_GET['postal_code'];

		if ( !$postal_code )
			return false;

		return true;
	} // valid_postal_code()

	/**
	 * validate shipping_mode
	 *
	 * @param  string $shipping_mode
	 * @return boolean
	 */
	public function valid_shipping_mode($shipping_mode=null) {

		if ( $shipping_mode == null )
			$shipping_mode = $this->request_type == 'post' ? @$_POST['shipping_mode'] : @$_GET['shipping_mode'];

		if ( $shipping_mode != 'none' && $shipping_mode !== 'standard' )
			return false;

		return true;
	} // valid_shipping_mode()

	/**
	 * validate shipping_mode standard options
	 *
	 * @param  string $shipping_mode_standard_options
	 * @return boolean
	 */
	public function valid_shipping_mode_standard_options($shipping_mode_standard_options=null) {

		if ( $shipping_mode_standard_options == null )
			$shipping_mode_standard_options = $this->request_type == 'post' ? @$_POST['shipping_mode_standard_options'] : @$_GET['shipping_mode_standard_options'];

		if ( $shipping_mode_standard_options != 'Saturday Delivery' && $shipping_mode_standard_options !== 'Recycling at home' && $shipping_mode_standard_options !== 'Signature' )
			return false;

		return true;
	} // valid_shipping_mode_standard_options()

	/**
	 * validate payment_mode
	 *
	 * @param  string $payment_mode
	 * @return boolean
	 */
	public function valid_payment_mode($payment_mode=null) {

		if ( $payment_mode == null )
			$payment_mode = $this->request_type == 'post' ? @$_POST['payment_mode'] : @$_GET['payment_mode'];

		if ( $payment_mode != 'none' && $payment_mode !== 'Credit Card' && $payment_mode !== 'Invoice' && $payment_mode !== 'PostFinance' )
			return false;

		return true;
	} // valid_payment_mode()

	/**
	 * validate subscribed_in_nesspresso_club
	 *
	 * @param  string $subscribed_in_nesspresso_club
	 * @return boolean
	 */
	public function valid_subscribed_in_nesspresso_club($subscribed_in_nesspresso_club=null) {

		if ( $subscribed_in_nesspresso_club == null )
			$subscribed_in_nesspresso_club = $this->request_type == 'post' ? @$_POST['subscribed_in_nesspresso_club'] : @$_GET['subscribed_in_nesspresso_club'];

		if ( $subscribed_in_nesspresso_club && $subscribed_in_nesspresso_club != '1' )
			return false;

		return true;
	} // valid_subscribed_in_nesspresso_club()

	/**
	 * validate contact_preferences
	 *
	 * @param  string $contact_preferences
	 * @return boolean
	 */
	public function valid_contact_preferences($contact_preferences=null) {

		if ( $contact_preferences == null )
			$contact_preferences = $this->request_type == 'post' ? @$_POST['contact_preferences'] : @$_GET['contact_preferences'];

		if ( $contact_preferences ) {

			$validValues = ['phone', 'post', 'sms'];

			$valid_contact_preferneces = true;

			foreach ( $contact_preferences as $contact_preference ) :
				if ( !in_array($contact_preference, $validValues)) {
					$valid_contact_preferneces = false;
					break;
				}
			endforeach;

			return $valid_contact_preferneces;
		}

		return true;
	} // valid_subscribed_in_nesspresso_club()

	/**
	 * validate subscribed_in_nesspresso_news
	 *
	 * @param  string $subscribed_in_nesspresso_news
	 * @return boolean
	 */
	public function valid_subscribed_in_nesspresso_news($subscribed_in_nesspresso_news=null) {

		if ( $subscribed_in_nesspresso_news == null )
			$subscribed_in_nesspresso_news = $this->request_type == 'post' ? @$_POST['subscribed_in_nesspresso_news'] : @$_GET['subscribed_in_nesspresso_news'];

		if ( $subscribed_in_nesspresso_news && $subscribed_in_nesspresso_news != '1' )
			return false;

		return true;
	} // validate_subscribed_in_nesspresso_news()


	/**
	 * validate billing_ward
	 *
	 * @param  string $billing_ward
	 * @return boolean
	 */
	public function valid_billing_ward($billing_ward=null) {

		if ( $billing_ward == null )
			$billing_ward = $this->request_type == 'post' ? @$_POST['billing_ward'] : @$_GET['billing_ward'];

		if ( !$billing_ward )
			return false;

		return true;
	} // valid_billing_ward()

	/**
	 * validate billing_district
	 *
	 * @param  string $billing_district
	 * @return boolean
	 */
	public function valid_billing_district($billing_district=null) {

		if ( $billing_district == null )
			$billing_district = $this->request_type == 'post' ? @$_POST['billing_district'] : @$_GET['billing_district'];

		if ( !$billing_district )
			return false;

		return true;
	} // valid_billing_district()


} // class Validation
