<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with coffee products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * get all the coffee products, not trached
 * sorted by post id
 *
 * @return array
 */
function get_coffee_products() {

	global $wpdb;

	$posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='product_type' AND meta_value='%s' ", 'Coffee') );

	$product_count = 0;

	$products = [];

	if ( !empty($posts) && $posts ) :

	    foreach ( $posts as $post ) :

	    	$product = wc_get_product($post->post_id);

	    	if ( !$product )
	    		continue;


	    	$product = $product->get_data();

	    	if ( !$product ||
	    		$product['status'] != 'publish'
    		) {
	        	continue;
	    	}

	        $product = include_cofee_product_meta_from_results($post);
			
	        $category_name_obj = get_field_object('category', $post->post_id);

	        $category_name = isset($category_name_obj['choices'][$product->category]) ? $category_name_obj['choices'][$product->category] : '';

	        $products[$category_name][$product_count] =  $product;

	        $product_count++;

	    endforeach;

	    $products ? ksort($products) : [];
	endif;
	return $products;
}

/**
 * get all the discovery products, not trached
 * sorted by post id
 *
 * @return array
 */
function get_discovery_products() {

	global $wpdb;

	$posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='product_type' AND meta_value='%s' ", 'Coffee') );

	$product_count = 0;

	$products = [];

	if ( !empty($posts) && $posts ) :

	    foreach ( $posts as $post ) :

	    	$aromatic_profile = get_post_meta($post->post_id, 'aromatic_profile', true );

	    	if ($aromatic_profile == 'Varied'):

		    	$product = wc_get_product($post->post_id);

		    	if ( !$product )
		    		continue;


		    	$product = $product->get_data();

		    	if ( !$product ||
		    		$product['status'] != 'publish' ||
		    		$product['stock_status'] != 'instock'
	    		) {
		        	continue;
		    	}

		        $product = include_cofee_product_meta_from_results($post);

		        $products[$product_count] =  $product;

		        $product_count++;

	        endif;

	    endforeach;

	    $products ? ksort($products) : [];

	endif;

	return $products;
}

function get_discovery_gifts($id_only = false) {

	global $wpdb;

	$posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='product_type' AND meta_value='%s' ", 'Discovery Gift') );

	$product_count = 0;

	$products = [];

	$product_ids = [];

	if ( !empty($posts) && $posts ) :

	    foreach ( $posts as $post ) :

	    	$product_ids[] 	= $post->post_id;
	    	$product 		= wc_get_product($post->post_id);

	    	if ( !$product )
	    		continue;


	    	$product = $product->get_data();

	    	if ( !$product ||
	    		$product['status'] != 'publish'
    		) {
	        	continue;
	    	}

	        $product = include_cofee_product_meta_from_results($post);

	        $products[$product_count] =  $product;

	        $product_count++;

	    endforeach;

	    $products ? ksort($products) : [];

	endif;

	return $id_only ? $product_ids : $products;
}

/**
 * get discovery gift data
 *
 */
add_action('wp_ajax_get_discovery_gift_key', 'get_discovery_gift_key');
add_action('wp_ajax_nopriv_get_discovery_gift_key', 'get_discovery_gift_key');
function get_discovery_gift_key() {
    $discovery_gift_ids = get_discovery_gifts(true);
    $cart_item_key = null;
    foreach( WC()->cart->get_cart() as $key => $cart_item ){
	    if (in_array($cart_item['product_id'], $discovery_gift_ids)) {
	    	$cart_item_key = $key;
	    	break;
	    }

	}
    wp_send_json(['cart_item_key' => $cart_item_key]);
}

/**
 * include the cofee product meta(custom) data from the result given
 *
 * @param  object|null $product - product result
 * @return object
 */
function include_cofee_product_meta_from_results( $product=null, $single_product_page=false ) {

	$product = (object)$product;

	if ( isset($product->post_id) )
		$product_id = $product->post_id;
	elseif ( isset($product->ID) )
		$product_id = $product->ID;
	elseif ( isset($product->id) )
		$product_id = $product->id;
	else
		$product_id = 0;

	if ( get_post_status($product_id) == 'trash' )
		return null;

	return (object) array_merge(
		(array) $product,
		(array) get_coffee_product_meta_data( $product_id, $single_product_page )
	);

} // include_cofee_product_meta_from_results()

/**
 * get the coffee product meta(custom) data
 *
 * @param  integer $product_id
 * @param  boolean $single_product_page
 * @return object|null
 */
function get_coffee_product_meta_data( $product_id=0, $single_product_page=false ) {

	$product_id = $product_id ? $product_id : 0;

	$product = (object)null;

		$product->product_id = $product_id;

	$product->name = get_the_title( $product_id );

	$product->url = get_permalink( $product_id );

	$product->price = get_post_meta( $product_id, '_price', true );
	$product->regular_price = get_post_meta( $product_id, '_regular_price', true );
	$product->description = get_field('description', $product_id );

	$product->short_description = get_field('short_description', $product_id );

	$product->category = get_field( 'category', $product_id );

	$product->size_of_cup = get_field('size_of_cup', $product_id);;

	$product->aromatic_profile = get_field( 'aromatic_profile', $product_id );

	$product->aromatic_notes = get_field( 'aromatic_notes', $product_id );

	$product->intensity = get_field( 'intensity', $product_id );
	$product->qty_x1 = get_field( 'qty_x1', $product_id );//add x1
	$product->property = get_field( 'property', $product_id );

	$product->product_type = get_field( 'product_type', $product_id );
$product->label_off = get_field( 'label_off', $product_id );
	$product->ingredients_and_allergens = get_field( 'ingredients_and_allergens', $product_id );

	$product->recycling = get_field( 'recycling', $product_id );

	$product->sleeve = get_field( 'sleeve', $product_id );

	if ($product->sleeve) {
		$product->sleeve_quantity = get_field( 'sleeve_quantity', $product_id );
	}

	$product->recommended_products = $single_product_page ? get_recommended_coffee_products($product_id) : '';

	/**
	 * single_product_page is usually used for product page (single product page view)
	 */
	$product->similar_aromatic_profile_products = $single_product_page ? get_similar_aromatic_profile_products($product->aromatic_profile) : '';

	$product->image = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );

	$product->product_view_image = get_field( 'product_view_image', $product_id );

	$product->sku = get_post_meta( $product_id, '_sku', true );

	$product->stock = get_post_meta( $product_id, '_stock', true );
	
	// add custom field for coffee_range_2
	$product->coffee_range2_banner_modal = get_post_meta( $product_id, 'coffee_range2_banner_modal', true );
	$product->coffee_range2_logo_modal = get_post_meta( $product_id, 'coffee_range2_logo_modal', true );
	$product->description_coffee_range2_modal = get_post_meta( $product_id, 'description_coffee_range2_modal', true );

	return $product;

} // get_coffee_product_meta_data()

/**
 * get recommended coffee products from acf form
 *
 * @param  integer $product_id
 * @return array|string - string = no product
 */
function get_recommended_coffee_products($product_id=0) {

	if ( !$product_id )
		return '';

	$recommended_products = get_field( 'recommended_products', $product_id );

	if ( !$recommended_products )
		return '';

	$return = [];
	foreach ( $recommended_products as $recommended_product ) :
		$return[]  = (object) array_merge(
			(array) $recommended_product,
			(array) get_machines_product_meta_data( $recommended_product->ID, $single_product_page=false )
		);
	endforeach;

	return $return;

} // single_product_page()

/**
 * get similar aromatic profile products from acf form
 *
 * @param  integer $aromatic_profile
 * @return array|string - string = no product
 */
function get_similar_aromatic_profile_products($aromatic_profile=null) {

	if ( !$aromatic_profile )
		return '';

	global $wpdb;

	$similar_products= $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->postmeta WHERE meta_key='aromatic_profile' AND meta_value='%s' LIMIT 10", $aromatic_profile ) );

	if ( !$similar_products )
		return '';

	$return = [];

	foreach ($similar_products as $similar_product ) :

		$return[] =  include_cofee_product_meta_from_results($similar_product);

	endforeach;

	return $return;

} // single_product_page()


