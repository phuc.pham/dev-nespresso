<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with accessory products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * get the ordered products by order_id
 *
 * @param  integer $order_id
 * @return array
 */
function get_ordered_products($order_id=0) {

	$ordered_products = [];

	if ( !$order_id )
		return $ordered_products;

	$order = new WC_Order( $order_id );

	$items = $order->get_items();

	foreach ( $items as $item ) :

		$product = $item->get_data();

		$product_id = $product['product_id'];

		$product_type = get_field( 'product_type', $product_id );

		switch ($product_type) {
			case 'Accessory':
				$ordered_products[$product_type][$product_id] = array_merge( $product, (array)get_accessory_product_meta_data($product['product_id']) );
				break;
			case 'Coffee':
				$ordered_products[$product_type][$product_id] = array_merge( $product, (array)get_coffee_product_meta_data($product['product_id']) );
				break;
			case 'Machine':
				$ordered_products[$product_type][$product_id] = array_merge( $product, (array)get_machines_product_meta_data($product['product_id']) );
				break;
		}

	endforeach;

	return $ordered_products;

} // get_ordered_products()


if ( isset($_POST['form-for'])
	&& isset($_POST['nespresso_post_nonce_field'])
	&& wp_verify_nonce($_POST['nespresso_post_nonce_field'], 'nespresso_post_nonce')
) :

	$form_for = sanitize_text_field( $_POST['form-for'] );

	switch ( $form_for ) {
		case 'my-personal-information' :
			save_update_my_personal_information();
		break;
		case 'change-password' :
			save_update_my_personal_information_change_password();
		break;
		case 'my-machine-form' :
			save_update_my_machine_form();
		break;
		case 'my-machine-remove' :
			save_update_my_machine_remove();
		break;
		case 'my-addresses-billing' :
		case 'my-addresses-shipping' :
			save_update_my_addresses($form_for);
		break;
	}

endif;

/**
 * save my addresses
 */
function save_update_my_addresses($form_for=null) {

	session_start();

    $nespresso_my_addresses_hook_script = '';

    $validation = new Validation();

    if ( $form_for == 'my-addresses-billing') :

	    if ( !$validation->valid_first_name( $_POST['billing_first_name'] ) && @$_POST['billing_first_name'] ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_first_name();MyAddresses.showEditForm("billing");';

	    elseif ( !$validation->valid_last_name( $_POST['billing_last_name'] ) && @$_POST['billing_last_name'] ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_first_name();MyAddresses.showEditForm("billing");';

	    elseif ( !$validation->valid_billing_address_1() ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_address_1();MyAddresses.showEditForm("billing");';

	    elseif ( !$validation->valid_billing_address_2() && @$_POST['billing_address_2'] ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_address_2();MyAddresses.showEditForm("billing");';

	    elseif ( !$validation->valid_billing_city() ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_city();MyAddresses.showEditForm("billing");';

	    // elseif ( !$validation->valid_billing_postcode() ) :
	    //     $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_postcode();MyAddresses.showEditForm("billing");';

	    elseif ( !$validation->valid_billing_phone() && @$_POST['billing_phone'] != '' ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_phone();MyAddresses.showEditForm("billing");';

	    // elseif ( !$validation->valid_billing_state() ) :
	    //     $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_state();MyAddresses.showEditForm("billing");';

	    elseif ( !$validation->valid_billing_country() ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_country();MyAddresses.showEditForm("billing");';

	    endif;

    endif;

    if ( $form_for == 'my-addresses-shipping') :

	    if ( !$validation->valid_first_name( $_POST['shipping_first_name'] ) && @$_POST['shipping_first_name']) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_address_1();MyAddresses.showEditForm("shipping");';

	    elseif ( !$validation->valid_last_name( $_POST['shipping_last_name'] ) && @$_POST['shipping_last_name'] ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_address_1();MyAddresses.showEditForm("shipping");';

	    elseif ( !$validation->valid_billing_address_1( $_POST['shipping_address_1'] ) ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_address_1();MyAddresses.showEditForm("shipping");';

	    elseif ( !$validation->valid_billing_address_2( $_POST['shipping_address_2'] ) && @$_POST['shipping_address_2'] ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_address_2();$().showEditForm();';

	    // elseif ( !$validation->valid_billing_postcode( $_POST['shipping_postcode'] ) ) :
	    //     $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_postcode();MyAddresses.showEditForm("shipping");';

	    elseif ( !$validation->valid_billing_city( $_POST['shipping_city'] ) ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_city();MyAddresses.showEditForm("shipping");';

	    // elseif ( !$validation->valid_billing_state( $_POST['shipping_state'] ) ) :
	    //     $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_state();MyAddresses.showEditForm("shipping");';

	     elseif ( !$validation->valid_billing_country( $_POST['shipping_country'] ) ) :
	        $nespresso_my_addresses_hook_script = 'validation.set_valid(false).validate_billing_country();MyAddresses.showEditForm("shipping");';

	    endif;

    endif;

    if ( !$nespresso_my_addresses_hook_script ) :

    	$user = nespresso_get_user_data();

	 	global $nespresso_user_metas;
        foreach ( $_POST as $k => $v ) :
            if ( in_array($k, $nespresso_user_metas) ) :
                update_user_meta( $user->ID, $k, sanitize_text_field( $v ) );
            endif;
        endforeach;

        $nespresso_my_addresses_hook_script = 'validation.success_information_updated();';

	endif;

    $_SESSION['nespresso_my_addresses_hook_script'] = $nespresso_my_addresses_hook_script;

    //Update User to octopus
	do_action('admin_octopus_update_customer', $user);

   	wp_safe_redirect( $_POST['_wp_http_referer'] );

   	exit;

} // save_update_addresses()

/**
 * save personal information passowrd
 */
function save_update_my_personal_information_change_password() {

	session_start();

	$validation = new Validation();

	$nespresso_my_info_hook_script = '';

    if ( !$validation->valid_password_current() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_password_current();';

    elseif ( !$validation->valid_password() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_password();';

     elseif ( !$validation->valid_password_same() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_password_same();';

    elseif ( !$validation->valid_password_confirmation() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_password_confirmation();';

    else :

        /**
         * update the current user meta data
         */
        $user = nespresso_get_user_data();

        wp_set_password( sanitize_text_field( $_POST['password'] ), $user->ID );

        $nespresso_my_info_hook_script = 'validation.success_password_updated();';

    endif;

    $_SESSION['nespresso_my_info_hook_script'] = $nespresso_my_info_hook_script;

    wp_safe_redirect( $_POST['_wp_http_referer'] );

    exit;

} // save_update_my_personal_information_change_password()

/**
 * saving my-personal-information form from my-personal-information.php
 * @return [type] [description]
 */
function save_update_my_personal_information() {

	session_start();

	$validation = new Validation();

	$nespresso_my_info_hook_script = '';

    if ( !$validation->valid_first_name() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_first_name();';

    elseif ( !$validation->valid_last_name() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_last_name();';

    elseif ( !$validation->valid_date_of_birth() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_date_of_birth();';

    elseif ( !$validation->valid_email() ):
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_email();';

    elseif ( !$validation->valid_email_is_unique() ):
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_email_is_unique();';

    elseif ( !$validation->valid_email_confirmation() ):
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_email_confirmation();';

    elseif ( !$validation->valid_mobile() ) :
        $nespresso_my_info_hook_script = 'validation.set_valid(false).validate_mobile();';

    else :

        /**
         * update the current user meta data
         */
        $user = nespresso_get_user_data();
        update_user_meta( $user->ID, 'title', sanitize_text_field( $_POST['title'] ) );
        update_user_meta( $user->ID, 'first_name', sanitize_text_field( $_POST['first_name'] ) );
        // update_user_meta( $user->ID, 'middle_name', sanitize_text_field( $_POST['middle_name'] ) );
        update_user_meta( $user->ID, 'last_name', sanitize_text_field( $_POST['last_name'] ) );
        update_user_meta( $user->ID, 'date_of_birth', sanitize_text_field( $_POST['date_of_birth'] ) );
        update_user_meta( $user->ID, 'email', sanitize_text_field( $_POST['email'] ) );
        update_user_meta( $user->ID, 'mobile', sanitize_text_field( $_POST['mobile'] ) );
        // update_user_meta( $user->ID, 'phone', sanitize_text_field( $_POST['phone'] ) );
        update_user_meta( $user->ID, 'country_code', sanitize_text_field( $_POST['country_code'] ) );
        if (isset( $_POST['newsletter_subscription'])) {
	        update_user_meta( $user->ID, 'newsletter_subscription', sanitize_text_field( $_POST['newsletter_subscription'] ) );
        }else{
	        update_user_meta( $user->ID, 'newsletter_subscription', null);
        }


        $nespresso_my_info_hook_script = 'validation.success_information_updated();';

    endif;

    $_SESSION['nespresso_my_info_hook_script'] = $nespresso_my_info_hook_script;

    //Update User to octopus
	// do_action('admin_octopus_update_customer', $user);

    wp_safe_redirect( $_POST['_wp_http_referer'] );

    exit;

} // save_update_my_personal_information()

/**
 * saving my-machines adding from my-machines.php
 */
function save_update_my_machine_form() {

	session_start();

	global $nespresso_user_metas;

	$update_user_meta = [];
	if ( isset($nespresso_user_metas['my_machines'])) :

		$day = sanitize_text_field($_POST['day']);
		$month = sanitize_text_field($_POST['month']);
		$year = sanitize_text_field($_POST['year']);

		$update_user_meta['purchase_date'] = "$year-$month-$day";
		$update_user_meta['serial_no'] = sanitize_text_field($_POST['serial_no']);
		$update_user_meta['product_id'] = sanitize_text_field($_POST['product_id']);
        $update_user_meta['product_sku'] = sanitize_text_field($_POST['product_sku']);
		$update_user_meta['obtained_by'] = sanitize_text_field($_POST['obtained_by']);
		$update_user_meta['product_color_variant_index'] = sanitize_text_field($_POST['product_color_variant_index']);

	endif;


 	$user = nespresso_get_user_data();

	// get existing my machines
	$my_machines = get_user_meta($user->ID, 'my_machines');
	$my_machines = isset($my_machines[0]) ? $my_machines[0] : [];
	$old_machine_data = [];
	// for edit/updated
	if ( isset($_POST['my_machine_index']) && $_POST['my_machine_index'] !== '' ) :
		$action = 'update';
		$my_machine_index = sanitize_text_field($_POST['my_machine_index']);
		$old_machine_data = $my_machines[$my_machine_index];
		$my_machines[$my_machine_index] = $update_user_meta;


		$_SESSION['flash_message'] =['message'=>'Machine updated', 'type' => 'success'];

	// for new machine
	else :
		$action = 'add';
		$my_machines[] = $update_user_meta;
		$_SESSION['flash_message'] =['message'=>'Added new machine', 'type' => 'success'];

	endif;

	update_user_meta($user->ID, 'my_machines', $my_machines);

	//Update User to my machine
	$args = [
		'user' => $user,
		'update_user_meta' => $update_user_meta,
		'action' => $action,
		'old_machine_data' => $old_machine_data
	];

	do_action('admin_octopus_update_machine', $args);

	wp_safe_redirect( $_POST['_wp_http_referer'] );

	exit;
} // save_update_my_machine_form()

/**
 * removing my-machines adding from my-machines.php
 */
function save_update_my_machine_remove() {

	$my_machine_index = sanitize_text_field($_POST['my_machine_index']);

	$user = nespresso_get_user_data();

	$my_machines = get_user_meta($user->ID, 'my_machines');
	$my_machines = isset($my_machines[0]) ? $my_machines[0] : [];

	$machine_data = $my_machines[$my_machine_index];
	if ( isset($my_machines[$my_machine_index]) )
		unset($my_machines[$my_machine_index]); // remove the machine from index

	update_user_meta($user->ID, 'my_machines', $my_machines);

	// session_start();
	// $_SESSION['flash_message'] =['message'=>'', 'type' => 'success'];

	//Update User to my machine

	// $args = [
	// 	'user' => $user,
	// 	'machine_data' => $machine_data
	// ];

	// do_action('admin_octopus_delete_machine', $args);

	wp_safe_redirect( $_POST['_wp_http_referer'] );
	exit;

} //save_update_my_machine_remove()

