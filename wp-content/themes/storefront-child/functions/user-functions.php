<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
// @todo : alter adding machine in registration

/**
 * meta data comming from woocommerce customer/user inputs
 */
global $nespresso_user_metas;
$nespresso_user_metas = [
    'title', // string
    'first_name', // string
    'middle_name', // string
    'last_name', // string
    'date_of_birth', // string
    'email', // string
    'mobile', // string
    'phone', // string
    'has_club_membership_no', // boolean
    'club_membership_no', // integer
    'postal_code', // string
    'billing_email', // string
    'billing_first_name', // string
    'billing_last_name', // string
    'billing_contact_name', // string
    'billing_company', // string
    'billing_address_1', // string
    'billing_address_2', // string
    'billing_postcode', // string
    'billing_city', // string
    'billing_state', // string
    'billing_country', // string
    'billing_phone', // string
    'shipping_first_name', // string
    'shipping_last_name', // string
    'shipping_company', // string
    'shipping_address_1', // string
    'shipping_address_2', // string
    'shipping_city', // string
    'shipping_state', // string
    'shipping_postcode', // string
    'shipping_country', // string
    'order_comments',
    'newsletter_subscription', // boolean
    'has_machine_registered', // boolean
    'my_machines' => [ // array
    	'serial_no', // string
	    'product_id', // integer
        'product_sku', // string
	    'purchase_date', // datetime
	    'obtained_by', // string
	    'product_color_variant_index', // string
    ],
    'shipping_mode', // string
    'shipping_mode_standard_options', // string
    'payment_mode', // string
    'express_checkout_active', // boolean
    // 'receive_alerts_on_my_capsule_order', // boolean
    'receive_alerts_on_for_descaling', // boolean
    'subscribed_in_nesspresso_club', // boolean
    'contact_preferences', // array
    'subscribed_in_nesspresso_news', // boolean
    'pick_up_location', // string
    'country_code', // string
    'billing_mobile', // string
    'billing_country_code', // string
    'shipping_country_code', // string
    'shipping_mobile', // string
];


/**
 * redirect login woocoommerce
 */
add_filter('woocommerce_login_redirect', 'wc_login_redirect');

function wc_login_redirect( $redirect_to ) {	     
    ob_start();
	global $woocommerce;	
	global $wp;
	if($woocommerce->cart->is_empty())
		return home_url(add_query_arg(array(),$wp->request)) . '/my-account';
	else{
		 $checkout_url = $woocommerce->cart->get_checkout_url();
		 wp_redirect($checkout_url);
		 exit;
	}
}

/**
 * redirect login woocoommerce
 */
function logout_redirect( $redirect_to ) {

	global $wp;
	return home_url(add_query_arg(array(),$wp->request)) . '/login';
}
add_filter('wp_logout', 'logout_redirect');

function add_phone_edit() {
	 $customer = nespresso_get_user_data($_GET['user_id']);
	// echo '<pre>',print_r($customer),'</pre>';
	?>
	<h2><?php _e( 'Phone Information'); ?></h2>

	<table class="form-table">
	<tr class="user-mobile-wrap">
		<th><label for="mobile"><?php _e('Customer phone number') ?></label></th>
		<td><input type="text" name="meta_mobile" id="mobile" value="<?php echo esc_attr( $customer->mobile ) ?>" class="regular-text code" /></td>
	</tr>
	<tr class="user-mobile-wrap">
		<th><label for="billing_mobile"><?php _e('Billing  phone number') ?></label></th>
		<td><input type="text" name="meta_billing_mobile" id="billing_mobile" value="<?php echo esc_attr( $customer->billing_mobile ) ?>" class="regular-text code" /></td>
	</tr>
	<tr class="user-mobile-wrap">
		<th><label for="shipping_mobile"><?php _e('Shipping phone number') ?></label></th>
		<td><input type="text" name="meta_shipping_mobile" id="shipping_mobile" value="<?php echo esc_attr( $customer->shipping_mobile ) ?>" class="regular-text code" /></td>
	</tr>
	</table>
	<?php

}
function save_phone_number($userid)
{
	//echo $userid;
	//print_r($_POST);exit;
	update_user_meta( $userid, 'mobile', sanitize_text_field( $_POST['meta_mobile'] ) );
	update_user_meta( $userid, 'billing_mobile', sanitize_text_field( $_POST['meta_billing_mobile'] ) );
	update_user_meta( $userid, 'shipping_mobile', sanitize_text_field( $_POST['meta_shipping_mobile'] ) );
}
add_action( 'show_user_profile', 'add_phone_edit' );
add_action( 'edit_user_profile', 'add_phone_edit' );
add_action('personal_options_update', 'save_phone_number');
add_action('edit_user_profile_update', 'save_phone_number');
/**
 * post reistration of new customer and some validation
 */
function nespresso_registration_errors($errors, $sanitized_user_login, $user_email ) {

	if ( !isset( $_POST['title'] ) )
		$errors->add( 'title_error', __( '<strong>Error</strong>: Title is required!', 'woocommerce' ) );

	if ( !isset( $_POST['first_name'] ) )
		$errors->add( 'first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );

	if ( !isset( $_POST['last_name'] ) )
		$errors->add( 'last_name_error', __( '<strong>Error</strong>: Last name is required!', 'woocommerce' ) );

	if ( !isset( $_POST['date_of_birth'] ) )
		$errors->add( 'date_of_birth_error', __( '<strong>Error</strong>: Date of Birth is required!', 'woocommerce' ) );

	if ( !isset( $_POST['email'] ) )
		$errors->add( 'email_error', __( '<strong>Error</strong>: Email is required!', 'woocommerce' ) );

	if ( !isset( $_POST['email'] ) && !preg_match("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", $_POST['email']) )
		$errors->add( 'email_error', __( '<strong>Error</strong>: Invalid Email!', 'woocommerce' ) );

	if ( !isset( $_POST['mobile'] ) )
		$errors->add( 'mobile_error', __( '<strong>Error</strong>: Mobile is required!', 'woocommerce' ) );

	if ( !isset( $_POST['password'] ) && !preg_match("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!~@$%^&*-]).{4,}$", $_POST['password']) )
		$errors->add( 'password_error', __( '<strong>Error</strong>: Invalid Password!', 'woocommerce' ) );

	if ( !isset( $_POST['billing_address_1'] ) )
		$errors->add( 'billing_address_1_error', __( '<strong>Error</strong>: Delivery Address line 1 is required!', 'woocommerce' ) );

	if ( !isset( $_POST['billing_city'] ) )
		$errors->add( 'billing_city_error', __( '<strong>Error</strong>: Delivery City code is required!', 'woocommerce' ) );

	// if ( !isset( $_POST['billing_state'] ) )
	// 	$errors->add( 'billing_state_error', __( '<strong>Error</strong>: State is required!', 'woocommerce' ) );

	// if ( !isset( $_POST['billing_postcode'] ) )
	// 	$errors->add( 'billing_postcode_error', __( '<strong>Error</strong>: Delivery Post code is required!', 'woocommerce' ) );

	if ( isset($errors->errors) && count($errors->errors) > 0 ) {
		$_SESSION['registration_errors'] = $errors->errors;
		$_SESSION['registration_data'] = $_POST;
		wp_redirect('/register');
		exit;
	}

	unset($_SESSION['registration_errors']);
	unset($_SESSION['registration_data']);

	return $errors;

}
add_filter( 'registration_errors', 'nespresso_registration_errors', 10, 3 );
add_filter( 'register_post', 'nespresso_registration_errors', 9, 3 );

/**
 * saving registration data from form
 */
function nespresso_registration_save( $user_id ) {

	global $nespresso_user_metas;

	/**
	 * for an unknown reason, I have to update the password aftere wp-registration
	 * might be a bug on woocommerce + wordpress or other plugins maybe
	 */
    wp_set_password(sanitize_text_field($_POST['password']), $user_id);
    foreach ( $nespresso_user_metas as $meta ) :
    	if ( !is_array($meta) && isset($_POST[$meta]) ) :
			update_user_meta( $user_id, $meta, sanitize_text_field( $_POST[$meta] ) );
		endif;
	endforeach;

	$day = sanitize_text_field($_POST['day']);
	$month = sanitize_text_field($_POST['month']);
	$year = sanitize_text_field($_POST['year']);

	$product_id = isset($_POST['product_id']) && $_POST['product_id'] ? $_POST['product_id'] : 0;
    $product_sku = isset($_POST['product_sku']) && $_POST['product_sku'] ? $_POST['product_sku'] : 0;

	$update_user_meta['purchase_date'] = "$year-$month-$day";
	$update_user_meta['serial_no'] = sanitize_text_field($_POST['serial_no']);
	$update_user_meta['product_id'] = sanitize_text_field($product_id);
    $update_user_meta['product_sku'] = sanitize_text_field($product_sku);
	$update_user_meta['obtained_by'] = sanitize_text_field( $_POST['obtained_by'] );
	// $update_user_meta['product_color_variant_index'] = sanitize_text_field($_POST['product_color_variant_index']);

	update_user_meta($user_id, 'my_machines', [$update_user_meta]);

	$user = get_user_by('ID', $user_id);
	$user = isset($user->data) ? $user->data : null;

	$new_customer_data = apply_filters( 'woocommerce_new_customer_data', array(
			'user_login' => $user->user_login,
			'user_pass'  => $user->user_pass,
			'user_email' => $user->user_email,
			'role'       => 'customer',
		) );

	do_action('woocommerce_created_customer', $user_id, $new_customer_data, false);

	//Add User to octopus
	// do_action('admin_octopus_create_customer', $user);

	//autlogin after registration and redirect to welcome-offer
    $creds = array();
	$creds['user_login'] = $_POST['email'];
	$creds['user_password'] = $_POST['password'];
	$creds['remember'] = true;
	$user = wp_signon( $creds, false );
	session_start();
	$_SESSION['flash_message'] =['message'=>'You have registered a new account. Please check your email for more details.', 'type' => 'success'];
	if ( is_wp_error($user) ) {
		wp_redirect( home_url() . '/login' );
	}
	else
	{
	    if( WC()->cart->get_cart_contents_count() ==  0 )
        {
	        wp_redirect( home_url() . '/my-account' );
        }
	    else
        {
	        wp_redirect( home_url() . '/checkout' );
        }
	    
        //wp_redirect( home_url() . '/my-account' );
		// wp_redirect( home_url() . '/welcome-offer' );
	}
	exit;

}
add_action( 'user_register', 'nespresso_registration_save', 10, 1 );

/**
 * custom build login process only for customer logins
 * and if valid, logs-in the user
 *
 * @return  redirect
 */
function nespresso_customer_login() {

	global $user_ID;

	// $user = wp_get_current_user();

	// dd($user);

	if ( !isset($_SESSION) ||
		!isset($_SESSION['nespresso_token']) ||
		$_SESSION['nespresso_token'] != session_id()
	) {
		wp_redirect('/');
		exit;
	}

	global $my_account_url;

	// if user already logged in
	if ($user_ID) {
		wp_redirect($my_account_url);
		exit;
	}

	// if nothing is posted
    if( !isset($_POST) || empty($_POST) ) {
    	wp_redirect('/');
    	exit;
    }

	$username = null;
	if ( isset($_REQUEST['username']) && $_REQUEST['username'] )
		$username = $_REQUEST['username'];
	if ( isset($_REQUEST['user_login']) && $_REQUEST['user_login'] )
		$username = $_REQUEST['user_login'];

	$password = null;
	if ( isset($_REQUEST['password']) && $_REQUEST['password'] )
		$password = $_REQUEST['password'];
	if ( isset($_REQUEST['user_password']) && $_REQUEST['user_password'] )
		$password = $_REQUEST['user_password'];

	$remember = null;
	if ( isset($_REQUEST['remember']) && $_REQUEST['remember'] )
		$remember = $_REQUEST['remember'];
	if ( isset($_REQUEST['rememberme']) && $_REQUEST['rememberme'] )
		$remember = $_REQUEST['rememberme'];

    $login_data = array();
    $login_data['user_login'] = sanitize_text_field($username);;
    $login_data['user_pass'] = sanitize_text_field($password);
    $login_data['remember'] = sanitize_text_field($remember);

    $user = get_user_by('email', $login_data['user_login']);
    if ( !$user )
    	$user = get_user_by('user_login', $login_data['user_login']);

    if (!$user)
    	wp_redirect( home_url() .'/login?login-status=failed' );

    if ( !wp_check_password($login_data['user_pass'], $user->user_pass) ) {
    	$_SESSION['login_fail_message'] = "Invalid username or password";
    	wp_redirect('/login');
    } else {
    	// auto login registered user
	    wp_set_current_user($user->ID);
	    wp_set_auth_cookie($user->ID);
	    wp_redirect('/my-account');
    }

    exit;
    // $user_verify = wp_signon( $login_data, false );
    // $login_success = is_wp_error($user_verify);
}
add_action('login_init ', 'nespresso_customer_login');
add_action('wp_authenticate ', 'nespresso_customer_login');
// add_action('wp_ajax_nopriv_nespresso_customer_login', 'nespresso_customer_login');
// add_action('wp_ajax_nespresso_customer_login', 'nespresso_customer_login');


function nespresso_login_fail( $username ) {
    //redirect to custom login page and append login error flag
    wp_redirect( home_url() .'/login?login-status=failed' );
    exit;
}
add_action( 'wp_login_failed', 'nespresso_login_fail' );

/**
 * get current user data
 * includes billing and shipping data from woocommerce
 *
 * @return object
 */
function nespresso_get_user_data($user_id=null) {

	global $nespresso_user_metas;

	if ( $user_id ) :
		$user = get_user_by('ID', $user_id);

		if ( !$user )
			return;

		$user = $user->data;

	else :
		$user = wp_get_current_user();
		$user = $user->data;
	endif;

	if ( !$user || empty( (array)$user ) )
		return null;

	foreach ( $nespresso_user_metas as $k => $meta ) {

		if ( is_array($meta) ) :
			$v = get_user_meta($user->ID, $k);
			$user->$k = $v;
		else :
			$v = get_user_meta($user->ID, $meta);

			$user->$meta = null;
			if ( !empty($v) && $v && isset($v[0]))
				$user->$meta = $v[0];
		endif;

	}

	return $user;

} // nespresso_get_user_data()
