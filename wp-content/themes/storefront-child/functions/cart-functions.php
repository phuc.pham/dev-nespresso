<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with coffee products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
/*
* Automatically adding the product to the cart.
*/

function welcomcoffe_add_product_to_cart( $product_id ) {

	$product_category_id 	= 19; // cricket bat category id

	$product_cats_ids 	= wc_get_product_term_ids( $product_id, 'product_cat' );

    if ( in_array( $product_category_id, $product_cats_ids ) ) {
		$product_data  = wc_get_product( $product_id);
		if($product_data->get_type()!='woosb')
		{
        $free_product_id = 399808;  // Product Id of the free product which will get added to cart
        $found 		= false;

        //check if product already in cart
        WC()->cart->add_to_cart( $free_product_id, 1,0 );
		}
    }    
}
function welcomcoffe_remove_product_to_cart( $key ) {
    $product_id ='';
    $keyfree = '';
    $free_product_id = 399808;
    $bqty = $getqty = 0;
    foreach( WC()->cart->get_cart() as $k=>$cart_item ){ 
        if($cart_item['product_id'] == $free_product_id){
            $keyfree = $k;
            $getqty = $cart_item['quantity'];
        }
        if($k === $key){
            $product_id = $cart_item['product_id'];
            $bqty = $cart_item['quantity'];
            //break;
        }       
    }    
	$product_category_id 	= 19; // cricket bat category id

	$product_cats_ids 	= wc_get_product_term_ids( $product_id, 'product_cat' );

    if ( in_array( $product_category_id, $product_cats_ids ) ) {
         // Product Id of the free product which will get added to cart
        $found 		= false;
        //dd([$getqty,$bqty]);
      // if($getqty!=$bqty){
           // WC()->cart->remove_cart_item( $keyfree);			
       // }else{
            $product_data  = wc_get_product( $product_id);
		if($product_data->get_type()!='woosb')
		{
		   WC()->cart->set_quantity($keyfree,$getqty-$bqty);
		}
     //   }
        //check if product already in cart
        //WC()->cart->remove_cart_item( $free_product_id);
    }    
}
//add_action( 'wp_ajax_add_to_cart', 'welcomcoffe_add_product_to_cart', 10, 2 );
/**
 * get the cart items
 *
 * @return json
 */
function fetch_cart() {

    $products = WC()->cart->get_cart();

    $cart_items = [];
    foreach ( $products as $cart_item_key => $product) :

        $k = $cart_item_key;
        // $k =  $product['product_id'];

        $cart_items[$k] = $product;
        $cart_items[$k]['id'] =  $product['product_id'];
        $cart_items[$k]['name'] = get_the_title( $product['product_id'] );
        $cart_items[$k]['price'] = (int)get_post_meta( $product['product_id'], '_price', true );
        $cart_items[$k]['type'] = get_field( 'product_type', $product['product_id'] );
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product['product_id'] ), 'single-post-thumbnail' );
        $cart_items[$k]['image_url'] = isset($image[0]) ? $image[0] : '';
        $cart_items[$k]['cart_item_key'] = $cart_item_key;
        // dd($product);
        if ( $product['variation_id'] ) :
            $variation = new WC_Product_Variation( $product['variation_id'] );
            $variation_product= $variation->get_data();

            $cart_items[$k]['name'] = $variation_product['name'];

            $v_image = wp_get_attachment_image_src( $variation_product['image_id']);
            $cart_items[$k]['image_url'] = isset($v_image[0]) ? $v_image[0] : $cart_items[$k]['image_url'];

        endif;

        $aromatic_profile = get_post_meta($product['product_id'], 'aromatic_profile', true );
        $discovery_offer = '';
        if ($aromatic_profile === 'Varied'):
            $discovery_offer = 'discovery-offer-item';
        endif;
        $cart_items[$k]['discovery_offer'] = $discovery_offer;

    endforeach;

    wp_send_json($cart_items);
} // fetch_cart()
add_action('wp_ajax_nopriv_fetch_cart', 'fetch_cart');
add_action('wp_ajax_fetch_cart', 'fetch_cart');

/**
 * add/renew products in cart
 */
function add_to_cart() {

    if ( !isset($_POST['products']) || !is_array($_POST['products']) ) {
        wp_send_json([ 'status' => 'failed', 'data' => 0, 'message' => 'Invalid product']);
        exit;
    }

    $error_message = '';

    foreach ( $_POST['products'] as $product ) :

        $product = wc_clean( $product );

        $product_data  = wc_get_product( $product['id']);

        // check if valid product
        if ( !isset($product['id']) ||
            get_post_status($product['id']) == 'trash' ||
            !$product_data
        ) :
            $error_message = $error_message ? $error_message : 'Product does not exists.';
        endif;

        if ( !isset($product['quantity']) || !$product['quantity'] || (int)$product['quantity'] < 1 ) {
            $error_message = $error_message ? $error_message : 'Please add quantity greater than zero.';
        }

        $product_data = $product_data->get_data();

        if ( $product_data['manage_stock'] &&
            $product_data['stock_quantity'] >= $product['quantity'] &&
            $product_data['stock_status'] == 'instock'
        ) :
            add_cart_item($product);
        elseif ( !$product_data['manage_stock'] && $product_data['stock_status'] == 'instock') :
            
           add_cart_item($product);

        else :
            if ( $product_data['stock_status'] != 'instock' ) :
               $error_message = $error_message ? $error_message : 'Product is out of stock';
            else :
               $error_message = $error_message ? $error_message : 'Product stock has not enough stock for your order';
            endif;
        endif;

    endforeach;
    if ( $error_message )
        wp_send_json([ 'status' => 'failed', 'data' => 0, 'message' => $error_message]);
    else
        wp_send_json(['status' => 'success', 'data' => fetch_cart() ]);
    exit;

} // add_to_cart()
add_action('wp_ajax_nopriv_add_to_cart', 'add_to_cart');
add_action('wp_ajax_add_to_cart', 'add_to_cart');

function add_cart_item($product) {

    if ( !isset($product['id']) || !isset($product['quantity']) ) :
        return false;
    endif;

   
    $product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product['id'] ) );
    $quantity          = empty( $product['quantity'] ) ? 1 : wc_stock_amount( $product['quantity'] );
    $variation_id          = isset( $product['variation_id'] ) ? $product['variation_id'] :  0;

    $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, null, null );
   // dd($passed_validation);
    if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity, $variation_id ) ) :
        WC()->cart->discount_total = 100;
        do_action( 'woocommerce_ajax_added_to_cart', $product_id );

        if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
            wc_add_to_cart_message( $product_id );
        }
        welcomcoffe_add_product_to_cart($product_id);
        return true;

    endif;

    return false;

} // add_cart_item()

function remove_cart_item() {

    $cart_item_key = wc_clean( $_POST['cart_item_key'] ); 
    welcomcoffe_remove_product_to_cart($cart_item_key);   
    $item_deleted = wc()->cart->remove_cart_item( $cart_item_key );

    if ( !$item_deleted )
        wp_send_json(['status' => 'failed', 'data' => 0 ]);
    else{        
        wp_send_json(['status' => 'success', 'data' => 1 ]);
    }
    exit;
}
add_action('wp_ajax_nopriv_remove_cart_item', 'remove_cart_item');
add_action('wp_ajax_remove_cart_item', 'remove_cart_item');

/**
 * empty the cart
 *
 * @return [type] [description]
 */
function empty_cart() {
    $empty_cart =  WC()->cart->empty_cart();

     if ( !$empty_cart )
        wp_send_json(['status' => 'failed', 'data' => 0 ]);
    else
        wp_send_json(['status' => 'success', 'data' => 1 ]);
    exit;
}
add_action('wp_ajax_nopriv_empty_cart', 'empty_cart');
add_action('wp_ajax_empty_cart', 'empty_cart');


add_action('wp_ajax_reorder_item', 'reorder_item');

function reorder_item()
{
    global $wpdb;
    $arr[] = 'wc-completed';
    $customer_orders = get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => get_current_user_id(),
        'post_type' => wc_get_order_types('view-orders'),
        'post_status' => $arr,
        'order' => 'DESC',
    )));
    $order = wc_get_order($customer_orders[0]->ID);
    $ordered_products = get_ordered_products( $customer_orders[0]->ID );
    foreach ($order->get_items() as $key => $value) {
     $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $value->get_product_id(), $value->get_quantity(), $value->get_variation_id(), null, null );
        if ($value->get_variation_id())
            $product = wc_get_product($value->get_variation_id());
        else
            $product = wc_get_product($value->get_product_id());

         if ($product->get_stock_quantity()) {
             WC()->cart->add_to_cart( $value->get_product_id(), $value->get_quantity(), $value->get_variation_id() );
         }
         if ($product->get_stock_quantity() === 0) {
            $data[] = $product->name;
         }
    }
    wp_send_json(['prod' => $data]);
    return null;
}

add_action('wp_ajax_reorder_item_co', 'reorder_item_co');

function reorder_item_co()
{
    global $wpdb;
    $id = $_POST['id'];
    $order = wc_get_order($id);
    $ordered_products = get_ordered_products( $id );
    $data = [];
    foreach ($order->get_items() as $key => $value) {
     $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $value->get_product_id(), $value->get_quantity(), $value->get_variation_id(), null, null );
        if ($value->get_variation_id())
            $product = wc_get_product($value->get_variation_id());
        else
            $product = wc_get_product($value->get_product_id());

         if ($product->get_stock_quantity()) {
             WC()->cart->add_to_cart( $value->get_product_id(), $value->get_quantity(), $value->get_variation_id() );
         }
         if ($product->get_stock_quantity() === 0) {
            $data[] = $product->name;
         }
    }
    wp_send_json(['prod' => $data]);
    return null;
}


function check_last_order()
{
    global $wpdb;
    $arr[] = 'wc-completed';
    $customer_orders = get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => get_current_user_id(),
        'post_type' => wc_get_order_types('view-orders'),
        'post_status' => $arr,
        'order' => 'DESC',
    )));
    if (empty($customer_orders)) {
        return false;
    }
    $order = wc_get_order($customer_orders[0]->ID);
    if (!$order) {
        return false;
    }
    $ordered_products = get_ordered_products( $customer_orders[0]->ID );
    return true;
}

function woo_add_cart_coys_discount() {

    global $woocommerce;
    $coys_discount = get_nespresso_coys_discount();
    if (isset($coys_discount['coys_discount_status']) && $coys_discount['coys_discount_status'] == 'Active') {
        $cartitems              = $woocommerce->cart->get_cart();
        $conditional_products   = $coys_discount['conditional_product'];
        $discounted_product     = $coys_discount['discounted_product'];
        $amount_percentage      = $coys_discount['coys_amount_percentage'];
        $discount_type          = $coys_discount['coys_discount_type'];
        $discount_label         = $coys_discount['discount_label'];
        $conditional_products   = preg_replace('/\s+/', '', $conditional_products); //remove spaces
        $conditional_products   = explode(',',$conditional_products);

        $condtion_1 = false;
        $condtion_2 = false;
        $condtion_2_count = 0;
        $condtion_1_count = 0;
        foreach($cartitems as $cartitem) {

            if (in_array($cartitem['data']->get_sku(), $conditional_products)){
                $condtion_1 = true;
                $condtion_1_count = $condtion_1_count+$cartitem['quantity'];
            }
            if ($discounted_product == $cartitem['data']->get_sku()){
                $condtion_2 = true;
                $amount = $cartitem['data']->get_price();
                $condtion_2_count = $condtion_2_count+$cartitem['quantity'];
            }
        }
        if ($condtion_1 && $condtion_2) {
            $multiplier         = $condtion_2_count <= $condtion_1_count ? $condtion_2_count : $condtion_1_count;
            if ($discount_type == 'Fixed Amount') {
                $discount_amount = $amount_percentage * $multiplier;
            } else {
                $amount             = $amount * $multiplier;
                $percent            = $amount_percentage/100;
                $discount_amount    = round($amount * $percent, 2);
            }
            if ($discount_amount > 0) {
                $discount_amount = -abs($discount_amount);
                $woocommerce->cart->add_fee( $discount_label, $discount_amount);
            }

        }
    }
}
add_action( 'woocommerce_cart_calculate_fees', 'woo_add_cart_coys_discount' );

function chekout_validation() {

    $products = WC()->cart->get_cart();

    //sleeve checker
    $sleeve_quantity = 0;
    $with_sleeve = false;
    $sleeve_checker = true;
    //capsule checker
    $capsule_checker = true;
    $with_capsule = false;
    $capsule_quantity = 0;

    $missing_capsule = 20;
    foreach ($products as $key => $product) {
        $custom_fields = get_coffee_product_meta_data($product['product_id'], true);
        $bundle = isset($product['woosb_ids']) || isset($product['woosb_parent_id']) ;
        //dd( $bundle);
        //capsule validation
        if ($custom_fields->product_type == 'Coffee' && !$custom_fields->qty_x1 && !$bundle) {
            $with_capsule = true;
            $capsule_quantity = $capsule_quantity+$product['quantity'];
        }
        //sleeve validation
        if ($custom_fields->sleeve && !$custom_fields->qty_x1 && !$bundle) {
            $with_sleeve = true;
            $sleeve_quantity = $sleeve_quantity+$custom_fields->sleeve_quantity;
        }

    }

    if ($with_sleeve && $sleeve_quantity % 5 != 0 ) {
        $sleeve_checker = false;
        $missing_capsule = 5 - ($sleeve_quantity % 5);
        $missing_capsule = $missing_capsule * 5;
        ?><script type="text/javascript">
            window.sleeve_checker = false;
            window.missing_capsule = '<?= $missing_capsule ?>';
        </script><?php
    }

    if ($with_capsule && $capsule_quantity % 50 != 0 ) {
        $capsule_checker = false;
        $missing_capsule = 50 - ($capsule_quantity % 50);
        ?><script type="text/javascript">
            window.capsule_checker = false;
            window.missing_capsule = '<?= $missing_capsule ?>';
        </script><?php
    }

    return compact('sleeve_checker', 'capsule_checker', 'missing_capsule');
}

function discount_validation() {

    $products = WC()->cart->get_cart();

    //sleeve checker
    $sleeve_quantity = 0;
    $with_sleeve = false;
    $sleeve_checker = true;
    //capsule checker
    $capsule_checker = true;
    $with_capsule = false;
	$discount_template = '';
    $capsule_quantity = 0;
	
	$discount_template = get_option('nespresso_discount_template') ? get_option('nespresso_discount_template') : '<p style="text-transform: uppercase;color: red;margin: 20px;font-size: large;">giam 20% neu mua 30 vien do , vo mua di !!! .</p>';
	
	
	$missing_capsule = 20;
    foreach ($products as $key => $product) {
        $custom_fields = get_coffee_product_meta_data($product['product_id'], true);
        $bundle = isset($product['woosb_ids']) || isset($product['woosb_parent_id']) ;
        //dd( $bundle);
        //capsule validation
        if ($custom_fields->product_type == 'Coffee' && !$custom_fields->qty_x1 && !$bundle) {
            $with_capsule = true;
            $capsule_quantity = $capsule_quantity+$product['quantity'];
        }
        //sleeve validation
        if ($custom_fields->sleeve && !$custom_fields->qty_x1 && !$bundle) {
            $with_sleeve = true;
            $sleeve_quantity = $sleeve_quantity+$custom_fields->sleeve_quantity;
        }

    }

    if ($with_sleeve && $sleeve_quantity % 3 != 0 ) {
        $sleeve_checker = false;
        $missing_capsule = 3 - ($sleeve_quantity % 3);
        $missing_capsule = $missing_capsule * 3;
        ?><script type="text/javascript">
            window.sleeve_checker = false;
            window.missing_capsule = '<?= $missing_capsule ?>';
            window.discount_template = '<?= $discount_template ?>';
        </script><?php
    }

    if ($with_capsule && $capsule_quantity % 30 != 0 ) {
        $capsule_checker = false;
        $missing_capsule = 30 - ($capsule_quantity % 30);
        ?><script type="text/javascript">
            window.capsule_checker = false;
            window.missing_capsule = '<?= $missing_capsule ?>';
            window.discount_template = '<?= $discount_template ?>';
        </script><?php
    }
	
    
    return compact('sleeve_checker', 'capsule_checker', 'missing_capsule','discount_template');
}


