<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * here short codes for the component templates and sub templates
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * shortcodes for pages
 * used in pages backend
 * in alphabetical order
 *
 * @var array
 */
global $nespresso_short_codes;
$nespresso_short_codes = [
	'nespresso_page_machine_range' => 'Machine range page content',
	'nespresso_page_accessory_list' => 'Accessory list page content',
	'nespresso_page_b2b' => 'B2b page content',
	'nespresso_page_coffee_list' => 'Coffee list page content',
	'nespresso_page_coffee_list_question' => 'Coffee list page question',
	'nespresso_page_coffee_list_question_thank_you' => 'Coffee list page question thank you',
	'nespresso_page_coffee_list_question_discover' => 'Coffee list page question discover',
	'nespresso_page_coffee_list_question_step_2' => 'Coffee list page question step 2',
	'nespresso_page_coffee_list_question_result' => 'Coffee list page question result',
	'nespresso_page_contact_us' => 'Contact Us page content',
	'nespresso_page_customer_services' => 'Customer Services page content',
	'nespresso_page_delivery_address' => 'Delivery Address page content',
	'nespresso_page_delivery_mode' => 'Delivery Mode page content',
	'nespresso_page_faq' => 'FAQ page content',
	'nespresso_page_recycling' => 'Recycling page content',
	'nespresso_page_gift_card' => 'Gift Card page content',
	'nespresso_page_home' => 'Home page content',
	'nespresso_page_login' => 'Login page content',
	'nespresso_page_lost_password' => 'Lost password page content',
	'nespresso_page_machine_list' => 'Machine list page content',
	'nespresso_page_my_account' => 'My account page content',
	'nespresso_page_my_addresses' => 'My addresses page content',
	'nespresso_page_my_alerts_subscriptions' => 'My alerts and subscriptions page content',
	'nespresso_page_my_contact_preferences' => 'My contact preferneces page content',
	'nespresso_page_my_express_checkout' => 'My express checkout page content',
	'nespresso_page_my_easyorders' => 'My EasyOrders page content',
	'nespresso_page_my_machines' => 'My machines page content',
	'nespresso_page_order_confirmation' => 'Order Confirmation page content',
	'nespresso_page_my_order_history' => 'Order History page content',
	'nespresso_page_payment' => 'Payment page content',
	'nespresso_page_personal_information' => 'Personal Information page content',
	'nespresso_page_register' => 'Register page content',
	'nespresso_page_reset_password' => 'Reset password page content',
	'nespresso_page_services' => 'Services page content',
	'nespresso_page_service' => 'Services page content',
	'nespresso_page_shopping_bag' => 'Shopping bag page content',
	'nespresso_page_store_locator' => 'Store locator page content',
	'nespresso_single_product_page_coffee' => 'Coffee single product page content',
	'nespresso_single_product_page_machine' => 'Machine single product page content',
	'nespresso_page_welcome_offer' => 'Welcome Offer page content',
	'nespresso_coffee_origins' => 'Coffee origins page content',
	'nespresso_coffee_range' => 'Coffee range page content',
	'nespresso_coffee_range_2' => 'Coffee range 2 page content',
	'nespresso_cafetera_essenza_mini' => 'Cafetera Essenza Mini page content',
	'nespresso_lattissima_touch_machines_machines_range' => 'Lattissima Touch Machines Range page content',
	'nespresso_inissia_machines_range' => 'Inissia Machines Range page content',
	'nespresso_pixie_machines_range' => 'Pixie Machines Range page content',
	'nespresso_citiz_machines_range' => 'Citiz Machines Range page content',
	'nespresso_langding_page_2' => 'Langding Page 2 content',
	'nespresso_langding_page_survey_1' => 'Langding Page 2 content Survey 1 ',
	'nespresso_langding_page_survey_2' => 'Langding Page 2 content Survey 2',
	'nespresso_langding_page_survey_3' => 'Langding Page 2 content Survey 3',
	'nespresso_langding_page_survey_4' => 'Langding Page 2 content Survey 4',
	'nespresso_langding_page_survey_5' => 'Langding Page 2 content Survey 5',
	'nespresso_langding_page_phone_login' => 'Langding Page 2 customer phone login',
	'nespresso_langding_page_thanks_you' => 'Langding Page 2 thanks you',
	'nespresso_store_locator' => 'Store locator Page',
];


/**
 * Welcome Offer
 *
 * @param  array|object $attributes
 */
function nespresso_page_welcome_offer( $attributes ) {
	get_template_part('components/welcome-offer');
}
add_shortcode( 'nespresso_page_welcome_offer', 'nespresso_page_welcome_offer' );

/**
 * B2B
 *
 * @param  array|object $attributes
 */
function nespresso_page_b2b( $attributes ) {
	get_template_part('components/b2b');
	add_action('wp_footer', 'b2b_footer', PHP_INT_MAX);
}
add_shortcode( 'nespresso_page_b2b', 'nespresso_page_b2b' );
function b2b_footer() {
	get_template_part('components/b2b-footer');
}

/**
 * Citiz Machine range
 *
 * @param  array|object $attributes
 */
function nespresso_citiz_machines_range( $attributes ) {
	get_template_part('components/citiz-machines-range');
}
add_shortcode( 'nespresso_citiz_machines_range', 'nespresso_citiz_machines_range' );


/**
 * Pixie Machine range
 *
 * @param  array|object $attributes
 */
function nespresso_pixie_machines_range( $attributes ) {
	get_template_part('components/pixie-machines-range');
}
add_shortcode( 'nespresso_pixie_machines_range', 'nespresso_pixie_machines_range' );

/**
 * Inissia Machine range
 *
 * @param  array|object $attributes
 */
function nespresso_inissia_machines_range( $attributes ) {
	get_template_part('components/inissia-machines-range');
}
add_shortcode( 'nespresso_inissia_machines_range', 'nespresso_inissia_machines_range' );

/**
 * Lattisima touch machine range
 *
 * @param  array|object $attributes
 */
function nespresso_lattissima_touch_machines_machines_range( $attributes ) {
	get_template_part('components/lattissima-touch-machines-machines-range');
}
add_shortcode( 'nespresso_lattissima_touch_machines_machines_range', 'nespresso_lattissima_touch_machines_machines_range' );

/**
 * Cafetera Essenza Mini
 *
 * @param  array|object $attributes
 */
function nespresso_cafetera_essenza_mini( $attributes ) {
	get_template_part('components/cafetera-essenza-mini');
}
add_shortcode( 'nespresso_cafetera_essenza_mini', 'nespresso_cafetera_essenza_mini' );

/**
 * Coffee Range
 *
 * @param  array|object $attributes
 */
function nespresso_coffee_range( $attributes ) {
	get_template_part('components/coffee-range');
}
add_shortcode( 'nespresso_coffee_range', 'nespresso_coffee_range' );

/**
 * Coffee Origins
 *
 * @param  array|object $attributes
 */
function nespresso_coffee_origins( $attributes ) {
	get_template_part('components/coffee-origins');
}
add_shortcode( 'nespresso_coffee_origins', 'nespresso_coffee_origins' );

/**
 * Accessory List
 *
 * @param  array|object $attributes
 */
function nespresso_page_accessory_list( $attributes ) {
	get_template_part('components/accessory-list');
}
add_shortcode( 'nespresso_page_accessory_list', 'nespresso_page_accessory_list' );

/**
 * Coffee List
 *
 * @param  array|object $attributes
 */
function nespresso_page_coffee_list( $attributes ) {
	get_template_part('components/coffee-list');
}
add_shortcode( 'nespresso_page_coffee_list', 'nespresso_page_coffee_list' );

/**
 * Contact Us
 *
 * @param  array|object $attributes
 */
function nespresso_page_contact_us( $attributes ) {
	get_template_part('components/contact-us');
}
add_shortcode( 'nespresso_page_contact_us', 'nespresso_page_contact_us' );

/**
 * Customer Services
 *
 * @param  array|object $attributes
 */
function nespresso_page_customer_services( $attributes ) {
	get_template_part('components/customer-services');
}
add_shortcode( 'nespresso_page_customer_services', 'nespresso_page_customer_services' );

/**
 * Delivery Address
 *
 * @param  array|object $attributes
 */
function nespresso_page_delivery_address( $attributes ) {
	get_template_part('components/delivery-address');
}
add_shortcode( 'nespresso_page_delivery_address', 'nespresso_page_delivery_address' );

/**
 * Delivery Mode
 *
 * @param  array|object $attributes
 */
function nespresso_page_delivery_mode( $attributes ) {
	get_template_part('components/delivery-mode');
}
add_shortcode( 'nespresso_page_delivery_mode', 'nespresso_page_delivery_mode' );

/**
 * FAQ
 *
 * @param  array|object $attributes
 */
function nespresso_page_faq( $attributes ) {
	get_template_part('components/faq');
}
add_shortcode( 'nespresso_page_faq', 'nespresso_page_faq' );


/**
 * recycling
 *
 * @param  array|object $attributes
 */
function nespresso_page_recycling( $attributes ) {
	get_template_part('components/recycling');
}
add_shortcode( 'nespresso_page_recycling', 'nespresso_page_recycling' );


/**
 * Gift Card
 *
 * @param  array|object $attributes
 */
function nespresso_page_gift_card( $attributes ) {
	get_template_part('components/gift-card');
}
add_shortcode( 'nespresso_page_gift_card', 'nespresso_page_gift_card' );

/**
 * Home Page
 *
 * @param  array|object $attributes
 */
function nespresso_page_home( $attributes ) {
	get_template_part('components/home');
}
add_shortcode( 'nespresso_page_home', 'nespresso_page_home' );

/**
 * Lost Password
 *
 * @param  array|object $attributes
 */
function nespresso_page_lost_password( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/lost-password');

}
add_shortcode( 'nespresso_page_lost_password', 'nespresso_page_lost_password' );

/**
 * Login form and registration link
 *
 * @param  array|object $attributes
 */
function nespresso_page_login( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/login');
}
add_shortcode( 'nespresso_page_login', 'nespresso_page_login' );

/**
 * Machine List
 *
 * @param  array|object $attributes
 */
function nespresso_page_machine_list( $attributes ) {
	get_template_part('components/machine-list');
}
add_shortcode( 'nespresso_page_machine_list', 'nespresso_page_machine_list' );

/**
 * My Addresses
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_addresses( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/my-addresses');

}
add_shortcode( 'nespresso_page_my_addresses', 'nespresso_page_my_addresses' );

/**
 * My Express Checkout
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_express_checkout( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/my-express-checkout');

}
add_shortcode( 'nespresso_page_my_express_checkout', 'nespresso_page_my_express_checkout' );

/**
 * My EasyOrders
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_easyorders( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/my-easyorders');

}
add_shortcode( 'nespresso_page_my_easyorders', 'nespresso_page_my_easyorders' );

/**
 * My Alerts and subscriptions
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_alerts_subscriptions( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/my-alerts-subscriptions');

}
add_shortcode( 'nespresso_page_my_alerts_subscriptions', 'nespresso_page_my_alerts_subscriptions' );

/**
 * My Contact and preferences
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_contact_preferences( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/my-contact-preferences');

}
add_shortcode( 'nespresso_page_my_contact_preferences', 'nespresso_page_my_contact_preferences' );

/**
 * My Machines
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_machines( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/my-machines');

}
add_shortcode( 'nespresso_page_my_machines', 'nespresso_page_my_machines' );

/**
 * My Addresses
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_account( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="/login";</script>';
	else
		get_template_part('components/my-account');

}
add_shortcode( 'nespresso_page_my_account', 'nespresso_page_my_account' );

/**
 * Order Confirmation
 *
 * @param  array|object $attributes
 */
function nespresso_page_order_confirmation( $attributes ) {
	get_template_part('components/order-confirmation');
}
add_shortcode( 'nespresso_page_order_confirmation', 'nespresso_page_order_confirmation' );

/**
 * Order Histroy
 *
 * @param  array|object $attributes
 */
function nespresso_page_my_order_history( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/my-order-history');

}
add_shortcode( 'nespresso_page_my_order_history', 'nespresso_page_my_order_history' );

/**
 * Order Summary
 *
 * @param  array|object $attributes
 */
function nespresso_page_order_summary( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/order-summary');

}
add_shortcode( 'nespresso_page_order_summary', 'nespresso_page_order_summary' );

/**
 * Payement
 *
 * @param  array|object $attributes
 */
function nespresso_page_payment( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/payment');

}
add_shortcode( 'nespresso_page_payment', 'nespresso_page_payment' );

/**
 * Personal Information
 *
 * @param  array|object $attributes
 */
function nespresso_page_personal_information( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/personal-information');

}
add_shortcode( 'nespresso_page_personal_information', 'nespresso_page_personal_information' );

/**
 * services page
 *
 * @param  array|object $attributes
 */
function nespresso_page_services( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( !is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/services');

}
add_shortcode( 'nespresso_page_services', 'nespresso_page_services' );


/**
 * Coffee single_product_page
 *
 * @param  array|object $attributes
 */
function nespresso_single_product_page_coffee( $attributes ) {
	get_template_part('components/coffee-page');
}
add_shortcode( 'nespresso_single_product_page_coffee', 'nespresso_single_product_page_coffee' );

/**
 * Machine single_product_page
 *
 * @param  array|object $attributes
 */
function nespresso_single_product_page_machine( $attributes ) {
	get_template_part('components/machine-page');
}
add_shortcode( 'nespresso_single_product_page_machine', 'nespresso_single_product_page_machine' );

/**
 * Register
 *
 * @param  array|object $attributes
 */
function nespresso_page_register( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/register');

}
add_shortcode( 'nespresso_page_register', 'nespresso_page_register' );

/**
 * Reset Password
 *
 * @param  array|object $attributes
 */
function nespresso_page_reset_password( $attributes ) {

	global $my_account_url;

	// if the current user is currently logged in, it will be redirected to login page
	if ( is_user_logged_in() )
		echo '<script type="text/javascript">window.location="'.$my_account_url.'";</script>';
	else
		get_template_part('components/reset-password');

}
add_shortcode( 'nespresso_page_reset_password', 'nespresso_page_reset_password' );

/**
 * Shopping Bag
 *
 * @param  array|object $attributes
 */
function nespresso_page_shopping_bag( $attributes ) {
	get_template_part('components/shopping-bag');
}
add_shortcode( 'nespresso_page_shopping_bag', 'nespresso_page_shopping_bag' );

/**
 * Store Locator
 *
 * @param  array|object $attributes
 */
function nespresso_page_store_locator( $attributes ) {
	get_template_part('components/store-locator');
}
add_shortcode( 'nespresso_page_store_locator', 'nespresso_page_store_locator' );

/**
 * display the short codes list in admin pages page
 */
add_action('add_meta_boxes', 'add_nespresso_shortcodes_meta');
function add_nespresso_shortcodes_meta()
{
    add_meta_box(
		'nespresso_short_meta', // $id
		'Nespresso Shortcodes List', // $title
		'display_nespresso_short_codes', // $callback
		'page', // $page
		'normal', // $context
		'high' // $priority
	 );
}
function display_nespresso_short_codes() {

	global $nespresso_short_codes;

	echo '
		<p>List of Custom nespresso Shortcodes for custom content found in storefront-child/components folder.<br>
		<i>Note: copy and paste the code with the brakets e.g. "[nespresso_page_login]"  into the page textbox</i>
		<p>
	';

	foreach ( $nespresso_short_codes as $short_code => $description ) :
		echo "<p>[$short_code] = $description</p>";
	endforeach;
}

/**
 * Machine Range
 *
 * @param  array|object $attributes
 */
function nespresso_page_machine_range( $attributes ) {
	get_template_part('components/machine-range');
}
add_shortcode( 'nespresso_page_machine_range', 'nespresso_page_machine_range' );

/**
 * Coffee List Question
 *
 * @param  array|object $attributes
 */
function nespresso_page_coffee_list_question( $attributes ) {
	get_template_part('components/coffee-list-question');
}
add_shortcode( 'nespresso_page_coffee_list_question', 'nespresso_page_coffee_list_question' );

/**
 * Coffee List Question Thank You
 *
 * @param  array|object $attributes
 */
function nespresso_page_coffee_list_question_thank_you( $attributes ) {
	get_template_part('components/coffee-list-question-thank-you');
}
add_shortcode( 'nespresso_page_coffee_list_question_thank_you', 'nespresso_page_coffee_list_question_thank_you' );

/**
 * Coffee List Question Thank You
 *
 * @param  array|object $attributes
 */
function nespresso_page_coffee_list_question_discover( $attributes ) {
	get_template_part('components/coffee-list-question-discover');
}
add_shortcode( 'nespresso_page_coffee_list_question_discover', 'nespresso_page_coffee_list_question_discover' );

/**
 * Coffee list page question step 2
 *
 * @param  array|object $attributes
 */
function nespresso_page_coffee_list_question_step_2( $attributes ) {
	get_template_part('components/coffee-list-question-step-2');
}
add_shortcode( 'nespresso_page_coffee_list_question_step_2', 'nespresso_page_coffee_list_question_step_2' );

/**
 * Coffee list page question result
 *
 * @param  array|object $attributes
 */
function nespresso_page_coffee_list_question_result( $attributes ) {
	get_template_part('components/coffee-list-question-result');
}
add_shortcode( 'nespresso_page_coffee_list_question_result', 'nespresso_page_coffee_list_question_result' );

/**
 * service page
 *
 * @param  array|object $attributes
 */
function nespresso_page_service( $attributes ) {

	get_template_part('components/service');
		
}
add_shortcode( 'nespresso_page_service', 'nespresso_page_service' );

/**
 * service page
 *
 * @param  array|object $attributes
 */
function nespresso_coffee_range_2( $attributes ) {

	get_template_part('components/coffee-range-2');

}
add_shortcode( 'nespresso_coffee_range_2', 'nespresso_coffee_range_2' );

/**
 * service page
 *
 * @param  array|object $attributes
 */
function nespresso_langding_page_2( $attributes ) {
	
	get_template_part('components/langding-page-2');
	
}
add_shortcode( 'nespresso_langding_page_2', 'nespresso_langding_page_2' );


function nespresso_langding_page_survey_1( $attributes ) {
	
	get_template_part('components/langding-page-survey-1');
	
}
add_shortcode( 'nespresso_langding_page_survey_1', 'nespresso_langding_page_survey_1' );
	
function nespresso_langding_page_survey_2( $attributes ) {
	
	get_template_part('components/langding-page-survey-2');
	
}
add_shortcode( 'nespresso_langding_page_survey_2', 'nespresso_langding_page_survey_2' );

function nespresso_langding_page_survey_3( $attributes ) {
	
	get_template_part('components/langding-page-survey-3');
	
}
add_shortcode( 'nespresso_langding_page_survey_3', 'nespresso_langding_page_survey_3' );
	
function nespresso_langding_page_survey_4( $attributes ) {
	
	get_template_part('components/langding-page-survey-4');
	
}
add_shortcode( 'nespresso_langding_page_survey_4', 'nespresso_langding_page_survey_4' );
	
function nespresso_langding_page_survey_5( $attributes ) {
	
	get_template_part('components/langding-page-survey-5');
	
}
add_shortcode( 'nespresso_langding_page_survey_5', 'nespresso_langding_page_survey_5' );
	
	
function nespresso_langding_page_phone_login( $attributes ) {
	
	get_template_part('components/langding-page-phone-login');
	
}
add_shortcode( 'nespresso_langding_page_phone_login', 'nespresso_langding_page_phone_login' );

function nespresso_langding_page_thanks_you( $attributes ) {
	
	get_template_part('components/langding-page-thanks-you');
	
}
add_shortcode( 'nespresso_langding_page_thanks_you', 'nespresso_langding_page_thanks_you' );

function nespresso_store_locator( $attributes ) {
	
	get_template_part('components/store-locator-2');
	
}
add_shortcode( 'nespresso_store_locator', 'nespresso_store_locator' );