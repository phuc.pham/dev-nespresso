<?php

/**
 * start session in the earliest point
 */
function register_session() {
    if (!session_id()) {
        session_start();
    }
}
add_action('init', 'register_session', 1);
add_action('woocommerce_checkout_update_order_meta', 'redinvoice_field_update_order_meta');

function redinvoice_field_update_order_meta($order_id)
{
	//print_r($_POST);exit;
	if (isset($_POST['recycling_pick_up']) && !empty($_POST['recycling_pick_up'])) {
		update_post_meta($order_id, 'recycling_pick_up',sanitize_text_field($_POST['recycling_pick_up']));
	}
	if (isset($_POST['red_name']) && !empty($_POST['red_name'])) {
		update_post_meta($order_id, 'red_name',sanitize_text_field($_POST['red_name']));
	}
	if (isset($_POST['red_address']) && !empty($_POST['red_address'])) {
		update_post_meta($order_id, 'red_address',sanitize_text_field($_POST['red_address']));
	}
	if (isset($_POST['vat_number']) && !empty($_POST['vat_number'])) {
		update_post_meta($order_id, 'vat_number',sanitize_text_field($_POST['vat_number']));
	}
    if (isset($_POST['red_email_invoice']) && !empty($_POST['red_email_invoice'])) {
		update_post_meta($order_id, 'red_email_invoice',sanitize_text_field($_POST['red_email_invoice']));
	}
}
/**
 * disable woocommerce default stylesheet
 */
 // Register new status
function register_custom_order_statuses() {
    register_post_status('wc-exit-payment', array(
        'label' => 'Exit Payment',
        'public' => true,
        'exclude_from_search' => true,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop('Exit Payment <span class="count">(%s)</span>', 'Exit Payment <span class="count">(%s)</span>')
    ));
}
add_action('init', 'register_custom_order_statuses');
add_action( 'change_status_payment_cron', 'change_to_exit_payment' );
function change_to_exit_payment() {
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	
	//echo 123;
	$orders = wc_get_orders( array(
		'status' => 'pending',
		'limit' => 1,
		'orderby' => 'date',
		'order' => 'ASC'
	));
	$max = 20 *60;
	foreach($orders as $order)
	{
		$time = strtotime($order->get_date_created()->date('Y-m-d H:i:s'));
		//echo $time
		//echo '<br>';
		$time2 = time();
		//echo $time2;
		//echo '<br>';
		$less = $time2 - $time;
		//echo $order->get_id().'-'.$less;
		if($less>=$max)
		{
			$items = $order->get_items();			
			$order_note= [];
			foreach ( $items as $item ) {
                $product_id = $item->get_product_id();

                if ( $product_id > 0 ) {
                    $product = $item->get_product();

                    if ( $product && $product->exists() && $product->managing_stock() ) {

                        // Get the product initial stock quantity (before update)
                        $initial_stock = $product->get_stock_quantity();

                        $item_qty = apply_filters( 'woocommerce_order_item_quantity', $item->get_quantity(), $order, $item );

                        // Update the product stock quantity
                        // Replace DEPRECATED methods: increase_stock() & discrease_stock()
                        wc_update_product_stock( $product, $item_qty, 'increase' );
//echo $item->get_quantity().'--'.$initial_stock;exit ;
                        // Get the product updated stock quantity
                        $updated_stock = $initial_stock + $item_qty;

                        do_action( 'woocommerce_auto_stock_restored', $product, $item );

                        // A unique Order note: Store each order note in an array…
                        $order_note[] = sprintf( __( '#%s stock incremented from %s to %s.', 'woocommerce' ), $product->get_sku(), $initial_stock, $updated_stock);

                        // DEPRECATED & NO LONGER NEEDED - can be removed
                        //$order->send_stock_notifications( $product, $updated_stock, $item_qty );

                    }
                }
            }
            // Adding a unique composite order note (for multiple items)
            $order_notes = count($order_note) > 1 ? implode(' | ', $order_note) : $order_note[0];
            $order->add_order_note( $order_notes );			
			$order->update_status('exit-payment'); 
			//echo 'Cronjob change Payment Pending to Exit Payment';
		}
	}
	//echo date_default_timezone_get();
	//wp_mail( 'hieu.nvt@jacobins-digital.com', 'WP Crontrol','send');
	
	//exit; 
}

add_filter('woocommerce_enqueue_styles', '__return_empty_array');
add_filter('woocommerce_enqueue_styles', '__return_false');

function custom_registration_redirect() {
    die( home_url( '/coffee-list' ));
}

add_filter('registration_redirect', 'custom_registration_redirect', 2);

/*add notice field*/
add_filter( 'woocommerce_get_sections_products' , 'freeship_add_settings_tab' );
function freeship_add_settings_tab( $settings_tab ){
     $settings_tab['free_shipping_notices'] = __( 'Free Shipping Notices' );
     return $settings_tab;
}
add_filter( 'wc_order_statuses', 'rename_order_statuses', 20, 1 );
function rename_order_statuses( $order_statuses ) {
    $order_statuses['wc-pending']  			= _x( 'Payment Pending', 'Order status', 'woocommerce' );	
	$order_statuses['wc-failed']    		= _x( 'Failed Payment', 'Order status', 'woocommerce' );
    $order_statuses['wc-processing'] 		= _x( 'Pending', 'Order status', 'woocommerce' );
	$order_statuses['wc-exit-payment']    	= _x( 'Exit Payment', 'Order status', 'woocommerce' );
    $order_statuses['wc-on-hold']    		= _x( 'On Hold', 'Order status', 'woocommerce' );
	$order_statuses['wc-manual-processing'] = _x( 'Processing', 'Order status', 'woocommerce' );
	$order_statuses['wc-in-transit']    	= _x( 'In Transit', 'Order status', 'woocommerce' );
	$order_statuses['wc-cancelled']    		= _x( 'Cancelled', 'Order status', 'woocommerce' );
    $order_statuses['wc-completed']   		= _x( 'Completed', 'Order status', 'woocommerce' );
	$order_statuses['wc-undelivered']    	= _x( 'Undelivered', 'Order status', 'woocommerce' );
	$order_statuses['wc-ready-to-collect']  = _x( 'Ready to Collect', 'Order status', 'woocommerce' );
	$order_statuses['wc-refunded']    		= _x( 'Refunded', 'Order status', 'woocommerce' );
	
	$_SESSION['tip-pending']  			= ['vi'=>'Đơn hàng của bạn đã được tiếp nhận và đang chờ được thanh toán trực tuyến qua thẻ tín dụng.',
										   'en_US'=>'Your order has been received, waiting for online payment by credit card.'];
	$_SESSION['tip-failed']    			= ['vi'=>'Thanh toán trực tuyến qua thẻ tín dụng không thành công. Bạn vui lòng liên hệ qua hotline của Nespresso để được giải đáp.',
										   'en_US'=>'The Online payment by credit card didn\'t succeed. Please kindly contact Nespresso hotline for further information.'];
    $_SESSION['tip-processing'] 		= ['vi'=>'Đơn hàng của bạn đã được tạo thành công.',
										   'en_US'=>'Your order has been successfully created.'];
	$_SESSION['tip-exit-payment'] 		= ['vi'=>'Thanh toán trực tuyến qua thẻ tín dụng không thành công.',
										   'en_US'=>'The Online payment by credit card didn\'t succeed.'];
    $_SESSION['tip-on-hold']    		= ['vi'=>'Đơn hàng của bạn đang được tạm hoãn. Bạn vui lòng liên hệ qua hotline của Nespresso để được giải đáp.',
										   'en_US'=>'Your order is currently On Hold. Please kindly contact Nespresso hotline for further information.'];
	$_SESSION['tip-manual-processing'] 	= ['vi'=>'Đơn hàng của bạn đang được Nespresso team xử lý.',
										   'en_US'=>'Your order is being prepared by Nespresso team.'];
	$_SESSION['tip-in-transit']    		= ['vi'=>'Đơn hàng của bạn đang được giao đến địa chỉ đăng ký.',
										   'en_US'=>'Your order is being delivered to you.'];
	$_SESSION['tip-cancelled']    		= ['vi'=>'Đơn hàng của bạn bị hủy. Bạn vui lòng liên hệ qua hotline của Nespresso để được giải đáp.',
										   'en_US'=>'Your order has been cancelled. Please kindly contact Nespresso hotline for further information.'];
    $_SESSION['tip-completed']   		= ['vi'=>'Đơn hàng của bạn đã được giao thành công.',
										   'en_US'=>'Your order has been successfully delivered'];
	$_SESSION['tip-undelivered']    	= ['vi'=>'Đơn hàng của bạn không được giao. Bạn vui lòng liên hệ qua hotline của Nespresso để được giải đáp.',
										   'en_US'=>'Your order couldn\'t be delivered. Please kindly contact Nespresso hotline for further information.'];
	$_SESSION['tip-ready-to-collect']  	= ['vi'=>'Đơn hàng của bạn đã sẵn sàng. Bạn có thể đến nhận hàng tại cửa hàng đã đăng ký.',
										   'en_US'=>'Your order is now ready to be collected in the selected partner store.'];
	$_SESSION['tip-refunded']    		= ['vi'=>'Số tiền thanh toán của bạn được hoàn trả. Bạn vui lòng liên hệ qua hotline của Nespresso để được giải đáp.',
										   'en_US'=>'Your payment has been refunded. Please kindly contact Nespresso hotline for further information.'];


    return $order_statuses;
}
// Add to list of WC Order statuses
function add_exit_payment_to_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-pending' === $key ) {
            $new_order_statuses['wc-exit-payment'] = 'Exit Payment';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_exit_payment_to_order_statuses' );

add_filter( 'woocommerce_get_settings_products' , 'freeship_get_settings' , 10, 2 );
function freeship_get_settings( $settings, $current_section ) {
        $custom_settings = array();
        if( 'free_shipping_notices' == $current_section ) {
        	$custom_settings =  array(
			array(
				'name' => __( 'Free Shipping Notices' ),
				'type' => 'title',
				'desc' => __( 'Show the free shipping threshold to customers' ),
				'id'   => 'free_shipping' 
			),
			array(
				'name' => __( 'Enable Free shipping notices' ),
				'type' => 'checkbox',
				'desc' => __( 'Enable/Disabled'),
				'id'	=> 'free_enable'
			),
			array(
				'name' => __( 'Message' ),
				'type' => 'text',
				'desc' => __( 'Message to display on the notice'),
				'desc_tip' => true,
				'id'	=> 'free_msg'
			),
			array(
				'name' => __( 'Subtotal' ),
				'type' => 'text',
				'desc' => __( 'Subtotal on cart'),
				'desc_tip' => true,
				'id'	=> 'free_subtotal'
			),
			array(
				'name' => __( 'Background Color' ),
				'type' => 'color',
				'desc' => __( 'Background Color of the notice on the product page'),
				'desc_tip' => true,
				'id'	=> 'free_bgcolor'
			),
			
			array(
				'name' => __( 'Text Color' ),
				'type' => 'color',
				'desc' => __( 'Color of the text in the notice'),
				'desc_tip' => true,
				'id'	=> 'free_color',
			),array(
				'name' => __( 'Text button' ),
				'type' => 'text',
				'desc' => __( 'Text button'),
				'desc_tip' => true,
				'id'	=> 'free_btn',
			)
			,array(
				'name' => __( 'Link button' ),
				'type' => 'text',
				'desc' => __( 'Link button'),
				'desc_tip' => true,
				'id'	=> 'free_btnlink',
			),
			 array( 'type' => 'sectionend', 'id' => 'free_shipping' ),
	);
		return $custom_settings;
     } else {
        	return $settings;
    }
}
/**
 * change all shop url links to coffee list
 */
add_filter('woocommerce_return_to_shop_redirect', "custom_woocommerce_return_to_shop_redirect", 20);
function custom_woocommerce_return_to_shop_redirect() {
    return site_url() . '/coffee-list';
}

/**
 * change and remove the default login form of woocommerce through yith plugin into nespresso login form
 * can't completely remove login form due to plugin restrictions by both yith and woocommerce
 */
// remove_filter( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
add_filter('woocommerce_before_checkout_form', 'nespresso_login_form');
function nespresso_login_form() {

    if (is_user_logged_in()) {
        return;
    }

    echo '<div style="padding-top: 10px; padding-bottom: 10px;" id="checkout_login" class="woocommerce_checkout_login">
	';
    include get_stylesheet_directory() . "/components/login.php";
    echo '</div>';
} // nespresso_login_form()

/**
 * add the script in woocommerce checkout process
 */
add_filter('woocommerce_before_checkout_form', 'nespresso_before_checkout_form');
function nespresso_before_checkout_form() {

    global $scripts;
    $scripts[] = 'js/components/validation.js';
    $scripts[] = 'js/components/checkout.js';

    echo '<link rel="stylesheet" type="text/css" href="' . get_stylesheet_directory_uri() . '/css/components/checkout.css">';
}

/**
 * validation for checkout fields, and also for the custom ones
 */
function nespresso_checkout_field_process() {

    $validation = new Validation();

    if (!$validation->valid_title(@$_POST['title'])) {
        wc_add_notice(__('Title is required'), 'error');
    }

    if (!$validation->valid_email(@$_POST['billing_email'])) {
        wc_add_notice(__('Invalid Email'), 'error');
    }

    if (!$validation->valid_first_name(@$_POST['billing_first_name'])) {
        wc_add_notice(__('Billing first name is required'), 'error');
    }

    if (!$validation->valid_last_name(@$_POST['billing_last_name'])) {
        wc_add_notice(__('Billing last name is required'), 'error');
    }

    if (!$validation->valid_mobile(@$_POST['billing_mobile'])) {
        wc_add_notice(__('Billing mobile is required'), 'error');
    }

    if (!$validation->valid_billing_address_1()) {
        wc_add_notice(__('Billing address 1 is required'), 'error');
    }

    if (!$validation->valid_billing_city()) {
        wc_add_notice(__('City is required'), 'error');
    }

    if (!$validation->valid_billing_state()) {
        wc_add_notice(__('State is required'), 'error');
    }

    // if (!$validation->valid_billing_postcode()) {
    //     wc_add_notice(__('Postcode is required'), 'error');
    // }

} // nespresso_checkout_field_process()
// add_action('woocommerce_checkout_process', 'nespresso_checkout_field_process');

/**
 * save/update the custom fields during checkout
 *
 * @param  [type] $order_id
 * @param  [type] $posted - post data from forms
 */
function nespresso_checkout_update_order_meta($order_id, $posted) {
    if (isset($_POST['delivery_option'])) {
        update_post_meta( $order_id, 'delivery_option', sanitize_text_field( $_POST['delivery_option'] ) );
    }

    if (isset($_POST['billing_country_code'])) {
        update_post_meta( $order_id, '_billing_country_code', $_POST['billing_country_code'] );
    }
	
    if (isset($_POST['delivery_option']) && $_POST['delivery_option'] == 'pick_up') {

        $pick_up_location = explode('|', $_POST['pick_up_location']);

        $delivery_region = str_replace(' - ', ' ', $pick_up_location[2]);
        $delivery_region = str_replace(' ', '-', $delivery_region);

        update_post_meta( $order_id, '_shipping_address_1', $pick_up_location[0] );
        update_post_meta( $order_id, '_shipping_address_2', '' );
        update_post_meta( $order_id, '_shipping_city', $pick_up_location[1] );
        update_post_meta( $order_id, '_shipping_state', $delivery_region );
        update_post_meta( $order_id, '_shipping_mobile', $pick_up_location[3] );
        update_post_meta( $order_id, '_shipping_country_code', '+84' );
        update_post_meta( $order_id, '_shipping_company', '' );

    } else {
        if (isset($_POST['shipping_country_code'])) {
            update_post_meta( $order_id, '_shipping_country_code', $_POST['shipping_country_code'] );
        }
        if (isset($_POST['shipping_company'])) {
            update_post_meta( $order_id, '_shipping_company', $_POST['shipping_company'] );
        }
    }



}
add_action('woocommerce_checkout_update_order_meta', 'nespresso_checkout_update_order_meta', 10, 2);
function save_my_phone_number1($orderid)
{
	 if (isset($_POST['red_name'],$_POST['red_address'],$_POST['vat_number']) && $_POST['red_name'] && $_POST['red_address']&&$_POST['vat_number']&&$_POST['red_email_invoice']) 
     {
		
		  update_post_meta( $orderid, 'red_address', $_POST['red_address']);
		  update_post_meta( $orderid, 'vat_number', $_POST['vat_number'] );
		  update_post_meta( $orderid, 'red_name', $_POST['red_name'] );
          update_post_meta( $orderid, 'red_email_invoice', $_POST['red_email_invoice'] );
	 }
	if(isset($_POST['_mobile'], $_POST['_billing_mobile'], $_POST['_shipping_mobile'])){
		$order = wc_get_order( $orderid );
		//var_dump($order);exit;
		update_user_meta( $order->get_customer_id(), 'mobile', sanitize_text_field( $_POST['_mobile'] ) );
		update_user_meta( $order->get_customer_id(), 'billing_mobile', sanitize_text_field( $_POST['_billing_mobile'] ) );
		if ($order->get_meta('delivery_option') == 'pick_up'):		
			update_user_meta( $order->get_customer_id(), 'shipping_mobile', sanitize_text_field( $_POST['_shipping_mobile'] ) );
			update_post_meta( $orderid, '_shipping_mobile', sanitize_text_field( $_POST['_shipping_mobile'] ));
			//echo 1;exit;
		else:
			update_user_meta( $order->get_customer_id(), 'shipping_mobile', sanitize_text_field( $_POST['_shipping_mobile'] ) );
			update_post_meta( $orderid, '_shipping_mobile', sanitize_text_field( $_POST['_shipping_mobile'] ));
			//echo 2;exit;
		endif;
	}
}
add_action('save_post', 'save_my_phone_number1', 10, 2);
function reigel_woocommerce_checkout_update_user_meta($customer_id, $posted) {

    if (isset($_POST['title'])) {
        update_user_meta($customer_id, 'title', sanitize_text_field($_POST['title']));
    }

    if (isset($_POST['order_comments'])) {
        update_user_meta($customer_id, 'order_comments', sanitize_text_field($_POST['order_comments']));
    }

    // if (isset($_POST['billing_email'])) {
    //     update_user_meta($customer_id, 'email', sanitize_text_field($_POST['billing_email']));
    // }

    if (isset($_POST['first_name'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['first_name']));
    }

    // if (isset($_POST['billing_middle_name'])) {
    //     update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_middle_name']));
    // }

    if (isset($_POST['last_name'])) {
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['last_name']));
    }

    // if (isset($_POST['billing_mobile'])) {
    //     update_user_meta($customer_id, 'billing_mobile', sanitize_text_field($_POST['billing_mobile']));
    // }

    // if (isset($_POST['billing_phone'])) {
    //     update_user_meta($customer_id, 'phone', sanitize_text_field($_POST['billing_phone']));
    // }

    if (isset($_POST['billing_address_type'])) {
        update_user_meta($customer_id, 'billing_address_type', sanitize_text_field($_POST['billing_address_type']));
    }

    if (isset($_POST['billing_address_1'])) {
        update_user_meta($customer_id, 'billing_address_1', sanitize_text_field($_POST['billing_address_1']));
    }

    if (isset($_POST['billing_address_2'])) {
        update_user_meta($customer_id, 'billing_address_2', sanitize_text_field($_POST['billing_address_2']));
    }

    if (isset($_POST['billing_city'])) {
        update_user_meta($customer_id, 'billing_city', sanitize_text_field($_POST['billing_city']));
        update_user_meta($customer_id, 'city', sanitize_text_field($_POST['billing_city']));
    }

    if (isset($_POST['billing_state'])) {
        update_user_meta($customer_id, 'billing_state', sanitize_text_field($_POST['billing_state']));
        update_user_meta($customer_id, 'state', sanitize_text_field($_POST['billing_state']));
    }

    // if (isset($_POST['billing_postcode'])) {
    //     update_user_meta($customer_id, 'billing_postcode', sanitize_text_field($_POST['billing_postcode']));
    //     update_user_meta($customer_id, 'postal_code', sanitize_text_field($_POST['billing_postcode']));
    // }

    if (isset($_POST['pick_up_location'])) {
        update_user_meta($customer_id, 'pick_up_location', sanitize_text_field($_POST['pick_up_location']));
    }

    if (isset($_POST['billing_mobile'])) {
        update_user_meta($customer_id, 'billing_mobile', sanitize_text_field($_POST['billing_mobile']));
    }

    if (isset($_POST['shipping_mobile'])) {
        update_user_meta($customer_id, 'shipping_mobile', sanitize_text_field($_POST['shipping_mobile']));
    }

    if (isset($_POST['billing_country_code'])) {
        update_user_meta($customer_id, 'billing_country_code', sanitize_text_field($_POST['billing_country_code']));
    }

    if (isset($_POST['shipping_country_code'])) {
        update_user_meta($customer_id, 'shipping_country_code', sanitize_text_field($_POST['shipping_country_code']));
    }

    if (isset($_POST['billing_company'])) {
        update_user_meta($customer_id, 'billing_company', sanitize_text_field($_POST['billing_company']));
    }

    if (isset($_POST['shipping_company'])) {
        update_user_meta($customer_id, 'shipping_company', sanitize_text_field($_POST['shipping_company']));
    }


}
add_action('woocommerce_checkout_update_user_meta', 'reigel_woocommerce_checkout_update_user_meta', 10, 2);

/**
 * Display field value on the order edit page
 */
function my_custom_checkout_field_display_admin_order_meta($order) {

    if (!$order) {
        return;
    }

    $order_data = $order->get_data();

    if (!$order_data) {
        return;
    }

    $customer = nespresso_get_user_data($order_data['customer_id']);
	//var_dump($customer);
    ?>       
	<div class="edit_address">
		<p class="form-field form-field-wide">
				<label><?php esc_html_e( 'Billing phone number', 'woocommerce' ); ?></label>
				<input name="_billing_mobile" value="<?=$customer->billing_mobile?>"/>
			</p>
			<p class="form-field form-field-wide">
				<label><?php esc_html_e( 'Mobile', 'woocommerce' ); ?></label>
				<input name="_mobile" value="<?=$customer->mobile?>"/>
			</p>
	</div>
	 <p>
            <strong>Billing phone number:</strong>
            <?=$customer->billing_mobile//$customer->mobile?>
        </p>
		<p>
			<strong>Title:</strong>
			<?=$customer->title?>
		</p>
		<p>
			<strong>First Name:</strong>
			<?=$customer->first_name?>
		</p>
		<p>
			<strong>Middle Name:</strong>
			<?=$customer->middle_name?>
		</p>
		<p>
			<strong>Last Name:</strong>
			<?=$customer->last_name?>
		</p>
		<p>
			<strong>Date of Birth:</strong>
			<?=$customer->date_of_birth?>
		</p>
		<p>
			<strong>Mobile:</strong>
			<a href="tel:<?=$customer->mobile?>"><?=$customer->mobile?></a>
		</p>
		<p>
			<strong>Other authorized person to receive:</strong>
			<a href="tel:<?=$customer->order_comments?>"><?=$customer->order_comments?></a>
		</p>
	<?php
}
add_action('woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 1, 1);
function show_full_link_api($order) {    ?>       
	 <p style="clear:both;margin-top:20px;padding: 20px 0 !important;">       
			<label>
			<input type="checkbox" name="showfulllink" value="1"/>
			<strong>Get full link AX API and Json data:</strong>
			</label>
      </p>
	  <?php if(isset($_SESSION['fulllink'])){ ?>
		<style>
		.box-pupop {
			position: fixed;
			width: 100%;
			height: 100%;
			background: #211f1f8f;
			top: 0;
			left: 0;
			z-index: 9999;
		}
		.box-pupop-content {
			width: 80%;
			border: 1px solid #fff;
			margin: 5% auto 0;
			background: #fff;
			padding: 30px 0;
			position:relative;
		}
		.box-pupop-content span.box-close:hover {
				background: red;
				color: #fff;
			}
			.box-pupop-content span.box-close {
				    border: 1px solid red;
					display: inline-block;
					position: absolute;
					padding: 5px 10px;
					cursor: pointer;
					right: 0;
					top: 0;
			}
			.box-pupop-content div {
				display: flex;
				justify-content: center;
				margin-bottom: 10px;
			}
			.box-pupop-content div input, .box-pupop-content div textarea {
				width: 90%;
			}
		</style>
	  <div class="box-pupop">
			<div class="box-pupop-content">
				<span class="box-close" onclick="$('.box-pupop').remove()">X</span>
				<div>Link: <textarea rows="5"><?=$_SESSION['fulllink']?></textarea></div>
				<div>Json: <textarea rows="15"><?=$_SESSION['jsondata']?></textarea></div>
				<div>Port: <input value="<?=$_SESSION['port']?>"/></div>
			</div>
	  </div>
	<?php
	unset($_SESSION['fulllink']);unset($_SESSION['jsondata']);unset($_SESSION['port']);
	  }
}
add_action('woocommerce_admin_order_data_after_order_details', 'show_full_link_api', 1, 1);
/**
 * Display field value on the order edit page
 */
function my_custom_checkout_field_shipping_display_admin_order_meta($order) {

    if (!$order) {
        return;
    }

    $order_data = $order->get_data();

    if (!$order_data) {
        return;
    }

    $customer = nespresso_get_user_data($order_data['customer_id']);
    ?>       
	<div class="edit_address">
	<p class="form-field form-field-wide">
								<label><?php esc_html_e( 'Shipping phone number', 'woocommerce' ); ?></label>
								<input name="_shipping_mobile" value="<?php 
								if ($order->get_meta('delivery_option') == 'pick_up'):
            $pick_up_location = explode('|', $customer->pick_up_location);
			echo $order->get_meta('_shipping_mobile');//isset($pick_up_location[3]) ? $pick_up_location[3] : $customer->shipping_mobile;
			else:
			echo $order->get_meta('_shipping_mobile');
			endif;
			
        ?>"/>
							</p>
							<?php if($order->get_meta('vat_number')){  ?>
							<h3>Red invoice</h3>
							<p class="form-field form-field-wide">
								<label><?php esc_html_e( 'Billing name', 'woocommerce' ); ?></label>
								<input name="red_name" value="<?=$order->get_meta('red_name')?>"/>
							</p>
							<p class="form-field form-field-wide">
								<label><?php esc_html_e( 'Billing address', 'woocommerce' ); ?></label>
								<input name="red_address" value="<?=$order->get_meta('red_address')?>"/>
							</p>
							<p class="form-field form-field-wide">
								<label><?php esc_html_e( 'VAT number', 'woocommerce' ); ?></label>
								<textarea name="vat_number" ><?=$order->get_meta('vat_number')?></textarea>
							</p>
							<?php } ?>
							</div>
								 <?php if ($order->get_meta('delivery_option') == 'express'):
        ?>
        <p>
            <strong>Delivery: </strong>
			<?='Express Delivery'?>
        </p>
		  <?php endif; if ($order->get_meta('recycling_pick_up') == 1):
        ?>
        <p>
            <strong>Recycling Pick-up: </strong>
			Yes
        </p>
		 <?php endif; if ($order->get_meta('delivery_option') == 'pick_up'):
            $pick_up_location = explode('|', $customer->pick_up_location);
        ?>
        <p>
            <strong>Pickup Location:</strong>
            <?= $pick_up_location[0] . ' ' . $pick_up_location[1] . ' ' . $pick_up_location[2]?>
        </p>
        <p>
            <strong>Shipping phone number:</strong>
            <?=$customer->shipping_mobile//isset($pick_up_location[3]) ? $pick_up_location[3] : $customer->shipping_mobile ?>
        </p>
        <?php else: ?>
        <p>
            <strong>Shipping phone number:</strong>
            <?=$customer->shipping_mobile//$order->get_meta('_shipping_mobile')//$customer->mobile?>
        </p>
        <?php endif;
		
		if ($order->get_meta('vat_number'))
		{
		?>
		<h3>Red invoice</h3>
		 <p>
            <strong>Billing name:</strong>
            <?=$order->get_meta('red_name')?>
        </p>
		 <p>
            <strong>Billing address:</strong>
            <?=$order->get_meta('red_address')?>
        </p>
		 <p>
          <strong>VAT number:</strong>
            <?=$order->get_meta('vat_number')?>
        </p>
        <p>
          <strong>Email:</strong>
            <?=$order->get_meta('red_email_invoice')?>
        </p>
	<?php
		}
}
add_action('woocommerce_admin_order_data_after_shipping_address', 'my_custom_checkout_field_shipping_display_admin_order_meta', 1, 1);

/**
 * override default address fields, just like nespresso_override_checkout_fields()
 */
add_filter('woocommerce_default_address_fields', 'nespresso_override_default_address_fields');
function nespresso_override_default_address_fields($address_fields) {

    $address_fields['billing_phone']['required'] = false;
    $address_fields['phone']['required'] = false;
    $address_fields['country']['required'] = false;
    $address_fields['company']['required'] = false;

    return $address_fields;
}

add_action('wp_enqueue_scripts', 'my_register_javascript', 100);

function my_register_javascript() {
  wp_register_script('mediaelement', plugins_url('wp-mediaelement.min.js', __FILE__), array('jquery'), '4.8.2', true);
  wp_enqueue_script('mediaelement');
}

function prefix_wc_rest_prepare_order_object( $response, $object, $request ) {
    // Get the value
    $delivery_region = ( $value = get_post_meta($object->get_id(), 'delivery_region', true) ) ? $value : $object->get_shipping_state();
    $delivery_city = ( $value = get_post_meta($object->get_id(), 'delivery_city', true) ) ? $value : $object->get_shipping_city();
    $delivery_address = ( $value = get_post_meta($object->get_id(), 'delivery_address', true) ) ? $value : $object->get_shipping_address_1() . ' ' .$object->get_billing_address_2();
    $delivery_option = ( $value = get_post_meta($object->get_id(), 'delivery_option', true) ) ? $value : '';
    $delivery_contact = ( $value = get_post_meta($object->get_id(), 'delivery_contact', true) ) ? $value : get_user_meta($object->get_customer_id(),'shipping_country_code', true) . ' ' .get_user_meta($object->get_customer_id(),'shipping_mobile', true);

    $response->data['delivery_address'] = $delivery_address;
    $response->data['delivery_city'] = $delivery_city;
    $response->data['delivery_region'] = $delivery_region;
    $response->data['delivery_option'] = $delivery_option;
    $response->data['delivery_contact'] = $delivery_contact;

    //billing

    $response->data['billing']['billing_country_code'] = $object->get_meta('_billing_country_code');
    $response->data['billing']['billing_mobile'] = get_user_meta($object->get_customer_id(),'billing_mobile', true);

    //Shipping
    $response->data['shipping']['shipping_country_code'] = $object->get_meta('_shipping_country_code');
    $response->data['shipping']['shipping_mobile'] = $object->get_meta('_shipping_mobile');

    return $response;
}
add_filter( 'woocommerce_rest_prepare_shop_order_object', 'prefix_wc_rest_prepare_order_object', 10, 3 );

function prefix_wc_rest_prepare_customer_object( $response, $object, $request ) {
    //change timezone
    $tz_to = 'Asia/Ho_Chi_Minh';
    $tz_from = 'UTC';
    $format = 'Y-m-d\TH:i:s';
    $created_dt = new DateTime($response->data['date_created'], new DateTimeZone($tz_from));
    $created_dt->setTimeZone(new DateTimeZone($tz_to));
    $converted_created_date = $created_dt->format($format);

    $response->data['date_created_gmt'] = $response->data['date_created'];
    $response->data['date_created'] = $converted_created_date;

    $shipping_country_code = get_user_meta($object->ID,'shipping_country_code', true);
    $shipping_company = get_user_meta($object->ID,'shipping_company', true);

    $billing_country_code = get_user_meta($object->ID,'billing_country_code', true);
    $billing_company = get_user_meta($object->ID,'billing_company', true);

    $response->data['shipping']['shipping_country_code'] = $shipping_country_code;
    $response->data['shipping']['shipping_company'] = $shipping_company;

    $response->data['billing']['billing_country_code'] = $billing_country_code;
    $response->data['billing']['billing_company'] = $billing_company;
    return $response;
}
add_filter( 'woocommerce_rest_prepare_customer', 'prefix_wc_rest_prepare_customer_object', 10, 3 );

/**
 * override address checkout fileds
 */
add_filter('woocommerce_checkout_fields', 'nespresso_override_checkout_fields');
function nespresso_override_checkout_fields($fields) {

    unset($fields['billing']['billing_phone']);
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_billing_phone']);
    unset($fields['shipping']['shipping_phone']);
    unset($fields['shipping']['shipping_postcode']);
    unset($fields['billing']['billing_postcode']);
    // $fields['shipping']['shipping_postcode']['class'] = ['hide'];
    $fields['shipping']['shipping_country']['class'] = ['hide'];

    $fields['shipping']['shipping_address_1']['required'] = true;
    $fields['shipping']['shipping_address_1']['placeholder'] = 'Address line 1';
    $fields['shipping']['shipping_address_1']['label'] = 'Address line 1';

    $fields['shipping']['shipping_address_2']['required'] = false;
    $fields['shipping']['shipping_address_2']['placeholder'] = 'Address line 2';
    $fields['shipping']['shipping_address_2']['label'] = 'Address line 2';

    $fields['shipping']['shipping_city']['placeholder'] = 'District';
    $fields['shipping']['shipping_city']['label'] = 'District';
    $fields['shipping']['shipping_city']['priority'] = 90;

    $fields['shipping']['shipping_state']['placeholder'] = 'Province';
    $fields['shipping']['shipping_state']['label'] = 'Province';
    $fields['shipping']['shipping_state']['label'] = 'Province';

    $fields['shipping']['shipping_mobile']['label'] = 'Mobile';
    $fields['shipping']['shipping_mobile']['required'] = true;
    $fields['shipping']['shipping_mobile']['priority'] = 30;

    // $fields['shipping']['shipping_pickup']['label'] = 'Pickup Location';

    $pick_up_locations = get_nespresso_pickup_location();

    if ($pick_up_locations) {
        $pick_up_locations['address'] = json_decode($pick_up_locations['address']);
        $pick_up_locations['province'] = json_decode($pick_up_locations['province']);
        $pick_up_locations['city'] = json_decode($pick_up_locations['city']);
        $pick_up_locations['contact'] = json_decode($pick_up_locations['contact']);
		$pick_up_locations['img'] = json_decode($pick_up_locations['img']);
        foreach ($pick_up_locations['address'] as $key => $address) {
            $location = $address . ' ' . $pick_up_locations['city'][$key] . ' ' . $pick_up_locations['province'][$key] .' (' . $pick_up_locations['contact'][$key] . ')|*|' . $pick_up_locations['img'][$key];
            $location_value = $address . '|' . $pick_up_locations['city'][$key] . '|' . $pick_up_locations['province'][$key] . '|' . $pick_up_locations['contact'][$key];
            $options[$location_value] = $location;
        }
        // dd($options);
        $fields['shipping']['pick_up_location'] = array(
        'priority' => 100,
        'required' => true,
        'type'     => 'select',
        'options'  => $options,
        'label'     => __('Pick Up Location ', 'woocommerce'),
        'class'     => array('form-row-wide','hide'),
            'clear'     => true
         );

    }



     return $fields;
}

add_action('woocommerce_checkout_create_order', 'nespresso_additional_order_data', 20, 2);
function nespresso_additional_order_data( $order, $data ) {
	
    if (isset($data['pick_up_location'])) {
        $pick_up_location = explode('|', $data['pick_up_location']);
        $order->update_meta_data( 'delivery_address', $pick_up_location[0] );
        $order->update_meta_data( 'delivery_city', $pick_up_location[1] );
        $order->update_meta_data( 'delivery_region', $pick_up_location[2] );
        $order->update_meta_data( 'delivery_contact', $pick_up_location[3] );
    } else {
        $order->update_meta_data( 'delivery_address', $data['shipping_address_1'] . ' ' . $data['shipping_address_2'] );
        $order->update_meta_data( 'delivery_city', $data['shipping_city']);
        $order->update_meta_data( 'delivery_region', $data['shipping_state']);
        $order->update_meta_data( 'delivery_contact', $data['shipping_country_code'] . $data['shipping_mobile']);
    }
}

function wc_empty_cart_redirect_url() {
    return site_url() . '/coffee-list';
}

add_filter('woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url');

function redirection_function() {
    global $woocommerce;
    if (is_cart()) {
        wp_safe_redirect(site_url() . '/coffee-list');
    }
}
add_action("template_redirect", 'redirection_function');


// define the woocommerce_available_payment_gateways callback
function filter_woocommerce_available_payment_gateways( $available_gateways ) {
    if( WC()->cart->subtotal > 15000000 ){

        // then unset the 'cod' key (cod is the unique id of COD Gateway)
        unset( $available_gateways['cod'] );
        ?>
            <script type="text/javascript">
                jQuery('#disable_cod').removeClass('hide');
            </script>
        <?php
    }
    return $available_gateways;
};

// add the filter
add_filter( 'woocommerce_available_payment_gateways', 'filter_woocommerce_available_payment_gateways', 10, 1 );
//custom create user for woocommerce
function custom_wp_insert_user( $userdata ) {
    global $wpdb;

    if ( $userdata instanceof stdClass ) {
        $userdata = get_object_vars( $userdata );
    } elseif ( $userdata instanceof WP_User ) {
        $userdata = $userdata->to_array();
    }

    // Are we updating or creating?
    if ( ! empty( $userdata['ID'] ) ) {
        $ID = (int) $userdata['ID'];
        $update = true;
        $old_user_data = get_userdata( $ID );

        if ( ! $old_user_data ) {
            return new WP_Error( 'invalid_user_id', __( 'Invalid user ID.' ) );
        }

        // hashed in wp_update_user(), plaintext if called directly
        $user_pass = ! empty( $userdata['user_pass'] ) ? $userdata['user_pass'] : $old_user_data->user_pass;
    } else {
        $update = false;
        // Hash the password
        $user_pass = wp_hash_password( $userdata['user_pass'] );
    }

    $sanitized_user_login = sanitize_user( $userdata['user_login'], true );

    /**
     * Filters a username after it has been sanitized.
     *
     * This filter is called before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $sanitized_user_login Username after it has been sanitized.
     */
    $pre_user_login = apply_filters( 'pre_user_login', $sanitized_user_login );

    //Remove any non-printable chars from the login string to see if we have ended up with an empty username
    $user_login = trim( $pre_user_login );

    // user_login must be between 0 and 60 characters.
    if ( empty( $user_login ) ) {
        return new WP_Error('empty_user_login', __('Cannot create a user with an empty login name.') );
    } elseif ( mb_strlen( $user_login ) > 60 ) {
        return new WP_Error( 'user_login_too_long', __( 'Username may not be longer than 60 characters.' ) );
    }

    if ( ! $update && username_exists( $user_login ) ) {
        return new WP_Error( 'existing_user_login', __( 'Sorry, that username already exists!' ) );
    }

    /**
     * Filters the list of blacklisted usernames.
     *
     * @since 4.4.0
     *
     * @param array $usernames Array of blacklisted usernames.
     */
    $illegal_logins = (array) apply_filters( 'illegal_user_logins', array() );

    if ( in_array( strtolower( $user_login ), array_map( 'strtolower', $illegal_logins ) ) ) {
        return new WP_Error( 'invalid_username', __( 'Sorry, that username is not allowed.' ) );
    }

    /*
     * If a nicename is provided, remove unsafe user characters before using it.
     * Otherwise build a nicename from the user_login.
     */
    if ( ! empty( $userdata['user_nicename'] ) ) {
        $user_nicename = sanitize_user( $userdata['user_nicename'], true );
        if ( mb_strlen( $user_nicename ) > 50 ) {
            return new WP_Error( 'user_nicename_too_long', __( 'Nicename may not be longer than 50 characters.' ) );
        }
    } else {
        $user_nicename = mb_substr( $user_login, 0, 50 );
    }

    $user_nicename = sanitize_title( $user_nicename );

    // Store values to save in user meta.
    $meta = array();

    /**
     * Filters a user's nicename before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $user_nicename The user's nicename.
     */
    $user_nicename = apply_filters( 'pre_user_nicename', $user_nicename );

    $raw_user_url = empty( $userdata['user_url'] ) ? '' : $userdata['user_url'];

    /**
     * Filters a user's URL before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $raw_user_url The user's URL.
     */
    $user_url = apply_filters( 'pre_user_url', $raw_user_url );

    $raw_user_email = empty( $userdata['user_email'] ) ? '' : $userdata['user_email'];

    /**
     * Filters a user's email before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $raw_user_email The user's email.
     */
    $user_email = apply_filters( 'pre_user_email', $raw_user_email );

    /*
     * If there is no update, just check for `email_exists`. If there is an update,
     * check if current email and new email are the same, or not, and check `email_exists`
     * accordingly.
     */
    if ( ( ! $update || ( ! empty( $old_user_data ) && 0 !== strcasecmp( $user_email, $old_user_data->user_email ) ) )
        && ! defined( 'WP_IMPORTING' )
        && email_exists( $user_email )
    ) {
        return new WP_Error( 'existing_user_email', __( 'Sorry, that email address is already used!' ) );
    }
    $nickname = empty( $userdata['nickname'] ) ? $user_login : $userdata['nickname'];

    /**
     * Filters a user's nickname before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $nickname The user's nickname.
     */
    $meta['nickname'] = apply_filters( 'pre_user_nickname', $nickname );

    $first_name = empty( $userdata['first_name'] ) ? '' : $userdata['first_name'];

    /**
     * Filters a user's first name before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $first_name The user's first name.
     */
    $meta['first_name'] = apply_filters( 'pre_user_first_name', $first_name );

    $last_name = empty( $userdata['last_name'] ) ? '' : $userdata['last_name'];

    /**
     * Filters a user's last name before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $last_name The user's last name.
     */
    $meta['last_name'] = apply_filters( 'pre_user_last_name', $last_name );

    if ( empty( $userdata['display_name'] ) ) {
        if ( $update ) {
            $display_name = $user_login;
        } elseif ( $meta['first_name'] && $meta['last_name'] ) {
            /* translators: 1: first name, 2: last name */
            $display_name = sprintf( _x( '%1$s %2$s', 'Display name based on first name and last name' ), $meta['first_name'], $meta['last_name'] );
        } elseif ( $meta['first_name'] ) {
            $display_name = $meta['first_name'];
        } elseif ( $meta['last_name'] ) {
            $display_name = $meta['last_name'];
        } else {
            $display_name = $user_login;
        }
    } else {
        $display_name = $userdata['display_name'];
    }

    /**
     * Filters a user's display name before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $display_name The user's display name.
     */
    $display_name = apply_filters( 'pre_user_display_name', $display_name );

    $description = empty( $userdata['description'] ) ? '' : $userdata['description'];

    /**
     * Filters a user's description before the user is created or updated.
     *
     * @since 2.0.3
     *
     * @param string $description The user's description.
     */
    $meta['description'] = apply_filters( 'pre_user_description', $description );

    $meta['rich_editing'] = empty( $userdata['rich_editing'] ) ? 'true' : $userdata['rich_editing'];

    $meta['comment_shortcuts'] = empty( $userdata['comment_shortcuts'] ) || 'false' === $userdata['comment_shortcuts'] ? 'false' : 'true';

    $admin_color = empty( $userdata['admin_color'] ) ? 'fresh' : $userdata['admin_color'];
    $meta['admin_color'] = preg_replace( '|[^a-z0-9 _.\-@]|i', '', $admin_color );

    $meta['use_ssl'] = empty( $userdata['use_ssl'] ) ? 0 : $userdata['use_ssl'];

    $user_registered = empty( $userdata['user_registered'] ) ? gmdate( 'Y-m-d H:i:s' ) : $userdata['user_registered'];

    $meta['show_admin_bar_front'] = empty( $userdata['show_admin_bar_front'] ) ? 'true' : $userdata['show_admin_bar_front'];

    $meta['locale'] = isset( $userdata['locale'] ) ? $userdata['locale'] : '';

    $user_nicename_check = $wpdb->get_var( $wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_nicename = %s AND user_login != %s LIMIT 1" , $user_nicename, $user_login));

    if ( $user_nicename_check ) {
        $suffix = 2;
        while ($user_nicename_check) {
            // user_nicename allows 50 chars. Subtract one for a hyphen, plus the length of the suffix.
            $base_length = 49 - mb_strlen( $suffix );
            $alt_user_nicename = mb_substr( $user_nicename, 0, $base_length ) . "-$suffix";
            $user_nicename_check = $wpdb->get_var( $wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_nicename = %s AND user_login != %s LIMIT 1" , $alt_user_nicename, $user_login));
            $suffix++;
        }
        $user_nicename = $alt_user_nicename;
    }

    $compacted = compact( 'user_pass', 'user_email', 'user_url', 'user_nicename', 'display_name', 'user_registered' );
    $data = wp_unslash( $compacted );

    if ( $update ) {
        if ( $user_email !== $old_user_data->user_email ) {
            $data['user_activation_key'] = '';
        }
        $wpdb->update( $wpdb->users, $data, compact( 'ID' ) );
        $user_id = (int) $ID;
    } else {
        $wpdb->insert( $wpdb->users, $data + compact( 'user_login' ) );
        $user_id = (int) $wpdb->insert_id;
    }

    $user = new WP_User( $user_id );

    /**
     * Filters a user's meta values and keys before the user is created or updated.
     *
     * Does not include contact methods. These are added using `wp_get_user_contact_methods( $user )`.
     *
     * @since 4.4.0
     *
     * @param array $meta {
     *     Default meta values and keys for the user.
     *
     *     @type string   $nickname             The user's nickname. Default is the user's username.
     *     @type string   $first_name           The user's first name.
     *     @type string   $last_name            The user's last name.
     *     @type string   $description          The user's description.
     *     @type bool     $rich_editing         Whether to enable the rich-editor for the user. False if not empty.
     *     @type bool     $comment_shortcuts    Whether to enable keyboard shortcuts for the user. Default false.
     *     @type string   $admin_color          The color scheme for a user's admin screen. Default 'fresh'.
     *     @type int|bool $use_ssl              Whether to force SSL on the user's admin area. 0|false if SSL is
     *                                          not forced.
     *     @type bool     $show_admin_bar_front Whether to show the admin bar on the front end for the user.
     *                                          Default true.
     * }
     * @param WP_User $user   User object.
     * @param bool    $update Whether the user is being updated rather than created.
     */
    $meta = apply_filters( 'insert_user_meta', $meta, $user, $update );

    // Update user meta.
    foreach ( $meta as $key => $value ) {
        update_user_meta( $user_id, $key, $value );
    }

    foreach ( wp_get_user_contact_methods( $user ) as $key => $value ) {
        if ( isset( $userdata[ $key ] ) ) {
            update_user_meta( $user_id, $key, $userdata[ $key ] );
        }
    }

    if ( isset( $userdata['role'] ) ) {
        $user->set_role( $userdata['role'] );
    } elseif ( ! $update ) {
        $user->set_role(get_option('default_role'));
    }
    wp_cache_delete( $user_id, 'users' );
    wp_cache_delete( $user_login, 'userlogins' );

    return $user_id;
}

function order_pending_payment($order_id) {
    $order = wc_get_order( $order_id );

    if ( ! $order ) {
        return;
    }

    $stock_reduced  = $order->get_data_store()->get_stock_reduced( $order_id );
    $trigger_reduce = apply_filters( 'woocommerce_payment_complete_reduce_order_stock', ! $stock_reduced, $order_id );

    // Only continue if we're reducing stock.
    if ( ! $trigger_reduce ) {
        return;
    }

    wc_reduce_stock_levels( $order );

    // Ensure stock is marked as "reduced" in case payment complete or other stock actions are called.
    $order->get_data_store()->set_stock_reduced( $order_id, true );;
}
add_action( 'woocommerce_checkout_order_processed', 'order_pending_payment',  1, 1  );

function order_status_failed( $order_id ) {
    $order = wc_get_order( $order_id );

    if ( ! $order ) {
        return;
    }

    $stock_reduced  = $order->get_data_store()->get_stock_reduced( $order_id );
    $trigger_reduce = (bool) $stock_reduced;

    // Only continue if we're reducing stock.
    if ( ! $trigger_reduce ) {
        return;
    }

    wc_increase_stock_levels( $order );

    // Ensure stock is marked as "reduced" in case payment complete or other stock actions are called.
    $order->get_data_store()->set_stock_reduced( $order_id, false );
}
add_action( 'woocommerce_order_status_failed', 'order_status_failed' );

//function rename_order_statuses( $order_statuses ) {
    //$order_statuses['wc-processing'] = _x( 'Pending', 'Order status', 'woocommerce' );

   // return $order_statuses;
//}
//add_filter( 'wc_order_statuses', 'rename_order_statuses', 20, 1 );


function register_my_new_order_statuses() {
    register_post_status( 'wc-manual-processing', array(
        'label'                     => _x( 'Processing', 'Order status', 'woocommerce' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Processing <span class="count">(%s)</span>', 'Processing<span class="count">(%s)</span>', 'woocommerce' )
    ) );

    register_post_status( 'wc-in-transit', array(
        'label'                     => _x( 'In Transit', 'Order status', 'woocommerce' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'In Transit <span class="count">(%s)</span>', 'In Transit<span class="count">(%s)</span>', 'woocommerce' )
    ) );

    register_post_status( 'wc-ready-to-collect', array(
        'label'                     => _x( 'Ready to Collect', 'Order status', 'woocommerce' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Ready to Collect <span class="count">(%s)</span>', 'Ready to Collect<span class="count">(%s)</span>', 'woocommerce' )
    ) );

    register_post_status( 'wc-undelivered', array(
        'label'                     => _x( 'Undelivered', 'Order status', 'woocommerce' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Undelivered <span class="count">(%s)</span>', 'Undelivered<span class="count">(%s)</span>', 'woocommerce' )
    ) );
}
add_action( 'init', 'register_my_new_order_statuses' );

function my_new_wc_order_statuses( $order_statuses ) {
    $order_statuses['wc-manual-processing'] = _x( 'Processing', 'Order status', 'woocommerce' );
    $order_statuses['wc-in-transit'] = _x( 'In Transit', 'Order status', 'woocommerce' );
    $order_statuses['wc-ready-to-collect'] = _x( 'Ready to Collect', 'Order status', 'woocommerce' );
    $order_statuses['wc-undelivered'] = _x( 'Undelivered', 'Order status', 'woocommerce' );

    return $order_statuses;
}
add_filter( 'wc_order_statuses', 'my_new_wc_order_statuses' );

function custom_dropdown_bulk_actions_shop_order( $actions ) {
    $actions['mark_processing'] = __( 'Change status to pending', 'woocommerce' );

    return $actions;
}
add_filter( 'bulk_actions-edit-shop_order', 'custom_dropdown_bulk_actions_shop_order', 20, 1 );

foreach( array( 'post', 'shop_order' ) as $hook )
    add_filter( "views_edit-$hook", 'shop_order_modified_views' );

function shop_order_modified_views( $views ){
    if( isset( $views['wc-processing'] ) )
        $views['wc-processing'] = str_replace( 'Processing', __( 'Pending', 'woocommerce'), $views['wc-processing'] );

    return $views;
}


function email_on_hold_status($order_id, $from_status, $to_status, $order)
{

    if ($to_status != "on-hold")
        return;

    WC()->mailer()->get_emails()['WC_Email_Customer_On_Hold_Order']->trigger( $order_id );
}
add_action( 'woocommerce_order_status_changed', 'email_on_hold_status', 10, 5 );
// //email red vat.
// function woocommerce_order_status_completed_send_mail_invoice($order_id)
// {
// 	$email = get_post_meta($order_id,'red_email_invoice',true);
// 	$name = get_post_meta($order_id,'red_name',true);
// 	$address = get_post_meta($order_id,'red_address',true);
// 	$vat = get_post_meta($order_id,'vat_number',true);

//     if(
//         $email && $name && $address && $vat
//     )
//     {        
//         $subject = "HĐ #{$order_id} - $name";
//         $headers = array('Content-Type: text/html; charset=UTF-8');
//         $headers[] = 'Cc: ty.dv <ty.dv@annam-professional.com>';
//         $headers[] = 'Cc: hieu.nv <hieu.nvt@jacobins-digital.com>';
// 		$body = get_option('red_email_invoice');

//         if(empty($body))
//         {
//             $body = "<p>Dear team kế toán,</p>
//             <p> Nespresso có đơn hàng yêu cầu xuất hóa đơn. Phiền team check và hỗ trợ với thông tin sau đây:</p>
//             <p><strong>Red Invoice:</strong></p>
//             <p><strong>Billing name:</strong> {$name}</p>
//             <p><strong>Billing address:</strong>  {$address} </p>
//             <p><strong>VAT number:</strong>  {$vat} </p>​
//             <p><strong>Recipient Email:</strong> {$email}  </p>
//             <p> Best regards,  </p>";
//         }else
//         {
//             $body = str_replace("{red_name}", $name , $body);
//             $body = str_replace("{red_address}", $address , $body);
//             $body = str_replace("{vat_number}", $vat, $body);
//             $body = str_replace("{red_email_invoice}", $email, $body);
//         }

//         $content_type = apply_filters( 'wp_mail_content_type', $content_type );

//         add_filter('wp_mail_content_type', function( $content_type ) {
//             return 'text/html';
//         });

// 		wp_mail($email,$subject,$body,$headers);
//     }  
// }


// add_action( 'woocommerce_order_status_completed', 'woocommerce_order_status_completed_send_mail_invoice');
// //add_action( 'woocommerce_order_status_completed', 'woocommerce_order_status_completed_send_mail_invoice_plugin');