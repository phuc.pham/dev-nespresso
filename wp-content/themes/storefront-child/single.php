<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 * page for single post
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

?>

<?php get_header(); ?>

	<!-- <div id="primary" class="content-area"> -->
		<!-- <main id="main" class="site-main" role="main"> -->

		<?php while ( have_posts() ) : the_post();

			do_action( 'storefront_single_post_before' );

			get_template_part( 'content', 'single' );

			do_action( 'storefront_single_post_after' );

		endwhile; // End of the loop. ?>

		<!--</main>--><!-- #main -->
	<!--</div>--><!-- #primary -->

<!-- get_footer -->
<?php get_footer(); ?>
