<?php
/**
 * Nespresso Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$category = get_the_category();
$categorySlug = sizeof($category) > 0 ? $category[0]->slug : '';

$_SESSION['nespresso_token'] = session_id();

global $wp;
$current_url = home_url(add_query_arg(array(), $wp->request));
$category = get_the_category();
$category_name = sizeof($category) > 0 ? $category[0]->name : '';

if (get_the_title() == 'Login') {
    $categorySlug = 'login';
    $category_name = 'User Account';
} else if (get_the_title() == 'Register') {
    $categorySlug = 'register';
    $category_name = 'User Account';
}

$user_id = get_current_user_id();

$clubMemberID = '';
$clubMemberStatus = 'false';
$clubMemberTitle = '';
$clubMemberLoginStatus = 'false';
$clubMemberLevel = '';
$clubMemberLevelID = '';
$persistentBasketLoaded = 'false';
$language = 'EN';
$parsed = parse_url($current_url);
if (isset($parsed['path'])) {
    $path = $parsed['path'];
    $path_parts = explode('/', $path);
    $segment = sizeof($path_parts) > 1 ? $path_parts[1] : '';

    if ($segment == 'vi' || $segment == 'vn') {
        $language = 'VN';
    }
}
function get_val($array) {
    if ($array) {
        if (sizeof($array) > 0) {
            return $array[0];
        }
    }
    return;
}

if ($user_id > 0) {
    $clubMemberID = $user_id; //get_user_meta($user_id, 'club_membership_no', true);
    $club_status = 'True';//get_user_meta($user_id, 'has_club_membership_no', true);
    $clubMemberStatus = 'True'; //$club_status > 0 ? 'True' : 'False';
    $clubMemberTitle = get_user_meta($user_id, 'title', true);
    $clubMemberLoginStatus = 'True';
    $machine_owner = get_user_meta($user_id, 'has_machine_registered', true);
    // $machineOwner = $machine_owner > 0 ? 'True' : 'False';
    $my_machines = get_user_meta($user_id, 'my_machines', false);
    $user_total_spent = wc_get_customer_total_spent($user_id);
    if ($user_total_spent == 0) {
        $clubMemberLevel = 'Bronze';
        $clubMemberLevelID = '3';
    } else if ($user_total_spent >= 10000000) {
        $clubMemberLevel = 'Gold';
        $clubMemberLevelID = '1';
    } else if ($user_total_spent <= 9999999) {
        $clubMemberLevel = 'Silver';
        $clubMemberLevelID = '2';
    }

    $machineOwned = '';
    $machine_counter = 0;

    foreach ($my_machines as $k => $v) {
        foreach ($v as $m) {
            if (isset($m['product_sku']) && $m['product_sku'] != '0') {
                $machine_id = wc_get_product_id_by_sku($m['product_sku']);
                $machine_data = wc_get_product( $machine_id );
                $machineOwned .= $machine_data->get_name() . '|';
                $machine_counter++;
            } else if( $m['serial_no'] != '' ) {
                if( $m['product_sku'] == '0' || $m['product_sku'] == '') {
                    $machineOwned .=  'Others|';
                    $machine_counter++;
                }
            }

        }
    }

    $machineOwned = substr($machineOwned, 0, strlen($machineOwned) - 1) != false ? substr($machineOwned, 0, strlen($machineOwned) - 1) : '';
    $machineOwner = $machine_counter > 0 ? 'Yes' : 'No';
    $persistentBasketLoaded = count(WC()->cart->get_cart()) > 0 ? 'True' : 'False';
}

$segment = '';
$parsed = parse_url($current_url);
if (isset($parsed['path'])) {
    $path = $parsed['path'];
    $path_parts = explode('/', $path);
    $segment = sizeof($path_parts) > 1 ? $path_parts[1] : '';

    if ($segment == 'vi' || $segment == 'vn') {
        $segment = $path_parts[2];
        $thank_path = @$path_parts[3];
    } else {
        $thank_path = @$path_parts[2];
    }
}
?>

<?php

if ( isset($thank_path) && $thank_path == 'order-received' ) :

    nespresso_gtm_thank_you_page();

elseif ($segment == 'checkout'):

    nespresso_gtm_checkout();

elseif ($segment == 'my-account'):

    nespresso_gtm_my_account();

elseif ($segment == 'register'):

    nespresso_gtm_register();

else :

    global $post;
    $pageCategory = @$post->post_name;

    $pageSubCategory = '';

    $slugs = array(
        0 => array('category' => 'coffee', 'alias_name' => 'Capsule', 'alias_slug' => 'capsule'),
        1 => array('category' => 'machines', 'alias_name' => 'Machine', 'alias_slug' => 'machine'),
        2 => array('category' => 'accessories', 'alias_name' => 'Accessory', 'alias_slug' => 'accessory'),
        3 => array('category' => 'login', 'alias_name' => 'Login', 'alias_slug' => 'login'),
        4 => array('category' => 'register', 'alias_name' => 'Register', 'alias_slug' => 'register'),
    );

    $products_array = [];
    foreach ($slugs as $slug) {
        if ($categorySlug == $slug['alias_slug']) {

            $args = array('post_type' => 'product', 'product_cat' => $slug['category']);
            $loop = new WP_Query($args);
            while ($loop->have_posts()): $loop->the_post();
                global $product;
                if ($product->is_type('simple')) {
                    $product_type = 'Simple Product - ' . $slug['alias_name'];
                } elseif ($product->is_type('variable')) {
                $product_type = 'Variable Product - ' . $slug['alias_name'];
            }

            $product_attr_str = '';
            $product_id = get_the_id();
            $product_attributes = get_post_meta($product_id);

            $product = new WC_Product($product_id);
            // Get SKU
            $sku      = $product->get_sku();
            $hq_data  = get_hq_data($sku);
            if ( isset($product_attributes['_product_attributes'] ) ) :
                foreach ($product_attributes['_product_attributes'] as $k => $v) {
                    $attr = unserialize($v);
                    if ((isset($attr['name'])) && isset($attr['color'])) {
                        $attr_name = $attr['name']['value'];
                        $attr_color = $attr['color']['value'];
                        $product_attr_str = $attr_name . '|' . $attr_color;
                    }
                }
            endif;

            $product_array = array(
                'name' => isset($hq_data['name']) ? $hq_data['name'] : get_the_title(),
                'brand' => 'Nespresso',
                'id' => isset($hq_data['id']) ? $hq_data['id'] : $product_id,
                'price' => $product->get_price(),
                'category' => strtolower($slug['alias_name']),
                'quantity' => $product->get_stock_quantity(),
                'dimension44' => 'FALSE',
                'dimension53' => $sku,
                'dimension54' => get_the_title(),
                'dimension55' => isset($hq_data['product_range']) ? $hq_data['product_range'] : $product_attr_str,
                'dimension56' => isset($hq_data['technology']) ? $hq_data['product_range'] : 'VirtuoLine',
                'dimension57' => $product_type,
            );

            array_push($products_array, $product_array);
            endwhile;
            wp_reset_query();
        }
    }

    if ( $categorySlug ) :

        switch ( $categorySlug ) {
            case 'capsule':
                $products = get_coffee_products();
            break;
            case 'machine':
                $products = get_machine_products();
            break;
            case 'accessory':
                $products = get_accessory_products();
            break;
            default:
                $products = [];
            break;
        }

        $subCategory = [];
        foreach ( $products as $k => $pro ) :
            $subCategory[] = strtolower($k);
        endforeach;
        $pageSubCategory = implode('|', $subCategory);

    else:

        $product = wc_get_product( @$post->ID );

        $product = $product ? $product->get_data() : null;

        // $category_name = isset( $product['meta_data'] ) && isset( $product['meta_data'][0] ) && isset( $product['meta_data'][0]->key ) && $product['meta_data'][0]->key == 'product_type' ? strtolower($product['meta_data'][0]->value) : '';
        $category_name = get_post_meta($post->ID, 'product_type', true);

        if ( $category_name == 'coffee' || $category_name == 'Coffee' || $category_name == 'Machine' || $category_name == 'machine' || $category_name == 'accessory' || $category_name == 'Accessory')
            $category_name = 'Product';

        $product_type = $category_name;

        $categorySlug = strtolower($category_name);

        $category_name = ucfirst($category_name);

        $pageSubCategory = strtolower( get_the_title($product['id']) );

        $products_array = [ 0 => []];
        if ( $product ) :
            foreach ($product as $k => $val ) {
                $products_array[0][$k] = $val;
            }

            $product_data = new WC_Product($product['id']);
            // Get SKU
            $sku      = $product_data->get_sku();
            $hq_data  = get_hq_data($sku);
            $product_category = get_post_meta($product['id'], 'category', true);
            $dimension44 = 'False';
            if ($product_category == 'Discovery Offer')
                $dimension44 = 'True';

            $dimension57 = ($product_type == 'machine') ? 'Variable Product - ' . $product_type : 'Simple Product - ' . $product_type;

            $products_array[0]['id'] = isset($hq_data['id']) ? $hq_data['id'] : $product['id'];
            $products_array[0]['quantity'] = $product['stock_quantity'] ;
            $products_array[0]['brand'] = 'Nespresso';
            $products_array[0]['category'] = $pageSubCategory;
            $products_array[0]['dimension44'] = $dimension44;
            $products_array[0]['dimension53'] = $sku;
            $products_array[0]['dimension54'] = $product['name'];
            $products_array[0]['dimension55'] = isset($hq_data['product_range']) ? $hq_data['product_range'] : $pageSubCategory;
            $products_array[0]['dimension56'] = isset($hq_data['technology']) ? $hq_data['technology'] : 'VirtuoLine';
            $products_array[0]['dimension57'] = $dimension57;

        endif;

    endif;

    if ($category_name == '')
    	$category_name = get_the_title();
    if ($categorySlug == '')
    	$categorySlug = get_the_title();
?>

    <script type="text/javascript">

        //Please execute the following dataLayer initialization on page load with the required site or page level parameters and attributes.
        // **NOTE** If any value is not obtainable or not relevant to the page, please leave the value blank/empty so nothing is passed into Google Analytics.
        var page_category = document.getElementById('product_category');
        gtmDataObject = [{
            'isEnvironmentProd': 'Yes',
            'pageName': '<?php echo get_the_title(); ?> | Nespresso',
            'pageType': '<?php echo $category_name; ?>', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
            'pageCategory': '<?php echo $categorySlug; ?>', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
            'pageSubCategory': '<?php echo $pageSubCategory; ?>', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
            'pageTechnology': 'OriginalLine', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
            'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
            'market': 'VN',
            'version': 'VN - WooCommerce V1',
            'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
            'segmentBusiness': 'B2C',
            'currency': 'VND',
            'clubMemberID': '<?php echo $clubMemberID; ?>', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
            'clubMemberStatus': '<?php echo $clubMemberStatus; ?>', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
            'clubMemberLevel': '<?= $clubMemberLevel ?>', //market level name for user level that is a club member
            'clubMemberTierID': '<?= $clubMemberLevelID ?>', //Global HQ level ID for club membership level
            'clubMemberTitle': '<?php echo $clubMemberTitle; ?>',
            'clubMemberLoginStatus': '<?php echo $clubMemberLoginStatus; ?>', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
            'machineOwner': '<?php echo isset($machineOwner) ? $machineOwner : '' ; ?>', //If user owns a machine or not, if known
            'machineOwned': '<?php echo isset($machineOwned) ? $machineOwned : ''; ?>', //If multiple machines owned, separate with "|||" symbol
            'preferredTechnology': 'Espresso',
            'event': 'event_pageView',
            'persistentBasketLoaded': '<?= $persistentBasketLoaded ?>', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
            'language': '<?= $language ?>'
        }];
    </script>
    <?php
    // global $wp;
    // $pos = strpos($wp->request, 'product/');

    // Note our use of ===.  Simply == would not work as expected
    // because the position of 'a' was the 0th (first) character.
    if ($category_name == 'Product'):?>
        <script type="text/javascript">
            gtmDataObject.push({
                'event': 'detailView',
                'landscape': 'Wordpress',
                'segmentBusiness': 'B2C',
                'currencyCode' : 'VND',
                'ecommerce': {
                    'detail': {
                        'actionField': {

                        },
                        'products': [
                            <?php
                                foreach ($products_array as $product) {
                            ?>
                                {
                                    'name': '<?php echo $product['name']; ?>',
                                    'brand': '<?php echo $product['brand']; ?>',
                                    'id': '<?php echo $product['id']; ?>',
                                    'price': '<?php echo $product['price']; ?>',
                                    'category': '<?php echo strtolower($product['category']); ?>',
                                    'quantity': '<?php echo $product['quantity']; ?>',
                                    'dimension44': '<?php echo $product['dimension44']; ?>',
                                    'dimension53': '<?php echo $product['dimension53']; ?>',
                                    'dimension54': '<?php echo $product['dimension54']; ?>',
                                    // 'dimension55': '<?php echo $product['dimension55']; ?>',
                                    'dimension55': '<?= $pageSubCategory ?>',
                                    'dimension56': '<?php echo $product['dimension56']; ?>',
                                    'dimension57': '<?php echo $product['dimension57']; ?>'
                                },
                            <?php
                                }
                            ?>
                        ],
                    },
                },
            });
        </script>
    <?php endif;?>
<?php endif;?>
<script type="text/javascript">
    function removeFromCartGTM(cart_item_key) {
        var data = {
            action: 'gtm_get_basket',
            cart_item_key: cart_item_key
        };
        $.ajax({
            dataType: 'json',
            type: 'post',
            data: data,
            url: ajaxurl,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                // return false;
            },
            success: function (data) {
                if(data.status) {
                    gtmDataObject.push({
                        'event': 'removeFromCart',
                        'landscape': 'Wordpress',
                        'segmentBusiness': 'B2C',
                        'currencyCode' : 'VND',
                        'ecommerce': {
                            'remove': {
                                'actionField': {

                                },
                                'products': data.products
                            },
                        },
                    });
                }

            } // success()

        });
    }

    function addToCartGTM(item) {
        var data = {
            action: 'gtm_get_basket',
        };
        var metric10 = ''
        var metric11 = ''
        var metric12 = ''

        var dimension57 = '';

        switch (item.type)
        {
            case 'Coffee':
                dimension57 = 'Single Product - Capsule';
                metric10 = item.quantity;
            break;
            case 'Machine':
                dimension57 = 'Variable Product - Machine';
                metric11 = item.quantity;
            break;
            case 'Accessory':
                dimension57 = 'Single Product - Accessory';
                metric12 = item.quantity;
            break;

        }

        // var data = {
        //     action: 'gtm_add_to_cart',
        //     id: item.id
        // };
        // $.ajax({
        //     dataType: 'json',
        //     type: 'post',
        //     data: data,
        //     url: ajaxurl,
        //     dataType: 'json',
        //     error: function (jqXHR, textStatus, errorThrown) {
        //         console.log(errorThrown);
        //         // return false;
        //     },
        //     success: function (data) {
        //         if(data.status) {
        //             gtmDataObject.push({
        //                 'event': 'removeFromCart',
        //                 'landscape': 'Wordpress',
        //                 'segmentBusiness': 'B2C',
        //                 'currencyCode' : 'VND',
        //                 'ecommerce': {
        //                     'remove': {
        //                         'actionField': {

        //                         },
        //                         'products': data.products
        //                     },
        //                 },
        //             });
        //         }

        //     } // success()

        // });

        gtmDataObject.push({
            'event': 'addToCart',
            'landscape': 'Wordpress',
            'segmentBusiness': 'B2C',
            'currencyCode' : 'VND',
            'ecommerce': {
                'add': {
                    'actionField': {

                    },
                    'products': [
                        {
                            'name': item.name,
                            'id': item.hqid,
                            'dimension54': item.name,
                            'dimension55': item.range,
                            'dimension53': item.sku,
                            'quantity': item.quantity,
                            'price': item.price,
                            'category': item.type.toLowerCase(),
                            'dimension56': item.technology,
                            'brand': 'Nespresso',
                            'metric10': metric10,
                            'metric11': metric11,
                            'metric12': metric12,
                            'dimension57': dimension57
                        }
                    ]
                },
            },
        });
    }
</script>
