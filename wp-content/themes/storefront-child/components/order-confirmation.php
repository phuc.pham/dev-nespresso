<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
?>

<!-- content -->

<main id="content"  class="shopping-bag">
	<div class="container">
	    <div class="checkister step5">
	        <div class="main-content clearfix" id="JumpContent" tabindex="-1">
	            <div class="checkister-wrapper">
	                <h2>Thank you for your order</h2>
	                <p>You order has been succesfully sent. Thank you for shopping with Nespresso. </p>
	                <h3>Your package will be delevered to</h3>
	                <div class="address__preview"> Mr First Name Last Name <br>Address line <br>1234 City <br>Switzerland</div>
	                <h3>Delivery details</h3>
	                <span class="bloc__summary"><i class="icon-Delivery_off"></i> Delivery mode: <strong>Standard</strong></span>
		            <br>
		            <p>
	                    <a href="#" title="Track your order" class="link">Track your order status in the my account section</a>
		            </p>
	                <a class="btn btn-green btn-icon" href="<?php bloginfo('url');?>" title="Proceed to payment"><i class="icon icon-arrow_right"></i> <span>Back to shop</span></a>
	            </div>
	        </div>
	    </div>
    </div>
</main>

<!-- /content -->
