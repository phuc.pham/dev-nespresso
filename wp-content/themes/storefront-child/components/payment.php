<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
?>

<!-- content -->

<main id="content" class="shopping-bag">

	<div class="container">
    <?php $stepActive = 3; ?>

    <?php get_template_part('components/wizard'); ?>

    <div class="checkister step3" id="JumpContent" tabindex="-1">
        <header>
            <h2>Payment</h2>
        </header>
        <section class="row">
            <div class="hidden-xs hidden-sm col-md-4 pull-right">
                <aside>
                    <span class="col-title">Order details</span>

                    <div class="sbag-small sbag-loading"></div>
                    <ul class="bloc-total hidden">
                        <li class="bloc-total-stotal">
                            <span class="stotal">Subtotal</span>
                            <span class="price">$<span class="price-int">0</span></span>
                        </li>
                        <li class="bloc-total-vat">
                            <span class="vat">Vat (inCL.)</span>
                            <span class="price">$<span class="price-int">0</span></span>
                        </li>
                        <li class="bloc-total-total">
                            <span class="total">Total</span>
                            <span class="tprice">$<span class="price-int">0</span></span>
                        </li>
                    </ul>
	                <a href="<?php bloginfo('url'); ?>/shopping-bag" title="Modify" class="btn btn-primary btn-block">Edit</a>
                </aside>
            </div>

            <div class="col-xs-12 col-md-8">
                <div class="main-content clearfix">

                    <form method="POST" action="delivery-mode">
                        <section class="form">
	                        <section  class="delivery-address">
		                        <h3 class="main-content__title">Billing address</h3>
		                        <div class="bloc-delivery bloc-address account-detail" style="background: white">
			                        <div class="row">
				                        <dl class="col-sm-6">
					                        <dt class="address__preview__label">Address</dt>
					                        <dd class="address__preview__value">
						                        Mr First name second name <br>
						                        Adress line <br>
						                        1234 City <br>
						                        Switzerland
					                        </dd>
					                        <a href="<?php bloginfo('url'); ?>/delivery-address" class="link link--right" title="Edit address">Edit address</a>
				                        </dl>
				                        <dl class="col-sm-6">
					                        <dt class="address__preview__label">Belling remark</dt>
					                        <dd class="address__preview__value">Door Code 1234 – Leave on the door front if not there.</dd>
				                        </dl>
				                        <p class="pull-left col-sm-12">Currently set as your default billing address</p>
			                        </div>
			                        <div class="delivery-address-buttons">
				                        <a href="<?php bloginfo('url'); ?>/adresses" class="btn btn-primary" title="Choose another delivery address">Choose another address</a>
				                        <a href="<?php bloginfo('url'); ?>/adresses" class="btn btn-primary" title="Add another delivery address">Add another address</a>
			                        </div>
		                        </div>
	                        </section>

                            <section class="payment-method">
                                <h3 class="main-content__title">Payment method</h3>
                                <h4 class="main-content__subtitle">Gift card</h4>
                                <a href="#" class="coupon-help" title="Do you have a coupon">Do you have a coupon <i class="fa fa-question-circle"></i></a>
                                <div class="bloc-payment">
                                    <div class="radio" role="group" aria-labelledby="payment_method_1">
                                        <label for="payment_method_1">
                                            <input type="radio" name="payment_method" id="payment_method_1" value="Gift card payment" title="Gift card payment">
                                            <i class="icon icon-Gift_cart_off"></i>
                                            <span>Gift card payment</span>
                                        </label>
                                    </div>
                                    <div class="content">
	                                    <a href="#" title="Learn More" class="link link--right">Learn more</a>
                                    </div>
                                    <div class="bloc-gift-card" data-link="giftCard" style="margin: 0 -20px; padding: 0 20px;">
                                        <div class="new-gift-card">
                                            <div class="row">
	                                            <div class="input-group input-group-generic">
		                                            <label class="desktop-label col-sm-3 col-md-4" for="serial-number">Serial number<span class="required">*</span></label>
		                                            <div class="col-sm-6">
			                                            <input type="text" title="My postal code" id="serial-number" name="serial-number" placeholder="Serial number" class="form-control col-sm-12 col-md-9">
			                                            <span class="mobile-label">Serial number<span class="required">*</span></span>
		                                            </div>
	                                            </div>
	                                            <div class="input-group input-group-generic">
		                                            <label class="desktop-label col-sm-3 col-md-4" for="scratch-code">Scratch code<span class="required">*</span></label>
		                                            <div class="col-sm-6">
			                                            <input type="text" title="My postal code" id="scratch-code" name="scratch-code" placeholder="Scratch code" class="form-control col-sm-12 col-md-9">
			                                            <span class="mobile-label">Scratch code<span class="required">*</span></span>
		                                            </div>
	                                            </div>
	                                            <a href="#" class="btn btn-primary pull-right" title="Select this point">Select</a>
                                            </div>
                                        </div>
	                                    <div class="clearfix" style="background: white; margin: 20px -20px 0; padding: 20px">
	                                        <table class="table">
	                                            <caption class="hidden">Gift Card</caption>
	                                            <thead>
	                                                <tr>
	                                                    <th scope="col" class="hidden-xs">Serial number</th>
	                                                    <th scope="col" class="hidden-xs">Scratch code</th>
	                                                    <th scope="col" >Expiration date</th>
	                                                    <th scope="col" class="hidden-xs">Balance before use</th>
	                                                    <th scope="col" colspan="2">Amount used</th>
	                                                </tr>
	                                            </thead>
	                                            <tbody>
	                                                <tr>
	                                                    <td class="hidden-xs">100000000060978</td>
	                                                    <td class="hidden-xs">1111</td>
	                                                    <td>12 / 10 / 2017</td>
	                                                    <td class="hidden-xs">$ 100.00</td>
	                                                    <td>$ 100.00</td>
	                                                    <td><a href="#" title="remove"><i class="icon icon-Picto_croisrefermer"></i></a></td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="hidden-xs">100000000060978</td>
	                                                    <td class="hidden-xs">1111</td>
	                                                    <td>12 / 10 / 2017</td>
	                                                    <td class="hidden-xs">$ 100.00</td>
	                                                    <td>$ 100.00</td>
	                                                    <td><a href="#" title="remove"><i class="icon icon-Picto_croisrefermer"></i></a></td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="hidden-xs">100000000060978</td>
	                                                    <td class="hidden-xs">1111</td>
	                                                    <td>12 / 10 / 2017</td>
	                                                    <td class="hidden-xs">$ 100.00</td>
	                                                    <td>$ 100.00</td>
	                                                    <td><a href="#" title="remove"><i class="icon icon-Picto_croisrefermer"></i></a></td>
	                                                </tr>
	                                            </tbody>
	                                        </table>
		                                    <div class="clearfix">
	                                            <a class="btn btn-primary pull-right" href="#" title="Add a gift card">Add a gift card</a>
		                                    </div>
	                                        <ul class="bloc-total">
	                                            <li class="bloc-total-vat">
	                                                <span class="vat">Gift Card Total</span>
	                                                <span class="price">$<span class="price-int">200.00</span></span>
	                                            </li>
	                                            <li class="bloc-total-total">
	                                                <span class="total">Remaining amount to pay</span>
	                                                <span class="tprice">$<span class="price-int">75.00</span></span>
	                                            </li>
	                                        </ul>
	                                        <p>Please add a gift card select one of the payment option bellow to pay for the remainder of this order.</p>
	                                    </div>
                                    </div>
                                </div>

                                <h4 class="main-content__subtitle">Other payment methods</h4>
                                <div class="bloc-payment bloc-payment--other">
                                    <div class="radio" role="group" aria-labelledby="payment_method_2">
                                        <label for="payment_method_2">
                                            <input type="radio" name="payment_method" id="payment_method_2" value="Credit card payment" title="Credit card payment">
                                            <i class="icon icon-Checkout_prefcredit_cart_off"></i>
                                            <span>Credit card payment</span>
                                        </label>
                                    </div>
                                    <div class="content">
                                        <a href="#" title="Learn More" class="link link--right">Learn more</a>
                                    </div>
                                    <div class="bloc-card-type" data-link="creditCard">
                                        <div class="input-group input-group-generic">
                                            <div class="col-xs-12 radio-group">
                                                <div class="radio" role="group" aria-labelledby="card_type-1">
                                                    <label for="card_type-1">
                                                        <input value="none" id="card_type-1" name="cart-type" checked="checked" type="radio">
                                                        <img class="icon-img" height="24" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-mastercard.png" alt="Mastercard">
                                                    </label>
                                                </div>
                                                <div class="radio" role="group" aria-labelledby="card_type-2">
                                                    <label for="card_type-2">
                                                        <input value="none" id="card_type-2" name="cart-type" type="radio">
                                                        <img class="icon-img" height="24" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-visa.png" alt="Visa">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4" for="holder-name">Holder name<span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" title="Holder name" id="holder-name" name="name" placeholder="Holder name *" class="form-control col-sm-12 col-md-9">
                                                <span class="mobile-label">Holder name<span class="required">*</span></span>
                                            </div>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4" for="card-number">Card number<span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" title="Card number" id="card-number" name="name" placeholder="Card number *" class="form-control col-sm-12 col-md-9">
                                                <span class="mobile-label">Card number<span class="required">*</span></span>
                                            </div>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-4 col-md-4" for="month" id="expiry-date">Expiry<span class="required">*</span></label>
                                            <div class="col-sm-4 col-xs-6">
                                                <div class="dropdown dropdown--input">
                                                    <button type="button" class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current">Please make a choice</span></button>
                                                    <select tabindex="-1" name="month" id="month" aria-labelledby="expiry-date">
                                                        <option value="1">Month</option>
                                                        <option value="2">Fran&ccedil;ais</option>
                                                        <option value="3">Deutsch</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-xs-6">
                                                <div class="dropdown dropdown--input ">
                                                    <button type="button" class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current">Please make a choice</span></button>
                                                    <select tabindex="-1" name="year" id="year" aria-labelledby="expiry-date">
                                                        <option value="1">Year</option>
                                                        <option value="2">Fran&ccedil;ais</option>
                                                        <option value="3">Deutsch</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-4 col-md-4" for="cvv">CVC / CVV Code<span class="required">*</span></label>
                                            <div class="col-xs-12 col-sm-4">
                                                <input type="text" title="CVC / CVV Code" id="cvv" name="name" placeholder="Code" class="form-control col-sm-12 col-md-9">
                                                <span class="mobile-label">CVC / CVV Code</span>
                                            </div>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <div class="checkbox">
                                                <label for="save-card">
                                                    <input value="Save this card for a future order" id="save-card" name="save-card" type="checkbox">
                                                    Save this card for a future order
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="bloc-payment bloc-payment--other">
                                    <div class="radio" role="group" aria-labelledby="payment_method_3">
                                        <label for="payment_method_3">
                                            <input type="radio" name="payment_method" id="payment_method_3" value="Invoice" title="Invoice">
                                            <i class="icon icon-invoice-off"></i>
                                            <span>Invoice</span>
                                        </label>
                                    </div>
                                    <div class="content">
                                        <p>Payable upon 30 days</p>
                                    </div>
                                </div>

                                <div class="bloc-payment bloc-payment--other">
                                    <div class="radio" role="group" aria-labelledby="payment_method_4">
                                        <label for="payment_method_4">
                                            <input type="radio" name="payment_method" id="payment_method_4" value="PostFinance Card" title="PostFinance Card">
                                            <i class="icon icon-Checkout_prefcredit_cart_off"></i>
                                            <span>PostFinance Card</span>
                                        </label>
                                    </div>
                                </div>

                            </section>
                            <div class="link-wrapper">
                                <a class="btn btn-green btn-icon-right pull-right" href="<?php bloginfo('url'); ?>/order-summary" title="Place order"><span>Place order</span> <i class="icon icon-arrow_left"></i></a>
                            </div>
                        </section>
                    </form>
                </div>
            </div>

        </section>
    </div>
    </div>
</main>
<?php
global $scripts;
$scripts[] = 'js/components/payment-accordeon.js';
$scripts[] = 'js/components/shopping-bag-small.js';
?>

<!-- /content -->
