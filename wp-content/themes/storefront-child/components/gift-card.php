<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
?>

<!-- content -->

<div class="container">
    <div class="row">
        <div class="welcome-offer">
            <div class="welcome-offer-title"><h1><em>Nespresso</em> Gift Card</h1></div>
            <div class="welcome-offer-main clearfix">
                <div class="col-xs-12 col-sm-6 welcome-offer-bloc clearfix">
                    #1 YOUR <span class="welcome-offer-hilight">GIFT CARD COVER</span>
                    <img class="welcome-offer-cover-card" src="<?php echo get_stylesheet_directory_uri(); ?>/images/welcome-offer.png" alt="Gift card cover"/>
                </div>
                <div class="col-xs-12 col-sm-6 welcome-offer-bloc welcome-offer-delimiter clearfix">
                    #2 CHOSE A <span class="welcome-offer-hilight">GIFT CARD AMOUNT</span>
                    <div class="welcome-offer-selector">
                        <div class="welcome-offer-selector-down"><i class="fa fa-chevron-down"></i></div>
                        <div class="welcome-offer-selector-value">25</div>
                        <div class="welcome-offer-selector-up"><i class="fa fa-chevron-up"></i></div>
                    </div>
                    <span class="welcome-offer-selector-details">
                        Gift card value must be a multiple of 25, minimum $ 25 - maximum $ 500.
                    </span>
                    <button class="btn btn-icon btn-green">
                        <i class="icon-nespresso"></i> Add to basket
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /content -->
