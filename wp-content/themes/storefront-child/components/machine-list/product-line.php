<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$p_colors = [];

if ( isset($product->variations) && $product->variations ) :
    foreach ( $product->variations as $i=>$variant ) :
        if ( isset( $variant['attributes'] ) )
            if ( isset( $variant['attributes']['attribute_pa_color'] ) )
                if ( !in_array($variant['attributes']['attribute_pa_color'], $p_colors) )
                    $p_colors[] = strtolower($variant['attributes']['attribute_pa_color']);
				if($i==0 && $variant['display_price'])
					{
						 $product->price = $variant['display_price'];
						 $product->regular_price = $variant['display_regular_price'];
					}
    endforeach;
endif;
//echo '<pre>',print_r($product),'</pre>';
$hq_data  = get_hq_data($product->sku);
?>

<li class="track-impression-product product product--machine"
    data-product-item-id="<?= $product->sku ?>"
    data-product-section="Machine List"
    data-product-position="<?= $counter ?>"

    data-color="<?= implode(",", $p_colors) ?>"
    data-price="<?= $product->price ?>"
>
    <div class="product-image">
	<?=get_nespresso_show_off($product->label_off);?>
        <a href="<?= $product->url ?>" title="<?= $product->name ?>"
            class="nespresso-product-item"
            title="<?= $product->name ?>"
            data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
            data-sku="<?= $product->sku ?>"
            data-name="<?=$product->name?>"
            data-type="<?=$product->product_type?>"
            data-position="<?= $counter ?>"
            data-list="<?= get_the_title() ?>"
            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
            data-price="<?= $product->price ?>"
            data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
        >
            <img src="<?= $product->image ? $product->image[0] : '' ?>" class="product-image" alt="<?= $product->name ?>">
			
        </a>
    </div>

    <div class="product-text">

        <?php if( isset( $product->description ) ) : ?>
          <div class="product-description">
        <?php else: ?>
          <div class="product-description small">
        <?php endif; ?>

            <h3>
                <a href="<?= $product->url ?>" title="<?= $product->name ?>"
                    class="nespresso-product-item"
                    title="<?= $product->name ?>"
                    data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
                    data-sku="<?= $product->sku ?>"
                    data-name="<?=$product->name?>"
                    data-type="<?=$product->product_type?>"
                    data-position="<?= $counter ?>"
                    data-list="<?= get_the_title() ?>"
                    data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                    data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
                    data-price="<?= $product->price ?>"
                >
                    <?= $product->name ?>
                </a>
            </h3>

            <?php if(isset($product->short_description)): ?>
                <p><?= $product->short_description ?></p>
            <?php endif; ?>

            <?php if( $p_colors ): ?>
                <div class="color">
                    <span class="color__title">Available colors :</span>
                    <?php foreach( $p_colors as $color): ?>
                        <style type="text/css"> #colt__elt_<?= $color ?>:after { background-color:<?= $color ?>; }</style>
                        <span class="color__elt" id="colt__elt_<?= $color ?>"></span>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($product->price): ?>
            <div class="product-price"><?= wc_price($product->price) ?>
			<?php if($product->regular_price && $product->regular_price>$product->price){ ?>
                <span class="product-price--old"><?=wc_price($product->regular_price) ?></span>
            <?php  } ?>
			</div>
        <?php endif;?>

    </div>

    <div class="product-add">
       <!-- <button class="btn btn-icon btn-block btn-green btn-icon-right"
            data-id="<?= $product->post_id ?>"
            data-cart="true"
            data-name="<?= $product->name ?>"
            data-price="<?= $product->price ?>"
            data-image-url="<?= $product->image ? $product->image[0] : '' ?>"
            data-type="<?= $product->product_type ?>"
            data-qty-step="1"
            data-vat="1"
            data-url="<?= $product->url ?>"
        >
            <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
        </button> -->
        <?php if ($product->price):
            $class = "btn-green";
            $label = "View Details &amp; Buy";
        else:
            $class = "btn-disabled";
            $label = "Out of stock";
        endif; ?>
        <a href="<?= $product->url ?>" title="" class="btn btn-icon btn-block <?= $class ?> btn-icon-right">
            <?= $label ?>
        </a>
        <?php if (isset($product->discover_the_range_link) && trim($product->discover_the_range_link) ) { ?>
            <a href="<?php echo trim($product->discover_the_range_link); ?>" class="link-btn discover-the-range" title="Discover the range"> <span class="fa fa-long-arrow-right"></span> DISCOVER THE RANGE</a>
        <?php } // endif ?>
    </div>
</li>
