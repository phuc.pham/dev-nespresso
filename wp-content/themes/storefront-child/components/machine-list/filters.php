<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$f_colors = [];
foreach ( $products as $k => $product ) :
    if ( isset($product->variations) && $product->variations ) :
        foreach ( $product->variations as $variant ) :
            if ( isset( $variant['attributes'] ) )
                if ( isset( $variant['attributes']['attribute_color'] ) )
                    $f_colors[] = strtolower($variant['attributes']['attribute_color']);
        endforeach;
    endif;
endforeach;

?>

<form action="#" method="get" id="filters-form-<?php echo rand(0,9999); ?>">

    <?php if ( $f_colors ) :?>
        <div class="filter">
            <div class="title">COLOR</div>
            <div class="control colors">
                <?php foreach( $f_colors as $k => $color ) : ?>
                    <span role="group" aria-labelledby="control-color<?= $k ?>">
                        <label>
                            <input type="checkbox" name="color" value="<?= $color ?>">
                            <i class="circle-<?= strtolower($color) ?>" style="background-color: <?= $color ?>;"></i> <span class="tooltip"><?= $color ?></span>
                        </label>
                    </span>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="filter">
        <div class="title">PRICE</div>
        <div class="control price-range">
            <div class="range"></div>
        </div>
    </div>

</form>
