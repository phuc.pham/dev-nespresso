<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( isset($_POST['form-for']) && $_POST['form-for'] == 'contact-preferences' ) :

    global $nespresso_my_contact_preferences_hook_script;

	$validation = new Validation();

	if ( !$validation->valid_subscribed_in_nesspresso_club() ) :
        $nespresso_my_contact_preferences_hook_script = 'validation.set_valid(false).validate_billing_address_type();';

    elseif ( !$validation->valid_contact_preferences() ) :
        $nespresso_my_contact_preferences_hook_script = 'validation.set_valid(false).validate_contact_preferences();';

    elseif ( !$validation->valid_subscribed_in_nesspresso_news() ) :
        $nespresso_my_contact_preferences_hook_script = 'validation.set_valid(false).validate_subscribed_in_nesspresso_news();';

    else :

	    $user = nespresso_get_user_data();

	    $contact_preferences = [];
	    if ( isset($_POST['contact_preferences']) ) :
			foreach ( $_POST['contact_preferences'] as $contact_preference ) :
				$contact_preferences[] = $contact_preference;
			endforeach;
		endif;

	    update_user_meta( $user->ID, 'subscribed_in_nesspresso_club', sanitize_text_field( isset($_POST['subscribed_in_nesspresso_club']) ? $_POST['subscribed_in_nesspresso_club'] : 0 ) );
	    update_user_meta( $user->ID, 'contact_preferences', $contact_preferences );
	    update_user_meta( $user->ID, 'subscribed_in_nesspresso_news', sanitize_text_field( isset($_POST['subscribed_in_nesspresso_news']) ? $_POST['subscribed_in_nesspresso_news'] : 0 ) );

        $nespresso_my_contact_preferences_hook_script = 'validation.success_information_updated();';

    endif;

    // put some scripts on the bottom
    if ( $nespresso_my_contact_preferences_hook_script ) :
        function nespresso_my_contact_preferences_hook() {
            global $nespresso_my_contact_preferences_hook_script;
        ?>
            <script type="text/javascript">
                var validation = new Validation();
                <?= $nespresso_my_contact_preferences_hook_script ?>
            </script>
        <?php
        } // nespresso_my_express_checkout_hook()
        add_action('wp_footer', 'nespresso_my_contact_preferences_hook');
    endif;

endif;

$my_account_url = get_permalink( get_option('woocommerce_myaccount_page_id') );

$user = nespresso_get_user_data();

$current_url = home_url(add_query_arg(null, null));

?>

<main id="content">
	<div class="container">
	    <div class="row">

	        <!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <form action="<?= $current_url ?>" method="post" id="form-contact-preferences" role="form">

	        	<input type="hidden" name="form-for" value="contact-preferences">

	            <div class="col-xs-12 col-md-8-5 account-detail my-contact">
		            <header>
		                <h2><?= get_the_title() ?></h2>
		            </header>

	                <header>
	                    <h3>My <i>Nespresso</i> News</h3>
	                </header>

	                <div class="checkbox mr-top-20 mr-bottom-20">
	                    <span class="checkbox-title mr-bottom-20">Subscribe to our newsletter</span>
	                    <label for="keep-me-informated">
	                    	<div class="pull-left mr-bottom-20">
		                        <input value="1"
		                        	id="subscribed_in_nesspresso_club"
		                        	name="subscribed_in_nesspresso_club"
		                        	type="checkbox"
		                        	<?= isset($user->subscribed_in_nesspresso_club) && $user->subscribed_in_nesspresso_club ? 'checked="checked"' : '' ?>
	                        	>
                        	</div>
                        	<div class="pull-left mr-bottom-20">
	                       		Get the latest updates on our promos and exclusive invites to upcoming events of Nespresso Vietnam
                       		</div>
	                    </label>
	                </div>

<!-- 	                <header>
	                    <h3>My contact preferences</h3>
	                </header>

	                <div class="checkbox">
	                    <span class="checkbox-title">Your Contact Methods</span>
	                    <label for="contact-methods1">
	                        <input value="phone"
	                        	name="contact_preferences[]"
	                        	type="checkbox"
	                        	<?= isset($user->contact_preferences) && in_array('phone', $user->contact_preferences) ? 'checked="checked"' : '' ?>
                        	>
	                        By phone
	                    </label>
	                    <label for="contact-methods2">
	                        <input value="post"
	                        	name="contact_preferences[]"
	                        	type="checkbox"
	                        	<?= isset($user->contact_preferences) && in_array('post', $user->contact_preferences) ? 'checked="checked"' : '' ?>
                        	>
	                        By post
	                    </label>
	                    <label for="contact-methods3">
	                        <input value="sms"
	                        	name="contact_preferences[]"
	                        	type="checkbox"
	                        	<?= isset($user->contact_preferences) && in_array('sms', $user->contact_preferences) ? 'checked="checked"' : '' ?>
                        	>
	                        By SMS
	                    </label>
	                </div>

	                <header>
	                    <h3>Nespresso Magazine</h3>
	                </header>

	                <div class="checkbox">
	                    <span class="checkbox-title">Be notified of the launch of Limited edition Coffees</span>
	                    <label for="notify-limited-edition">
	                        <input value="1"
	                        	id="subscribed_in_nesspresso_news"
	                        	name="subscribed_in_nesspresso_news"
	                        	type="checkbox"
	                        	<?= isset($user->subscribed_in_nesspresso_news) && $user->subscribed_in_nesspresso_news ? 'checked="checked"' : '' ?>
                        	>
	                        Subscribe to <i>Nespresso</i> News via email
	                    </label>
	                </div> -->

	                <div class="account-detail__footer clearfix">
	                    <button type="submit" class="btn btn-primary pull-right" id="btn-save" title="Save my preferences">Save my preferences</a>
	                </div>

		        </div><!-- .account-detail -->

	        </form><!-- #form-contanct-preferences -->
        </div><!-- .row -->
    </div><!-- .container -->
</main><!-- #content -->

<?php
	global $scripts;
	$scripts[] = 'js/components/validation.js';
	$scripts[] = 'js/components/my-contact-preferences.js';
?>
