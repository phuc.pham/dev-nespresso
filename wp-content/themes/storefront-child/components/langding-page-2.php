<?php
	ob_start();
	session_start();
	
	if (!defined('ABSPATH'))
	{
		exit; // Exit if accessed directly
	}
	
	
	if($_POST)
    {
		$result = "";

        foreach ($_POST as $key => $value)
        {
	        $result .= '"'. $key .'":"'.$value.'",';
        }
	    $result = rtrim($result,',');
	    $result = '{'. $result .'}';
	
	    $_SESSION['result_customer_langdingpage_2'] = $result;
	    
    }
	
	$table = 'wp_langdingpage2_question';
	$questions = $wpdb->get_results(  "SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 0,2" );
	
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/langdingpage2.css">
<!-- content -->

<main id="content" class="fullPage fullPage__plp coffee-list">
	<!-- end banner -->
	<div class="container">
		<div class="row">
			<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
				<div class="productlist-main clearfix">
					<div class="productlist-panel col-xs-12 col-md-12">
						<div class="productlist-contents">
							<div class="col-md-12 text-center">
								<h1>
									WELCOME TO NESPRESSO BUILDING CORNER 2021
								</h1>
							</div>
							<section class="product-category text-center" >
								<a title="PERSONAL INFORMATION"  style="background-color:#986F38" class="btn">PERSONAL INFORMATION</a>
                                <?php
                                
                                 foreach ($questions as $question)
                                 {
                                     if($question->type_question == 1)
                                     {
                                ?>
                                <div class="card" style="margin: 20px 0px ">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h1 class="input-group-text" ><?= $question->question ?></h1>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" style="width: 100%" class="form-control langdingpage2" name="yourcompany" id="yourcompany" placeholder="Insert your answer here… ...." value="" required="">
                                            <small id="yourcompany" data-msgvi="Vui lòng nhập tên công ty của bạn " data-msg="Insert your answer here…" class="text-danger"></small>
                                        </div>


                                    </div>
                                </div>
                                <?php
                                     }
                                     else
                                     {
                                         ?>
                                             <div class="row">
                                         <div class="col-md-6">
                                                <h1 class="input-group-text" ><?= $question->question ?></h1>
                                            </div>
                                         <div class="col-md-6" >
	
	                                     <?php
                                             global $wpdb;
                                             $table_answer = 'wp_langdingpage2_answer';
//
                                             $answer = $wpdb->get_row(  $wpdb->prepare(
	                                             "SELECT `id`,`answer`,`class` FROM $table_answer as a WHERE a.id_question = %d",
	                                             $question->id
                                             ));
                                             
                                             $i = 1;
                                             
                                             foreach (explode(',',$answer->answer) as $key => $ite)
                                             {
                                         ?>
                                                
                                             <div class="custom-control custom-radio">
                                                 <input type="radio" id="<?= $question->name. $i ?>" name="<?= $question->name ?>" value="<?= $ite ?>" class="custom-control-input radiolangdingpage2">
                                                 <label class="custom-control-label" for="<?= $question->name. $i ?>"><?= $ite ?></label>
                                             </div>
                                                 
                                         
                                        <?php
                                                 $i++;
                                            }
                                             
                                             ?>
                                         </div>
                                             </div>
                                    <?php
                                     }
                                }
                                ?>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</main>
<script>
    function statusChange(con1,con2,langdingpage2,radiolangdingpage2){
        if(con1 == true && con2 == true)
        {
            jQuery.ajax({
                url : "#",
                type : "post",
                dataType:"text",
                data : {
                    question01 : langdingpage2,
                    question02 : radiolangdingpage2,
                },
                success : function (result){
                    window.location.href = "/langding_page_survey_1/";
                }
            });
        }
       
    };
    
    jQuery(document).ready(function($)
    {
        let con1 = false;
        let langdingpage2 = "";
        let con2 = false;
        let radiolangdingpage2 = "";

        $('.langdingpage2').focusout(function() {
               con1 = true;
               langdingpage2 = $(this).val();
                statusChange(con1,con2,langdingpage2,radiolangdingpage2);
        })

        $('.radiolangdingpage2').click(function(){
            if ($(this).is(':checked')) {
                con2= true;
                radiolangdingpage2 = $(this).val();
                statusChange(con1,con2,langdingpage2,radiolangdingpage2);
            }
        });
        
    });
</script>
<style>
    #content.fullPage__plp {
        background-image: url('/wp-content/themes/storefront-child/images/langding-page-2/BACKGROUND3.jpg');
        background-size: cover;
        background-position: center center;
        height: 700px;

    }

    .productlist .productlist-main .productlist-panel{
        background: unset;
    }
    /*input[type="checkbox"], input[type="radio"] {*/
    /*    display: none;*/
    /*}*/
    
    


</style>