<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $nespresso_lost_password_hook_script;
$nespresso_lost_password_hook_script = '';

// check if there is a post from my persona information form
if ( $_POST && isset($_POST['email'])) :

    $validation = new Validation();

    if ( !$validation->valid_email() ) :
        $nespresso_lost_password_hook_script = 'validation.set_valid(false).validate_email();';

    elseif ( !$validation->valid_email_exists() ) :
        $nespresso_lost_password_hook_script = 'validation.set_valid(false).validate_email_exists();';

    else :

        $to = sanitize_text_field($_POST['email']);
        $token = md5($to . '12wasdfgsd');

        // update the token field of the user
        $user = get_user_by('email', $_POST['email']);
        update_user_meta( $user->ID, 'token', sanitize_text_field( $token ) );

        // wp_met
        $token_url = get_site_url().'/reset-password?token='.$token;
        $headers = array('Content-Type: text/html; charset=UTF-8');
        //get woocommerce lost-password subject
        $email_setting  = get_option('woocommerce_customer_reset_password_settings');
        $subject        = isset($email_setting['subject']) && $email_setting['subject'] ? $email_setting['subject'] : 'Nespresso password reset';
        ob_start();
            include(__DIR__ . '/lost-password-template.php');
            $message = ob_get_contents();
        ob_end_clean();

        wp_mail( $to, $subject, $message, $headers );

        $nespresso_lost_password_hook_script = 'validation.success_password_email_token_send();';

    endif;

   // put some scripts on the bottom
    if ( $nespresso_lost_password_hook_script ) :
        function nespresso_lost_password_hook() {
            global $nespresso_lost_password_hook_script;
        ?>
            <script type="text/javascript">
                var validation = new Validation();
                <?= $nespresso_lost_password_hook_script ?>
            </script>
        <?php
        } // nespresso_my_info_hook()
        add_action('wp_footer', 'nespresso_lost_password_hook');
    endif;

endif;

?>

<main id="content" class="fullPage fullPage__plp" style="background-color:black" id="container-lost-password">

    <div class="container">

        <div class="mr-bottom-20 row login-page__row-container">

            <div class="col-xs-12 col-md-12 login-page__col">

                <div class="account-detail mr-bottom-20 gray-bgcolor">

                    <div class="row mr-top-20">
                        <header class="col-md-12">
                            <h2><?= wp_title() ?></h2>
                        </header>

                        <div class="col-md-6 col-xs-12">
                            Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <form method="post" action="/lost-password" class="form" role="form" id="form-lost-password">


                                <div class="input-group input-group-generic mr-top-20">
                                    <label class="desktop-label col-md-2" for="password_confirmation">Email<span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-9">
                                        <input type="email" id="email" name="email" class="form-control col-md-12">
                                        <span class="mobile-label">Email<span class="required">*</span></span>
                                    </div>
                                </div>

                                  <button type="submit" class="btn btn-default btn-icon-right mr-top-20" title="Submit" name="submit">Submit<i class="fa fa-send"></i></button>

                            </form>

                        </div>

                    </div><!-- ,row -->
                </div>

            </div><!-- .account-detail -->

        </div>

    </div>

</main>

<?php

global $scripts;
$scripts[] = 'js/components/validation.js';
$scripts[] = 'js/components/lost-password.js';
?>
