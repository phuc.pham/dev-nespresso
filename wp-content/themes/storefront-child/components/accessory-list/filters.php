<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$f_categories_obj = get_acf_field('accessory_category');
$f_categories = $f_categories_obj && isset($f_categories_obj['choices']) ? $f_categories_obj['choices'] : [];


$f_collections_obj = get_acf_field('collection');
$f_collections = $f_collections_obj && isset($f_collections_obj['choices']) ? $f_collections_obj['choices'] : [];

?>
<form action="#" method="get" id="filters-form-<?php echo rand(0,9999); ?>">

    <div class="filter">
        <div class="title">CATEGORY</div>

            <div class="control">

                <?php $i=1; foreach ( $f_categories as $k => $category ) : ?>

                    <?php if ( $i % 4 == 1 ) : ?>
                        </div>
                        <div class="title"> </div>
                        <div class="control">
                    <?php endif; ?>

                    <label><input type="checkbox" name="category" value="<?= $k ?>"> <span><?= $category ?></span></label>

                <?php $i++; endforeach; ?>

            </div>


    </div>

    <div class="filter">
        <div class="title">COLLECTION</div>
            <div class="control">

                <?php $i=1; foreach ( $f_collections as $k => $collection ) : ?>

                    <?php if ( $i % 4 == 1 ) : ?>
                        </div>
                        <div class="title"> </div>
                        <div class="control">
                    <?php endif; ?>

                    <label>
                        <input type="checkbox" id="control-color<?= $k ?>" name="collection" value="<?= $k ?>"> <span><?= $collection ?></span>
                    </label>

                <?php $i++; endforeach; ?>

            </div>
    </div>

</form>
