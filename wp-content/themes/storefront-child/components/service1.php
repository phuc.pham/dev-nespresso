<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * Template Name: Nespresso Services
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>

<?php get_header(); ?>

<!-- block 1-->
<div class="text-center blog1">
    <h1>NESPRESSO SERVICES</h1>

    <h3>Good afternoon, how can we help you?</h3>

    <p>As a Nespresso Member, you benefit from many personalized services, so that you can enjoy your coffee your way.</p>
</div>


<section id="order" class="main hvrbox" >
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-6 block1" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/ordering.webp');">
                
                <div class="boxtop1">
                    <h1> <i class="icon icon-accessories " ></i>  <br> Ordering <br> <div class="ordertitle">
                        Order your capsules 24/7 from our website
                    </div> </h1>
                   
                    

                </div>
                

                <div class="box" data-toggle="modal" data-target=".bd-example-modal-lg-1-1">
                        Online
                </div>


                <div class="modal fade bd-example-modal-lg-1-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="row">
                                
                                    <div class="col-sm-6">
                                       <img style="height: 350px;"  src="http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/ordering.webp'" >
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="boxtop1">
                                            <h1> <i class="icon icon-accessories " ></i>  <br> Ordering <br> <div class="ordertitle">
                                                Order your capsules 24/7 from our website
                                            </div> </h1>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

            <div class="col-sm-6  text-center" >
                    
                    <h1> Order online</h1>

                   
                    <div class="boxleft" >
                            Receive your coffee, your way
                    </div>
                    <span class="boxleft2">
                       <a href="#"> Order coffee now</a>
                    </span>
            </div>
        </div>
        
    </div>
</section>


<section id="order2" class="main hvrbox" >
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-6 block2" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/delivery.webp');">
            
                <div class="boxtop2">
                    <h1 style="color: #FFF;font-size: 2rem;"> <div class="text-center order2-img"> <img class="text-center" style="width: 100px;" src="https://www.nespresso.vn/wp-content/uploads/2019/02/delivery-navi-229-x-229.jpg"> </div>  <br> Delivery <br>
                    
                    </h1>
                    <div class="order2title">
                    Being delivered has never been so simple. We adapt to your time and to your place to deliver you the best way you deserve...
                    
                    </div>
                </div>

                <div class="box2" data-toggle="modal" data-target=".bd-example-modal-lg-2-1">
                    Delivery
                </div>


                <div class="modal fade bd-example-modal-lg-2-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                             Delivery
                            </div>
                        </div>
                    </div>

            </div>

            <div class="box2_boxleft3 col-sm-6  text-center" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/customer-care.webp');height: 500px;"  >
                        
                
                    <h1 style="color: #FFF;font-size: 2rem;position: absolute;top: 170px;"> Customer Care<br>
                
                    <p class="box2_boxleft2" >
                            By internet, on mobile or by phone, order your capsules 24h/7 with the solution that best suits your needs…
                     </p>


                    <div class="box2left">
                        <ul>
                            <li class="boxlist" data-toggle="modal" data-target=".bd-example-modal-lg-3-1">
                                <div>Machine assistance</div>
                            </li>
                            <li class="boxlist" data-toggle="modal" data-target=".bd-example-modal-lg-3-2">
                                <div>Customer Care</div>
                            </li>
                            <li class="boxlist" data-toggle="modal" data-target=".bd-example-modal-lg-3-3">
                                <div>REPAIR SERVICE</div>
                            </li>
                        </ul>
                    </div>


                    <div class="modal fade bd-example-modal-lg-3-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            Machine assistance
                            </div>
                        </div>
                    </div>

                    <div class="modal fade bd-example-modal-lg-3-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            Customer Care
                            </div>
                        </div>
                    </div>


                    <div class="modal fade bd-example-modal-lg-3-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            REPAIR SERVICE
                            </div>
                        </div>
                    </div>
        
            </div>


        </div>
        
    </div>
</section>

<section id="order3" >
    <div class="container">
        <div class="row text-center">
            <div class="col-sm-6 box2_boxleft4" style="background-image: url('http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/recycling.webp'); height: 500px;background-size: 500px;">
            
                <div class="boxtop3">
                    <h1 style="color: #FFF;font-size: 2rem;padding-top: 120px;"> Recycling 
                    
                    </h1>
                    <div class="order3title">
                        Recycling services so that you can recycle your used capsules daily.
                    </div>
                </div>

                <div class="box3" id="myBtn4" data-toggle="modal" data-target=".bd-example-modal-lg">
                        Recycling
                    
                </div>

                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                            Recycling
                    </div>
                </div>
                </div>

             
        

            </div>

        </div>
        
    </div>
</section>


<script>
jQuery('.block1').hover(function()
{
    jQuery(".box").show();
    // jQuery(".boxtop1").hide();
    jQuery(".box").css("opacity", "1");
    jQuery(".block1").css("opacity", "1");
    
}, function()
{ 
    jQuery(".box").hide();
});

jQuery('.block2').hover(function()
{
    jQuery(".box2").show();
    jQuery(".box2").css("opacity", "1");
    jQuery(".block2").css("opacity", "1");
    
}, function()
{ 
    jQuery(".box2").hide();
});

jQuery('.box2_boxleft3').hover(function()
{
    jQuery(".box2left").show();
    jQuery(".box2left").css("opacity", "1");
    jQuery(".box2_boxleft3").css("opacity", "1");
    
}, function()
{ 
    jQuery(".box2left").hide();
});


jQuery('.box2_boxleft4').hover(function()
{
    jQuery(".box3").show();
    jQuery(".box3").css("opacity", "1");
    jQuery(".box2_boxleft4").css("opacity", "1");
    
}, function()
{ 
    jQuery(".box3").hide();
});





</script>
<style>
/* section#order {
    opacity: 0.7;
    z-index: -1;
} */
.order3title {
    font-size: 1.2rem;
    color: #FFF;
    padding: 0px 30px;
    font-weight: 300;
    line-height: normal;
}


.box2left{
    display: none;
    margin: 1rem;
    padding: 25px 20px;
    color: #000;
    font-size: 1.5rem;
  }
.boxlist{
    padding: 30px 10px;
    font-size: 1.1rem;
    display: block;
    width: 32%;
    float: left;
    color: #fff;
    text-align: center;
    border: 1px #ffffff solid;
    text-transform: uppercase;
    margin: 2px;
    border-radius: 5px;
  }
  .boxlist:hover{
      background:#FFF;
      color: #000;
  }
  .box:hover{
      background:#FFF;
      color: #000;
      border: 1px #000 solid;
  }

p.box2_boxleft2 {
    font-size: 1.2rem;
    color: #fff;
}
p.text_modal2 {
    position: absolute;
    font-size: 1.1rem;
    top: 85px;
    padding: 20px;
    text-transform: lowercase;
}
h3.modal_h3 {
    position: absolute;
    font-size: 1.2rem;
    top: 20px;
}

h4.modal_h4 {
    position: absolute;
    font-size: 1.1rem;
    top: 50px;
}


p.box2_boxleft {
    position: absolute;
    top: 150px;
    left: 35px;
    font-size: 1.2rem;
    float: right;
}
h4.box2_h4 {
    position: absolute;
    top: 90px;
    left: 35px;
    font-size: 1.2rem;
    color: #FFF;
}
h3.box2_h3 {
    color: #ffff;
    position: absolute;
    top: 50px;
    left: 35px;
    font-size: 1.2rem;
}
.ordertitle{
    font-size: 1.2rem;
    color: #FFF;
    padding: 0px 30px;
    font-weight: 300;
    line-height: normal;
}
.order2title{
    font-size: 1.2rem;
    color: #FFF;
    padding: 0px 30px;
    font-weight: 300;
    line-height: normal;
}
.boxtop2 {
    padding-top: 120px;
    height: 350px;
}

.text-center.order2-img {
    position: absolute;
    left: 35%;
    top: 60px;
}
p.text_1 {
    font-size: 1rem;
    float: right;
}
h4.g_h4 {
    float: inherit;
}
h3.g_h3 {
    float: inherit;
    font-size: 1.2rem;
}
.text_1{
    color: #000 !important;
    font-size: 1.3rem;
}

body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal2 {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding: 20px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto; 
  margin: auto;
  color: #000;

}
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding: 20px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto; 
  margin: auto;
  color: #000;

}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
    top: 25%;
    position: absolute;
    left: 15%;
    height: 400px;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.boxleft{
    color: #fff;
    position: absolute;
    font-size: 1rem;
    opacity: 1;
    padding-top: 30px;
    top: 210px;
    left: 25%;
}
.boxleft2{
    color: #f7fb42;
    position: absolute;
    font-size: 1rem;
    opacity: 1;
    padding-top: 30px;
    top: 240px;
    left: 35%;
    text-decoration: underline;
}
.box{
    display: none;
    margin: -5rem 10rem;
    padding: 25px 20px;
    color: #fff;
    font-size: 1.3rem;
    border: 1px #FFF solid;
    border-radius: 5px;
}
.box3{
    display: none;
    margin: 1rem 10rem;
    padding: 25px 20px;
    color: #fff;
    font-size: 1.5rem;
    border: 1px #FFF solid;
    border-radius: 5px;
}
.box3:hover{
    background: #fff;
    color: #000;
}
.box2{
    display: none;
    margin: 1rem 10rem;
    padding: 25px 20px;
    color: #fff;
    font-size: 1.5rem;
    border: 1px #FFF solid;
    border-radius: 5px;
}
.box2:hover {
    background: #fff;
    color: #000;
}
.block1,.block2{
    height: 500px;
    opacity: 1;
}
.blog1{
    display: table;
    position: relative;
    width: 100%;
    height: 100%;
    max-width: 66.25em;
    padding: 0 2em;
    margin: 0 auto;
}
.blog1 h1{
    margin-bottom: 3rem;
    color: #FFF;
    font-size: 3.5rem;
    
}
.blog1 h3{
    margin-bottom: 1.33333em;
    font-size: 1.3rem;
    color: #fff;
}
.blog1 p{
    margin-bottom: 1.33333em;
    font-size: 1rem;
}

#order h1 {
    color: #fff;
    font-size: 2rem;
    padding: 80px 0px;
}
</style>


<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/libs/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/components/services.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php get_footer(); ?>
