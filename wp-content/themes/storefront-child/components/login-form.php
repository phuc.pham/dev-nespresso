<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

global $my_account_url;
?>

<main id="content" class="login-page">
    <?php
    $activeMenu = 1;
    ?>
    <div class="container">
        <div class="mr-bottom-20 row login-page__row-container">
            <div class="col-xs-12 col-md-6 login-page__col no-padding-left md-no-padding-right">
                <div class="account-detail">
                    <header>
                        <h2>Log in</h2>
                    </header>

                       <form action="<?= $my_account_url ?>" method="post" name="login-form" role="form">

                        <?php if ( isset($_GET['login-status']) && $_GET['login-status'] == 'failed' ) : ?>
                            <p><strong class="text-red">Invalid username or password</strong></p>
                            <script>
                                // remove the login-status param on url
                                window.history.pushState(null, null, window.location.pathname);
                            </script>
                        <?php unset($_GET['login-status']); endif; ?>

                        <section class="form">
                            <div class="checkister-wrapper clearfix">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php do_action('nespresso_login_header'); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4" for="email">Email address<span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input title="Holder name" id="username" name="username" placeholder="Email" class="form-control col-sm-12 col-md-9" type="text" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                                                <span class="mobile-label">Email<span class="required">*</span></span>
                                            </div>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4" for="password">Password<span class="required">*</span></label>
                                            <div class="col-sm-8">
                                               <input title="Holder name" id="password" name="password" placeholder="Password *" class="form-control col-sm-12 col-md-9" type="password" />
                                            <span class="mobile-label">Password<span class="required">*</span></span>
                                            </div>
                                        </div>

                                        <?php do_action( 'woocommerce_login_form' ); ?>

                                        <div class="col-sm-8 col-md-offset-4 col-sm-offset-3 login-page__password-forgotten">
                                            <div class="clearfix">
                                                <a title="FORGOT PASSWORD?" class="link-btn uppercase" href="/lost-password"><span class="fa fa-long-arrow-right"></span> FORGOT PASSWORD?</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-md-offset-4 col-sm-offset-3 login-page__remember">
                                            <div class="checkbox">
                                                <label for="remember-me">
                                                    <input value="none" id="remember-me" name="keep-me-informated" type="checkbox">
                                                    Remember me
                                                </label>
                                            </div>
                                        </div>

                                        <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>

                                        <button type="submit" class="btn btn-grey btn-icon-right" title="Login" name="login">Login<i class="icon icon-arrow_left"></i></button>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <?php do_action( 'woocommerce_login_form_end' ); ?>

                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 login-page__col no-padding-right md-no-padding-left">
                <div class="account-detail">
                    <header>
                        <h2>Register new account</h2>
                    </header>
                    <p class="no-margin-bottom">
                        Don't have a Nespresso Vietnam account yet?
                    </p>
                    <div class="login-page__register-info">
                        <ul>
                            <li><i class="fa fa-check-circle"></i>Order your coffee, machine, and accessories online</li>
                            <li><i class="fa fa-check-circle"></i>Register your machine and activate the warranty</li>
                            <li><i class="fa fa-check-circle"></i>Subscribe and learn about our upcominng offers and events</li>
                        </ul>
                    </div>
                    <a class="btn btn-grey btn-icon-right" href="/register" title="Register now"><span>Register now</span> <i class="icon icon-arrow_left"></i></a>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
global $scripts;

$scripts[] = 'js/components/shopping-bag.js';
?>
