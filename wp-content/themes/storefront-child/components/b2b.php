<?php
/**
 * Nespresso Custom Theme developed by Koodi
 * Child theme of Storefront
 *
 * @link https://koodi.ph
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>

<!-- <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/icon.png" rel="icon" sizes="128x128" /> -->

<!-- b2b assets -->
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/assets/bootstrap-3.3.7/css/bootstrap.css" >
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/assets/slick/slick.css" >
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/assets/slick/slick-theme.css" >
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/css/style.css" >
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/css/b2b.css" >

<!-- content -->
<div class="container" id="b2b">
    <div class="row">

        <header class="text-center">
            <div class="container-fluid">
                <div class="row">
                    <a class="" title="nespresso business">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Nespresso-Professional.png" alt="Nespresso Professional" class="img-responsive">
                    </a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <h1 class="title"><em>Nespresso</em> for your Business</h1>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Home-Cover.jpg" alt="home cover" class="img-full img-b2b-banner">
                    <nav class="main-menu">
                        <ul>
                            <li><a href="#coffee" class="on-scroll">Coffee</a></li>
                            <li><a href="#machine" class="on-scroll">Machines</a></li>
                            <li><a href="#services" class="on-scroll">Services</a></li>
                            <li><a href="#business-channels" class="on-scroll">Business Channels</a></li>
                            <li><a href="#contact-us" class="on-scroll">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <section id="">
            <div class="container">
                <div class="row">
                    <article class="post-panel">
                        <div class="post-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Coffee.png" alt="coffee">
                        </div>
                        <div class="post-description">
                            <h2 class="title">THE FINEST QUALITY COFFEE</h2>
                            <p>
                                To guarantee the best beans, <em>Nespresso</em> only sources exceptional coffees that are rigorously selected and tested to satisfy the most demanding palate. Only 1 to 2% of the world’s coffee production meets the high standards of quality set by <em>Nespresso</em>.
                            </p>
                            <p>
                                The <em>Nespresso</em> high quality coffee range offers a huge variety of profiles and aromas – each a product of a complex process of selection, assembly and roasting. Our high quality coffee are categorized by cup size and level of intensity. Customers, guests and employees can enjoy an excellent cup of coffee to suit their taste, whatever time of the day.
                            </p>
                            <p>
                                We offer different ranges of coffees to be enjoyed as Ristretto, Espresso, Lungo, Decaffeinated and Flavored.
                            </p>
                        </div>
                    </article>
                </div>
            </div>
        </section>
        <section id="coffee">
            <div class="container main">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="title text-center">13 HIGH QUALITY COFFEE TO SUIT EVERY TASTE, ANY TIME OF THE DAY</h2>
                        </div>
                    </div>
                    <div id="slider-grandcru">
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Ristretto-Intenso.png" alt="ristretto intenso" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Ristretto Intenso</h3>
                                    <p>
                                        A daring blend of South and Central American Robustas and Arabica, <em>Ristretto Intenso</em> is a full coffee of exceptional intensity with notes of pepper and creamy texture.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-12.png" alt=""> 12</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/25ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Exceptionally intense and syrupy</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Ristretto-Origin-India.png" alt="ristretto origin india" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Ristretto Origina India</h3>
                                    <p>
                                        <em>Ristretto Origin</em> is the marriage of the finest Arabicas with a hint of Robusta from Southern India. It is a full-bodied coffee, which has powerful character and notes of spices.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-11.png" alt=""> 11</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/25ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Intense and spicy</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Untitled-2.png" alt="ristretto" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Ristretto</h3>
                                    <p>
                                        Pure and dark-roasted South and Central American Arabicas make <em>Ristretto</em> a coffee with a dense body and distinct cocoa notes.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-9.png" alt=""> 9</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/25ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Full bodied and persistent</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Espresso-Forte.png" alt="espresso forte" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Espresso Forte</h3>
                                    <p>
                                        Made exclusively from South and Central American Arabicas, the complex aroma of this intensely roasted espresso is a balance of strong roasted and fruity notes.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-7.png" alt=""> 7</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/40ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Round and balanced</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Espresso-Leggero.png" alt="espresso leggero" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Espresso Leggero</h3>
                                    <p>
                                        A delicious blend of South American Arabicas and Robusta, Espresso Leggero adds smooth cocoa and cereal notes to a well-balanced body.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-6.png" alt=""> 6</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/40ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Light and refreshing</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Espresso-Origin-Brazil.png" alt="espresso origin brazil" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Espresso Origin Brazil</h3>
                                    <p>
                                        A pure Arabica coffee, Espresso Origin Brazil is a delicate blend with a smooth texture and an elegantly mild and sweet flavor enhanced by a note of lightly toasted grain.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-4.png" alt=""> 4</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/40ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Sweet and satiny smooth</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Lungo-Guatemala.png" alt="lungo origin guatemala" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Lungo Origin Guatemala</h3>
                                    <p>
                                        A blend of Arabica and washed Gourmet Robusta coffee, Lungo Origin Guatemala is a smooth and balanced blend with intense dry and malty cereal notes underlining its bold character.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-6.png" alt=""> 6</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/110ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Bold and silky</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Lungo-Forte.png" alt="lungo forte" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Lungo Forte</h3>
                                    <p>
                                        A complex blend of South and Central American Arabicas, Lungo Forte holds intense roasted notes with a subtle hint of fruit.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-4.png" alt=""> 4</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/110ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Elegant and roasted</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Lungo-Leggero.png" alt="lungo leggero" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Lungo Leggero</h3>
                                    <p>
                                        A delicate blend of lightly roasted East African, South and Central American Arabicas, Lungo Leggero is an aromatic coffee with mild notes of jasmine.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-4.png" alt=""> 4</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/110ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Flowery and refreshing</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Espresso-Decaf.png" alt="espresso decaffeinato" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Espresso Decaffeinato</h3>
                                    <p>
                                        Dark roasted South American Arabicas with a touch of Robusta bring out the subtle cocoa and roasted cereal notes of this full-bodied decaffeinated espresso.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-7.png" alt=""> 7</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/40ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Dense and powerful</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Lungo-Decaf.png" alt="lungo decaffeinato" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Lungo Decaffeinato</h3>
                                    <p>
                                        A blend of decaffeinated South American Arabicas and Robusta, this coffee reveals flavours of red fruit balanced with sweet cereal notes.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-7.png" alt=""> 4</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/110ml.png" alt=""></p>
                                    <!-- <p class="aroma">Aromatic Profile: Velvety and aromatic</p> -->
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Espresso-Caramel.png" alt="espresso caramel" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Espresso Caramel</h3>
                                    <p>
                                        The sweetness of the caramel flavor mellows Espresso Forte roasted notes giving birth to a pleasant coffee reminiscent of the browning of sugar.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-7.png" alt=""> 7</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/40ml.png" alt=""></p>
                                </figcaption>
                            </figure>
                        </div>
                        <div>
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Espresso-Vanillo.png" alt="espresso vanillo" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Espresso Vanillo</h3>
                                    <p>
                                        Discover a silky flavored coffee thanks to the full and slightly caramelized aroma of vanilla combined with the complex Espresso Forte profile.
                                    </p>
                                    <p class="intensity">Intensity: <img class="coffee-intensity-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/intensity-7.png" alt=""> 7</p>
                                    <p class="size">Size: <img class="coffee-size-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/40ml.png" alt=""></p>
                                </figcaption>
                            </figure>
                        </div>
                    </div><!-- end of #slider-grandcru -->
                </div>
            </div>
        </section>
        <section id="">
            <div class="container">
                <div class="row">
                    <article class="post-panel">
                        <div class="post-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Machine.jpg" alt="machine">
                        </div>
                        <div class="post-description">
                            <h2 class="title">Reliable and Efficient Solutions</h2>
                            <p>
                                Elegant, innovative design combined with forward-thinking technology: <em>Nespresso</em> professional coffee machines are a natural fit for your company.
                            </p>
                            <p>
                                <em>Nespresso</em> machines are equipped with an exclusive extraction system. Ristrettos, Espressos, Lungos or any of our delicious milk-based coffee recipes can be prepared with just a touch of a button. The ideal pressure and temperature in the extraction process generates an incomparable crema (the golden foam that sits on top of the coffee) which seals in all the coffee aromas. All machines are equipped with energy saving features that cuts down electrical consumption.
                            </p>
                        </div>
                    </article>
                </div>
            </div>
        </section>
        <section id="machine">
            <div class="container main">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="title text-center">Our Complete Machine Range Designed To Meet Your Professional Needs</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a href="#machine-modal" class="btn-modal"
                            data-img-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Zenius.jpg"
                            data-poster-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/poster-zenius.jpg"
                            data-vid-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/videos/Nespresso-Zenius.mp4"
                            data-video="Zenius" data-description="Ideal for any size of business where quality and simplicity are both important. Fast and efficient, exceptional coffee and hot water are prepared quickly, at the touch of a button.">
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Zenius.jpg" alt="zenius" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Zenius</h3>
                                    <p>
                                        Ideal for any size of business where quality and simplicity are both important. Fast and efficient, exceptional coffee and hot water are prepared quickly, at the touch of a button.
                                    </p>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#machine-modal" class="btn-modal"
                            data-img-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Gemini.jpg"
                            data-poster-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/poster-gemini.jpg"
                            data-vid-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/videos/Nespresso-Gemini.mp4"
                            data-video="Gemini" data-description="Dedicated to professional use, Gemini range has combined advanced technology and forward-thinking design. Double head extraction system enables simultaneous coffee preparations. Your customers, clients and employees can enjoy a superb high quality coffee in a matter of seconds.">
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Gemini.jpg" alt="gemini" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Gemini</h3>
                                    <p>
                                        Dedicated to professional use, Gemini range has combined advanced technology and forward-thinking design. Double head extraction system enables simultaneous coffee preparations. Your customers, clients and employees can enjoy a superb Grand Cru coffee in a matter of seconds.
                                    </p>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#machine-modal" class="btn-modal"
                            data-img-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Aguila.jpg"
                            data-poster-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/poster-aguila.jpg"
                            data-vid-src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/videos/Nespresso-Aguila.mp4"
                            data-video="Aguila" data-description="Make a little space for a great professional. With Aguila 220, find high performance in a compact machine and unmatched choice of one-touch recipes, cup after cup with 2 extraction heads working simultaneously.">
                            <figure class="box box--centered">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Aguila.jpg" alt="auila" class="img-full box-img">
                                <figcaption>
                                    <h3 class="heading">Aguila</h3>
                                    <p style="width: 95%;">
                                        Make a little space for a great professional. With Aguila 220, find high performance in a compact machine and unmatched choice of one-touch recipes, Grand Crus after Grand Cru with 2 extraction heads working simultaneously.
                                    </p>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
            </div>
            <div id="machine-modal" class="modal modal-inverse fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header clearfix">
                            <h5 class="modal-title">Machine Range</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h3 class="heading">Our Complete Machine Range Designed To Meet Your Professional Needs</h3>
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Zenius.jpg" alt="zenius" class="img-full modal-img">
                                </div>
                                <div class="col-md-7">
                                    <figure>
                                        <h3 class="heading heading--left"><span class="how-title"></span></h3>
                                        <p id="video-description"></p>
                                        <figcaption>
                                            <h3 class="heading heading--left">How to use <span class="how-title"></span></h3>
                                            <video width="320" height="240" controls poster="">
                                                <source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end #machine-modal -->
        </section>
        <section id="services" class="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="title text-center">Dedicated Professional Services</h2>
                        <p class="text-center">
                            To respond to your specific business needs, <em>Nespresso</em> offers you a complete range of dedicated services. Whether you are requesting for supplies, maintenance, or technical expertise, <em>Nespresso</em> is your partner of choice.
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <figure class="box">
                            <h3 class="heading">Complete Machine Care</h3>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Machine-Care.png" alt="machine care" class="box-img img-full">
                            <figcaption>
                                <ul>
                                    <li>Thorough annual service and machine assessment by our dedicated team of <em>Nespresso</em> technicians</li>
                                    <li>Guaranteed service within 2-3 working days</li>
                                    <li>Substitute machine during servicing</li>
                                </ul>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-md-4">
                        <figure class="box">
                            <h3 class="heading">Next Working Day Deliver</h3>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Next-Working-Day-Delivery-2.png" alt="next working day delivery" class="box-img img-full">
                            <figcaption>
                                <ul>
                                    <li>Simply place your order via phone or email through our dedicated <em>Nespresso</em> Account Specialists and your capsules will be delivered directly to your company</li>
                                    <li>Automatic and recurring shipment can be requested</li>
                                </ul>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-md-4">
                        <figure class="box">
                            <h3 class="heading">Service Guarantee</h3>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Service-Guarantee.png" alt="service guarantee" class="box-img img-full">
                            <figcaption>
                                <ul>
                                    <li>Dedicated Customer Care Center</li>
                                    <li>Free installation and demonstration of new machines</li>
                                    <li>Free coffee recipe training conducted by <em>Nespresso</em> Coffee Ambassador</li>
                                    <li>Merchandising Support</li>
                                </ul>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
        <section id="business-channels" class="main text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="title">Tailored Coffee Solutions To Fit Your Business Needs</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <figure class="box box--centered">
                            <h3 class="heading">Restaurant and Cafe</h3>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Restaurant.png" alt="restaurant and cafe" class="img-full box-img">
                            <figcaption>
                                <p>
                                    Delight the discerning palates of your customers. All <em>Nespresso</em> Grand Crus deliver consistent and  perfect coffee, cup after cup.
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-md-4">
                        <figure class="box box--centered">
                            <h3 class="heading">Hotel</h3>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Hotel.png" alt="hotel" class="img-full box-img">
                            <figcaption>
                                <p>
                                    To choose <em>Nespresso</em> coffee capsules and coffee machines for your hotel is a symbol of your passion for excellence and commitment to your customers.
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-md-4">
                        <figure class="box box--centered">
                            <h3 class="heading">Office</h3>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Office.png" alt="office" class="img-full box-img">
                            <figcaption>
                                <p>
                                    Offering a great quality cup of coffee shows you care about small detail and it creates a positive impression of your workplace.
                                </p>
                                <p>
                                    It can open a conversation, close a deal and make employees and customers feel special.
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
        <section id="contact-us" class="main text-center">
            <div class="container">
                <h2 class="title">Contact us to learn more about  <em>Nespresso</em> Professional.</h2>
                 <h4>
                    Call us at 1900 633 474 or email us at 
                </h4>
                <h4>
                    <a href="mailto:pro@nespresso.vn">pro@nespresso.vn</a>
                </h4>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/b2b/img/Contact-us.jpg" alt="contact us" class="img-full">
            </div>
        </section>


    </div>
</div><!-- .container -->
<!-- /content -->