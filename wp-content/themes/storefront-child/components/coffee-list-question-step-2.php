<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if(!isset($_SESSION['sv_customer_id']) || !$_SESSION['sv_customer_id'])
{
    header('location: /coffee-list-question');
    exit;
}
$customer_id = $_SESSION['sv_customer_id'];
global $wpdb;
$table = $wpdb->prefix.'coffee_customers';
$cus = $wpdb->get_row($wpdb->prepare( "select * from $table where id = %d" ,$customer_id));
if(!$cus)
{
    header('location: /coffee-list-question');
    exit;
}
$tableq = $wpdb->prefix.'coffee_questions';
$tablea = $wpdb->prefix.'coffee_answers';
$tableqa = $wpdb->prefix.'coffee_question_answer';
$questions = $wpdb->get_results("select * from $tableq");
//dd($questions);
?>

<!-- content -->

<main id="content" class="fullPage fullPage__plp coffee-list">
    <!-- end banner -->
    <div class="container">
        <div class="row">
                <div class="productlist-main clearfix">
                    <div class="row">

                        <form id="regForm" name="coffee_step_2" action="/nespresso_page_coffee_list_question_result/" method="POST">
                            <div class="panel-question" id="panel-question">
                                <?php foreach($questions as $q){ 
                                $answers  =  $wpdb->get_results("select a.*,qa.id qaid from $tablea a join $tableqa qa on a.id=qa.id_answer where id_question = {$q->id} ");
                                //dd($answers) ;
                                ?>
                                <div class="tab">
                                    <h1 style="color: #a95b5b;"><?=$q->name?></h1>
                                    <div class="row">
                                        <?php foreach($answers as $as){ ?>
                                        <div class="col-xs-6">
                                            <div class="answer">
                                                <label class="custom-control-label" for="answer_<?=$as->id?>" >                                                    
                                                    <?php if($as->image){?><img src="<?=$as->image?>" alt="<?=$as->name?>" class="img-fluid"><?php } ?>
                                                    <span class="text-question"><?=$as->name?></span>
                                                    <input type="radio" class="custom-control-input" value="<?=$as->id.':'.$as->qaid?>" name="questions[<?=$q->id?>]" id="answer_<?=$as->id?>" onclick="nextPrev(1)">
                                                </label>
                                            
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <!-- <div class="col-md-6">
                                            <p>
                                                <label class="custom-control-label"  for="question01_answer02" >
                                                    <img src="http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/CÂU-1-ảnh-2.png" alt="#" class="img-fluid">
                                                    <span class="text-question">A moment of pleasure</span>
                                                    <input type="radio" class="custom-control-input"  value="question01_answer02" name="question01" id="question01_answer02" onclick="nextPrev(1)" >
                                                    
                                                </label>
                                            </p>
                                        </div> -->
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div style="text-align:center;margin-top:40px;">
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                                <span class="step"></span>
                            </div>

                        </form>

                        <script>
                            var currentTab = 0; // Current tab is set to be the first tab (0)
                            showTab(currentTab); // Display the current tab

                            function showTab(n) {
                            // This function will display the specified tab of the form...
                            var x = document.getElementsByClassName("tab");
                            x[n].style.display = "block";
                            //... and fix the Previous/Next buttons:
                            if (n == 0) {
                                document.getElementById("prevBtn").style.display = "none";
                            } else {
                                document.getElementById("prevBtn").style.display = "inline";
                            }
                            if (n == (x.length - 1)) {
                                document.getElementById("nextBtn").innerHTML = "Submit";
                            } else {
                                document.getElementById("nextBtn").innerHTML = "Next";
                            }
                            //... and run a function that will display the correct step indicator:
                            fixStepIndicator(n)
                            }

                            function nextPrev(n) {
                            // This function will figure out which tab to display
                            var x = document.getElementsByClassName("tab");
                            // Exit the function if any field in the current tab is invalid:
                            if (n == 1 && !validateForm()) return false;
                            // Hide the current tab:
                            x[currentTab].style.display = "none";
                            // Increase or decrease the current tab by 1:
                            currentTab = currentTab + n;
                            // if you have reached the end of the form...
                            if (currentTab >= x.length) {
                                // ... the form gets submitted:
                                document.getElementById("regForm").submit();
                                return false;
                            }
                            // Otherwise, display the correct tab:
                            showTab(currentTab);
                            }

                            function validateForm() {
                            // This function deals with validation of the form fields
                            var x, y, i, valid = true;
                            x = document.getElementsByClassName("tab");
                            y = x[currentTab].getElementsByTagName("input");
                            // A loop that checks every input field in the current tab:
                            for (i = 0; i < y.length; i++) {
                                // If a field is empty...
                                if (y[i].value == "") {
                                // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                                }
                            }
                            // If the valid status is true, mark the step as finished and valid:
                            if (valid) {
                                document.getElementsByClassName("step")[currentTab].className += " finish";
                            }
                            return valid; // return the valid status
                            }

                            function fixStepIndicator(n) {
                            // This function removes the "active" class of all steps...
                            var i, x = document.getElementsByClassName("step");
                            for (i = 0; i < x.length; i++) {
                                x[i].className = x[i].className.replace(" active", "");
                            }
                            //... and adds the "active" class on the current step:
                            x[n].className += " active";
                            }


                        </script>

                        <style>
                            input#nextBtn {
                                display: none;
                            }
                            .text-question{
                                cursor: pointer;
                                color: #FFF;
                                font-size: 14px;
                                text-align: center !important;
                                background: #a95b5b;
                                padding: 20px 0px;
                                margin: 5px;
                                display: inline-block;
                                width: -webkit-fill-available;
                                height: 70px;
                            }
                            .answer,.answer label{
                                cursor:pointer;
                            }
                            .answer input[type="radio"]
                            {
                                display:none;
                            }
                            #regForm {
                            /* background-color: #ffffff; */
                            /* margin: 100px auto; */
                            font-family: Raleway;
                            padding: 40px;
                            /* width: 70%; */
                            min-width: 300px;
                            }

                            h1 {
                            text-align: center;  
                            }

                            input {
                            padding: 10px;
                            width: 100%;
                            font-size: 17px;
                            font-family: Raleway;
                            border: 1px solid #aaaaaa;
                            }

                            /* Mark input boxes that gets an error on validation: */
                            input.invalid {
                            background-color: #ffdddd;
                            }

                            /* Hide all steps by default: */
                            .tab {
                            display: none;
                            }

                            button {
                            background-color: #4CAF50;
                            color: #ffffff;
                            border: none;
                            padding: 10px 20px;
                            font-size: 17px;
                            font-family: Raleway;
                            cursor: pointer;
                            }

                            button:hover {
                            opacity: 0.8;
                            }

                            #prevBtn {
                            background-color: #bbbbbb;
                            }

                            /* Make circles that indicate the steps of the form: */
                            .step {
                                height: 15px;
                                width: 15px;
                                margin: 0 2px;
                                background-color: #bbbbbb;
                                border: none;  
                                border-radius: 50%;
                                display: inline-block;
                                opacity: 0.5;
                            }

                            .step.active {
                                opacity: 1;
                            }

                            /* Mark the steps that are finished and valid: */
                            .step.finish {
                                background-color: #4CAF50;
                            }
                    
                            img.img-fluid {
                                width: 450px;
                                height: 190px;
                            }
                            #content.fullPage__plp {
                                background: url(http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/CÂU-2-ảnh-1.jpg) no-repeat;
                                background-size: cover;
                                background-position: center center;
                            }
                        </style>
                        
                    </div>

            </div>
        </div>
    </div>

</main><!-- #content -->

<!-- /content -->