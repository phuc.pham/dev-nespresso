<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// get order column headers, formatted from woocommerces
$my_orders_columns = apply_filters( 'woocommerce_my_account_my_orders_columns', array(
	'order-date'    => __( 'Date', 'woocommerce' ),
	'order-source'  => __( 'Source', 'woocommerce' ),
	'order-number'  => __( 'Order ID', 'woocommerce' ),
	'order-status'  => __( 'Status', 'woocommerce' ),
	'order-total'   => __( 'Amount', 'woocommerce' ),
	'order-actions' => '&nbsp;',
	'order-reorder' => '&nbsp;',
) );

// get all orders of the current user
// maybe also filter the status of the orders displayed? in the future
$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
	'numberposts' => -1,
	'meta_key'    => '_customer_user',
	'meta_value'  => get_current_user_id(),
	'post_type'   => wc_get_order_types( 'view-orders' ),
	'post_status' => array_keys( wc_get_order_statuses() ),
	'order' => 'DESC'
) ) );
global $wp;

// get the order id, if present on url
$order_id = isset($wp->query_vars['view-order']) ? $wp->query_vars['view-order'] : 0;
// get the list of ordered products data and meta data by order_id
$ordered_products = get_ordered_products( $order_id );
$current_order = [];
$current_order_data = [];
if ( !empty($ordered_products) ) {
	$current_order = new WC_Order( $order_id ); // get the current order data
	$current_order_data = $current_order->get_data();
}


// current user data and metadata
$user = nespresso_get_user_data();

$print_current_order_invoice_url = '';

// get the current users order print invoice url
// ref: https://github.com/wcvendors/wcvendors/issues/196
if (class_exists('WooCommerce_PDF_Invoices') && ( $current_order && !empty($current_order) ) ) :

    $listing_actions = array(
        'invoice'       => array (
            'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=generate_wpo_wcpdf&template_type=invoice&order_ids=' . $order_id ), 'generate_wpo_wcpdf' ),
            'img'       => WooCommerce_PDF_Invoices::$plugin_url . 'images/invoice.png',
            'alt'       => __( 'PDF Invoice', 'wpo_wcpdf' ),
        ),
        'packing-slip'  => array (
            'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=generate_wpo_wcpdf&template_type=packing-slip&order_ids=' . $order_id ), 'generate_wpo_wcpdf' ),
            'img'       => WooCommerce_PDF_Invoices::$plugin_url . 'images/packing-slip.png',
            'alt'       => __( 'PDF Packing Slip', 'wpo_wcpdf' ),
        ),
    );

    $listing_actions = apply_filters( 'wpo_wcpdf_listing_actions', $listing_actions, $order );
    $invoice_listing = isset( $listing_actions['invoice'] ) ? $listing_actions['invoice'] : null;

    $print_current_order_invoice_url = $invoice_listing && isset($invoice_listing['url']) ? $invoice_listing['url'] : '';

endif;
?>

<?php get_header(); ?>

<main id="content">

	<div class="container">
	    <div class="row">

	        <!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <div class="pd-right-0 col-md-9 col-xs-12">

		        <div class="col-xs-12 account-detail my-order-history" id="my-order-history">

			        <?php if ( !$customer_orders || count($customer_orders) < 1 ) : ?>
			        	<!-- empty order history -->
			        	<div class="row">
			                <div class="col-xs-12 text-center">
			                    <p><strong>You did not order anything yet.</strong></p>
			                    <p>You can place your first order using the link below.</p>
			                    <div class="my-easyorders__link">
			                        <a class="btn btn-icon-right btn-primary" title="SHOP NOW" href="/coffee-list"><i class="fa fa-angle-right"></i>SHOP NOW</a>
			                    </div>
			                </div>
			            </div>
		        	<?php else : ?>

			            <div class="my-order-history-list-container clearfix">
				            <header>
				                <h2>My Order History</h2>
				            </header>

				            <table class="table table-responsive table-striped my-order-history-list">
								<thead>
									<tr>
							            <?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
											<th class="column-header text-center <?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
										<?php endforeach; ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $customer_orders as $customer_order ) :
										$order      = wc_get_order( $customer_order );
										$item_count = $order->get_item_count();
									?>
										<tr class="order">
											<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
												<?php if ( 'order-reorder' === $column_id  && 'completed' ===  $order->get_status()): ?>
												<td class="text-center order-actions <?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
												<?php else: ?>
												<td class="text-center  <?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">

												<?php endif ?>
													<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
														<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

													<?php elseif ( 'order-number' === $column_id ) : ?>
														<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
															<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?>
														</a>

													<?php elseif ( 'order-date' === $column_id ) : ?>
														<time class="large_view" datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>
														<time class="small_view" datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created(), 'm/d/Y' ) ); ?></time>

													<?php elseif ( 'order-status' === $column_id ) : ?>
														<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

													<?php elseif ( 'order-total' === $column_id ) : ?>
														<span class="price"><?= $order->get_formatted_order_total() ?></span>

													<?php elseif ( 'order-actions' === $column_id ) : ?>
														<?php
															$actions = array(
																'pay'    => array(
																	'url'  => $order->get_checkout_payment_url(),
																	'name' => __( 'Pay', 'woocommerce' ),
																),
																'view'   => array(
																	'url'  => $order->get_view_order_url(),
																	'name' => __( 'View', 'woocommerce' ),
																),
																'cancel' => array(
																	'url'  => $order->get_cancel_order_url( wc_get_page_permalink( 'myaccount' ) ),
																	'name' => __( 'Cancel', 'woocommerce' ),
																),
															);

															if ( ! $order->needs_payment() ) {
																unset( $actions['pay'] );
															}

															if ( ! in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ) ) ) {
																unset( $actions['cancel'] );
															}

															if ( $actions = apply_filters( 'woocommerce_my_account_my_orders_actions', $actions, $order ) ) {
																foreach ( $actions as $key => $action ) {
																	if ( sanitize_html_class( $key ) == 'view' ) :
																		 echo '<a href="' . esc_url( $action['url'] ) . '" class="large_view button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
																		 echo '<a href="' . esc_url( $action['url'] ) . '" class="small_view small_view_button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
																	endif;
																}
															}
														?>
													<?php elseif ( 'order-reorder' === $column_id && 'completed' ===  $order->get_status()) : ?>
														<a class="button view large_reorder" id="btn-reorder-co" data-id="<?= $customer_order->ID ?>" data-val="<?= check_last_order() ? 'true':'false' ?>"  >Re-order</a>
													<?php elseif ( 'order-source' === $column_id ) : ?>
														Internet

													<?php endif; ?>
												</td>
											<?php endforeach; ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>

			                <div class="my-order-mobile clearfix hide">
			                    <div class="my-order-mobile__block">
			                        <div class="my-order-mobile__order">
			                            <dl class="col-xs-4">
			                                <dt>Order date</dt>
			                                <dd>13/01/2016</dd>
			                            </dl>
			                            <dl class="col-xs-3">
			                                <dt>Status</dt>
			                                <dd>Pending</dd>
			                            </dl>
			                            <div class="col-xs-5">
			                                <dl class="pull-right">
			                                    <dt>Amount</dt>
			                                    <dd><span class="price">$ 263.60</span></dd>
			                                </dl>
			                            </div>
			                        </div>
			                        <div class="my-order-mobile__info">
			                            <div class="col-xs-7 my-order-mobile__info-order">
			                                <span class="my-order-mobile__info-line">Source: Internet</span>
			                                <span class="my-order-mobile__info-line">Delivery mode: Standard</span>
			                                <span class="my-order-mobile__info-line">Order ID: Pending</span>
			                            </div>
			                            <div class="col-xs-5 my-order-mobile__info-btn">
			                                <button class="btn btn-primary pull-right btn-open-detail">More detail</button>
			                                <!-- <button class="btn btn-green pull-right">Re-order</button> -->
			                            </div>
			                        </div>
			                    </div>
			                </div>

			                <a href="#" class="link link--right pull-right more-order" title="View more orders">
			                    <span>See more orders</span>
			                    <span>See less orders</span>
			                </a>
			            </div>
			            <div class="my-order-mobile-detail">
			                <div class="my-order-mobile-detail__header clearfix">
			                    <a class="btn btn-back btn-primary btn-close-detail btn-block btn-icon" href="#"><i class="icon-arrow_right"></i> All orders</a>
			                    <!-- <button class="btn btn-green pull-right">Re-order</button> -->
			                </div>
			                <div class="my-order-mobile-detail__dynamic"></div>
			            </div>

			            <!-- <section class="my-order-history-detail" id="my-order-history-fake-ajax"> -->
			            <?php if ( $ordered_products && !empty($ordered_products) ) : ?>
				            <section class="my-order-history-detail" data-section="order-details" id="order-details">
				                <header>
				                	<h3>Order #<?= $current_order_data['id'] ?></h3>
				                    <div class="div__btn">
				                        <!-- <button class="btn btn-green hidden-xs hidden-sm">Re-order</button> -->
				                    </div>
				                    <div>
				                    	<br>
				                    	<h4>Order details</h3>
				                    	<h6>Date: <?= esc_html( wc_format_datetime( $current_order->get_date_created() ) ) ?></h6>
				                    	<h6>Order Status: <?= strtoupper($current_order->get_status())?> </h6>
				                    </div>
				                </header>
				                <div class="row">
				                    <div class="sbag-list">
				                        <div class="sbag-list-content">

		                        			<?php foreach ( $ordered_products as $product_type => $products ) : ?>
		                        			    <div class="sbag-list-title">
					                                <?= $product_type ?>
					                                <sup>(<span class="sbag-nb-capsules"><?= count($products) ?></span>)</sup>
					                                <div class="sbag-list-title-col">
					                                	<span class="sbag-head-unit-price">Unit Price</span>
					                                	<span class="sbag-head-qty">Quantity</span>
					                                	<span class="sbag-head-total">Total</span>
				                                	</div>
					                            </div>
					                            <div class="sbag-capsules">

					                            	<?php foreach ( $products as $product_id => $product ) : ?>
						                                <div class="sbag-item">
						                                    <div class="sbag-name">
						                                        <img src="<?= $product['image'][0] ?>" alt="<?= $product['name'] ?>">
						                                        <?= $product['name'] ?>
						                                        <span class="hidden-sm hidden-md show-xs sb-price">
						                                        	<?= wc_price($product['price']) ?>
					                                        	</span>
						                                    </div>
						                                    <div class="sbag-unit-price">
						                                       <?= wc_price($product['price']) ?>
					                                       	</div>
						                                    <div class="sbag-multi">x</div>
						                                    <div class="sbag-qty">
						                                        <span class="qty"><?= $product['quantity'] ?></span>
						                                    </div>
						                                    <div class="sbag-price">
						                                        <?= wc_price($product['total']) ?>
						                                    </div>
						                                </div>
					                                <?php endforeach; ?>

					                            </div>
				                            <?php endforeach; ?>

		                                </div>
				                        <div class="sbag-resume-content">
				                            <div class="sbag-resume-head">
				                                <span class="sbag-resume-head-top">Total: </span>
				                                <ul>
				                                	<?php foreach ( $ordered_products as $product_type => $products ) : ?>
					                                    <li><?= $product_type ?> (<span><?= count($products) ?></span>)</li>
					                                <?php endforeach; ?>
				                                </ul>
				                            </div>

				                            <div class="sbag-resume-item sbag-total">
				                                <div class="sbag-total-key">Subtotal</div>
				                                <div class="sbag-total-value sbag-padding-cross"><span class="sbag-subtotal-price-int"><?= $current_order->get_subtotal_to_display(true) ?></span></div>
				                            </div>
				                            <div class="sbag-resume-item">
				                                <div class="sbag-total-key">Shipping cost</div>
				                                <div class="sbag-total-value sbag-padding-cross"><?= $current_order->get_shipping_to_display() ?></div>
				                            </div>
				                            <div class="sbag-resume-item">
				                                <div class="sbag-total-key">Vat (incl.)</div>
				                                <?php
				                                	$tax_totals = $current_order->get_tax_totals();
				                                	if ( !empty($tax_totals) ) :
				                                		foreach ( $tax_totals as $tax) :
			                                				$vat = get_woocommerce_currency_symbol() . ' 0';
				                                			// do something
				                                		endforeach;
				                                	else :
				                                		$vat = get_woocommerce_currency_symbol() . ' 0';
			                                		endif;
				                                ?>
				                                <div class="sbag-total-value sbag-padding-cross"><span class="sbag-val-price-int"><?= $vat ?></span></div>
				                            </div>
				                        </div>

				                        <div class="sbag-footer">
				                            <div class="sbag-total-item">
				                                <div class="sbag-total-title">Total</div>
				                                <div class="sbag-total-price sbag-padding-cross"><span class="sbag-total-price-int"><?= $current_order->get_formatted_order_total() ?></span></div>
				                            </div>
				                        </div>

				                    </div>
				                </div>
				                <div class="row">
				                    <div class="col-xs-12">
				                        <p class="my-order-history-detail__order-type">You have made your order via Internet</p>
				                    </div>
				                    <div class="col-address col-xs-12 col-sm-6">
				                        <p>
				                            <strong>Delivered to:</strong>
				                           	<?= $user->title ?>
				                           	<?= $current_order_data['shipping']['first_name'] ?>
				                           	<?= $current_order_data['shipping']['last_name'] ?>
				                           	<br>
				                            <?= $current_order_data['shipping']['address_1'] ?>
				                            <br>
				                            <?= $current_order_data['shipping']['address_2'] ?>
				                            <br>
				                            <?= $current_order_data['shipping']['postcode'] ?>
				                            <?= $current_order_data['shipping']['city'] ?>
				                            <br>
				                            <?= $current_order_data['shipping']['country'] ?>
				                        </p>
				                        <span class="col-address__summary"><i class="icon-Delivery_off"></i> Delivery mode: <strong><?= ucfirst($user->shipping_mode) ?></strong></span>
				                        <!-- <a href="#" class="link link--right" title="Order tracking #1">Order tracking #1</a> -->
				                    </div>
				                    <div class="col-address col-xs-12 col-sm-6">
				                        <p>
				                            <strong>Invoiced to:</strong>
				                            <?= $user->title ?>
				                           	<?= $current_order_data['billing']['first_name'] ?>
				                           	<?= $current_order_data['billing']['last_name'] ?>
				                           	<br>
				                            <?= $current_order_data['billing']['address_1'] ?>
				                            <br>
				                            <?= $current_order_data['billing']['address_2'] ?>
				                            <br>
				                            <?= $current_order_data['billing']['postcode'] ?>
				                            <?= $current_order_data['billing']['city'] ?>
				                            <br>
				                            <?= $current_order_data['billing']['country'] ?>
				                        </p>
				                        <span class="col-address__summary"><i class="icon-invoice-off"></i> <strong>Invoice</strong></span>
				                    </div>
				                </div>

				                <div class="sbag-list">
				                    <div class="row">
				                        <div class="sbag-reorder">
				                        	<?php if ( $print_current_order_invoice_url ) : ?>
					                        	<a href="<?= $print_current_order_invoice_url ?>" target="_blank" class="btn btn-primary btn-print btn-noWidth">Print</a>
				                        	<?php endif; ?>

					                        <button class="btn pull-right btn-green" id="btn-reorder-co" data-id="<?= $order_id ?>">Re-order</button>
					                        <!-- <button class="btn pull-right  btn-green">Make this order recurrent</button> -->
				                        </div>
				                    </div>
				                </div>
				            </section>
			            <?php endif; ?>
		        	<?php endif; ?>

	        	</div><!-- .my-order-history -->

        	</div><!-- .col-md-9 -->

        </div><!-- .row -->
    </div><!-- .container -->
</main><!-- .content -->

<!-- /content -->

<?php
	global $scripts;
	$scripts[] = 'js/components/my-order-history.js';
?>

<!-- get_footer -->
<?php get_footer(); ?>

