<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $woocommerce;
$user = nespresso_get_user_data();
$states = nespresso_get_states();
?>

<div class="col-xs-12  account-detail" id="container-my-address-list">

    <header>
        <h2><?= get_the_title() ?></h2>
    </header>

    <div class="address__preview">

        <!-- address line 1 -->
        <div class="row active">
            <dl class="col-sm-6">
                <dt class="address__preview__label">Billing Address</dt>
                <dd class="address__preview__value">
                    <span class="text-gray">First name:</span> <?= $user->billing_first_name ?><br>
                    <span class="text-gray">Last name:</span> <?= $user->billing_last_name ?><br>
                    <span class="text-gray">Company:</span> <?= $user->billing_company ?><br>
                    <span class="text-gray">Address 1:</span> <?= $user->billing_address_1 ?><br>
                    <span class="text-gray">Address 2:</span> <?= $user->billing_address_2 ?><br>
                    <span class="text-gray">District:</span> <?= $user->billing_city ?><br>
                    <span class="text-gray">Province:</span> <?php echo $states[$user->billing_state] ?><br>
                    <span class="text-gray">Contact:</span> <?= $user->billing_country_code . ' ' . $user->billing_mobile?><br>
                    <!-- <span class="text-gray">Phone:</span> <?= $user->billing_phone ?><br> -->
                    <!-- <span class="text-gray">Postal Code:</span> <?= $user->billing_postcode ?><br> -->
                </dd>
            </dl>

            <div class="col-xs-12 address__preview__actions">
                <button class="btn btn-primary btn-noWidth btn-edit-my-address" data-address-type="billing">Edit</button>
            </div>
        </div>

        <!-- address line 2 -->
        <div class="row">
            <dl class="col-sm-6">
                <dt class="address__preview__label">Shipping Address</dt>
                <dd class="address__preview__value">
                    <span class="text-gray">First name:</span> <?= $user->shipping_first_name ?><br>
                    <span class="text-gray">Last name:</span> <?= $user->shipping_last_name ?><br>
                    <span class="text-gray">Company:</span> <?= $user->shipping_company ?><br>
                    <span class="text-gray">Address 1:</span> <?= $user->shipping_address_1 ?><br>
                    <span class="text-gray">Address 2:</span> <?= $user->shipping_address_2 ?><br>
                    <span class="text-gray">District:</span> <?= $user->shipping_city ?><br>
                    <span class="text-gray">Province:</span> <?php echo $states[$user->shipping_state] ?><br>
                    <span class="text-gray">Contact:</span> <?= $user->shipping_country_code . ' ' . $user->shipping_mobile?><br>
                </dd>
            </dl>
<!--             <dl class="col-sm-12">
                <dt class="address__preview__label">Delivery remark</dt>
                <dd class="address__preview__value">Door Code 1234 – Leave on the door front if not there.</dd>
            </dl> -->
            <div class="col-xs-12 address__preview__actions">
                <button class="btn btn-primary btn-noWidth btn-edit-my-address" data-address-type="shipping">Edit</button>
            </div>
           <!--  <div class="col-xs-12  address__preview__radios"  role="group" aria-labelledby="delivery__default--1">
                <div><i class="icon icon-check"></i> My default delivery address</div>
                <a href="#" class="link link--right">Set as the default billing address</a>
            </div> -->
        </div>

    </div>

  <!--   <div class="account-detail__footer">
        <a class="btn btn-primary pull-right" href="adresses.php?edit=1" title="Add an address">Add an address</a>
    </div> -->

</div>
