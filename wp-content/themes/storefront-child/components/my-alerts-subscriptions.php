<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( isset($_POST['form-for']) && $_POST['form-for'] == 'alerts-subscriptions' ) :


    $user = nespresso_get_user_data();

    update_user_meta( $user->ID, 'receive_alerts_on_for_descaling', sanitize_text_field( isset($_POST['receive_alerts_on_for_descaling']) ? $_POST['receive_alerts_on_for_descaling'] : 0 ) );

    global $nespresso_my_alerts_subscription_hook_script;
    $nespresso_my_alerts_subscription_hook_script = 'validation.success_information_updated();';

    // put some scripts on the bottom
    if ( $nespresso_my_alerts_subscription_hook_script ) :
        function nespresso_my_express_checkout_hook() {
            global $nespresso_my_alerts_subscription_hook_script;
        ?>
            <script type="text/javascript">
                var validation = new Validation();
                <?= $nespresso_my_alerts_subscription_hook_script ?>
            </script>
        <?php
        } // nespresso_my_express_checkout_hook()
        add_action('wp_footer', 'nespresso_my_express_checkout_hook');
    endif;

endif;


$my_account_url = get_permalink( get_option('woocommerce_myaccount_page_id') );

$user = nespresso_get_user_data();

$current_url = home_url(add_query_arg(null, null));

?>

<main id="content">
	<div class="container">
	    <div class="row">

	        <!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <form action="<?= $current_url ?>" method="post" id="form-alerts-subscriptions" role="form">

	        	<input type="hidden" name="form-for" value="alerts-subscriptions">

	            <div class="col-xs-12 col-md-8-5 account-detail my-alerts">
		            <header>
		                <h2><?= get_the_title() ?></h2>
		            </header>
		            <form method="post" action="my-alerts.php">
		                <div class="row">

		                    <div class="bloc-alert col-xs-12 col-sm-6">
		                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/my_alert2.jpg" alt="Alerts on for descaling">
		                        <div class="checkbox" role="group" aria-labelledby="my_alert2">
		                            <label for="my_alert2">
		                                <input type="checkbox"
		                                	name="receive_alerts_on_for_descaling"
		                                	id="receive_alerts_on_for_descaling"
		                                	value="Receive alerts on for descaling"
		                                	title="Receive alerts on for descaling"
		                                	<?= isset($user->receive_alerts_on_for_descaling) && $user->receive_alerts_on_for_descaling ? 'checked="checked"' : '' ?>
	                                	>
		                                <span>Receive alerts on for descaling</span>
		                            </label>
		                        </div>
		                        <div class="content">
		                            <p>
		                                We recommend that your machine is cleaned regularly and descaled after 600 coffees. To help you with this regular maintenance, <i>Nespresso</i> offers to send you an alert once you reach the treshold of 600 capsules ordered.
		                            </p>
		                        </div>
		                    </div>
		                </div>
		                <div class="account-detail__footer clearfix">
					        <button type="send" class="btn btn-primary pull-right" id="btn-save-my-alerts-subscriptions" title="Save changes">Save changes</button>
				        </div>
		            </form>
		        </div>

	        </form>

        </div>
    </div>
</main>

<?php
	global $scripts;
	$scripts[] = 'js/components/validation.js';
	$scripts[] = 'js/components/my-alerts-subscriptions.js';
?>
