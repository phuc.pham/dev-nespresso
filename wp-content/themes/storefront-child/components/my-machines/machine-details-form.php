<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>
<div class="pd-top-20 pd-bottom-20 container-fluid hide" id="machine-details">

    <div class="col-md-12">
        <div class="mr-bottom-20 row" id="prodct_name_container" style="font-size: 1.5em;"></div>
    </div>

    <div class="col-md-6 col-xs-12">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="serial_no">
                        MACHINE SERIAL NUMBER
                    </label>
                    <input type="text"
                        name="serial_no"
                        class="form-control mr-top-10"
                        style="width: 100%;"
                        placeholder="e.g. 13364D50p128304A095"
                    >
                    <input type="hidden" name="product_id" id="product_id" value="">
                    <input type="hidden" name="product_sku" id="product_sku" value="">
                    <div>
                        <a href="<?= bloginfo('url') ?>/services#customer-care" class="text-gray" target="_blank">
                            Need assistance? Call our Coffee Specialists
                        </a>
                    </div>
                </div><!-- .form-group -->
            </div><!-- .col-md-12 -->
        </div><!-- .row -->

        <div class="row mr-top-20">
            <div class="col-md-12 mr-top-20">
                <div class="form-group">
                     <label for="day">
                        DATE OF PURCHASE
                    </label>

                    <div class="mr-top-10 row">

                        <!-- day -->
                        <div class="pd-left-10 pd-right-5 pd-0 col-md-4 col-xs-12">
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="current">Day</span>
                                </button>
                                <select tabindex="-1" name="day" id="day">
                                    <option value="0">Day</option>
                                    <?php for($day=1; $day<=31; $day++) : ?>
                                        <option value="<?= $day ?>" <?php echo $day == date('d') ? 'selected' : '' ?>><?= $day ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div><!-- .col-md-4 -->

                        <!-- month -->
                        <div class="pd-right-5 pd-0 col-md-4 col-xs-12">
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="current">Month</span>
                                </button>
                                <select tabindex="-1" name="month" id="month">
                                    <option value="0">Month</option>
                                    <?php
                                        for($month=1; $month<=12; $month++) :
                                            $monthName =  date('F', mktime(0, 0, 0, $month, 10));
                                    ?>
                                        <option value="<?= $month ?>" <?php echo $monthName == date('F') ? 'selected' : '' ?>><?= $monthName ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div><!-- .col-md-4 -->

                        <!-- year -->
                        <div class="pd-right-10 col-md-4 col-xs-12">
                            <div class="dropdown dropdown--input dropdown--init">
                                <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                    <span class="current">Year</span>
                                </button>
                                <select tabindex="-1" name="year" id="year">
                                    <option value="0">Year</option>
                                     <?php for($year=date('Y'); $year>=2000; $year--) : ?>
                                        <option value="<?= $year ?>" <?php echo $year == date('Y') ? 'selected' : '' ?>><?= $year ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div><!-- .col-md-4 -->

                    </div><!-- .row -->

                    <div class="row mr-top-20">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="obtained_by">
                                   How did you obtain your machine? <span class="text-red">*</span>
                                </label>
                                <div class="dropdown dropdown--input dropdown--init mr-top-10">
                                    <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                        <span class="current">Please Select</span>
                                    </button>
                                    <select tabindex="-1" name="obtained_by" id="obtained_by">
                                        <option value="">Please select</option>
                                        <option value="Online">Online</option>
                                        <option value="Retailer">Retailer</option>
                                        <option value="Outside the Vietnam">Outside the Vietnam</option>
                                    </select>
                                </div>
                            </div><!-- .form-group -->
                        </div><!-- .col-md-12 -->
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="hide" id="container-product-color-variant">
                                <!-- <div class="product__info"> -->
                                    <div class="product__info__colors">
                                        <label for="registered_machine_obtained_by">
                                            Available product colors <span class="text-red">*</span>
                                        </label>
                                        <ul class="product__info__colours__list"></ul>
                                    </div><!-- .form-group -->
                                <!-- </div> -->
                            </div>
                        </div><!-- .col-md-12 -->
                    </div><!-- .row -->

                </div><!-- .form-group -->
            </div><!-- .col-md-12 -->
        </div><!-- .row -->

    </div><!-- .col-md-6 -->

    <div class="col-md-6 col-xs-12 text-center">
        <img src="" class="step3-selected-img img-responsive" id="machine_details_image">
    </div><!-- .col-md-6 -->
</div><!-- .container-fluid -->
