<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// for post validation and saving, please see my-account-functions.php
?>

<form method="post" action="<?= $current_url ?>" id="form-my-machines" role="form">

    <?php wp_nonce_field( 'nespresso_post_nonce', 'nespresso_post_nonce_field' ); ?>

    <input type="hidden" name="form-for" value="my-machine-form">
    <input type="hidden" name="my_machine_index" value="">
    <input type="hidden" name="product_color_variant_index" value="">

    <div class="row" id="step_1_container">
        <div class="pd-0 col-md-12">
            <h3 class="main-content__title">Step 1: Choose your machine type</h3>
        	<div class="my-machines__addlist">
                <?php $i = 1; foreach($products as $name => $product ) :
                    if (get_field('bundle', $product->product_id)) {
                        continue;
                    }
                     ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 my-machines__addlist__machine my-machines__addlist__step1">
                    	<a href="javascript:void(0)" class="open-family" data-product-name="<?= $name ?>" data-family="family<?php echo $name; ?>" title="View the <?= $name ?> machines">
                            <img src="<?= $product->image[0]; ?>" class="img-responsive img-responsive-center img-machines-list" alt="<?= $name ?> Machine" />
                        </a>
                        <h4><?= $name ?></h4>
                    </div>
                <?php $i++; endforeach; ?>

                <div class="col-xs-6 col-sm-4 col-md-3 my-machines__addlist__machine my-machines__addlist__step1 text-center">
                        <a href="javascript:void(0)" class="open-family mr-top-20 pd-top-20" data-product-name="" data-family="" title="View the <?= $name ?> machines">
                            <i class="fa fa-plus-circle fa-5x mr-top-20" aria-hidden="true"></i>
                        </a>
                        <h4>Other</h4>
                    </div>

            </div><!-- .my-machines__addlist -->
        </div><!-- .col-md-12 -->
    </div><!-- .row -->

    <!-- <div class="mr-top-20 row hide step_2_container">

        <div class="col-md-12">

        	<div class="my-machines__addlist my-machines__step2" id="step2-family0">
                <h3 class="main-content__title">Step 2: Choose your machine color</h3>
            </div>

            <div class="my-machines__addlist_colors"></div>
        </div>
    </div> -->

    <div id="sub_machine__detail" class="row my-machines__addlist__step3 step_3_container hide">
        <div class="pd-0 col-md-12">
            <h3 class="main-content__title">Step 2: Enter your machine information</h3>
            <?php include ( __DIR__ . '/../my-machines/machine-details-form.php'); ?>
        </div>
    </div>

    <div class="account-detail__footer">
        <button type="button" class="hide btn btn-primary btn-icon" id="btn-back-to-my-machines" title="Back to my machines"><i class="icon-arrow_right"></i>Back to my machines</button>
        <button type="submit" class="hide btn btn-primary pull-right btn-save-info" id="btn-save" title="Save changes and go back to my machines">Save changes</button>
    </div>

</form>
