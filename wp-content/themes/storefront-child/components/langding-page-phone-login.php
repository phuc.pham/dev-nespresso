<?php
	
	if (!defined('ABSPATH')) {
		exit; // Exit if accessed directly
	}
	
	$errors = array();
	global $wpdb;
	session_start();
	$step = 1;
	
	if (isset($_POST['loginsuccess'])) {
		$step = 4;
	}
	
	if (isset($_POST['tittle']) && isset($_POST['fullname']) && isset($_POST['email']) && isset($_POST['phone']) )
	{
		$table = 'wp_coffee_customers';
		if(!$_POST['tittle'])
			$errors['tittle'] = 'Please select title';
		if(!$_POST['fullname'])
			$errors['fullname'] = 'Please enter your name';
		if(!$_POST['email'])
		{
			$errors['email'] = 'Please enter your email';
		}else
			if (!preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $_POST['email'])) {
				$errors['email'] = 'Please enter  the correct email address (e.g. customer@gmail.com)';
			}else
			{
				$cus = $wpdb->get_row($wpdb->prepare( "select * from $table where email = %s" ,$_POST['email']));
				if($cus)
				{
					$errors['email'] = 'Email is already in use';
				}
			}
			
		if(!$_POST['phone'])
		{
			$errors['phone'] = 'Please enter your phone';
		}else if (!preg_match('/^(087|086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$/',$_POST['phone'])) {
				$errors['phone'] = 'Should be numeric, start with \'0\' and with 9 characters minimum (example: 0912345678)';
			}else
			{
				$cus = $wpdb->get_row($wpdb->prepare( "select * from $table where phone = %s" ,$_POST['phone']));
				if($cus)
				{
					$errors['phone'] = 'Phone number is already in use';
				}
			}
	
	
        if(!$errors)
        {
            
            $data = array(
                'title' => $_POST['tittle'],
                'name' => $_POST['fullname'],
                'email' => $_POST['email'],
                'phone' => $_POST['phone'],
                'created_at' => date('Y-m-d H:i:s')
            );
            $format = array('%s', '%s', '%s', '%s');
            $wpdb->insert($table, $data, $format);
            $customer_id = $wpdb->insert_id;
            if ($customer_id) {
                $cus = $wpdb->get_row($wpdb->prepare("select * from  $table  where phone = %s", $_POST['phone']));
                $_SESSION['customer_langdingpage'] = $cus;
                $step = 3;
            } else {
                $step = 2;
            }
        
        }
        else
        {
	        $step = 2;
        }
			
	  
		
	}
	
	if (isset($_POST['customerphone'])) {
		global $wpdb;
		$table = 'wp_langdingpage2_customers';
		
		if (!$_POST['customerphone']) {
			$errors['customerphone'] = 'Please enter your phone';
		} else if (!preg_match('/^(087|086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$/', $_POST['customerphone'])) {
			$errors['customerphone'] = 'Should be numeric, start with \'0\' and with 9 characters minimum (example: 0912345678)';
		} else {
			$tablecus = $wpdb->prefix . 'coffee_customers';
			$cus = $wpdb->get_row($wpdb->prepare("select * from $tablecus where phone = %s", $_POST['customerphone']));
			if ($cus) {
				$_SESSION['customer_langdingpage'] = $cus;
				$step = 3;
			} else {
				$step = 2;
			}
		}
	}
	
	if ($_POST['yourcompany'] && $_POST['monthlyincome']) {
		$_SESSION['result_langdingpage'] = "";
		$result = "";
		foreach ($_POST as $key => $value) {
			if($key == 'yourcompany' || $key == 'monthlyincome') {
				$result .= '"' . $key . '":"' . $value . '",';
			}
		}
		
		$_SESSION['result_langdingpage'] .= $result;
		$step = 5;
		
	}
	
	if ($_POST['question03'] && $_POST['question04'] && $_POST['question05']) {
		$result = "";
		foreach ($_POST as $key => $value) {
		    if($key == 'question03' || $key == 'question04' || $key == 'question05')
            {
	            $result .= '"' . $key . '":"' . $value . '",';
            }
			
		}
		
		$_SESSION['result_langdingpage'] .= $result;
		$step = 6;
	}
	
	if ($_POST['question06'] && $_POST['question07'] && $_POST['question08']) {
		$result = "";
		foreach ($_POST as $key => $value) {
			if($key == 'question06' || $key == 'question07' || $key == 'question08') {
				$result .= '"' . $key . '":"' . $value . '",';
			}
		}
		
		$_SESSION['result_langdingpage'] .= $result;
		$step = 7;
	}
	
	if ($_POST['question09'] && $_POST['question10']) {
		$result = "";
		foreach ($_POST as $key => $value) {
			if($key == 'question09' || $key == 'question10') {
				$result .= '"' . $key . '":"' . $value . '",';
			}
		}
		
		$_SESSION['result_langdingpage'] .= $result;
		$step = 8;
	}
	
	if ($_POST['question11'] && $_POST['question12'] && $_POST['question13'] && $_POST['question14']) {
		$result = "";
		foreach ($_POST as $key => $value) {
			if($key == 'question11' || $key == 'question12' || $key == 'question13' || $key == 'question14' ) {
				$result .= '"' . $key . '":"' . $value . '",';
			}
		}
		
		$_SESSION['result_langdingpage'] .= $result;
		$step = 9;
	}
	
	if ($_POST['question15'] && $_POST['question16'] && $_POST['question19']) {
		$result = "";
		foreach ($_POST as $key => $value) {
			if($key == 'question15' || $key == 'question16' || $key == 'question19') {
				$result .= '"' . $key . '":"' . $value . '",';
			}
		}
		
		$_SESSION['result_langdingpage'] .= $result;
		$step = 10;
	}
	
	if ($_POST['question20'] && $_POST['question21'] && $_POST['question22'] && $_POST['question23']) {
		$result = "";
		foreach ($_POST as $key => $value) {
			if($key == 'question20' || $key == 'question21' || $key == 'question22' || $key == 'question23' ) {
				$result .= '"' . $key . '":"' . $value . '",';
			}
		}
		
		$_SESSION['result_langdingpage'] .= $result;
		$step = 11;
	}
	
	if ($_POST['question24'] && $_POST['question25']) {
		$result ="";
		foreach ($_POST as $key => $value) {
			if($key == 'question24' || $key == 'question25') {
				$result .= '"' . $key . '":"' . $value . '",';
			}
		}
		$result = rtrim($result, ',');
		$_SESSION['result_langdingpage'] .= $result;
		

		$data = [
			'phone' => $_SESSION['customer_langdingpage']->phone,
			'title' => $_SESSION['customer_langdingpage']->title,
			'name' => $_SESSION['customer_langdingpage']->name,
			'email' => $_SESSION['customer_langdingpage']->email,
			'result' => '{'.$_SESSION['result_langdingpage'].'}',
			'created_at' => date('Y-m-d H:i:s')
		];
		
		$format = array('%s', '%s', '%s', '%s', '%s' );
		$wpdb->insert('wp_langdingpage2_customers', $data);
		$customer_id = $wpdb->insert_id;
		
		if ($customer_id)
		{
			$_SESSION['langding_page_customer_id'] = $customer_id;
			header("Location: /langding_page_thanks_you/");
			exit;
		} else {
			$step = 2;
		}
	}
	
	
	if ($step == 1) {
		?>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/langdingpage2.css">
		<main id="content" class="fullPage fullPage__plp coffee-list <?= 'step'.$step ?> ">
			<!-- end banner -->
			<div class="container">
				<div class="row">
					<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
						<div class="productlist-main clearfix">
							<div class="productlist-panel col-xs-12 col-md-12">
								<div class="productlist-contents">
									<form action="#" method="post">
										<section class="product-category text-center">

											<div class="card text-center">

												<div class="card-body">
													<h1 class="card-title" style="color: #d4a304;font-size: 2rem;">
														WELCOME TO NESPRESSO BUILDING CORNER 2021</h1>
													<h1 class="card-title" style="color: #000;font-size: 2rem;">
														LOGIN</h1>
													<small class="text-danger"><?= $errors['customerphone'] ?? '' ?></small>
													<div class="row">
														<div class="col-md-6">
															<h2 style="float: left;font-size: 1.3rem !important;color: black;">
																Phone number* </h2>
														</div>
														<div class="col-md-6">
															<input type="text" style="width: 100%" class="form-control "
																   name="customerphone" id="customerphone">
															<small id="customerphone"
																   data-msgvi="Vui lòng nhập số điện thoại của bạn"
																   data-msg="Insert your phone here…"
																   class="text-danger"></small>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">

														</div>

														<div class="text-center"
															 style="margin-bottom: 30px; margin-top: 10px;">
															<button class="text-center"
																	style="color: #fff;background: #866b0a;"
																	type="submit" title="Save my informations"> Login
															</button>
														</div>
														<h3 style="font-size: 1.3rem !important;color: #FFF;">Create new
															account</h3>

													</div>
												</div>

											</div>


										</section>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</main>
		<?php
	} elseif ($step == 2) {
		?>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/langdingpage2.css">
		<main id="content" class="fullPage fullPage__plp coffee-list <?= 'step'.$step ?> ">
			<!-- end banner -->
			<div class="container">
				<div class="row">
					<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
						<div class="productlist-main clearfix">
							<div class="productlist-panel col-xs-12 col-md-12">
								<div class="productlist-contents">
									<form action="#" method="post">
										<section class="product-category text-center">

											<div class="card text-center">

												<div class="card-body">
													<h1 class="card-title" style="color: #d4a304;font-size: 2rem;">
														WELCOME TO NESPRESSO BUILDING CORNER 2021</h1>
													<h1 class="card-title" style="color: #000;font-size: 2rem;">PERSONAL
														INFORMATION</h1>
													<small class="text-danger"><?= $errors['alert'] ?? '' ?></small>
													<div class="row">
														<div class="col-md-6">
															<h2 style="float: left;font-size: 1.3rem !important;color: black;">
																Tittle* </h2>
														</div>
														<div class="col-md-6">
															<input type="text" style="width: 100%" class="form-control "
																   name="tittle" id="tittle">
                                                            <small  class="text-danger"><?=$errors['tittle']??''?></small>
														</div>
														<div class="col-md-6">
															<h2 style="float: left;font-size: 1.3rem !important;color: black;">
																Full name* </h2>
														</div>
														<div class="col-md-6">
															<input type="text" style="width: 100%" class="form-control "
																   name="fullname" id="fullname">
                                                            <small  class="text-danger"><?=$errors['fullname']??''?></small>
														</div>
														<div class="col-md-6">
															<h2 style="float: left;font-size: 1.3rem !important;color: black;">
																E-mail address* </h2>
														</div>
														<div class="col-md-6">
															<input type="text" style="width: 100%" class="form-control "
																   name="email" id="email">
                                                            <small  class="text-danger"><?=$errors['email']??''?></small>
														</div>
														<div class="col-md-6">
															<h2 style="float: left;font-size: 1.3rem !important;color: black;">
																Phone number* </h2>
														</div>
														<div class="col-md-6">
															<input type="text" style="width: 100%" class="form-control "
																   name="phone" id="phone">
                                                            <small  class="text-danger"><?=$errors['phone']??''?></small>
														</div>
													</div>
													<div class="row">

														<div class="text-center"
															 style="margin-bottom: 30px; margin-top: 10px;">
															<button class="text-center"
																	style="color: #fff;background: #866b0a;"
																	type="submit" title="Save my informations"> Submit
															</button>
														</div>

													</div>
												</div>

											</div>


										</section>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</main>
		<?php
	} elseif ($step == 3) {
		?>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/langdingpage2.css">
		<main id="content" class="fullPage fullPage__plp coffee-list <?= 'step'.$step ?> ">
			<!-- end banner -->
			<div class="container">
				<div class="row">
					<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
						<div class="productlist-main clearfix">
							<div class="productlist-panel col-xs-12 col-md-12">
								<div class="productlist-contents">
									<form action="#" method="post">
										<section class="product-category text-center">

											<div class="card text-center">

												<div class="card-body">
													<h1 class="card-title" style="color: #d4a304;font-size: 2rem;">
														WELCOME TO NESPRESSO BUILDING CORNER 2021</h1>
													<h1 class="card-title" style="color: #000;font-size: 2rem;">
														SUCCESSFULLY SIGNED IN!</h1>
													<small class="text-danger"><?= $errors['alert'] ?? '' ?></small>
													<div class="row">
														<div class="col-md-12>
													<h2 style=" float: left;font-size: 1.3rem !important;color: black;
														">Your feedback matters! Finish this short survey & you will
														have a chance to WIN A NESPRESSO MACHINE </h2>
													</div>
												</div>
												<div class="row">
													<div class="text-center"
														 style="margin-bottom: 30px; margin-top: 10px;">
														<input type="hidden" name="loginsuccess" value="1">
														<button class="text-center"
																style="color: #fff;background: #866b0a;" type="submit"
																title="Save my informations"> TAKE IT NOW!
														</button>
													</div>

												</div>
											</div>

								</div>


								</section>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>

		</main>
		<?php
	} elseif ($step >= 4) {
		global $wpdb;
		$table = 'wp_langdingpage2_question';
		
		
		switch ($step) {
			case 4:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 0,2");
				break;
			case 5:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 2,3");
				break;
			case 6:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 5,3");
				break;
			case 7:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 8,2");
				break;
			case 8:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 10,4");
				break;
			case 9:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 14,3");
				break;
			case 10:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 17,4");
				break;
			case 11:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 21,2");
				break;
			default:
				$questions = $wpdb->get_results("SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 0,2");
		}
		
		?>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/langdingpage2.css">
		<main id="content" class="fullPage fullPage__plp coffee-list <?= 'step'.$step ?> ">
			<!-- end banner -->
			<div class="container">
				<div class="row">
					<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
						<div class="productlist-main clearfix">
							<div class="productlist-panel col-xs-12 col-md-12">
								<div class="productlist-contents">
									<form action="#" method="post" id="formlangdingpage2">
										<section class="product-category text-center">
											<a title="PERSONAL INFORMATION" style="background-color:#986F38"
											   class="btn">CONSUMPTION HABIT</a>
											<?php
												
												foreach ($questions as $question) {
													if ($question->type_question == 1) {
														
														?>
														<div class="card" style="margin: 20px 0px ">
															<div class="row">
																<div class="col-md-6">
																	<h1 class="input-group-text"><?= $question->question ?></h1>
																</div>
																<div class="col-md-6">
																	<input type="text" style="width: 100%"
																		   class="form-control <?= $question->name ?>"
																		   name="<?= $question->name ?>"
																		   id="<?= $question->name ?>"
																		   placeholder="Insert your answer here… ...."
																		   required="">
																	<small id="<?= $question->name ?>"
																		   data-msgvi="Vui lòng nhập tên công ty của bạn "
																		   data-msg="Insert your answer here…"
																		   class="text-danger"></small>
																</div>


															</div>
														</div>
														<?php
													} else {
														?>
														<div class="row">
															<div class="col-md-6">
																<h1 class="input-group-text"><?= $question->question ?></h1>
															</div>
															<div class="col-md-6">
																
																<?php
																	global $wpdb;
																	$table_answer = 'wp_langdingpage2_answer';
																	//
																	$answer = $wpdb->get_row($wpdb->prepare(
																		"SELECT `id`,`answer`,`class` FROM $table_answer as a WHERE a.id_question = %d",
																		$question->id
																	));
																	$i = 1;
																	
																	foreach (explode(',', $answer->answer) as $key => $ite) {
																		?>
																		<div class="custom-control custom-radio <?= $answer->class ?>">
																			<input type="radio"
																				   id="<?= $question->name . $i ?>"
																				   name="<?= $question->name ?>"
																				   value="<?= $ite ?>"
																				   class="custom-control-input <?= $question->name ?>">
																			<label class="custom-control-label"
																				   for="<?= $question->name . $i ?>"><?= $ite ?></label>
																		</div>
																		<?php
																		$i++;
																	}
																
																?>
															</div>
														</div>
														<?php
													}
												}
											?>
										</section>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			</div>


		</main>
		<?php
	}
?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/langdingpage2.js"></script>

<style>
	#content.fullPage__plp {
		background-image: url(/wp-content/themes/storefront-child/images/langding-page-2/BACKGROUND3.jpg);
		background-size: cover;
		background-position: center center;
		height: 900px;
		padding: 100px 50px;
	}
	.productlist .productlist-main .productlist-panel {
		background: unset;
	}

	.productlist .productlist-main .productlist-panel {
		background: unset;
	}
    .custom-control.custom-radio.questionyesno {
        float: left !important;
        width: 50% !important;
    }
    .custom-control.custom-radio.question4 {
        display: grid;
    }
    .custom-control.custom-radio.question5 {
        display: inline-grid;
        width: calc(33% - 5px);
        margin: 5px 0px;
    }
    .custom-control.custom-radio.question6 {
        display: grid;
        margin: 5px 0px;
        float: left;
        width: 50%;
    }
    .custom-control.custom-radio.question7 {
        display: inline-grid;
        width: 50%;
        float: left;
    }
    .custom-control.custom-radio.question9 {
        display: unset;
        float: left;
        padding: 5px 0px;
        margin: 5px -10px;
    }
    .custom-control.custom-radio.question9 label {
        font-size: 15px !important;
        padding: 5px;
        margin: 5px 0px;
    }
    .custom-control.custom-radio.question10 {
        display: inline-grid;
        width: 13%;
    }
    .custom-control.custom-radio.question15 {
        display: inline-grid;
        float: left;
        width: 50%;
        margin: 5px 0px;
    }
    .custom-control.custom-radio.question15 label {
        font-size: 14px;
    }
    .custom-control.custom-radio.question16 {
        display: inline-grid;
        width: 8%;
        margin: 5px 0px 0px 0px;
    }
    .custom-control.custom-radio.question16 label {
        font-size: 14px;
    }
    .custom-control.custom-radio.question17 label {
        font-size: 15px;
    }
    .custom-control.custom-radio.question17 {
        display: inline-grid;
        float: left;
        align-items: center;
        width: 33%;
    }
    .custom-control.custom-radio.question18 {
        display: inline-grid;
        width: 8%;
        margin: 5px 0px 0px 0px;
    }
    .custom-control.custom-radio.question19 label {
        font-size: 13px;
    }
    .custom-control.custom-radio.question19 {
        display: inline-grid;
        margin: 5px 0px 0px 0px;
    }
    .custom-control.custom-radio.question20, .custom-control.custom-radio.question21,.custom-control.custom-radio.question23,.custom-control.custom-radio.question22 {
        display: inline-grid;
        width: 35%;
    }
    input[type="checkbox"], input[type="radio"]{
        display: none;
    }
    input[type=radio]:checked+label {
        color: blue;
    }
</style>
