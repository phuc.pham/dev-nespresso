<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if(!isset($_SESSION['sv_customer_id']) || !$_SESSION['sv_customer_id'])
{
    header('location: /coffee-list-question');
    exit;
}
   
?>

<!-- content -->

<main id="content" class="fullPage fullPage__plp coffee-list">
    <!-- end banner -->
    <div class="container">
        <div class="row">
            <div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
                <div class="productlist-main clearfix">
                    <div class="productlist-panel col-xs-12 col-md-12">
                        <div class="productlist-contents">
                        
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1>
                                        WELCOME TO NESPRESSO BUILDING CORNER 2021 
                                    </h1>
                                </div>

                                <div class="col-md-12 text-center">
                                    <h1>
                                        THANK YOU!
                                    </h1>
                                </div>

                                <div class="col-md-12 text-center">
                                    <h2>
                                        We ‘ve got your information. Thank you for your time!
                                    </h2>
                                </div>

                              
                                <form method="post" action="/coffee-list-question-discover/">
                                    <div class="text-center" style="margin-bottom: 30px; margin-top: 10px;">
                                        <button class="text-center" style="color: #fff;background: #866b0a;" type="submit" title="Save my informations"> COFFEE TEST NOW! </button>
                                    </div>
                                </form>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main><!-- #content -->

<!-- /content -->
