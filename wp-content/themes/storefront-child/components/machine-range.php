<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

// Category Banner
$category_banner = get_nespresso_category_banner();

$machine_desktop_img = isset($category_banner['machine_category_banner_image_url']) && $category_banner['machine_category_banner_image_url'] ? $category_banner['machine_category_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 25/MachineList_Desktop.png";
$machine_mobile_img = isset($category_banner['machine_category_banner_image_url_mobile']) && $category_banner['machine_category_banner_image_url_mobile']  ? $category_banner['machine_category_banner_image_url_mobile'] : get_stylesheet_directory_uri() . "/images/banner/September 25/MachineList_Mobile.png";
$machine_link = isset($category_banner['machine_category_banner_link']) && $category_banner['machine_category_banner_link'] ? $category_banner['machine_category_banner_link'] : "/machine-list/";

global $scripts;
$scripts[] = 'js/components/machine-range.js';

$products = get_machine_products();

$product_count = count($products);
$arrays = get_nespresso_product_list();
if (!$arrays) {
    $machines = [];
    foreach ($products as $value) {
        $machines[] = $value->name;
    }
}else{
    $machines = $arrays['machine'];
}

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/machine-range.css">
<main id="main" class="fullPage fullPage__plp coffee-list background-black">
    <!-- banner -->
    <div class="new-banner">
        <!-- web -->
        <a class="web-banner-img"><img src="<?php echo $machine_desktop_img ?>" draggable="false"/></a>
        <!-- mobile -->
        <a class="mobile-banner-img"><img src="<?php echo $machine_mobile_img ?>" draggable="false"/> </a>
    </div>
    <!-- end banner -->
    <div id="machine-list" class="wrapper centered-on-page site-width space-after ng-scope">
      <div class="machine-list ng-scope">
        <article class="machine-list-header clearfix">
          <div class="machine-list-title">
              <h2><em>Nespresso</em> Coffee Machines</h2>
              <p>All <em>Nespresso</em> coffee machines are equipped with a patented extraction system for perfect pressure to produce an exceptional coffee, every time. Add a Nespresso Aeroccino milk frother to create a luxurious cappuccino or latte. Plus enjoy free delivery with any Nespresso machine purchased. The Perfect Cup – Every Time.</p>
          </div>
            <a class="btn-machine-range button-primary button-green buy-btn" href="/machine-list">Buy</a>
            <a class="btn-machine-range button-primary button-grey compare-btn hide" href="#">Compare</a>
        </article>
        <div class="machine-list-filter-buttons hide" data-ng-show="true">
            <span>Filter By :</span>

            <!-- ngRepeat: technology in machineListTechnologies -->
            <label for="filters.technology.original" id="original-line" class="btn-filter full-width-elem-2 technology ng-scope first">
            <input class="angular-custom-input visually-hidden ng-pristine ng-valid" name="filters.technology" id="filters.technology.original" value="original" type="radio">
            </label>
            <!-- end ngRepeat: technology in machineListTechnologies -->
            <label for="filters.technology.vertuo" id="vertuo-line" class="btn-filter full-width-elem-2 technology ng-scope last">
                <input class="angular-custom-input visually-hidden ng-pristine ng-valid" name="filters.technology" id="filters.technology.vertuo" value="vertuo" type="radio">
            </label><!-- end ngRepeat: technology in machineListTechnologies -->

            <a href="#" class="container-tooltip learnmore-trigger popin-trigger" data-popin-id="learnmore-machine-technology">
                <img src="/wp-content/themes/storefront-child/images/machine-range/question-mark-dark.png" alt="" width="22" height="22">
            </a>
        </div>
        <div>
          <ul class="machine-list-grid clearfix">
            <?php if ( $product_count > 0 ) : ?>
              <?php foreach ($machines as $key => $machine):?>
                <?php foreach($products as $k => $product): ?>
                  <?php if (html_entity_decode($machine) == html_entity_decode($product->name)):
                    $p_colors = [];

                    if ( isset($product->variations) && $product->variations ) :
                        foreach ( $product->variations as $variant ) :
                            if ( isset( $variant['attributes'] ) )
                                if ( isset( $variant['attributes']['attribute_pa_color'] ) )
                                    if ( !in_array($variant['attributes']['attribute_pa_color'], $p_colors) )
                                        $p_colors[] = strtolower($variant['attributes']['attribute_pa_color']);
                        endforeach;
                    endif;
                  ?>
                    <li class="machine-list-item lbt-component ng-scope" data-id="<?= $product->post_id ?>">
                      <a class="top nespresso-product-item" href="<?= $product->url ?>"
                        title="<?= $product->name ?>"
                        data-id="<?=$product->post_id?>"
                        data-name="<?=$product->name?>"
                        data-type="<?=$product->product_type?>"
                        data-position="<?= @$counter ?>"
                        data-list="<?= get_the_title() ?>"
                      >
                        <h3 class="visually-hidden ng-binding"><?= $product->name ?></h3>
                        <img src="<?= $product->image ? $product->image[0] : '' ?>" alt="<?= $product->name ?>" width="200" height="200">
                        <ul class="colors">
                            <?php if( $p_colors ): ?>
                              <?php foreach( $p_colors as $color): ?>
                                <li class="ng-scope">
                                    <span class="visually-hidden ng-binding">Red</span>
                                    <span class="inner-disc" style="background-color: <?= $color ?>;"></span>
                                </li>
                              <?php endforeach; ?>
                            <?php endif ?>
                        </ul>
                      </a>
                      <div class="bottom">
                        <span class="machine-title ng-binding" aria-hidden="true"><?= $product->name ?></span>
                        <div class="machine-list-item-description machine-list-item-description-<?= $product->post_id ?>">
                            <p class="ng-binding"><?= $product->short_description ?></p>
                        </div>
                        <div class="rollover rollover-<?= $product->post_id ?>">
                          <p class="ng-binding"><?=  substr(strip_tags($product->specification), 0, 60) ?>... <a target="_blank" href="<?= $product->url ?>" title="<?= $product->name ?>">See More</a></p>
                          <div class="discover-btn hide">
                               <a class="btn-machine-range button-primary button-grey-shadow ng-binding" href="#">
                                   Discover more
                               </a>
                          </div>
                        </div>
                      </div>
                      <div class="foot clearfix">
                        <div class="price pull-right">
                            <!-- ngIf: machine.notSamePriceWithinTheRange -->
                            <span class="ng-binding" data-ng-bind="machine.primaryPrice"><?= wc_price($product->price) ?></span>
                        </div>
                        <a class="btn-machine-range button-primary button-green pull-right nespresso-product-item" href="<?= $product->url ?>"
                          title="<?= $product->name ?>"
                          data-id="<?=$product->post_id?>"
                          data-name="<?=$product->name?>"
                          data-type="<?=$product->product_type?>"
                          data-position="<?= @$counter ?>"
                          data-list="<?= get_the_title() ?>"
                        >
                            Buy<span class="visually-hidden"></span>
                        </a>
                      </div>
                      <img class="hide" src="/wp-content/themes/storefront-child/images/machine-range/YEP-BADGE-80x80.png" style="position: absolute; top: 10px; right: 0px;">
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </ul>
        </div>
      </div>
    </div>
</main><!-- #content -->
<!-- /content -->
