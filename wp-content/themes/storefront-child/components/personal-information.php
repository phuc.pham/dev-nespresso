<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * some message from backend processing
 */
if (isset($_SESSION['nespresso_my_info_hook_script'])) :

    function nespresso_my_info_hook() {
        global $nespresso_my_info_hook_script;
        $hook_script = $_SESSION['nespresso_my_info_hook_script'];
		unset($_SESSION['nespresso_my_info_hook_script']);
    ?>
        <script type="text/javascript">
            var validation = new Validation();
            <?= $hook_script ?>
        </script>
    <?php
    } // nespresso_my_info_hook()
    add_action('wp_footer', 'nespresso_my_info_hook');

endif;

$user = nespresso_get_user_data();
$current_url = home_url(add_query_arg(array(), $wp->request));
$country_codes = get_country_codes();
?>

<main id="content">
	<div class="container">
	    <div class="row">

	        <!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <div class="pd-right-0 col-xs-12 col-md-9">

		        <form method="post" action="<?= $current_url ?>" role="form" id="form-my-infos">

		        	<?php wp_nonce_field( 'nespresso_post_nonce', 'nespresso_post_nonce_field' ); ?>

		            <input type="hidden" name="form-for" value="my-personal-information">

		            <section class="form">
		                <div class="col-xs-12 account-detail my-infos" style="background: #f2f2f2">
		                    <header>
		                        <h2>My Personal information</h2>
		                    </header>
			                <p class="message message__saved hide">
				                <i class="icon icon-check"></i> Global success
			                </p>
		                    <p>Please fill the form below, Fields marked with<span class="required">*</span> are required.</p>
		                    <div class="row">

		                    	 <!-- title -->
		                        <div class="col-xs-12 my-infos__form">
		                            <div class="input-group input-group-generic is-dirty">
		                                <label class="desktop-label col-sm-3 col-md-4" for="title">Title<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <div class="dropdown dropdown--input dropdown--init">
	                                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
	                                                <span class="current">Please select</span>
	                                            </button>

	                                            <select tabindex="-1" name="title" id="title" class="custom-mobile-select">
	                                                <option value="">Please select</option>
	                                                <option value="Mr/Ms" <?= $user->title == "Mr/Ms" ? 'selected="selected"' : '' ?>>Mr/Ms</option>
	                                                <option value="Mr" <?= $user->title == "Mr" ? 'selected="selected"' : '' ?>>Mr</option>
	                                                <option value="Ms" <?= $user->title == "Ms" ? 'selected="selected"' : '' ?>>Ms</option>
	                                            </select>
	                                        </div>
		                                    <span class="mobile-label">Title<span class="required">*</span></span>
		                                </div>
	                                </div>
	                            </div>

		                        <!-- first name -->
		                        <div class="col-xs-12 my-infos__form">
		                            <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="name">First Name<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <input type="text"
		                                        title="First name"
		                                        id="first_name"
		                                        name="first_name"
		                                        placeholder="First name"
		                                        class="form-control col-sm-12 col-md-9"
		                                        value="<?= $user->first_name; ?>"
		                                    >
		                                    <span class="mobile-label">First Name<span class="required">*</span></span>
		                                </div>
		                            </div>

		                            <!-- middle name -->
		                            <!-- <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="lastname">Middle Name</label>
		                                <div class="col-sm-6">
		                                    <input type="text"
		                                        title="Last name"
		                                        id="middle_name"
		                                        name="middle_name"
		                                        placeholder="Last name"
		                                        class="form-control col-sm-12 col-md-9"
		                                        value="<?= $user->middle_name; ?>"
		                                     >
		                                    <span class="mobile-label">Middle Name</span>
		                                </div>
		                            </div> -->

		                            <!-- last name -->
		                            <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="lastname">Last Name<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <input type="text"
		                                        title="Last name"
		                                        id="last_name"
		                                        name="last_name"
		                                        placeholder="Last name"
		                                        class="form-control col-sm-12 col-md-9"
		                                        value="<?= $user->last_name; ?>"
		                                     >
		                                    <span class="mobile-label">Last Name<span class="required">*</span></span>
		                                </div>
		                            </div>

		                            <!-- date of birth -->
		                            <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="date_of_birth">Date of Birth<span class="required">*</span></label>
		                                <div class="col-sm-8 date-of-birth-container">
		                                	<?php
		                                		$date_ex = explode('-', @$user->date_of_birth);
		                                		$year = isset($date_ex[0]) ? (int)$date_ex[0] : null;
		                                		$month = isset($date_ex[1]) ? (int)$date_ex[1] : null;
		                                		$day = isset($date_ex[2]) ? (int)$date_ex[2] : null;
		                                	?>
		                                	<select name="year" class="form-control">
		                                		<option value="">Select Year</option>
		                                		<?php for ($y = date('Y'); $y > (date('Y')-100); $y--) : ?>
		                                			<option value="<?= $y ?>" <?= $year == $y ? 'selected="selected"' : ''?> ><?= $y ?></option>
	                                			<?php endfor; ?>
		                                	</select>
		                                	<select name="month" class="form-control">
		                                		<option value="">Select Month</option>
		                                		<?php for ($m = 1; $m < 13; $m++) : ?>
		                                			<?php $dateObj = DateTime::createFromFormat('!m', $m); ?>
		                                			<option value="<?= $m ?>" <?= $month == $m ? 'selected="selected"' : ''?>><?= $dateObj->format('F') ?></option>
	                                			<?php endfor; ?>
		                                	</select>
		                                	<select name="day" class="form-control">
		                                		<option value="">Select Day</option>
		                                		<?php for ($d = 1; $d < 32; $d++) : ?>
		                                			<option value="<?= $d ?>" <?= $day == $d ? 'selected="selected"' : ''?>><?= $d ?></option>
	                                			<?php endfor; ?>
		                                	</select>
		                                    <input type="hidden"
		                                        title="Date of Birth"
		                                        id="date_of_birth"
		                                        name="date_of_birth"
		                                        value="<?= $user->date_of_birth; ?>"
		                                    >
		                                    <span class="mobile-label">Date of Birth<span class="required">*</span></span>
		                                </div>
		                            </div>

		                            <!-- email -->
		                            <div class="input-group input-group-generic hidden">
		                                <label class="desktop-label col-sm-3 col-md-4" for="email">Email address<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <input type="text"
		                                        title="Email address"
		                                        id="email"
		                                        name="email"
		                                        placeholder="Email address"
		                                        class="form-control col-sm-12 col-md-9"
		                                        value="<?= @$user->user_email; ?>"
		                                    >
		                                    <span class="mobile-label">Email address<span class="required">*</span></span>
		                                </div>
		                            </div>

		                             <!-- email confirmation -->
		                            <div class="input-group input-group-generic hidden">
		                                <label class="desktop-label col-sm-3 col-md-4" for="email-confirm">Confirm email address<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <input type="text"
		                                        title="Confirm email address"
		                                        id="email_confirmation"
		                                        name="email_confirmation"
		                                        placeholder="Confirm email address"
		                                        class="form-control col-sm-12 col-md-9"
		                                        value="<?= @$user->user_email; ?>"
		                                     >
		                                    <span class="mobile-label">Confirm email address<span class="required">*</span></span>
		                                </div>
		                            </div>

		                            <!-- mobile -->
		                            <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="mobile">Mobile<span class="required">*</span></label>
		                                <div class="col-sm-2">
	                                        <select name="country_code" id="country_code" class="form-control">
	                                            <?php foreach ($country_codes as $c_code): ?>
				                                   <option value="<?=$c_code?>" <?= $user->country_code == $c_code ? 'selected="selected"' : '' ?> ><?=$c_code?></option>
				                                <?php endforeach?>
	                                        </select>
	                                    </div>
		                                <div class="col-sm-4">
		                                    <input type="text"
		                                        title="Mobile"
		                                        id="mobile"
		                                        name="mobile"
		                                        placeholder="Mobile"
		                                        class="form-control col-sm-12 col-md-9"
		                                        value="<?= $user->mobile; ?>"
		                                    >
		                                    <span class="mobile-label">Mobile<span class="required">*</span></span>
		                                </div>
		                            </div>

		                            <!-- phone -->
		                            <!-- <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="phone">Phone number</label>
		                                <div class="col-sm-6">
		                                    <input type="text"
		                                        title="Phone number"
		                                        id="phone"
		                                        name="phone"
		                                        placeholder="Email address"
		                                        class="form-control col-sm-12 col-md-9"
		                                        value="<?= $user->phone; ?>"
		                                    >
		                                    <span class="mobile-label">Phone number<span class="required">*</span></span>
		                                </div>
		                            </div> -->
			                        <span class="title">Newsletter</span>
			                        <div class="input-group input-group-generic">
			                            <div class="checkbox">
			                                <label for="keep-me-informated">
			                                    <input value="1"
			                                        name="newsletter_subscription"
			                                        type="checkbox"
			                                        <?php if ((int)$user->newsletter_subscription == 1): ?>
		                                        	checked="checked"
			                                        <?php endif ?>
			                                        >
			                                     Yes, I would like to receive special offers and information from <i>Nespresso</i> by email.
			                                </label>
			                            </div>
			                        </div>
			                        <div class="account-detail__footer clearfix" style="margin-bottom: 30px; margin-top: 10px;">
				                        <button class="pull-right btn btn-primary  btn-save-info" type="submit" title="Save my informations">Save my information</button>
			                        </div>

		                        </div><!-- .my-infos__form -->
		                    </div><!-- .row -->
		                </div><!-- .account-detail -->
		            </section><!-- .form -->
		        </form>

		        <form method="post" action="<?= $current_url ?>" role="form" id="form-my-infos-password">

		        	<?php wp_nonce_field( 'nespresso_post_nonce', 'nespresso_post_nonce_field' ); ?>

		        	<input type="hidden" name="form-for" value="change-password">

		            <section class="form">
		                <div class="col-xs-12 account-detail my-infos" style="background: #f2f2f2">
		                    <div class="row">
		                        <hr />
		                        <!-- first name -->
		                        <div class="col-xs-12 my-infos__form">
		                            <h2>Change your password</h2>
		                            <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both;"><small> Choose a secure alphanumeric password with at least one uppercase letter and one special character</small></p>
		                            <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="password_current">Current password<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <input type="password" title="Current password" id="password_current" name="password_current" class="form-control col-sm-12 col-md-9">
		                                    <span class="mobile-label">Current password<span class="required">*</span></span>
		                                </div>

		                            </div>

		                            <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="password">New password<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <input type="password" title="New password" id="password" name="password" class="form-control col-sm-12 col-md-9">
		                                    <span class="mobile-label">New password<span class="required">*</span></span>
		                                </div>

		                            </div>
		                            <div class="input-group input-group-generic">
		                                <label class="desktop-label col-sm-3 col-md-4" for="password_confirmation">Confirm new password<span class="required">*</span></label>
		                                <div class="col-sm-6">
		                                    <input type="password" title="Confirm new password" id="password_confirmation" name="password_confirmation" class="form-control col-sm-12 col-md-9">
		                                    <span class="mobile-label">Confirm new password<span class="required">*</span></span>
		                                </div>

		                            </div>

		                        </div>
		                    </div>

		                    <div class="account-detail__footer">
		                        <button class="pull-right btn btn-primary btn-save-info" type="button" id="btn-change-password" title="Change password">Change password</button>
		                    </div>
		                </div>
		            </section>
		        </form>
	        </div><!-- .col-md-9 -->

	    </div><!-- .row -->

	</div><!-- .container -->

</main><!-- .content -->

<?php

global $scripts;
$scripts[] = 'js/components/validation.js';
$scripts[] = 'js/components/my-infos.js';
?>
