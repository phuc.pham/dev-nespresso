<div class="pickup" id="JumpContent" tabindex="-1">
    <header>
    	<div class="row">
        	<div class="col-xs-12">
        		<h2>Select a pickup point</h2>
        	</div>
            <div class="col-xs-2 pickup__header">
                 <div class="pickup__header__target">
                    <img alt="Geolocate me!" src="images/target.png">
                </div>
            </div>
            <div class="col-xs-10">
                <label for="city" class="label-masked">City</label>
                <input type="text" id="city" name="city" placeholder="Enter your city" class="form-control col-xs-9 col-sm-10">
				<button class="btn btn-primary btn-grey" title="search"><i class="fa fa-search"></i></button>
            </div>
         </div>
    </header>
    
    <div class="tab-switcher pickup__switcher clearfix">
            <div class="col-xs-6 no-padding">
                <a href="#pickup-shoplist" title="List" class="pickup__switcher__item active">
                    <div class="outer">
                        <div class="inner">
                            <i class="fa fa-list"></i> List
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xs-6 no-padding">
                <a href="#pickup-map" title="Map" class="pickup__switcher__item">
                    <div class="outer">
                        <div class="inner">
                            <i class="fa fa-map"></i> Map
                        </div>
                    </div>
                </a>
            </div>
     </div>
   
   <div id="pickup-shoplist" class="row pickup__shoplist pickup__panel active">
   		<?php for($i=0; $i<4;$i++){ ?>
        <div class="col-xs-12 col-sm-6 pickup__shop">
            <img src="images/pickup-points/laposte.gif" alt="La Poste" class="img-point" />
            <div class="pickup__shop__info">
              <strong>Office de Poste Petit-Lancy<br /> 2 Les Marbiers</strong>
                <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <p>Chemin Daniel-Ihly 10<br />
                        1213 Petit-Lancy<br />
                        Shipping cost : 0.00 &euro;<br />
                        Distance (Km) : 1.31</p>
                    <a href="#" class="link-btn uppercase"><span class="fa fa-long-arrow-right"></span> See on the map</a>
                </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="pickup__shop__select pull-right">
                            <a href="#" class="btn btn-green btn-icon" title="Select this point"><i class="icon icon-check"></i> Select</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php } ?>
   </div>
   
   <div id="pickup-map" class="pickup__map pickup__panel"><img src="images/pickup-points/map.png" width="335" height="340" alt="Pickup Map"/></div>
                        
</div>