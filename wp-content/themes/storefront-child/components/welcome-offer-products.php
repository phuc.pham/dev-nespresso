<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<!-- content -->
<?php

// Category Banner
$category_banner = get_nespresso_category_banner();

$coffee_desktop_img = isset($category_banner['coffee_category_banner_image_url']) && $category_banner['coffee_category_banner_image_url'] ? $category_banner['coffee_category_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 12/Category Banners/CoffeeList_Desktop.png";
$coffee_mobile_img = isset($category_banner['coffee_category_banner_image_url_mobile']) && $category_banner['coffee_category_banner_image_url_mobile'] ? $category_banner['coffee_category_banner_image_url_mobile'] : get_stylesheet_directory_uri() . "/images/banner/September 12/Category Banners/CoffeeList_Mobile.png";
$coffee_link = isset($category_banner['coffee_category_banner_link']) && $category_banner['coffee_category_banner_link'] ? $category_banner['coffee_category_banner_link'] : "/coffee-list/";

global $scripts;
$scripts[] = 'js/components/welcome-offer.js';

$discover_products = get_discovery_products();
$discover_gifts = get_discovery_gifts();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/discoveryOffersBlock.css">
<main id="main" class="fullPage fullPage__plp coffee-list">
    <!-- banner -->
    <div class="new-banner">
        <!-- web -->
        <a class="web-banner-img" href="<?php echo $coffee_link ?>"><img src="<?php echo $coffee_desktop_img ?>" draggable="false"/></a>
        <!-- mobile -->
        <a class="mobile-banner-img" href="<?php echo $coffee_link ?>"><img src="<?php echo $coffee_mobile_img ?>" draggable="false"/></a>
    </div>

    <!-- end banner -->
    <div id="discovery-offers">
      <div class="wrapper">
        <form id="welcomeOffer" novalidate="novalidate" action="addWelcomeOffer" method="post">
         <div id="discovery-tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div class="h2-like">Which authentic Nespresso coffee experience would you like to discover?</div>

            <!-- Discovery tabs -->
            <ul id="discover-products" class="tabs-items discovery-offers__list ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
            <?php
              $active = 'ui-tabs-active ui-state-active';
              foreach ($discover_products as $key => $product):
              	$product_image = (isset($product->image[0]) && $product->image[0]) ? $product->image[0] : '';
            ?>
                 <li class="pack-0 discovery-offers__list-item ui-state-default ui-corner-top <?= $active ?>" data-tab="<?= $product->post_id ?>" data-name="<?= $product->name ?>" data-img="<?= $product_image ?>" role="tab">
                    <a id="href-pack-0" class="discovery-offers__list-item-link ui-tabs-anchor" role="presentation" tabindex="-1">
                    <?= $product->name ?>
                    </a>
                 </li>
            <?php
              $active = '';
              endforeach; ?>
            </ul>
            <!-- End Discovery tabs -->

            <!-- Discovery Product Details -->
            <div class="discovery-offer discover-pack ui-tabs-panel ui-widget-content ui-corner-bottom">
              <!-- discovery list -->
              <?php
                $active = '';
                foreach ($discover_products as $key => $product):
                  // dd($product);
              ?>
               <div class="main-product <?= $active ?>" id="<?= $product->post_id ?>">
                  <div class="main-img">
                     <img src="<?= $product->image ? $product->image[0] : '/wp-content/themes/storefront-child/images/welcome-offer/10287-CN-MediaMain-217x253.png' ?>" alt="" width="217" height="253">
                  </div>
                  <div class="main-desc">
                     <h3><?= $product->name ?></h3>
                     <?= substr($product->description, 0, 230) . ' ... ';?>
                     <a target="_blank" href="<?= $product->url ?>" class="closed as-link">More details</a>
                </span>
                  </div>
                  <div class="main-details">
                     <div class="main-price">
                        <span class="capsule">
                        150 capsules
                        </span>
                        <span class="price">
                        1 gift
                        </span>
                     </div>
                     <div class="offer-price">
                        <?= get_woocommerce_currency_symbol() . ' ' . number_format($product->price) ?>
                        <span>
                        Tax incl.
                        </span>
                     </div>
                  </div>
               </div>
              <?php
                $active = 'hide';
                endforeach; ?>
               <!-- Start gift -->
               <div class="offer-items">
                <?php if($discover_gifts): ?>
                  <fieldset class="discovery-offer-gifts__fieldset">
                     <legend class="discovery-offer-gifts__title">
                        Select Your Gift
                     </legend>
                     <?php
                     	foreach ($discover_gifts as $key => $gift):
                     	$gift_image = (isset($gift->image[0]) && $gift->image[0]) ? $gift->image[0] : '';
                     ?>
	                     <article class="discovery-offer-gift__block offer-item" data-gift="<?= $gift->post_id ?>">
	                        <span class="item-background">&nbsp;</span>
	                        <div class="free-tips">
	                           Free
	                        </div>
	                        <div class="offer-details-wrapper">
	                           <img src="<?= $gift_image ?>?impolicy=small&amp;imwidth=108" srcset="<?= $gift_image ?>?impolicy=small&amp;imwidth=108 1x, <?= $gift_image ?>?impolicy=small&amp;imwidth=216 2x, <?= $gift_image ?>?impolicy=small&amp;imwidth=324 3x, <?= $gift_image ?>?impolicy=small&amp;imwidth=432 4x" alt="" class="discovery-offer-gift__image" height="108" width="108">
	                           <div class="offer-details-desc-text">
	                              <div class="product-name">
	                                 <?= $gift->name ?>
	                              </div>
	                              <p class="discovery-offer-gift__product-description">
	                                 <?= $gift->description ?>
	                              </p>
	                           </div>
	                           <div class="discovery-offer-gift__selection-container">
	                              <div class="offer-radio">
	                                 <div class="separator">&nbsp;</div>
	                                 <input id="<?= $gift->post_id ?>" class="discovery-offer-gift__input" data-name="<?= $gift->name ?>" data-img="<?= $gift_image ?>" name="productCodes" value="<?= $gift->post_id ?>" type="radio">
	                                 <label for="<?= $gift->post_id ?>" class="radio">
	                                 Select Your Gift
	                                 </label>
	                              </div>
	                           </div>
	                        </div>
	                     </article>
                 	<?php endforeach; ?>
                  </fieldset>
                <?php else: ?>
                  <fieldset class="discovery-offer-gifts__fieldset">
                     <legend class="discovery-offer-gifts__title">
                          No gift available.
                     </legend>
                  </fieldset>
                <?php endif ?>
               </div>
               <!-- End gift -->
            </div>
            <!-- End Discovery Product Details -->

         </div>
         <div class="offer-confirm clearfix">
            <div class="pull-right">
               <span class="more">
               <a id="ta-skip-welcome-offer" href="/my-account">I am not interested for now</a>
               </span>
               <button id="addDiscoveryOfferButton" class="btn button-primary txt-sdw clear-session-cart button-green disabled" name="_eventId_choose" type="button" disabled>
               Add to basket</button>
            </div>
         </div>
        </form>
      </div>
    </div>

</main><!-- #content -->

<!-- /content -->
