<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if(!isset($_SESSION['sv_customer_id']) || !$_SESSION['sv_customer_id'])
{
    header('location: /coffee-list-question');
    exit;
}
$customer_id = $_SESSION['sv_customer_id'];
global $wpdb;
$table = $wpdb->prefix.'coffee_customers';
$cus = $wpdb->get_row($wpdb->prepare( "select * from $table where id = %d" ,$customer_id));
if(!$cus)
{
    header('location: /coffee-list-question');
    exit;
}
$tableq = $wpdb->prefix.'coffee_questions';
$tablea = $wpdb->prefix.'coffee_answers';
$tableqa = $wpdb->prefix.'coffee_question_answer';
$tablers = $wpdb->prefix.'coffee_results';
$tablecrs = $wpdb->prefix.'coffee_customer_results';
$tablecf = $wpdb->prefix.'coffees';
$errors=[];
if(isset($_POST['questions']) && is_array($_POST['questions']) && count($_POST['questions'])==6)
{   
  // $coffee = [];
   $qaids = '';
   $qaidlast = '';
   foreach($_POST['questions'] as $qid=>$ch)
   {
       $d = explode(':',$ch);
       $aid = $d[0];
       $qaids .= $d[1].',';
       if($qid==6)
       {
            $qaidlast = $d[1];
       }
    //    if($qid==1)
    //    {
    //         $coffee = $wpdb->get_col("select id_coffee from $tablers where id_qa = $qaid");
    //    }else{
    //         $coffee = array_intersect($coffee,$wpdb->get_col("select id_coffee from $tablers where id_qa = $qaid"));
    //    }     
   }
   $qaids = rtrim($qaids,',');
   $coffees = $wpdb->get_col("select d.id_coffee
   from (SELECT id_coffee FROM $tablers WHERE id_qa in ($qaids)) d
   group by d.id_coffee
   having count(*) = (select count(*) n
               from (SELECT id_coffee FROM $tablers WHERE id_qa in ($qaids)) d2
               group by d2.id_coffee
              order by n desc
              limit 1
             )");
    if(count($coffees)>1)
    {
        //dd($qaidlast);
        $cflast = $wpdb->get_col("select id_coffee from $tablers where id_qa = $qaidlast");
        $coffees = array_intersect($coffees,$cflast);
        //dd($cflast);
    }
    $coffee = $wpdb->get_row($wpdb->prepare( "select * from $tablecf where id = %d" ,$coffees[0]));
    if($coffee)
    {
        $data = array(
            'id_customer' => $customer_id, 
            'id_coffee' =>$coffee->id,
            'answers' =>json_encode($_POST['questions']),
            'created_at' =>date('Y-m-d H:i:s')
        );
        $format = array('%d','%d','%s','%s');
        $wpdb->insert($tablecrs,$data,$format);
        $rs_id = $wpdb->insert_id;
        if($rs_id)
        {
            $_SESSION['sv_rs_id'] = $rs_id;
            unset($_SESSION['sv_customer_id']);
        }else{
            $errors['alert'] = 'Cannot survey at this time, please try again later';
        }
    }else{
        $errors['alert'] = 'No matches were found for your selections, please try again';
    }
}
?>

<!-- content -->

<main id="content" class="fullPage fullPage__plp coffee-list">
    <div class="container">
        <div class="row">
            <div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
                <div class="productlist-main clearfix">
                    <div class="productlist-panel col-xs-12 col-md-12">
                        <div class="productlist-contents">
                            <?php 
                            if(isset($_SESSION['sv_rs_id'],$coffee))
                            {
                            ?>                   
                            <div class="row text-center">
                                <div class="col-md-12 text-center">
                                    <h1 style="color: #a95b5b;">
                                        DISCOVER A SELECTION MADE FOR YOU!
                                    </h1>
                                </div>

                                <div class=" col-md-12 text-center" >
                                    <img style="width:150px;"  class="center img-result" src="<?=$coffee->image?>" alt="<?=$coffee->name?>" >
                                </div>

                                <div class="col-md-12 text-center">
                                    <h2>
                                        <?=$coffee->name?>
                                    </h2>
                                </div>

                                <div class=" container">
                                        <div class="nrow">
                                            <article class="product-item col-lg-6">
                                                <div class="directory-section">
                                                    <div class="thumb-section">
                                                        <?=$coffee->content?>
                                                    </div>
                                                    
                                                </div>
                                            </article>

                                            <article class="product-item col-lg-6">
                                                <div class="directory-section">
                                                    <div class="thumb-section">
                                                        <h3>Based on your answers, we have identified the Nespresso Grands Crus that best fit your aromatic profile preferences and your drinking habits.</h3>
                                                    </div>
                                                    
                                                </div>
                                            </article>
                                    </div>
            
                                </div>

                              
                                
                            </div>


                            <style>
                            .img-result {
                                width: 450px;
                            
                            }

                            img {
                                display: block;
                                margin-left: auto;
                                margin-right: auto;
                            }

                            .productlist .productlist-main .productlist-panel {
                                    background: unset;
                            }
                            #content.fullPage__plp {
                                background-image: url(http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/BACKGROUND-3.jpg);
                                background-repeat: no-repeat;
                                background-position: center center;
                                background-size: cover;
                            }

                        
                            </style>
                            <?php }else{
                                ?>
                                 <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 style="color: #a95b5b;">
                                    FIND THE COFFEE THAT BEST SUITS YOUR TASTE.
                                    </h1>
                                </div>
								<small  class="text-danger"><?=$errors['alert']??''?></small>
                                <div class="col-md-12 text-center">
                                    <h2>
                                    Discover your own Nespresso experience in a few easy steps.
                                    </h2>
                                </div>
                                <form action="/coffee-list-question-step-2/" method="post">
                                    <div class="text-center" style="margin-bottom: 30px; margin-top: 10px;">
                                        <button class="text-center" style="color: #fff;background: #866b0a;" type="submit" title="Save my informations"> START HERE </button>
                                    </div>
                                </form>
                            </div>
                                <?php
                            } ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main><!-- #content -->

<style>
.thumb-section ul {
    padding-left: 40px;
}

</style>

<!-- /content -->
<?php
	
	ob_end_flush();

?>
