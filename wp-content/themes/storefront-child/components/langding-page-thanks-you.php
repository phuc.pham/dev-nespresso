<?php
	
	if (!defined('ABSPATH'))
	{
		exit; // Exit if accessed directly
	}
	session_start();
	
	$tep = 1;
	
	if($_POST['result_wheel'])
	{
		global $wpdb;
		$wpdb->update('wp_langdingpage2_customers', array('result_wheel'=>$_POST['result_wheel']), array('id' => $_SESSION['langding_page_customer_id'] ));

//	    unset($_SESSION['result_langdingpage']);
//	    unset($_SESSION['customer_langdingpage']);
//	    unset($_SESSION['langding_page_customer_id']);
	}

	if($_POST['step'])
	{
		$tep = 2;
	}
	
	if($tep ==1)
	{
	?>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/luckywheel.scss">
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/Winwheel.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/TweenMax.min.js"></script>
		<main id="content" class="fullPage fullPage__plp coffee-list ">
			<!-- end banner -->
			<div class="container">
				<div class="row">
					<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
						<div class="productlist-main clearfix">
							<div class="productlist-panel col-xs-12 col-md-12">
								<div class="productlist-contents">
									<form action="#" method="post" id="form2thankyou">
										<section class="product-category text-center" >
											<h1 title="PERSONAL INFORMATION"  style="background-color:#986F38" class="btn">WELCOME TO NESPRESSO BUILDING CORNER 2021</h1><br>
											<h1 title="THANK YOU!"  class="btn">THANK YOU!</h1><br>
											<h3 title="Your answers have been recorded. "   class="btn">Your answers have been recorded. </h3><br>
											
											<canvas id="canvas" width="420" height="420"></canvas>
											<br>
											<a class="btn text-center" id="spin_button" onClick="startSpin();">START THE LUCKY WHEEL!</a>
											<input type="hidden" name="step"  value="2" >
										 

										</section>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>



<style>
	#content.fullPage__plp {
		background-image: url(/wp-content/themes/storefront-child/images/langding-page-2/BACKGROUND3.jpg);
		background-size: cover;
		background-position: center center;
		height: 900px;
		padding: 100px 50px;
	}
	.productlist .productlist-main .productlist-panel{
		background: unset;
	}

	.productlist .productlist-main .productlist-panel{
		background: unset;
	}
	
	/* Sets the background image for the wheel */
	td.the_wheel
	{
		background-image: url(/wp-content/themes/storefront-child/images/wheel/wheel_back.png);
		background-position: center;
		background-repeat: no-repeat;
	}1

	/* Do some css reset on selected elements */
	h1, p
	{
		margin: 0;
	}

	div.power_controls
	{
		margin-right:70px;
	}

	div.html5_logo
	{
		margin-left:70px;
	}

	/* Styles for the power selection controls */
	table.power
	{
		background-color: #cccccc;
		cursor: pointer;
		border:1px solid #333333;
	}

	table.power th
	{
		background-color: white;
		cursor: default;
	}

	td.pw1
	{
		background-color: #6fe8f0;
	}

	td.pw2
	{
		background-color: #86ef6f;
	}

	td.pw3
	{
		background-color: #ef6f6f;
	}

	/* Style applied to the spin button once a power has been selected */
	.clickable
	{
		cursor: pointer;
	}

	/* Other misc styles */
	.margin_bottom
	{
		margin-bottom: 5px;
	}


</style>

<script>
	// Create new wheel object specifying the parameters at creation time.
	let theWheel = new Winwheel({
		'numSegments'       : 8,                 // Specify number of segments.
		'outerRadius'       : 200,               // Set outer radius so wheel fits inside the background.
		'drawText'          : true,              // Code drawn text can be used with segment images.
		'textFontSize'      : 16,
		'textOrientation'   : 'curved',
		'responsive'        : true,
		'textAlignment'     : 'inner',
		'textMargin'        : 90,
		'textFontFamily'    : 'monospace',
		'textStrokeStyle'   : 'black',
		'textLineWidth'     : 3,
		'pointerGuide'      :
			[
				{'display'     : true},
				{'strokeStyle' : 'red'},
				{'lineWidth'   : 3}
			],
		'textFillStyle'     : 'white',
		'drawMode'          : 'segmentImage',    // Must be segmentImage to draw wheel using one image per segemnt.
		'segments'          :                    // Define segments including image and text.
			[
				{'image' : '/wp-content/themes/storefront-child/images/wheel/jane.png',  'text' : 'Discount 20%','id_wheel': '1'},
				{'image' : '/wp-content/themes/storefront-child/images/wheel/tom.png',   'text' : 'may pha cafe ','id_wheel': '2'},//1
				{'image' : '/wp-content/themes/storefront-child/images/wheel/mary.png',  'text' : 'FreeShip','id_wheel': '3'},
				{'image' : '/wp-content/themes/storefront-child/images/wheel/alex.png',  'text' : 'May Pha Cafe','id_wheel': '4'},//3
				{'image' : '/wp-content/themes/storefront-child/images/wheel/sarah.png', 'text' : 'Discount 50%','id_wheel': '5'},
				{'image' : '/wp-content/themes/storefront-child/images/wheel/bruce.png', 'text' : 'May Pha Cafe 2','id_wheel': '6'},//5
				{'image' : '/wp-content/themes/storefront-child/images/wheel/rose.png',  'text' : 'Cafe','id_wheel': '7'},//6
				{'image' : '/wp-content/themes/storefront-child/images/wheel/steve.png', 'text' : 'Good Luck Next Time','id_wheel': '8'}
			],
		'animation' :           // Specify the animation to use.
			{
				'type'     : 'spinToStop',
				'duration' : 5,     // Duration in seconds.
				'spins'    : 8,     // Number of complete spins.
				'callbackFinished' : alertPrize
				// 'callbackAfter' : callbackAfter
			}
	});

	// Vars used by the code in this page to do power controls.
	let wheelPower    = 0;
	let wheelSpinning = false;

	// -------------------------------------------------------
	// Function to handle the onClick on the power buttons.
	// -------------------------------------------------------
	function powerSelected(powerLevel)
	{
		// Ensure that power can't be changed while wheel is spinning.
		if (wheelSpinning == false) {
			// Reset all to grey incase this is not the first time the user has selected the power.
			document.getElementById('pw1').className = "";
			document.getElementById('pw2').className = "";
			document.getElementById('pw3').className = "";

			// Now light up all cells below-and-including the one selected by changing the class.
			if (powerLevel >= 1) {
				document.getElementById('pw1').className = "pw1";
			}

			if (powerLevel >= 2) {
				document.getElementById('pw2').className = "pw2";
			}

			if (powerLevel >= 3) {
				document.getElementById('pw3').className = "pw3";
			}

			// Set wheelPower var used when spin button is clicked.
			wheelPower = powerLevel;

			// Light up the spin button by changing it's source image and adding a clickable class to it.
			document.getElementById('spin_button').src = "/wp-content/themes/storefront-child/images/wheel/spin_on.png";
			// document.getElementById('spin_button').className = "clickable";
		}
	}

	// -------------------------------------------------------
	// Click handler for spin button.
	// -------------------------------------------------------
	function startSpin()
	{
		// Ensure that spinning can't be clicked again while already running.
		if (wheelSpinning == false) {
			// Based on the power level selected adjust the number of spins for the wheel, the more times is has
			// to rotate with the duration of the animation the quicker the wheel spins.
			if (wheelPower == 1) {
				theWheel.animation.spins = 3;
			} else if (wheelPower == 2) {
				theWheel.animation.spins = 8;
			} else if (wheelPower == 3) {
				theWheel.animation.spins = 15;
			}

			// Disable the spin button so can't click again while wheel is spinning.
			document.getElementById('spin_button').src       = "/wp-content/themes/storefront-child/images/wheel/spin_off.png";
			document.getElementById('spin_button').className = "";

			// Begin the spin animation by calling startAnimation on the wheel object.
			theWheel.startAnimation();

			// Set to true so that power can't be changed and spin button re-enabled during
			// the current animation. The user will have to reset before spinning again.
			wheelSpinning = false;
		}
	}

	// -------------------------------------------------------
	// Function for reset button.
	// -------------------------------------------------------
	function resetWheel()
	{
		theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
		theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
		theWheel.draw();                // Call draw to render changes to the wheel.

		document.getElementById('pw1').className = "";  // Remove all colours from the power level indicators.
		document.getElementById('pw2').className = "";
		document.getElementById('pw3').className = "";

		wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
	}

	// -------------------------------------------------------
	// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
	// note the indicated segment is passed in as a parmeter as 99% of the time you will want to know this to inform the user of their prize.
	// -------------------------------------------------------s
	function alertPrize(indicatedSegment)
	{
		// Do basic alert of the segment text. You would probably want to do something more interesting with this information.
		// alert(indicatedSegment.text);
		
	   if(indicatedSegment.id_wheel == 2 || indicatedSegment.id_wheel == 4 || indicatedSegment.id_wheel == 6 )
	   {
		   console.log(indicatedSegment.id_wheel);
		   theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
		   theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
		   theWheel.draw();                // Call draw to render changes to the wheel.
		   startSpin();
	   }else
	   {

		   if (confirm(indicatedSegment.text))
		   {
			   alert('Thanks for confirming');
		   }
		   
		   
		   jQuery.ajax({
			   url : "#",
			   type : "post",
			   dataType:"text",
			   data : {
				   result_wheel : indicatedSegment.text,
			   },
			   success : function (result){
				   jQuery('#form2thankyou').submit();

			   }
		   });
		   
		   
		   //alert(indicatedSegment.text);
		   
	   }

	   
	}
	
</script>
<?php
	}
	elseif ($tep ==2)
	{
   ?>
		<main id="content" class="fullPage fullPage__plp coffee-list ">
			<!-- end banner -->
			<div class="container">
				<div class="row">
					<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
						<div class="productlist-main clearfix">
							<div class="productlist-panel col-xs-12 col-md-12">
								<div class="productlist-contents">
									<form action="#" method="post">
										<section class="product-category text-center" >
											<h1 title="PERSONAL INFORMATION"  style="background-color:#986F38" class="btn">WELCOME TO NESPRESSO BUILDING CORNER 2021</h1><br>
											<h1 title="THANK YOU!"  class="btn">THANK YOU!</h1><br>
											<h3 title="Your answers have been recorded. "   class="btn">Your answers have been recorded. </h3><br>

										</section>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>

		<style>
			#content.fullPage__plp {
				background-image: url(/wp-content/themes/storefront-child/images/langding-page-2/BACKGROUND3.jpg);
				background-size: cover;
				background-position: center center;
				height: 900px;
				padding: 100px 50px;
			}
			.productlist .productlist-main .productlist-panel{
				background: unset;
			}

			.productlist .productlist-main .productlist-panel{
				background: unset;
			}


		</style>
  <?php
	}

?>
