<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if(!isset($_SESSION['sv_customer_id']) || !$_SESSION['sv_customer_id'])
{
    header('location: /coffee-list-question');
    exit;
}
$customer_id = $_SESSION['sv_customer_id'];
?>

<!-- content -->

<main id="content" class="fullPage fullPage__plp coffee-list">
    <div class="container">
        <div class="row">
            <div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
                <div class="productlist-main clearfix">
                    <div class="productlist-panel col-xs-12 col-md-12">
                        <div class="productlist-contents">
                        
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h1 style="color: #a95b5b;">
                                    FIND THE COFFEE THAT BEST SUITS YOUR TASTE.
                                    </h1>
                                </div>

                                <div class="col-md-12 text-center">
                                    <h2>
                                    Discover your own Nespresso experience in a few easy steps.
                                    </h2>
                                </div>
                                <form action="/coffee-list-question-step-2/" method="post">
                                    <div class="text-center" style="margin-bottom: 30px; margin-top: 10px;">
                                        <button class="text-center" style="color: #fff;background: #866b0a;" type="submit" title="Save my informations"> START HERE </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main><!-- #content -->

<!-- /content -->
<style>
.productlist .productlist-main .productlist-panel{
    background: unset;
}
#content.fullPage__plp {
    background: url(http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/CÂU-2-ảnh-1.jpg) no-repeat;
    background-size: cover;
    background-position: center center;
    height: 500px;
}

</style>