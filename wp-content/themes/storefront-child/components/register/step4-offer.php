<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>
<div data-step="5" class="checkister step4 hide" id="JumpContent" tabindex="-1">
    <header>
        <h2>WELCOME</h2>
    </header>
    <section>
        <div class="main-content clearfix">
            <h3 class="main-content__title">WELCOME</h3>
            <!-- <form method="POST" action="#"> -->
                <section class="form">
                    <div class="checkister-wrapper clearfix">
                        STEP 4 : WELCOME OFFER
                        <a class="btn btn-primary btn-icon-right pull-right" href="<?php bloginfo('url'); ?>" title="Shop"><i class="icon icon-arrow_left"></i>Shop</a>
                    </div>
                </section>
            <!-- </form> -->
        </div>
    </section>
</div>

