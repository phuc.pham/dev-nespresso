<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
$country_codes = get_country_codes();
?>
<div data-step="1" class="checkister step1 ">
    <header>
        <h2>Personal information</h2>
    </header>

    <section>
        <div class="main-content clearfix">
            <!-- <form method="POST" action="<?php bloginfo('url');?>/delivery-mode"> -->
                <section class="form">
                    <div class="checkister-wrapper clearfix gray-bgcolor" >
                        <p>Please complete the form below. Fields marked with an <span class="required">*</span> are required.</p>

                        <div class="row">
                            <div class="col-md-8">

                                <div class="input-group input-group-generic company-hidden">
                                    <label class="desktop-label col-sm-3 col-md-4" for="title">Title<span class="required">*</span></label>
                                    <div class="col-sm-4">
                                        <div class="dropdown dropdown--input dropdown--init">
                                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                                <span class="mobile-label">Title<span class="required">*</span></span>
                                                <span class="current">Please select</span>
                                            </button>
                                            <?php
$title = isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['title']) ? $_SESSION['registration_data']['title'] : ''
?>
                                            <select tabindex="-1" name="title" id="title" required>
                                                <option value="">Please select</option>
                                                <option value="Mr/Ms" <?=$title == "Mr/Ms" ? 'selected="selected"' : ''?>>Mr/Ms</option>
                                                <option value="Mr" <?=$title == "Mr" ? 'selected="selected"' : ''?>>Mr</option>
                                                <option value="Ms" <?=$title == "Ms" ? 'selected="selected"' : ''?>>Ms</option>
                                            </select>
											<small id="helptitle" data-msgvi="Vui lòng chọn giới tính" data-msg="Please select a Title" class="text-danger"></small>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="first_name">First name<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="First name"
                                            id="first_name"
											required
                                            name="first_name"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['first_name']) ? $_SESSION['registration_data']['first_name'] : '' ?>"
                                        >
										<small id="helpfirst_name" data-msgvi="Vui lòng nhập tên của bạn" data-msg="Please provide a first name" class="text-danger"></small>
                                        <span class="mobile-label">First name<span class="required">*</span></span>
                                    </div>
                                </div>
                                <!-- <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="middle_name">Middle name</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Middle name"
                                            id="middle_name"
                                            name="middle_name"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['middle_name']) ? $_SESSION['registration_data']['middle_name'] : '' ?>"
                                        >
                                        <span class="mobile-label">Middle name</span>
                                    </div>
                                </div> -->

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="last_name">Last name<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Last name"
                                            id="last_name"
											required
                                            name="last_name"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['last_name']) ? $_SESSION['registration_data']['last_name'] : '' ?>"
                                        >
										<small id="helplast_name" data-msgvi="Vui lòng nhập họ của bạn" data-msg="Please provide a last name" class="text-danger"></small>
                                        <span class="mobile-label">Last name<span class="required">*</span></span>
                                    </div>
                                </div>

                                <!-- date of birth -->
                                <div class="input-group input-group-generic">
                                    <label class="custom-label-1 col-sm-3 col-md-4" for="date_of_birth">Date of Birth<span class="required">*</span></label>
                                    <div class="col-sm-8 date-of-birth-container" style="display:flex">
                                        <?php
                                            $date_ex = explode('-', @$_SESSION['registration_data']['date_of_birth']);
                                            $year = isset($date_ex[0]) ? (int) $date_ex[0] : null;
                                            $month = isset($date_ex[1]) ? (int) $date_ex[1] : null;
                                            $day = isset($date_ex[2]) ? (int) $date_ex[2] : null;
                                        ?>
										<div>
											<select name="day" id="day" class="form-control" required>
												<option value="">Select Day</option>
												<?php for ($d = 1; $d < 32; $d++): ?>
													<option value="<?=$d?>" <?=$day == $d ? 'selected="selected"' : ''?>><?=$d?></option>
												<?php endfor;?>
											</select>
											<small id="helpday" data-msgvi="Vui lòng chọn ngày" data-msg="Please select day" class="text-danger"></small>
										</div>
										<div>
											<select name="month" id="month" class="form-control" required>
												<option value="">Select Month</option>
												<?php for ($m = 1; $m < 13; $m++): ?>
													<?php $dateObj = DateTime::createFromFormat('!m', $m);?>
													<option value="<?=$m?>" <?=$month == $m ? 'selected="selected"' : ''?>><?=$dateObj->format('F')?></option>
												<?php endfor;?>
											</select>
											<small id="helpmonth" data-msgvi="Vui lòng chọn tháng" data-msg="Please select month" class="text-danger"></small>
										</div>
										<div>
											<select name="year" id="year" class="form-control" required>
												<option value="">Select Year</option>
												<?php for ($y = date('Y')-15; $y > (date('Y') - 100); $y--): ?>
													<option value="<?=$y?>" <?=$year == $y ? 'selected="selected"' : ''?> ><?=$y?></option>
												<?php endfor;?>
											</select>
											<small id="helpyear" data-msgvi="Vui lòng chọn năm" data-msg="Please select year" class="text-danger"></small>
										</div>
                                        <input type="hidden"
                                            title="Date of Birth"
                                            id="date_of_birth"
                                            name="date_of_birth"
                                            value="<?=@$_SESSION['registration_data']['date_of_birth'];?>"
                                        >
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="email">Email address<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <?php
$registering_email = isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['email']) ? $_SESSION['registration_data']['email'] : ''
?>
                                        <input type="text"
                                            title="Email address"
                                            id="email"
											data-regix="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$"
											required
                                            name="email"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?=$registering_email?>"
                                        >
										<small id="helpemail" data-msgvi="Vui lòng đúng nhập email của bạn (e.g: customer@nespresso.vn)" data-msg="Please provide a valid email address (example: customer@nespresso.vn)" class="text-danger"></small>
                                        <input type="hidden" name="user_email" value="<?=$registering_email?>">
                                        <input type="hidden" name="user_login" value="<?=$registering_email?>">
                                        <span class="mobile-label">Email address<span class="required">*</span></span>
                                    </div>
                                </div>

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="mobile">Mobile<span class="required">*</span></label>
                                    <div class="col-sm-2">
                                        <select name="country_code" id="country_code" class="form-control">
                                            <?php foreach ($country_codes as $c_code): ?>
                                               <option value="<?=$c_code?>"><?=$c_code?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text"
                                            title="Mobile"
											data-regix="^(086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$"
                                            id="mobile"
											required
                                            name="mobile"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['last_name']) ? $_SESSION['registration_data']['mobile'] : '' ?>"
                                        >
										<small id="helpmobile" data-msgvi="Vui lòng nhập đúng số điện thoại của bạn (e.g. 0987654321)" data-msg="Please enter a valid phone number (e.g. 0987654321)" class="text-danger"></small>
                                        <span class="mobile-label">Mobile<span class="required">*</span></span>
                                        <p style="margin-top: 0px; margin-bottom: 0px; display: block; line-height: 1em; clear: both; margin-left: -105px;"><small>Should be numeric, start with '0' and with 9 characters minimum</small></p>
                                        <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both; margin-left: -105px;"><small>Example: 0912345678</small></p>
                                    </div>
                                </div>

                                <!-- <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="phone">Phone</label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                            title="Phone"
                                            id="phone"
                                            name="phone"
                                            class="form-control col-sm-12 col-md-9"
                                            value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['last_name']) ? $_SESSION['registration_data']['phone'] : '' ?>"
                                        >
                                        <span class="mobile-label">Phone</span>
                                    </div>
                                </div> -->

                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="password">Password<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="password"
                                            title="Password"
                                            id="password"
											required
											data-regix="^(?=(.*[a-z]){1,})(?=(.*[A-Z]){1,})(?=(.*[\d]){1,})(?=(.*[\!\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\`\{\|\}\~]){1,})(?!.*\s).{6,30}$"
                                            name="password"
                                            class="form-control col-sm-12 col-md-9"
                                        >
										<small id="helppassword" data-msgvi="Mật khẩu phải có: <br>- Ký tự hoa<br>- Ký tự thường<br>- Chữ số<br>- Ký tự đặc biệt: !#$%&'()*+,-./:;<=>?@[]^_`{|}~<br>- Ít nhất 6 ký tự"  data-msg="Please enter a valid password. Must contain a combination of:<br>- Uppercase<br>- Lowercase<br>- Number<br>- Special characters: !#$%&'()*+,-./:;<=>?@[]^_`{|}~<br>- Min length 6 characters" class="text-danger"></small>
                                        <span class="mobile-label">Password<span class="required">*</span></span>
                                        <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both;"><small> Choose a secure alphanumeric password with at least one uppercase letter and one special character</small></p>
                                    </div>
                                </div>
                                <div class="input-group input-group-generic">
                                    <label class="desktop-label col-sm-3 col-md-4" for="password_confirmation">Confirm password<span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="password" required data-match="password" title="Confirm password" id="password_confirmation" name="password_confirmation" class="form-control col-sm-12 col-md-9">
										<small id="helppassword_confirmation" data-msgvi="Mật khẩu xác nhận không khớp" data-msg="Passwords are not matching" class="text-danger"></small>
                                        <span class="mobile-label">Confirm password<span class="required">*</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="my-expresscheckout-step">
                            <div class="my-expresscheckout-step__header">
                                <a class="my-expresscheckout-step__link toggler" href="#" title="Choose the first step : shipping mode">
                                    <h3 class="my-expresscheckout-step__title">Are you already a Club Member in the Vietnam?</h3>
                                </a>
                            </div>
                            <div class="my-expresscheckout-step__content step-toggle active">

                                <div class="row">
                                    <div class="radio col-sm-12">
                                        <label for="has_club_membership_no">
                                            <input type="radio"
                                                name="has_club_membership_no"
                                                id="member-no"
                                                value="0"
                                                <?php echo !isset($_SESSION['registration_data']) || !isset($_SESSION['registration_data']['has_club_membership_no']) || $_SESSION['registration_data']['has_club_membership_no'] == '0' ? 'checked="checked"' : '' ?>
                                            >
                                            <span>No, I do not have a Vietnam <i>Nespresso</i> Club Membership number</span>
                                        </label>
                                    </div>
                                    <div class="radio col-sm-12">
                                        <label for="has_club_membership_no">
                                            <input type="radio"
                                                name="has_club_membership_no"
                                                id="member-yes"
                                                value="1"
                                                <?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['has_club_membership_no']) && $_SESSION['registration_data']['has_club_membership_no'] == '1' ? 'checked="checked"' : '' ?>
                                            >
                                            <span>Yes, I do have a Vietnam <i>Nespresso</i> Club Membership number</span>
                                        </label>
                                    </div>
                                    <div class="input-group input-group-generic col-md-8">
                                        <label class="desktop-label col-sm-3 col-md-4" for="club_membership_no">Club Membership Number</label>
                                        <div class="col-sm-6">
	                                        <input type="text"
                                                title="Club Membership Number"
                                                id="club_membership_no"
                                                name="club_membership_no"
                                                class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                                value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['club_membership_no']) ? $_SESSION['registration_data']['club_membership_no'] : '' ?>"
                                            >
	                                        <span class="mobile-label">Club Membership Number</span>
                                        </div>
                                    </div>
                                    <div class="input-group input-group-generic col-md-8">
                                        <label class="desktop-label col-sm-3 col-md-4" for="postal_code">My postal code</label>
                                        <div class="col-sm-6">
	                                        <input type="text"
                                                title="My postal code"
                                                id="postal_code"
                                                name="postal_code"
                                                class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                                                value="<?php echo isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['postal_code']) ? $_SESSION['registration_data']['postal_code'] : '' ?>"
                                            >
	                                        <span class="mobile-label">My postal code</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <p style="margin-top: 20px;">By clicking on “continue” I consent to the processing of my data and agree with <i>Nespresso</i> <a href="<?php bloginfo('url');?>/legal?q=privacy-policy" class="link" target="_blank">Privacy Policy</a></p>
                        <button type="button" class="btn btn-primary btn-icon-right pull-right btn-next-step"
                            href="javascript:void(0)"
                            title="Continue"
                            data-step="1"
                        ><i class="icon icon-arrow_left"></i>Continue</button>
                    </div>
                </section>
            <!-- </form> -->
        </div>
    </section>
</div>

