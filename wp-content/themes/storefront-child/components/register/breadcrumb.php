<?php
$steps = [
    'Your personal information',
    'Your address',
    'Your machine',
    'Welcome to Nespresso'
];

$stepActive = 1; // by default
?>

<div class="wizard wizard--register">
    <ol class="steps">

        <?php foreach($steps as $step => $title): ?>

            <li data-step="<?= $step+1 ?>"
                class="step <?= $stepActive == $step+1 ? 'active' : ($stepActive > $step+1 ? 'completed' : null) ?>"
            >
                <div class="step-content btn-next-step">
                    <strong><?= $step+1 ?></strong>
                    <span><?= $title ?></span>
                </div>
            </li>

        <?php endforeach; ?>

    </ol>
</div>
