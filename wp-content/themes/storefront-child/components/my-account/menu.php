<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $wpm;
$current_url = home_url(add_query_arg(array(), $wp->request));

$user = wp_get_current_user();

global $my_account_url;

?>

<div class="account-sidebar__title hidden-xs hidden-sm">My account</div>
<div class="account-sidebar__subtitle hidden-xs hidden-sm">
    <p>
        <span class="weight-normal">Welcome</span>&nbsp;<strong><?=$user->first_name?> <?=$user->last_name?></strong>
    </p>
    <p>
        <!-- <span class="weight-normal">Membership number:&nbsp;<strong><?=$user->club_membership_no?></strong></span> -->
    </p>
</div>

<nav class="account-sidebar__list">
    <ul class="hidden-xs hidden-sm">
        <li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>my-order-history"
                title="My orders"
                <?=nespresso_is_current_url('order-history') || nespresso_is_current_url('view-order') ? 'class="active"' : ''?>
            >
                <i class="icon icon-Basket_off account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt" style="font-size: 11px;">My order history</span>
            </a>
        </li>
        <!--li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>my-easyorders"
                title="My Easyorders"
                <?=nespresso_is_current_url('my-easyorders') ? 'class="active"' : ''?>
            >
                <i class="icon icon-easy-order account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt">My Easyorders</span>
            </a>
        </li-->
        <li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>my-addresses"
                title="My adresses"
                <?=nespresso_is_current_url('my-addresses') ? 'class="active"' : ''?>
            >
                <i class="icon icon-My_address account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt" style="font-size: 11px;">My addresses</span>
            </a>
        </li>
        <li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>personal-information"
                title="My personal information"
                <?=nespresso_is_current_url('personal-information') ? 'class="active"' : ''?>
            >
                <i class="icon icon-My_information_off account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt" style="font-size: 11px;">My personal information</span>
            </a>
        </li>
        <!--li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>my-express-checkout"
                title="Express checkout"
                <?=nespresso_is_current_url('my-express-checkout') ? 'class="active"' : ''?>
            >
                <i class="icon icon-Privacy_policy_2_off account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt">Express checkout</span>
            </a>
        </li-->
        <li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>my-machines"
                title="My machines"
                <?=nespresso_is_current_url('my-machines') ? 'class="active"' : ''?>
            >
                <i class="icon icon-Machines_off account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt" style="font-size: 11px;">My machines</span>
            </a>
        </li>
        <!--li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>alerts-and-subscriptions"
                title="Alerts and subscriptions"
                <?=nespresso_is_current_url('alerts-subscriptions') ? 'class="active"' : ''?>
            >
                <i class="icon icon-Alerts_subscriptions_off account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt">Alerts and subscriptions</span>
            </a>
        </li>
        <li class="account-sidebar__item account-sidebar-item">
            <a href="<?=$my_account_url?>my-contact-preferences"
                title="Contact Preferences"
                <?=nespresso_is_current_url('my-contact-preferences') ? 'class="active"' : ''?>
            >
                <i class="icon icon-Alerts_subscriptions_off account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt">Contact Preferences</span>
            </a>
        </li-->
        <li class="account-sidebar__item account-sidebar-item">
            <a href="<?=wp_logout_url('login')?>" title="Logout">
                <i class="fa fa-sign-out account-sidebar-item__icon"></i>
                <span class="account-sidebar-item__txt" style="font-size: 11px;">Logout</span>
            </a>
        </li>
    </ul>
    <div class="visible-xs visible-sm">
        <form action="/" method="GET">
            <label for="account-menu" class="hidden">Menu</label>
            <div class="dropdown">
                <button class="btn btn-primary btn-icon"><i class="fa fa-angle-down"></i>My account menu</button>
                <select class="select__element" id="account-menu" name="account-menu" onChange="window.document.location.href=this.options[this.selectedIndex].value;">
                    <option value="<?=$my_account_url?>my-order-history" <?=nespresso_is_current_url('order-history') || nespresso_is_current_url('view-order') ? 'selected="selected"' : ''?> >My orders</option>
                    <!--option value="easyorder.php" <?php if (nespresso_is_current_url('easyorder.php')) {echo 'selected="selected"';}?>>My Easyorders</option-->
                    <option value="<?=$my_account_url?>my-addresses" <?php if (nespresso_is_current_url('my-addresses')) {echo 'selected="selected"';}?>>My adresses</option>
                    <option value="<?=$my_account_url?>personal-information" <?php if (nespresso_is_current_url('personal-information.php')) {echo 'selected="selected"';}?>>My personal information</option>
                    <!--option value="express-checkout.php" <?php if (nespresso_is_current_url('express-checkout.php')) {echo 'selected="selected"';}?>>Express checkout</option-->
                    <option value="<?=$my_account_url?>my-machines" <?php if (nespresso_is_current_url('my-machines')) {echo 'selected="selected"';}?>>My machines</option>
                    <!--option value="alerts-subscriptions.php" <?php if (nespresso_is_current_url('alerts-subscriptions.php')) {echo 'selected="selected"';}?>>Alerts and subscriptions</option>
                    <option value="contact-preferences.php" <?php if (nespresso_is_current_url('contact-preferences.php')) {echo 'selected="selected"';}?>>Contact Preferences</option-->
                    <option value="<?=wp_logout_url('login')?>">Logout</option>
                </select>
            </div>
            <input type="submit" class="hidden" value="Valider" name="submit" />
        </form>
    </div>
</nav>
