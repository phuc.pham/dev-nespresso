<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * Template Name: Personal Information 1
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php get_header(); ?>

<!-- content -->

<main id="content" class="shopping-bag">
	<div class="container">
	    <header>

	        <?php
		        $stepActive = 1;
		        $nameStep = 'Personal Information';
		        $nameStepUrl = 'personal-information';
	        ?>

	        <?php get_template_part('components/wizard'); ?>

	    </header>
	    <div class="checkister step1" id="JumpContent" tabindex="-1">
	        <header>
	            <h2>Personal information</h2>
	        </header>

	        <section class="row">
	            <div class="hidden-xs hidden-sm col-md-4 pull-right">
	                <aside>
	                    <span class="col-title">Order details</span>
	                    <div class="sbag-small sbag-loading"></div>
	                    <ul class="bloc-total hidden">
	                        <li class="bloc-total-stotal">
	                            <span class="stotal">Subtotal</span>
	                            <span class="price">$<span class="price-int">50.40</span></span>
	                        </li>
	                        <li class="bloc-total-vat">
	                            <span class="vat">Vat (inCL.)</span>
	                            <span class="price">$<span class="price-int">5.04</span></span>
	                        </li>
	                        <li class="bloc-total-total">
	                            <span class="total">Total</span>
	                            <span class="tprice">$<span class="price-int">55.44</span></span>
	                        </li>
	                    </ul>
		                <a href="<?php bloginfo('url'); ?>/shopping-bag" title="Modify" class="btn btn-primary btn-block">Edit</a>
	                </aside>
	            </div>
	            <div class="col-xs-12 col-md-8">
	                <div class="main-content clearfix">
	                    <form method="POST" action="delivery-mode">
	                        <section class="form" style="background: #F9F9F9;">
	                            <div class="checkister-wrapper clearfix">
	                                <p>Please fill the form below, Fields marked with * are required.</p>

	                                <div class="input-group input-group-generic">
	                                    <label class="desktop-label col-sm-3 col-md-4" for="email">Email address<span class="required">*</span></label>
	                                    <div class="col-sm-6">
	                                        <input type="text" title="Email address" id="email" name="email" placeholder="Email address" class="form-control col-sm-12 col-md-9">
	                                        <span class="mobile-label">Email address</span>
	                                    </div>
	                                </div>
	                                <div class="input-group input-group-generic">
	                                    <label class="desktop-label col-sm-3 col-md-4" for="password">Password<span class="required">*</span></label>
	                                    <div class="col-sm-6">
	                                        <input type="password" title="Password" id="password" name="password" placeholder="Password" class="form-control col-sm-12 col-md-9">
	                                        <span class="mobile-label">Password</span>
	                                    </div>
	                                </div>
	                                <div class="input-group input-group-generic">
	                                    <label class="desktop-label col-sm-3 col-md-4" for="confirm-password">Confirm password<span class="required">*</span></label>
	                                    <div class="col-sm-6">
	                                        <input type="password" title="Confirm password" id="confirm-password" name="confirm-password" placeholder="Confirm password" class="form-control col-sm-12 col-md-9">
	                                        <span class="mobile-label">Confirm password</span>
	                                    </div>
	                                </div>
	                                <div class="input-group input-group-generic">
	                                    <div class="col-xs-12 col-sm-6 col-sm-offset-4 checkbox-group">
	                                        <div class="checkbox">
	                                            <label for="remember">
	                                                <input value="none" id="remember" name="remember" type="checkbox">
	                                                Remember me
	                                            </label>
	                                        </div>
	                                    </div>
	                                </div>
		                            <div class="my-expresscheckout-step">
			                            <div class="my-expresscheckout-step__header">
				                            <a class="my-expresscheckout-step__link toggler" href="#" title="Choose the first step : shipping mode">
					                            <h3 class="my-expresscheckout-step__title">Are you already a club member ?</h3>
					                            <i class="fa fa-angle-down"></i>
				                            </a>
			                            </div>
			                            <div class="my-expresscheckout-step__content step-toggle">

				                            <div class="row">
					                            <div class="radio col-sm-12">
						                            <label for="member-no">
							                            <input type="radio" name="payment_method" id="member-no" value="giftCard">
							                            <span>No, I do not have a <i>Nespresso</i> Club Membership number</span>
						                            </label>
					                            </div>
					                            <div class="radio col-sm-12">
						                            <label for="member-yes">
							                            <input type="radio" name="payment_method" id="member-yes" value="giftCard">
							                            <span>Yes, I do have a <i>Nespresso</i> Club Membership number</span>
						                            </label>
					                            </div>
					                            <div class="input-group input-group-generic col-xs-12">
						                            <label class="desktop-label col-sm-3 col-md-4" for="membership-adress">Club membership number<span class="required">*</span></label>
						                            <div class="col-sm-6">
							                            <input type="text" title="Club membership number" id="membership-adress" name="membership-adress" placeholder="Ex: 2109749284" class="form-control col-sm-12 col-md-9 form-control--withPlaceholder">
							                            <span class="mobile-label">Club membership number</span>
						                            </div>
					                            </div>
					                            <div class="input-group input-group-generic col-xs-12">
						                            <label class="desktop-label col-sm-3 col-md-4" for="membership-password">My postal code<span class="required">*</span></label>
						                            <div class="col-sm-6">
							                            <input type="text" title="My postal code" id="membership-password" name="membership-password" placeholder="Ex: 1221" class="form-control col-sm-12 col-md-9 form-control--withPlaceholder">
							                            <span class="mobile-label">My postal code</span>
						                            </div>
					                            </div>
				                            </div>
				                            <div class="clearfix">
					                            <a href="#" class="link link--right" title="Help me find my number">Help me find my number</a>
				                            </div>
			                            </div>
		                            </div>
	                            </div>
		                        <div class="link-wrapper">
			                        <p>By clicking on “continue” I consent to the processing of my data and agree with <i>Nespresso</i> <a href="#">Privacy Notice</a></p>
		                            <a class="btn btn-primary btn-icon-right pull-right" href="<?php bloginfo('url'); ?>/delivery-address" title="Continue"><span>Continue</span> <i class="icon icon-arrow_left"></i></a>
		                        </div>
	                        </section>
	                    </form>
	                </div>
	            </div>
	        </section>
	    </div>
    </div>
</main>
<?php
global $scripts;
$scripts[]='js/components/shopping-bag-small.js';
$scripts[]='js/components/my-expresscheckout.js';
?>


<!-- /content -->

<!-- get_footer -->
<?php get_footer(); ?>
