<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

?>

<!-- content -->

<main id="content" class="pd-top-20 pd-bottom-20 mr-top-20 mr-bottom-20">
	<div class="pd-top-20 pd-bottom-20 mr-top-20 mr-bottom-20 container">
		<div class="pd-top-20 pd-bottom-20 mr-top-20 mr-bottom-20 row">
			<h1 class="text-gray text-center pd-top-20 pd-bottom-20 mr-top-20 mr-bottom-20">COMING SOON</h1>
		</div>
    </div>
</main>

<!-- /content -->
