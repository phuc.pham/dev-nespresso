<?php
	ob_start();
	session_start();
	
	if (!defined('ABSPATH'))
	{
		exit; // Exit if accessed directly
	}
	global $wp;
	global $wpdb;
	$table = 'wp_langdingpage2_question';
	$questions = $wpdb->get_results(  "SELECT `id`,`question`,`type_question`,`name` FROM $table  LIMIT 2,3" );
	
	if (!isset($_SESSION['result_customer_langdingpage_2']))
	{
		ob_clean();
		$url = get_home_url() . '/langding-page-2/';
		wp_redirect($url);
		exit();
	}
	
	if($_POST)
    {
		$result = "";

		foreach ($_POST as $key => $value)
		{
			$result .= '"'. $key .'":"'.$value.'",';
		}
		$result = rtrim($result,',');
		$result = '{'. $result .'}';

		$_SESSION['result_customer_langdingpage_2'] .= $result;
		
	}
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/langdingpage2.css">
<!-- content -->

<main id="content" class="fullPage fullPage__plp coffee-list ">
	<!-- end banner -->
	<div class="container">
		<div class="row">
			<div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
				<div class="productlist-main clearfix">
					<div class="productlist-panel col-xs-12 col-md-12">
						<div class="productlist-contents">
							<section class="product-category text-center" >
								<a title="PERSONAL INFORMATION"  style="background-color:#986F38" class="btn">CONSUMPTION HABIT</a>
								<?php
									
									foreach ($questions as $question)
									{
										if($question->type_question == 1)
										{
										 
											?>
											<div class="card" style="margin: 20px 0px ">
												<div class="row">
                                                    <div class="col-md-6">
														<h1 class="input-group-text" ><?= $question->question ?></h1>
													</div>
													<div class="col-md-6">
														<input type="text" style="width: 100%" class="form-control <?= $question->question ?>" name="<?= $question->question ?>" id="<?= $question->question ?>"  placeholder="Insert your answer here… ...."  required="">
														<small id="yourcompany" data-msgvi="Vui lòng nhập tên công ty của bạn " data-msg="Insert your answer here…" class="text-danger"></small>
													</div>
												
												
												</div>
											</div>
											<?php
										}
										else
										{
											?>
                                                <div class="row">
                                                    <div class="col-md-6">
												<h1 class="input-group-text" ><?= $question->question ?></h1>
											</div>
                                                    <div class="col-md-6">
												
												<?php
													global $wpdb;
													$table_answer = 'wp_langdingpage2_answer';
													//
													$answer = $wpdb->get_row(  $wpdb->prepare(
														"SELECT `id`,`answer`,`class` FROM $table_answer as a WHERE a.id_question = %d",
														$question->id
													));
													$i = 1;
													
													foreach (explode(',',$answer->answer) as $key => $ite)
													{
														?>
														<div class="custom-control custom-radio <?= $answer->class ?>" >
															<input type="radio" id="<?= $question->name. $i ?>" name="<?= $question->name ?>" value="<?= $ite ?>" class="custom-control-input <?= $question->name ?>">
															<label class="custom-control-label" for="<?= $question->name. $i ?>"><?= $ite ?></label>
														</div>
														<?php
                                                        $i++;
													}
												
												?>
											</div></div>
											<?php
										}
									}
								?>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</main>
<script>
    function statusChange(con1,con2,con3,question03,question04,question05)
    {
        if(con1 == true && con2 == true && con3 == true)
        {
            jQuery.ajax({
                url : "#",
                type : "post",
                dataType:"text",
                data : {
                    question03 : question03,
                    question04 : question04,
                    question05 : question05
                },
                success : function (result){
                    window.location.href = "/langding_page_survey_2/";
                }
            });
        }

    };

    jQuery(document).ready(function($)
    {
        let con1 = false;
        let question03 = "";
        let con2 = false;
        let question04 = "";
        let con3 = false;
        let question05 = "";

        $('.question03').click(function() {
            if ($(this).is(':checked')) {
                con1= true;
                question03 = $(this).val();
                console.log(question03);
                statusChange(con1,con2,con3,question03,question04,question05);
            }
        })

        $('.question04').click(function(){
            if ($(this).is(':checked')) {
                con2= true;
                question04 = $(this).val();
                console.log(question04);
                statusChange(con1,con2,con3,question03,question04,question05);
            }
        });

        $('.question05').click(function(){
            if ($(this).is(':checked')) {
                con3= true;
                question05 = $(this).val();
                console.log(question05);
                statusChange(con1,con2,con3,question03,question04,question05);
            }
        });

    });
</script>
<style>
    #content.fullPage__plp {
        background-image: url('/wp-content/themes/storefront-child/images/langding-page-2/BACKGROUND3.jpg');
        background-size: cover;
        background-position: center center;
        height: 700px;

    }

    .productlist .productlist-main .productlist-panel{
        background: unset;
    }

    .custom-control.custom-radio {
        margin: 30px 0px 10px -120px;
        display: inline-table;
        width: calc(50% - 20px);
    }
    
    /*input[type="checkbox"], input[type="radio"] {*/
    /*    display: none;*/
    /*}*/

    label.custom-control-label {
        font-size: 1.5rem;
        border: 2px #986F38 solid;
        padding: 10px;
        margin: 0px 0px 0px 25px !important;
    }
    .custom-control.custom-radio.question4 {
        display: grid;
        position: relative;
        width: 100%;
        margin: 20px;
    }
    .questionyesno .custom-control-label {
        display: inline-grid;
        width: 50%;
        margin: 0px !important;
    }


</style>