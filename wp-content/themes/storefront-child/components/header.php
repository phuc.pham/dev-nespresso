<?php
/**
 * Nespresso Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

discount_validation();

$myaccount_page_id = get_option('woocommerce_myaccount_page_id');

$myaccount_page_url = get_permalink($myaccount_page_id);

//Navigation Banner
$navigation_banner = get_nespresso_navigation_banner();
//Coffee Nav
$coffee_nav_banner = isset($navigation_banner['coffee_navigation_banner_image_url']) && $navigation_banner['coffee_navigation_banner_image_url'] ? $navigation_banner['coffee_navigation_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 12/SubNav Banners//Coffee.png";
$coffee_nav_link = isset($navigation_banner['coffee_navigation_banner_link']) && $navigation_banner['coffee_navigation_banner_link'] ? $navigation_banner['coffee_navigation_banner_link'] : "/coffee-list/";
$coffee_nav_title = isset($navigation_banner['coffee_navigation_banner_title']) && $navigation_banner['coffee_navigation_banner_title'] ? $navigation_banner['coffee_navigation_banner_title'] : "Coffee";

//Machine Nav
$machine_nav_banner = isset($navigation_banner['machine_navigation_banner_image_url']) && $navigation_banner['machine_navigation_banner_image_url'] ? $navigation_banner['machine_navigation_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 12/SubNav Banners//Machine.png";
$machine_nav_link = isset($navigation_banner['machine_navigation_banner_link']) && $navigation_banner['machine_navigation_banner_link'] ? $navigation_banner['machine_navigation_banner_link'] : "/machine-list/";
$machine_nav_title = isset($navigation_banner['machine_navigation_banner_title']) && $navigation_banner['machine_navigation_banner_title'] ? $navigation_banner['machine_navigation_banner_title'] : "Machine";

//Accessory Nav
$accessory_nav_banner = isset($navigation_banner['accessory_navigation_banner_image_url']) && $navigation_banner['accessory_navigation_banner_image_url'] ? $navigation_banner['accessory_navigation_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 25/Accessories.png";
$accessory_nav_link = isset($navigation_banner['accessory_navigation_banner_link']) && $navigation_banner['accessory_navigation_banner_link'] ? $navigation_banner['accessory_navigation_banner_link'] : "/accessory-list/";
$accessory_nav_title = isset($navigation_banner['accessory_navigation_banner_title']) && $navigation_banner['accessory_navigation_banner_title'] ? $navigation_banner['accessory_navigation_banner_title'] : "Accessory";

//Our Services Nav
$our_services_nav_banner = isset($navigation_banner['our_services_navigation_banner_image_url']) && $navigation_banner['our_services_navigation_banner_image_url'] ? $navigation_banner['our_services_navigation_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 25/Technical Services.png";
$our_services_nav_link = isset($navigation_banner['our_services_navigation_banner_link']) && $navigation_banner['our_services_navigation_banner_link'] ? $navigation_banner['our_services_navigation_banner_link'] : "/services/";
$our_services_nav_title = isset($navigation_banner['our_services_navigation_banner_title']) && $navigation_banner['our_services_navigation_banner_title'] ? $navigation_banner['our_services_navigation_banner_title'] : "Our Services";

//Coffee Nav
$contact_us_nav_banner = isset($navigation_banner['contact_us_navigation_banner_image_url']) && $navigation_banner['contact_us_navigation_banner_image_url'] ? $navigation_banner['contact_us_navigation_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 12/SubNav Banners/Contact Us.png";
$contact_us_nav_link = isset($navigation_banner['contact_us_navigation_banner_link']) && $navigation_banner['contact_us_navigation_banner_link'] ? $navigation_banner['contact_us_navigation_banner_link'] : "/faq/";
$contact_us_nav_title = isset($navigation_banner['contact_us_navigation_banner_title']) && $navigation_banner['contact_us_navigation_banner_title'] ? $navigation_banner['contact_us_navigation_banner_title'] : "Contact Us";

// Category Banner
$category_banner = get_nespresso_category_banner();

$advisory_desktop_img = isset($category_banner['advisory_banner_image_url']) && $category_banner['advisory_banner_image_url'] ? $category_banner['advisory_banner_image_url'] : "";
$advisory_mobile_img = isset($category_banner['advisory_banner_image_url_mobile']) && $category_banner['advisory_banner_image_url_mobile'] ? $category_banner['advisory_banner_image_url_mobile'] : "";
$advisory_status = isset($category_banner['advisory_banner_status']) && $category_banner['advisory_banner_status'] ? $category_banner['advisory_banner_status'] : "";
$advisory_callback = isset($category_banner['advisory_banner_callback']) && $category_banner['advisory_banner_callback'] ? $category_banner['advisory_banner_callback'] : "";

// slider pop up
$slider_pop_up = get_nespresso_slider_pop_up();

?>

<?php wc_print_notices();?>

<div class="header-container">
    <header id="header">
        <div class="header__fixelts">
		<?php if(get_option( 'free_enable', false )=='yes'){ ?>
			<div style="background: <?php echo get_option( 'free_bgcolor', '#A95B5B' ); ?>;text-transform: uppercase;
    color: <?php echo get_option( 'free_color', '#fff' ); ?>;
    display: block;
    border: 1px solid;
    clear: both;
	text-align: center;padding: 10px;margin:0;
    font-size: 13px;
    position: relative;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="" data-src="https://www.nespresso.com/shared_res/agility/commons/img/services2018/express-delivery.svg" aria-hidden="true" style="
    stroke-width: 4px;
    margin-bottom: -1rem;
    width: 40px;
    height: 40px;
    display: inline-block;
    fill: none;
    stroke: currentColor;
"><g class="icon-stroke" stroke-linejoin="bevel"><path d="M32,64h-5l0-35h44.3c4,0,7.6,2.3,9.3,5.9l3.3,8.9l12,4.3V64h-8"></path><path d="M44.4,64.4c0,3.4-2.8,6.1-6.1,6.1c-3.4,0-6.1-2.8-6.1-6.1c0-3.4,2.7-6.1,6.1-6.1C41.7,58.3,44.4,61,44.4,64.4z"></path><path d="M88,64.4v0.2c0,2.3-1.3,4.5-3.5,5.4c-5.5,2.3-10.7-2.9-8.2-8.4c0.5-1.2,1.5-2.2,2.8-2.7C83.6,56.9,88,60.1,88,64.4z"></path><path d="M58,33v11h19.9l-2.4-6.8c-0.8-2.5-3.2-4.2-5.9-4.2H58z"></path><line x1="76" y1="64" x2="44" y2="64"></line><line x1="24" y1="36" x2="3" y2="36"></line><line x1="24" y1="45" x2="10" y2="45"></line><line x1="24" y1="55" x2="18" y2="55"></line></g></svg> <?php echo get_option( 'free_msg', true ); ?> <font color="yellow"><?php echo get_option( 'free_subtotal', '1,500,000 đ' ); ?></font>
 <a href="<?php echo get_option( 'free_btnlink', '/coffee-list' ); ?>" style="    color: #fff;
    border: 1px solid;
    padding: 5px;
    border-radius: 5px;    white-space: nowrap;
    margin-left: 20px;
"><?php echo get_option( 'free_btn', 'Order Coffee' ); ?></a></div>
		<?php } ?>
            <div class="container">
                <div class="row bg-m-menu">

                    <div class="site-branding">
                        <h1>
                            <a href="/" title="Nespresso Home Page">Nespresso</a>
                        </h1>
                         <h2 class="mobile-menu">
                            <button class="bars"><span>Menu</span></button>
                        </h2>
                    </div>

                    <div class="site-control">
                        <div class="site-control-wrapper pull-left">
                           <!--  <h2 class="mobile-menu">
                                <button class="bars"><span>Menu</span></button>
                            </h2> -->

                            <div class="drop__sign">
                                <div class="drop__sign__mask"></div>

                                    <?php if (is_user_logged_in()):
    global $my_account_url;?>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												                                        <a class="btn btn-icon btn-nav btn-user" href="<?=$my_account_url;?>">
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												                                            <i class="fa fa-user"></i>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												                                            <span class="hide-md">YOUR ACCOUNT</span>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												                                        </a>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												                                    <?php else: ?>
                                        <button id="sign__tooltip" class="btn btn-icon btn-nav">
                                            <i class="icon-user"></i>
                                            <span class="hide-md">Sign in or register</span>
                                        </button>
                                    <?php endif;?>

                                <button id="drop__sign__close" class="drop__sign__close"><i class="icon icon-Picto_croisrefermer"></i></button>

                                <div class="drop__sign__tooltip fgray-bgcolor">
                                    <div class="drop__sign__title">Sign in</div>
                                    <p>Access your account and place an order: </p>

                                    <form action="<?=$myaccount_page_url;?>" method="post" name="login-form" role="form">

                                        <input type="hidden" name="login" value="Login">

                                        <?php do_action('woocommerce_login_form_start');?>

                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4 hidden" for="your-email">Email<span class="required">*</span></label>
                                            <input title="Holder name" id="username" name="username" placeholder="Email" class="form-control col-sm-12 col-md-9" type="text" value="<?php if (!empty($_POST['username'])) {
    echo esc_attr($_POST['username']);
}
?>" />
                                            <span class="mobile-label">Email<span class="required">*</span></span>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4 hidden" for="Password">Password<span class="required">*</span></label>
                                            <input title="Holder name" id="password" name="password" placeholder="Password *" class="form-control col-sm-12 col-md-9" type="password" />
                                            <span class="mobile-label">Password<span class="required">*</span></span>
                                        </div>

                                        <?php do_action('woocommerce_login_form');?>

                                        <a href="/lost-password" class="link link--right" title="Forgot your password ?">Forgot your password ?</a>
                                        <div class="input-group input-group-generic">
                                            <div class="checkbox">
                                                <label for="save-card">
                                                    <input value="Remember me" id="save-card" name="rememberme" type="checkbox" value="forever" />
                                                    Remember me
                                                </label>
                                            </div>
                                        </div>

                                        <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce');?>

                                        <button type="submit" class="btn btn-primary btn-icon-right btn-block" title="Login" >Login<i class="icon icon-arrow_left"></i></button>

                                        <?php do_action('woocommerce_login_form_end');?>

                                    </form>

                                    <hr>
                                    <p class="text-center">Don't have an account yet ?</p>
                                    <a class="btn btn-primary btn-icon-right btn-block" href="/register" title="Register now">Register now<i class="icon icon-arrow_left"></i></a>
                                </div>

                            </div><!-- .drop__sign -->

                            <div class="custom-dropdown basket">

                                <button class="btn btn-icon btn-nav btn-basket" style="padding-right: 30px;">
                                    <i class="icon-basket"></i>
                                    <span class="btn-add-qty">0</span>
                                    <span class="hide-md basket-empty">Your basket is empty</span>
                                    <span class="hide-md basket-full">
                                        Your basket: <?=get_woocommerce_currency_symbol();?> <span class="basket-price" style="display: none;"></span>
                                        <span class="basket-price-formatted"></span>
                                    </span>
                                </button>

                                <div class="basket-dialog"
                                    data-start-shopping-url="<?php bloginfo('url');?>/coffee-list"
                                    data-reorder-url="<?php bloginfo('url');?>/my-account"
                                > <!-- basket-dialog--warning -->
                                    <script>
                                        var check_last_order = "<?=check_last_order() ? 'true' : 'false';?>";
                                    </script>
                                    <div class="basket-dialog__wrapper ">

                                        <div class="basket-dialog-header clearfix">
                                            <div class="shopping-bag" id="basket">SHOPPING BAG</div>
                                            <a href="#" title="Close" class="icon icon-Picto_croisrefermer close"></a>
												<?php if(get_option( 'free_enable', false )=='yes'){ ?>
                                             <p style="text-transform: uppercase;"><?php echo get_option( 'free_msg', 'FREE SHIPPING FROM' ) ?> <font color="red"><?php echo get_option( 'free_subtotal', '1,500,000 đ' ) ?></font></p>
											<?php } ?>
                                        </div>

                                        <div class="basket-dialog-warning hidden">
                                            <span>For packaging purposes, capsules can only be ordered in multiples of 50. You can add 20 more on top of the 30 already selected to reach the next threshold.</span>
                                        </div>
                                        
                                        <div class="basket-dialog-product-discount" id="basket-dialog-product-discount">
                                        
                                        </div>
                                        
                                        <div class="basket-dialog-product ">
                                            <ul></ul>
                                        </div>

                                        <div class="basket-dialog-footer clearfix">
                                            <div class="basket-100">
                                            <p><b>FREE <i>Nespresso</i> Touch Travel Mug</b> for the first 100 orders worth P5,000 and up.</p><p>Add the item to your shopping bag and use code: <b>FIRST100</b> upon checkout. </p>
                                            </div>
                                            <div class="basket-subtotal-price">
                                                SUBTOTAL
                                                <span style="display: none;"><?=get_woocommerce_currency_symbol();?><span class="price-int">0</span></span>
                                                <span><?=get_woocommerce_currency_symbol();?><span class="price-int-formatted">0</span></span>
                                            </div>

                                            <!-- <div class="basket-vat-price">
                                                Vat (inCL.)
                                                <span><?=get_woocommerce_currency_symbol();?><span class="price-int">0</span></span>
                                            </div> -->

                                            <div class="basket-total-price">
                                                TOTAL
                                                <span style="display: none;"><?=get_woocommerce_currency_symbol();?><span class="price-int">0</span></span>
                                                <span><?=get_woocommerce_currency_symbol();?><span class="price-int-formatted">0</span></span>
                                            </div>

                                            <div class="basket-footer-bottom">
                                                <p>(without shipping costs and personalized special offers)</p>
                                                <?php if (is_user_logged_in()): ?>
                                                    <a href="<?php echo esc_url(wc_get_checkout_url()); ?>" class="btn btn-green">Proceed to checkout</a>
                                                <?php else: ?>
                                                    <a href="/login" class="btn btn-green">Login to checkout</a>
                                                <?php endif;?>
                                                <br>


                                            </div>

                                        </div><!-- .basket-dialog-footer -->

                                    </div><!-- .basket-dialog__wrapper -->

                                </div>

                                <div class="basket-number nb-item-in-basket hidden">0</div>
                            </div><!-- .custom-dropdown .basket -->

                            <!-- add the template for basket list items -->
                            <?php get_template_part('components/generics/minibasket-line');?>

                            <div id="mask-mini-basket"></div>

                        </div><!-- .ite-control-wrapper -->
                            <button id="search-btn" class="btn btn-icon btn-nav btn-basket btn-search" style="min-width: 5px;">
                                <i class="fa fa-search fa-5" style="margin-left: 10px;" aria-hidden="true"></i>
                            </button>
                    </div><!-- .site-control -->

                </div><!-- .row -->
            </div><!-- .container -->

        </div><!-- .header__fixelts -->
        <div class="container"><br><br>
            <div class="search-div hide-me" id="search-div" >
                <div class="widget woocommerce widget_product_search">
                    <form role="" method="get" class="woocommerce-product-search " action="<?php bloginfo('url');?>">
                        <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products&hellip;" value="" name="s" autocomplete="off" style="height:23px;" />
                        <input type="submit" value="Search" />
                        <input type="hidden" name="post_type" value="product" />
                    </form>
                </div>
            </div>
        </div>
        <nav class="main-nav" role="menubar">
            <div class="container">
                <div class="row">
                    <ul class="menu-container menu-container-inner" id="JumpMenu" tabindex="-1">
                        <li class="menu-item menu-home hidden-md hidden-lg">
                            <a href="/" title="Home"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li class="menu-item menu-coffee" data-menu="coffee">
                            <a href="/coffee-list" accesskey="c" title="Coffee Page"><i class="icon icon-capsule"></i>Coffee</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/coffee-list">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/order-your-capsules.jpg" alt="coffee-order-your-capsules" class="hidden-xs hidden-sm" style="height: 150%; max-height: 150px; margin-top: -5px;" />
                                        </div>
                                        <div class="menu-description" style="margin-top: 15px;">
                                            <span class="menu-head__title">Order Your Capsules</span>
                                            <span class="menu-head__sub-title">Enjoy free delivery as from 1,500,000đ</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/coffee-range">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/order-your-capsules.png" alt="coffee-range" class="hidden-xs hidden-sm" style="height: 100%; max-height: 100px; margin-top: 20px;" />
                                        </div>
                                        <div class="menu-description" style="margin-top: -10px;">
                                            <span class="menu-head__title">Discover the Range</span>
                                            <span class="menu-head__sub-title">Explore Nespresso coffee selection</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <ul class="sub-menu">
                                        <li class="menu-info">
                                            <div class="product-info-wrap">
                                                <a role="menuitem" href="/coffee-origins" class="custom-menu-item" title="Coffee Origins" style="height: 112px;">
                                                    <div class="product-image">
                                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/our-coffee-stories.png" alt="coffee-origins" class="hidden-xs hidden-sm" style="max-width: 140%; margin-left: -27px;" />
                                                    </div>
                                                    <div class="product-info override-css">
                                                        <p class="product-head_title" style="position: absolute; top:45px; margin-left: 4px; color: #000;">Our Coffee Stories</p>
                                                        <p class="product-head_sub-title" style="position: absolute; top:70px; width: 150%; margin-left: -95px; font-size: 10px;">Journey to the perfect cup</p>
                                                    </div>
                                                    <div class="product-info override-vn-css" style="display: none;">
                                                        <p class="product-head_title" style="position: absolute; top:20px; margin-left: 4px; width: 66%; color: #000;">Our Coffee Story</p>
                                                        <p class="product-head_sub-title" style="position: absolute; top:85px; width: 115%; margin-left: -95px; font-size: 10px;">Journey to the perfect cup</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product-info-wrap">
                                                <a role="menuitem" class="custom-menu-item" href="https://www.nespresso.com/recipes/int/en/recipes.html" title="Coffee Recipes" target="_blank" style="margin-top: 10px;">
                                                    <div class="product-image">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/recipes.png" alt="coffee-recipes" class="hidden-xs hidden-sm" style="max-width: 85%; margin-left: -5px;" />
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="product-info" style="margin-top: 0px; color: #000;">
                                                        <p class="product-head_title">Recipes</p>
                                                        <p class="product-head_sub-title" style="font-size: 10px;">Be your own barista</p><br>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="<?php echo $coffee_nav_link; ?>" class="noarrow" ><img src="<?php echo $coffee_nav_banner; ?>" alt="<?php echo $coffee_nav_title; ?>" data-promo-name="<?=$coffee_nav_title;?>" data-promo-id="Coffee_Navgitation_banner" data-promo-creative="Header Navigation | Coffee Banner" data-promo-position="Coffee Navigation | 4" class="promo-banner coffee-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-machines" data-menu="machine">
                            <a href="/machine-list" accesskey="d" title="Machines"><i class="icon icon-machine"></i>Machines</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/machine-list">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/order-your-machines.png" alt="machines-order-your-machine" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Order Your Machine</span>
                                            <span class="menu-head__sub-title" >Free delivery for every<br>machine purchase</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/my-account/my-machines/">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/mymachine.png" alt="machines-compare-machine" class="hidden-xs hidden-sm" style="height: 145%; max-height: 220px; margin-top: -15px;"/>
                                        </div>
                                        <div class="menu-description" style="margin-top: 25px;">
                                            <span class="menu-head__title">Register your machine</span>
                                            <span class="menu-head__sub-title" >Benefit from 2 years warranty</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <ul class="sub-menu">
                                        <li class="menu-info">
                                            <div class="product-info-wrap">
                                                <a role="menuitem" href="/descaling-kit" class="custom-menu-item" title="Decaling your machine regularly" style="">
                                                    <div class="product-image">
                                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/decaling.png" alt="coffee-origins" class="hidden-xs hidden-sm" style="max-width: 140%; margin-left: -27px;" />
                                                    </div>
                                                    <div class="product-info override-css">
                                                        <p class="product-head_title" style="position: absolute; top:45px; margin-left: 4px; color: #000;">Decaling your machine regularly</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product-info-wrap">
                                                <a role="menuitem" class="custom-menu-item" href="#" title="Video machine assistance" target="_blank" style="margin-top: 10px;">
                                                    <div class="product-image">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/machivideo.png" alt="coffee-recipes" class="hidden-xs hidden-sm" style="max-width: 85%; margin-left: -5px;" />
                                                    </div>
                                                    <div class="product-info override-css">
                                                        <p class="product-head_title" style="position: absolute; top:25px; margin-left: 4px; color: #000;">Video machine assistance</p>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="<?php echo $coffee_nav_link; ?>" class="noarrow" ><img src="<?php echo $coffee_nav_banner; ?>" alt="<?php echo $coffee_nav_title; ?>" data-promo-name="<?=$coffee_nav_title;?>" data-promo-id="Coffee_Navgitation_banner" data-promo-creative="Header Navigation | Coffee Banner" data-promo-position="Coffee Navigation | 4" class="promo-banner coffee-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-accessories" data-menu="accessory">
                            <a href="/accessory-list" accesskey="e" title="Accessories"><i class="icon icon-accessories"></i>Accessories</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/accessory-list">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/order-your-accessories.png" alt="accessories-buy accessories" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Order Your Accessories</span>
                                            <span class="menu-head__sub-title" >Serve your Nespresso in style</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/aeroccino-3/">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/aeroccino-3.png" alt="accessories-aeroccino" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">The Aeroccino 3</span>
                                            <span class="menu-head__sub-title" >Create light or creamy, cold or<br>hot coffee with our milk frother</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/descaling-kit/">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/descaling-kit.png" alt="descaling-kit" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Descaling Kit</span>
                                            <span class="menu-head__sub-title" >
                                                Care for your coffee machine
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="<?php echo $accessory_nav_link; ?>" class="noarrow" ><img src="<?php echo $accessory_nav_banner; ?>" alt="<?php echo $accessory_nav_title; ?>" data-promo-name="<?=$accessory_nav_title;?>" data-promo-id="Accessory_Navgitation_banner" data-promo-creative="Header Navigation | Accessory Banner" data-promo-position="Accessory Navigation | 4" class="promo-banner accessory-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-our-services" data-menu="our-services">
                            <a href="/services" title="Our services"><i class="icon icon-services"></i>Our Services</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/services">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/services-order.png" alt="services-order" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">ORDERS</span>
                                            <span class="menu-head__sub-title">Our Coffee Specialists are ready to assist you</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/services/#delivery">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/services-delivery.png" alt="services-delivery" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">DELIVERY</span>
                                            <span class="menu-head__sub-title" >Learn about our delivery terms</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/services/#customer-care">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/services-customer-care.png" alt="services-customer-care" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">CUSTOMER CARE</span>
                                            <span class="menu-head__sub-title" >
                                                Do you need assistance for your machine?
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="<?php echo $our_services_nav_link; ?>" class="noarrow" ><img src="<?php echo $our_services_nav_banner; ?>" alt="<?php echo $our_services_nav_title; ?>" data-promo-name="<?=$our_services_nav_title;?>" data-promo-id="Our_Services_Navgitation_banner" data-promo-creative="Header Navigation | Our Services Banner" data-promo-position="Our Services Navigation | 4" class="promo-banner our-services-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-become-a-member" style="text-align: center;" title="New to Nespresso?">
                            <a href="/login/"><i class="icon icon-Nespresso_Monogram_2"></i>New to Nespresso?</a>
                        </li>
                        <li class="menu-item menu-store-locator">
                            <a href="/recycling" title="Recycling"><i class="icon icon-Recycling_on"></i>Recycling</a>
                        </li>
                        <li class="menu-item menu-store-locator">
                            <a href="/store-locator" title="Recycling"><i class="icon icon-location"></i>Store Locator</a>
                        </li>
                        <li class="menu-item menu-customer-service" data-menu="contact-us">
                            <a href="/faq" id="JumpContact" tabindex="-1" title="Contact us"><i class="icon icon-faq"></i>Contact Us</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/faq">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/contact-faq.png" alt="contact-faq" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">FAQs</span>
                                            <span class="menu-head__sub-title" >We answer your frequently<br>asked questions</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/contact-us">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/contact-email.png" alt="contact-email" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Email us</span>
                                            <span class="menu-head__sub-title" >
                                                Our coffee specialists are ready to help you
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/services#customer-care">
                                        <div class="menu-image">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/menu/contact-telephone.png" alt="contact-telephone" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Call us at 1900 633 474</span>
                                            <span class="menu-head__sub-title" >
                                                Lines are open Mon - Sat,<br>from 10am to 6pm
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="<?php echo $contact_us_nav_link; ?>" class="noarrow" ><img src="<?php echo $contact_us_nav_banner; ?>" alt="<?php echo $contact_us_nav_title; ?>" data-promo-name="<?=$contact_us_nav_title;?>" data-promo-id="Contact_Us_Navgitation_banner" data-promo-creative="Header Navigation | Contact Us Banner" data-promo-position="Contact Us Navigation | 4" class="promo-banner contact-us-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-business-solutions last">
                            <a href="/pro" title="Professional"><i class="icon icon-business"></i>Professional</a>
                        </li>
                        <li class="menu-item menu-language visible-xs visible-sm">
                            <form action="/" method="GET">
                                <div class="dropdown menu-language__item  dropdown--language">
                                    <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current">English</span></button>
                                    <select class="select__element" id="header-language" name="">
                                        <option value="1">English</option>
                                        <option value="2">Français</option>
                                    </select>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="mask"></div>

        <!-- Set Gift Information -->
        <?php $other_settings = get_nespresso_other_settings();?>
        <?php $coys_promo = get_nespresso_coys_promo();?>
        <input type="hidden" id="gwp_gift_status" value="<?=@$other_settings['gwp_status'];?>">
        <input type="hidden" id="gwp_gift_by_product_status" value="<?=@$coys_promo['coys_promo_status'];?>">
    </header>
</div>
<?php if ($advisory_status && $advisory_status == 'Active'): ?>
    <!-- banner -->
    <div class="new-banner">
        <!-- web -->
        <a class="web-banner-img <?= $advisory_callback ? 'call-modal-advisory' : '' ?>" data-callback="<?= $advisory_callback ?>" href="javascript:void(0)"><img class="advisory-banner" src="<?php echo $advisory_desktop_img; ?>" draggable="false"/></a>
        <!-- mobile -->
        <a class="mobile-banner-img <?= $advisory_callback ? 'call-modal-advisory' : '' ?>" data-callback="<?= $advisory_callback ?>" href="javascript:void(0)"><img class="advisory-banner" src="<?php echo $advisory_mobile_img; ?>" draggable="false"/></a>
    </div>
    <?php if ($advisory_callback):?>
        <div id="modal-advisory" class="modal-advisory">
          <!-- Modal content -->
            <?php
	            $custom_modal_banner_nespresso = "";
	            if(get_option( 'custom_modal_banner_nespresso' ))
	            {
		            $custom_modal_banner_nespresso = get_option( 'custom_modal_banner_nespresso');
	            }
	            
                if($custom_modal_banner_nespresso)
                {
                    ?>
                        <div class="container" style="background: #fff;border: 2px solid;color: black;">
                            <?= $custom_modal_banner_nespresso; ?>
                        </div>
                    <?php
                }
                else
                {
                    ?>
                    <div class="modal-advisory-content">
                        <div class="modal-advisory-header">
                            <span class="close-modal-advisory">&times;</span>
                        </div>
                        <div class="modal-advisory-body">
                            <img src="/wp-content/themes/storefront-child/images/how-to-order-online.png">
                        </div>
                        <div class="modal-advisory-body">
                            <img src="/wp-content/themes/storefront-child/images/how-to-order-online.png">
                        </div>
                        <div class="modal-advisory-footer text-center">
                            <button type="button" class="button alt dismiss-modal-advisory" >Close</button>
                        </div>
                    </div>
                    
                    <?php
                }
            ?>
        </div>
    <?php endif;?>

    <?php
        if (
            isset($slider_pop_up['slider_pop_up_header']) &&
            $slider_pop_up['slider_pop_up_header'] &&
            isset($slider_pop_up['slider_pop_up_content']) &&
            $slider_pop_up['slider_pop_up_content']
        ):?>
            <div id="modal-slider" class="modal-slider">
              <!-- Modal content -->
              <div class="modal-slider-content">
                <div class="modal-slider-header">
                    <span class="close-modal-slider">&times;</span>
                </div>
                <div class="modal-slider-body text-center">
                    <h2><?= $slider_pop_up['slider_pop_up_header'] ?></h2>
                    <?= $slider_pop_up['slider_pop_up_content'] ?>
                </div>
                <div class="modal-slider-footer text-center">
                  <!-- <button type="button" class="button alt dismiss-modal-slider" >Close</button> -->
                </div>
              </div>

            </div>
        <?php endif;?>
<?php endif;?>
<?php
global $scripts;

$scripts[] = 'js/components/withinviewport.js';
$scripts[] = 'js/components/jquery.withinviewport.js';
$scripts[] = 'js/components/gtm_function.js';
$scripts[] = 'js/components/minibasket.js?t='.time();
$scripts[] = 'js/components/nespressodiscount.js';
?>
