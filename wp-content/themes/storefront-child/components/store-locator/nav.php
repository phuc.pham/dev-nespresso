<nav class="slocator-stores col-md-12 no-padding no-margin">
    <div id="tabs-nav">
        <ul>
            <li class="w-31"></li>
            <li><a class="tab-1" href="javascript:void(0)" title="Retailers">
                <span class="slocator-stores__icon icon icon-Resellers_on"></span>
                <span class="slocator-stores__store">Machine Retailers</span>
            </a></li>
            <!-- <li><a class="tab-10" href="#" title="Nespresso Boutiques">
                <span class="slocator-stores__icon icon icon-Boutique_on" style="font-size: 23px"></span>
                <span class="slocator-stores__store">Nespresso Boutique</span>
            </a></li> -->
        </ul>
    </div>
</nav>
<div class="slocator-switchers">
    <div class="slocator-switchers__tab active" data-show="#slocator-locations">
        <a class="mobile-store-locator-tab" data-tab="tab-1" title="Retailers">
            <span class="slocator-stores__icon icon icon-Resellers_on"></span>
            <span class="slocator-stores__store">Machine Retailers</span>
        </a>
    </div>
    <!-- <div class="slocator-switchers__tab active" data-show="#slocator-container-map">
        <a class="mobile-store-locator-tab" data-tab="tab-10" title="Nespresso Boutiques">
            <span class="slocator-stores__icon icon icon-Boutique_on"></span>
            <span class="slocator-stores__store">Nespresso Boutique</span>
        </a>
    </div> -->
</div>
