<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDupijAUtPSPwefNh473b7gIyCgzfJQcFo&callback=initMap"></script>
<script>
      function initMap() {
        var center = {lat:10.7682141, lng:106.6755519};
        var main_retailer = {lat:10.7682141, lng: 106.6755519};

        //init
        var map = new google.maps.Map(document.getElementById('getMap'), {
          zoom: 12,
          center: center
        });

        //Rustan Shangri-la
        var contentString = '<div id="rus_shang" class="map-lbl-content">'+
                "<h5>ÂAN NAM FINE FOOD CO., LTD</h5>"+
                '<div class="map-lbl-bodycontent">'+
                '<p>322 Dien Bien Phu street,<br>Ward 22, Binh Thanh District,<br>Ho Chi Minh City, VN</p>'+
                '</div>'+
                '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        var marker = new google.maps.Marker({
          position: main_retailer,
          map: map
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      }
</script>
