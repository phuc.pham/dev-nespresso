<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$user = nespresso_get_user_data();

$my_machines = isset($user->my_machines) && isset($user->my_machines[0]) ? $user->my_machines[0] : null;

$products = get_machine_products();

$current_url = home_url(add_query_arg(null, null));

?>

<script>
    // used in my-machines.js
    var products = <?= json_encode($products); ?>;
    var my_machines = <?= json_encode($my_machines) ?>;
</script>

<main id="content">
	<div class="container">
	    <div class="row">

	    	<!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <div class="pd-right-0 col-md-9 col-xs-12">

		        <div class="col-xs-12 account-detail account-detail--withfooter my-machines gray-bgcolor">
		            <header>
		                <h2><?= get_the_title() ?></h2>
		            </header>

		            <div id="container-machine-list" class="">

		            	<?php if ( $my_machines) : ?>
				            <?php
				            	foreach($my_machines as $k => $my_machine) :
				            		$machine = get_machines_product_meta_data($my_machine['product_id']);

				            		$product_color_variant_index = '';
				            		if ( isset($my_machine['product_color_variant_index']) && $my_machine['product_color_variant_index'] ) :
				            			$product_color_variant_index = $my_machine['product_color_variant_index'];
			            			endif;
				            ?>
						            <div class="row my-machines__list">
						                <div class="col-xs-12 col-sm-3">
						                	<?php
						                		$image_url = $machine->image[0];
						                		if ($image_url) :
						                	?>
						                    	<img src="<?= $image_url ?>" class="img-responsive img-responsive-center img-machines-list" alt="<?= $machine->name ?>" >
					                    	<?php endif; ?>
						                </div>

						                <div class="col-xs-12 col-sm-4">
						                    <h3 class="my-machines__list__title"><?= $machine->name ?></h3>

						                    <div class="my-machines__list__details">
						                        <strong>Serial number:</strong>
						                        <span><?= $my_machine['serial_no'] ?></span>
						                        <?php if ($my_machine['purchase_date'] !== '0-0-0') :?>
							                        <strong>Purchase date:</strong>
							                        <span><?= $my_machine['purchase_date'] ?></span>
						                    	<?php endif; ?>
						                    </div>
						                </div>

						                <div class="col-xs-12 col-sm-5">
						                    <div class="my-machines__list__buttons" style="text-align: right">
						                        <button class="btn btn-primary btn-noWidth btn-edit"
						                        	title="Edit machine"
						                        	data-my-machine-index="<?= $k ?>"
						                        	data-product-name="<?= $machine->name ?>"
						                        	data-product-color-variant-index="<?= $product_color_variant_index ?>"
					                        	>Edit</button>
						                        <button class="btn btn-primary  btn-noWidth btn-remove"  title="Remove machine" data-my-machine-index="<?= $k ?>">Remove</button>
						                    </div>
						                </div>
						            </div>
				            <?php endforeach; ?>
			            <?php  else : ?>
			            	<h3>You have no machines yet.</h3>
		            	<?php endif; ?>

			            <div class="account-detail__footer">
			                <button class="btn btn-primary pull-right" id="btn-add-machine" title="Add a Machine">Add a Machine</button>
			            </div>
			       	</div><!-- #container-mahine-list -->

	   				<div id="container-machine-add" class="hide">
						<?php include ( __DIR__ . '/my-machines/my-machines-form.php'); ?>
					</div><!-- #container-mahine-add -->

				</div><!-- .my-machines -->

				<form method="post" action="<?= $current_url ?>" role="form" class="hide" id="form-my-machines-remove">
					<?php wp_nonce_field( 'nespresso_post_nonce', 'nespresso_post_nonce_field' ); ?>
					<input type="hidden" name="form-for" value="my-machine-remove">
					<input type="hidden" name="my_machine_index" value="">
				</form>

			</div><!-- col-md-9 -->

		</div><!-- .row -->
	</div><!-- .container -->
</main>

<?php
	global $scripts;
	$scripts[] = 'js/components/validation.js';
	$scripts[] = 'js/components/my-machines.js';

?>
