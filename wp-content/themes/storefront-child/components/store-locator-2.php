<?php
	/**
	 * Nespresso Custom Theme developed by Minion Solutions
	 * Child theme of Storefront
	 *
	 * @link https://minionsolutions.com
	 *
	 * @since 1.0
	 * @version 1.0
	 */
	
	if (!defined('ABSPATH')) {
		exit; // Exit if accessed directly
	}
	
	// Category Banner
	$category_banner = get_nespresso_category_banner();
	
	$accessory_desktop_img = isset($category_banner['accessory_category_banner_image_url']) && $category_banner['accessory_category_banner_image_url'] ? $category_banner['accessory_category_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 25/Accessories List_Desktop.png";
	$accessory_mobile_img = isset($category_banner['accessory_category_banner_image_url_mobile']) && $category_banner['accessory_category_banner_image_url_mobile'] ? $category_banner['accessory_category_banner_image_url_mobile'] : get_stylesheet_directory_uri() . "/images/banner/September 25/Accessories List_Mobile.png";
	$accessory_sidebar_img = isset($category_banner['accessory_sidebar_image_url']) && $category_banner['accessory_sidebar_image_url'] ? $category_banner['accessory_sidebar_image_url'] : '';
	$accessory_link = isset($category_banner['accessory_category_banner_link']) && $category_banner['accessory_category_banner_link'] ? $category_banner['accessory_category_banner_link'] : "/accessory-list/";
	$accessory_promo = isset($category_banner['accessory_category_banner_promo_name']) && $category_banner['accessory_category_banner_promo_name'] ? $category_banner['accessory_category_banner_promo_name'] : "Accessory Banner Promo";
	
	/**
	 * get the accessory products
	 */
	$products = get_accessory_products();
	
	$product_count = 0;
	foreach ($products as $product):
		$product_count += count($product);
	endforeach;
	$arrays = get_nespresso_product_list();
	if (!$arrays) {
		$categories = custom_acf_get_data('Product Details', 'collection');
	} else {
		$categories['choices'] = $arrays['accessory'];
	}

?>

<!-- content -->

<!-- quantity selector for add to basket when clicked -->
<?php get_template_part('components/generics/qty-selector'); ?>

<main id="content" style="margin: 0px 0px 50px 0px;" class="fullPage fullPage__plp accessory-list">

    <div id="post-74" class="post-74 page type-page status-publish hentry category-store-locator pmpro-has-access">
        <div class="entry-content">
            <div class="slocator">
                <!-- banner -->
                <div class="new-banner">
                    <!-- web -->
                    <a class="web-banner-img"
                       href="<?= get_option('link_banner_store_locator_2') ? get_option('link_banner_store_locator_2') : ""; ?>"><img
                                src="<?= get_option('banner_store_locator_2') ? get_option('banner_store_locator_2') : ""; ?>"
                                draggable="false"></a>
                    <!-- mobile -->
                    <a class="mobile-banner-img"
                       href="<?= get_option('link_mobile_banner_store_locator_2') ? get_option('link_mobile_banner_store_locator_2') : ""; ?>"><img
                                src="<?= get_option('mobile_banner_store_locator_2') ? get_option('mobile_banner_store_locator_2') : ""; ?>"
                                draggable="false"> </a>
                </div>
                <!-- end banner -->
                <div class="slocator-main clearfix">
                    <div class="container">
                        <div class="slocator-menu">
                            <div class="row no-padding no-margin">
                                <nav class="slocator-stores col-md-12 no-padding no-margin">
                                    <div id="tabs-nav">
                                        <ul>
                                            <li class="w-31"></li>
											<?php
												if (get_option('on_off_nespresso_boutique')) {
													?>
                                                    <li>
                                                        <a class="tab-10" href="#" title="Nespresso Boutiques">
                                                            <span class="slocator-stores__icon icon icon-Boutique_on"
                                                                  style="font-size: 23px"></span>
                                                            <span class="slocator-stores__store">Nespresso Boutique</span>
                                                        </a>
                                                    </li>
													<?php
												}
												
												if (get_option('on_off_nespresso_nano_boutique')) {
													?>
                                                    <li>
                                                        <a class="tab-pop-up-store" href="#" title="Pop-Up Store">
                                                            <span class="slocator-stores__icon icon icon-Boutique_on"
                                                                  style="font-size: 23px"></span>
                                                            <span class="slocator-stores__store">Nespresso Boutique</span>
                                                        </a>
                                                    </li>
													<?php
												}
											?>
                                        </ul>
                                    </div>
                                </nav>
                                <div class="slocator-switchers">
									<?php
										if (get_option('on_off_nespresso_boutique')) {
											?>
                                            <div class="slocator-switchers__tab active"
                                                 data-show="#slocator-container-map">
                                                <a class="mobile-store-locator-tab active" data-tab="tab-10"
                                                   title="Nespresso Boutiques">
                                                    <span class="slocator-stores__icon icon icon-Boutique_on"></span>
                                                    <span class="slocator-stores__store">Nespresso Boutique</span>
                                                </a>
                                            </div>
											<?php
										}
										
										if (get_option('on_off_nespresso_nano_boutique')) {
											?>
                                            <div class="slocator-switchers__tab <?php echo get_option('on_off_nespresso_boutique') == false && get_option('on_off_nespresso_nano_boutique') == true ? "active" : " "; ?>"
                                                 data-show="#slocator-container-map">
                                                <a class="mobile-store-locator-tab" data-tab="tab-pop-up-store"
                                                   title="Pop-Up Store">
                                                    <span class="slocator-stores__icon icon icon-Pick_up_point_on"></span>
                                                    <span class="slocator-stores__store">Nespresso Boutique</span>
                                                </a>
                                            </div>
											<?php
										}
									?>
                                </div>
                            </div>
                        </div>
                        <div style="overflow: inherit;" class="slocator-results">
                            <div class="row no-margin no-padding">
                                <div class="slocator-map col-md-12 no-margin no-padding slocator-switchers__view active"
                                     id="slocator-container-map">
                                    <div id="tabs-content">
										<?php
											if (get_option('on_off_nespresso_boutique')) {
												?>
                                                <div id="tab-10" class="tab" style="display: block;">
                                                    <div class="mapouter">
                                                        <div class="gmap_canvas">
                                                            <iframe style="width: 100%;" width="100%" height="600"
                                                                    id="gmap_canvas"
                                                                    src="<?= get_option('maps_nespresso_boutique') ? get_option('maps_nespresso_boutique') : ""; ?>"
                                                                    frameborder="0" scrolling="no" marginheight="0"
                                                                    marginwidth="0"></iframe>
                                                            embed google map <a href="http://www.embedgooglemap.net">embedgooglemap.net</a>
                                                        </div>
                                                        <style>.mapouter {
                                                                overflow: hidden;
                                                                height: 600px;
                                                                width: 100%;
                                                            }

                                                            .gmap_canvas {
                                                                background: none !important;
                                                                height: 600px;
                                                                width: 100%;
                                                            }</style>
                                                    </div>
                                                    <div class="slocator-popup">
														<?php
															if (get_option('on_off_nespresso_boutique_tabs_1')) {
																?>
                                                                <div class="row">
                                                                    <div class="slocator-popup__location col-md-4"
                                                                         id="boutique_power_plant"
                                                                         data-src="<?= get_option('maps_nespresso_boutique') ? get_option('maps_nespresso_boutique') : ""; ?>">
                                                                        <div class="d-inline">
                                                                            <span class="slocator-stores__icon icon icon-Boutique_on footer-map-icon"
                                                                                  style="font-size: 25px"></span>
                                                                            <!--                                                            Power Plant Boutique-->
                                                                        </div>
                                                                        <div class="slocator-popup__location-content">
																			<?= get_option('nespresso_boutique') ? get_option('nespresso_boutique') : ""; ?>
                                                                            <br>
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                    <div class="slocator-popup__location col-md-4 store-reseller-landmark-trinoma">
                                                                        <div class="slocator-popup__location-content">
                                                                            Store hours
                                                                            <br>
																			<?= get_option('store_hours_nespresso_boutique') ? get_option('store_hours_nespresso_boutique') : ""; ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="slocator-popup__location col-md-4">
                                                                        <div class="d-inlinet">
                                                                            <!--                                                            For direct order and pick-up please call:-->
                                                                            <!--                                                            <br>-->
																			<?= get_option('direct_pickup_nespresso_boutique') ? get_option('direct_pickup_nespresso_boutique') : ""; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
																<?php
															}
														?>
                                                    </div>
                                                </div>
												<?php
											}
											
											if (get_option('on_off_nespresso_nano_boutique')) {
												?>
                                                <div id="tab-pop-up-store" class="tab"
                                                     style="display:<?php echo get_option('on_off_nespresso_boutique') == false && get_option('on_off_nespresso_nano_boutique') == true ? "block" : "none"; ?> ;">
                                                    <div class="mapouter">
                                                        <div class="gmap_canvas">
                                                            <iframe style="width: 100%;" width="100%" height="600"
                                                                    id="gmap_canvas"
                                                                    src="<?= get_option('maps_nespresso_boutique_2') ? get_option('maps_nespresso_boutique_2') : ""; ?>"
                                                                    frameborder="0" scrolling="no" marginheight="0"
                                                                    marginwidth="0"></iframe>
                                                            embed google map <a href="http://www.embedgooglemap.net">embedgooglemap.net</a>
                                                        </div>
                                                        <style>.mapouter {
                                                                overflow: hidden;
                                                                height: 600px;
                                                                width: 100%;
                                                            }

                                                            .gmap_canvas {
                                                                background: none !important;
                                                                height: 600px;
                                                                width: 100%;
                                                            }</style>
                                                    </div>
                                                    <div class="slocator-popup">
														<?php
															if (get_option('on_off_nespresso_nano_boutique_tabs_1')) {
																?>
                                                                <div class="row">
                                                                    <div class="slocator-popup__location col-md-4"
                                                                         id="boutique_power_plant"
                                                                         data-src="<?= get_option('maps_nespresso_boutique_2') ? get_option('maps_nespresso_boutique_2') : ""; ?>">
                                                                        <div class="d-inline">
                                                                            <span class="slocator-stores__icon icon icon-Boutique_on footer-map-icon"
                                                                                  style="font-size: 25px"></span>
                                                                            <!--                                                            Power Plant Boutique-->
                                                                        </div>
                                                                        <div class="slocator-popup__location-content">
																			<?= get_option('nespresso_nano_boutique_1') ? get_option('nespresso_nano_boutique_1') : ""; ?>
                                                                            <br>
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                    <div class="slocator-popup__location col-md-4 store-reseller-landmark-trinoma">
                                                                        <div class="slocator-popup__location-content">
                                                                            Store hours
                                                                            <br>
																			<?= get_option('store_hours_nespresso_nano_boutique_1') ? get_option('store_hours_nespresso_nano_boutique_1') : ""; ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="slocator-popup__location col-md-4">
                                                                        <div class="d-inlinet">
                                                                            <!--                                                            For direct order and pick-up please call:-->
                                                                            <!--                                                            <br>-->
																			<?= get_option('direct_pickup_nespresso_nano_boutique_1') ? get_option('direct_pickup_nespresso_nano_boutique_1') : ""; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
																<?php
															}
															
															if (get_option('on_off_nespresso_nano_boutique_tabs_2')) {
																?>
                                                                <div class="row">
                                                                    <div class="slocator-popup__location col-md-4"
                                                                         id="boutique_podium"
                                                                         data-src="https://maps.google.com/maps?q=ADB+Avenue+Entrance,+The+Podium,+Mandaluyong+City&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed">
                                                                        <div class="d-inline">
                                                                            <span class="slocator-stores__icon icon icon-Boutique_on footer-map-icon" style="font-size: 25px"></span>
<!--                                                                            Podium Boutique-->
                                                                        </div>
                                                                        <div class="slocator-popup__location-content">
																			<?= get_option('nespresso_nano_boutique_2') ? get_option('nespresso_nano_boutique_2') : ""; ?>
                                                                            <br>
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                    <div class="slocator-popup__location col-md-4 store-reseller-landmark-trinoma">
                                                                        <div class="slocator-popup__location-content">
                                                                            Store hours
                                                                            <br>
																			<?= get_option('store_hours_nespresso_nano_boutique_2') ? get_option('store_hours_nespresso_nano_boutique_2') : ""; ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="slocator-popup__location col-md-4">
                                                                        <div class="d-inlinet">
                                                                            <!--                                                            For direct order and pick-up please call:-->
                                                                            <!--                                                            <br>-->
																			<?= get_option('direct_pickup_nespresso_nano_boutique_2') ? get_option('direct_pickup_nespresso_nano_boutique_2') : ""; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
																<?php
															}
														?>
                                                    </div>
                                                </div>
												<?php
											}
										?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var $ = jQuery;
                $(document).on('click', '#tabs-nav > ul > li > a', function () {
                    var href = $(this).attr('class');
                    $('.tab').hide();
                    $('#tabs-nav a').removeClass('active');
                    $(this).addClass('active');
                    $('#' + href).show();
                });
                $('.mobile-store-locator-tab').on('click', function () {
                    var tab = $(this).data('tab');
                    $('.tab').hide();
                    $('#tabs-nav a').removeClass('active');
                    $(this).addClass('active');
                    $('#' + tab).show();
                });
                $('[data-tab="tab-10"]').click();
            </script>
            <script async="" defer=""
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDupijAUtPSPwefNh473b7gIyCgzfJQcFo&amp;callback=initMap"></script>
            <script>
                function initMap() {
                    var center = {lat: 14.6192553, lng: 121.0490137};
                    var center_recycling = {lat: 14.5029376, lng: 120.9888038};
                    var rustan_alabang = {lat: 14.423486, lng: 121.028949};
                    var rustan_makati = {lat: 14.5522611, lng: 121.0269731};
                    var rustan_shangrila = {lat: 14.581435, lng: 121.054284};
                    var bonifacio = {lat: 14.550649, lng: 121.046762};
                    var homeworld_sm_makati = {lat: 14.5507066, lng: 121.0235386};
                    var homeworld_sm_megamall = {lat: 14.5844928, lng: 121.0566151};
                    var abenson_ascott = {lat: 14.5505258, lng: 121.0446348};
                    var nespresso_boutique = {lat: 14.5652755, lng: 121.0366256};
                    var nespresso_boutique_recycling = {lat: 14.5509828, lng: 121.0452332};
                    var nespresso_booth = {lat: 14.585389, lng: 121.0576328};
                    var pop_up_store = {lat: 14.5129597, lng: 121.0272166};
                    var greenbelt_5 = {lat: 14.552688, lng: 121.0182666};
                    var shangrila_plaza = {lat: 14.5814581, lng: 121.0549553};
                    var alabang_town = {lat: 14.4185776, lng: 121.0351921};
                    var one_bonifacio = {lat: 14.5517221, lng: 121.046915};
                    var robinsons_magnolia = {lat: 14.6147108, lng: 121.0357845};
                    var trinoma = {lat: 14.6523907, lng: 121.0332098};
                    var rustans_gateway = {lat: 14.6221565, lng: 121.0507448};
                    var homeworld_sm_north_edsa = {lat: 14.6564769, lng: 121.0287359};
                    var allhome_libis = {lat: 14.603846, lng: 121.0776903};
                    var robinsons_department = {lat: 14.5904849, lng: 121.0596267};
                    var allhome_north_molino = {lat: 14.4357257, lng: 120.9670723};
                    var allhome_sta_rosa = {lat: 14.2434282, lng: 121.0577028};
                    var allhome_evia = {lat: 14.3756258, lng: 121.0110414};
                    var abenson_robinsons_place = {lat: 14.5783055, lng: 120.981509};
                    var uptown_bgc = {lat: 14.5564973, lng: 121.0520231};
                    var sm_aura = {lat: 14.5467532, lng: 121.0523612};
                    var rustans_gateway_recycle = {lat: 14.6218822, lng: 121.0509506};
                    var abenson_glorietta_1 = {lat: 14.5515876, lng: 121.0214628};

                    var map = new google.maps.Map(document.getElementById('getMap'), {
                        zoom: 12,
                        center: center
                    });

                    var mapRecycling = new google.maps.Map(document.getElementById('getMapRecycling'), {
                        zoom: 12,
                        center: center_recycling
                    });

                    var mapPopUpStore = new google.maps.Map(document.getElementById('getPopUpStore'), {
                        zoom: 12,
                        center: pop_up_store
                    });

                    // alabang
                    var contentString = '<div id="rus_mak" class="map-lbl-content">' +
                        "<h5>Rustan’s Alabang</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>2nd level, Rustan’s Department Store, Home Department, Alabang Town Center, Alabang Zapote Road, Muntinlupa, Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_alabang = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_alabang = new google.maps.Marker({
                        position: rustan_alabang,
                        map: map
                    });
                    marker_alabang.addListener('click', function () {
                        infowindow_alabang.open(map, marker_alabang);
                    });

                    var marker_alabang_recycling = new google.maps.Marker({
                        position: rustan_alabang,
                        map: mapRecycling
                    });
                    marker_alabang_recycling.addListener('click', function () {
                        infowindow_alabang.open(mapRecycling, marker_alabang_recycling);
                    });
                    // alabang

                    var contentString = '<div id="rus_mak" class="map-lbl-content">' +
                        "<h5>Rustan's Makati</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>4th Level Home Department,<br>Ayala Center, Ayala Avenue,<br>Makati City 1200 Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow1 = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker1 = new google.maps.Marker({
                        position: rustan_makati,
                        map: map
                    });
                    marker1.addListener('click', function () {
                        infowindow1.open(map, marker1);
                    });

                    var marker1_recycling = new google.maps.Marker({
                        position: rustan_makati,
                        map: mapRecycling
                    });
                    marker1_recycling.addListener('click', function () {
                        infowindow1.open(mapRecycling, marker1_recycling);
                    });

                    var contentString = '<div id="rus_shang" class="map-lbl-content">' +
                        "<h5>Rustan's Shangri-La</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>3rd Level Home Department,<br>EDSA cor. Shaw Boulevard,<br>Mandaluyong City Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow2 = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker2 = new google.maps.Marker({
                        position: rustan_shangrila,
                        map: map
                    });
                    marker2.addListener('click', function () {
                        infowindow2.open(map, marker2);
                    });

                    var marker2_recycling = new google.maps.Marker({
                        position: rustan_shangrila,
                        map: mapRecycling
                    });
                    marker2_recycling.addListener('click', function () {
                        infowindow2.open(mapRecycling, marker2_recycling);
                    });

                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>Abenson Ascott</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>2nd Level Ascott Building, 26th street, Bonifacio Global City, Taguig</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow3 = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker3 = new google.maps.Marker({
                        position: abenson_ascott,
                        map: map
                    });
                    marker3.addListener('click', function () {
                        infowindow3.open(map, marker3);
                    });

                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>Homeworld SM Makati</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>5th level, The SM Store, Home Department, Hotel Dr.  Cor. East Dr. Ayala Center, Makati City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow8 = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker8 = new google.maps.Marker({
                        position: homeworld_sm_makati,
                        map: map
                    });
                    marker8.addListener('click', function () {
                        infowindow8.open(map, marker8);
                    });

                    // marker 9
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>Homeworld SM Megamall</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>>LGF, The SM Store, EDSA cor. Doña Julia Vargas ave., Ortigas Center, Wack-Wack Greenhills, Mandaluyong City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow9 = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker9 = new google.maps.Marker({
                        position: homeworld_sm_megamall,
                        map: map
                    });
                    marker9.addListener('click', function () {
                        infowindow9.open(map, marker9);
                    });
                    // marker 9

                    // homeworld_sm_north_edsa
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>Homeworld SM North Edsa</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>LGF, The SM Store, Home Department, EDSA cor. North Ave., Quezon City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_homeworld_sm_north_edsa = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_homeworld_sm_north_edsa = new google.maps.Marker({
                        position: homeworld_sm_north_edsa,
                        map: map
                    });
                    marker_homeworld_sm_north_edsa.addListener('click', function () {
                        infowindow_homeworld_sm_north_edsa.open(map, marker_homeworld_sm_north_edsa);
                    });
                    // homeworld_sm_north_edsa

                    // allhome_libis
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>AllHome Libis</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>AllHome Libis, E. Rodriguez Jr. Ave. Quezon City.</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_allhome_libis = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_allhome_libis = new google.maps.Marker({
                        position: allhome_libis,
                        map: map
                    });
                    marker_allhome_libis.addListener('click', function () {
                        infowindow_allhome_libis.open(map, marker_allhome_libis);
                    });
                    // allhome_libis

                    // robinsons_department
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>Robinson’s Department Store- NOW OPEN</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>3rd floor Robinson’s Galleria Ortigas Center, Quezon City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_robinsons_department = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_robinsons_department = new google.maps.Marker({
                        position: robinsons_department,
                        map: map
                    });
                    marker_robinsons_department.addListener('click', function () {
                        infowindow_robinsons_department.open(map, marker_robinsons_department);
                    });
                    // robinsons_department

                    // allhome_north_molino
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>AllHome North Molino</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Vista Mall North Molino, Molino Cavite</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_allhome_north_molino = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_allhome_north_molino = new google.maps.Marker({
                        position: allhome_north_molino,
                        map: map
                    });
                    marker_allhome_north_molino.addListener('click', function () {
                        infowindow_allhome_north_molino.open(map, marker_allhome_north_molino);
                    });
                    // allhome_north_molino

                    // allhome_sta_rosa
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>AllHome Sta. Rosa</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Vista Mall, Ground Floor, Sta. Rosa – Tagaytay Road, Don Jose Sta. Rosa Laguna.</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_allhome_sta_rosa = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_allhome_sta_rosa = new google.maps.Marker({
                        position: allhome_sta_rosa,
                        map: map
                    });
                    marker_allhome_sta_rosa.addListener('click', function () {
                        infowindow_allhome_sta_rosa.open(map, marker_allhome_sta_rosa);
                    });
                    // allhome_sta_rosa

                    // allhome_evia
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>AllHome Evia</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>3rd Level, Evia Lifestyle Center, Daang Hari Road, Almanza Dos, Las Pinas</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_allhome_evia = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_allhome_evia = new google.maps.Marker({
                        position: allhome_evia,
                        map: mapRecycling
                    });
                    marker_allhome_evia.addListener('click', function () {
                        infowindow_allhome_evia.open(mapRecycling, marker_allhome_evia);
                    });
                    // allhome_evia

                    // abenson_robinsons_place
                    var contentString = '<div id="bon"  class="map-lbl-content">' +
                        "<h5>Abenson Robinsons Place</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Abenson 2nd floor, Padre Faura Wing, Robinson’s Place Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_abenson_robinsons_place = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_abenson_robinsons_place = new google.maps.Marker({
                        position: abenson_robinsons_place,
                        map: map
                    });
                    marker_abenson_robinsons_place.addListener('click', function () {
                        infowindow_abenson_robinsons_place.open(map, marker_abenson_robinsons_place);
                    });
                    // abenson_robinsons_place

                    // uptown_bgc
                    var contentString = '<div id="uptown_bgc"  class="map-lbl-content">' +
                        "<h5>Uptown BGC Mall</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Unit R1-7, Upper Ground Floor, Uptown Mall, 36th Street, Taguig, Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_uptown_bgc = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_uptown_bgc = new google.maps.Marker({
                        position: uptown_bgc,
                        map: mapPopUpStore
                    });
                    marker_uptown_bgc.addListener('click', function () {
                        infowindow_uptown_bgc.open(map, marker_uptown_bgc);
                    });
                    // uptown_bgc

                    // sm_aura
                    var contentString = '<div id="sm_aura"  class="map-lbl-content">' +
                        "<h5>Homeworld SM Aura</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>LGF, The SM Store, Home Department, SM Aura, 8 Mckinley Pkwy, Taguig, Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_sm_aura = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_sm_aura = new google.maps.Marker({
                        position: sm_aura,
                        map: mapRecycling
                    });
                    marker_sm_aura.addListener('click', function () {
                        infowindow_sm_aura.open(map, marker_sm_aura);
                    });
                    // sm_aura

                    // rustans_gateway_recycle
                    var contentString = '<div id="rustans_gateway_recycle"  class="map-lbl-content">' +
                        "<h5>Homeworld SM Aura</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>LGF, The SM Store, Home Department, SM Aura, 8 Mckinley Pkwy, Taguig, Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_rustans_gateway_recycle = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_rustans_gateway_recycle = new google.maps.Marker({
                        position: rustans_gateway_recycle,
                        map: mapRecycling
                    });
                    marker_rustans_gateway_recycle.addListener('click', function () {
                        infowindow_rustans_gateway_recycle.open(map, marker_rustans_gateway_recycle);
                    });
                    // rustans_gateway_recycle

                    // abenson_glorietta_1
                    var contentString = '<div id="abenson_glorietta_1"  class="map-lbl-content">' +
                        "<h5>Abenson Glorietta 1</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>3rd Level, Glorietta 1 Ayala Center, Makati City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_abenson_glorietta_1 = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_abenson_glorietta_1 = new google.maps.Marker({
                        position: abenson_glorietta_1,
                        map: mapRecycling
                    });
                    marker_abenson_glorietta_1.addListener('click', function () {
                        infowindow_abenson_glorietta_1.open(map, marker_abenson_glorietta_1);
                    });
                    // abenson_glorietta_1

                    // nespresso-boutique
                    var contentString = '<div id="nespresso_boutique" class="map-lbl-content">' +
                        "<h5>Nespresso Boutique</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Level 1 Power Plant Mall, Rockwell Center, Poblacion, Makati City, Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_nespresso_boutique = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_nespresso_boutique = new google.maps.Marker({
                        position: nespresso_boutique,
                        map: map
                    });
                    marker_nespresso_boutique.addListener('click', function () {
                        infowindow_nespresso_boutique.open(map, marker_nespresso_boutique);
                    });

                    // greenbelt 5
                    var contentString = '<div id="podium_pop_up_store" class="map-lbl-content">' +
                        "<h5>Greenbelt 5</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Ground Level, Greenbelt 5, Legaspi St., Ayala Center, Makati City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_greenbelt_5 = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_greenbelt_5 = new google.maps.Marker({
                        position: greenbelt_5,
                        map: mapPopUpStore
                    });
                    marker_greenbelt_5.addListener('click', function () {
                        infowindow_greenbelt_5.open(map, marker_greenbelt_5);
                    });

                    // shangrila_plaza
                    var contentString = '<div id="podium_pop_up_store" class="map-lbl-content">' +
                        "<h5>Shangri-La Plaza</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Shangri-La Plaza Main Wing, Level 1,<br />Unit K-130 (beside Healthy Options) Ortigas Center,<br /> Mandaluyong, Metro Manila</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_shangrila_plaza = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_shangrila_plaza = new google.maps.Marker({
                        position: shangrila_plaza,
                        map: mapPopUpStore
                    });
                    marker_shangrila_plaza.addListener('click', function () {
                        infowindow_shangrila_plaza.open(map, marker_shangrila_plaza);
                    });

                    // alabang_town
                    var contentString = '<div class="map-lbl-content">' +
                        "<h5>Alabang Town Center</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Ground Floor Commerce Mall, Alabang Town Center, Commerce Avenue, Alabang Commercial Corporation, Metro Manila, Muntinlupa, 1770</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_alabang_town = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_alabang_town = new google.maps.Marker({
                        position: alabang_town,
                        map: mapPopUpStore
                    });
                    marker_alabang_town.addListener('click', function () {
                        infowindow_alabang_town.open(map, marker_alabang_town);
                    });

                    // one bonifacio
                    var contentString = '<div id="podium_pop_up_store" class="map-lbl-content">' +
                        "<h5>One Bonifacio High Street</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Upper Ground Floor, One Bonifacio High Street 3rd Avenue, BGC, Taguig City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_one_bonifacio = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_one_bonifacio = new google.maps.Marker({
                        position: one_bonifacio,
                        map: mapPopUpStore
                    });
                    marker_one_bonifacio.addListener('click', function () {
                        infowindow_one_bonifacio.open(map, marker_one_bonifacio);
                    });

                    // robinsons magnolia
                    var contentString = '<div id="podium_pop_up_store" class="map-lbl-content">' +
                        "<h5>Robinsons Magnolia</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p> Upper Ground level, Aurora Blvd cor Dona Hemady St, Quezon City.</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_robinsons_magnolia = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_robinsons_magnolia = new google.maps.Marker({
                        position: robinsons_magnolia,
                        map: mapPopUpStore
                    });
                    marker_robinsons_magnolia.addListener('click', function () {
                        infowindow_robinsons_magnolia.open(map, marker_robinsons_magnolia);
                    });

                    // trinoma
                    var contentString = '<div id="podium_pop_up_store" class="map-lbl-content">' +
                        "<h5>TriNoma Mall</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Level2, TriNoma Mall EDSA corner 173, Mindanao Ave, Quezon City, Metro Manila 1105 (infront of Taco Bell)</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_trinoma = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_trinoma = new google.maps.Marker({
                        position: trinoma,
                        map: mapPopUpStore
                    });
                    marker_trinoma.addListener('click', function () {
                        infowindow_trinoma.open(map, marker_trinoma);
                    });

                    // nespresso-boutique
                    var marker_nespresso_boutique_recycling = new google.maps.Marker({
                        position: nespresso_boutique_recycling,
                        map: map
                    });
                    marker_nespresso_boutique_recycling.addListener('click', function () {
                        infowindow_nespresso_boutique.open(map, marker_nespresso_boutique_recycling);
                    });
                    // nespresso-boutique

                    // nespresso-booth
                    var contentString = '<div id="nespresso_booth" class="map-lbl-content">' +
                        "<h5>Greenbelt 5</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>Ground Level, Greenbelt 5, Legaspi St., Ayala Center, Makati City</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_nespresso_booth = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_nespresso_booth = new google.maps.Marker({
                        position: nespresso_booth,
                        map: map
                    });
                    marker_nespresso_booth.addListener('click', function () {
                        infowindow_nespresso_booth.open(map, marker_nespresso_booth);
                    });

                    var marker_nespresso_booth_recycling = new google.maps.Marker({
                        position: nespresso_booth,
                        map: mapRecycling
                    });
                    marker_nespresso_booth_recycling.addListener('click', function () {
                        infowindow_nespresso_booth.open(mapRecycling, marker_nespresso_booth_recycling);
                    });
                    // nespresso-booth

                    // rustans_gateway
                    var contentString = '<div id="nespresso_booth" class="map-lbl-content">' +
                        "<h5>Rustan’s Gateway</h5>" +
                        '<div class="map-lbl-bodycontent">' +
                        '<p>2nd level, Rustan’s Gateway Mall, Araneta Center Cubao, Quezon City 1109</p>' +
                        '</div>' +
                        '</div>';
                    var infowindow_rustans_gateway = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker_rustans_gateway = new google.maps.Marker({
                        position: rustans_gateway,
                        map: map
                    });
                    marker_rustans_gateway.addListener('click', function () {
                        infowindow_rustans_gateway.open(map, marker_rustans_gateway);
                    });

                    // rustans_gateway

                }
            </script>
        </div>

    </div>

</main>

<!-- /content -->

<?php
	global $scripts;
	$scripts[] = 'js/components/productlist.js';
?>

<!-- get_footer -->
<?php get_footer(); ?>
