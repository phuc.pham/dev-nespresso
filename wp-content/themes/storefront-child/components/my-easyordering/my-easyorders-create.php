<?php
$activeMenu = 1;
$activeStep = 1;
?>
<div class="container">
    <div class="row">

        <!-- Sidebar account -->
        <div class="col-xs-12 col-md-3 account-sidebar">
            <?php include('components/my-account/menu.php'); ?>
        </div>
        <!-- endof: Sidebar account -->

        <div class="col-xs-12 col-md-8-5 account-detail my-easyorders my-easyorders__step1">
            <header>
                <h2>CREATION</h2>
            </header>

            <?php include('components/my-easyordering/steps.php'); ?>

            <div class="my-easyordering-header clearfix">
                <h3 class="pull-left my-easyordering-header__title">Products</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php" title="Cancel" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Cancel</a>
                    <a href="easyorder.php?create=2" title="Validate" class="btn btn-icon-right btn-green"><i class="fa fa-angle-right"></i>Validate</a>
                </div>
	            <p>Select your products on our dedicated catalog</p>
            </div>


            <div class="row create-easyorder__list-header">
                <div class="col-xs-4 no-padding">
                    <a href="#" title="Coffee capsules" class="create-easyorder__list-header__item active">
                        <div class="outer">
                            <div class="inner">
                                <i class="icon icon-capsule hidden-xs"></i> Coffee Original <span class="nespresso-badge pull-right">50</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4 no-padding">
                    <a href="#" title="Sweets" class="create-easyorder__list-header__item">
                        <div class="outer">
                            <div class="inner">
                                <i class="icon icon-Vertuo_Coffee_capsule_on hidden-xs"></i> Coffee Vertuo <span class="nespresso-badge pull-right">1</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4 no-padding">
                    <a href="#" title="Products" class="create-easyorder__list-header__item">
                        <div class="outer">
                            <div class="inner">
                                <i class="icon icon-Espresso_on-off hidden-xs"></i> Accessories <span class="nespresso-badge pull-right">3</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row create-easyorder__list">

                <div class="sbag-list col-xs-12">
                    <div class="sbag-list-container sbag-list-container--grey">
                        <div class="row">
                            <div class="sbag-list-inner col-xs-12">
                                <div class="sbag-list-head">
                                    <span class="sbag-head-article">Intenso</span>
                                    <span class="sbag-head-unit-price">Unit Price</span>
                                    <span class="sbag-head-qty">Quantity</span>
                                    <span class="sbag-head-total">Price</span>
                                </div>
                                <div class="sbag-list-content sbag-list-content--transparent">

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/product-09.png" alt="Arpeggio">
                                            Arpeggio
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.41
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Arpeggio"
                                                        data-price="0.41"
                                                        data-picture="images/product-09.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">10</span>
                                                            <?php
                                                            $productQty = 10;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                            $5.20
                                        </div>
                                    </div>

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/kazaar2.png" alt="Kazaar">
                                            Kazaar
                                            <span class="hidden-sm hidden-md show-xs">$0.41 x 10</span>
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.41
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        data-cart="true"
                                                        data-name="Kazaar"
                                                        data-price="0.75"
                                                        data-picture="images/kazaar2.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">0</span>
                                                            <?php
                                                            $productQty = 0;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price"></div>
                                    </div>



                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/darkhan.png" alt="Darkhan">
                                            Darkhan
                                            <span class="hidden-sm hidden-md show-xs">$0.37 x 10</span>
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.37
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Darkhan"
                                                        data-price="0.37"
                                                        data-picture="images/darkhan.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">0</span>
                                                            <?php
                                                            $productQty = 0;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                        </div>
                                    </div>

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/ristretto.png" alt="Ristretto">
                                            Ristretto
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.37
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Ristretto"
                                                        data-price="0.37"
                                                        data-picture="images/ristretto.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">0</span>
                                                            <?php
                                                            $productQty = 0;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="sbag-list-inner col-xs-12">
                                <div class="sbag-list-head">
                                    <span class="sbag-head-article">Espresso</span>
                                    <span class="sbag-head-unit-price">Unit Price</span>
                                    <span class="sbag-head-qty">Quantity</span>
                                    <span class="sbag-head-total">Price</span>
                                </div>
                                <div class="sbag-list-content sbag-list-content--transparent">

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/product-03.png" alt="Name of product">
                                            Kazaar
                                            <span class="hidden-sm hidden-md show-xs">$0.75 x 10</span>
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.75
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Kazaar"
                                                        data-price="0.75"
                                                        data-picture="images/product-03.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">10</span>
                                                            <?php
                                                            $productQty = 10;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                            $7.50
                                        </div>
                                    </div>

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/product-03.png" alt="Name of product">
                                            Kazaar
                                            <span class="hidden-sm hidden-md show-xs">$0.75 x 10</span>
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.75
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Kazaar"
                                                        data-price="0.75"
                                                        data-picture="images/product-03.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">10</span>
                                                            <?php
                                                            $productQty = 10;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                            $7.50
                                        </div>
                                    </div>

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/product-09.png" alt="Name of product">
                                            Arpeggio decafeinato
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.52
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Arpeggio decafeinato"
                                                        data-price="0.52"
                                                        data-picture="images/product-09.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">10</span>
                                                            <?php
                                                            $productQty = 10;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                            $5.20
                                        </div>
                                    </div>

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/product-03.png" alt="Name of product">
                                            Kazaar
                                            <span class="hidden-sm hidden-md show-xs">$0.75 x 10</span>
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.75
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-icon-right btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Kazaar"
                                                        data-price="0.75"
                                                        data-picture="images/product-03.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">0</span>
                                                            <?php
                                                            $productQty = 0;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                        </div>
                                    </div>

                                    <div class="sbag-item">
                                        <div class="sbag-name">
                                            <img src="images/product-09.png" alt="Name of product">
                                            Arpeggio decafeinato
                                        </div>
                                        <div class="sbag-unit-price">
                                            $0.52
                                        </div>
                                        <div class="sbag-qty">
                                            <div class="sbag-update">
                                                <button class="btn btn-green product-qty"
                                                        class="product-qty" data-cart="true"
                                                        data-name="Arpeggio decafeinato"
                                                        data-price="0.52"
                                                        data-picture="images/product-09.png"
                                                        data-qty-step="10"
                                                        data-replace="true"
                                                        ><span class="qty">0</span>
                                                            <?php
                                                            $productQty = 0;
                                                            include 'components/generics/qty-select.php';
                                                            ?></button>
                                            </div>
                                        </div>
                                        <div class="sbag-price">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row create-easyorder__list-footer">
                <div class="col-xs-12 clearfix">
                    <p>For all information regarding ingredients and allergens of our products, please refer to our <a href="#">product descriptions</a>.</p>
                </div>
            </div>
            <div class="row create-easyorder__list-footer">
                <div class="col-xs-12 clearfix">
                    <span class="pull-right">(price incl. VAT)</span>
                    <a href="#" title="Top of page" class="pull-left"><span class="fa fa-long-arrow-up"></span> Top of the page</a>
                </div>
            </div>

            <div class="row create-easyorder__detail orderlist-detail">
                <div class="col-xs-12 clearfix">
                    <div class="orderlist-detail__title clearfix">
                        <span class="pull-left">Capsules (30)</span>
                    </div>
                    <div class="orderlist-detail__item">
                        <div class="orderlist-detail__child orderlist-detail__child--name">Kazaar</div>
                        <div class="orderlist-detail__child orderlist-detail__child--price">$0.415</div>
                        <div class="orderlist-detail__child orderlist-detail__child--qty">x10</div>
                        <div class="orderlist-detail__child orderlist-detail__child--total">$4.15</div>
                    </div>
                    <div class="orderlist-detail__item">
                        <div class="orderlist-detail__child orderlist-detail__child--name">Cosi</div>
                        <div class="orderlist-detail__child orderlist-detail__child--price">$0.315</div>
                        <div class="orderlist-detail__child orderlist-detail__child--qty">x10</div>
                        <div class="orderlist-detail__child orderlist-detail__child--total">$3.15</div>
                    </div>
                </div>
            </div>

            <div class="create-easyorder__total row">
                <div class="col-xs-12 clearfix">
                    <span class="create-easyorder__total__txt pull-left"><strong>Estimated Total</strong></span>
                    <span class="create-easyorder__total__price pull-right"><strong>$66.05</strong></span>
                </div>
            </div>

            <div class="row create-easyorder__header-footer create-easyorder__header-footer--footer">
                <div>
                    <a href="easyorder,php" title="Back to my easyorders" class="link link--left">Back to my easyorders</a>
                    <a href="#" title="FAQ" class="link link--right pull-right">FAQ</a>
                </div>
                <div>
                    <a href="easyorder.php" title="Cancel" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Cancel</a>
                    <a href="easyorder.php?create=2" title="Next" class="btn btn-icon-right btn-green pull-right"><i class="fa fa-angle-right"></i>Next</a>
                </div>
            </div>

        </div>
    </div>
</div>