<?php
if(!isset($activeStep)) {
	$activeStep = 0;
}

$numSteps = 5;

$titles = array(
	'Select your products',
	'Define your shipping frequency',
	'Choose your delivery method',
	'Choose your payment method',
	'Validate your recurring order'
);


$links = array(
	'easyorder.php?create=1',
	'easyorder.php?create=2',
	'easyorder.php?create=3',
	'easyorder.php?create=4',
	'easyorder.php?create=5'
);

?>

<div class="wizard wizard__eo">
	<ol class="steps">
		<?php
			for($i=1; $i<= $numSteps; $i++) :

				$class = 'step';
				$activeLink = false;

				if($activeStep == $i) {
					$class .= ' active';
				}
				if($activeStep > $i) {
					$class .= ' completed';
					$containerTop = '';
					$activeLink = true;
				}
				if($activeStep + 1 == $i) {
					$activeLink = true;
				}

		?>
		<li class="<?php echo $class; ?>">
			<?php if($activeLink) :
					echo '<a class="step-content" href="'.$links[$i-1].'">';
				else :
					echo '<span class="step-content">';
				endif;
			?>
				<strong><?php echo $i ?></strong>
				<span><?php echo $titles[$i-1] ?></span>
			<?php if($activeLink) :
				echo '</a>';
			else :
				echo '</span>';
			endif;
			?>
		</li>
		<?php
			endfor;
		?>
	</ol>
</div>