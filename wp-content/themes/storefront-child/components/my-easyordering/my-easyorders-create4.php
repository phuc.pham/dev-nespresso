<?php
$activeMenu = 1;
$activeStep = 4;
?>
<div class="container">
    <div class="row">

        <!-- Sidebar account -->
        <div class="col-xs-12 col-md-3 account-sidebar">
            <?php include('components/my-account/menu.php'); ?>
        </div>
        <!-- endof: Sidebar account -->

        <div class="col-xs-12 col-md-8-5 account-detail my-easyorders  my-easyorders__step4 checkister">
            <header>
                <h2>CREATION</h2>
            </header>

            <?php include('components/my-easyordering/steps.php'); ?>

            <div class="my-easyordering-header clearfix">
                <h3 class="pull-left my-easyordering-header__title">Payment</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php?create=3" title="Back" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Back</a>
                    <a href="easyorder.php?create=5" title="Validate" class="btn btn-icon-right btn-green"><i class="fa fa-angle-right"></i>Validate</a>
                </div>
            </div>

            <div class="row main-content">
                <h3 class="main-content__title">Billing address</h3>
                <div class="bloc-delivery bloc-address" style="background: white">
	                <div class="row">
		                <dl class="col-sm-6">
			                <dt class="address__preview__label">Address</dt>
			                <dd class="address__preview__value">
				                Mr First name second name <br>
				                Adress line <br>
				                1234 City <br>
				                Switzerland
			                </dd>
			                <a href="adresses.php" class="link link--right" title="Edit address">Edit address</a>
		                </dl>
		                <dl class="col-sm-6">
			                <dt class="address__preview__label">Belling remark</dt>
			                <dd class="address__preview__value">Door Code 1234 – Leave on the door front if not there.</dd>
		                </dl>
		                <p class="pull-left col-sm-12">Currently set as your default billing address</p>
	                </div>
                    <div class="delivery-address-buttons">
                        <a href="adresses.php" class="btn btn-primary" title="Choose another delivery address">Choose another address</a>
                        <a href="adresses.php" class="btn btn-primary" title="Add another delivery address">Add another address</a>
                    </div>
                </div>
	            <h3 class="main-content__title">Payment method</h3>
                <div class="bloc-payment bloc-payment--other">
                    <div class="radio" role="group" aria-labelledby="payment_method_2">
                        <label for="payment_method_card">
                            <input type="radio" name="payment_method_card" id="payment_method_card" value="Credit card payment" title="Credit card payment" checked>
                            <i class="icon icon-Checkout_prefcredit_cart_off"></i>
                            <span>Credit card payment</span>
                        </label>
                    </div>
                    <div class="content">
                        <a href="#" title="Learn More" class="link link--right">Learn more</a>
                    </div>
                </div>
            </div>

	        <div class="row create-easyorder__header-footer create-easyorder__header-footer--footer">
		        <div>
			        <a href="#" title="FAQ" class="link link--right">FAQ</a>
		        </div>
		        <div>
			        <a href="easyorder.php?create=3" title="Back" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Back</a>
			        <a href="easyorder.php?create=5" title="Validate" class="btn btn-icon-right btn-green pull-right"><i class="fa fa-angle-right"></i>Validate</a>
		        </div>
	        </div>


        </div>
    </div>
</div>