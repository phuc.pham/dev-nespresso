<?php
    $activeMenu = 1;
?>
<div class="container">
    <div class="row">

        <!-- Sidebar account -->
        <div class="col-xs-12 col-md-3 account-sidebar">
            <?php include('components/my-account/menu.php'); ?>
        </div>
        <!-- endof: Sidebar account -->
        
        <div class="col-xs-12 col-md-8-5 account-detail my-easyorders my-easyorders__step0">
            <header>
                <h2>MY EASYORDERS</h2>
                <a class="btn btn-green btn-icon-right" href="easyorder.php?create=1" title="Create new Easyorder"><span>Create new Easyorder</span> <i class="icon icon-arrow_left"></i></a>
            </header>

	        <div class="my-easyordering-header my-easyordering-header--grey clearfix">
		        <h3 class="pull-left my-easyordering-header__title">Easyorder - 1</h3>
		        <div class="pull-right clearfix">
			        <div class="input-group input-group-generic">
				        <div class="dropdown dropdown--input dropdown--init">
					        <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						        <span class="mobile-label">Manage</span>
						        <span class="current">Manage</span>
					        </button>
					        <select tabindex="-1" name="language" id="language">
						        <option value="0">Manage</option>
						        <option value="1">Order details</option>
						        <option value="2">Products</option>
						        <option value="3">Frequency</option>
						        <option value="4">Delivery method</option>
						        <option value="5">Payment method</option>
						        <option value="6">Delete order</option>
					        </select>
				        </div>
			        </div>
		        </div>
	        </div>

	        <div class="my-expresscheckout-step">
                <div class="my-easyordering-content my-easyordering-content__products clearfix">
                    <div class="table-row">
                        <div class="easyordering-resume-main col-xs-12 col-sm-8">
                            <p class="message message__saved">
	                            <i class="icon icon-check"></i> Global success
                            </p>
                            <div class="my-easyorders-item__txt">
                                <i class="fa fa-calendar my-easyorders-item__icon"></i> <span class="uppercase">Every month</span>
                            </div>
                            <div class="my-easyorders-item__txt">
                                <i class="icon icon-Delivery_off my-easyorders-item__icon"></i> <span class="uppercase">Standard delivery</span> (2 working days)<br>
                                <span class="my-easyorders-item__txtsub my-easyorders-item__txtsub--withicon">Delivered to : 5 County Line Road, 4959 Springfield, ILLINOIS</span>
                            </div>
                            <div class="my-easyorders-item__txt">
                                <i class="icon icon-Espresso_on-off my-easyorders-item__icon"></i> <span class="uppercase">Product(s)</span>
                            </div>
                            <div class="sbag-list">
	                            <div class="sbag-list-head">
		                            <span class="sbag-head-article">Coffee capsules (2)</span>
		                            <span class="sbag-head-qty">Quantity</span>
	                            </div>
	                            <div class="sbag-list-content sbag-list-content--transparent">

		                            <div class="sbag-list-inner col-xs-12">
			                            <div class="sbag-item">
				                            <div class="sbag-name">
					                            <img src="images/product-09.png" alt="Arpeggio">
					                            Arpeggio
				                            </div>
				                            <div class="sbag-qty">20</div>
			                            </div>

			                            <div class="sbag-item">
				                            <div class="sbag-name">
					                            <img src="images/kazaar2.png" alt="Kazaar">
					                            Kazaar
					                            <span class="hidden-sm hidden-md show-xs">$0.41 x 10</span>
				                            </div>
				                            <div class="sbag-qty">10</div>
			                            </div>
	                                </div>
	                            </div>
                            </div>
                            <div class="sbag-list">
	                            <div class="sbag-list-head">
		                            <span class="sbag-head-article">Sweets (4)</span>
		                            <span class="sbag-head-qty">Quantity</span>
	                            </div>
	                            <div class="sbag-list-content sbag-list-content--transparent">

		                            <div class="sbag-list-inner col-xs-12">
			                            <div class="sbag-item">
				                            <div class="sbag-name">
					                            <img src="images/product-09.png" alt="Arpeggio">
					                            Arpeggio
				                            </div>
				                            <div class="sbag-qty">20</div>
			                            </div>

			                            <div class="sbag-item">
				                            <div class="sbag-name">
					                            <img src="images/kazaar2.png" alt="Kazaar">
					                            Kazaar
				                            </div>
				                            <div class="sbag-qty">10</div>
			                            </div>
			                            <div class="sbag-item">
				                            <div class="sbag-name">
					                            <img src="images/darkhan.png" alt="Arpeggio">
					                            Darkhan
				                            </div>
				                            <div class="sbag-qty">20</div>
			                            </div>

			                            <div class="sbag-item">
				                            <div class="sbag-name">
					                            <img src="images/ristretto.png" alt="Kazaar">
					                            Ristretto
				                            </div>
				                            <div class="sbag-qty">10</div>
			                            </div>
	                                </div>
	                            </div>
                            </div>
                        </div>
                        <div class="easyordering-resume-sidebar col-xs-12 col-sm-4">
                            <span>Next shipment on:</span>
                            <span class="lh-btn shipment-text">May 25th 2016 <i class="icon icon-Information"></i></span>
                            <a href="#" class="btn btn-primary" title="Change date">Change date</a>
                            <div class="total-products clearfix">
                                <span class="pull-left"><strong>TOTAL</strong><small>(incl. VTA)</small></span>
                                <span class="price pull-right">$66.05</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="my-expresscheckout-step">
	            <div class="my-easyordering-header my-easyordering-header--grey clearfix">
		            <h3 class="pull-left my-easyordering-header__title">Easyorder - 2</h3>
		            <div class="pull-right clearfix">
			            <div class="input-group input-group-generic">
				            <div class="dropdown dropdown--input dropdown--init">
					            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						            <span class="mobile-label">Manage</span>
						            <span class="current">Manage</span>
					            </button>
					            <select tabindex="-1" name="language" id="language">
						            <option value="0">Manage</option>
						            <option value="1">Order details</option>
						            <option value="2">Products</option>
						            <option value="3">Frequency</option>
						            <option value="4">Delivery method</option>
						            <option value="5">Payment method</option>
						            <option value="6">Delete order</option>
					            </select>
				            </div>
			            </div>
		            </div>
	            </div>
                <div class="my-easyordering-content my-easyordering-content__products clearfix">
                    <div class="table-row">
                        <div class="easyordering-resume-main col-xs-12 col-sm-8">
                            <div class="my-easyorders-item__txt">
                                <i class="fa fa-calendar my-easyorders-item__icon"></i> <span class="uppercase">Every month</span>
                            </div>
                            <div class="my-easyorders-item__txt">
                                <i class="icon icon-Delivery_off my-easyorders-item__icon"></i> <span class="uppercase">Standard delivery</span> (2 working days)<br>
                                <span class="my-easyorders-item__txtsub my-easyorders-item__txtsub--withicon">Delivered to : 5 County Line Road, 4959 Springfield, ILLINOIS</span>
                            </div>
                            <div class="my-easyorders-item__txt">
                                <i class="icon icon-Espresso_on-off my-easyorders-item__icon"></i> <span class="uppercase">Product(s)</span>
                            </div>
	                        <div class="sbag-list">
		                        <div class="sbag-list-head">
			                        <span class="sbag-head-article">Coffee capsules (3)</span>
			                        <span class="sbag-head-qty">Quantity</span>
		                        </div>
		                        <div class="sbag-list-content sbag-list-content--transparent">

			                        <div class="sbag-list-inner col-xs-12">
				                        <div class="sbag-item">
					                        <div class="sbag-name">
						                        <img src="images/product-09.png" alt="Arpeggio">
						                        Arpeggio
					                        </div>
					                        <div class="sbag-qty">20</div>
				                        </div>

				                        <div class="sbag-item">
					                        <div class="sbag-name">
						                        <img src="images/kazaar2.png" alt="Kazaar">
						                        Kazaar
						                        <span class="hidden-sm hidden-md show-xs">$0.41 x 10</span>
					                        </div>
					                        <div class="sbag-qty">10</div>
				                        </div>
			                        </div>
		                        </div>
	                        </div>
                        </div>
                        <div class="easyordering-resume-sidebar col-xs-12 col-sm-4">
                            <span>Next shipment on:</span>
                            <span class="lh-btn shipment-text">May 25th 2016 <i class="icon icon-Information"></i></span>
                            <a href="#" class="btn btn-primary" title="Change date">Change date</a>
                            <div class="total-products clearfix">
                                <span class="pull-left"><strong>TOTAL</strong><small>(incl. VTA)</small></span>
                                <span class="price pull-right">$66.05</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 account-detail__footer">
                    <div class="clearfix">
                        <div class="account-detail__footer__l"><a href="order-history.php" class="link--right link" title="Access to previous orders in «my order history»">Access to previous orders in «my order history»</a></div>
                        <div class="account-detail__footer__r"><a href="#" class="link--right link" title="FAQ">Faq</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
global $scripts;
$scripts[] = 'js/components/my-easyorder.js';
?>