<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>

<style>
.nespreso-hide {
	display: none !important;
}
</style>

<div id="" class="">
	<div>
		<a href="javascript:void(0)" class="taxonomy-add-new" id="machine-group-new">Add new group</a>
	</div>
</div>

<script type="text/javascript">

jQuery(document).ready(function($){
	MachineGroup.init($);
});

var MachineGroup = {

	that: null,

	init : function($) {

		this.machine_group_container = $('#nespresso-container-machine-group');
		this.product_type = $('#acf-field-product_type');

		that = this;

		this.ready($);
		this.hideShowMachineGroupBox();
	}, // init()

	ready : function($) {

		this.product_type.on('change', function() {
			that.hideShowMachineGroupBox();
		});

	}, //ready

	add : function() {

	},

	edit : function() {

	},

	delete : function() {

	},

	hideShowMachineGroupBox : function() {

		if ( this.product_type.val() == 'Machine' )
			this.machine_group_container.removeClass('nespreso-hide');
		else
			this.machine_group_container.addClass('nespreso-hide');
	},
};

</script>
