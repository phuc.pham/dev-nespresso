<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if(isset($_SESSION['sv_customer_id']) && $_SESSION['sv_customer_id'])
    wp_redirect('/coffee-list-question-discover',201);
$errors=[];
if(isset($_POST['sv_name']))
{
    global $wpdb;
    $table = $wpdb->prefix.'coffee_customers';
    if(!$_POST['sv_title'])
        $errors['title'] = 'Please select title';
    if(!$_POST['sv_name'])
        $errors['name'] = 'Please enter your name';
    if(!$_POST['sv_email'])
    {
        $errors['email'] = 'Please enter your email';
    }else
    if (!preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $_POST['sv_email'])) {
        $errors['email'] = 'Please enter  the correct email address (e.g. customer@gmail.com)';
    }else
    {
        $cus = $wpdb->get_row($wpdb->prepare( "select * from $table where email = %s" ,$_POST['sv_email']));
        if($cus)
        {
            $errors['email'] = 'Email is already in use';
        }
    }
    if(!$_POST['sv_phone'])
    {
        $errors['phone'] = 'Please enter your phone';
    }else
    if (!preg_match('/^(087|086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$/',$_POST['sv_phone'])) {
        $errors['phone'] = 'Should be numeric, start with \'0\' and with 9 characters minimum (example: 0912345678)';
    }else
    {
        $cus = $wpdb->get_row($wpdb->prepare( "select * from $table where phone = %s" ,$_POST['sv_phone']));
        if($cus)
        {
            $errors['phone'] = 'Phone number is already in use';
        }
    }
    //$sql = $wpdb->prepare( 'query' , value_parameter[, value_parameter ... ] );
    if(!$errors)
    {
        $data = array(
            'title' => $_POST['sv_title'], 
            'name' =>$_POST['sv_name'],
            'email' =>$_POST['sv_email'],
            'phone' =>$_POST['sv_phone'],
            'created_at' =>date('Y-m-d H:i:s')
        );
        $format = array('%s','%s','%s','%s');
        $wpdb->insert($table,$data,$format);
        $customer_id = $wpdb->insert_id;
        if($customer_id)
        {
            $_SESSION['sv_customer_id'] = $customer_id;
        }else{
            $errors['alert'] = 'Cannot register at this time, please try again later';
        }
    }
}
?>

<!-- content -->

<main id="content" class="fullPage fullPage__plp coffee-list">
    <!-- end banner -->
    <div class="container">
        <div class="row">
            <div class="productlist productlist-caps" id="JumpContent" tabindex="-1">
                <div class="productlist-main clearfix">
                    <div class="productlist-panel col-xs-12 col-md-12">
                        <div class="productlist-contents">                           
                            <div class="col-md-12 text-center">
                                <h1>
                                    WELCOME TO NESPRESSO BUILDING CORNER 2021 
                                </h1>
                            </div>
                            <section class="product-category text-center">
                            <?php if(!$customer_id){ ?>
                                <h2>PERSONAL INFORMATION</h2>
                                <div class="col-xs-12 my-infos__form">
                                            <small  class="text-danger"><?=$errors['alert']??''?></small>
                                            <div class="input-group input-group-generic is-dirty">
                                                <div class="row">
                                                    <form action="" method="POST" >                                                          
                                                            <div class="col-xs-3">
                                                                <select tabindex="-1" name="sv_title" id="title"
                                                                 class="dropdown dropdown--input dropdown--init"
                                                                 required>
                                                                        <option value="Mr" selected="selected"  >Mr</option>
                                                                        <option value="Ms" >Ms</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-9">     
                                                                <div class="">
                                                                    <input type="text"
                                                                        title="Full name"
                                                                        id="full_name_coffee_question"
                                                                        name="sv_name"
                                                                        placeholder="Full name"
                                                                        class="form-control col-sm-12 col-md-9"
                                                                        value="<?=$_POST['sv_name']??''?>"
                                                                        required
                                                                    >
                                                                    <small id="helpsv_name" data-msgvi="Vui lòng nhập họ và tên" data-msg="Please fill your name" class="text-danger"><?=$errors['name']??''?></small>
                                                                    <span class="mobile-label">Full name<span class="required">*</span></span>
                                                                </div>                                                           
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <div class="">
                                                                    <input type="email"
                                                                        title="E-mail address"
                                                                        id="email_address_coffee_question"
                                                                        name="sv_email"
                                                                        placeholder="E-mail address"
                                                                        class="form-control col-sm-12 col-md-9"
                                                                        value="<?=$_POST['sv_email']??''?>"
                                                                        required
                                                                    >
                                                                    <small id="helpsv_email" data-msgvi="Vui lòng nhập đúng email" data-msg="Please enter a valid your email" class="text-danger"><?=$errors['email']??''?></small>
                                                                    <span class="mobile-label">E-mail address<span class="required">*</span></span>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <div class="">
                                                                    <input type="phone"
                                                                        title="Phone number"
                                                                        id="phone_number_coffee_question"
                                                                        name="sv_phone"
                                                                        placeholder="Phone number"
                                                                        class="form-control col-sm-12 col-md-9"
                                                                        value="<?=$_POST['sv_phone']??''?>"
                                                                        data-regix="^(086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$"                                            
											required
                                                                    >
                                                                </div>
                                                                <small id="helpsv_email" data-msgvi="Vui lòng nhập đúng số điện thoại" data-msg="Please enter a valid your phone" class="text-danger"><?=$errors['phone']??''?></small>
                                                                <span class="mobile-label">Phone number<span class="required">*</span></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <button class="pull-right btn btn-primary  btn-save-info" type="submit" title="Save my informations">Submit</button>
                                                            </div>
                                                    </form>
                                                        


                                                    
                                                </div>

                                            

                                            </div>
                                        </div>
                           
                           
                           
                           
                           <?php }else{ ?>
                            <div class="col-md-12 text-center">
                                    <h2>
                                        We ‘ve got your information. Thank you for your time!
                                    </h2>
                                </div>

                              
                                <form action="/coffee-list-question-discover/">
                                    <div class="text-center" style="margin-bottom: 30px; margin-top: 10px;">
                                        <input type="hidden" value="<?=$customer_id?>" name="sv_customer"/>
                                        <button class="text-center" style="color: #fff;background: #866b0a;" type="submit" title="Save my informations"> COFFEE TEST NOW! </button>
                                    </div>
                                </form>
                                <?php } ?>
                           </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>

<style>
#content.fullPage__plp {
    background-image: url(http://nespresso-dev.annam-group.com/wp-content/uploads/2021/03/CÂU-2-ảnh-2.jpg);
    background-size: cover;
    background-position: center center;
    height: 500px;

}

.productlist .productlist-main .productlist-panel{
    background: unset;
}


</style>