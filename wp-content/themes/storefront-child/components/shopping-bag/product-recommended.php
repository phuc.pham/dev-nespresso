<div class="sbag-recommended-product clearfix">
    <?php if(!isset($recommendedProduct['url'])) : $recommendedProduct['url'] = '#'; endif; ?>
    <a href="<?php bloginfo('url');?>/<?php print($recommendedProduct['url']); ?>" title="<?= $recommendedProduct['title']; ?>"><img src="<?php echo get_stylesheet_directory_uri();?>/<?php print($recommendedProduct['img']); ?>" alt="<?php print($recommendedProduct['title']); ?>"></a>
    <h5><?php print($recommendedProduct['title']); ?></h5>
    <?php if (isset($recommendedProduct['text'])) : ?>
        <p><?php print($recommendedProduct['text']); ?></p>
    <?php endif; ?>
    <div class="sbag-actions">
        <span class="price">$<?php print($recommendedProduct['price']); ?></span>
	    <div class="container-btn pull-right">
		    <button class="btn btn-green btn-small add-to-cart"
		            data-cart="true"
		            data-type="<?php print($recommendedProduct['type']); ?>"
		            data-name="<?php print($recommendedProduct['title']); ?>"
		            data-price="<?php print($recommendedProduct['price']); ?>"
		            data-picture="<?php print($recommendedProduct['img']); ?>"
		            data-qty-step="1">
			    <i class="icon-plus"></i>
			    <span class="btn-add-qty"></span>
		    </button>
	    </div>
    </div>
</div>