<?php
	/**
	 * Nespresso Custom Theme developed by Minion Solutions
	 * Child theme of Storefront
	 *
	 * @link https://minionsolutions.com
	 *
	 * @since 1.0
	 * @version 1.0
	 */
	
	if (!defined('ABSPATH')) {
		exit; // Exit if accessed directly
	}

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/service.css">
<!-- content -->
<div class="container-fluid" id="service">
	<div class="row">
		<div class="service-page">
			<div class="service-main clearfix">
				<div class="col-xs-12">
					<div id="block" class="free-html" data-label="">
						<div class="g g_services2018">
							<div>
								<div class="g_landing">
									<div class="g_parallax">
										<div class="g_parallax__image is-parallax"
											 style="transform: translate3d(0px, -5px, 0px);">
											<img class="g_imgSrc g_imgSrc_loaded"
												 src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/why_nespresso_L.jpg">
										</div>
									</div>
									<section id="hero" data-label="Nespresso Services"
											 class="g_section g_hero g_center">
										<!---->
										<div class="g_restrict">
											<div class="g_content">
												<div class="g_text">
													<h1 class="g_h1">
														<strong class="v_brand" term="nespresso">Nespresso</strong> Services
													</h1>
													<div class="g_wysiwyg g_txt_XL">
														<p>
															<?php
																date_default_timezone_set('Asia/Ho_Chi_Minh');
																$status = "Good afternoon";
																$time = date("H");
																
																if ($time >= 5 && $time < 12) {
																	$status = "Good morning";
																} elseif ($time >= 13 && $time <= 18) {
																	$status = "Good afternoon";
																} elseif ($time >= 18) {
																	$status = "Good evening";
																}
															
															?>
															<?= $status ?>, how can we help
															you?
														</p>
													</div>
													<div class="g_wysiwyg g_txt_M">
														<p>As a <strong class="v_brand"
																		term="nespresso">Nespresso</strong>
															Member, you benefit from many personalized services, so that
															you can enjoy your coffee your way.
														</p>
													</div>
												</div>
											</div>
										</div>
									</section>
									<section id="services" data-label="Services" class="g_section g_services g_services4">
										<!---->  <!---->
										<ul class="g_servicesList g_row2">
											<li class="g_service pushed">
												<div class="g_serviceTileWrapper serviceId-ordering"
													 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/ordering.jpg');">
													<div class="g_serviceTile"
														 style="transform: translate3d(0px, 0px, 0px);">
														<header style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_serviceTileIcon"
																 style="transform: translate3d(0px, 0px, 0px);">
																<svg xmlns="http://www.w3.org/2000/svg"
																	 viewBox="0 0 100 100"
																	 class="injected-svg g_visual ordering"
																	 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/services2018/ordering.svg"
																	 aria-hidden="true">
																	<g class="icon-stroke">
																		<rect x="14.3" y="25.5" width="71.4"
																			  height="71.4"></rect>
																		<path d="M63.3,25.5v-9.4c0-7.3-5.8-13.1-13.1-13.1h-0.4c-7.3,0-13.1,5.8-13.1,13.1v9.4"></path>
																		<path d="M59.8,51.1l-3.2,12.6c-0.2,0.9-0.3,1.8-0.1,2.8c0.3,1.3,1,2.8,3.2,3.3c7.3,1.5,10.7-6.2,11.3-12c1.7-15-16.5-20.3-28.2-15.8c-6.7,2.6-11.6,9-13.5,15.8c-2.1,7.8,1.5,16.7,8.2,21.2c6.9,4.7,15.5,2.8,22.1-1.5c0,0,0,0,0.1,0"></path>
																		<path d="M57.8,59.2c0,0,0-7.6-7-8c-7-0.3-10,7-10.5,9.1c-0.7,2.6-1.1,8.6,4.4,10.4c5.5,1.8,10.8-2.3,12-6.9"></path>
																	</g>
																</svg>
															</div>
															<h2 class="g_h2">Ordering</h2>
														</header>
														<ul class="g_center g_serviceCtas g_row3"
															style="display: none;">
															<li>
																<a class="g_link">
																	<div class="g_serviceCta">
																		<svg xmlns="http://www.w3.org/2000/svg"
																			 viewBox="0 0 100 100"
																			 class="injected-svg g_visual ordering"
																			 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/ordering.svg"
																			 aria-hidden="true">
																			<g class="icon-stroke">
																				<rect x="14.3" y="25.5" width="71.4"
																					  height="71.4"></rect>
																				<path d="M63.3,25.5v-9.4c0-7.3-5.8-13.1-13.1-13.1h-0.4c-7.3,0-13.1,5.8-13.1,13.1v9.4"></path>
																				<path d="M59.8,51.1l-3.2,12.6c-0.2,0.9-0.3,1.8-0.1,2.8c0.3,1.3,1,2.8,3.2,3.3c7.3,1.5,10.7-6.2,11.3-12c1.7-15-16.5-20.3-28.2-15.8c-6.7,2.6-11.6,9-13.5,15.8c-2.1,7.8,1.5,16.7,8.2,21.2c6.9,4.7,15.5,2.8,22.1-1.5c0,0,0,0,0.1,0"></path>
																				<path d="M57.8,59.2c0,0,0-7.6-7-8c-7-0.3-10,7-10.5,9.1c-0.7,2.6-1.1,8.6,4.4,10.4c5.5,1.8,10.8-2.3,12-6.9"></path>
																			</g>
																		</svg>
																		<div class="g_wysiwyg g_txt_XS">
																			<p>Online</p>
																		</div>
																	</div>
																</a>
                                                            </li>
                                                            <li><a class="g_link">
                                                                    <div class="g_serviceCta">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             viewBox="0 0 100 100"
                                                                             class="injected-svg g_visual phone"
                                                                             data-src="/shared_res/agility/commons/img/services2018/phone.svg"
                                                                             aria-hidden="true">
                                                                            <g class="icon-stroke"
                                                                               stroke-linejoin="bevel">
                                                                                <path d="M4,23.5c0.4-6,2.6-11.7,7.4-16.2c1.9-1.8,3.6-4,7.6-3c1.6,0.4,3,1.2,4.1,2.3c4.6,4.5,9.1,9.1,13.6,13.7c2.5,2.5,2.3,6.6-0.3,9.2c-2.4,2.4-4.8,4.8-7.2,7.1c-0.4,0.4-0.5,1-0.2,1.5c3.7,6.8,7.9,13.4,13.4,18.9c5.8,5.7,12.5,9.8,19.5,14c0.5,0.3,0.8,0.3,1.2-0.2c2.3-2.4,4.7-4.8,7.1-7.1c2.3-2.3,5.4-2.9,8.1-1.4c1.2,0.6,2.1,1.7,3.1,2.6c4.2,4.2,8.4,8.4,12.6,12.6c2.5,2.6,2.6,6.6,0.2,9.3c-2.6,2.9-5.4,5.4-9,6.9c-3.6,1.5-7.3,2.1-11.2,1.7c-8.5-0.9-16.2-4-23.6-8.1c-3.7-2.1-7.2-4.4-10.5-7.1c-7-5.6-13.4-11.7-19.1-18.6c-2.9-3.5-5.7-7-8.3-10.7C7.2,43.5,3.3,33.1,4,23.5z"></path>
                                                                            </g>
                                                                        </svg>
                                                                        <div class="g_wysiwyg g_txt_XS"><p>By phone</p>
                                                                        </div>
                                                                    </div>
                                                                </a></li>
														</ul>
														<footer style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_wysiwyg g_txt_L"
																 style="transform: translate3d(0px, 0px, 0px);">
																<p>Order your capsules 24/7 from our website</p>
															</div>
														</footer>
														<button style="transform: translate3d(0px, 0px, 0px);"><i
																	class="fn_more"></i></button>
													</div>
												</div>
												<div class="g_serviceTilePushWrapper">
													<a href="#" class="g_serviceTilePush" style="color: #FFF;text-decoration: unset;">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
															 class="injected-svg g_visual ordering"
															 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/ordering.svg"
															 aria-hidden="true">
															<g class="icon-stroke">
																<rect x="14.3" y="25.5" width="71.4"
																	  height="71.4"></rect>
																<path d="M63.3,25.5v-9.4c0-7.3-5.8-13.1-13.1-13.1h-0.4c-7.3,0-13.1,5.8-13.1,13.1v9.4"></path>
																<path d="M59.8,51.1l-3.2,12.6c-0.2,0.9-0.3,1.8-0.1,2.8c0.3,1.3,1,2.8,3.2,3.3c7.3,1.5,10.7-6.2,11.3-12c1.7-15-16.5-20.3-28.2-15.8c-6.7,2.6-11.6,9-13.5,15.8c-2.1,7.8,1.5,16.7,8.2,21.2c6.9,4.7,15.5,2.8,22.1-1.5c0,0,0,0,0.1,0"></path>
																<path d="M57.8,59.2c0,0,0-7.6-7-8c-7-0.3-10,7-10.5,9.1c-0.7,2.6-1.1,8.6,4.4,10.4c5.5,1.8,10.8-2.3,12-6.9"></path>
															</g>
														</svg>
														<h4 style="color: #FFF;text-decoration: unset;" class="g_h4">Order online</h4>
														<div class="g_wysiwyg g_txt_S">
															<p>Receive your coffee, your
																way
															</p>
														</div>
														<a href="https://www.nespresso.vn/coffee-list/" style="text-decoration: unset;"
														   class="g_link g_link"><span
																	class="">Order coffee now</span><i
																	class="fn_angleLink"></i></a></a>
													<a href="tel:1900633474" class="g_serviceTilePush" style="color: #FFF;text-decoration: unset;">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
															 class="injected-svg g_visual phone"
															 data-src="/shared_res/agility/commons/img/services2018/phone.svg"
															 aria-hidden="true">
															<g class="icon-stroke" stroke-linejoin="bevel">
																<path d="M4,23.5c0.4-6,2.6-11.7,7.4-16.2c1.9-1.8,3.6-4,7.6-3c1.6,0.4,3,1.2,4.1,2.3c4.6,4.5,9.1,9.1,13.6,13.7c2.5,2.5,2.3,6.6-0.3,9.2c-2.4,2.4-4.8,4.8-7.2,7.1c-0.4,0.4-0.5,1-0.2,1.5c3.7,6.8,7.9,13.4,13.4,18.9c5.8,5.7,12.5,9.8,19.5,14c0.5,0.3,0.8,0.3,1.2-0.2c2.3-2.4,4.7-4.8,7.1-7.1c2.3-2.3,5.4-2.9,8.1-1.4c1.2,0.6,2.1,1.7,3.1,2.6c4.2,4.2,8.4,8.4,12.6,12.6c2.5,2.6,2.6,6.6,0.2,9.3c-2.6,2.9-5.4,5.4-9,6.9c-3.6,1.5-7.3,2.1-11.2,1.7c-8.5-0.9-16.2-4-23.6-8.1c-3.7-2.1-7.2-4.4-10.5-7.1c-7-5.6-13.4-11.7-19.1-18.6c-2.9-3.5-5.7-7-8.3-10.7C7.2,43.5,3.3,33.1,4,23.5z"></path>
															</g>
														</svg>
														<h4 style="color: #FFF;text-decoration: unset;" class="g_h4">Order by phone</h4>
														<div class="g_wysiwyg g_txt_S"><p>Our coffee specialists are
																available 24/7 to advise you on your coffee
																selection</p></div>
														<a href="tel:1900633474" style="text-decoration: unset;" class="g_link g_link"><span class="">1900 633 474</span><i
																	class="fn_angleLink"></i></a>
													</a>
												</div>
											</li>
											<li class="g_service">
												<div class="g_serviceTileWrapper serviceId-delivery g_imgSrc_loaded"
													 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/delivery.jpg');">
													<div class="g_serviceTile"
														 style="transform: translate3d(0px, 0px, 0px);">
														<header style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_serviceTileIcon"
																 style="transform: translate3d(0px, 0px, 0px);">
																<svg xmlns="http://www.w3.org/2000/svg"
																	 viewBox="0 0 100 100"
																	 class="injected-svg g_visual delivery"
																	 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/delivery.svg"
																	 aria-hidden="true">
																	<g class="icon-stroke" stroke-linejoin="bevel">
																		<line x1="69" y1="70" x2="26.9" y2="70"></line>
																		<path d="M10.7,70H4V23h59.1c5.3,0,10.2,3.1,12.5,7.9L80,42.8l16,5.8V70H85"></path>
																		<path d="M26.9,69.4c0,4.5-3.7,8.1-8.1,8.1c-4.5,0-8.1-3.7-8.1-8.1c0-4.5,3.6-8.1,8.1-8.1S26.9,64.9,26.9,69.4z"></path>
																		<path d="M85,69.4v0.3c0,3.2-1.9,6.1-4.8,7.3c-7,2.8-13.6-3.7-10.9-10.6c0.8-2,2.4-3.7,4.4-4.4C79.5,59.7,85,63.9,85,69.4z"></path>
																		<path d="M44,43h27.2L68,33.8c-1.1-3.4-4.4-5.8-8-5.8H44V43z"></path>
																	</g>
																</svg>
															</div>
															<h2 class="g_h2">Delivery</h2>
														</header>
														<ul class="g_center g_serviceCtas g_row3"
															style="display: none;">
															<li>
																<a href="#/delivery/standard-delivery" class="g_link">
																	<div class="g_serviceCta">
																		<svg xmlns="http://www.w3.org/2000/svg"
																			 viewBox="0 0 100 100"
																			 class="injected-svg g_visual delivery"
																			 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/delivery.svg"
																			 aria-hidden="true">
																			<g class="icon-stroke"
																			   stroke-linejoin="bevel">
																				<line x1="69" y1="70" x2="26.9"
																					  y2="70"></line>
																				<path d="M10.7,70H4V23h59.1c5.3,0,10.2,3.1,12.5,7.9L80,42.8l16,5.8V70H85"></path>
																				<path d="M26.9,69.4c0,4.5-3.7,8.1-8.1,8.1c-4.5,0-8.1-3.7-8.1-8.1c0-4.5,3.6-8.1,8.1-8.1S26.9,64.9,26.9,69.4z"></path>
																				<path d="M85,69.4v0.3c0,3.2-1.9,6.1-4.8,7.3c-7,2.8-13.6-3.7-10.9-10.6c0.8-2,2.4-3.7,4.4-4.4C79.5,59.7,85,63.9,85,69.4z"></path>
																				<path d="M44,43h27.2L68,33.8c-1.1-3.4-4.4-5.8-8-5.8H44V43z"></path>
																			</g>
																		</svg>
																		<div class="g_wysiwyg g_txt_XS">
																			<p>
																				standard-delivery
																			</p>
																		</div>
																	</div>
																</a>
															</li>
														</ul>
														<footer style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_wysiwyg g_txt_L"
																 style="transform: translate3d(0px, 0px, 0px);">
																<p>Being
																	delivered has never been so simple. We adapt to your
																	time and to your place to deliver you the best way
																	you deserve...
																</p>
															</div>
														</footer>
														<button style="transform: translate3d(0px, 0px, 0px);"><i
																	class="fn_more"></i></button>
													</div>
												</div>
											</li>
											<li class="g_service">
												<div class="g_serviceTileWrapper serviceId-customer-care"
													 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/customer-care.jpg');">
													<div class="g_serviceTile"
														 style="transform: translate3d(0px, 0px, 0px);">
														<header style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_serviceTileIcon"
																 style="transform: translate3d(0px, 0px, 0px);">
																<svg xmlns="http://www.w3.org/2000/svg"
																	 viewBox="0 0 100 100"
																	 class="injected-svg g_visual customer-care"
																	 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/customer-care.svg"
																	 aria-hidden="true">
																	<g class="icon-stroke">
																		<line x1="44" y1="25.5" x2="59.1"
																			  y2="25.5"></line>
																		<g stroke-linejoin="bevel">
																			<path d="M21.4,76.5h11.6h5L67.2,73c1.6-0.2,3.2-0.9,4.4-1.9c5.9-5.3,23.7-21.3,24.6-22.2c1.1-1.1,1.1-4.3-1.5-4.9c-2.7-0.6-5.4,1.4-7.1,2.5c-1.7,1.1-15.8,10.2-15.8,10.2s-2.5,2-4.6,2.2c-2,0.2-10.7,2.3-10.7,2.3l-6.1-4.6L64.8,50c0,0,3.9-1,2.3-4.6c-1.6-3.6-5.3-1.5-5.3-1.5s-14.5,4.9-16.8,5.3c-2.3,0.4-7.9,0.5-10.7,2.8s-5.3,6.1-5.3,6.1h-7.4"></path>
																			<path d="M63.1,21.4h3.6c2,0,3.6,1.6,3.6,3.6l0,0c0,2-1.6,3.6-3.6,3.6h-5"></path>
																			<polyline
																					points="39.1,17.4 44.6,40.6 59.2,40.6 63.9,17.4"></polyline>
																			<rect x="3.1" y="52" width="18.4"
																				  height="30.6"></rect>
																		</g>
																	</g>
																</svg>
															</div>
															<h2 class="g_h2">Customer Care</h2>
														</header>
														<ul class="g_center g_serviceCtas g_row3"
															style="display: none;">
															<li>
																<a href="#/customer-care/machine-assistance"
																   class="g_link">
																	<div class="g_serviceCta">
																		<svg xmlns="http://www.w3.org/2000/svg"
																			 viewBox="0 0 100 100"
																			 class="injected-svg g_visual machine-assistance"
																			 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/machine-assistance.svg"
																			 aria-hidden="true">
																			<g class="icon-stroke"
																			   stroke-linejoin="bevel">
																				<line x1="3" y1="36" x2="24"
																					  y2="36"></line>
																				<path d="M24,23L4,22.8V82h19l0.1-65H80v5.7c0,0,5.6-1.6,8.8,3S91,40,91,40h-5v-6h-8l0,31h19L80.5,82H22"></path>
																				<polygon
																						points="38,64 38,38 64,51 "></polygon>
																			</g>
																		</svg>
																		<div class="g_wysiwyg g_txt_XS">
																			<p>Machine
																				assistance
																			</p>
																		</div>
																	</div>
																</a>
															</li>
															<li>
																<a href="#/customer-care/24-customer-care"
																   class="g_link">
																	<div class="g_serviceCta">
																		<svg xmlns="http://www.w3.org/2000/svg"
																			 viewBox="0 0 100 100"
																			 class="injected-svg g_visual customer-care-24"
																			 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/customer-care-24.svg"
																			 aria-hidden="true">
																			<g class="icon-stroke">
																				<line x1="50" y1="20.6" x2="65.1"
																					  y2="20.6"></line>
																				<g stroke-linejoin="bevel">
																					<path d="M27.4,71.6H39h5L73.2,68c1.6-0.2,3.2-0.9,4.4-1.9c5.9-5.3,23.7-21.3,24.6-22.2c1.1-1.1,1.1-4.3-1.5-4.9s-5.4,1.4-7.1,2.5c-1.7,1.1-15.8,10.2-15.8,10.2s-2.5,2-4.6,2.3c-2,0.2-10.7,2.3-10.7,2.3l-6.1-4.6l14.5-6.6c0,0,3.9-1,2.3-4.6s-5.3-1.5-5.3-1.5s-14.5,4.9-16.8,5.3c-2.4,0.4-7.9,0.5-10.7,2.8c-2.8,2.3-5.3,6.1-5.3,6.1h-7.4"></path>
																					<path d="M69.1,16.5h3.6c2,0,3.6,1.6,3.6,3.6l0,0c0,2-1.6,3.6-3.6,3.6h-5"></path>
																					<polyline
																							points="45.1,12.4 50.6,35.7 65.2,35.7 69.9,12.4"></polyline>
																					<rect x="9.1" y="47.1" width="18.4"
																						  height="30.6"></rect>
																				</g>
																			</g>
																		</svg>
																		<div class="g_wysiwyg g_txt_XS">
																			<p>CUSTOMER CARE
																			</p>
																		</div>
																	</div>
																</a>
															</li>
															<li>
																<a href="#/customer-care/repair-services"
																   class="g_link">
																	<div class="g_serviceCta">
																		<svg xmlns="http://www.w3.org/2000/svg"
																			 viewBox="0 0 100 100"
																			 class="injected-svg g_visual machine-warranty"
																			 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/machine-warranty.svg"
																			 aria-hidden="true">
																			<g class="icon-stroke"
																			   stroke-linejoin="bevel">
																				<rect x="12" y="4" width="77"
																					  height="92"></rect>
																				<polyline
																						points="34,4 34,28.6 28.5,21.4 23,28.6 23,4"></polyline>
																				<path d="M34,45l-11-0.1V81h10l0.1-39H68v3h5.6c1.8,0,3.3,1.5,3.4,3.3l0.1,7.7H73v-4h-4l0,20h10.1l-8.8,9H33"></path>
																				<line x1="22" y1="54" x2="34"
																					  y2="54"></line>
																			</g>
																		</svg>
																		<div class="g_wysiwyg g_txt_XS">
																			<p>REPAIR SERVICE</p>
																		</div>
																	</div>
																</a>
															</li>
														</ul>
														<footer style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_wysiwyg g_txt_L"
																 style="transform: translate3d(0px, 0px, 0px);">
																<p>By internet or by phone, our Coffee Specialist are
																	always happy to help.</p>
															</div>
														</footer>
														<button style="transform: translate3d(0px, 0px, 0px);"><i
																	class="fn_more"></i></button>
													</div>
												</div>
											</li>
											<li class="g_service">
												<div class="g_serviceTileWrapper serviceId-recycling g_imgSrc_loaded"
													 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/recycling.jpg');">
													<div class="g_serviceTile"
														 style="transform: translate3d(0px, 0px, 0px);">
														<header style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_serviceTileIcon"
																 style="transform: translate3d(0px, 0px, 0px);">
																<svg xmlns="http://www.w3.org/2000/svg"
																	 viewBox="0 0 100 100"
																	 class="injected-svg g_visual recycling"
																	 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/recycling.svg"
																	 aria-hidden="true">
																	<g class="icon-stroke" stroke-linejoin="bevel"
																	   stroke-miterlimit="3">
																		<path d="M30.6,26.9c1.1-0.9,2.1-1.5,3.2-2.1l1.7-0.9c0.6-0.2,1.3-0.4,1.7-0.6c2.3-0.9,4.9-1.3,7.2-1.5c2.3,0,4.9,0.2,7,0.9c2.3,0.6,4.5,1.7,6.4,3c1.9,1.3,3.6,2.8,4.9,4.5c0.9,1.1,1.5,2.1,2.1,3.2c0.2,0.4,2.1,5,2.3,5.9h-4.5L73.5,50l10.6-10.8h-4.7C78,33,74.4,27.5,69.7,23.5c-4.9-4.3-10.8-6.6-16.8-7.2c-3-0.2-6,0-8.9,0.6c-3,0.6-5.5,1.7-8.1,3.2c-0.6,0.4-3.4,2.5-3.4,2.5c-1.1,1.1-3,3-3,3S27.9,27.7,27,29c-0.6,1.3-1.3,2.3-1.9,3.6c0.9-1.1,1.7-2.1,2.6-3C28.5,28.6,29.5,27.5,30.6,26.9z"></path>
																	</g>
																	<g class="icon-fill">
																		<path d="M69.3,63.6c-0.4,0.4-1.1,0.8-1.5,1.1l-1.7,1.1c-1.1,0.6-2.3,1.1-3.4,1.5c-4.7,1.7-9.8,1.9-14.2,0.6c-2.3-0.6-4.5-1.5-6.4-2.8c-0.8-0.6-1.9-1.3-2.8-1.9c-0.9-0.6-1.7-1.5-2.3-2.3c-1.5-1.7-2.6-3.6-3.4-5.5c-0.4-1.3-0.8-2.3-1.1-3.4h4.7L26.2,41.3L15.9,52h4.7c1.7,6.2,5.3,11.9,10,15.9c1.3,1.1,2.5,1.9,3.8,2.8c1.3,0.8,2.8,1.5,4.3,2.1c3,1.1,6,1.9,8.9,2.1c6,0.4,11.9-1.1,16.8-4c1.3-0.6,2.3-1.7,3.4-2.5l1.5-1.5c0.4-0.4,0.8-1.1,1.5-1.5l0.6-0.8l0.6-0.8c0.4-0.6,1.3-1.9,1.7-2.8l0.4-0.6c0.4-0.6,0.6-1.3,0.8-1.9C73.3,60.2,71.4,62.2,69.3,63.6z"></path>
																	</g>
																</svg>
															</div>
															<h2 class="g_h2">Recycling</h2>
														</header>
														<ul class="g_center g_serviceCtas g_row3"
															style="display: none;">
															<li>
																<a href="#/recycling/home-recycling" class="g_link">
																	<div class="g_serviceCta">
																		<svg xmlns="http://www.w3.org/2000/svg"
																			 viewBox="0 0 100 100"
																			 class="injected-svg g_visual home-recycling"
																			 data-src="/wp-content/themes/storefront-child/shared_res/agility/commons/img/home-recycling.svg"
																			 aria-hidden="true">
																			<g class="icon-stroke">
																				<polyline
																						points="3.2,40.7 49.9,9.1 96.8,40.7 "></polyline>
																				<polyline
																						points="12.7,34.3 12.7,90.9 89.3,90.9 89.3,35.6 "></polyline>
																				<path stroke-miterlimit="1"
																					  d="M30.5,38.5c1.1-0.9,2.1-1.5,3.2-2.1l1.7-0.8c0.6-0.2,1.3-0.4,1.7-0.6c2.3-0.9,4.9-1.3,7.2-1.5c2.3,0,4.9,0.2,7,0.9c2.3,0.6,4.5,1.7,6.4,3c1.9,1.3,3.6,2.8,4.9,4.5c0.9,1.1,1.5,2.1,2.1,3.2c0.2,0.4,2.2,5,2.3,5.9h-4.5l10.8,10.8l10.6-10.8h-4.7c-1.5-6.2-5.1-11.7-9.8-15.7c-4.9-4.3-10.8-6.6-16.8-7.2c-3-0.2-6,0-8.9,0.6c-3,0.6-5.5,1.7-8.1,3.2c-0.6,0.4-3.4,2.5-3.4,2.5c-1.1,1.1-3,3-3,3s-1.7,2.1-2.5,3.4c-0.6,1.3-1.3,2.3-1.9,3.6c0.9-1.1,1.7-2.1,2.6-3C28.4,40.2,29.5,39.1,30.5,38.5z"></path>
																			</g>
																			<path class="icon-fill"
																				  d="M69.2,75.2c-0.4,0.4-1.1,0.9-1.5,1.1L66,77.4c-1.1,0.6-2.3,1.1-3.4,1.5c-4.7,1.7-9.8,1.9-14.2,0.6c-2.3-0.6-4.5-1.5-6.4-2.8c-0.9-0.6-1.9-1.3-2.8-1.9c-0.9-0.7-1.7-1.5-2.3-2.3c-1.5-1.7-2.5-3.6-3.4-5.5c-0.4-1.3-0.9-2.3-1.1-3.4h4.7L26.1,52.9L15.9,63.5h4.7c1.7,6.2,5.3,11.9,10,15.9c1.3,1.1,2.5,1.9,3.8,2.8s2.8,1.5,4.3,2.1c3,1.1,6,1.9,8.9,2.1c6,0.4,11.9-1.1,16.8-4c1.3-0.6,2.3-1.7,3.4-2.6l1.5-1.5c0.4-0.4,0.8-1.1,1.5-1.5l0.6-0.9l0.6-0.8c0.4-0.6,1.3-1.9,1.7-2.8l0.4-0.6c0.4-0.6,0.6-1.3,0.9-1.9C73.3,71.8,71.3,73.8,69.2,75.2z"></path>
																		</svg>
																		<div class="g_wysiwyg g_txt_XS">
																			<p>Recycling at
																				home
																			</p>
																		</div>
																	</div>
																</a>
															</li>
														</ul>
														<footer style="transform: translate3d(0px, 0px, 0px);">
															<div class="g_wysiwyg g_txt_L"
																 style="transform: translate3d(0px, 0px, 0px);">
																<p>
																	Recycling services so that you can recycle your used
																	capsules daily.
																</p>
															</div>
														</footer>
														<button style="transform: translate3d(0px, 0px, 0px);"><i
																	class="fn_more"></i></button>
													</div>
												</div>
											</li>
										</ul>
									</section>
									<div role="alertdialog" class="g_popin serviceId-orderingg_popinService">
										<div class="g_popinOverlay"></div>
										<div class="g_popinFixed">
											<div class="serviceId-orderingg_popinRestrict g_popinRestrict g_light">
												<button class="g_btnRoundS g_btnClose" aria-label="close"><span
															class="">close</span><i class="fn_close"></i></button>
												<div class="serviceId-orderingg_popinContent g_popinContent">
													<article class="g_popinContent g_loaded">
														<div class="g_leftCol">
															<div class="g_table">
																<div class="g_header g_tableRow">
																	<div class="g_tableCell"></div>
																</div>
																<div class="g_main g_tableRow">
																	<div class="g_tableCell">
																		<div class="g_serviceTileWrapper serviceId-ordering unfolded g_imgSrc_loaded"
																			 activeservice="true"
																			 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/ordering.jpg');">
																			<div class="g_serviceTile">
																				<header>
																					<div class="g_serviceTileIcon">
																						<svg xmlns="http://www.w3.org/2000/svg"
																							 viewBox="0 0 100 100"
																							 class="injected-svg g_visual ordering"
																							 data-src="/shared_res/agility/commons/img/services2018/ordering.svg"
																							 aria-hidden="true">
																							<g class="icon-stroke">
																								<rect x="14.3" y="25.5"
																									  width="71.4"
																									  height="71.4"></rect>
																								<path d="M63.3,25.5v-9.4c0-7.3-5.8-13.1-13.1-13.1h-0.4c-7.3,0-13.1,5.8-13.1,13.1v9.4"></path>
																								<path d="M59.8,51.1l-3.2,12.6c-0.2,0.9-0.3,1.8-0.1,2.8c0.3,1.3,1,2.8,3.2,3.3c7.3,1.5,10.7-6.2,11.3-12c1.7-15-16.5-20.3-28.2-15.8c-6.7,2.6-11.6,9-13.5,15.8c-2.1,7.8,1.5,16.7,8.2,21.2c6.9,4.7,15.5,2.8,22.1-1.5c0,0,0,0,0.1,0"></path>
																								<path d="M57.8,59.2c0,0,0-7.6-7-8c-7-0.3-10,7-10.5,9.1c-0.7,2.6-1.1,8.6,4.4,10.4c5.5,1.8,10.8-2.3,12-6.9"></path>
																							</g>
																						</svg>
																					</div>
																					<h2 class="g_h2">Ordering</h2>
																					<p class="text-center">Order your
																						capsules 24/7 from our
																						website</p>
																				</header>
																				<ul class="g_center g_serviceCtas g_row3">
																					<li>
																						<a class="g_link">
																							<div class="g_serviceCta">
																								<svg xmlns="http://www.w3.org/2000/svg"
																									 viewBox="0 0 100 100"
																									 class="injected-svg g_visual ordering"
																									 data-src="/shared_res/agility/commons/img/services2018/ordering.svg"
																									 aria-hidden="true">
																									<g class="icon-stroke">
																										<rect x="14.3"
																											  y="25.5"
																											  width="71.4"
																											  height="71.4"></rect>
																										<path d="M63.3,25.5v-9.4c0-7.3-5.8-13.1-13.1-13.1h-0.4c-7.3,0-13.1,5.8-13.1,13.1v9.4"></path>
																										<path d="M59.8,51.1l-3.2,12.6c-0.2,0.9-0.3,1.8-0.1,2.8c0.3,1.3,1,2.8,3.2,3.3c7.3,1.5,10.7-6.2,11.3-12c1.7-15-16.5-20.3-28.2-15.8c-6.7,2.6-11.6,9-13.5,15.8c-2.1,7.8,1.5,16.7,8.2,21.2c6.9,4.7,15.5,2.8,22.1-1.5c0,0,0,0,0.1,0"></path>
																										<path d="M57.8,59.2c0,0,0-7.6-7-8c-7-0.3-10,7-10.5,9.1c-0.7,2.6-1.1,8.6,4.4,10.4c5.5,1.8,10.8-2.3,12-6.9"></path>
																									</g>
																								</svg>
																								<div class="g_wysiwyg g_txt_XS">
																									<p>Online</p>
																								</div>
																							</div>
																						</a>
																					</li>
																					<li><a class="g_link">
																							<div class="g_serviceCta">
																								<svg xmlns="http://www.w3.org/2000/svg"
																									 viewBox="0 0 100 100"
																									 class="injected-svg g_visual phone"
																									 data-src="/shared_res/agility/commons/img/services2018/phone.svg"
																									 aria-hidden="true">
																									<g class="icon-stroke"
																									   stroke-linejoin="bevel">
																										<path d="M4,23.5c0.4-6,2.6-11.7,7.4-16.2c1.9-1.8,3.6-4,7.6-3c1.6,0.4,3,1.2,4.1,2.3c4.6,4.5,9.1,9.1,13.6,13.7c2.5,2.5,2.3,6.6-0.3,9.2c-2.4,2.4-4.8,4.8-7.2,7.1c-0.4,0.4-0.5,1-0.2,1.5c3.7,6.8,7.9,13.4,13.4,18.9c5.8,5.7,12.5,9.8,19.5,14c0.5,0.3,0.8,0.3,1.2-0.2c2.3-2.4,4.7-4.8,7.1-7.1c2.3-2.3,5.4-2.9,8.1-1.4c1.2,0.6,2.1,1.7,3.1,2.6c4.2,4.2,8.4,8.4,12.6,12.6c2.5,2.6,2.6,6.6,0.2,9.3c-2.6,2.9-5.4,5.4-9,6.9c-3.6,1.5-7.3,2.1-11.2,1.7c-8.5-0.9-16.2-4-23.6-8.1c-3.7-2.1-7.2-4.4-10.5-7.1c-7-5.6-13.4-11.7-19.1-18.6c-2.9-3.5-5.7-7-8.3-10.7C7.2,43.5,3.3,33.1,4,23.5z"></path>
																									</g>
																								</svg>
																								<div class="g_wysiwyg g_txt_XS">
																									<p>By phone</p>
																								</div>
																							</div>
																						</a></li>
																				</ul>
																				<footer>
																					<div class="g_wysiwyg g_txt_L">
																					</div>
																				</footer>
																				<button><i class="fn_more"></i></button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="g_rightCol">
															<article class="g_table">
																<header class="g_tableRow g_headerRow">
																	<div class="g_tableCell"></div>
																</header>
																<section class="g_tableRow g_sectionRow">
																	<div class="g_tableCell">
																		<div class="g_scrollContainer">
																			<div class="g_scroll">
																				<div class="g_scrollOverflow">
																					<div class="g_scrollPadding">

																						<div class="g_serviceContent">
																							<h2 class="g_h2">
																								Ordering</h2>
																							<div class="g_serviceContent__section">
																								<h3 class="g_h3">Order
																									online</h3>
																								<div class="g_wysiwyg g_txt_L">
																									<p></p>
																								</div>
																								<div class="g_serviceContent__paragraph">
																									<!---->
																									<h4 class="g_h4">
																										Receive your
																										coffee, your
																										way</h4>
																									<div class="g_wysiwyg g_txt_M">
																										<p>Enjoy free
																											delivery
																											when you
																											spend a
																											minimum of
																											1,500,000
																											VND.
																											Delivery
																											takes up to
																											3 working
																											days for Ho
																											Chi Minh
																											City and up
																											to 5 days
																											for other
																											provinces.</p>
																									</div>
																								</div>
                                                                                                <a href="https://www.nespresso.vn/coffee-list/" class="g_btn g_btnBuy"><span class="">Order capsule</span></a>

																							</div>
																							<div class="g_serviceContent__section">
																								<h3 class="g_h3">Order
																									by phone</h3>
																								<div class="g_wysiwyg g_txt_L">
																									<p></p></div>
																								<div class="g_serviceContent__paragraph">
																									<!----> <h4
																											class="g_h4">
																										<strong class="v_brand"
																												term="nespresso">Nespresso</strong>
																										at your
																										fingertips</h4>
																									<div class="g_wysiwyg g_txt_M">
																										<p>Our Coffee Specialists are available 24/7 to advise you on your coffee selection and to make sure your order will be delivered tailored to your needs.</p>
																										<p>Call us at 1900 633 474</p>
																									</div>
																								</div> <!----> <!---->
																							</div>

																						</div>
                      
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</section>
																<footer class="g_tableRow g_footerRow">
																	<div class="g_tableCell"></div>
																</footer>
															</article>
														</div>
													</article>
												</div>
											</div>
										</div>
									</div>
									<div role="alertdialog" class="g_popin serviceId-deliveryg_popinService">
										<div class="g_popinOverlay"></div>
										<div class="g_popinFixed">
											<div class="serviceId-deliveryg_popinRestrict g_popinRestrict g_light">
												<button class="g_btnRoundS g_btnClose" aria-label="close"><span
															class="">close</span><i class="fn_close"></i></button>
												<div class="serviceId-deliveryg_popinContent g_popinContent">
													<article class="g_popinContent g_loaded">
														<div class="g_leftCol">
															<div class="g_table">
																<div class="g_header g_tableRow">
																	<div class="g_tableCell"></div>
																</div>
																<div class="g_main g_tableRow">
																	<div class="g_tableCell">
																		<div class="g_serviceTileWrapper serviceId-delivery unfolded g_imgSrc_loaded"
																			 activeservice="true"
																			 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/delivery.jpg');">
																			<div class="g_serviceTile">
																				<header>
																					<div class="g_serviceTileIcon">
																						<svg xmlns="http://www.w3.org/2000/svg"
																							 viewBox="0 0 100 100"
																							 class="injected-svg g_visual delivery"
																							 data-src="/shared_res/agility/commons/img/services2018/delivery.svg"
																							 aria-hidden="true">
																							<g class="icon-stroke"
																							   stroke-linejoin="bevel">
																								<line x1="69" y1="70"
																									  x2="26.9"
																									  y2="70"></line>
																								<path d="M10.7,70H4V23h59.1c5.3,0,10.2,3.1,12.5,7.9L80,42.8l16,5.8V70H85"></path>
																								<path d="M26.9,69.4c0,4.5-3.7,8.1-8.1,8.1c-4.5,0-8.1-3.7-8.1-8.1c0-4.5,3.6-8.1,8.1-8.1S26.9,64.9,26.9,69.4z"></path>
																								<path d="M85,69.4v0.3c0,3.2-1.9,6.1-4.8,7.3c-7,2.8-13.6-3.7-10.9-10.6c0.8-2,2.4-3.7,4.4-4.4C79.5,59.7,85,63.9,85,69.4z"></path>
																								<path d="M44,43h27.2L68,33.8c-1.1-3.4-4.4-5.8-8-5.8H44V43z"></path>
																							</g>
																						</svg>
																					</div>
																					<h2 class="g_h2">Delivery</h2>
																				</header>
																				<ul class="g_center g_serviceCtas g_row3">
																					<li>
																						<a href="#/delivery/standard-delivery"
																						   class="g_link router-link-exact-active router-link-active active">
																							<div class="g_serviceCta">
																								<svg xmlns="http://www.w3.org/2000/svg"
																									 viewBox="0 0 100 100"
																									 class="injected-svg g_visual delivery"
																									 data-src="/shared_res/agility/commons/img/services2018/delivery.svg"
																									 aria-hidden="true">
																									<g class="icon-stroke"
																									   stroke-linejoin="bevel">
																										<line x1="69"
																											  y1="70"
																											  x2="26.9"
																											  y2="70"></line>
																										<path d="M10.7,70H4V23h59.1c5.3,0,10.2,3.1,12.5,7.9L80,42.8l16,5.8V70H85"></path>
																										<path d="M26.9,69.4c0,4.5-3.7,8.1-8.1,8.1c-4.5,0-8.1-3.7-8.1-8.1c0-4.5,3.6-8.1,8.1-8.1S26.9,64.9,26.9,69.4z"></path>
																										<path d="M85,69.4v0.3c0,3.2-1.9,6.1-4.8,7.3c-7,2.8-13.6-3.7-10.9-10.6c0.8-2,2.4-3.7,4.4-4.4C79.5,59.7,85,63.9,85,69.4z"></path>
																										<path d="M44,43h27.2L68,33.8c-1.1-3.4-4.4-5.8-8-5.8H44V43z"></path>
																									</g>
																								</svg>
																								<div class="g_wysiwyg g_txt_XS">
																									<p>
																										standard-delivery</p>
																								</div>
																							</div>
																						</a>
																					</li>
																				</ul>
																				<footer>
																					<div class="g_wysiwyg g_txt_L">
																						<p>Being delivered has never
																							been so simple. We adapt to
																							your time and to your place
																							to deliver you the best way
																							you deserve...</p>
																					</div>
																				</footer>
																				<button><i class="fn_more"></i></button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="g_rightCol">
															<article class="g_table">
																<header class="g_tableRow g_headerRow">
																	<div class="g_tableCell"></div>
																</header>
																<section class="g_tableRow g_sectionRow">
																	<div class="g_tableCell">
																		<div class="g_scrollContainer">
																			<div class="g_scroll">
																				<div class="g_scrollOverflow">
																					<div class="g_scrollPadding">
																						<div class="g_serviceContent">
																							<h2 class="g_h2">
																								Delivery</h2>
																							<div class="g_serviceContent__section">
																								<h3 class="g_h3">
																									Standard
																									Delivery</h3>
																								<div class="g_wysiwyg g_txt_L">
																									<p></p>
																								</div>
																								<div class="g_serviceContent__paragraph">
																									<!---->
																									<h4 class="g_h4">
																										Enjoy your
																										<strong class="v_brand"
																												term="nespresso">Nespresso</strong>
																									</h4>
																									<div class="g_wysiwyg g_txt_M">
																										<p>
																											<a href="https://www.nespresso.com/my/en/registration?execution=e1s1">Creating
																												an
																												account</a>
																											with us will
																											give you
																											full access
																											to
																											everything
																											<strong class="v_brand"
																													term="nespresso">Nespresso</strong>.
																											Including
																											the delivery
																											of your
																											favourite
																											capsules
																											when it’s
																											most
																											convenient
																											for you.</p>
																										<p>Enjoy free
																											delivery
																											when you
																											spend a
																											minimum of
																											1,500,000
																											VND.
																											Delivery
																											takes up to
																											3 working
																											days for Ho
																											Chi Minh
																											City and up
																											to 5 days
																											for other
																											provinces.</p>
																									</div>
																								</div>
																								<!----> <!---->
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</section>
																<footer class="g_tableRow g_footerRow">
																	<div class="g_tableCell"></div>
																</footer>
															</article>
														</div>
													</article>
												</div>
											</div>
										</div>
									</div>
									<div role="alertdialog" class="g_popin serviceId-recyclingg_popinService">
										<div class="g_popinOverlay"></div>
										<div class="g_popinFixed">
											<div class="serviceId-recyclingg_popinRestrict g_popinRestrict g_light">
												<button class="g_btnRoundS g_btnClose" aria-label="close"><span
															class="">close</span><i class="fn_close"></i></button>
												<div class="serviceId-recyclingg_popinContent g_popinContent">
													<article class="g_popinContent g_loaded">
														<div class="g_leftCol">
															<div class="g_table">
																<div class="g_header g_tableRow">
																	<div class="g_tableCell"></div>
																</div>
																<div class="g_main g_tableRow">
																	<div class="g_tableCell">
																		<div class="g_serviceTileWrapper serviceId-recycling unfolded g_imgSrc_loaded"
																			 activeservice="true"
																			 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/recycling.jpg');">
																			<div class="g_serviceTile">
																				<header>
																					<div class="g_serviceTileIcon">
																						<svg xmlns="http://www.w3.org/2000/svg"
																							 viewBox="0 0 100 100"
																							 class="injected-svg g_visual recycling"
																							 data-src="/shared_res/agility/commons/img/services2018/recycling.svg"
																							 aria-hidden="true">
																							<g class="icon-stroke"
																							   stroke-linejoin="bevel"
																							   stroke-miterlimit="3">
																								<path d="M30.6,26.9c1.1-0.9,2.1-1.5,3.2-2.1l1.7-0.9c0.6-0.2,1.3-0.4,1.7-0.6c2.3-0.9,4.9-1.3,7.2-1.5c2.3,0,4.9,0.2,7,0.9c2.3,0.6,4.5,1.7,6.4,3c1.9,1.3,3.6,2.8,4.9,4.5c0.9,1.1,1.5,2.1,2.1,3.2c0.2,0.4,2.1,5,2.3,5.9h-4.5L73.5,50l10.6-10.8h-4.7C78,33,74.4,27.5,69.7,23.5c-4.9-4.3-10.8-6.6-16.8-7.2c-3-0.2-6,0-8.9,0.6c-3,0.6-5.5,1.7-8.1,3.2c-0.6,0.4-3.4,2.5-3.4,2.5c-1.1,1.1-3,3-3,3S27.9,27.7,27,29c-0.6,1.3-1.3,2.3-1.9,3.6c0.9-1.1,1.7-2.1,2.6-3C28.5,28.6,29.5,27.5,30.6,26.9z"></path>
																							</g>
																							<g class="icon-fill">
																								<path d="M69.3,63.6c-0.4,0.4-1.1,0.8-1.5,1.1l-1.7,1.1c-1.1,0.6-2.3,1.1-3.4,1.5c-4.7,1.7-9.8,1.9-14.2,0.6c-2.3-0.6-4.5-1.5-6.4-2.8c-0.8-0.6-1.9-1.3-2.8-1.9c-0.9-0.6-1.7-1.5-2.3-2.3c-1.5-1.7-2.6-3.6-3.4-5.5c-0.4-1.3-0.8-2.3-1.1-3.4h4.7L26.2,41.3L15.9,52h4.7c1.7,6.2,5.3,11.9,10,15.9c1.3,1.1,2.5,1.9,3.8,2.8c1.3,0.8,2.8,1.5,4.3,2.1c3,1.1,6,1.9,8.9,2.1c6,0.4,11.9-1.1,16.8-4c1.3-0.6,2.3-1.7,3.4-2.5l1.5-1.5c0.4-0.4,0.8-1.1,1.5-1.5l0.6-0.8l0.6-0.8c0.4-0.6,1.3-1.9,1.7-2.8l0.4-0.6c0.4-0.6,0.6-1.3,0.8-1.9C73.3,60.2,71.4,62.2,69.3,63.6z"></path>
																							</g>
																						</svg>
																					</div>
																					<h2 class="g_h2">Recycling</h2>
																				</header>
																				<ul class="g_center g_serviceCtas g_row3">
																					<li>
																						<a href="#/home-recycling"
																						   class="g_link">
																							<div class="g_serviceCta">
																								<svg xmlns="http://www.w3.org/2000/svg"
																									 viewBox="0 0 100 100"
																									 class="injected-svg g_visual home-recycling"
																									 data-src="/shared_res/agility/commons/img/services2018/home-recycling.svg"
																									 aria-hidden="true">
																									<g class="icon-stroke">
																										<polyline
																												points="3.2,40.7 49.9,9.1 96.8,40.7 "></polyline>
																										<polyline
																												points="12.7,34.3 12.7,90.9 89.3,90.9 89.3,35.6 "></polyline>
																										<path stroke-miterlimit="1"
																											  d="M30.5,38.5c1.1-0.9,2.1-1.5,3.2-2.1l1.7-0.8c0.6-0.2,1.3-0.4,1.7-0.6c2.3-0.9,4.9-1.3,7.2-1.5c2.3,0,4.9,0.2,7,0.9c2.3,0.6,4.5,1.7,6.4,3c1.9,1.3,3.6,2.8,4.9,4.5c0.9,1.1,1.5,2.1,2.1,3.2c0.2,0.4,2.2,5,2.3,5.9h-4.5l10.8,10.8l10.6-10.8h-4.7c-1.5-6.2-5.1-11.7-9.8-15.7c-4.9-4.3-10.8-6.6-16.8-7.2c-3-0.2-6,0-8.9,0.6c-3,0.6-5.5,1.7-8.1,3.2c-0.6,0.4-3.4,2.5-3.4,2.5c-1.1,1.1-3,3-3,3s-1.7,2.1-2.5,3.4c-0.6,1.3-1.3,2.3-1.9,3.6c0.9-1.1,1.7-2.1,2.6-3C28.4,40.2,29.5,39.1,30.5,38.5z"></path>
																									</g>
																									<path class="icon-fill"
																										  d="M69.2,75.2c-0.4,0.4-1.1,0.9-1.5,1.1L66,77.4c-1.1,0.6-2.3,1.1-3.4,1.5c-4.7,1.7-9.8,1.9-14.2,0.6c-2.3-0.6-4.5-1.5-6.4-2.8c-0.9-0.6-1.9-1.3-2.8-1.9c-0.9-0.7-1.7-1.5-2.3-2.3c-1.5-1.7-2.5-3.6-3.4-5.5c-0.4-1.3-0.9-2.3-1.1-3.4h4.7L26.1,52.9L15.9,63.5h4.7c1.7,6.2,5.3,11.9,10,15.9c1.3,1.1,2.5,1.9,3.8,2.8s2.8,1.5,4.3,2.1c3,1.1,6,1.9,8.9,2.1c6,0.4,11.9-1.1,16.8-4c1.3-0.6,2.3-1.7,3.4-2.6l1.5-1.5c0.4-0.4,0.8-1.1,1.5-1.5l0.6-0.9l0.6-0.8c0.4-0.6,1.3-1.9,1.7-2.8l0.4-0.6c0.4-0.6,0.6-1.3,0.9-1.9C73.3,71.8,71.3,73.8,69.2,75.2z"></path>
																								</svg>
																								<div class="g_wysiwyg g_txt_XS">
																									<p>Recycling at
																										home</p>
																								</div>
																							</div>
																						</a>
																					</li>
																				</ul>
																				<footer>
																					<div class="g_wysiwyg g_txt_L">
																						<p>Recycling services so that
																							you can recycle your used
																							capsules daily.</p>
																					</div>
																				</footer>
																				<button><i class="fn_more"></i></button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="g_rightCol">
															<article class="g_table">
																<header class="g_tableRow g_headerRow">
																	<div class="g_tableCell"></div>
																</header>
																<section class="g_tableRow g_sectionRow"
																		 id="home-recycling">
																	<div class="g_tableCell">
																		<div class="g_scrollContainer">
																			<div class="g_scroll">
																				<div class="g_scrollOverflow">
																					<div class="g_scrollPadding">
																						<div class="g_serviceContent">
																							<h2 class="g_h2">
																								Recycling</h2>
																							<div class="g_serviceContent__section">
																								<h3 class="g_h3">
																									Recycling at
																									home</h3>
																								<div class="g_wysiwyg g_txt_L">
																									<p></p>
																								</div>
																								<div class="g_serviceContent__paragraph">
																									<!---->
																									<div class="g_wysiwyg g_txt_M">
																										<p> When the
																											courier
																											delivers
																											your next
																											order to
																											your home,
																											simply pass
																											him the
																											Recycling
																											Bag.</p>
																									</div>
																								</div>
																								<!----> <!---->
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</section>
																<footer class="g_tableRow g_footerRow">
																	<div class="g_tableCell"></div>
																</footer>
															</article>
														</div>
													</article>
												</div>
											</div>
										</div>
									</div>
									<div role="alertdialog" class="g_popin serviceId-customer-careg_popinService">
										<div class="g_popinOverlay"></div>
										<div class="g_popinFixed">
											<div class="serviceId-customer-careg_popinRestrict g_popinRestrict g_light">
												<button class="g_btnRoundS g_btnClose" aria-label="close"><span
															class="">close</span><i class="fn_close"></i></button>
												<div class="serviceId-customer-careg_popinContent g_popinContent">
													<article class="g_popinContent g_loaded">
														<div class="g_leftCol">
															<div class="g_table">
																<div class="g_header g_tableRow">
																	<div class="g_tableCell"></div>
																</div>
																<div class="g_main g_tableRow">
																	<div class="g_tableCell">
																		<div class="g_serviceTileWrapper serviceId-customer-care unfolded g_imgSrc_loaded"
																			 activeservice="true"
																			 style="background-image: url('/wp-content/themes/storefront-child/shared_res/agility/commons/img/customer-care.jpg');">
																			<div class="g_serviceTile">
																				<header>
																					<div class="g_serviceTileIcon">
																						<svg xmlns="http://www.w3.org/2000/svg"
																							 viewBox="0 0 100 100"
																							 class="injected-svg g_visual customer-care"
																							 data-src="/shared_res/agility/commons/img/services2018/customer-care.svg"
																							 aria-hidden="true">
																							<g class="icon-stroke">
																								<line x1="44" y1="25.5"
																									  x2="59.1"
																									  y2="25.5"></line>
																								<g stroke-linejoin="bevel">
																									<path d="M21.4,76.5h11.6h5L67.2,73c1.6-0.2,3.2-0.9,4.4-1.9c5.9-5.3,23.7-21.3,24.6-22.2c1.1-1.1,1.1-4.3-1.5-4.9c-2.7-0.6-5.4,1.4-7.1,2.5c-1.7,1.1-15.8,10.2-15.8,10.2s-2.5,2-4.6,2.2c-2,0.2-10.7,2.3-10.7,2.3l-6.1-4.6L64.8,50c0,0,3.9-1,2.3-4.6c-1.6-3.6-5.3-1.5-5.3-1.5s-14.5,4.9-16.8,5.3c-2.3,0.4-7.9,0.5-10.7,2.8s-5.3,6.1-5.3,6.1h-7.4"></path>
																									<path d="M63.1,21.4h3.6c2,0,3.6,1.6,3.6,3.6l0,0c0,2-1.6,3.6-3.6,3.6h-5"></path>
																									<polyline
																											points="39.1,17.4 44.6,40.6 59.2,40.6 63.9,17.4"></polyline>
																									<rect x="3.1" y="52"
																										  width="18.4"
																										  height="30.6"></rect>
																								</g>
																							</g>
																						</svg>
																					</div>
																					<h2 class="g_h2">Customer Care</h2>
																					<p class="text-center">By internet
																						or by phone, our Coffee
																						Specialist are always happy to
																						help.</p>
																				</header>
																				<ul class="g_center g_serviceCtas g_row3">
																					<li>
																						<a href="#machine-assistance"
																						   class="g_link">
																							<div class="g_serviceCta">
																								<svg xmlns="http://www.w3.org/2000/svg"
																									 viewBox="0 0 100 100"
																									 class="injected-svg g_visual machine-assistance"
																									 data-src="/shared_res/agility/commons/img/services2018/machine-assistance.svg"
																									 aria-hidden="true">
																									<g class="icon-stroke"
																									   stroke-linejoin="bevel">
																										<line x1="3"
																											  y1="36"
																											  x2="24"
																											  y2="36"></line>
																										<path d="M24,23L4,22.8V82h19l0.1-65H80v5.7c0,0,5.6-1.6,8.8,3S91,40,91,40h-5v-6h-8l0,31h19L80.5,82H22"></path>
																										<polygon
																												points="38,64 38,38 64,51 "></polygon>
																									</g>
																								</svg>
																								<div class="g_wysiwyg g_txt_XS">
																									<p>Machine
																										assistance</p>
																								</div>
																							</div>
																						</a>
																					</li>
																					<li>
																						<!--																							<a href="#/customer-care" class="g_link router-link-exact-active router-link-active active">-->
																						<a href="#customer-care"
																						   class="g_link">
																							<div class="g_serviceCta">
																								<svg xmlns="http://www.w3.org/2000/svg"
																									 viewBox="0 0 100 100"
																									 class="injected-svg g_visual customer-care-24"
																									 data-src="/shared_res/agility/commons/img/services2018/customer-care-24.svg"
																									 aria-hidden="true">
																									<g class="icon-stroke">
																										<line x1="50"
																											  y1="20.6"
																											  x2="65.1"
																											  y2="20.6"></line>
																										<g stroke-linejoin="bevel">
																											<path d="M27.4,71.6H39h5L73.2,68c1.6-0.2,3.2-0.9,4.4-1.9c5.9-5.3,23.7-21.3,24.6-22.2c1.1-1.1,1.1-4.3-1.5-4.9s-5.4,1.4-7.1,2.5c-1.7,1.1-15.8,10.2-15.8,10.2s-2.5,2-4.6,2.3c-2,0.2-10.7,2.3-10.7,2.3l-6.1-4.6l14.5-6.6c0,0,3.9-1,2.3-4.6s-5.3-1.5-5.3-1.5s-14.5,4.9-16.8,5.3c-2.4,0.4-7.9,0.5-10.7,2.8c-2.8,2.3-5.3,6.1-5.3,6.1h-7.4"></path>
																											<path d="M69.1,16.5h3.6c2,0,3.6,1.6,3.6,3.6l0,0c0,2-1.6,3.6-3.6,3.6h-5"></path>
																											<polyline
																													points="45.1,12.4 50.6,35.7 65.2,35.7 69.9,12.4"></polyline>
																											<rect x="9.1"
																												  y="47.1"
																												  width="18.4"
																												  height="30.6"></rect>
																										</g>
																									</g>
																								</svg>
																								<div class="g_wysiwyg g_txt_XS">
																									<p>CUSTOMER CARE</p>
																								</div>
																							</div>
																						</a>
																					</li>
																					<li>
																						<a href="#repair-services"
																						   class="g_link">
																							<div class="g_serviceCta">
																								<svg xmlns="http://www.w3.org/2000/svg"
																									 viewBox="0 0 100 100"
																									 class="injected-svg g_visual machine-warranty"
																									 data-src="/shared_res/agility/commons/img/services2018/machine-warranty.svg"
																									 aria-hidden="true">
																									<g class="icon-stroke"
																									   stroke-linejoin="bevel">
																										<rect x="12"
																											  y="4"
																											  width="77"
																											  height="92"></rect>
																										<polyline
																												points="34,4 34,28.6 28.5,21.4 23,28.6 23,4"></polyline>
																										<path d="M34,45l-11-0.1V81h10l0.1-39H68v3h5.6c1.8,0,3.3,1.5,3.4,3.3l0.1,7.7H73v-4h-4l0,20h10.1l-8.8,9H33"></path>
																										<line x1="22"
																											  y1="54"
																											  x2="34"
																											  y2="54"></line>
																									</g>
																								</svg>
																								<div class="g_wysiwyg g_txt_XS">
																									<p>REPAIR
																										SERVICE</p>
																								</div>
																							</div>
																						</a>
																					</li>
																				</ul>
																				<footer>
																					<div class="g_wysiwyg g_txt_L">
																						<!--																							<p>By internet, on mobile or by phone, order your capsules 24h/7 with the solution that best suits your needs…</p>-->
																					</div>
																				</footer>
																				<button><i class="fn_more"></i></button>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="g_rightCol">
                                                            <article class="g_table">
                                                                <header class="g_tableRow g_headerRow">
                                                                    <div class="g_tableCell"></div>
                                                                </header>
                                                                <section class="g_tableRow g_sectionRow">
                                                                    <div class="g_tableCell">
                                                                        <div class="g_scrollContainer">
                                                                            <div class="g_scroll">
                                                                                <div class="g_scrollOverflow">
                                                                                    <div class="g_scrollPadding">
                                                                                        <div class="g_serviceContent">
                                                                                            <h2 class="g_h2">Customer
                                                                                                Care</h2>
                                                                                            <div class="g_serviceContent__section"
                                                                                                 id="machine-assistance"
                                                                                                 name="machine-assistance">
                                                                                                <h3 class="g_h3">Machine
                                                                                                    assistance</h3>
                                                                                                <div class="g_wysiwyg g_txt_L">
                                                                                                    <p></p>
                                                                                                </div>
                                                                                                <div class="g_serviceContent__paragraph">
                                                                                                    <!---->
                                                                                                    <h4 class="g_h4">
                                                                                                        Keep getting the
                                                                                                        most out of your
                                                                                                        coffee
                                                                                                        machine</h4>
                                                                                                    <div class="g_wysiwyg g_txt_M">
                                                                                                        <p>Would you
                                                                                                            like advice
                                                                                                            to keep your
                                                                                                            machine
                                                                                                            running
                                                                                                            smoothly?
                                                                                                            We’re happy
                                                                                                            to help. We
                                                                                                            offer tips
                                                                                                            on how to
                                                                                                            clean,
                                                                                                            descale and
                                                                                                            program your
                                                                                                            coffee
                                                                                                            machine, or
                                                                                                            maybe solve
                                                                                                            potential
                                                                                                            issues?</p>
                                                                                                        <p>Select your
                                                                                                            <strong class="v_brand black"
                                                                                                                    term="nespresso">Nespresso</strong>
                                                                                                            machine
                                                                                                            below and
                                                                                                            let us guide
                                                                                                            you with our
                                                                                                            videos.</p>
                                                                                                        <p>You can find
                                                                                                            them
                                                                                                            <a href="#">here</a>.
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                
                                                                                                <!----> <!---->
                                                                                            </div>
                                                                                            <div class="g_serviceContent__section"
                                                                                                 id="customer-care"
                                                                                                 name="customer-care">
                                                                                                <h3 class="g_h3">
                                                                                                    CUSTOMER CARE</h3>
                                                                                                <div class="g_wysiwyg g_txt_L">
                                                                                                    <p></p>
                                                                                                </div>
                                                                                                <div class="g_serviceContent__paragraph">
                                                                                                    <!---->
                                                                                                    <h4 class="g_h4">Our
                                                                                                        lines are open
                                                                                                        from</h4>
                                                                                                    <div class="g_wysiwyg g_txt_M">
                                                                                                        <p><strong>Monday
                                                                                                                -
                                                                                                                Saturday,
                                                                                                                10am to
                                                                                                                6pm.</strong>
                                                                                                        </p>
                                                                                                        <p><u>1900 633
                                                                                                                474</u>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                             
                                                                                                <!----> <!---->
                                                                                            </div>
                                                                                            <div class="g_serviceContent__section"
                                                                                                 id="repair-services"
                                                                                                 name="repair-services">
                                                                                                <h3 class="g_h3">REPAIR
                                                                                                    SERVICE</h3>
                                                                                                <div class="g_wysiwyg g_txt_L">
                                                                                                    <p></p>
                                                                                                </div>
                                                                                                <div class="g_serviceContent__paragraph">
                                                                                                    <!---->
                                                                                                    <h4 class="g_h4">
                                                                                                        REPAIR
                                                                                                        SERVICE</h4>
                                                                                                    <div class="g_wysiwyg g_txt_M">
                                                                                                        <p>Machine
                                                                                                            Assistance
                                                                                                            is offered
                                                                                                            to you if
                                                                                                            your machine
                                                                                                            requires
                                                                                                            repair and
                                                                                                            is within
                                                                                                            the 2 years
                                                                                                            warranty
                                                                                                            period.</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                
                                                                                                <!----> <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <footer class="g_tableRow g_footerRow">
                                                                    <div class="g_tableCell"></div>
                                                                </footer>
                                                            </article>
														</div>
													</article>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	global $scripts;
	$scripts[] = 'js/components/services.js';
?>


<script>
	function removeclassmodal(idobject) {
		jQuery(`${idobject}g_popinService`).removeClass('g_popinOpened');
	}

	function animationcard(idobject, i) {
		jQuery(idobject).hover(function () {
			jQuery(`${idobject} .g_center`).removeClass("ctas-leave-active ctas-leave-to");
			jQuery(`${idobject} .g_center`).addClass("ctas-enter-active ctas-enter-to");
			jQuery(idobject).addClass("flipped");


			jQuery(`${idobject} .g_serviceTile`).css({"transform": "translate3d( 0px , 10px, 0px)"});
			jQuery(`${idobject} .g_serviceTile footer`).css({"transform": "translate3d( 0px , 39.125px, 0px)"});
			jQuery(`${idobject} .g_serviceTile button`).css({"transform": "translate3d( 0px , 40px, 0px)"});
			// jQuery(`${idobject} header`).css( {"transform":  "translate3d( 0px , -176px, 0px)" });
			jQuery(`${idobject} header`).css({"transform": "translate3d( 0px , -140px, 0px)"});
			jQuery(`${idobject} header h2`).css({"color": "#FFF"});
			jQuery(`${idobject} header .g_serviceTileIcon`).css({"transform": "translate3d( 0px , 78.25px, 0px)"});

			jQuery(`${idobject} .g_center`).show();

		});


		jQuery(`${idobject} .g_serviceCta`).click(function () {
			jQuery(`${idobject}g_popinService`).addClass('g_popinOpened');
			jQuery(`${idobject}g_popinContent`).addClass('g_popinOpened');
		});

		jQuery(`.g_btnClose`).click(function () {
			jQuery(`${idobject}g_popinService`).removeClass('g_popinOpened');
		});

		jQuery(`.g_popinFixed`).click(function (obj) {
			var object = jQuery(`.g_popinContent`);

			if (!object.is(event.target) && !object.has(event.target).length) {
				jQuery(`${idobject}g_popinService`).removeClass('g_popinOpened');
			}

		});

		jQuery(idobject).mouseleave(function () {
			jQuery(`${idobject} .g_center`).removeClass("ctas-enter-active ctas-enter-to");
			jQuery(`${idobject} .g_center`).addClass("ctas-leave-active ctas-leave-to");
			jQuery(idobject).removeClass("flipped");
			jQuery(`${idobject} .g_serviceTile`).css({"transform": "translate3d( 0px , 0px, 0px)"});
			jQuery(`${idobject} .g_serviceTile footer`).css({"transform": "translate3d( 0px , 0px, 0px)"});
			jQuery(`${idobject} .g_serviceTile button`).css({"transform": "translate3d( 0px , 0px, 0px)"});
			jQuery(`${idobject} header`).css({"transform": "translate3d( 0px , 0px, 0px)"});
			jQuery(`${idobject} header .g_serviceTileIcon`).css({"transform": "translate3d( 0px , 0px, 0px)"});

		});

	}

	let arr = ['.serviceId-ordering', '.serviceId-delivery', '.serviceId-recycling', '.serviceId-customer-care'];

	for (let i = 0; i < arr.length; i++) {
		this.animationcard(arr[i], i);
	}


</script>
<style>
	.black {
		color: #000 !important;
		font-weight: bold !important;
	}
	.box {
		background: red;
		width: 100px;
		height: 0;
		opacity: 0;
		transition: 0.5s;
	}

	.hidden {
		opacity: 1;
		height: 100px;
	}
</style>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>