<?php
session_start();
if (isset($_GET['id'])) {
    $productId = $_GET['id'];
    $product = $_SESSION['products']->{$productId};
    ?>
    <li>
        <span class="name"><?php echo $product->{'name'}; ?> </span>
        <span class="nb"><?php echo $product->{'qty'}; ?></span>
        <span class="price">$<?php echo number_format((float) ($product->{'price'} * $product->{'qty'}), 2, '.', ''); ?></span>
    </li>
<?php } ?>