<?php
session_start();
if (isset($_GET['id'])) {
    $productId = $_GET['id'];
    $product = $_SESSION['products']->{$productId};
    ?>
    <div class="sbag-item">
        <div class="sbag-name">
            <img src="<?php echo $product->{'picture'}; ?> " alt="<?php echo $product->{'name'}; ?> ">
            <?php echo $product->{'name'}; ?> 
            <span class="hidden-sm hidden-md show-xs sb-price">$<?php echo number_format((float)($product->{'price'} * $product->{'qty'}), 2, '.', ''); ?></span>
        </div>
        <div class="sbag-unit-price">
            $<?php echo number_format((float)$product->{'price'}, 2, '.', ''); ?>
        </div>
        <div class="sbag-multi">x</div>
        <div class="sbag-qty">
            <div class="product-add" style="position: relative">
                <span class="btn btn-green product-qty" data-cart="true" data-name="<?php echo $product->{'name'}; ?>" data-price="<?php echo $product->{'price'}; ?>" data-picture="<?php echo $product->{'picture'}; ?>" data-type="<?php echo $product->{'type'}; ?>" data-qty-step="1">
                    <span class="qty"><?php echo $product->{'qty'}; ?></span>
                    <?php
                    $productQty = $product->{'qty'};
                    include 'qty-select.php';
                    ?>
                </span>
            </div>
            <span class="qty"><?php echo $product->{'qty'}; ?></span>
        </div>
        <div class="sbag-price">
            $<?php echo number_format((float)($product->{'price'} * $product->{'qty'}), 2, '.', ''); ?> <span tabindex="0" class="sbag-remove hidden-xs" data-cart-remove="true" data-name="<?php echo $product->{'name'}; ?>"></span>
        </div>
    </div>
<?php } ?>