<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$my_orders_columns = apply_filters( 'woocommerce_my_account_my_orders_columns', array(
	'order-date'    => __( 'Date', 'woocommerce' ),
	'order-status'  => __( 'Status', 'woocommerce' ),
	'order-source'  => __( 'Source', 'woocommerce' ),
	'order-number'  => __( 'Order ID', 'woocommerce' ),
	'order-total'   => __( 'Amount', 'woocommerce' ),
	'order-actions' => '&nbsp;',
) );

$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
	'numberposts' => -1,
	'meta_key'    => '_customer_user',
	'meta_value'  => get_current_user_id(),
	'post_type'   => wc_get_order_types( 'view-orders' ),
	'post_status' => array_keys( wc_get_order_statuses() ),
	'order' => 'DESC'
) ) );

$recent_products = [];

foreach ( $customer_orders as $customer_order ) :

	$order_id = $customer_order->ID;

	$order = new WC_Order( $order_id );

	$items = $order->get_items();

	foreach ( $items as $item ) :

		$product = $item->get_data();

		$product_id = $product['product_id'];

		$product_type = get_field( 'product_type', $product_id );

		switch ($product_type) {
			case 'Accessory':
				$recent_products[$order_id][$product_type] = array_merge( $product, (array)get_accessory_product_meta_data($product['product_id']) );
				break;
			case 'Coffee':
				$recent_products[$order_id][$product_type] = array_merge( $product, (array)get_coffee_product_meta_data($product['product_id']) );
				break;
			case 'Machine':
				$recent_products[$order_id][$product_type] = array_merge( $product, (array)get_machines_product_meta_data($product['product_id']) );
				break;
		}

	endforeach;

endforeach;

?>

<?php get_header(); ?>

<main id="content">

	<div class="container">
	    <div class="row">

	        <!-- Sidebar account -->
	        <div class="col-xs-12 col-md-3 account-sidebar">
	            <?php get_template_part('components/my-account/menu'); ?>
	        </div>
	        <!-- endof: Sidebar account -->

	        <div class="col-xs-12 col-md-9 account-detail my-order-history" id="my-order-history">

		        <?php if ( !$customer_orders || count($customer_orders) < 1 ) : ?>
		        	<!-- empty order history -->
		        	<div class="row">
		                <div class="col-xs-12 text-center">
		                    <p><strong>You did not order anything yet.</strong></p>
		                    <p>You can place your first order using the link below.</p>
		                    <div class="my-easyorders__link">
		                        <a class="btn btn-icon-right btn-primary" title="SHOP NOW" href="/coffee-list"><i class="fa fa-angle-right"></i>SHOP NOW</a>
		                    </div>
		                </div>
		            </div>
	        	<?php else : ?>

		            <div class="my-order-history-list-container clearfix">
			            <header>
			                <h2>My Order History</h2>
			            </header>

			            <table class="table table-responsive table-striped my-order-history-list">
							<thead>
								<tr>
						            <?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
										<th class="column-header text-center <?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
									<?php endforeach; ?>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $customer_orders as $customer_order ) :
									$order      = wc_get_order( $customer_order );
									$item_count = $order->get_item_count();
								?>
									<tr class="order">
										<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
											<td class="text-center <?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">

												<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
													<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

												<?php elseif ( 'order-number' === $column_id ) : ?>
													<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
														<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?>
													</a>

												<?php elseif ( 'order-date' === $column_id ) : ?>
													<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

												<?php elseif ( 'order-status' === $column_id ) : ?>
													<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

												<?php elseif ( 'order-total' === $column_id ) : ?>
													<span class="price"><?= $order->get_formatted_order_total() ?></span>

												<?php elseif ( 'order-actions' === $column_id ) : ?>
													<?php
														$actions = array(
															'pay'    => array(
																'url'  => $order->get_checkout_payment_url(),
																'name' => __( 'Pay', 'woocommerce' ),
															),
															'view'   => array(
																'url'  => $order->get_view_order_url(),
																'name' => __( 'View', 'woocommerce' ),
															),
															'cancel' => array(
																'url'  => $order->get_cancel_order_url( wc_get_page_permalink( 'myaccount' ) ),
																'name' => __( 'Cancel', 'woocommerce' ),
															),
														);

														if ( ! $order->needs_payment() ) {
															unset( $actions['pay'] );
														}

														if ( ! in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ) ) ) {
															unset( $actions['cancel'] );
														}

														if ( $actions = apply_filters( 'woocommerce_my_account_my_orders_actions', $actions, $order ) ) {
															foreach ( $actions as $key => $action ) {

																if ( sanitize_html_class( $key ) == 'view' ) :
																	echo '<a href="javascript:void(0)"
																			class="button btn-view ' . sanitize_html_class( $key ) . '"
																			data-order-id="'. $order->get_order_number() .'"
																		>' . esc_html( $action['name'] ) . '</a>';
																else :

																echo '<a href="' . esc_url( $action['url'] ) . '" class="button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
																endif;
															}
														}
													?>

												<?php elseif ( 'order-source' === $column_id ) : ?>
													Internet

												<?php endif; ?>
											</td>
										<?php endforeach; ?>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

		                <div class="my-order-mobile clearfix">
		                    <div class="my-order-mobile__block">
		                        <div class="my-order-mobile__order">
		                            <dl class="col-xs-4">
		                                <dt>Order date</dt>
		                                <dd>13/01/2016</dd>
		                            </dl>
		                            <dl class="col-xs-3">
		                                <dt>Status</dt>
		                                <dd>Pending</dd>
		                            </dl>
		                            <div class="col-xs-5">
		                                <dl class="pull-right">
		                                    <dt>Amount</dt>
		                                    <dd><span class="price">$ 263.60</span></dd>
		                                </dl>
		                            </div>
		                        </div>
		                        <div class="my-order-mobile__info">
		                            <div class="col-xs-7 my-order-mobile__info-order">
		                                <span class="my-order-mobile__info-line">Source: Internet</span>
		                                <span class="my-order-mobile__info-line">Delivery mode: Standard</span>
		                                <span class="my-order-mobile__info-line">Order ID: Pending</span>
		                            </div>
		                            <div class="col-xs-5 my-order-mobile__info-btn">
		                                <button class="btn btn-primary pull-right btn-open-detail">More detail</button>
		                                <button class="btn btn-green pull-right">Re-order</button>
		                            </div>
		                        </div>
		                    </div>
		                </div>

		                <a href="#" class="link link--right pull-right more-order" title="View more orders">
		                    <span>See more orders</span>
		                    <span>See less orders</span>
		                </a>
		            </div>
		            <div class="my-order-mobile-detail">
		                <div class="my-order-mobile-detail__header clearfix">
		                    <a class="btn btn-back btn-primary btn-close-detail btn-block btn-icon" href="#"><i class="icon-arrow_right"></i> All orders</a>
		                    <button class="btn btn-green pull-right">Re-order</button>
		                </div>
		                <div class="my-order-mobile-detail__dynamic"></div>
		            </div>

		            <!-- <section class="my-order-history-detail" id="my-order-history-fake-ajax"> -->
		            <section class="my-order-history-detail hide" id="order-details">

		                <header>
		                    <h3 id="order-details-date"></h3>
		                    <div class="div__btn">
		                        <button class="btn btn-green hidden-xs hidden-sm">Re-order</button>
		                    </div>
		                </header>
		                <div class="row">
		                    <div class="sbag-list">
		                        <div class="sbag-list-content">
		                            <div class="sbag-list-title">
		                                Capsules
		                                <sup>(<span class="sbag-nb-capsules">4</span>)</sup>
		                                <div class="sbag-list-title-col">
		                                	<span class="sbag-head-unit-price">Unit Price</span>
		                                	<span class="sbag-head-qty">Quantity</span>
		                                	<span class="sbag-head-total">Total</span>
	                                	</div>
		                            </div>
		                            <div class="sbag-capsules">
		                                <div class="sbag-item">
		                                    <div class="sbag-name">
		                                        <img src="images/products/livanto.png " alt="LIVANTO ">
		                                        LIVANTO
		                                        <span class="hidden-sm hidden-md show-xs sb-price">$140.00</span>
		                                    </div>
		                                    <div class="sbag-unit-price">
		                                        $0.70        </div>
		                                    <div class="sbag-multi">x</div>
		                                    <div class="sbag-qty">
		                                        <span class="qty">200</span>
		                                    </div>
		                                    <div class="sbag-price">
		                                        $140.00
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="sbag-list-title">
		                                Accessories <sup>(<span class="sbag-nb-accessories">2</span>)</sup>
		                                <div class="sbag-list-title-col"><span class="sbag-head-unit-price">Unit Price</span><span class="sbag-head-qty">Quantity</span><span class="sbag-head-total">Total</span>
		                                </div></div><div class="sbag-accessories">
	                                	<div class="sbag-item">
		                                    <div class="sbag-name">
		                                        <img src="images/touch-espresso_main.png " alt="Touch Espresso - Set of 2 espresso cups ">
		                                        Touch Espresso - Set of 2 espresso cups
		                                        <span class="hidden-sm hidden-md show-xs sb-price">$4000.00</span>
		                                    </div>
		                                    <div class="sbag-unit-price">
		                                        $16.00        </div>
		                                    <div class="sbag-multi">x</div>
		                                    <div class="sbag-qty">
		                                        <span class="qty">250</span>
		                                    </div>
		                                    <div class="sbag-price">
		                                        $4000.00
		                                    </div>
		                                </div></div></div>
		                        <div class="sbag-resume-content">
		                            <div class="sbag-resume-head">
		                                <span class="sbag-resume-head-top">Total: </span>
		                                <ul>
		                                    <li>Capsules (<span>200</span>)</li>
		                                    <li>Accessories (<span>1</span>)</li>
		                                </ul>
		                            </div>
		                            <div class="sbag-resume-item sbag-total">
		                                <div class="sbag-total-key">Subtotal</div>
		                                <div class="sbag-total-value sbag-padding-cross">$<span class="sbag-subtotal-price-int">4698.00</span></div>
		                            </div>
		                            <div class="sbag-resume-item">
		                                <div class="sbag-total-key">Shipping cost</div>
		                                <div class="sbag-total-value sbag-padding-cross">FREE</div>
		                            </div>
		                            <div class="sbag-resume-item">
		                                <div class="sbag-total-key">Vat (incl.)</div>
		                                <div class="sbag-total-value sbag-padding-cross">$<span class="sbag-val-price-int">469.80</span></div>
		                            </div>
		                        </div>

		                        <div class="sbag-footer">
		                            <div class="sbag-total-item">
		                                <div class="sbag-total-title">Total</div>
		                                <div class="sbag-total-price sbag-padding-cross">$<span class="sbag-total-price-int">5167.80</span></div>
		                            </div>
		                        </div>

		                    </div>
		                </div>
		                <div class="row">
		                    <div class="col-xs-12">
		                        <p class="my-order-history-detail__order-type">You have made your order via Internet</p>
		                    </div>
		                    <div class="col-address col-xs-12 col-sm-6">
		                        <p>
		                            <strong>Delivered to:</strong>
		                            Mr First name second name<br>
		                            Adress line<br>
		                            1234 City<br>
		                            Switzerland
		                        </p>
		                        <span class="col-address__summary"><i class="icon-Delivery_off"></i> Delivery mode: <strong>Standard</strong></span>
		                        <a href="#" class="link link--right" title="Order tracking #1">Order tracking #1</a>
		                    </div>
		                    <div class="col-address col-xs-12 col-sm-6">
		                        <p>
		                            <strong>Invoiced to:</strong>
		                            Mr First name second name<br>
		                            Adress line<br>
		                            1234 City<br>
		                            Switzerland
		                        </p>
		                        <span class="col-address__summary"><i class="icon-invoice-off"></i> <strong>Invoice</strong></span>
		                    </div>
		                </div>

		                <div class="sbag-list">
		                    <div class="row">
		                        <div class="sbag-reorder">
			                        <a href="images/tmp.pdf" target="_blank" class="btn btn-primary btn-print btn-noWidth">Print</a>
			                        <button class="btn pull-right btn-green">Re-order</button>
			                        <button class="btn pull-right  btn-green">Make this order recurrent</button>
		                        </div>
		                    </div>
		                </div>

		            </section>
	        	<?php endif; ?>

        	</div><!-- .my-order-history -->

        </div><!-- .row -->
    </div><!-- .container -->
</main><!-- .content -->

<!-- /content -->

<script type="text/javascript">
 	var recent_products = <?= $recent_products ? json_encode($recent_products) : 'null' ?>;
</script>

<?php
	global $scripts;
	$scripts[] = 'js/components/my-order-history.js';
?>

<!-- get_footer -->
<?php get_footer(); ?>

