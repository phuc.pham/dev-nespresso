<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * get the coffee product
 */

global $post;

$product = include_cofee_product_meta_from_results( $post, $get_recommended_products=true );

if ( isset($product->post_name) ) :

	$product->name =  $product->post_title;

	$product_image_url = isset($product->product_view_image) && $product->product_view_image ? $product->product_view_image['url'] : $product->image[0];

	$hq_data  = get_hq_data($product->sku);

?>

<?php get_template_part('components/generics/qty-selector'); ?>

	<main id="content">

		<div class="product-page product-page--coffee" id="JumpContent" tabindex="-1">

		    <div id="product__header" class="product__header product__header--light">
		        <div class="back-container">
		            <a href="/coffee-list" title="All Coffee" class="btn-back link link--left">Back to all coffee</a>
		        </div>
				<?=get_nespresso_show_off($product->label_off,'pro-detail-off');?>
		        <div class="product__slider">
		            <a href="#" class="product__slider__close"></a>
		            <ul class="product__slider__list">
			            <li class="active">
				            <img src="<?php echo $product_image_url; ?>" alt="<?php $product->name ?>" draggable="false" alt="<?php echo $product->name; ?>"/>
			            </li>
		            </ul>
		        </div>
		        <div class="product__info">
		            <div>
			            <div class="product__info__type"><?= $product->category; ?></div>
						
			            <h2 class="product__info__title"><?= strtoupper($product->name) ?></h2>
			            <i><?= $product->property ?></i>
			            <span class="product__info__price uppercase"><?= wc_price($product->price) ?>
						<?php if($product->regular_price>$product->price){ ?>
							<span class="product-price--old" style="color:gray;text-decoration: line-through;display:block"><?=wc_price($product->regular_price) ?></span>
						<?php  } ?>
						</span>

			            <div class="container-btn">
						<?php 
							if($product->qty_x1){
							?>
							<?php if ($product->price):
                            $class = "btn-green";
                            $label = "Add to basket";
                        else:
                            $class = "btn-disabled";
                            $label = "Out of stock";
                        endif; ?>
				        <a href="#" class="btn-custom-add-to-cart btn btn-icon <?= $class ?> btn-block btn-icon-right btn-large"
                            data-id="<?= $product->product_id ?>"
                            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->product_id ?>"
                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                            data-sku="<?= $product->sku ?>"
                            data-type="Machine"
                            data-name="<?=$product->name;?>"
                            data-price="<?=$product->price;?>"
                            data-image-url="<?=$product->image ? $product->image[0] : '';?>"
                            data-picture="<?=$product->image ? $product->image[0] : '';?>"
                            data-qty-step="1"
                            data-vat="1"
                            data-page="machine"
                        >
					        <i class="icon-basket"></i><span class="btn-add-qty"></span><span><?= $label ?></span><i class="icon-plus"></i>
				        </a>
							<?php }else{ ?>
			            	<?php if ($product->stock > 0):
					            $class = "btn-green";
					            $label = "Add to basket";
					        else:
					            $class = "btn-disabled";
					            $label = "Out of stock";
					        endif; ?>
			            	<div id="qty-selector-content" class=""></div>
				            <a href="#" class="btn btn-icon btn-block btn-icon-right btn-large <?= $class ?>"
				               data-id="<?= $product->product_id ?>"
				               data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->product_id ?>"
							   data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
							   data-sku="<?= $product->sku ?>"
				               data-cart="true"
				               data-type="<?= $product->product_type ?>"
				               data-name="<?= $product->name ?>"
				               data-price="<?= $product->price ?>"
				               data-image-url="<?= $product->image ? $product->image[0] : '' ?>"
				               data-picture="<?= $product->image ? $product->image[0] : '' ?>"
				               data-qty-step="10"
				               data-vat="1"
				               data-aromatic-profile="<?= $product->aromatic_profile?>"

			               	>
					            <i class="icon-basket"></i><span class="btn-add-qty"></span><span><?= $label ?></span><i class="icon-plus"></i>
				            </a>
				            <div  class="mobi-select-cont" >
				                <select class="mobi-sec" style="width: 100%;height: 100%;"
				                   data-id="<?= $product->product_id ?>"
					               data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->product_id ?>"
								   data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
								   data-sku="<?= $product->sku ?>"
					               data-cart="true"
					               data-type="<?= $product->product_type ?>"
					               data-name="<?= $product->name ?>"
					               data-price="<?= $product->price ?>"
					               data-image-url="<?= $product->image ? $product->image[0] : '' ?>"
					               data-picture="<?= $product->image ? $product->image[0] : '' ?>"
					               data-qty-step="10"
					               data-vat="1"
					               data-aromatic-profile="<?= $product->aromatic_profile?>"
				                >

				                    <?php
				                    $c=0;
				                    $quantity = 300;
                    				$quantity_increment = 10;
                    				 if($product->aromatic_profile == 'Varied') {
				                            $quantity = 100;
				                            $quantity_increment = 1;
				                        }
				                        while ( $c<= 300) {
				                           echo "<option value='$c'>$c</option>";
				                           $c+=$quantity_increment;
				                        }
				                     ?>
				                </select>
				            </div>
							<?php } ?>
			            </div>
			            <p>
				            <i class="icon-Delivery_off"></i>
				            <span>Get Free delivery on all orders above ₫1,500,000</span>
			            </p>
		            </div>
		        </div>
		    </div>

		    <div class="product-features">
			    <div class="container">
			        <div class="product-feature">
			            <i class="icon icon-Coffee_capsule_on"></i>
			            <?= $product->category; ?>
			        </div>
			        <div class="product-feature">
			            <i class="icon icon-INTENSITY-on"></i> <span><?= $product->intensity; ?></span> Intensity
			        </div>
			        <?php if($product->size_of_cup == 'ristretto'): ?>
			            <div class="product-feature">
			                <i class="icon icon-ristretto_on-off"></i> Ristretto 25 ML
			            </div>
			        <?php endif; ?>
			        <?php if($product->size_of_cup == 'espresso'): ?>
			            <div class="product-feature">
			                <i class="icon icon-espresso_on-off"></i> Espresso 40 ML
			            </div>
			        <?php endif; ?>
			        <?php if($product->size_of_cup == 'lungo'): ?>
			            <div class="product-feature">
			                <i class="icon icon-lungo_on-off"></i> Lungo 110 ML
			            </div>
			        <?php endif; ?>
		        </div>
		    </div>

		    <div class="product-content">

			    <div class="container">
			        <?php if(isset($product->description)): ?>
			        	<!-- product description -->
			            <div class="product-content__text"><?php echo $product->description; ?></div>
			        <?php endif; ?>

			        <?php /*if(isset($product->ingredients_and_allergens)): ?>
			        	<!--ingredients and allergens -->
				        <div class="btn-mobile-toggle">
			                <h3 class="btn-mobile-toggle__header js-toggle js-toggle--desktop" data-toggle="+">INGREDIENTS AND ALLERGENS</h3>
			                <div class="btn-mobile-toggle__content" style="display: none">
			                	<?php echo $product->ingredients_and_allergens; ?>
			                </div>
			            </div>
		            <?php endif; ?>

		            <?php if(isset($product->recycling)): ?>
		            	<!--recycling -->
			            <div class="btn-mobile-toggle">
			                <h3 class="btn-mobile-toggle__header js-toggle js-toggle--desktop" data-toggle="+">Recycling</h3>
			                <div class="btn-mobile-toggle__content" style="display: none">
			                   	<?php echo $product->recycling; ?>
			                </div>
			            </div>
		            <?php endif;*/ ?>

		            <!-- social links buttons -->
			   		<?php get_template_part('components/generics/social-links'); ?>

			        <?php if( isset($product->aromatic_profile) && !empty($product->similar_aromatic_profile_products) ): ?>

					    </div><!-- .container -->

				        <div class="similar-product__test">
					        <div class="container">
					            <h3>SIMILAR AROMATIC PROFILE</h3>
					            <div class="similar-products row">
					                <?php $ap_counter = 1; foreach($product->similar_aromatic_profile_products as $ap_product ) : ?>
					                <?php
					                	$post_data = get_post($ap_product->post_id);
					                	if ($post_data->post_status != "publish") {
					                		continue;
					                	}
					                 ?>
					                	<?php if ( $ap_product ) :
					                		$hq_data  = get_hq_data($ap_product->sku);
					                	?>
							                <div class="track-impression-product similar-product"
							                	data-product-item-id="<?= $ap_product->sku ?>"
									            data-product-section="Similar Aromatic Profile List"
									            data-product-position="<?= $ap_counter ?>"
							                >
							                    <a href="<?= $ap_product->url; ?>"
							                    	class="nespresso-product-item"
											        title="<?= $ap_product->name ?>"
											        data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $ap_product->post_id ?>"
											        data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
											        data-price="<?= $ap_product->price ?>"
											        data-name="<?=$ap_product->name?>"
											        data-type="<?=$ap_product->product_type?>"
											        data-position="<?= $ap_counter ?>"
											        data-list="Similar Aromatic Profile"
											        data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
											        data-sku="<?= $ap_product->sku ?>"
							                    >
							                        <img src="<?= isset($ap_product->image) && $ap_product->image ? $ap_product->image[0] : '' ?>" alt="<?= $ap_product->name ?>">
							                        <?= ucfirst(strtolower($ap_product->name)) ?>
							                    </a>
							                </div>
						                <?php endif; ?>
					                <?php $ap_counter++; endforeach; ?>
					            </div>
				            </div>
				        </div><!-- .similar-product__test -->

			        <?php else : ?>
			        	</div><!-- .container -->
	        		<?php endif; ?>

		    </div><!-- .product-content -->

		    <?php  if ( isset($product->recommended_products) && $product->recommended_products) : ?>
	            <div class="recommended-grey">
		            <div class="container">
			            <h4>RECOMMENDED PRODUCTS</h4>
			            <?php include 'wp-content/themes/storefront-child/components/generics/recommended-products.php'; ?>
		            </div>
	            </div>
	        <?php endif; ?>

		</div><!-- .product-page -->
	</main>

<?php else : ?>
	<h4 class="center">Product not found</h4>
<?php endif; ?>
