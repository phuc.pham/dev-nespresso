<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$products = get_machine_products();
$arrays = get_nespresso_product_list();
$machines = [];
$machine_product = 11163;

foreach ($products as $value) {
	if($machine_product == $value->post_id) {
		$machine_product = $value;
	}
    $machines[] = $value->name;
}

if ($arrays) {
	$machines = $arrays['machine'];
}

global $scripts;
$scripts[] = 'js/components/machine-discover-more.js';

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/machine-discover-more.css">

<main id="main" class="fullPage fullPage__plp coffee-list desktop">
	<div id="block-8809565345269" class="free-html" data-label="">
		<div class="vue vue_machines v_inissia">
			<section class="vue_introduction v_introduction v_sectionnull v_backgroundVideoPlaying" id="introduction" data-label="Introduction">
			    <div class="v_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/placeholder_XL.jpg');" lazy="loaded"> <iframe tabindex="-1" aria-hidden="true" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" src="https://www.youtube.com/embed/amp3EhtxTs4?rel=0&fs=0&autoplay=1&playlist=amp3EhtxTs4&controls=0&loop=1&enablejsapi=1&origin=https%3A%2F%2Fwww.nespresso.com&widgetid=1" id="widget2" class="play gtm-video-start gtm-video-progress25 gtm-video-progress50 gtm-video-progress75 gtm-video-complete" data-gtm-yt-inspected-2212929_367="true" height="360" frameborder="0" width="640"></iframe> </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <header>
			                <div class="v_cell">
			                    <div class="v_menu">
			                        <p class="v_visually_hidden"></p>
			                        <ul>
			                            <li> <a href="/machine-list">All machines</a> </li>
			                            <li> <a href="#">Assistance</a> </li>
			                            <li> <a href="#">Specifications</a> </li>
			                        </ul>
			                    </div>
			                </div>
			            </header>
			            <article>
						    <div class="v_articleContent">
						        <h2> <span><strong class="v_brand" term="pixie">Pixie</strong></span> </h2>
						        <div class="v_wysiwyg">
						            <p>Concentrated functionality in a compact design <strong class="v_brand" term="pixie">Pixie</strong> is the SMART model in our range, condensing a wide range of innovative, advanced features into a surprisingly small machine.</p>
						        </div>
						        <div class="v_buttonContainer"> <a class="v_btnRoundM" tabindex="-1" aria-hidden="true" href="#visualVariations"> <i class="fn_angleDownCircle"></i> </a>  </div>
						    </div>
						</article>
			        </div>
			    </div>
			    <div class="v_video">
			        <button type="button" name="button" class="v_btnRoundSM v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			        <div></div>
			    </div>
			</section>
			<section class="vue_visualVariations vue_productConfigurator v_sectionLight v_toggleVariation0 v_toggle_lattissimaTouch v_key_visualVariations" id="visualVariations" data-label="Visual Variations">
			    <h2 class="v_visually_hidden">Customize and Buy your <strong class="v_brand" term="pixie">pixie</strong></h2>
			    <p class="v_visually_hidden">Use the buttons below to expand front or side view</p>
			    <div class="v_visualVariationsSlider" style="touch-action: pan-y; -moz-user-select: none;">
			        <article class="v_product">
	                    <div class="v_restrict">
			                <button style="box-shadow: none !important;" class="v_activeView"><span class="v_visually_hidden">Press ENTER or SPACE to zoom on front view</span><span class="v_visually_hidden">Front</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/alu_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/red_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/titan_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button><button style="box-shadow: none !important;"> <span class="v_visually_hidden">Press ENTER or SPACE to zoom on side view</span> <span class="v_visually_hidden">Side</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/alu_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/red_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/titan_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button>
			            </div>
	                </article>
			    </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_productConfiguratorPosition">
			                <div class="v_productConfigurator">
			                    <p class="v_visually_hidden">Use the form below to choose your machine model, change color, and add to bag</p>
			                    <form>
			                        <div class="v_cell v_toggleColor">
			                            <fieldset>
			                                <legend>Choose your color</legend>
			                                <div class="machine-product-color machine-product-color-1 v_bullets0">
			                                    <p aria-hidden="true">Colors</p>
			                                    <div class="v_inputs">
			                                        <input name="colorToggle0" id="radio_colorToggle0_0" value="0" type="radio"><label style="background-color: rgb(241, 241, 241);" for="radio_colorToggle0_0"> <b>Electric Aluminium <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle0" id="radio_colorToggle0_1" value="1" type="radio"><label style="background-color: rgb(218, 31, 30);" for="radio_colorToggle0_1"> <b>Electric Red <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle0" id="radio_colorToggle0_2" value="2" type="radio"><label style="background-color: rgb(146, 146, 146);" for="radio_colorToggle0_2"> <b>Electric Titan <span class="v_visually_hidden">color</span></b> </label>
			                                        <div class="v_bullet_selected"><span></span></div>
			                                    </div>
			                                </div>
			                            </fieldset>
			                        </div>
			                        <div class="v_cell v_buyProduct">
				                        <fieldset>
				                            <legend>Review your product: pixie Glam Red color</legend>
				                            <div class="v_addToCart">
				                                <div class="v_priceAndButton">
				                                    <p class="v_productPrice"><?= wc_price(@$machine_product->price) ?></p>
				                                    <div class="v_addToCartCustom">
				                                        <button type="button" class="view_buy" data-link="<?= @$machine_product->url ?>" style="padding: 0.7em 1em .7em 1em;"><span class="v_label" aria-hidden="true">View Details & Buy</span> </button>
				                                    </div>
				                                </div>
				                            </div>
				                        </fieldset>
				                    </div>
			                    </form>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_benefits v_key_benefits showBenefits_pixie" id="benefits" data-label="benefits">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover the key benefits of the <strong class="v_brand" term="pixie">Pixie</strong> range</h2>
			            <div class="v_wysiwyg wow" data-wow="">
			                <p><strong class="v_brand" term="pixie">Pixie</strong>, the quickest of the <strong class="v_brand" term="nespresso">Nespresso</strong> machines, reaches its heating temperature in 25 seconds, thanks to its rapid thermo-block, and provides you with the perfect coffee in record time. A powerful, unrivaled technique.</p>
			            </div>
			            <ul class="v_row6">
			                <li data-wow="" class="v_icon_speed wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/speed.svg"></div>
			                    <div>
			                        <p><strong class="v_brand" term="pixie">Pixie</strong> is fast</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_cube wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/cube.svg"></div>
			                    <div>
			                        <p><strong class="v_brand" term="pixie">Pixie</strong> is small</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_ecofriendly wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/ecofriendly.svg"></div>
			                    <div>
			                        <p><strong class="v_brand" term="pixie">Pixie</strong> is eco-friendly</p>
			                    </div>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>

			<section class="vue_video v_key_video" id="video" data-label="Video">
			    <h2 class="v_visually_hidden">Watch the <strong class="v_brand" term="pixie">Pixie</strong> video</h2>
			    <div class="v_backgroundImage" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/video_XL.jpg');" lazy="loaded"></div>
			    <button type="button" name="button" class="v_btnRoundLG v_btnOpen video-modal" data-source="video" title="Press ENTER or SPACE to start the Maestria presentation"> <i class="fn_videoCircle"></i> <span tabindex="-1" aria-hidden="true">Watch the <strong class="v_brand" term="pixie">Pixie</strong> video</span> <span class="v_visually_hidden">Play video</span> </button>

				<div id="video-modal-container" class="video-modal">
	  				<iframe class="video-modal-content" id="modal_video video_iframe"  allowfullscreen="1" allow="encrypted-media" title="YouTube video player" src="http://www.youtube.com/embed/ne1xJJRqhq8?enablejsapi=1&autoplay=0&rel=0"  data-gtm-yt-inspected-2212929_367="true" class="gtm-video-start gtm-video-complete gtm-video-progress25 gtm-video-progress50 gtm-video-progress75" height="360" frameborder="0" width="640"></iframe>
				</div>

			    <div class="v_videoContainer">
			        <button type="button" name="button" class="v_btnRoundMD v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			    </div>
			</section>
			<section class="vue_details v_sectionLeft v_key_details1" id="compact" data-label="compact">
			    <div class="bg_normal bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/details1_XL.jpg');" lazy="loaded"> </div>
			    <div class="bg_normal bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/details1_S.jpg');" lazy="loaded"> </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Compact &amp; modern design</h2>
			                <p class="headline wow" data-wow="">Playful shapes, electric colors, a sleek finish... a sustainable beauty you can't stop admiring.</p>
			                <div class="v_wysiwyg wow" data-wow="">
			                    <p>Long before he became the designer of today, the creator of the <strong class="v_brand" term="pixie">Pixie</strong> was a little boy fascinated by colorful shapes and objects. Critics talk about the machine's combination of Happy Design and Smart Technology. We prefer just to say <strong class="v_brand" term="pixie">Pixie</strong>.</p>
			                    <p>Standing a little over the height of 8 <strong class="v_brand" term="nespresso">Nespresso</strong> capsules, with a deep water storage space, the <strong class="v_brand" term="pixie">Pixie</strong> is no larger or longer than a coffee spoon. It is the size of a small rabbit, with delicate ears and soft fur. Easy to hold in the hand, squeeze into any space in your kitchen, living room, office, or... garden.</p>
			                </div>
			            </div>
			        </div>
			    </div>
			    <div class="v_videoContainer">
			        <button type="button" name="button" class="v_btnRoundMD v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			        <div></div>
			    </div>
			</section>

			<section class="vue_carousel v_key_carousel v_slide4" id="carousel" data-label="Carousel">
			    <h2 class="v_visually_hidden">Gallery</h2>
			    <p class="v_visually_hidden">Use ENTER or SPACE key on thumbnails buttons to enter slider mode</p>
			    <div class="vue_imageGrid">
			        <div>
			        	<span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/1_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/1_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/1_XL.jpg');" lazy="loaded">
			        	<button tabindex="-1" aria-hidden="true"></button></span>
			        </div><div>
			            <div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/2_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/2_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/2_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> </span> </div><div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/3_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/3_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/3_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> <img class="v_visually_hidden" alt="Image 3" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/3_XL.jpg" lazy="loaded"> </span> </div><div>
			            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/4_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/4_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/4_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div><div>
			            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/5_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/5_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/5_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div>
			        </div>
			    </div>
			    <div id="modal-container" class="image-modal">
	  				<img class="image-modal-content" id="modal_img" src="">
				</div>
			</section>
			<section class="vue_details v_sectionRight v_key_details2" id="intelligent" data-label="intelligent">
			    <div class="bg_normal bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/details2_XL.jpg');" lazy="loaded"> </div>
			    <div class="bg_normal bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/details2_S.jpg');" lazy="loaded"> </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow"><strong class="v_brand" term="pixie">Pixie</strong> is intelligent</h2>
			                <p class="headline wow" data-wow="">Adjusting the water level is easy</p>
			                <div class="v_wysiwyg wow" data-wow="">
			                    <p>With a smart alert system, <strong class="v_brand" term="pixie">Pixie</strong> will notify you when the tank is almost empty. With its 0.7 liter tank, <strong class="v_brand" term="pixie">Pixie</strong> has been engineered for use in everyday life.</p>
			                    <p>When the water level gets too low, <strong class="v_brand" term="pixie">Pixie</strong> warns you with an indicator light.</p>
			                </div>
			            </div>
			        </div>
			    </div>
			    <div class="v_videoContainer">
			        <button type="button" name="button" class="v_btnRoundMD v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			        <div></div>
			    </div>
			</section>

			<section class="vue_faq v_sectionLeft v_key_faq" id="faq" data-label="Faq">
			    <div class="bg_normal bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/faq_XL.jpg');" lazy="loaded"></div>
			    <div class="bg_normal bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/pixie/faq_S.jpg');" lazy="loaded"></div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Frequently asked questions</h2>
			                <div class="vue_accordion">
			                    <ul role="accordion" class="v_isCollapsable">
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 1 of 6</span> How do I take care of my machine? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>Check the <a href="https://www.youtube.com/watch?v=0RCCTmZPANk" target="_blank">‘how to descale’ video</a>.</p>
			                                <ol>
			                                    <li>Descale your machine at least once a year if you use soft water. Note: for hard water, descale twice a year. (If you don’t know what type of water you have, check the water tank – hard water tends to leave white marks). <a href="order/accessories/kit-descalcificador-cafeteras-nespresso">Order a descaling kit now</a></li>
			                                    <li>Eject your capsules after each use</li>
			                                    <li>Change the water tank regularly, and refill with fresh drinking water</li>
			                                    <li>Empty and clean the capsule container and drip tray on a regular basis</li>
			                                    <li>For integrated milk devices, activate the cleaning procedure after each use</li>
			                                    <li>Before your first coffee, brew water without a capsule by pressing any coffee button. When you do this, you pre-heat your coffee cup while rinsing the extraction system for a better coffee experience.</li>
			                                </ol>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 2 of 6</span> How high is the percentage of recyclable material used in <strong class="v_brand" term="nespresso">Nespresso</strong> machines? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>On average, our machines have a 60% recyclability potential. And today, <strong class="v_brand" term="nespresso">Nespresso</strong> Members can bring their machines back to wherever they bought them or in any electrical appliance store. In Europe, the European Union’s Waste Electrical and Electronic Equipment (WEEE) has been in force since 2003. It provides a framework in which consumers can return their used electrical and electronic appliances for recycling.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 3 of 6</span> What does 19 bars mean? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>All the&nbsp;<strong class="v_brand" term="nespresso">Nespresso</strong>&nbsp;machines are equipped with a 19-bar pressure pump, which provides the power needed to pierce the film of the capsule and release the coffee's 900 or so different aromas.</p>
			                                <p>The shape of the&nbsp;<strong class="v_brand" term="nespresso">Nespresso</strong>&nbsp;capsule has been specially designed to ensure that the pressurized water flows evenly through the ground coffee during extraction. The temperature and flow time are also set to ensure that each precious aroma is expressed.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 4 of 6</span> What does it mean when my <strong class="v_brand" term="pixie">Pixie</strong> machine has an irregular flashing light? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>If the capsule container is lit up in red, it means that the water tank is almost empty.<br>If it's lit up in red and the water tank is full, try the following steps:</p>
			                                <ul>
			                                    <li>Empty the water tank and fill it with fresh water</li>
			                                    <li>Clean the bottom of the water tank with a soft cloth.</li>
			                                </ul>
			                                <p>If there is no light, it means that the machine switches itself OFF automatically after 9 mins. This time can be changed to 30 mins. <br>Procedure: </p>
			                                <ul>
			                                    <li>Switch the machine OFF. </li>
			                                    <li>Press the Espresso and Lungo buttons simultaneously, then press the ON/OFF button. </li>
			                                </ul>
			                                <p>If the light is flashing irregularly, please call the call center.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 5 of 6</span> How do I troubleshoot my machine when it has little or no coffee flow? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>If there is no coffee flow, please check that:</p>
			                                <ol>
			                                    <li>The main supply cord is not trapped between the water tank and the machine</li>
			                                    <li>The water tank is sufficiently full and correctly positioned</li>
			                                    <li>Your machine is turned ON (lights ON)</li>
			                                </ol>
			                                <p>If there is low coffee flow, it means you need to descale your machine.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 6 of 6</span> What is the size of the water tank for the <strong class="v_brand" term="pixie">Pixie</strong> model? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>The removable water tank on the <strong class="v_brand" term="pixie">Pixie</strong> models holds 0.7 liters. Backlight indicators detect when the water tank needs to be refilled (Blue/gray light when full, red light when empty) You can also refill the water tank directly on the machine without removing the tank.</p>
			                            </div>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>

			<section class="vue_relativeProducts v_sectionLight v_key_relativeProducts" id="relativeProducts" data-label="Relative Products">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover all the <strong class="v_brand" term="nespresso">Nespresso</strong> machines</h2>
			            <div class="v_slider v_sliderFlex">
			                <button class="v_sliderArrow v_sliderPrev" tabindex="-1" aria-hidden="true"> <i class="fn_angleLeft"></i> </button>
			                <div class="v_slideContainer" style="touch-action: pan-y; -moz-user-select: none;">
			                    <div class="v_slide" style="transform: translateX(0); transition: all 1.3s ease 0.15s;">
			                        <ul aria-hidden="true">
			                        	<?php foreach ($machines as $key => $machine):?>
	                                        <?php foreach($products as $k => $product): ?>
	                                            <?php if (html_entity_decode($machine) == html_entity_decode($product->name)): ?>
	                                                <li class="nepresso-machine-product" style="width: 199.2px;">
						                                <div data-product-item-id="<?= $product->post_id ?>">
						                                    <div class="v_imageContainer">
						                                        <a class="v_cell" href="<?= $product->url ?>" tabindex="-1" style="background-image: url('<?= $product->image ? $product->image[0] : '' ?>');" lazy="loading"> <span class="v_visually_hidden"><strong class="v_brand" term="<?= $product->name ?>"><?= $product->name ?></strong></span> </a>
						                                    </div>
						                                    <div class="v_sliderItemText">
						                                        <h3 aria-hidden="true"><span><strong class="v_brand" term="prodigio"><?= $product->name ?></strong></span></h3>

						                                    </div>
						                                </div>
						                            </li>
	                                            <?php endif ?>
	                                        <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </ul>
			                    </div>
			                </div>
			                <button class="v_sliderArrow v_sliderNext" tabindex="-1" aria-hidden="true"> <i class="fn_angleRight"></i> </button>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_corporate v_sectionLeft v_sectionTop v_key_corporate" id="corporate" data-label="Corporate">
			    <div class="bg_parallax skrollable skrollable-between bg-xl" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_XL.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="bg_parallax skrollable skrollable-between bg-small" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_S.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Why <strong class="v_brand" term="nespresso">Nespresso</strong>?</h2>
			                <p data-wow="" class="wow">A cup of coffee is much more than a break. It is your small ritual. Make it an unparalleled experience.</p>
			                <div class="v_wysiwyg wow" data-wow="">
			                    <p>Choose <strong class="v_brand" term="nespresso">Nespresso</strong>, do not settle for less: strictly-selected coffee coming in a matchless range of exclusive varieties, coffee machines combining smart design with simple state-of-the-art technology, refined accessories, indulgent sweet treats and services anticipating your every desire. What else?</p>
			                </div>
			                <!-- <a class="v_link v_iconLeft" href="https://www.nespresso.com/es/en/discover-le-club"> <i class="fn_arrowLink"></i> Read more about <strong class="v_brand" term="nespresso">Nespresso</strong> </a> -->
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_services v_sectionDark v_key_services" id="services" data-label="Services">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 class="v_visually_hidden"><strong class="v_brand" term="nespresso">Nespresso</strong> Services</h2>
			            <ul class="v_row5">
			                <li data-wow="" class="wow"> <img alt="" class="freeDelivery" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/freeDelivery.svg">
			                    <h3>Free delivery in the next 24 hours</h3>
			                    <p>Receive your coffee in just 24 hours at an address of your choice.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="pickup" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/pickup.svg">
			                    <h3>Pick up in a boutique today</h3>
			                    <p>Order online and collect your order at your favourite Boutique just 2 hours later.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="assistance247" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/assistance247.svg">
			                    <h3>Assistance for your machine</h3>
			                    <p>Need help with your machine? Our experts are here for you.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="payment" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/payment.svg">
			                    <h3>Secured payment transactions</h3>
			                    <p>SSL encryption means sensitive information is always transmitted securely.</p>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>
		</div>
	</div>
</main>
