<?php
	if (!defined('ABSPATH')) {
		exit; // Exit if accessed directly
	}
	
	$url = "http://nespresso.vn/coffee-list";
	global $scripts;
	$scripts[] = 'js/components/productlist.js';
	$scripts[] = 'js/components/shopping-bag.js';
	$link = get_stylesheet_directory_uri();
	$products = get_coffee_products();
	
	krsort($products);
	$product_count = 0;
	foreach ($products as $product):
		foreach ($product as $product):
			$product_count++;
		endforeach;
	endforeach;
	
	$arrays = get_nespresso_product_list();
	if (!$arrays) {
		$categories = custom_acf_get_data('Product Details', 'category');
	} else {
		$categories['choices'] = $arrays['coffee'];
	}


?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/OriginalCoffeeHub.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsiveLoadedFirst.css">
<link rel="stylesheet/less" href="<?php echo get_stylesheet_directory_uri(); ?>/css/grid.sass">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/styles-firenze.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<?php get_template_part('components/generics/qty-selector'); ?>
<div id="block-8810324710901" class="free-html" data-label="">
	<div class="g g_originalLine g_originalCoffeeHub">
		<div class="g_breadcrumb">
			<nav class="g_nav">
				<div class="g_restrict">
					<div class="g_fil">
						<ol>
							<li><a href="https://www.nespresso.com/th/en/order/capsules/" class=""><span
											class="">Coffee</span></a></li>
							<!---->
						</ol>
					</div>
					<div class="g_mobileMenu">
						<button class="g_btnMenu g_txt_S"><span class="">Coffee</span><i class="fn_angleDown"></i>
						</button>
						<div class="g_mobileMenuList">
							<ul>
								<!-- <li><a href="./original-coffee-capsules" class="g_selected"><span class="">Original</span></a></li>
								   <li><a href="./coffeeSelector" class=""><span class="">Coffee selector</span></a></li>
								   <li><a href="./coffee-expertise" class=""><span class="">Expertise</span></a></li>
								   <li><a href="./coffee-origins" class=""><span class="">Origins</span></a></li> -->
								<li>
									<button class=""><span class="">Buy</span></button>
								</li>
							</ul>
						</div>
					</div>
					<div class="g_menu">
						<ul>
							<!-- <li><a href="./original-coffee-capsules" class="g_btn g_selected"><span class="">Original</span></a></li>
							   <li><a href="./coffeeSelector" class="g_btn"><span class="">Coffee selector</span></a></li>
							   <li><a href="./coffee-expertise" class="g_btn"><span class="">Expertise</span></a></li>
							   <li><a href="./coffee-origins" class="g_btn"><span class="">Origins</span></a></li> -->
							<li>
								<button class="g_btn g_btnBuy"><span class="">Buy</span></button>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<div tabindex="-1" aria-hidden="true"></div>
			<!---->
		</div>
		<section id="hero" data-label="YOUR CLASSIC ESPRESSO EXPERIENCE" class="g_section g_hero g_dark g_mobileBottom">
			<div class="g_bg g_parallax g_imgSrc g_imgSrc_loaded"
				 style="background-image: url('/wp-content/uploads/2021/03/original_capsules_ground_L.jpg');"></div>
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text">
						<!---->
						<h1 class="g_h1" style="color: #FFF;font-family:Halcom;font-size: 2.5rem">YOUR CLASSIC ESPRESSO EXPERIENCE</h1>
						<!---->  <!---->
					</div>
				</div>
			</div>
		</section>
		<section id="expertise" data-label="Coffee expertise in a cup" class="g_section g_dark g_left g_mobileBottom">
			<div class="g_bg g_imgSrc g_imgSrc_loaded"
				 style="background-image: url('/wp-content/uploads/2021/03/ol_cups_L.webp');"></div>
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text">
						<h2 class="g_h2" style="color: #FFF;font-family: Halcom-Light;">Coffee expertise in a cup</h2>
						<div class="g_wysiwyg g_txt_XL">
							<p style="font-family: Halcom;">With <strong>Nespresso</strong>, live your authentic
								espresso experience from a milder fruity espresso to
								the Neapolitan style short <strong class="v_brand" term="ristretto">Ristretto</strong>,
								with or without milk.
							</p>
						</div>
						<div class="g_wysiwyg g_txt_L">
							<p style="font-family: Halcom;color: #FFF">Coffee pleasure is no accident: it must be deliberately
								created, consistently and without
								compromise, cup after cup from coffee origins to capsules, by living our principles.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="original_expertise" class="g_section g_photo g_proportion_5_2">
			<div class="g_bg g_imgSrc g_imgSrc_loaded"
				 style="background-image: url('/wp-content/uploads/2021/03/original_expertise_L.jpg');"></div>
		</section>
		<section class="g_section g_sectionRange g_dark g_center">
			<!---->
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text">
						<h2 class="g_h2" style="color: #FFF;font-family: Halcom-Light">Discover <strong class="v_brand"
																										term="nespresso">Nespresso</strong>
							<br>capsules
						</h2>
						<div class="g_wysiwyg g_txt_XL">
							<p>Enjoy your authentic coffee experience with a wide variety of capsules, each has its own
								distinctive character and aroma, with or without milk.
							</p>
						</div>
						<ul class="g_coffeeRange g_row">
							<li class="g_ristretto">
								<div class="g_imageContainer"><img loading="lazy"
																   class="g_visual g_imgSrc g_imgSrc_loaded"
																   src="/wp-content/uploads/2021/03/ristretto_L.webp">
								</div>
								<p class="g_h4_nomargin" style=" font-family:Halcom-Medium; "><strong class="v_brand"
																									  term="ristretto">Ristretto</strong>
								</p>
								<div class="g_wysiwyg g_txt_M">
									<p></p>
								</div>
								<span size="XS" style="font-family: Halcom-Light">25 ml</span>
							</li>
							<li class="g_espresso">
								<div class="g_imageContainer"><img loading="lazy"
																   class="g_visual g_imgSrc g_imgSrc_loaded"
																   src="/wp-content/uploads/2021/03/espresso_L.webp">
								</div>
								<p class="g_h4_nomargin" style=" font-family:Halcom-Medium; ">Espresso</p>
								<div class="g_wysiwyg g_txt_M">
									<p></p>
								</div>
								<span size="XS" style="font-family: Halcom-Light">40 ml</span>
							</li>
							<li class="g_lungo">
								<div class="g_imageContainer"><img loading="lazy"
																   class="g_visual g_imgSrc g_imgSrc_loaded"
																   src="/wp-content/uploads/2021/03/lungo_L.webp"></div>
								<p class="g_h4_nomargin" style=" font-family:Halcom-Medium; ">Lungo</p>
								<div class="g_wysiwyg g_txt_M">
									<p></p>
								</div>
								<span size="XS" style="font-family: Halcom-Light">110 ml</span>
							</li>
						</ul>
						<p class="g_h1"></p>
						<div class="g_wysiwyg g_txt_XL">
							<p></p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="coffeeRange" data-label="" class="g_section g_sectionRange2 g_dark g_center">
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text" style="">
						<div>
							<ul class="g_tiles">
								<li class="g_ispirazione-italiana ispirazione-italiana">
									<section id="ispirazione-italiana"
											 class="g_section g_sectionRange g_dark g_center g_tilesWrapper ">
										<div class="g_bg g_imgSrc g_imgSrc_loaded"
											 style="background-image: url('/wp-content/uploads/2021/03/ispirazione_italiana_L.webp');"></div>
										<button class="g_tile">
											<div class="g_rangeSvgTitle"><img
														class="g_titleSvg g_imgSrc g_imgSrc_loaded"
														src="/wp-content/uploads/2021/03/ispirazione-italiana.svg">
											</div>
											<div class="g_products">
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/ristretto-decaffeinato_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/ristretto-decaffeinato_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/ristretto_S.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/ristretto_S.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/roma_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/roma_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/kazaar_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/kazaar_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/arpeggio_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/arpeggio_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/arpeggio-decaffeinato_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/arpeggio-decaffeinato_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/venezia-2019_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/venezia-2019_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/napoli-2019_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/napoli-2019_L.webp');"></div>
												</div>
											</div>
											<div class="g_description">
												<p class="g_h3">ispirazione italiana</p>
												<div class="g_wysiwyg g_txt_L">
													<p style="font-family: Halcom-Light;color: #FFF">inspired by italian roasting
														tradition
													</p>
												</div>
											</div>
											<ul class="g_features">
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1" id="Layer_1"
														 x="0px" y="0px" style="enable-background:new 0 0 28 28;"
														 xml:space="preserve" class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-ristretto.svg"
														 aria-hidden="true">
										  <path d="M22.3,8h-1.8L21,5.9L20.3,5H2.7L2,5.9l3.6,16.5L6.3,23h10.3l0.7-0.6l1.4-6.4h3.5c2.2,0,4-1.8,4-4S24.5,8,22.3,8z M19.3,6.5  L17.2,16H5.7L3.7,6.5H19.3z M16,21.5H6.9l-0.9-/shared_res4h10.9L16,21.5z M22.3,14.4h-3.1l1.1-4.8h2c1.3,0,2.4,1.1,2.4,2.4  S23.6,14.4,22.3,14.4z"></path>
									   </svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #FFF">
														<strong class="v_brand" term="ristretto">Ristretto 25 ml </strong>
													</div>
												</li>
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1" id="Layer_1"
														 style="enable-background:new 0 0 28 28;" xml:space="preserve"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-espresso.svg"
														 aria-hidden="true">
										  <path d="M22.1,8.5h-1.6l0.5-2.1l-0.7-0.9H2.7L2,6.4L5.5,23l0.7,0.6h10.3l0.7-0.6l1.5-7h3.3c2.1,0,3.8-1.7,3.8-3.8S24.1,8.5,22.1,8.5  z M19.2,7l-1,4.5H4.6L3.7,7H19.2z M16,22.1H6.9L4.9,13h13L16,22.1z M22.1,14.5h-2.9l1-4.5h1.9c1.2,0,2.2,1,2.2,2.2  S23.3,14.5,22.1,14.5z"></path>
									   </svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #FFF">
														Espresso 40 ml
													</div>
												</li>
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-capsule.svg"
														 aria-hidden="true">
														<!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
														<g id="Page-1" stroke="none" stroke-width="1" fill="none"
														   fill-rule="evenodd">
															<g id="original-capsule" fill-rule="nonzero" fill="#000000">
																<path d="M22.8336706,21.1192836 L24.0619573,21.1326567 C24.4416481,21.1326567 24.75,21.4379316 24.75,21.8163284 C24.75,22.1947251 24.4416481,22.5 24.0619573,22.5 L3.93900758,22.5 C3.56001534,22.5 3.25,22.1944733 3.25,21.8163284 C3.25,21.4381834 3.56001534,21.1326567 3.93900758,21.1326567 C4.17452656,21.1307918 4.29794895,21.1291998 4.30927474,21.1278807 L5.16929715,21.127069 C5.28317736,20.8902929 5.36939062,20.6523138 5.40714284,20.4358238 C5.43261247,20.2872326 5.53296875,19.5771527 5.67606627,18.5343082 C5.78834796,17.7226787 5.83647512,17.3695017 6.16996264,14.9157531 L6.9113599,9.42808048 C6.94045588,9.22952735 6.9756872,9.07484319 7.0237638,8.93746624 C7.08266866,8.76718039 7.16588517,8.61648123 7.27393073,8.48953451 C7.40969157,8.33140965 7.53694378,8.23573138 7.81529689,8.06769677 L7.89751036,8.01983244 C8.07641103,7.9103005 8.38431104,7.7356441 9.29045335,7.22631111 L9.36765179,7.18291688 L10.786356,6.38862949 C10.9218272,6.3065953 11.0284433,6.24692553 11.1467617,6.19016378 C11.4125712,6.06459094 11.6705202,6.00191045 11.9317243,6.00191047 L16.0565817,6 C16.3135216,6 16.568618,6.06143865 16.8403032,6.18661438 C16.9522432,6.23984432 17.0409009,6.28980743 17.1851376,6.37674553 L18.6164275,7.17882614 C19.3498406,7.58739665 19.886675,7.89064277 20.088374,8.01219103 L20.1804264,8.06763043 C20.3594707,8.17370586 20.4478238,8.23189261 20.5458752,8.31591091 C20.7355658,8.47553897 20.8804671,8.68729233 20.9666404,8.93282755 C21.0142895,9.076445 21.0477284,9.22993321 21.0740187,9.41848309 C21.0960825,9.56491661 21.1446057,9.91972908 21.3996651,11.7937257 C21.6024386,13.2792879 21.7195909,14.1373499 21.8296028,14.9415492 L21.8384299,15.006072 C22.0404493,16.4826805 22.1986061,17.6325565 22.3230237,18.5270647 C22.4889611,19.7409235 22.5700127,20.3035503 22.5939889,20.4263019 C22.6302779,20.6432188 22.7148655,20.8749902 22.8336706,21.1192836 Z M8.13300614,11.093939 C7.85186656,13.1774114 7.74789575,13.9471297 7.58542208,15.1457247 L7.57489277,15.2233945 C7.38198833,16.6462372 7.22150089,17.8232148 7.0966409,18.7289251 C6.94396278,19.8322508 6.84833128,20.5033805 6.81843532,20.6763417 L6.57208822,20.6337612 L6.81830101,20.6771116 C6.79338901,20.8186017 6.75417677,20.9643272 6.69847962,21.1278806 L21.3028186,21.1278806 C21.2448122,20.9610566 21.2047324,20.8147662 21.1796664,20.6717473 C21.148048,20.4896209 21.0478854,19.7859397 20.90146,18.7205489 C20.6820088,17.1450344 20.548026,16.1749319 20.4025123,15.0984275 C20.0359539,12.3975669 19.7581886,10.3600222 19.6772125,9.78019151 L19.619566,9.43479068 C19.6071996,9.38809375 19.6079584,9.38945452 19.6077572,9.38917915 C19.6096383,9.39153444 19.5217207,9.33450806 19.3550941,9.23413292 C19.1607363,9.1186819 18.5812852,8.79083292 17.9153371,8.41873031 L16.4605386,7.60452098 C16.3532853,7.54081118 16.3263322,7.52518873 16.2803129,7.50114317 C16.2534212,7.48732954 16.2534212,7.48732954 16.2264687,7.47459611 C16.1491862,7.43838988 16.0947379,7.42370149 16.0566975,7.42370147 L11.9318401,7.42561194 C11.8922463,7.42561194 11.8387202,7.43994928 11.7624992,7.4752947 C11.706728,7.5015874 11.6788185,7.51736488 11.5331931,7.60280794 L10.0713446,8.42340021 C9.29816064,8.85696163 8.81748589,9.12952831 8.64061846,9.2368647 C8.44349025,9.35224298 8.40472632,9.37624035 8.37584275,9.40138405 C8.37471511,9.41251173 8.37386395,9.41559252 8.36631209,9.4449449 L8.13300614,11.093939 Z"
																	  id="Fill-1"></path>
															</g>
														</g>
													</svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #FFF">
														Intensity 6-13
													</div>
												</li>
											</ul>
											<a id="ispirazione-italiana-modal" class="ispirazione-italiana-modal">
												<div class="g_roundButton"><i class="fn_more"></i></div>
											</a>
										</button>
									</section>
								</li>
								<li class="g_espresso espresso">
									<section id="espresso"
											 class="g_section g_sectionRange g_dark g_center g_tilesWrapper">
										<div class="g_bg g_imgSrc g_imgSrc_loaded"
											 style="background-image: url('/wp-content/uploads/2021/03/espresso_L.jpg');"></div>
										<button class="g_tile">
											<div class="g_rangeSvgTitle"><img
														class="g_titleSvg g_imgSrc g_imgSrc_loaded"
														src="/wp-content/uploads/2021/03/espresso.svg"></div>
											<div class="g_products">
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/capriccio_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/capriccio_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/volluto_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/volluto_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/cosi_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/cosi_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/volluto-decaffeinato_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/volluto-decaffeinato_L.webp');"></div>
												</div>
											</div>
											<div class="g_description">
												<p class="g_h3">Espresso</p>
												<div class="g_wysiwyg g_txt_L">
													<p style="font-family: Halcom-Light;color: #000">Traditional is best</p>
												</div>
											</div>
											<ul class="g_features">
												<li class="g_feature" style="color: #000">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1" id="Layer_1"
														 style="enable-background:new 0 0 28 28;" xml:space="preserve"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-espresso.svg"
														 aria-hidden="true">
										  <path style="fill: #000;" d="M22.1,8.5h-1.6l0.5-2.1l-0.7-0.9H2.7L2,6.4L5.5,23l0.7,0.6h10.3l0.7-0.6l1.5-7h3.3c2.1,0,3.8-1.7,3.8-3.8S24.1,8.5,22.1,8.5  z M19.2,7l-1,4.5H4.6L3.7,7H19.2z M16,22.1H6.9L4.9,13h13L16,22.1z M22.1,14.5h-2.9l1-4.5h1.9c1.2,0,2.2,1,2.2,2.2  S23.3,14.5,22.1,14.5z"></path>
									   </svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;">
														Espresso 40 ml
													</div>
												</li>
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-capsule.svg"
														 aria-hidden="true">
														<g id="Page-1" stroke="none" stroke-width="1" fill="none"
														   fill-rule="evenodd">
															<g id="original-capsule" fill-rule="nonzero" fill="#000000">
																<path style="fill: #000;" d="M22.8336706,21.1192836 L24.0619573,21.1326567 C24.4416481,21.1326567 24.75,21.4379316 24.75,21.8163284 C24.75,22.1947251 24.4416481,22.5 24.0619573,22.5 L3.93900758,22.5 C3.56001534,22.5 3.25,22.1944733 3.25,21.8163284 C3.25,21.4381834 3.56001534,21.1326567 3.93900758,21.1326567 C4.17452656,21.1307918 4.29794895,21.1291998 4.30927474,21.1278807 L5.16929715,21.127069 C5.28317736,20.8902929 5.36939062,20.6523138 5.40714284,20.4358238 C5.43261247,20.2872326 5.53296875,19.5771527 5.67606627,18.5343082 C5.78834796,17.7226787 5.83647512,17.3695017 6.16996264,14.9157531 L6.9113599,9.42808048 C6.94045588,9.22952735 6.9756872,9.07484319 7.0237638,8.93746624 C7.08266866,8.76718039 7.16588517,8.61648123 7.27393073,8.48953451 C7.40969157,8.33140965 7.53694378,8.23573138 7.81529689,8.06769677 L7.89751036,8.01983244 C8.07641103,7.9103005 8.38431104,7.7356441 9.29045335,7.22631111 L9.36765179,7.18291688 L10.786356,6.38862949 C10.9218272,6.3065953 11.0284433,6.24692553 11.1467617,6.19016378 C11.4125712,6.06459094 11.6705202,6.00191045 11.9317243,6.00191047 L16.0565817,6 C16.3135216,6 16.568618,6.06143865 16.8403032,6.18661438 C16.9522432,6.23984432 17.0409009,6.28980743 17.1851376,6.37674553 L18.6164275,7.17882614 C19.3498406,7.58739665 19.886675,7.89064277 20.088374,8.01219103 L20.1804264,8.06763043 C20.3594707,8.17370586 20.4478238,8.23189261 20.5458752,8.31591091 C20.7355658,8.47553897 20.8804671,8.68729233 20.9666404,8.93282755 C21.0142895,9.076445 21.0477284,9.22993321 21.0740187,9.41848309 C21.0960825,9.56491661 21.1446057,9.91972908 21.3996651,11.7937257 C21.6024386,13.2792879 21.7195909,14.1373499 21.8296028,14.9415492 L21.8384299,15.006072 C22.0404493,16.4826805 22.1986061,17.6325565 22.3230237,18.5270647 C22.4889611,19.7409235 22.5700127,20.3035503 22.5939889,20.4263019 C22.6302779,20.6432188 22.7148655,20.8749902 22.8336706,21.1192836 Z M8.13300614,11.093939 C7.85186656,13.1774114 7.74789575,13.9471297 7.58542208,15.1457247 L7.57489277,15.2233945 C7.38198833,16.6462372 7.22150089,17.8232148 7.0966409,18.7289251 C6.94396278,19.8322508 6.84833128,20.5033805 6.81843532,20.6763417 L6.57208822,20.6337612 L6.81830101,20.6771116 C6.79338901,20.8186017 6.75417677,20.9643272 6.69847962,21.1278806 L21.3028186,21.1278806 C21.2448122,20.9610566 21.2047324,20.8147662 21.1796664,20.6717473 C21.148048,20.4896209 21.0478854,19.7859397 20.90146,18.7205489 C20.6820088,17.1450344 20.548026,16.1749319 20.4025123,15.0984275 C20.0359539,12.3975669 19.7581886,10.3600222 19.6772125,9.78019151 L19.619566,9.43479068 C19.6071996,9.38809375 19.6079584,9.38945452 19.6077572,9.38917915 C19.6096383,9.39153444 19.5217207,9.33450806 19.3550941,9.23413292 C19.1607363,9.1186819 18.5812852,8.79083292 17.9153371,8.41873031 L16.4605386,7.60452098 C16.3532853,7.54081118 16.3263322,7.52518873 16.2803129,7.50114317 C16.2534212,7.48732954 16.2534212,7.48732954 16.2264687,7.47459611 C16.1491862,7.43838988 16.0947379,7.42370149 16.0566975,7.42370147 L11.9318401,7.42561194 C11.8922463,7.42561194 11.8387202,7.43994928 11.7624992,7.4752947 C11.706728,7.5015874 11.6788185,7.51736488 11.5331931,7.60280794 L10.0713446,8.42340021 C9.29816064,8.85696163 8.81748589,9.12952831 8.64061846,9.2368647 C8.44349025,9.35224298 8.40472632,9.37624035 8.37584275,9.40138405 C8.37471511,9.41251173 8.37386395,9.41559252 8.36631209,9.4449449 L8.13300614,11.093939 Z"
																	  id="Fill-1"></path>
															</g>
														</g>
													</svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #000;">
														Intensity 4-5
													</div>
												</li>
											</ul>
											<a id="espresso-modal" class="espresso-modal">
												<div class="g_roundButton"><i class="fn_more"></i></div>
											</a>
										</button>
									</section>
								</li>
								<li class="g_world-explorations world-explorations">
									<section id="world-explorations"
											 class="g_section g_sectionRange g_dark g_center g_tilesWrapper">
										<div class="g_bg g_imgSrc g_imgSrc_loaded"
											 style="background-image: url('/wp-content/uploads/2021/03/world-explorations_L.webp');"></div>
										<button class="g_tile">
											<div class="g_rangeSvgTitle"><img
														class="g_titleSvg g_imgSrc g_imgSrc_loaded"
														src="/wp-content/uploads/2021/03/world-explorations.svg"></div>
											<div class="g_products">
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/capetown-lungo_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/capetown-lungo_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/stockholm-lungo_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/stockholm-lungo_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/tokyo-lungo_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/tokyo-lungo_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/viena-lungo_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/viena-lungo_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/shanghai-lungo_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/shanghai-lungo_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/buenosaires-lungo_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/buenosaires-lungo_L.webp');"></div>
												</div>
											</div>
											<div class="g_description">
												<p class="g_h3">World Explorations</p>
												<div class="g_wysiwyg g_txt_L">
													<p style="font-family: Halcom-Light;color: #FFF">An invitation to travel the
														globe through coffee
													</p>
												</div>
											</div>
											<ul class="g_features">
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1" id="Layer_1"
														 style="enable-background:new 0 0 28 28;" xml:space="preserve"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-espresso.svg"
														 aria-hidden="true">
										  <path d="M22.1,8.5h-1.6l0.5-2.1l-0.7-0.9H2.7L2,6.4L5.5,23l0.7,0.6h10.3l0.7-0.6l1.5-7h3.3c2.1,0,3.8-1.7,3.8-3.8S24.1,8.5,22.1,8.5  z M19.2,7l-1,4.5H4.6L3.7,7H19.2z M16,22.1H6.9L4.9,13h13L16,22.1z M22.1,14.5h-2.9l1-4.5h1.9c1.2,0,2.2,1,2.2,2.2  S23.3,14.5,22.1,14.5z"></path>
									   </svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color:#FFF">
														Lungo 110 ml
													</div>
												</li>
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-capsule.svg"
														 aria-hidden="true">
														<g id="Page-1" stroke="none" stroke-width="1" fill="none"
														   fill-rule="evenodd">
															<g id="original-capsule" fill-rule="nonzero" fill="#000000">
																<path d="M22.8336706,21.1192836 L24.0619573,21.1326567 C24.4416481,21.1326567 24.75,21.4379316 24.75,21.8163284 C24.75,22.1947251 24.4416481,22.5 24.0619573,22.5 L3.93900758,22.5 C3.56001534,22.5 3.25,22.1944733 3.25,21.8163284 C3.25,21.4381834 3.56001534,21.1326567 3.93900758,21.1326567 C4.17452656,21.1307918 4.29794895,21.1291998 4.30927474,21.1278807 L5.16929715,21.127069 C5.28317736,20.8902929 5.36939062,20.6523138 5.40714284,20.4358238 C5.43261247,20.2872326 5.53296875,19.5771527 5.67606627,18.5343082 C5.78834796,17.7226787 5.83647512,17.3695017 6.16996264,14.9157531 L6.9113599,9.42808048 C6.94045588,9.22952735 6.9756872,9.07484319 7.0237638,8.93746624 C7.08266866,8.76718039 7.16588517,8.61648123 7.27393073,8.48953451 C7.40969157,8.33140965 7.53694378,8.23573138 7.81529689,8.06769677 L7.89751036,8.01983244 C8.07641103,7.9103005 8.38431104,7.7356441 9.29045335,7.22631111 L9.36765179,7.18291688 L10.786356,6.38862949 C10.9218272,6.3065953 11.0284433,6.24692553 11.1467617,6.19016378 C11.4125712,6.06459094 11.6705202,6.00191045 11.9317243,6.00191047 L16.0565817,6 C16.3135216,6 16.568618,6.06143865 16.8403032,6.18661438 C16.9522432,6.23984432 17.0409009,6.28980743 17.1851376,6.37674553 L18.6164275,7.17882614 C19.3498406,7.58739665 19.886675,7.89064277 20.088374,8.01219103 L20.1804264,8.06763043 C20.3594707,8.17370586 20.4478238,8.23189261 20.5458752,8.31591091 C20.7355658,8.47553897 20.8804671,8.68729233 20.9666404,8.93282755 C21.0142895,9.076445 21.0477284,9.22993321 21.0740187,9.41848309 C21.0960825,9.56491661 21.1446057,9.91972908 21.3996651,11.7937257 C21.6024386,13.2792879 21.7195909,14.1373499 21.8296028,14.9415492 L21.8384299,15.006072 C22.0404493,16.4826805 22.1986061,17.6325565 22.3230237,18.5270647 C22.4889611,19.7409235 22.5700127,20.3035503 22.5939889,20.4263019 C22.6302779,20.6432188 22.7148655,20.8749902 22.8336706,21.1192836 Z M8.13300614,11.093939 C7.85186656,13.1774114 7.74789575,13.9471297 7.58542208,15.1457247 L7.57489277,15.2233945 C7.38198833,16.6462372 7.22150089,17.8232148 7.0966409,18.7289251 C6.94396278,19.8322508 6.84833128,20.5033805 6.81843532,20.6763417 L6.57208822,20.6337612 L6.81830101,20.6771116 C6.79338901,20.8186017 6.75417677,20.9643272 6.69847962,21.1278806 L21.3028186,21.1278806 C21.2448122,20.9610566 21.2047324,20.8147662 21.1796664,20.6717473 C21.148048,20.4896209 21.0478854,19.7859397 20.90146,18.7205489 C20.6820088,17.1450344 20.548026,16.1749319 20.4025123,15.0984275 C20.0359539,12.3975669 19.7581886,10.3600222 19.6772125,9.78019151 L19.619566,9.43479068 C19.6071996,9.38809375 19.6079584,9.38945452 19.6077572,9.38917915 C19.6096383,9.39153444 19.5217207,9.33450806 19.3550941,9.23413292 C19.1607363,9.1186819 18.5812852,8.79083292 17.9153371,8.41873031 L16.4605386,7.60452098 C16.3532853,7.54081118 16.3263322,7.52518873 16.2803129,7.50114317 C16.2534212,7.48732954 16.2534212,7.48732954 16.2264687,7.47459611 C16.1491862,7.43838988 16.0947379,7.42370149 16.0566975,7.42370147 L11.9318401,7.42561194 C11.8922463,7.42561194 11.8387202,7.43994928 11.7624992,7.4752947 C11.706728,7.5015874 11.6788185,7.51736488 11.5331931,7.60280794 L10.0713446,8.42340021 C9.29816064,8.85696163 8.81748589,9.12952831 8.64061846,9.2368647 C8.44349025,9.35224298 8.40472632,9.37624035 8.37584275,9.40138405 C8.37471511,9.41251173 8.37386395,9.41559252 8.36631209,9.4449449 L8.13300614,11.093939 Z"
																	  id="Fill-1"></path>
															</g>
														</g>
													</svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #FFF">
														Intensity 4-10
													</div>
												</li>
											</ul>
											<a id="worldexplorations-modal" class="worldexplorations-modal">
												<div class="g_roundButton"><i class="fn_more"></i></div>
											</a>
										</button>
									</section>
								</li>
								<li class="g_master-origins master-origins">
									<section id="master-origins"
											 class="g_section g_sectionRange g_dark g_center g_tilesWrapper">
										<div class="g_bg g_imgSrc g_imgSrc_loaded"
											 style="background-image: url('/wp-content/uploads/2021/03/master_origins_L.webp');"></div>
										<button class="g_tile">
											<div class="g_rangeSvgTitle"><img
														class="g_titleSvg g_imgSrc g_imgSrc_loaded"
														src="/wp-content/uploads/2021/03/master-origins.svg"></div>
											<div class="g_products">
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/india_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/india_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/indonesia_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/indonesia_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/nicaragua_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/nicaragua_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/ethiopia_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/ethiopia_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/colombia_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/colombia_L.webp');"></div>
												</div>
											</div>
											<div class="g_description">
												<p class="g_h3" style="color: #FFF">Master Origins</p>
												<div class="g_wysiwyg g_txt_L">
													<p style="font-family: Halcom-Light;color: #FFF">Mastered By Craftsmen, Inspired
														By The Land
													</p>
												</div>
											</div>
											<ul class="g_features">
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1" id="Layer_1"
														 style="enable-background:new 0 0 28 28;" xml:space="preserve"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-espresso.svg"
														 aria-hidden="true">
										  <path d="M22.1,8.5h-1.6l0.5-2.1l-0.7-0.9H2.7L2,6.4L5.5,23l0.7,0.6h10.3l0.7-0.6l1.5-7h3.3c2.1,0,3.8-1.7,3.8-3.8S24.1,8.5,22.1,8.5  z M19.2,7l-1,4.5H4.6L3.7,7H19.2z M16,22.1H6.9L4.9,13h13L16,22.1z M22.1,14.5h-2.9l1-4.5h1.9c1.2,0,2.2,1,2.2,2.2  S23.3,14.5,22.1,14.5z"></path>
									   </svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color:#FFF;">
														Espresso 40 ml
													</div>
												</li>
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1" id="Layer_1"
														 x="0px" y="0px" style="enable-background:new 0 0 28 28;"
														 xml:space="preserve" class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-lungo.svg"
														 aria-hidden="true">
										  <path d="M23.6,7.2h-1.8l0.7-3.1l-0.7-0.9H1L0.3,4.2l4.2,19.8l0.7,0.6h12.2l0.7-0.6l2-9.2h3.4c2.1,0,3.8-1.7,3.8-3.8  S25.7,7.2,23.6,7.2z M20.8,4.8L19.7,10H3L1.9,4.8H20.8z M16.8,23h-11L3.4,11.5h16L16.8,23z M23.6,13.2h-2.9l1-4.5h1.9  c1.2,0,2.2,1,2.2,2.2S24.8,13.2,23.6,13.2z"></path>
									   </svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #FFF">
														Lungo 110 ml
													</div>
												</li>
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg"
														 width="28px"
														 height="28px" viewBox="0 0 28 28" version="1.1"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-capsule.svg"
														 aria-hidden="true">
														<!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
														<g id="Page-1" stroke="none" stroke-width="1" fill="none"
														   fill-rule="evenodd">
															<g id="original-capsule" fill-rule="nonzero" fill="#000000">
																<path d="M22.8336706,21.1192836 L24.0619573,21.1326567 C24.4416481,21.1326567 24.75,21.4379316 24.75,21.8163284 C24.75,22.1947251 24.4416481,22.5 24.0619573,22.5 L3.93900758,22.5 C3.56001534,22.5 3.25,22.1944733 3.25,21.8163284 C3.25,21.4381834 3.56001534,21.1326567 3.93900758,21.1326567 C4.17452656,21.1307918 4.29794895,21.1291998 4.30927474,21.1278807 L5.16929715,21.127069 C5.28317736,20.8902929 5.36939062,20.6523138 5.40714284,20.4358238 C5.43261247,20.2872326 5.53296875,19.5771527 5.67606627,18.5343082 C5.78834796,17.7226787 5.83647512,17.3695017 6.16996264,14.9157531 L6.9113599,9.42808048 C6.94045588,9.22952735 6.9756872,9.07484319 7.0237638,8.93746624 C7.08266866,8.76718039 7.16588517,8.61648123 7.27393073,8.48953451 C7.40969157,8.33140965 7.53694378,8.23573138 7.81529689,8.06769677 L7.89751036,8.01983244 C8.07641103,7.9103005 8.38431104,7.7356441 9.29045335,7.22631111 L9.36765179,7.18291688 L10.786356,6.38862949 C10.9218272,6.3065953 11.0284433,6.24692553 11.1467617,6.19016378 C11.4125712,6.06459094 11.6705202,6.00191045 11.9317243,6.00191047 L16.0565817,6 C16.3135216,6 16.568618,6.06143865 16.8403032,6.18661438 C16.9522432,6.23984432 17.0409009,6.28980743 17.1851376,6.37674553 L18.6164275,7.17882614 C19.3498406,7.58739665 19.886675,7.89064277 20.088374,8.01219103 L20.1804264,8.06763043 C20.3594707,8.17370586 20.4478238,8.23189261 20.5458752,8.31591091 C20.7355658,8.47553897 20.8804671,8.68729233 20.9666404,8.93282755 C21.0142895,9.076445 21.0477284,9.22993321 21.0740187,9.41848309 C21.0960825,9.56491661 21.1446057,9.91972908 21.3996651,11.7937257 C21.6024386,13.2792879 21.7195909,14.1373499 21.8296028,14.9415492 L21.8384299,15.006072 C22.0404493,16.4826805 22.1986061,17.6325565 22.3230237,18.5270647 C22.4889611,19.7409235 22.5700127,20.3035503 22.5939889,20.4263019 C22.6302779,20.6432188 22.7148655,20.8749902 22.8336706,21.1192836 Z M8.13300614,11.093939 C7.85186656,13.1774114 7.74789575,13.9471297 7.58542208,15.1457247 L7.57489277,15.2233945 C7.38198833,16.6462372 7.22150089,17.8232148 7.0966409,18.7289251 C6.94396278,19.8322508 6.84833128,20.5033805 6.81843532,20.6763417 L6.57208822,20.6337612 L6.81830101,20.6771116 C6.79338901,20.8186017 6.75417677,20.9643272 6.69847962,21.1278806 L21.3028186,21.1278806 C21.2448122,20.9610566 21.2047324,20.8147662 21.1796664,20.6717473 C21.148048,20.4896209 21.0478854,19.7859397 20.90146,18.7205489 C20.6820088,17.1450344 20.548026,16.1749319 20.4025123,15.0984275 C20.0359539,12.3975669 19.7581886,10.3600222 19.6772125,9.78019151 L19.619566,9.43479068 C19.6071996,9.38809375 19.6079584,9.38945452 19.6077572,9.38917915 C19.6096383,9.39153444 19.5217207,9.33450806 19.3550941,9.23413292 C19.1607363,9.1186819 18.5812852,8.79083292 17.9153371,8.41873031 L16.4605386,7.60452098 C16.3532853,7.54081118 16.3263322,7.52518873 16.2803129,7.50114317 C16.2534212,7.48732954 16.2534212,7.48732954 16.2264687,7.47459611 C16.1491862,7.43838988 16.0947379,7.42370149 16.0566975,7.42370147 L11.9318401,7.42561194 C11.8922463,7.42561194 11.8387202,7.43994928 11.7624992,7.4752947 C11.706728,7.5015874 11.6788185,7.51736488 11.5331931,7.60280794 L10.0713446,8.42340021 C9.29816064,8.85696163 8.81748589,9.12952831 8.64061846,9.2368647 C8.44349025,9.35224298 8.40472632,9.37624035 8.37584275,9.40138405 C8.37471511,9.41251173 8.37386395,9.41559252 8.36631209,9.4449449 L8.13300614,11.093939 Z"
																	  id="Fill-1"></path>
															</g>
														</g>
													</svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #FFF">
														Intensity 4-11
													</div>
												</li>
											</ul>
											<a id="master-origins-modal" class="master-origins-modal">
												<div class="g_roundButton"><i class="fn_more"></i></div>
											</a>
										</button>
									</section>
								</li>
								<li class="g_barista-creation barista-creation">
									<section id="barista-creation"
											 class="g_section g_sectionRange g_dark g_center g_tilesWrapper">
										<div class="g_bg g_imgSrc g_imgSrc_loaded"
											 style="background-image: url('/wp-content/uploads/2021/03/barista_creation_L.webp');"></div>
										<button class="g_tile">
											<div class="g_rangeSvgTitle"><img
														class="g_titleSvg g_imgSrc g_imgSrc_loaded"
														src="/wp-content/uploads/2021/03/barista-creation.svg"></div>
											<div class="g_products">
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/cocoa-truffle_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/cocoa-truffle_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/vanilla-eclair_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/vanilla-eclair_L.webp');"></div>
												</div>
												<div class="g_product">
													<div class="g_capsule g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/caramel-creme-brulee_L.webp');"></div>
													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>
													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"
														 style="background-image: url('/wp-content/uploads/2021/03/caramel-creme-brulee_L.webp');"></div>
												</div>
<!--												<div class="g_product">-->
<!--													<div class="g_capsule g_imgSrc g_imgSrc_loaded"-->
<!--														 style="background-image: url('/wp-content/uploads/2021/03/chiaro_L.webp');"></div>-->
<!--													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>-->
<!--													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"-->
<!--														 style="background-image: url('/wp-content/uploads/2021/03/chiaro_L.webp');"></div>-->
<!--												</div>-->
<!--												<div class="g_product">-->
<!--													<div class="g_capsule g_imgSrc g_imgSrc_loaded"-->
<!--														 style="background-image: url('/wp-content/uploads/2021/03/scuro_L.webp');"></div>-->
<!--													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>-->
<!--													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"-->
<!--														 style="background-image: url('/wp-content/uploads/2021/03/scuro_L.webp');"></div>-->
<!--												</div>-->
<!--												<div class="g_product">-->
<!--													<div class="g_capsule g_imgSrc g_imgSrc_loaded"-->
<!--														 style="background-image: url('/wp-content/uploads/2021/03/corto_L.webp');"></div>-->
<!--													<div class="g_capsule g_mask g_imgSrc g_imgSrc_outOfScreen"></div>-->
<!--													<div class="g_capsule g_reflect g_imgSrc g_imgSrc_loaded"-->
<!--														 style="background-image: url('/wp-content/uploads/2021/03/corto_L.webp');"></div>-->
<!--												</div>-->
											</div>
											<div class="g_description">
												<p class="g_h3">Barista Creations</p>
												<div class="g_wysiwyg g_txt_L">
													<p style="font-family: Halcom-Light;color: #000">Designed for the full spread of
														the coffee recipes
													</p>
												</div>
											</div>
											<ul class="g_features">
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 141.73 141.73"
														 class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/flavoured.svg"
														 aria-hidden="true">
														<path style="fill: #000" d="M25.37 70.7c7.42-3.22 13.98-6.43 20.8-8.93 6.45-2.36 12.99-3.33 19.37 1.14 3.92 2.74 8.61 2.27 12.41-.38 7.33-5.11 14.33-3.68 21.22.42 5.59 3.34 11.05 6.86 17.19 9.47-6.58 1.05-13.06-4.84-19.6-7.16-4.85-1.72-9.65-1.68-14.51.48-7.43 3.31-14.87 3.9-22.31-.42-3.35-1.94-7.3-2.2-11.07-1.47-7.51 1.43-14.97 8.55-23.5 6.85M113.58 76.18c-1.67 1.1-3.46 2.02-5.28 2.86-10.18 4.49-20.88 5.37-31.64 4.04-4.99-1.15-9.14-.52-15.53 2.59 14.28 5.49 35.33 2.06 45.47-3.47.43-.23.86-.48 1.29-.75.19-.12.39-.23.57-.36.41-.28.81-.59 1.21-.91.16-.13.33-.25.48-.37a23.06 23.06 0 0 0 3.43-3.63"></path>
													</svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #000">Flavoured</div>
												</li>
												<li class="g_feature">
													<svg xmlns="http://www.w3.org/2000/svg" height="35"
														 viewBox="0 0 65 72" class="injected-svg g_visual"
														 data-src="/wp-content/uploads/2021/03/original-milk.svg"
														 aria-hidden="true">
														<g fill="#000" fill-rule="nonzero">
															<path style="fill: #000" d="M9.81 38.17c.44 0 .88-.05 1.33-.15a5.874 5.874 0 0 0 3.68-2.62c.83-1.34 1.1-2.92.74-4.46-.4-1.75-1.54-2.82-2.54-3.76-.4-.38-.79-.74-1.13-1.14-.95-1.1-1.21-1.87-1.14-2.22.01-.07.05-.24.42-.44l-.93-1.77s-.01.01-.02.01-.02.01-.02.01c-.7.37-7.87 4.47-6.14 11.96a5.918 5.918 0 0 0 5.75 4.58zm-.89-13.18c.25.73.73 1.51 1.45 2.35.41.48.85.89 1.28 1.29.91.86 1.7 1.6 1.97 2.76.24 1.02.06 2.06-.49 2.95-.55.89-1.41 1.5-2.43 1.73a3.91 3.91 0 0 1-4.69-2.93c-.88-3.8 1.14-6.55 2.91-8.15zM64.24 36.64c-.15-.83-.46-1.65-.92-2.42-.24-.4-.53-.79-.84-1.16l-16.9-19.45c-1.57-1.8-3.02-2.86-5.16-3.77-.87-.37-3.01-1.28-8.24-3.54l-.26-.11c-.22-.09-.3-.15-.32-.17a.761.761 0 0 1-.1-.12c-.03-.04-.04-.08-.06-.11a.556.556 0 0 1-.03-.19c0-.11.01-.16.02-.18.02-.06.05-.12.09-.2l.11-.24c.08-.18.16-.4.21-.65.09-.42.09-.87 0-1.29-.06-.3-.17-.59-.31-.85-.16-.28-.37-.54-.62-.76-.39-.34-.86-.58-1.37-.69-.38-.08-.77-.08-1.17-.01-.54.1-1.03.33-1.43.68l-11.19 9.73c-.29.25-.53.54-.69.86-.21.39-.33.85-.34 1.3-.01.38.04.75.17 1.1.11.31.26.59.45.84.29.38.67.67 1.12.88.3.13.61.22.92.26.5.06.92-.02 1.19-.08.13-.03.26-.07.4-.11.07-.02.13-.04.17-.05 0 0 .05-.01.16.01s.16.04.18.06c.03.02.06.04.1.07.06.05.09.1.11.12.01.02.05.1.12.34l.49 1.78c.64 2.34 1.31 4.76 1.94 7.14.3 1.13.68 2.11 1.15 2.99.46.87 1.07 1.73 1.87 2.65l16.9 19.44c.32.37.67.7 1.03 1 .7.56 1.46.98 2.27 1.25.58.19 1.16.29 1.74.31h.2c.46 0 .93-.05 1.38-.16.58-.13 1.16-.36 1.71-.66.51-.28 1-.63 1.51-1.07l9.83-8.54c.51-.45.93-.89 1.27-1.34.37-.5.67-1.04.89-1.6.19-.5.32-1.02.38-1.54 0-.56-.03-1.15-.13-1.75zm-1.92 1.55c-.04.35-.12.69-.25 1.03-.15.39-.36.77-.63 1.13-.25.34-.57.68-.97 1.03l-9.83 8.54c-.4.35-.78.62-1.16.82-.39.21-.79.37-1.2.46-.35.08-.71.12-1.06.11-.39-.01-.78-.08-1.18-.21a5.03 5.03 0 0 1-1.64-.91c-.27-.22-.53-.47-.77-.75l-16.9-19.45c-.69-.8-1.22-1.54-1.61-2.28-.4-.74-.72-1.58-.98-2.56-.64-2.39-1.3-4.81-1.95-7.16l-.49-1.78c-.07-.24-.16-.54-.31-.81-.13-.23-.32-.45-.54-.64a2.75 2.75 0 0 0-.48-.33c-.21-.11-.46-.2-.73-.25-.18-.03-.34-.05-.51-.05-.13 0-.25.01-.38.03-.1.02-.24.06-.38.1-.1.03-.2.06-.3.08-.13.03-.3.06-.49.04-.11-.01-.22-.04-.33-.09a.957.957 0 0 1-.37-.28.856.856 0 0 1-.14-.27c-.04-.12-.06-.25-.06-.38.01-.16.04-.31.11-.44.05-.1.13-.2.23-.28l11.19-9.73c.13-.11.29-.18.48-.22.14-.03.28-.02.41 0 .17.04.33.12.46.23.08.07.15.15.2.24.04.08.08.17.1.27.03.15.03.31 0 .47-.03.12-.07.22-.1.3l-.09.21c-.07.15-.13.29-.16.39-.09.27-.13.56-.11.88.01.27.06.52.14.76.07.19.15.37.26.53.15.25.35.46.56.63.25.2.54.33.77.43l.26.11c5.25 2.26 7.38 3.18 8.25 3.54 1.86.79 3.06 1.67 4.43 3.24l16.9 19.45c.24.28.45.57.63.87.34.56.56 1.15.67 1.75.07.42.09.82.05 1.2zM19 55h-1.11l.12-1H18v-1.47c0-2.13-1.57-3.75-3.66-3.75-.36 0-.72.05-1.06.16-.89-1.21-2.25-1.94-3.74-1.94-1.49 0-2.86.72-3.74 1.94-.34-.11-.7-.16-1.06-.16-2.06 0-3.74 1.74-3.74 3.88V54H.99L3 71.12 4 72h11l.99-.88L17 62.55V63h2c1.61 0 4-1.07 4-4 0-2.93-2.39-4-4-4zM3 52.66c0-1.04.78-1.88 1.74-1.88.32 0 .62.09.91.28l1.46-.41c.47-1.01 1.4-1.64 2.44-1.64s1.97.63 2.44 1.64l1.46.41c.28-.19.59-.28.9-.28.98 0 1.66.72 1.66 1.75V54H3v-1.34zM3.24 56h12.52l-.24 2H3.48l-.24-2zm10.87 14H4.89L3.71 60h11.57l-1.17 10zM19 61h-1.82l.47-4H19c.33 0 2 .1 2 2 0 1.83-1.54 1.99-2 2z"></path>
														</g>
													</svg>
													<div class="g_wysiwyg g_txt_XS" style="font-family: Halcom-Medium;color: #000">
														For milk recipes
													</div>
												</li>
											</ul>
											<a id="barista-creations-modal" class="barista-creations-modal">
												<div class="g_roundButton"><i class="fn_more"></i></div>
											</a>
										</button>
									</section>
								</li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="tabs" data-label="" class="g_section g_sectionTab g_mobileBottom">
			<div class="g_multipleBg">
				<div class="g_bg g_active" id="nav001"
					 style="background-image: url('/wp-content/uploads/2021/03/origin_L.webp');"></div>
				<div class="g_bg " id="nav002"
					 style="background-image: url('/wp-content/uploads/2021/03/roasting_L.webp');"></div>
				<div class="g_bg " id="nav003"
					 style="background-image: url('/wp-content/uploads/2021/03/tasting_L.webp');"></div>
				<div class="g_bg " id="nav004"
					 style="background-image: url('/wp-content/uploads/2021/03/coffee_drop_L.webp');"></div>
			</div>
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text">
						<h2 class="g_h2" id="header-tabs" style="font-family: Halcom-Light;color: #FFF">Explore our coffee world</h2>
						<div role="tabpanel" class="g_tabs">
							<span id="v_a11y_categories" class="g_visually_hidden">You are in a tab selection: choose an option to display it's content below.</span>
							<ul role="tablist" class="nav nav-tabs" aria-labelledby="v_a11y_categories">
								<li>
									<button style="box-shadow: unset;" aria-expanded="true" id="nav001" class="navlist g_active">
										<h3 class="g_h3" style="font-family: Halcom">Origin</h3>
									</button>
								</li>
								<li>
									<button style="box-shadow: unset;" aria-expanded="false" id="nav002" class="navlist">
										<h3 class="g_h3" style="font-family: Halcom">Intensity</h3>
									</button>
								</li>
								<li>
									<button style="box-shadow: unset;" aria-expanded="false" id="nav003" class="navlist">
										<h3 class="g_h3" style="font-family: Halcom">AROMATIC NOTES</h3>
									</button>
								</li>
								<li>
									<button style="box-shadow: unset;" aria-expanded="false" id="nav004" class="navlist">
										<h3 class="g_h3" style="font-family: Halcom">CUP SIZE</h3>
									</button>
								</li>
							</ul>
						</div>
						<script>
							jQuery(`.navlist`).click(function () {
								console.log(this.id);
								
								if(this.id == 'nav003')
                                {
                                    let object = `.g_tabContent #${this.id}`;
                                    let object2 = `.g_multipleBg #${this.id}`;


                                    jQuery(`.g_tabs .g_active`).removeClass('g_active');
                                    jQuery(`.g_tabs #${this.id}`).addClass('g_active');


                                    jQuery(`.g_tabContent .g_active`).removeClass('g_active');
                                    jQuery(`.g_multipleBg .g_active`).removeClass('g_active');

                                    jQuery(object).addClass('g_active');
                                    jQuery(object2).addClass('g_active');
                                    jQuery(`#${this.id}`).addClass('active');
                                    jQuery(`h2#header-tabs`).css("color", "#000");

                                    jQuery(object).attr("aria-expanded", "true");
                                }
								else
                                {
                                    let object = `.g_tabContent #${this.id}`;
                                    let object2 = `.g_multipleBg #${this.id}`;


                                    jQuery(`.g_tabs .g_active`).removeClass('g_active');
                                    jQuery(`.g_tabs #${this.id}`).addClass('g_active');


                                    jQuery(`.g_tabContent .g_active`).removeClass('g_active');
                                    jQuery(`.g_multipleBg .g_active`).removeClass('g_active');

                                    jQuery(object).addClass('g_active');
                                    jQuery(object2).addClass('g_active');
                                    jQuery(`#${this.id}`).addClass('active');
                                    jQuery(`h2#header-tabs`).css("color", "#FFF");

                                    jQuery(object).attr("aria-expanded", "true");
                                }

								

							});

						</script>
						<div class="g_tabContent tab-content">
							<div class="g_text  g_active" id="nav001">
								<div class="g_wysiwyg g_txt_M">
									<p style="font-family: Halcom">The taste of a Nespresso coffee depends on its blend
										composition and terroir.
									</p>
								</div>
								<h4 class="g_headline" style="color: #FFF;font-family: Halcom-Medium">BLEND</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="font-family: Halcom">Various types of Arabicas and Robustas.</p>
								</div>
								<h4 class="g_headline" style="color: #FFF;font-family: Halcom-Medium">Terroir</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="font-family: Halcom">Altitude, soil composition and climate
										temperature.
									</p>
								</div>
							</div>
							<div class="g_text" id="nav002">
								<h4 class="g_headline" style="color: #FFF;font-family: Halcom-Medium">Body</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="font-family: Halcom">How bold can you go? Go full-bodied for a powerful
										experience or keep it gentle
										and light-bodied. Take your pick.
									</p>
								</div>
								<h4 class="g_headline" style="color: #FFF;font-family: Halcom-Medium">BITTERNESS</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="font-family: Halcom"> Dark or distinguished. Find out what level of
										bitterness is for you.
									</p>
								</div>
								<h4 class="g_headline" style="color: #FFF;font-family: Halcom-Medium">ROASTING</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="font-family: Halcom">Choose what level of fuller, richer roast flavours
										you prefer in your Nespresso
										coffee.
									</p>
								</div>
							</div>
							<div class="g_text" id="nav003">
								<div class="g_wysiwyg g_txt_M">
									<p style="color: #000;font-family: Halcom-Medium">Dominant aromatic notes define the
										aromatic profile, the
										character of the coffee, regardless of its intensity. Aromatic notes are
										clustered into 3 aromatic families:
									</p>
								</div>
								<h4 class="g_headline" style="font-family: Halcom-Medium">FRUITY &amp; FLOWERY</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="color: #000;font-family: Halcom">Flowery, Citrus, Fruity-winy</p>
								</div>
								<h4 class="g_headline" style="font-family: Halcom-Medium">BALANCED</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="color: #000;font-family: Halcom">Honey, Cereals, Roasted</p>
								</div>
								<h4 class="g_headline" style="font-family: Halcom-Medium">INTENSE</h4>
								<div class="g_wysiwyg g_txt_M">
									<p style="color: #000;font-family: Halcom">Cocoa, Spicy, Woody,Intensely roasted</p>
								</div>
							</div>
							<div class="g_text" id="nav004">
								<div class="g_wysiwyg g_txt_M">
									<p style="font-family: Halcom">Each capsule is developed in a given cup size to
										deliver and optimal taste and
										texture.
									</p>
									<p style="font-family: Halcom">A cup size larger than a Nespresso recommended size,
										results in unbalanced
										flavour extraction, as too much water goes through, too few grounds.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="sustainable" data-label="" class="g_section g_dark g_right g_mobileBottom">
			<div class="g_bg g_imgSrc g_imgSrc_loaded"
				 style="background-image: url('/wp-content/uploads/2021/03/sustainability_L.webp');"></div>
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text">
                        <h2 class="g_h2" style="color: #FFF">SUSTAINABLE COMPANY</h2>
						<h3 class="g_h3" style="color: #FFFF;font-family: Halcom">Responsibility means recycling</h3>
						<div class="g_wysiwyg g_txt_M">
							<p style="font-family: Halcom"><strong class="v_brand" term="nespresso">Nespresso</strong>
								capsules are made from
								aluminum to guarantee the freshness of the carefully crafted aromas and rich flavours of
								your coffee. By adding capsule recycling to your coffee ritual, your <strong
										class="v_brand" term="nespresso">Nespresso</strong> won't only taste good, it
								feels good too.
							</p>
						</div>
						<a href="/recycling/" class="g_link"><span>More about recycling</span><i
									class="fn_angleLink"></i></a>
					</div>
				</div>
			</div>
		</section>
		<section id="why" data-label="Nespresso. What else ?" class="g_section g_why g_dark g_center">
			<div class="g_bg g_imgSrc g_imgSrc_loaded"
				 style="background-image: url('/wp-content/uploads/2021/03/why_nespresso_L.webp');"></div>
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text">
						<h2 class="g_h2" style="color:#FFF"><strong class="v_brand" term="nespresso">Nespresso</strong>.
							What else?
						</h2>
						<div class="g_wysiwyg g_txt_L">
							<p>Get the most out of your Nespresso coffee experience knowing we're committed to making a
								positive impact on the environment and on sourcing the highest quality coffee to create
								a wide variety of authentic flavours - all at the touch of a button.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="services" data-label="Services" class="g_section g_services g_dark g_autoHeight"
				 jsonkey="services">
			<!---->
			<div class="g_restrict">
				<div class="g_content">
					<div class="g_text">
						<h2 class="g_visually_hidden" style="color: #FFF"><strong class="v_brand" term="nespresso">Nespresso</strong>
							Services
						</h2>
						<ul>
							<li>
								<div class="g_icon freeDelivery g_imgSrc g_imgSrc_loaded"
									 style="background-image: url('/wp-content/uploads/2021/03/freeDelivery.svg');"></div>
								<h3 class="g_h4" style="color: #FFF;font-family: Halcom">FREE STANDARD DELIVERY</h3>
								<p class="g_txt_M" style="font-family: Halcom-Light">Enjoy FREE STANDARD DELIVERY with a
									minimum of 1,500,000 VND
									spent.
								</p>
							</li>
							<li>
								<div class="g_icon pickup g_imgSrc g_imgSrc_loaded"
									 style="background-image: url('/wp-content/uploads/2021/03/pickup.svg');"></div>
								<h3 class="g_h4" style="color: #FFF;font-family: Halcom">CUSTOMER CARE</h3>
								<p class="g_txt_M" style="font-family: Halcom-Light">Need help with your machine or
									selecting your coffee? Call us any
									time between Monday to Saturday, from 10am to 6pm: 1900 633 474.
								</p>
							</li>
							<li>
								<div class="g_icon assistance g_imgSrc g_imgSrc_loaded"
									 style="background-image: url('/wp-content/uploads/2021/03/assistance.svg');"></div>
								<h3 class="g_h4" style="color: #FFF;font-family: Halcom">SECURED PAYMENT
									TRANSACTIONS
								</h3>
								<p class="g_txt_M" style="font-family: Halcom-Light">SSL encryption means sensitive
									information is always transmitted
									securely.
								</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<div role="alertdialog" class="g_popin  g_popinCollection ispirazione-italiana-modalg_popinService ">
			<div class="g_popinOverlay"></div>
			<div class="g_popinFixed">
				<div class="g_popinRestrict g_light">
					<button class="g_btnRoundS g_btnClose" aria-label="close"><span
								class="">close</span><i
								class="fn_close"></i></button>
					<div class="g_popinContent">
						<div class="g_leftCol">
							<div class="g_table">
								<div class="g_header g_tableRow">
									<div class="g_tableCell">
										<div class="g_popinTitle" slot="title">
											<p class="g_h2">ispirazione italiana</p>
											<div class="g_wysiwyg g_txt_M">
												<p>These intense multi-origin blends provide a powerful and strong
													coffee experience. Enjoy in a 25ml Ristretto or 40ml Espresso
													cup.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="g_main g_tableRow">
									<div class="g_tableCell">
										<div class="g_naturalScrollContainer">
											<div class="g_naturalScroll">
												<div class="g_naturalScrollOverflow">
													<div class="g_naturalScrollPadding">
														<div class="g_productContent">
															<ul class="g_productList nav nav-tabs "
																style="padding-top: 80px;" role="tablist">
																<?php
																	foreach ($products as $k => $pro) {
																		$cou = 1;
																		if ($k == 'Ispirazione Italiana') {
																			foreach ($pro as $val) {
																				
																				?>
																				<li class="g_productItem navlist ">
																					<a href="#<?php echo str_replace(' ', '', $val->name); ?>"
																					   data-toggle="tab"
																					   class="g_productItem <?= $val->sku ?> <?php echo $cou == 1 ? 'active show' : '' ?>">
																						<div class="g_clickableZone"></div>
																						<div class="g_table"
																							 style="color: #000;">
																							<div class="g_cell">
																								<img src="<?= $val->image['0']; ?>"
																									 alt="<?= $val->name ?>">
																							</div>
																							<div class="g_cell">
																								<div>
																									<p class="g_h5"><?= $val->name ?></p>
																									<p class="g_txt_XS">
																										<span><?= $val->property ?></span>
																									</p>
																									<p class="g_txt_XS g_intensity">
																										<span>Intensity</span>
																										<span><?= $val->intensity ?></span>
																									</p>
																								</div>
																							</div>
																							<div class="g_cell">
																								<div class="g_addToCart g_mini <?= $val->sku ?>"
																									 sku="<?= $val->sku ?>">
																									<div data-product-item-id="<?= $val->id ?>"
																										 data-product-section="<?= $val->name ?>"
																										 data-product-position="1"
																										 class="g_priceAndButton track-impression-product">
                                                                                                        <p class="g_productPrice">
                                                                                                            ₫<?= $val->price ?>
                                                                                                        </p>
																										<div class="g_addToCartCustom">
																											<div class="product-add <?= $val->sku ?>"
																												 id="<?= $val->sku ?>">
                                                                                                                <p class="g_productPriceproductadd" style="display: none">
                                                                                                                    ₫<?= $val->price ?>
                                                                                                                </p>
																												<?php
																													if ($val->qty_x1) {
																														?>
																														<?php if ($val->price):
																															$class = "btn-green";
																															$label = "View Details &amp; Buy";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<a href="<?= $val->url ?>"
																														   title=""
																														   class="btn  btn-block <?= $class ?> btn-icon-right">
																															<?= $label ?>
																														</a>
																													<?php } else { ?>
																														<?php if ($val->stock > 0):
																															$class = "btn-green";
																															$label = "Add to basket";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<div class="btn btn-icon btn-icon-right btn-block <?= $class ?>"
																															 data-id="<?= $val->post_id ?>"
																															 data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																															 data-sku="<?= $val->sku ?>"
																															 data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																															 data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																															 data-cart="true"
																															 data-name="<?= $val->name ?>"
																															 data-price="<?= $val->price ?>"
																															 data-image-url="<?= $val->image ? $product->image[0] : '' ?>"
																															 data-type="<?= $val->product_type ?>"
																															 data-vat="1"
																															 data-qty-step="10"
																															 data-url="<?= $val->url ?>"
																															 data-aromatic-profile="<?= $val->aromatic_profile ?>"
																														>
                                                                                                                            <i class="icon-basket"></i>
																															<span class="btn-add-qty"></span><span
																																	class="text"><?= $label ?></span>&nbsp;<i
																																	class="icon-plus text"></i>
																														</div>
																														<div class="mobi-select-cont">
																															<select class="mobi-sec"
																																	style="width: 100%;height: 100%;"
																																	data-id="<?= $val->post_id ?>"
																																	data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																																	data-sku="<?= $val->sku ?>"
																																	data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																																	data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																																	data-cart="true"
																																	data-name="<?= $val->name ?>"
																																	data-price="<?= $val->price ?>"
																																	data-image-url="<?= $val->image ? $val->image[0] : '' ?>"
																																	data-type="<?= $val->product_type ?>"
																																	data-vat="1"
																																	data-qty-step="10"
																																	data-url="<?= $val->url ?>"
																																	data-aromatic-profile="<?= $val->aromatic_profile ?>"
																															>
																																<?php
																																	$c = 0;
																																	$quantity = 500;
																																	$quantity_increment = 10;
																																	if ($val->aromatic_profile == 'Varied') {
																																		$quantity = 100;
																																		$quantity_increment = 1;
																																	}
																																	while ($c <= $quantity) {
																																		if ($c != '0') {
																																			echo "<option value='$c'>$c</option>";
																																		}
																																		if ($c == '100') {
																																			$quantity_increment = 50;
																																		} else if ($c > '150') {
																																			$quantity_increment = 100;
																																		}
																																		
																																		$c += $quantity_increment;
																																	}
																																?>
																															</select>
																														</div>
																													<?php } ?>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>


																						</div>
																					</a>
																				</li>`
																				<?php
																				$cou++;
																			}
																		}
																	}
																
																?>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="g_rightCol">
							<article class="g_table">
								<?php
									foreach ($products as $k => $pro) {
										if ($k == 'Ispirazione Italiana') {
											$count = 1;
											foreach ($pro as $val) {
												?>
                                                    <script>
                                                        jQuery(`a.g_productItem.<?= $val->sku ?>`).click(function (obj) {
                                                            // jQuery('#addtocart2').html('');
                                                            var <?= $val->sku ?> = jQuery(`.product-add.<?= $val->sku ?>`).html();
                                                            jQuery('#addtocart2').html(<?= $val->sku ?>);
                                                            console.log(<?= $val->sku ?>);
                                                        });
                                                        
                                                    </script>
												<div class="g_tableRow g_headerRow tab-content <?php echo $count == 1 ? "active show" : ''; ?>"
													 style="display: none"
													 id='<?php echo str_replace(' ', '', $val->name); ?>'
													 role="tabpanel"
												>
													<header>
														<div class="g_tableCel  ">
															<div class="g_productHeader napoli-2019 g_coffeeHeader tab-pane nav-item nav-link ">
																<button class="g_btnRoundS g_btnBack"><i
																			class="fn_angleLeft"></i>
																</button>
																<div>
																	<div class="g_background g_imgSrc g_imgSrc_loaded"
																		 style="background-image: url('<?php echo $val->coffee_range2_banner_modal ? $val->coffee_range2_banner_modal : '/wp-content/themes/storefront-child/images/coffee-range-2/napoli-2019_L.jpg'; ?>')">
																		<div class="g_product g_ispirazione-italiana">
																			<div class="g_capsule g_imgSrc g_imgSrc_loaded"
																				 style="background-image: url('<?= isset($val->coffee_range2_logo_modal) ? $val->coffee_range2_logo_modal : $val->image['0']; ?>');"></div>
																		</div>
																	</div>
																	<div class="g_title">
																		<h3 class="g_h3"><?= $val->name ?></h3>
																		<p class="g_txt_S">
																			<span><?= $val->property ?></span>
																		</p>
																		<p class="g_txt_S">
																			<span></span> <!----> <span>Intensity</span>
																			<span><?= $val->intensity ?></span>
																		</p>
																	</div>
																</div>
															</div>
														</div>

													</header>
													<?php echo $val->description_coffee_range2_modal; ?>
												</div>
												
												
												<?php
												$count++;
											}
										}
									}
								?>
								<footer class="g_tableRow g_footerRow">
									<div class="g_tableCell">
										<div class="g_buyFooter">
											<div id="addtocart2">

											</div>

										</div>
									</div>
								</footer>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="alertdialog" class="g_popin  g_popinCollection espresso-modalg_popinService ">
			<div class="g_popinOverlay"></div>
			<div class="g_popinFixed">
				<div class="g_popinRestrict g_light">
					<button class="g_btnRoundS g_btnClose" aria-label="close"><span
								class="">close</span><i
								class="fn_close"></i></button>
					<div class="g_popinContent">
						<div class="g_leftCol">
							<div class="g_table">
								<div class="g_header g_tableRow">
									<div class="g_tableCell">
										<div class="g_popinTitle" slot="title"><p class="g_h2">
												Espresso</p>
											<div class="g_wysiwyg g_txt_M"><p>Explore these smooth
													blends that are satisfying and surprisingly
													balanced. It's like meeting with an old friend - but
													in the size of an Espresso.</p></div>
										</div>
									</div>
								</div>
								<div class="g_main g_tableRow">
									<div class="g_tableCell">
										<div class="g_naturalScrollContainer">
											<div class="g_naturalScroll">
												<div class="g_naturalScrollOverflow">
													<div class="g_naturalScrollPadding">
														<div class="g_productContent">
															<ul class="g_productList nav nav-tabs "
																style="padding-top: 80px;"
																role="tablist">
																<?php
																	foreach ($products as $k => $pro) {
																		$cou = 1;
																		if ($k == 'Espresso') {
																			foreach ($pro as $val) {
																				
																				?>
																				<li class="g_productItem navlist ">
																					<a href="#<?php echo str_replace(' ', '', $val->name); ?>"
																					   data-toggle="tab"
																					   class="g_productItem <?= $val->sku ?> <?php echo $cou == 1 ? 'active show' : '' ?>">
																						<div class="g_clickableZone"></div>
																						<div class="g_table"
																							 style="color: #000;">
																							<div class="g_cell">
																								<img src="<?= $val->image['0']; ?>"
																									 alt="<?= $val->name ?>">
																							</div>
																							<div class="g_cell">
																								<div>
																									<p class="g_h5"><?= $val->name ?></p>
																									<p class="g_txt_XS">
																										<span><?= $val->property ?></span>
																									</p>
																									<p class="g_txt_XS g_intensity">
																										<span>Intensity</span>
																										<span><?= $val->intensity ?></span>
																									</p>
																								</div>
																							</div>
																							<div class="g_cell">
																								<div class="g_addToCart g_mini <?= $val->sku ?>"
																									 sku="<?= $val->sku ?>">
																									<div data-product-item-id="<?= $val->id ?>"
																										 data-product-section="<?= $val->name ?>"
																										 data-product-position="1"
																										 class="g_priceAndButton track-impression-product">
                                                                                                        <p class="g_productPrice">
                                                                                                            ₫<?= $val->price ?>
                                                                                                        </p>
																										<div class="g_addToCartCustom">
																											<div class="product-add <?= $val->sku ?>"
																												 id="<?= $val->sku ?>">
                                                                                                                <p class="g_productPriceproductadd" style="display: none">
                                                                                                                    ₫<?= $val->price ?>
                                                                                                                </p>
																												<?php
																													if ($val->qty_x1) {
																														?>
																														<?php if ($val->price):
																															$class = "btn-green";
																															$label = "View Details &amp; Buy";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<a href="<?= $val->url ?>"
																														   title=""
																														   class="btn  btn-block <?= $class ?> btn-icon-right">
																															<?= $label ?>
																														</a>
																													<?php } else { ?>
																														<?php if ($val->stock > 0):
																															$class = "btn-green";
																															$label = "Add to basket";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<div class="btn btn-icon btn-icon-right btn-block <?= $class ?>"
																															 data-id="<?= $val->post_id ?>"
																															 data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																															 data-sku="<?= $val->sku ?>"
																															 data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																															 data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																															 data-cart="true"
																															 data-name="<?= $val->name ?>"
																															 data-price="<?= $val->price ?>"
																															 data-image-url="<?= $val->image ? $product->image[0] : '' ?>"
																															 data-type="<?= $val->product_type ?>"
																															 data-vat="1"
																															 data-qty-step="10"
																															 data-url="<?= $val->url ?>"
																															 data-aromatic-profile="<?= $val->aromatic_profile ?>"
																														>
                                                                                                                            <i class="icon-basket"></i>
																															<span
																																	class="btn-add-qty"></span><span
																																	class="text"><?= $label ?></span>&nbsp;<i
																																	class="icon-plus text"></i>
																														</div>
																														<div class="mobi-select-cont">
																															<select class="mobi-sec"
																																	style="width: 100%;height: 100%;"
																																	data-id="<?= $val->post_id ?>"
																																	data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																																	data-sku="<?= $val->sku ?>"
																																	data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																																	data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																																	data-cart="true"
																																	data-name="<?= $val->name ?>"
																																	data-price="<?= $val->price ?>"
																																	data-image-url="<?= $val->image ? $val->image[0] : '' ?>"
																																	data-type="<?= $val->product_type ?>"
																																	data-vat="1"
																																	data-qty-step="10"
																																	data-url="<?= $val->url ?>"
																																	data-aromatic-profile="<?= $val->aromatic_profile ?>"
																															>
																																<?php
																																	$c = 0;
																																	$quantity = 500;
																																	$quantity_increment = 10;
																																	if ($val->aromatic_profile == 'Varied') {
																																		$quantity = 100;
																																		$quantity_increment = 1;
																																	}
																																	while ($c <= $quantity) {
																																		if ($c != '0') {
																																			echo "<option value='$c'>$c</option>";
																																		}
																																		if ($c == '100') {
																																			$quantity_increment = 50;
																																		} else if ($c > '150') {
																																			$quantity_increment = 100;
																																		}
																																		
																																		$c += $quantity_increment;
																																	}
																																?>
																															</select>
																														</div>
																													<?php } ?>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>


																						</div>
																					</a>
																				</li>
																				<?php
																				$cou++;
																			}
																		}
																	}
																
																?>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="g_rightCol">
							<article class="g_table">
								<?php
									foreach ($products as $k => $pro) {
										if ($k == 'Espresso') {
											$count = 1;
											foreach ($pro as $val) {
												?>
                                                <script>
                                                    jQuery(`a.g_productItem.<?= $val->sku ?>`).click(function (obj) {
                                                        // jQuery('#addtocart3').html('');
                                                        var <?= $val->sku ?> = jQuery(`.product-add.<?= $val->sku ?>`).html();
                                                        jQuery('#addtocart3').html(<?= $val->sku ?>);
                                                        console.log(<?= $val->sku ?>);
                                                    });

                                                </script>
												<div class="g_tableRow g_headerRow tab-content <?php echo $count == 1 ? "active show" : ''; ?>"
													 style="display: none"
													 id='<?php echo str_replace(' ', '', $val->name); ?>'
													 role="tabpanel"
												>
													<header>
														<div class="g_tableCel  ">
															<div class="g_productHeader napoli-2019 g_coffeeHeader tab-pane nav-item nav-link ">
																<button class="g_btnRoundS g_btnBack"><i
																			class="fn_angleLeft"></i>
																</button>
																<div>
																	<div class="g_background g_imgSrc g_imgSrc_loaded"
																		 style="background-image: url('<?php echo $val->coffee_range2_banner_modal ? $val->coffee_range2_banner_modal : '/wp-content/themes/storefront-child/images/coffee-range-2/napoli-2019_L.jpg'; ?>')">
																		<div class="g_product g_ispirazione-italiana">
																			<div class="g_capsule g_imgSrc g_imgSrc_loaded"
																				 style="background-image: url('<?= isset($val->coffee_range2_logo_modal) ? $val->coffee_range2_logo_modal : $val->image['0']; ?>');"></div>
																		</div>
																	</div>
																	<div class="g_title">
																		<h3 class="g_h3"><?= $val->name ?></h3>
																		<p class="g_txt_S">
																			<span><?= $val->property ?></span>
																		</p>
																		<p class="g_txt_S">
																			<span></span> <!----> <span>Intensity</span>
																			<span><?= $val->intensity ?></span>
																		</p>
																	</div>
																</div>
															</div>
														</div>

													</header>
													<?php echo $val->description_coffee_range2_modal; ?>
												</div>
												<?php
												$count++;
											}
										}
									}
								
								?>


								<footer class="g_tableRow g_footerRow">
									<div class="g_tableCell">
										<div class="g_buyFooter">
											<div id="addtocart3">

											</div>

										</div>
									</div>
								</footer>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="alertdialog" class="g_popin  g_popinCollection master-origins-modalg_popinService ">
			<div class="g_popinOverlay"></div>
			<div class="g_popinFixed">
				<div class="g_popinRestrict g_light">
					<button class="g_btnRoundS g_btnClose" aria-label="close"><span
								class="">close</span><i
								class="fn_close"></i></button>
					<div class="g_popinContent">
						<div class="g_leftCol">
							<div class="g_table">
								<div class="g_header g_tableRow">
									<div class="g_tableCell">
										<div class="g_popinTitle" slot="title"><p class="g_h2">MASTER
												ORIGINS</p>
											<div class="g_wysiwyg g_txt_M"><p>A range of coffees
													mastered by craftsmen and inspired by their land,
													associating one specific process with each of them
													for distinctive and memorable coffee
													experiences.</p></div>
										</div>
									</div>
								</div>
								<div class="g_main g_tableRow">
									<div class="g_tableCell">
										<div class="g_naturalScrollContainer">
											<div class="g_naturalScroll">
												<div class="g_naturalScrollOverflow">
													<div class="g_naturalScrollPadding">
														<div class="g_productContent">
															<ul class="g_productList nav nav-tabs "
																style="padding-top: 80px;"
																role="tablist">
																<?php
																	foreach ($products as $k => $pro) {
																		$cou = 1;
//																		if ($k == 'World Explorations') {
																		if ($k == 'Master Origins') {
																			foreach ($pro as $val) {
																				
																				?>
																				<li class="g_productItem navlist ">
																					<a href="#<?php echo str_replace(' ', '', $val->name); ?>"
																					   data-toggle="tab"
																					   class="g_productItem <?= $val->sku ?> <?php echo $cou == 1 ? 'active show' : '' ?>">
																						<div class="g_clickableZone"></div>
																						<div class="g_table"
																							 style="color: #000;">
																							<div class="g_cell">
																								<img src="<?= $val->image['0']; ?>"
																									 alt="<?= $val->name ?>">
																							</div>
																							<div class="g_cell">
																								<div>
																									<p class="g_h5"><?= $val->name ?></p>
																									<p class="g_txt_XS">
																										<span><?= $val->property ?></span>
																									</p>
																									<p class="g_txt_XS g_intensity">
																										<span>Intensity</span>
																										<span><?= $val->intensity ?></span>
																									</p>
																								</div>
																							</div>
																							<div class="g_cell">
																								<div class="g_addToCart g_mini <?= $val->sku ?>"
																									 sku="<?= $val->sku ?>">
																									<div data-product-item-id="<?= $val->id ?>"
																										 data-product-section="<?= $val->name ?>"
																										 data-product-position="1"
																										 class="g_priceAndButton track-impression-product">
                                                                                                        <p class="g_productPrice">
                                                                                                            ₫<?= $val->price ?>
                                                                                                        </p>
																										<div class="g_addToCartCustom">
																											<div class="product-add <?= $val->sku ?>"
																												 id="<?= $val->sku ?>">
                                                                                                                <p class="g_productPriceproductadd" style="display: none">
                                                                                                                    ₫<?= $val->price ?>
                                                                                                                </p>
																												<?php
																													if ($val->qty_x1) {
																														?>
																														<?php if ($val->price):
																															$class = "btn-green";
																															$label = "View Details &amp; Buy";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<a href="<?= $val->url ?>"
																														   title=""
																														   class="btn  btn-block <?= $class ?> btn-icon-right">
																															<?= $label ?>
																														</a>
																													<?php } else { ?>
																														<?php if ($val->stock > 0):
																															$class = "btn-green";
																															$label = "Add to basket";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<div class="btn btn-icon btn-icon-right btn-block <?= $class ?>"
																															 data-id="<?= $val->post_id ?>"
																															 data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																															 data-sku="<?= $val->sku ?>"
																															 data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																															 data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																															 data-cart="true"
																															 data-name="<?= $val->name ?>"
																															 data-price="<?= $val->price ?>"
																															 data-image-url="<?= $val->image ? $product->image[0] : '' ?>"
																															 data-type="<?= $val->product_type ?>"
																															 data-vat="1"
																															 data-qty-step="10"
																															 data-url="<?= $val->url ?>"
																															 data-aromatic-profile="<?= $val->aromatic_profile ?>"
																														>
                                                                                                                            <i class="icon-basket"></i>
																															<span
																																	class="btn-add-qty"></span><span
																																	class="text"><?= $label ?></span>&nbsp;<i
																																	class="icon-plus text"></i>
																														</div>
																														<div class="mobi-select-cont">
																															<select class="mobi-sec"
																																	style="width: 100%;height: 100%;"
																																	data-id="<?= $val->post_id ?>"
																																	data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																																	data-sku="<?= $val->sku ?>"
																																	data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																																	data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																																	data-cart="true"
																																	data-name="<?= $val->name ?>"
																																	data-price="<?= $val->price ?>"
																																	data-image-url="<?= $val->image ? $val->image[0] : '' ?>"
																																	data-type="<?= $val->product_type ?>"
																																	data-vat="1"
																																	data-qty-step="10"
																																	data-url="<?= $val->url ?>"
																																	data-aromatic-profile="<?= $val->aromatic_profile ?>"
																															>
																																<?php
																																	$c = 0;
																																	$quantity = 500;
																																	$quantity_increment = 10;
																																	if ($val->aromatic_profile == 'Varied') {
																																		$quantity = 100;
																																		$quantity_increment = 1;
																																	}
																																	while ($c <= $quantity) {
																																		if ($c != '0') {
																																			echo "<option value='$c'>$c</option>";
																																		}
																																		if ($c == '100') {
																																			$quantity_increment = 50;
																																		} else if ($c > '150') {
																																			$quantity_increment = 100;
																																		}
																																		
																																		$c += $quantity_increment;
																																	}
																																?>
																															</select>
																														</div>
																													<?php } ?>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>


																						</div>
																					</a>
																				</li>
																				<?php
																				$cou++;
																			}
																		}
																	}
																
																?>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="g_rightCol">
							<article class="g_table">
								<?php
									foreach ($products as $k => $pro) {
//										if ($k == 'World Explorations') {
										if ($k == 'Master Origins') {
											$count = 1;
											foreach ($pro as $val) {
												?>
                                                <script>
                                                    jQuery(`a.g_productItem.<?= $val->sku ?>`).click(function (obj) {
                                                        // jQuery('#addtocart4').html('');
                                                        var <?= $val->sku ?> = jQuery(`.product-add.<?= $val->sku ?>`).html();
                                                        jQuery('#addtocart4').html(<?= $val->sku ?>);
                                                        console.log(<?= $val->sku ?>);
                                                    });

                                                </script>
												<div class="g_tableRow g_headerRow tab-content <?php echo $count == 1 ? "active show" : ''; ?>"
													 style="display: none"
													 id='<?php echo str_replace(' ', '', $val->name); ?>'
													 role="tabpanel"
												>
													<header>
														<div class="g_tableCel  ">
															<div class="g_productHeader napoli-2019 g_coffeeHeader tab-pane nav-item nav-link ">
																<button class="g_btnRoundS g_btnBack"><i
																			class="fn_angleLeft"></i>
																</button>
																<div>
																	<div class="g_background g_imgSrc g_imgSrc_loaded"
																		 style="background-image: url('<?php echo $val->coffee_range2_banner_modal ? $val->coffee_range2_banner_modal : '/wp-content/themes/storefront-child/images/coffee-range-2/napoli-2019_L.jpg'; ?>')">
																		<div class="g_product g_ispirazione-italiana">
																			<div class="g_capsule g_imgSrc g_imgSrc_loaded"
																				 style="background-image: url('<?= isset($val->coffee_range2_logo_modal) ? $val->coffee_range2_logo_modal : $val->image['0']; ?>');"></div>
																		</div>
																	</div>
																	<div class="g_title">
																		<h3 class="g_h3"><?= $val->name ?></h3>
																		<p class="g_txt_S">
																			<span><?= $val->property ?></span>
																		</p>
																		<p class="g_txt_S">
																			<span></span> <!----> <span>Intensity</span>
																			<span><?= $val->intensity ?></span>
																		</p>
																	</div>
																</div>
															</div>
														</div>

													</header>
													<?php echo $val->description_coffee_range2_modal; ?>
												</div>
												<?php
												$count++;
											}
										}
									}
								
								?>

								<footer class="g_tableRow g_footerRow">
									<div class="g_tableCell">
										<div class="g_buyFooter">
											<div id="addtocart4">

											</div>

										</div>
									</div>
								</footer>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="alertdialog" class="g_popin  g_popinCollection worldexplorations-modalg_popinService ">
			<div class="g_popinOverlay"></div>
			<div class="g_popinFixed">
				<div class="g_popinRestrict g_light">
					<button class="g_btnRoundS g_btnClose" aria-label="close"><span
								class="">close</span><i
								class="fn_close"></i></button>
					<div class="g_popinContent">
						<div class="g_leftCol">
							<div class="g_table">
								<div class="g_header g_tableRow">
									<div class="g_tableCell">
										<div class="g_popinTitle" slot="title"><p class="g_h2">World
												Explorations</p>
											<div class="g_wysiwyg g_txt_M"><p>World Explorations is an
													invitation to experience the different ways coffee
													is appreciated around the globe and explore the
													uniqueness of both traditional and upcoming coffee
													cultures through your Nespresso cup.</p></div>
										</div>
									</div>
								</div>
								<div class="g_main g_tableRow">
									<div class="g_tableCell">
										<div class="g_naturalScrollContainer">
											<div class="g_naturalScroll">
												<div class="g_naturalScrollOverflow">
													<div class="g_naturalScrollPadding">
														<div class="g_productContent">
															<ul class="g_productList nav nav-tabs "
																style="padding-top: 80px;"
																role="tablist">
																<?php
																	foreach ($products as $k => $pro) {
																		$cou = 1;
//																		if ($k == 'Master Origins') {
																		if ($k == 'World Explorations') {
																			foreach (array_slice($pro, 0, 6) as $val) {
																				
																				?>
																				<li class="g_productItem navlist ">
																					<a href="#<?php echo str_replace(' ', '', $val->name); ?>"
																					   data-toggle="tab"
																					   class="g_productItem <?= $val->sku ?> <?php echo $cou == 1 ? 'active show' : '' ?>">
																						<div class="g_clickableZone"></div>
																						<div class="g_table"
																							 style="color: #000;">
																							<div class="g_cell">
																								<img src="<?= $val->image['0']; ?>"
																									 alt="<?= $val->name ?>">
																							</div>
																							<div class="g_cell">
																								<div>
																									<p class="g_h5"><?= $val->name ?></p>
																									<p class="g_txt_XS">
																										<span><?= $val->property ?></span>
																									</p>
																									<p class="g_txt_XS g_intensity">
																										<span>Intensity</span>
																										<span><?= $val->intensity ?></span>
																									</p>
																								</div>
																							</div>
																							<div class="g_cell">
																								<div class="g_addToCart g_mini <?= $val->sku ?>"
																									 sku="<?= $val->sku ?>">
																									<div data-product-item-id="<?= $val->id ?>"
																										 data-product-section="<?= $val->name ?>"
																										 data-product-position="1"
																										 class="g_priceAndButton track-impression-product">
																										<p class="g_productPrice">
																											₫<?= $val->price ?>
																										</p>
																										<div class="g_addToCartCustom">
																											<div class="product-add <?= $val->sku ?>"
																												 id="<?= $val->sku ?>">
                                                                                                                <p class="g_productPriceproductadd" style="display: none">
                                                                                                                    ₫<?= $val->price ?>
                                                                                                                </p>
																												<?php
																													if ($val->qty_x1) {
																														?>
																														<?php if ($val->price):
																															$class = "btn-green";
																															$label = "View Details &amp; Buy";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<a href="<?= $val->url ?>"
																														   title=""
																														   class="btn  btn-block <?= $class ?> btn-icon-right">
																															<?= $label ?>
																														</a>
																													<?php } else { ?>
																														<?php if ($val->stock > 0):
																															$class = "btn-green";
																															$label = "Add to basket";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<div class="btn btn-icon btn-icon-right btn-block <?= $class ?>"
																															 data-id="<?= $val->post_id ?>"
																															 data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																															 data-sku="<?= $val->sku ?>"
																															 data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																															 data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																															 data-cart="true"
																															 data-name="<?= $val->name ?>"
																															 data-price="<?= $val->price ?>"
																															 data-image-url="<?= $val->image ? $product->image[0] : '' ?>"
																															 data-type="<?= $val->product_type ?>"
																															 data-vat="1"
																															 data-qty-step="10"
																															 data-url="<?= $val->url ?>"
																															 data-aromatic-profile="<?= $val->aromatic_profile ?>"
																														>
                                                                                                                            <i class="icon-basket"></i>
																															<span class="btn-add-qty"></span>
                                                                                                                            <span class="text"><?= $label ?></span>&nbsp;
                                                                                                                            <i class="icon-plus text"></i>
                                                                                                                            
																														</div>
																														<div class="mobi-select-cont">
																															<select class="mobi-sec"
																																	style="width: 100%;height: 100%;"
																																	data-id="<?= $val->post_id ?>"
																																	data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																																	data-sku="<?= $val->sku ?>"
																																	data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																																	data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																																	data-cart="true"
																																	data-name="<?= $val->name ?>"
																																	data-price="<?= $val->price ?>"
																																	data-image-url="<?= $val->image ? $val->image[0] : '' ?>"
																																	data-type="<?= $val->product_type ?>"
																																	data-vat="1"
																																	data-qty-step="10"
																																	data-url="<?= $val->url ?>"
																																	data-aromatic-profile="<?= $val->aromatic_profile ?>"
																															>
																																<?php
																																	$c = 0;
																																	$quantity = 500;
																																	$quantity_increment = 10;
																																	if ($val->aromatic_profile == 'Varied') {
																																		$quantity = 100;
																																		$quantity_increment = 1;
																																	}
																																	while ($c <= $quantity) {
																																		if ($c != '0') {
																																			echo "<option value='$c'>$c</option>";
																																		}
																																		if ($c == '100') {
																																			$quantity_increment = 50;
																																		} else if ($c > '150') {
																																			$quantity_increment = 100;
																																		}
																																		
																																		$c += $quantity_increment;
																																	}
																																?>
																															</select>
																														</div>
																													<?php } ?>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>


																						</div>
																					</a>
																				</li>
																				<?php
																				$cou++;
																			}
																		}
																	}
																
																?>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="g_rightCol">
							<article class="g_table">
								<?php
         
									foreach ($products as $k => $pro) {
//										if ($k == 'Master Origins') {
										if ($k == 'World Explorations') {
											$count = 1;
											foreach (array_slice($pro, 0, 6) as $val) {
												?>
                                                <script>
                                                    jQuery(`a.g_productItem.<?= $val->sku ?>`).click(function (obj) {
                                                        // jQuery('#addtocart5').html('');
                                                        //$label = "Add to basket";g_productPriceproductadd<?= $val->sku ?>
	                                                   var  <?= $val->sku ?> = jQuery(`.product-add.<?= $val->sku ?>`).html();
                                                        jQuery('#addtocart5').html(<?= $val->sku ?>);
                                                        console.log(<?= $val->sku ?>);
                                                    });

                                                </script>
												<div class="g_tableRow g_headerRow tab-content <?php echo $count == 1 ? "active show" : ''; ?>"
													 style="display: none"
													 id='<?php echo str_replace(' ', '', $val->name); ?>'
													 role="tabpanel"
												>
													<header>
														<div class="g_tableCel  ">
															<div class="g_productHeader napoli-2019 g_coffeeHeader tab-pane nav-item nav-link ">
																<button class="g_btnRoundS g_btnBack"><i
																			class="fn_angleLeft"></i>
																</button>
																<div>
																	<div class="g_background g_imgSrc g_imgSrc_loaded"
																		 style="background-image: url('<?php echo $val->coffee_range2_banner_modal ? $val->coffee_range2_banner_modal : '/wp-content/themes/storefront-child/images/coffee-range-2/napoli-2019_L.jpg'; ?>')">
																		<div class="g_product g_ispirazione-italiana">
																			<div class="g_capsule g_imgSrc g_imgSrc_loaded"
																				 style="background-image: url('<?= isset($val->coffee_range2_logo_modal) ? $val->coffee_range2_logo_modal : $val->image['0']; ?>');"></div>
																		</div>
																	</div>
																	<div class="g_title">
																		<h3 class="g_h3"><?= $val->name ?></h3>
																		<p class="g_txt_S">
																			<span><?= $val->property ?></span>
																		</p>
																		<p class="g_txt_S">
																			<span></span> <!----> <span>Intensity</span>
																			<span><?= $val->intensity ?></span>
																		</p>
																	</div>
																</div>
															</div>
														</div>

													</header>
													<?php echo $val->description_coffee_range2_modal; ?>
												</div>
												<?php
												$count++;
											}
										}
										
									}
								
								?>

								<footer class="g_tableRow g_footerRow">
									<div class="g_tableCell">
										<div class="g_buyFooter">
											<div id="addtocart5">

											</div>

										</div>
									</div>
								</footer>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="alertdialog" class="g_popin  g_popinCollection barista-creations-modalg_popinService ">
			<div class="g_popinOverlay"></div>
			<div class="g_popinFixed">
				<div class="g_popinRestrict g_light">
					<button class="g_btnRoundS g_btnClose" aria-label="close"><span
								class="">close</span><i
								class="fn_close"></i></button>
					<div class="g_popinContent">
						<div class="g_leftCol">
							<div class="g_table">
								<div class="g_header g_tableRow">
									<div class="g_tableCell">
										<div class="g_popinTitle" slot="title"><p class="g_h2">Barista
												Creations</p>
											<div class="g_wysiwyg g_txt_M"><p>Inspired by the creativity
													and expertise of world’s finest baristas, these
													coffees are designed to make it easy for you to
													recreate the full spread of coffee recipes at
													home.</p></div>
										</div>
									</div>
								</div>
								<div class="g_main g_tableRow">
									<div class="g_tableCell">
										<div class="g_naturalScrollContainer">
											<div class="g_naturalScroll">
												<div class="g_naturalScrollOverflow">
													<div class="g_naturalScrollPadding">
														<div class="g_productContent">
															<ul class="g_productList nav nav-tabs "
																style="padding-top: 80px;"
																role="tablist">
																<?php
																	foreach ($products as $k => $pro) {
																		$cou = 1;
																		if ($k == 'Barista Creations') {
																			foreach ($pro as $val) {
																				
																				?>
																				<li class="g_productItem navlist ">
																					<a href="#<?php echo str_replace(' ', '', $val->name); ?>"
																					   data-toggle="tab"
																					   class="g_productItem <?= $val->sku ?> <?php echo $cou == 1 ? 'active show' : '' ?>">
																						<div class="g_clickableZone"></div>
																						<div class="g_table"
																							 style="color: #000;">
																							<div class="g_cell">
																								<img src="<?= $val->image['0']; ?>"
																									 alt="<?= $val->name ?>">
																							</div>
																							<div class="g_cell">
																								<div>
																									<p class="g_h5"><?= $val->name ?></p>
																									<p class="g_txt_XS">
																										<span><?= $val->property ?></span>
																									</p>
																									<p class="g_txt_XS g_intensity">
																										<span>Intensity</span>
																										<span><?= $val->intensity ?></span>
																									</p>
																								</div>
																							</div>
																							<div class="g_cell">
																								<div class="g_addToCart g_mini <?= $val->sku ?>"
																									 sku="<?= $val->sku ?>">
																									<div data-product-item-id="<?= $val->id ?>"
																										 data-product-section="<?= $val->name ?>"
																										 data-product-position="1"
																										 class="g_priceAndButton track-impression-product">
                                                                                                        <p class="g_productPrice">
                                                                                                            ₫<?= $val->price ?>
                                                                                                        </p>
																										<div class="g_addToCartCustom">
																											<div class="product-add "
																												 id="<?= $val->sku ?>">
                                                                                                                <p class="g_productPriceproductadd" style="display: none">
                                                                                                                    ₫<?= $val->price ?>
                                                                                                                </p>
																												<?php
																													if ($val->qty_x1) {
																														?>
																														<?php if ($val->price):
																															$class = "btn-green";
																															$label = "View Details &amp; Buy";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<a href="<?= $val->url ?>"
																														   title=""
																														   class="btn  btn-block <?= $class ?> btn-icon-right">
																															<?= $label ?>
																														</a>
																													<?php } else { ?>
																														<?php if ($val->stock > 0):
																															$class = "btn-green";
																															$label = "Add to basket";
																														else:
																															$class = "btn-disabled";
																															$label = "Out of stock";
																														endif; ?>
																														<div class="btn btn-icon btn-icon-right btn-block <?= $class ?>"
																															 data-id="<?= $val->post_id ?>"
																															 data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																															 data-sku="<?= $val->sku ?>"
																															 data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																															 data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																															 data-cart="true"
																															 data-name="<?= $val->name ?>"
																															 data-price="<?= $val->price ?>"
																															 data-image-url="<?= $val->image ? $product->image[0] : '' ?>"
																															 data-type="<?= $val->product_type ?>"
																															 data-vat="1"
																															 data-qty-step="10"
																															 data-url="<?= $val->url ?>"
																															 data-aromatic-profile="<?= $val->aromatic_profile ?>"
																														>
                                                                                                                            <i class="icon-basket"></i>
																															<span
																																	class="btn-add-qty"></span><span
																																	class="text"><?= $label ?></span>&nbsp;<i
																																	class="icon-plus text"></i>
																														</div>
																														<div class="mobi-select-cont">
																															<select class="mobi-sec"
																																	style="width: 100%;height: 100%;"
																																	data-id="<?= $val->post_id ?>"
																																	data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $val->post_id ?>"
																																	data-sku="<?= $val->sku ?>"
																																	data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
																																	data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
																																	data-cart="true"
																																	data-name="<?= $val->name ?>"
																																	data-price="<?= $val->price ?>"
																																	data-image-url="<?= $val->image ? $val->image[0] : '' ?>"
																																	data-type="<?= $val->product_type ?>"
																																	data-vat="1"
																																	data-qty-step="10"
																																	data-url="<?= $val->url ?>"
																																	data-aromatic-profile="<?= $val->aromatic_profile ?>"
																															>
																																<?php
																																	$c = 0;
																																	$quantity = 500;
																																	$quantity_increment = 10;
																																	if ($val->aromatic_profile == 'Varied') {
																																		$quantity = 100;
																																		$quantity_increment = 1;
																																	}
																																	while ($c <= $quantity) {
																																		if ($c != '0') {
																																			echo "<option value='$c'>$c</option>";
																																		}
																																		if ($c == '100') {
																																			$quantity_increment = 50;
																																		} else if ($c > '150') {
																																			$quantity_increment = 100;
																																		}
																																		
																																		$c += $quantity_increment;
																																	}
																																?>
																															</select>
																														</div>
																													<?php } ?>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>


																						</div>
																					</a>
																				</li>
																				<?php
																				$cou++;
																			}
																		}
																	}
																
																?>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="g_rightCol">
							<article class="g_table">
								<?php
									foreach ($products as $k => $pro) {
										if ($k == 'Barista Creations') {
											$count = 1;
											foreach ($pro as $val) {
												?>
                                                <script>
                                                    jQuery(`a.g_productItem.<?= $val->sku ?>`).click(function (obj) {
                                                        var <?= $val->sku ?> = jQuery(`.product-add.<?= $val->sku ?>`).html();
                                                        jQuery('#addtocart6').html(<?= $val->sku ?>);
                                                        console.log(<?= $val->sku ?>);
                                                    });

                                                </script>
												<div class="g_tableRow g_headerRow tab-content <?php echo $count == 1 ? "active show" : ''; ?>"
													 style="display: none"
													 id='<?php echo str_replace(' ', '', $val->name); ?>'
													 role="tabpanel"
												>
													<header>
														<div class="g_tableCel  ">
															<div class="g_productHeader napoli-2019 g_coffeeHeader tab-pane nav-item nav-link ">
																<button class="g_btnRoundS g_btnBack"><i
																			class="fn_angleLeft"></i>
																</button>
																<div>
																	<div class="g_background g_imgSrc g_imgSrc_loaded"
																		 style="background-image: url('<?php echo $val->coffee_range2_banner_modal ? $val->coffee_range2_banner_modal : '/wp-content/themes/storefront-child/images/coffee-range-2/napoli-2019_L.jpg'; ?>')">
																		<div class="g_product g_ispirazione-italiana">
																			<div class="g_capsule g_imgSrc g_imgSrc_loaded"
																				 style="background-image: url('<?= isset($val->coffee_range2_logo_modal) ? $val->coffee_range2_logo_modal : $val->image['0']; ?>');"></div>
																		</div>
																	</div>
																	<div class="g_title">
																		<h3 class="g_h3"><?= $val->name ?></h3>
																		<p class="g_txt_S">
																			<span><?= $val->property ?></span>
																		</p>
																		<p class="g_txt_S">
																			<span></span> <!----> <span>Intensity</span>
																			<span><?= $val->intensity ?></span>
																		</p>
																	</div>
																</div>
															</div>
														</div>

													</header>
													<?php echo $val->description_coffee_range2_modal; ?>
												</div>
												<?php
												$count++;
											}
										}
									}
								
								?>
								<footer class="g_tableRow g_footerRow">
									<div class="g_tableCell">
										<div class="g_buyFooter">
											<div id="addtocart6">

											</div>

										</div>
									</div>
								</footer>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>
<style>
	#qty-selector-content {
		margin-left: 80px;
	}
</style>
<script>
 
    
	function removeclassmodal(idobject) {
		jQuery(`${idobject}g_popinService`).removeClass('g_popinOpened');
	}

	function animationcard(idobject, i) {
		jQuery(`${idobject}`).click(function () {

            var object = jQuery(`.g_popinContent`);
            jQuery(`${idobject}g_popinService`).addClass('g_popinOpened');
            jQuery(`${idobject}g_popinContent`).addClass('g_popinOpened');
            
            
		});
  

		jQuery(`.g_btnClose`).click(function () {
			jQuery(`${idobject}g_popinService`).removeClass('g_popinOpened');
		});
		
		jQuery(`.fn_angleLeft`).click(function () {
			jQuery(`${idobject}g_popinService`).removeClass('g_popinSlide');
		});

		jQuery(`.g_popinContent`).click(function (obj) {
            var object = jQuery(`.g_popinContent`);
			if (object.is(event.target) && object.has(event.target).length) {
				jQuery(`${idobject}g_popinService`).removeClass('g_popinOpened');
                console.log('da xoa');
				
			} else {
                // jQuery(`${idobject}g_popinService`).addClass('g_popinOpened');
                // console.log('da them');
			}

		});

        jQuery(`a.g_productItem`).click(function (obj) {
            jQuery(`li.g_productItem.g_opened`).removeClass('g_opened');
            jQuery(`${idobject}g_popinService`).addClass('g_popinSlide');
            jQuery(this).parent().addClass('g_opened');
        });
        
		

	}


	let arr = ['.ispirazione-italiana-modal', '.espresso-modal', '.worldexplorations-modal', '.master-origins-modal', '.barista-creations-modal'];

	for (let i = 0; i < arr.length; i++) {
		this.animationcard(arr[i], i);
	}


</script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/coffee-range/manifest.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/coffee-range/vendor.js"></script>
<script type="text/javascript"
		src="<?php echo get_stylesheet_directory_uri(); ?>/js/coffee-range/OriginalCoffeeHub.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
<style>
    .g_rightCol .g_productPriceproductadd {
        display: flex!important;
        font-size: .875em;
        font-weight: 600;
        line-height: 1.5em;
        letter-spacing: .07143em;
        color: #3d8705;
        margin: -2rem -6rem;
        padding-bottom: .5em;
    }
    .product-add i.icon-basket {
        display: none;
    }
	.black {
		color: #000 !important;
		font-weight: bold !important;
	}

    div#addtocart2, div#addtocart3, div#addtocart4, div#addtocart5, div#addtocart6 {
        width: 60%;
        float: right;
		margin: 20px;
    }

	.g .g_innerScrollOverflow, .g .g_innerScrollPadding {
        /*overflow-x: hidden;*/
        /*width: 400px;*/
        /*height: 200px;*/
        /*overflow: overlay;*/
        width: 400px;
        min-height: 100px;
        overflow: overlay;
	}
    html {
        overflow-y: hidden;
    }
    .btn.btn-icon.btn-icon-right {
        padding: 0px 15px;
    }
    .g_leftCol span.text {
        display: none;
    }
    .g_leftCol #qty-selector-content {
        margin: 0px 30px;
    }
</style>