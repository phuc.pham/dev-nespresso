<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
?>

<!-- content -->
<main id="content"  class="shopping-bag">
    <div class="container">
        <?php $stepActive = 2; ?>

        <?php get_template_part('components/wizard'); ?>

        <div class="checkister step2" id="JumpContent" tabindex="-1">
            <header>
                <h2>Delivery Mode</h2>
            </header>
            <section class="row">
                <div class="hidden-xs hidden-sm col-md-4 pull-right">
                    <aside>
                        <span class="col-title">Order details</span>
                        <div class="sbag-small sbag-loading"></div>
                        <ul class="bloc-total hidden">
                            <li class="bloc-total-stotal">
                                <span class="stotal">Subtotal</span>
                                <span class="price">$<span class="price-int">0</span></span>
                            </li>
                            <li class="bloc-total-vat">
                                <span class="vat">Vat (inCL.)</span>
                                <span class="price">$<span class="price-int">0</span></span>
                            </li>
                            <li class="bloc-total-total">
                                <span class="total">Total</span>
                                <span class="tprice">$<span class="price-int">0</span></span>
                            </li>
                        </ul>
    	                <a href="<?php bloginfo('url'); ?>/shopping-bag" title="Modify" class="btn btn-primary btn-block">Edit</a>
                    </aside>
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="main-content clearfix">
                        <form method="POST" action="<?php bloginfo('url'); ?>/delivery-mode">
                            <section class="form">
                                <section class="bloc-promotion">
                                    <h3 class="main-content__title main-content__title--promo">Promotion code</h3>
                                        <div class="bloc-delivery clearfix">
                                        <div class="input-group input-group-generic">
    	                                    <label class="desktop-label col-sm-3 col-md-4" for="membership-password">Enter your promo code<span class="required">*</span></label>
    	                                    <div class="col-sm-6 col-xs-9">
    		                                    <input type="text" title="My postal code" id="membership-password" name="membership-password" placeholder="Enter your promo code*" class="form-control col-sm-12 col-md-9">
    		                                    <a href="#" title="Want to redeem a gift card ?" class="want-gift link">Want to redeem a gift card ? <i class="fa fa-question-circle"></i></a>
    		                                    <span class="mobile-label">Enter your promo code<span class="required">*</span></span>
    	                                    </div>
    	                                    <button class="btn btn-primary btn-noWidth col-xs-3 col-md-2" type="button">Ok</button>
                                        </div>
                                    </div>
                                </section>

                                <section  class="delivery-address">
                                    <h3 class="main-content__title">Delivery address</h3>
                                    <div class="bloc-delivery bloc-address account-detail" style="background: white">
    	                                <div class="row">
    		                                <dl class="col-sm-6">
    			                                <dt class="address__preview__label">Address</dt>
    			                                <dd class="address__preview__value">
    				                                Mr First name second name <br>
    				                                Adress line <br>
    				                                1234 City <br>
    				                                Switzerland
    			                                </dd>
    			                                <a href="<?php bloginfo('url'); ?>/delivery-address" class="link link--right" title="Edit address">Edit address</a>
    		                                </dl>
    		                                <dl class="col-sm-6">
    			                                <dt class="address__preview__label">Belling remark</dt>
    			                                <dd class="address__preview__value">Door Code 1234 – Leave on the door front if not there.</dd>
    		                                </dl>
    		                                <p class="pull-left col-sm-12">Currently set as your default billing address</p>
    	                                </div>
    	                                <div class="delivery-address-buttons">
    		                                <a href="<?php bloginfo('url'); ?>/adresses" class="btn btn-primary" title="Choose another delivery address">Choose another address</a>
    		                                <a href="<?php bloginfo('url'); ?>/adresses" class="btn btn-primary" title="Add another delivery address">Add another address</a>
    	                                </div>
                                    </div>
                                </section>

                                <section  class="delivery-method">
                                    <h3 class="main-content__title">Delivery method</h3>
                                    <h4 class="main-content__subtitle">Delivery at home or office</h4>
                                    <div class="bloc-delivery">
                                        <div class="radio" role="group" aria-labelledby="delivery_method2">
                                            <label for="delivery_method2">
                                                <input type="radio" name="delivery_method" id="delivery_method2" value="Standard" title="Standard">
                                                <i class="icon-Delivery_off"></i>
                                                <span>Standard</span>
                                                <span class="delivery-free">Free</span>
                                            </label>
                                        </div>
                                        <div class="content">
                                        <p>We deliver all your orders in 24h free of charge. Orders plan from Monday to Friday. For a delivery on Saturday, please select the free «Saturday Delivery».</p>
                                            <div class="delivery-options">
                                                <span>Available options :</span>
                                                <div class="radio" role="group" aria-labelledby="delivery_option1">
                                                    <label for="delivery_option1">
                                                        <input id="delivery_option1" type="radio" title="Saturday Delivery"  value="Saturday Delivery" name="delivery_option">
                                                        <span>Saturday Delivery</span>
                                                    </label>
                                                    <span class="price">(Free)</span>
                                                </div>
                                                <div class="radio" role="group" aria-labelledby="delivery_option2">
                                                    <label for="delivery_option2">
                                                        <input id="delivery_option2" type="radio" title="Recycling at home" value="Recycling at home" name="delivery_option">
                                                        <span>Recycling at home</span>
                                                    </label>
                                                    <span class="price">(Free)</span>
                                                </div>
                                                <div class="radio" role="group" aria-labelledby="delivery_option3">
                                                    <label for="delivery_option3">
                                                        <input id="delivery_option3" type="radio" title="Signature" value="Signature" name="delivery_option">
                                                        <span>Signature</span>
                                                    </label>
                                                    <span class="price">(Free)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="main-content__subtitle">Delivery at collection point</h4>
                                    <div class="bloc-delivery">
                                        <div class="radio" role="group" aria-labelledby="delivery_method3">
                                            <label for="delivery_method3">
                                                <input type="radio" name="delivery_method" id="delivery_method3" value="Pick-Up Points" title="Pick-Up Points" data-featherlight="lightbox?page=pickup-points">
                                                <i class="icon-Pick_up_point_off"></i>
                                                <span>Pick-Up Points</span>
                                                <span class="delivery-free">Free</span>
                                            </label>
                                        </div>
                                        <div class="content">
                                            <p>Choose the delivery of your orders at a pick-up point near to you. Delivery within one working day (Monday to Friday) for all orders placed before 4:30p Monday to Friday).</p>
                                        </div>
                                    </div>
                                    <div class="bloc-delivery">
                                        <div class="radio" role="group" aria-labelledby="delivery_method4">
                                            <label for="delivery_method4">
                                                <input type="radio" name="delivery_method" id="delivery_method4" value="Boutique Pick-Up" title="Boutique Pick-Up" data-featherlight="lightbox?page=boutique-pickup">
                                                <i class="icon-Boutique_pick_up_off"></i>
                                                <span>Boutique Pick-Up</span>
                                                <span class="delivery-free">Free</span>
                                            </label>
                                        </div>
                                        <div class="content">
                                            <p>Choose the <i>Nespresso</i> Boutique where you would like to pick up your order.</p>
                                            <a href="#" class="link link--right">Learn more</a>
                                        </div>
                                    </div>
                                    <h4 class="main-content__subtitle">Delivery on a time slot</h4>
                                    <div class="bloc-delivery">
                                        <div class="radio" role="group" aria-labelledby="delivery_method5">
                                            <label for="delivery_method5">
                                                <input type="radio" name="delivery_method" id="delivery_method5" value="Nespresso Your Time" title="Nespresso Your Time" data-featherlight="lightbox?page=delivery-time">
                                                <i class="icon-Your_time_off"></i>
                                                <span>Nespresso Your Time</span>
                                                <span class="delivery-free delivery-free-large">From $10.50</span>
                                            </label>
                                        </div>
                                        <div class="content">
                                            <p>Select a time slot for the delivery of your order to suit you, from Monday to Friday. Click on «Learn more» to view service availability. 1st time slot available : today 5pm.</p>
                                            <div class="delivery-options"  role="radiogroup">
                                                <span>Available options :</span>
                                                <div class="radio" role="group" aria-labelledby="delivery_options1">
                                                    <label for="delivery_options1" >
                                                        <input id="delivery_options1" type="radio" title="Recycling at home" name="delivery_options" aria-label="Recycling at home"  value="Recycling at home">
                                                        <span>Recycling at home</span>
                                                    </label>
                                                    <span class="price">(Free)</span>
                                                </div>
                                                <div class="radio" role="group" aria-labelledby="delivery_options2">
                                                    <label for="delivery_options2" >
                                                        <input id="delivery_options2" type="radio" title="Signature" name="delivery_options" aria-label="Signature"  value="Signature">
                                                        <span>Signature</span>
                                                    </label>
                                                    <span class="price">(Free)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </section>
                                <div class="link-wrapper">
                                    <a class="btn btn-green btn-icon-right pull-right" href="<?php bloginfo('url'); ?>/payment" title="Proceed to payment"><span>Proceed to payment</span> <i class="icon icon-arrow_left"></i></a>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>
<?php
global $scripts;
$scripts[]='js/components/shopping-bag-small.js';
?>
<!-- /content -->
