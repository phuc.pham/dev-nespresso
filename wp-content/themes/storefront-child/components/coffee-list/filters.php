<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$f_intensity = get_acf_field('intensity');
$f_intensity_min = isset($f_intensity['min']) ? $f_intensity['min'] : null;
$f_intensity_max = isset($f_intensity['max']) ? $f_intensity['max'] : null;

$f_size_of_cup_obj = get_acf_field('size_of_cup');
$f_size_of_cups = isset($f_size_of_cup_obj['choices']) ? $f_size_of_cup_obj['choices'] : [];

$f_aromatic_profile_obj = get_acf_field('aromatic_profile');
$f_aromatic_profiles = isset($f_aromatic_profile_obj['choices']) ? $f_aromatic_profile_obj['choices'] : [];
unset($f_aromatic_profiles['Varied']);

?>

<form action="#" method="get" id="filters-form-<?php echo rand(0,9999); ?>">

    <?php if ( $f_intensity_min && $f_intensity_max ) : ?>
        <div class="filter">
            <div class="title">INTENSITY</div>
            <div class="control intensities">
                <?php for( $i = $f_intensity_min; $i <= $f_intensity_max; $i++ ) : ?>
                <input type="checkbox" name="intensity" id="intensity-<?= $i ?>" value="<?= $i ?>" <?php if($i > 2 && $i < 12): ?>checked="checked"<?php endif; ?>><label for="intensity-<?= $i ?>"><?= $i ?></label>
                <?php endfor; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ( $f_size_of_cups ) : ?>
        <div class="filter">
            <div class="title">SIZE OF CUP</div>
            <div class="control">
                <?php foreach( $f_size_of_cups as $f_size => $f_size_of_cup ) : ?>
                    <label>
                        <input type="checkbox"
                            name="cupsize"
                            value="<?php echo $f_size ; ?>"
                        >
                        <span><?php echo $f_size_of_cup; ?></span>
                    </label>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ( $f_aromatic_profiles ) : ?>
        <div class="filter">
            <div class="title">AROMATIC PROFILE</div>
    		<div class="control">
                <?php foreach( $f_aromatic_profiles as $aromatic => $f_aromatic_profile ) : ?>
                    <label>
                        <input type="checkbox"
                            name="aromatic"
                            value="<?php echo $f_aromatic_profile ; ?>"
                        >
                        <span><?php echo $f_aromatic_profile; ?></span>
                    </label>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>

</form>
