<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with coffee products
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

$hq_data  = get_hq_data($product->sku);
?>

<li class="track-impression-product product"
    data-product-item-id="<?= $product->sku ?>"
    data-product-section="Coffee List"
    data-product-position="<?= $counter ?>"

    data-cupsize="<?=$product->size_of_cup[0]?>"
    data-aromatic="<?=$product->aromatic_profile ? $product->aromatic_profile : '';?>"
    data-intensity="<?=$product->intensity ? $product->intensity : '';?>"
>

    <div class="product-image">
				<?=get_nespresso_show_off($product->label_off);?>
        <a href="<?php echo $product->url; ?>" title="<?=$product->name?>"
            class="nespresso-product-item"
            title="<?= $product->name ?>"
            data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
            data-sku="<?= $product->sku ?>"
            data-name="<?=$product->name?>"
            data-price="<?=$product->price?>"
            data-type="<?=$product->product_type?>"
            data-position="<?= $counter ?>"
            data-list="<?= get_the_title() ?>"
            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
            data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
        >
            <img src="<?php echo $product->image ? $product->image[0] : ''; ?>" class="product-image" alt="<?=$product->name?>" >

        </a>
    </div>

    <div class="product-text">

        <?php if (isset($product->description)): ?>
            <div class="product-description">
        <?php else: ?>
            <div class="product-description small">
        <?php endif;?>

            <h3>
                <a href="<?=$product->url?>" title="<?=$product->name?>"
                    class="nespresso-product-item"
                    title="<?= $product->name ?>"
                    data-id="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
                    data-sku="<?= $product->sku ?>"
                    data-name="<?=$product->name?>"
                    data-price="<?=$product->price?>"
                    data-type="<?=$product->product_type?>"
                    data-position="<?= $counter ?>"
                    data-list="<?= get_the_title() ?>"
                    data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                    data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
                >
                    <?=$product->name?>
                </a>
            </h3>

            <?php if ($product->size_of_cup): ?>
                <div class="cupsize">
                    <?php if(is_array($product->size_of_cup)):
                        foreach ($product->size_of_cup as $key => $value): ?>
                            <i class="cupsize__<?=$value?>"></i>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <i class="cupsize__<?=$product->size_of_cup?>"></i>
                    <?php endif;
                    if ($product->aromatic_profile): ?>
                        <span><?=$product->aromatic_profile?></span>
                    <?php endif;?>

                </div>
            <?php endif;?>

            <?php if ($product->property): ?>
                <div class="product-property"><?=$product->property?></div>
            <?php endif;?>

            <?php if ($product->aromatic_profile && $product->aromatic_profile !== 'Varied'): ?>
                <?php if (isset($product->intensity)): ?>
                    <div class="intensity">
                        <?=str_repeat('<i></i>', $product->intensity ? $product->intensity : 0)?>
                        <?=$product->intensity?>
                        <?=str_repeat('<i class="empty"></i>', $product->intensity ? (13 - $product->intensity) : 0)?>
                    </div>
                <?php endif;?>
            <?php endif;?>
        </div>

        <div class="product-price">            
            <?=$product->price ? wc_price($product->price) : ''?>
			<?php if($product->regular_price>$product->price){ ?>
                <span class="product-price--old"><?=wc_price($product->regular_price) ?></span>
            <?php  } ?>
        </div>

    </div>	
    <div class="product-add">
		<?php 
		if($product->qty_x1){
		?>
		 <?php if ($product->price):
				$class = "btn-green";
				$label = "View Details &amp; Buy";
			else:
				$class = "btn-disabled";
				$label = "Out of stock";
			endif; ?>
			<a href="<?= $product->url ?>" title="" class="btn  btn-block <?= $class ?> btn-icon-right">
				<?= $label ?>
			</a>
		<?php }else{ ?>
        <?php if ($product->stock > 0):
            $class = "btn-green";
            $label = "Add to basket";
        else:
            $class = "btn-disabled";
            $label = "Out of stock";
        endif; ?>
        <button class="btn btn-icon btn-icon-right btn-block <?= $class ?>"
            data-id="<?= $product->post_id ?>"
            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
            data-sku="<?= $product->sku ?>"
            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
            data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
            data-cart="true"
            data-name="<?=$product->name?>"
            data-price="<?=$product->price?>"
            data-image-url="<?=$product->image ? $product->image[0] : ''?>"
            data-type="<?=$product->product_type?>"
            data-vat="1"
            data-qty-step="10"
            data-url="<?= $product->url ?>"
            data-aromatic-profile="<?= $product->aromatic_profile?>"
        >
        <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text"><?= $label ?></span>&nbsp;<i class="icon-plus text"></i>
        </button>
            <div  class="mobi-select-cont" >
                <select class="mobi-sec" style="width: 100%;height: 100%;"
                data-id="<?= $product->post_id ?>"
                data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->post_id?>"
                data-sku="<?= $product->sku ?>"
                data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                data-technology="<?= isset($hq_data['technology']) ? $hq_data['technology'] : '' ?>"
                data-cart="true"
                data-name="<?=$product->name?>"
                data-price="<?=$product->price?>"
                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
                data-type="<?=$product->product_type?>"
                data-vat="1"
                data-qty-step="10"
                data-url="<?= $product->url ?>"
                data-aromatic-profile="<?= $product->aromatic_profile?>"
                >

                    <?php
                    $c=0;
                    $quantity = 500;
                    $quantity_increment = 10;
                        if($product->aromatic_profile == 'Varied') {
                            $quantity = 100;
                            $quantity_increment = 1;
                        }
                        while ( $c<= $quantity) {
                           if ($c != '0') {
                                echo "<option value='$c'>$c</option>";
                           }
                           if ($c == '100') {
                                $quantity_increment = 50;
                           } else if ($c > '150') {
                                $quantity_increment = 100;
                           }

                           $c+=$quantity_increment;
                        }
                     ?>
                </select>
            </div>
<?php } ?>
    </div>
	
<!--
    <?php if ($product->name == 'Volluto Decaffeinato'): ?>
        <p>For this premium coffee you can purchase a maximum of 50 Capsules</p>
    <?php endif;?>
    <?php if ($product->name == 'Livanto'): ?>
        <p>You have already purchased the maximum number of 50 capsules for this premium coffee</p>
    <?php endif;?>

 -->
</li>
