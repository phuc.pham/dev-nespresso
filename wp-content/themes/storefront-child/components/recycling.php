<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>
 <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/recycle-capsule.css">
 <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/service.css">
<!-- content -->
<div class="container-fluid" id="recycling">
    <div class="row">
        <div class="g recycling-page">
            <div class="recycling-main clearfix">
                <div class="col-xs-12">
                <div class="free-html">
                     <div class="vue">
                        <!-- section 1 -->
                        <section class="vue_introduction v_introduction v_sectionnull v_backgroundVideoPlaying" id="introduction" data-label="Introduction">
                           <div class="v_placeholder g_desktop" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/Recycle_Page_V2.PNG.png');" lazy="loaded">
                           </div>
                           <div class="v_placeholder g_mobile" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/Still_Frame_Recycling_Page_V2.png');" lazy="loaded">
                           </div>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <header>
                                    <div class="v_cell">
                                       <div class="v_menu">
                                          <p class="v_visually_hidden"></p>
                                          <ul>
                                             <li>
                                                <!-- <a class="discover_recycling" href="/product/nespresso-recycling-bag" target="_blank">Discover Recycling Accessories</a> -->
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </header>
                                 <article>
                                    <div class="v_articleContent">
                                       <h2 style="margin-top: 70px;">
                                          <span>DISCOVER SOMETHING ELSE WHEN YOU RECYCLE WITH NESPRESSO</span>
                                       </h2>
                                       <div class="v_wysiwyg">
                                          <p>Your last sip of coffee marks the beginning of its second life.</p>
                                          <p>The aluminum in every Nespresso capsule you recycle goes into the making of something useful again.</p>
                                          <p>This time, it’s a pair of chopsticks.</p>
                                       </div>
                                       <div class="v_buttonContainer">
                                          <a class="v_btnRoundM serviceId-ordering" style="box-shadow: unset !important;">
                                          <i class="fn_videoCircle"></i>
                                          <span>Watch the video</span>
                                          </a>
                                       </div>
                                    </div>
                                 </article>
                              </div>
                           </div>
                           <div id="video_container" class="v_video hide">
                              <button id="pause_video" name="button" class="v_btnRoundSM v_btnClose">
                              <i class="fn_close"></i>
                              <span></span>
                              </button>
                              <iframe  id="v_video_container" allowfullscreen="1" width="640" height="360" frameborder="0" src="https://www.youtube.com/embed/Bppf9zl8xeQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           </div>
                        </section>
                        <!-- section 2 -->
                        <section class="vue_magicContent v_sectionDark v_findPoint v_sectionCenter" pv-height="auto" id="findPoint" data-label="Find Point">
                           <div class="bg_true" pv-speed="4">
                              <div class="bg_full g_desktop" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/findPoint_XL.jpg');" lazy="loaded"></div>
                              <div class="bg_full g_mobile" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/findPoint_S.jpg');" lazy="loaded"></div>
                           </div>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <div class="v_text">
                                    <h2 data-wow="" class="wow">Here is how you can recycle your coffee capsules</h2>
                                    <p class="v_headline wow" data-wow="">With the Nespresso recycling plan, we provide a solution for recycling your used aluminum capsules and fulfilling your commitment to protecting the environment.</p>
                                    <div class="v_row2 v_gridList" slot="content">
                                       <!--<div class="v_gridItem custom-gridItem">
                                          <img data-wow="" alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/pick.svg" data-wow-delay="0" class="wow">
                                          <p data-wow="" data-wow-delay="0" class="wow">
                                             Claim your recycling bag at the <strong class="v_brand" term="nespresso">Nespresso</strong> Boutique or add a recycling bag to your online coffee order.
                                          </p>
                                       </div>
                                        <div class="v_gridItem">
                                          <strong data-wow="" data-wow-delay="1" class="wow">19 000</strong>
                                          <p data-wow="" data-wow-delay="1" class="wow">COLLECTION POINTS
                                              <br aria-hidden="true">IN AUSTRALIA
                                          </p>
                                          </div> -->
                                       <div class="v_gridItem custom-gridItem">
                                          <img data-wow="" alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/trash.svg" data-wow-delay="2" class="wow">
                                          <p data-wow="" data-wow-delay="2" class="wow">
                                             Fill your recycling bag with used Nespresso capsules and return them during your next home delivery.                                          <p></p>
                                           <!--<br aria-hidden="true">
                                          <a style="opacity:1; font-size:14px;" class="v_btn v_btnRoundCorner v_btnGold v_block wow" data-wow="" href="/store-locator" target="_blank" data-wow-delay="NaN">
                                          <span style="color:#fff;">Discover the program</span>
                                          </a>
                                          <p></p>-->
                                          </p>
                                       </div>
                                       <!-- <div class="v_gridItem">
                                          <strong data-wow="" data-wow-delay="0" class="wow">100%</strong>
                                          <p data-wow="" data-wow-delay="0" class="wow">OF OUR CUSTOMERS HAVE
                                              <br aria-hidden="true">A RECYCLING SOLUTION NEARBY
                                          </p>
                                          </div> -->
                                       <div class="v_gridItem custom-gridItem">
                                          <img data-wow="" alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/Professional_Capsule_Recycling_Logo_-_White.png" data-wow-delay="1" class="wow">
                                          <p data-wow="" data-wow-delay="1" class="wow">
                                             For our <strong class="v_brand" term="nespresso">Nespresso</strong> Professional customers, we offer a free of charge collection service of Nespresso Professional capsules.
                                             <br aria-hidden="true">
                                             <!-- <a href="https://www.nespresso.com/au/en/order/accessories/original/recycling-postage-bag" target="_blank">Order an Australia Post recycling satchel</a> -->
                                          </p>
                                       </div>
                                       <!-- <div class="v_gridItem">
                                          <p class="v_legend wow" data-wow="" data-wow-delay="NaN"></p>
                                          <a class="v_btn v_btnRoundCorner v_btnGold v_block wow" data-wow="" href="/store-locator" target="_blank" data-wow-delay="NaN">
                                              <span>FIND YOUR NEAREST RECYCLING POINT</span>
                                          </a>
                                          </div> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- section 3 -->
                        <section class="vue_magicContent v_sectionDark v_keyPillars v_sectionCenter" pv-height="auto" id="keyPillars" data-label="3 areas">
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <div class="v_text">
                                    <h2 data-wow="" class="wow">
                                       <strong class="v_brand" term="nespresso">Nespresso</strong> focuses efforts around 3 areas
                                    </h2>
                                    <div class="v_row3 v_gridList" slot="content">
                                       <div class="v_gridItem">
                                          <div class="v_imageContainer v_imageRound wow" data-wow="" data-wow-delay="0">
                                             <div class="v_image section_nav g_desktop" href="#blocVideoPremium" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/premium-classic_XL.jpg');" lazy="loaded"></div>
                                             <div class="v_image section_nav g_mobile" href="#blocVideoPremium" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/premium-classic_S.jpg');" lazy="loaded"></div>
                                          </div>
                                          <h3 data-wow="" data-wow-delay="0" class="wow">COFFEE QUALITY</h3>
                                          <p data-wow="" data-wow-delay="0" class="wow">Aluminum is the most protective material available today. It guarantees the freshness…</p>
                                          <a class="v_link v_iconRight wow" data-wow="" href="#blocVideoPremium" data-wow-delay="0">
                                          <i class="fn_angleRight"></i> Learn more
                                          </a>
                                       </div>
                                       <div class="v_gridItem">
                                          <div class="v_imageContainer v_imageRound wow" data-wow="" data-wow-delay="1">
                                             <div class="v_image section_nav g_desktop" href="#blocVideoRecycling" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/recyclability-classic_XL.jpg');" lazy="loaded"></div>
                                             <div class="v_image section_nav g_mobile" href="#blocVideoRecycling" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/recyclability-classic_S.jpg');" lazy="loaded"></div>
                                          </div>
                                          <h3 data-wow="" data-wow-delay="1" class="wow">RECYCLABILITY</h3>
                                          <p data-wow="" data-wow-delay="1" class="wow">At
                                             <strong class="v_brand" term="nespresso">Nespresso</strong>, we seek ways to make a difference anywhere we can. This is why we…
                                          </p>
                                          <a class="v_link v_iconRight wow" data-wow="" href="#blocVideoRecycling" data-wow-delay="1">
                                          <i class="fn_angleRight"></i> Learn more
                                          </a>
                                       </div>
                                       <div class="v_gridItem">
                                          <div class="v_imageContainer v_imageRound wow" data-wow="" data-wow-delay="2">
                                             <div class="v_image section_nav g_desktop" href="#secondLife" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/second-classic_XL.jpg');" lazy="loaded"></div>
                                             <div class="v_image section_nav g_mobile" href="#secondLife" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/second-classic_S.jpg');" lazy="loaded"></div>
                                          </div>
                                          <h3 data-wow="" data-wow-delay="2" class="wow">SECOND LIFE</h3>
                                          <p data-wow="" data-wow-delay="2" class="wow">Beyond its ability to deliver your premium coffee, we selected aluminum for other reasons…</p>
                                          <a class="v_link v_iconRight wow" data-wow="" href="#secondLife" data-wow-delay="2">
                                          <i class="fn_angleRight"></i> Learn more
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- section 4 -->
                        <div id="video_1_checker"></div>
                        <section id="blocVideoPremium" class="vue_blocVideo v_blocVideoPremium v_sectionnull"  data-label="Premium Coffee">
                           <div class="v_placeholder g_desktop" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/blocVideoPremium-classic_XL.jpg');">
                              <video id="video_1" loop="" muted="" preload="auto" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoPremium-classic_XL.mp4" style="opacity: 1;" class="loaded"></video>
                           </div>
                           <div class="v_placeholder g_mobile" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/blocVideoPremium-classic_S.jpg');"></div>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <article>
                                    <div class="v_articleContent">
                                       <h2 data-wow="" class="wow">
                                          <span  >Coffee is protected from the outside,
                                          <br aria-hidden="true"> while aromas are preserved inside
                                          </span>
                                       </h2>
                                       <ul class="v_row v_icons">
                                          <li data-wow="" class="wow">
                                             <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/oxygen.svg">
                                             <h3>Oxygen</h3>
                                          </li>
                                          <li data-wow="" class="wow">
                                             <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/moisture.svg">
                                             <h3>Moisture</h3>
                                          </li>
                                          <li data-wow="" class="wow">
                                             <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/light.svg">
                                             <h3>Light</h3>
                                          </li>
                                       </ul>
                                       <div class="v_wysiwyg wow" data-wow="">
                                          <p>Aluminum is the most protective material available today. It guarantees the freshness of the carefully crafted aromas and rich flavors of our coffees.
                                             <br aria-hidden="true">Lightweight but strong, aluminum seals the coffee from external elements which could affect its quality – oxygen, moisture and light.
                                          </p>
                                       </div>
                                    </div>
                                 </article>
                              </div>
                           </div>
                        </section>
                        <!-- section 5 -->
                       <!-- <section class="vue_recycling v_sectionDark" id="recyclingBar" data-label="recyclingBar">
                           <div class="vue_recyclingBar">
                              <div class="v_sectionRestrict">
                                 <a href="/store-locator#recycling-point" target="_blank" class="v_link v_iconRight">
                                 <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/pick-gold.svg">
                                 <i class="fn_angleRight"></i>
                                 <span>Find a recycling point</span>
                                 </a>
                                 <div class="v_wrapper">
                                    <div class="v_addToCart">
                                       <div class="v_priceAndButton">
                                          <div class="v_addToCartCustom">
                                             <button style="height: unset; letter-spacing: 0.1em;"
                                                data-id="96373"
                                                data-cart="true"
                                                data-name="NESPRESSO RECYCLING BAG"
                                                data-price="0"
                                                data-image-url="https://www.nespresso.ph/wp-content/uploads/2018/09/Recycling-Bag-e1539328528946.png"
                                                data-type="Accessory"
                                                data-vat="1"
                                                data-qty-step="1"
                                                data-url="https://www.nespresso.ph/product/nespresso-recycling-bag"
                                                >
                                             <i class="fn_addToCart">
                                             <span>0</span>
                                             </i>
                                             <span class="v_label" aria-hidden="true">Order a Recycling Bag</span>
                                             </button>
                                          </div>
                                       </div>
                                    </div>
                                    <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/10800221323294.png" data-product-item-id="101852">
                                 </div>
                              </div>
                           </div>
                        </section>-->
                        <!-- section 6 -->
                        <div id="video_2_checker"></div>
                        <section class="vue_blocVideo v_blocVideoRecycling v_sectionnull" id="blocVideoRecycling" data-label="Inspiring sustainable consumption">
                           <div class="v_placeholder g_desktop" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/blocVideoRecycling-classic_XL.jpg');">
                              <video id="video_2" class="loaded" loop="" muted="" preload="auto" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoRecycling-classic_XL.mp4" style="opacity: 1;" class="loaded"></video>
                           </div>
                           <div class="v_placeholder g_mobile" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/blocVideoRecycling-classic_S.jpg');"></div>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <article>
                                    <div class="v_articleContent">
                                       <h2 data-wow="" class="wow">
                                          <span>Inspiring sustainable consumption</span>
                                       </h2>
                                       <ul class="v_row v_icons"></ul>
                                       <div class="v_wysiwyg wow" data-wow="">
                                          <p>At <strong class="v_brand" term="nespresso">Nespresso</strong>, we seek ways to make a difference anywhere we can. This is why we are committed to doing business responsibly.
                                             <br aria-hidden="true">It means ensuring the sustainability of your coffee through a comprehensive program, with observable results.
                                          </p>
                                       </div>
                                    </div>
                                 </article>
                              </div>
                           </div>
                        </section>
                        <!-- section 7 -->
                        <section class="vue_testimonial v_sectionDark v_sectionAutoHeight" id="testimonial" data-label="Testimonial">
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <div class="v_tableRow">
                                    <div class="v_col80">
                                       <div class="v_wysiwyg v_quote wow" data-wow="" data-wow-delay="1">"The sustainability program with
                                          <strong class="v_brand" term="nespresso">Nespresso</strong> is surpassed by no one… they have set this up so that if you are responsible and you want to participate in true recycling: it is very easy to do."
                                       </div>
                                       <p class="v_sign wow" data-wow="" data-wow-delay="2">
                                          <strong></strong>
                                          <span>Interview on Sustainability with George Clooney</span>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- section 8 -->
                        <section class="vue_collapsableContent v_sectionLight v_collapsed v_sectionundefined" id="collapsed" data-label="hidden">
                           <h2>
                              <button id="expand_section_8" style="height: unset;">
                                 <div>
                                    <span>LEARN MORE ABOUT THE GOALS</span>
                                 </div>
                                 <i class="fn_more"></i>
                              </button>
                           </h2>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <div class="v_text">
                                    <div class="v_row">
                                       <div class="v_wysiwyg wow" data-wow="">
                                          <ul>
                                             <li>Offer convenient recycling solutions to all of our customers.</li>
                                             <li>Continue to increase our recycling rate and unlock the circular use of aluminum</li>
                                             <li>Sourcing aluminum compliant with the Aluminum Stewardship Initiative (ASI) standards towards 100% by 2020. You can find more about this here:
                                                <a href="https://www.aluminium-stewardship.org" target="_blank">www.aluminium-stewardship.org</a>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- section 9 -->
                        <section class="vue_secondLife v_sectionDark" id="secondLife" data-label="Second Life">
                           <div class="v_placeholder g_desktop" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/BG_XL.jpg');"></div>
                           <div class="v_placeholder g_mobile" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/BG_S.jpg');"></div>
                           <i class="fn_more v_close" id="secondLife_close" style="top: 80px;"></i>
                           <div class="v_rangeSlider ui-draggable ui-draggable-handle" id="range_slider" data-position="middle">
                              <div class="v_rangeSlider__drag">
                                 <a id="v_rangeSlider--Alu" class="v_rangeSlider--Alu" href="javascript:void(0)" data-target="aluminium" title="Learn more"></a>
                                 <a id="v_rangeSlider--Ground" class="v_rangeSlider--Ground" href="javascript:void(0)" data-target="ground" title="Learn more"></a>
                              </div>
                           </div>
                           <div class="v_content">
                              <div class="v_sectionRestrict">
                                 <h2 data-wow="" class="wow">
                                    <span>The last drop doesn’t mean the last use</span>
                                 </h2>
                                 <div class="v_wysiwyg wow" data-wow="">
                                    <p>A second life for both capsule and coffee. </p>
                                 </div>
                              </div>
                           </div>
                           <div class="v_sectionAluminium" id="aluminium" style="height: 992px;">
                              <div class="v_sectionAluminium__video v_video">
                                 <video muted="" preload="auto" class="v_sectionAluminium__video--0 g_desktop" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoAlu1-classic_XL.mp4" style="opacity: 0"></video>
                                 <video muted="" preload="auto" class="v_sectionAluminium__video--0 g_mobile" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoAlu1-classic_S.mp4" style="opacity: 0"></video>
                                 <video muted="" preload="auto" class="v_sectionAluminium__video--1 g_desktop" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoAlu3-classic_XL.mp4" style="opacity: 1"></video>
                                 <video muted="" preload="auto" class="v_sectionAluminium__video--1 g_mobile" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoAlu3-classic_S.mp4" style="opacity: 1"></video>
                              </div>
                              <div class="v_sectionRestrict">
                                 <img alt="" class="v_sectionAluminium__visual g_desktop" src="/wp-content/themes/storefront-child/images/recycle-capsule/aluminium-classic_XL.png">
                                 <img alt="" class="v_sectionAluminium__visual g_mobile" src="/wp-content/themes/storefront-child/images/recycle-capsule/aluminium-classic_S.png">
                                 <div class="v_sectionAluminium__header">
                                    <img alt="" data-wow="" src="/wp-content/themes/storefront-child/images/recycle-capsule/alu-classic.svg" style="margin-left:auto; margin-right: auto;" class="wow">
                                    <strong data-wow="" class="wow">Aluminum</strong>
                                    <a id="v_link--Alu" href="#" class="v_link v_iconRight wow" data-wow=""> Learn more
                                    <i class="fn_angleRight"></i>
                                    </a>
                                 </div>
                                 <div class="v_sectionAluminium__description" style="height: 992px !important;">
                                    <h2>
                                       <span>The last drop doesn’t mean the last use for aluminum</span>
                                    </h2>
                                    <p>Aluminum is not just 100% recyclable, it’s infinitely recyclable. Each capsule you recycle can come back in another everyday object – another capsule, bicycle or window frame.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="v_sectionGround" id="ground" style="height: 995px;">
                              <div class="v_sectionGround__video v_video">
                                 <video loop="" muted="" class="v_sectionGround__video--0 g_desktop" preload="auto" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoGroundFertilizer-classic_XL.mp4"></video>
                                 <video loop="" muted="" class="v_sectionGround__video--0 g_mobile" preload="auto" src="/wp-content/themes/storefront-child/images/recycle-capsule/videoGroundFertilizer-classic_S.mp4"></video>
                                 <button type="button" name="button" class="v_btnRoundSM v_btnClose">
                                 <i class="fn_close"></i>
                                 </button>
                                 <div tabindex="-1" aria-hidden="true"></div>
                              </div>
                              <div class="v_sectionRestrict">
                                 <img alt="" class="v_sectionGround__visual g_desktop" src="/wp-content/themes/storefront-child/images/recycle-capsule/ground-classic_XL.png">
                                 <img alt="" class="v_sectionGround__visual g_mobile" src="/wp-content/themes/storefront-child/images/recycle-capsule/ground-classic_S.png">
                                 <div class="v_sectionGround__header">
                                    <img alt="" data-wow="" src="/wp-content/themes/storefront-child/images/recycle-capsule/ground.svg" style="margin-left:auto; margin-right: auto;" class="wow">
                                    <strong data-wow="" class="wow">Ground</strong>
                                    <a id="v_link--Ground" href="#" class="v_link v_iconRight wow" data-wow=""> Learn more
                                    <i class="fn_angleRight"></i>
                                    </a>
                                 </div>
                                 <div class="v_sectionGround__description" style="height: 992px !important;">
                                    <ul class="v_row v_tabIcons">
                                       <li class="active">
                                          <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/fertilizer.svg">
                                          <h3>Fertilizer</h3>
                                       </li>
                                       <li style="display: none;">
                                          <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/energy.svg">
                                          <h3>Energy</h3>
                                       </li>
                                    </ul>
                                    <h2>
                                       <span>The last drop doesn’t mean the last use for coffee grounds</span>
                                    </h2>
                                    <!-- <p>Once separated from the aluminum, coffee grounds can be reused in innovative ways. Today, you can find them in compost fertilizer to enrich the soil of rice fields or even vineyards. They’re also being transformed into renewable energy, like biofuel for buses.</p> -->
                                    <p>Once separated from the aluminum, the used coffee grounds are sent to local farms to be used in compost for vegetable farming. Using coffee grounds improves drainage, aerates soil and acts as a natural pest repellent.</p>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- section 10 -->
                        <section class="vue_blocVideo v_blocVideoCoffee v_sectionnull" id="blocVideoCoffee" data-label="Coffee sustainability">
                           <div id="hand_image_10" class="v_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/blocVideoCoffee-classic_XL.jpg');"></div>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <article>
                                    <div class="v_articleContent">
                                       <h2 data-wow="" class="wow">
                                          <span id="section_10_checker">Now and into the future</span>
                                       </h2>
                                       <ul class="v_row v_icons"></ul>
                                       <div class="v_wysiwyg wow" data-wow="">
                                          <p>Ever since our journey began with coffee more than 25 years ago, we have been deploying sustainability with the same passion and expertise.
                                             <br aria-hidden="true">Today,
                                             <strong class="v_brand" term="nespresso">Nespresso</strong>'s coffee capsule recycling program includes 39 countries.
                                             <br aria-hidden="true">We hope you will embark on this journey with us towards creating a more sustainable future.
                                          </p>
                                       </div>
                                       <!-- <a class="v_link v_iconRight wow" data-wow="" href="https://www.nespresso.com/positive/au/en#!/sustainability" target="_blank"> Learn more on sustainability
                                          <i class="fn_angleRight"></i>
                                          </a> -->
                                    </div>
                                 </article>
                              </div>
                           </div>
                        </section>
                        <!-- section 11 -->
                        <section pv-height="auto" class="vue_faq v_parallax v_sectionDark v_sectionRight v_key_faq" id="faq" data-label="FAQ" style="height: auto;">
                           <div class="bg_normal g_desktop" pv-speed="10" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/faq-classic_XL.jpg'); padding-bottom: 892.3px; margin-top: -123.7px; transform: translate3d(0px, 26.695px, 0px);" lazy="loaded"></div>
                           <div class="bg_normal g_mobile" pv-speed="10" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/faq-classic_S.jpg'); padding-bottom: 892.3px; transform: translate3d(0px, 26.695px, 0px);" lazy="loaded"></div>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <div class="v_text">
                                    <h2 data-wow="" class="wow">Frequently asked questions</h2>
                                    <div class="vue_accordion__container">
                                       <div class="vue_accordion">
                                          <ul role="accordion" class="v_isCollapsable">
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="1" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 1 of 10</span> Are <strong class="v_brand" term="nespresso">Nespresso</strong> coffee capsules recyclable?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-1" aria-hidden="true" style="max-height: 206px; display: none">
                                                   <p>
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> coffee capsules are made from aluminum – the best material to protect the freshness and quality of our coffee, which has the additional benefit of being infinitely recyclable.
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> has been investing in its dedicated recycling scheme for more than 25 years.
                                                      We are committed to making it as simple and convenient for our customers to recycle their Nespresso capsules.
                                                   </p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="2" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 2 of 10</span> How can
                                                <strong class="v_brand" term="nespresso">Nespresso</strong> customers recycle their used coffee capsules?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-2" aria-hidden="true" style="max-height: 283px; display: none">
                                                   <p>
												   Now in Vietnam, to recycle your used Nespresso aluminum coffee capsules, please return your used capsules for every home delivery order.
                                                   </p>                                                  
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="3" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 3 of 10</span> What is done with the recycled aluminum and coffee grounds?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-3" aria-hidden="true" style="max-height: 206px; display: none">
                                                   <p>
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> aluminum coffee capsules that are recycled with
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> are sent to a specialist recycling plant. There, the aluminum is separated from the residual coffee. The coffee is sent to local farms to be transformed into compost, while the aluminum is recycled and sent back to the aluminum industry to produce new aluminum products.
                                                   </p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="4" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 4 of 10</span> Why does
                                                <strong class="v_brand" term="nespresso">Nespresso</strong> use aluminum for its coffee capsules?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-4" aria-hidden="true" style="display: none">
                                                   <p>We use aluminum because it is the best material to protect the freshness, taste and quality of
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> coffee. Aluminum is a naturally-occurring metal and is infinitely recyclable. Thanks to the aluminum, there is no need for additional over-packaging to protect freshness and taste. Recycled aluminum is extremely versatile. An estimated 75% of all aluminum ever produced is still in use today. Aluminum is used in a wide range of products, from food packaging to cars and computers, window frames to bicycles.
                                                   </p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="5" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 5 of 10</span> Are your capsules made from recycled aluminum?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-5" aria-hidden="true" style=" display: none">
                                                   <p>
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> capsules require a special grade of aluminum. The availability of recycled aluminum in this specific grade is very low. In fact, the best source of the correct grade of aluminum is from recycled
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> capsules. We are using recycled
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> capsules to make new capsules wherever it is feasible and it makes sense environmentally.
                                                   </p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="6" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 6 of 10</span> Have you ever considered using plastic capsules as an alternative?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-6" aria-hidden="true" style=" display: none">
                                                   <p>Plastic capsules generally need more packaging such as an additional wrapper to preserve freshness. Ironically, this is often made of aluminum. We believe aluminum is the more sustainable material for our capsules as it reduces the need for extra packaging and protects the freshness and quality of our coffee - and it is infinitely recyclable.</p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="7" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 7 of 10</span> What is the carbon footprint of portioned coffee?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-7" aria-hidden="true" style="display: none">
                                                   <p>We believe that portioned coffee makes sense from a sustainability perspective. We use a Life Cycle Assessment (LCA) approach to measure the carbon footprint of our products. We are working at all stages of our product life-cycle – from seed to sip – to reduce our environment impact. Since 2009, we have already reduced the carbon footprint of every cup of <strong class="v_brand" term="nespresso">Nespresso</strong> by almost 20%.
                                                   </p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="8" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 8 of 10</span> Does portioned coffee generate unnecessary waste?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-8" aria-hidden="true" style="display: none">
                                                   <p>We use a Life Cycle Assessment (LCA) approach to measure the environmental impact of our products. It shows that the greatest impacts are the processes of coffee farming, followed by the machine-use phase. Packaging is not the main driver of the environmental impact, it only comes third. With the <strong class="v_brand" term="nespresso">Nespresso</strong> precision consumption system, our customers only use the exact amount of water, coffee and energy needed to make one cup with no wasted resources - minimising food, water and energy waste. The precise use of resources in a precision consumption system can actually reduce waste, and in many scenarios this more than compensates for the additional packaging used for portioned coffee.
                                                   </p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem">
                                                <button class="v_title faq-btn" data-faq="9" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 9 of 10</span> What about
                                                <strong class="v_brand" term="nespresso">Nespresso</strong> Professional customers, how can they recycle their used Professional coffee capsules?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-9" aria-hidden="true" style=" display: none; margin-top: 20px;">
                                                   <p>
                                                      We offer a free of charge collection service to our larger customers for their used
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> capsules. Upon request or during delivery, we pick up their used aluminum capsules (both Professional & in-home
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> capsules). As both
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> capsules are recyclable, we also encourage businesses to collect the used
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong> aluminum capsules that employees can bring in from home, making it even easier to participate in recycling.
                                                      <!-- <a href="https://www.nespresso.com/pro/au/en/pages/recycling-process" target="_blank">Discover more</a> -->
                                                   </p>
                                                </div>
                                             </li>
                                             <li class="v_accordionItem" style="padding-top: 20px;">
                                                <button class="v_title faq-btn" data-faq="10" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false">
                                                <i class="fn_moreCircle"></i>
                                                <span class="v_visually_hidden">Accordion item 10 of 10</span> What do I get for recycling?
                                                </button>
                                                <div class="v_wysiwyg faq-desc faq-container-10" aria-hidden="true" style=" display: none">
                                                   <p>We believe that customers understand and appreciate the impact that recycling can have on improving the environmental performance of a cup of
                                                      <i>
                                                      <strong class="v_brand" term="nespresso">Nespresso</strong>
                                                      </i> coffee and in itself this provides a strong incentive to recycle used capsules.
                                                   </p>
                                                </div>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- section 12 -->
                        <section pv-height="auto" class="vue_corporate v_sectionLeft v_parallax v_sectionDark v_sectionTop v_key_corporate" options="[object Object]" id="corporate" data-label="Corporate" style="height: auto;">
                           <div class="bg_normal g_desktop" pv-speed="10" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/why_nespresso_XL.jpg'); padding-bottom: 655.3px; margin-top: -100px; transform: translate3d(0px, 90.095px, 0px);" lazy="loaded"></div>
                           <div class="bg_normal g_mobile" pv-speed="10" style="background-image: url('/wp-content/themes/storefront-child/images/recycle-capsule/why_nespresso_S.jpg'); padding-bottom: 655.3px; margin-top: -100px; transform: translate3d(0px, 90.095px, 0px);" lazy="loaded"></div>
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <div class="v_text">
                                    <h2 data-wow="" class="wow">Why
                                       <strong class="v_brand" term="nespresso">Nespresso</strong>?
                                    </h2>
                                    <p data-wow="" class="wow">A cup of coffee is much more than a break. It is your small ritual. Make it an unparalleled experience.</p>
                                    <div class="v_wysiwyg wow" data-wow="">
                                       <p>Choose
                                          <strong class="v_brand" term="nespresso">Nespresso</strong>, do not settle for less: strictly-selected coffee coming in a matchless range of exclusive varieties, coffee machines combining smart design with simple state-of-the-art technology, refined accessories and services anticipating your every desire. What else?
                                       </p>
                                    </div>
                                    <a class="v_link v_iconRight" href="/coffee-list">
                                    <i class="fn_angleRight"></i> Place your order today
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <!-- section 12 -->
                        <section class="vue_services v_sectionDark v_key_services" id="services" data-label="Services">
                           <div class="v_sectionRestrict">
                              <div class="v_sectionContent">
                                 <h2 class="v_visually_hidden">
                                    <strong class="v_brand" term="nespresso">Nespresso</strong> Services
                                 </h2>
                                 <ul class="v_row5">
                                    <li data-wow="" class="wow">
                                       <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/freeDelivery.svg">
                                       <h3>Free Standard Delivery</h3>
                                       <p>Enjoy FREE STANDARD DELIVERY with a minimum of 1,500,000 VND spent</p>
                                    </li>
                                    <!--<li data-wow="" class="wow">
                                       <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/pickup.svg">
                                       <h3>Boutique</h3>
                                       <p>Visit the Nespresso Boutique at R1 Level, Power Plant Mall. Our coffee specialists would be glad to assist you in selecting your machine and the coffee that best suits your taste.</p>
                                    </li>-->
                                    <li data-wow="" class="wow">
                                       <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/assistance.svg">
                                       <h3>Customer Care</h3>
                                       <p>Need help with your machine or selecting your coffee? Call us any time between Monday to Saturday, from 9am to 7pm: 1900 633 474.</p>
                                    </li>
                                    <li data-wow="" class="wow">
                                       <img alt="" src="/wp-content/themes/storefront-child/images/recycle-capsule/payment.svg">
                                       <h3>Secured payment transactions</h3>
                                       <p>SSL encryption means sensitive information is always transmitted securely.</p>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </section>
                     </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .container -->
<!-- /content -->
<div class="g g_services2018">
    <div role="alertdialog" class="g_popin serviceId-orderingg_popinService">
        <div class="g_popinOverlay"></div>
        <div class="g_popinFixed">
            <div class="serviceId-orderingg_popinRestrict g_popinRestrict g_light">
                <button class="g_btnRoundS g_btnClose" aria-label="close"><span
                            class="">close</span><i class="fn_close"></i></button>
                <div class="serviceId-orderingg_popinContent g_popinContent">
                    <article class="g_popinContent g_loaded">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/i7F4-mXGy1A?&autoplay=0"
                                title="YouTube video player" frameborder="0"
                                autoplay="true"
                                id="playerid"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    jQuery(`.serviceId-ordering`).click(function () {
       
        jQuery(`.serviceId-orderingg_popinService`).addClass('g_popinOpened');
        jQuery(`.serviceId-orderingg_popinContent`).addClass('g_popinOpened');
        var video3 = 'https://www.youtube.com/embed/i7F4-mXGy1A?&autoplay=1';
        jQuery("#playerid").attr("src","");
        jQuery("#playerid").attr("src",video3);
       


    });

    jQuery(`.g_btnClose`).click(function () {
        jQuery(`.serviceId-orderingg_popinService`).removeClass('g_popinOpened');
        var video = 'https://www.youtube.com/embed/i7F4-mXGy1A?&autoplay=0';
        jQuery("#playerid").attr("src","");
    });

    jQuery(`.g_popinFixed`).click(function (obj) {
        var object = jQuery(`.g_popinContent`);

        if (!object.is(event.target) && !object.has(event.target).length) {
            jQuery(`.serviceId-orderingg_popinService`).removeClass('g_popinOpened');
            var video1 = 'https://www.youtube.com/embed/i7F4-mXGy1A?&autoplay=0';
            jQuery("#playerid").attr("src","");
        }

    });


</script>

<?php
global $scripts;
$scripts[] = 'js/components/recycling.js';
?>
