<?php
/**
 * Nespresso Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $wp;
$current_url = home_url(add_query_arg(array(), $wp->request));
$segment = '';
$parsed = parse_url($current_url);
if (isset($parsed['path'])) {
    $path = $parsed['path'];
    $path_parts = explode('/', $path);
    $segment = sizeof($path_parts) > 1 ? $path_parts[1] : '';

    if ($segment == 'vi' || $segment == 'vn') {
        $segment = $path_parts[2];
    }
}

?><!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta http-equiv="X-UA Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php bloginfo('description');?>">
    <meta name="author" content="Koodi">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="google-site-verification" content="Ihzx1tBV53gOkNIrs2JJCElahIqsxA6tiG6pMorjWdA" />
    <title><?php bloginfo('name');?> <?php wp_title();?></title>
    <link rel="icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico">

    <!-- wp_head -->
    <?php wp_head();?>
    <!-- /wp_head -->

    <?php if ($segment == 'faq'): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css">
    <?php endif;?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/libs/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/libs/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css?t=<?=time()?>">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/css-helpers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css?t=<?=time()?>">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/override.css">

    <?php get_template_part('components/gtm');?>
    <!--  Google Tag Manager  -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].unshift({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','gtmDataObject','GTM-KMPTP65');</script>
    <!-- End Google Tag Manager -->

    <script type="text/javascript">
        window.host_url = '<?=get_site_url();?>';
        window.nespresso_token = '<?=$_SESSION['nespresso_token'];?>';
        window.currency_symbol = '<?=get_woocommerce_currency_symbol();?>';
        window.currency = '<?=get_woocommerce_currency();?>';
        <?php if (is_user_logged_in()): ?>
            window.user = <?=json_encode(nespresso_get_user_data());?>;
        <?php else: ?>
            window.user =  null;
        <?php endif;?>

    </script>

</head>
<body <?php body_class();?>>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMPTP65" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="wrapper">
        <div id="main-accessibility">
            <ul>
                <li><a href="#JumpContent" class="activateKeyboardNavigation" title="Jump to Content">Jump to Content</a></li>
                <li><a href="#JumpMenu" title="Jump to Menu">Jump to Menu</a></li>
                <li><a href="#JumpContact" title="Jump to Contact">Jump to Contact</a></li>
                <li><a href="#JumpFooter" title="Jump to Footer">Jump to Footer</a></li>
                <li><a href="#" class="activateDyslexia" title="Activate mode Dyslexia"><span class="dyslexia-desactivate hidden">Desactivate</span><span class="dyslexia-activate">Activate</span> mode Dyslexia</a></li>
            </ul>
        </div>
        <div id="main-wrapper">

            <!-- header -->
            <?php get_template_part('components/header');?>
            <!-- /header -->
