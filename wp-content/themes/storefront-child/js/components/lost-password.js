
/**
 * lost-password.js
 */

$(document).ready(function() {

	$('#form-lost-password').on('submit', function(e) {

		// check if form lost password is valid
		$validForm = $().validFormLostPassword();

		if ( !$validForm ) {
			e.preventDefault();
			return false;
		}

		return true;

	}); // $('#form-lost-password')()


});

(function($) {

	/**
	 * check if form lost password is valid
	 * aka forgot password
	 * 
	 * @return {[type]} [description]
	 */
	$.fn.validFormLostPassword = function() {

		$email = $('[name="email"]');
		
		$validation = new Validation();
		
		if ( !$validation.validate_email($email) )
			return false;

		return true;

	}; // validFormMyInfos()


})(jQuery);
