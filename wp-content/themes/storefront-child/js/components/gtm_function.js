
var gtm_products = [];
var gtm_promos = [];
var gtm_dropdown_promos = [];
var gtm_slider_promos = [];
var impressionsOnScroll = {};
var page_products = [];
$(document).ready(function(){

    initGTM();
    //Impression On Scroll
    onLoadImpressionsOnScroll();
    //GTM for scrolling promo/product
    $(window).on('scroll', function () {
        onScrollGTM();
    });
    //GTM for clicking product
    $('.nespresso-product-item').on('click',function () {

        productClickGTM($(this));
    });
    //GTM for clicking promo banner
    $('.promo-banner').on('click',function () {

        promoClickGTM($(this));
    });
    //Dropdown
    $('#JumpMenu li.menu-item').mouseover(function() {
      var menu = $(this).data('menu');
      if (typeof menu != 'undefined') {
        var nav_banner = $('.' + menu + '-nav-banner');

        // if (!gtm_dropdown_promos.includes(nav_banner.data('promo-id'))) {
        if (!gtm_dropdown_promos.includes(nav_banner.data('promo-name'))) {
            gtmDataObject.push({
                'event': 'impression',
                'eventAction': 'Promo Impression - On Menu Dropdown',
                'ecommerce': {
                    promoView: {
                        promotions: [
                            {
                                // id: nav_banner.data('promo-id'),
                                id:nav_banner.data('promo-name'),
                                name: nav_banner.data('promo-name'),
                                creative: nav_banner.data('promo-creative'),
                                position: nav_banner.data('promo-position')
                            }
                        ]
                    }
                },
            });
        }
        // gtm_dropdown_promos.push(nav_banner.data('promo-id'));
        gtm_dropdown_promos.push(nav_banner.data('promo-name'));

      }

    });

});

function initGTM(){

    if (typeof withinviewport === 'function' && typeof generateMinibasket === 'function') {
        setTimeout(function(){
            //GTM for on load promo/product
            onLoadGTM();
        },1000);
    } else {
        setTimeout(function(){ //waiting during the initialize
            initGTM();
        },1000);
    }

}

function onLoadGTM() {
    //get the product that are being displayed
    if($('.nespresso-product-item').is(':within-viewport')) {
        var onload_product = [];
        $.each($('.nespresso-product-item').withinviewport(),function (index, value) {
            if (!gtm_products.includes(value.dataset.id)) {
                var category = value.dataset.type ? value.dataset.type.toLowerCase() : '';
                gtm_products.push(value.dataset.id);
                onload_product.push({
                    name: value.dataset.name,
                    id: value.dataset.id,
                    dimension54: value.dataset.name,
                    dimension53: value.dataset.sku,
                    price: value.dataset.price,
                    category: category,
                    dimension56: 'VirtuoLine',
                    brand: 'Nespresso',
                    dimension57: 'Simple Product - ' + value.dataset.type,
                    list: value.dataset.list,
                    position: value.dataset.position
                });
            }
        });
        // console.log(onload_product);
        gtmDataObject.push({
            'event': 'impression',
            'eventAction': 'Product Impression - On Load',
            'ecommerce': {
                impressions: onload_product
            },
        });
    }
    //get the promo that are being displayed
    if($('.nespress-promo-banner').is(':within-viewport')) {
        var onload_promo = [];
        $.each($('.nespress-promo-banner').withinviewport(),function (index, value) {
            gtm_promos.push(value.dataset.promoId);
            onload_promo.push({
                // id: value.dataset.promoId,
                id: value.dataset.promoName,
                name: value.dataset.promoName,
                creative: value.dataset.promoCreative,
                position: value.dataset.promoPosition
            });
        });
        gtmDataObject.push({
            'event': 'impression',
            'eventAction': 'Promo Impression - On Load',
            'ecommerce': {
                impressions: onload_promo
            },
        });
    }
}


function onScrollGTM () {
    //promo
    var scrollload_promo = [];
    if($('.nespress-promo-banner').is(':within-viewport')) {
        $.each($('.nespress-promo-banner').withinviewport(),function (index, value) {
            if (!gtm_promos.includes(value.dataset.promoId)) {
                gtm_promos.push(value.dataset.promoId);
                scrollload_promo.push({
                    // id: value.dataset.promoId,
                    id: value.dataset.promoName,
                    name: value.dataset.promoName,
                    creative: value.dataset.promoCreative,
                    position: value.dataset.promoPosition
                });
            }

        });
        if (scrollload_promo.length > 0) {
            gtmDataObject.push({
                'event': 'impression',
                'eventAction': 'Promo Impression - On Scroll',
                'ecommerce': {
                    'promoView': {
                        promotions: scrollload_promo
                    }
                }
            });
        }
    }
    //product
    var scrollload_product = [];
    if($('.nespresso-product-item').is(':within-viewport')) {
        $.each($('.nespresso-product-item').withinviewport(),function (index, value) {
            if (!gtm_products.includes(value.dataset.id)) {
                var category = value.dataset.type ? value.dataset.type.toLowerCase() : '';
                gtm_products.push(value.dataset.id);
                scrollload_product.push({
                    name: value.dataset.name,
                    id: value.dataset.id,
                    dimension54: value.dataset.name,
                    dimension53: value.dataset.sku,
                    price: value.dataset.price,
                    category: category,
                    dimension56: 'VirtuoLine',
                    brand: 'Nespresso',
                    dimension57: 'Simple Product - ' + value.dataset.type,
                    list: value.dataset.list,
                    position: value.dataset.position,
                    dimension55: value.dataset.range ? value.dataset.range : '',
                    price: value.dataset.price,
                });
            }

        });
        if (scrollload_product.length > 0) {
            gtmDataObject.push({
                'event': 'impression',
                'eventAction': 'Product Impression - On Scroll',
                'ecommerce': {
                    impressions: scrollload_product
                },
            });
        }
    }
}

function promoClickGTM (product) {
    gtmDataObject.push({
        'landscape': "Wordpress",
        'segmentBusiness': 'B2C',
        'event': 'promoClick',
        'currencyCode': 'PHP',
        'ecommerce': {
            promoClick: {
                promotions: [
                    {
                        // id: product.data('promo-id'),
                        id:nav_banner.data('promo-name'),
                        name: product.data('promo-name'),
                        creative: product.data('promo-creative'),
                        position: product.data('promo-position')
                    }
                ]
            }
        }
    });
}

function productClickGTM (product) {
    gtmDataObject.push({
        'landscape': "Wordpress",
        'segmentBusiness': 'B2C',
        'event': 'productClick',
        'currencyCode': 'PHP',
        'ecommerce': {
            click: {
                actionField: {
                    action: 'Click',
                    list: product.data('list')
                },
                products: [
                    {
                        brand: 'Nespresso',
                        category: product.data('type').toLowerCase(),
                        name: product.data('name'),
                        id: product.data('id'),
                        price: product.data('price'),
                        dimension54: product.data('name'),
                        dimension55: 'espresso',
                        dimension53: product.data('sku'),
                        dimension57: 'Simple Product - ' + product.data('type'),
                        dimension56: product.data('technology'),
                        position: product.data('position')
                    }
                ]
            }
        }
    });
}

function promoSlider (index) {
    var promo = $('.promo-slider-'+index);
    gtmDataObject.push({
        'landscape': "Wordpress",
        'segmentBusiness': 'B2C',
        'event': 'promoSliderChange',
        'currencyCode': 'PHP',
        'ecommerce': {
            promoView: {
                promotions: [
                    {
                        // id: promo.data('promo-id'),
                        id:promo.data('promo-name'),
                        name: promo.data('promo-name'),
                        creative: promo.data('promo-creative'),
                        position: promo.data('promo-position')
                    }
                ]
            }
        }
    });
}

function onLoadImpressionsOnScroll() {
    var counter = 1;
    $.each($('.nespresso-product-item'),function (index, value) {
        if (!page_products.includes(value.dataset.id)) {

            if (counter == 1) {
                impression_index = value.dataset.id  + ' ';
            } else {
                impression_index = value.dataset.id;
            }
            var category = value.dataset.type ? value.dataset.type.toLowerCase() : '';
            impressionsOnScroll[impression_index] = {
                name: value.dataset.name,
                id: value.dataset.id,
                dimension54: value.dataset.name,
                dimension53: value.dataset.sku,
                price: value.dataset.price,
                category: category,
                dimension56: 'VirtuoLine',
                dimension55: value.dataset.range ? value.dataset.range : '',
                price: value.dataset.price,
                brand: 'Nespresso',
                dimension57: 'Simple Product - ' + value.dataset.type,
                list: value.dataset.list,
                position: value.dataset.position
            };
            page_products.push(value.dataset.id);
        }
        counter++;
    });
}
