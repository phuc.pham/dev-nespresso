
/**
 * faq.js
 */

$(document).ready(function() {
	Faq.init();
});

/**
 * Faq class
 * @type {Object}
 */
var Faq = {

	that: null,

	init : function() {

		this.el = $('#faq');

		this.btn_show_modal = this.el.find('.btn-show-modal');

		that = this;

		this.ready();
	}, // init()

	ready : function() {

		this.btn_show_modal.on('click', function() {
			console.log($(this).siblings('.faq-content-item').html());
			NespressoModal.content = $(this).siblings('.faq-content-item').html();
			NespressoModal.show();
		});

	}, // ready()

}