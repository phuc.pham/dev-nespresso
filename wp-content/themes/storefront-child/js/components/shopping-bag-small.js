$(document).ready(function(){
    initLoading();
});
function initLoading(){
    if(!Products.loader){
        generateSmallShoppingBag();
    }else{
        setTimeout(function(){ //waiting during the api call
            initLoading();
        },1000);
    }
}
function generateSmallShoppingBag(){
    var el = $(".sbag-small");
    el.empty();
    el.removeClass('sbag-loading');
    
    if(Object.keys(Products.list).length>0){
        var machines = 0, accessoires = 0;
        
        //add total line 
        $('.bloc-total').removeClass('hidden');
        var subtotal = $('.bloc-total').find('.bloc-total-stotal .price-int');
        var total = $('.bloc-total').find('.bloc-total-total .price-int');
        var tax = $('.bloc-total').find('.bloc-total-vat .price-int');
        //reinit
        subtotal.text(0);
        total.text(0);
        tax.text(0);
        
        //for capsules
        var headerCapsule = false;
        $.each(Products.list,function(index,product){
            if(product.type==='capsules'){
                if(!headerCapsule){
                    headerCapsule = true;
                    el.append('<div class="bloc-title">CAPSULES <sup>(<span class="sbag-small-nb-capsules">0</span>)</sup></div><ul class="sbag-small-capsules"></ul>');
                }
                el.find('.sbag-small-capsules').loadTemplate("components/generics/product-small-line.php?id="+product.id,{
                    defaultValue:null
                },{
                    append:true,
                    overwriteCache: true,
                    success: function(){
                        var nbTotal = parseInt(el.find('.sbag-small-nb-capsules').text());
                        el.find('.sbag-small-nb-capsules').text(nbTotal+1);
                        
                        var oldPrice = parseFloat(subtotal.text());
                        var newPrice = oldPrice + (product.price*product.qty);
                        
                        var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(2);
                        tax.text(vatTax);
                        
                        subtotal.text(parseFloat(newPrice).toFixed(2));
                        
                        total.text(parseFloat(newPrice*VAT).toFixed(2));
                    },
                    error: function(){
                        console.log('Erreur durant le chargement du Tpl');
                    }
                });
            }else if(product.type==='machines'){
                machines++;
            }else if(product.type==='accessories'){
                accessoires++;
            }
        });

        if(machines > 0){ //quick fix 
            var headerMachine = false;
            $.each(Products.list,function(index,product){
                if(product.type==='machines'){
                    if(!headerMachine){
                        headerMachine = true;
                        el.append('<div class="bloc-title">MACHINES <sup>(<span class="sbag-small-nb-machines">0</span>)</sup></div><ul class="sbag-small-machines"></ul>');
                    }
                    el.find('.sbag-small-machines').loadTemplate("components/generics/product-small-line.php?id="+product.id,{
                        defaultValue:null
                    },{
                        append:true,
                        overwriteCache: true,
                        success: function(){   
                            var nbTotal = parseInt(el.find('.sbag-small-nb-machines').text());
                            el.find('.sbag-small-nb-machines').text(nbTotal+1);
                        
                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.qty);

                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(2);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(2));

                            total.text(parseFloat(newPrice*VAT).toFixed(2));
                        },
                        error: function(){
                            console.log('Erreur durant le chargement du Tpl');
                        }
                    });
                }
            });
        }

        if(accessoires > 0){//quick fix
            var headerAccessories = false;
            $.each(Products.list,function(index,product){
                if(product.type==='accessories'){
                    if(!headerAccessories){
                        headerAccessories = true;
                        el.append('<div class="bloc-title">Accessories <sup>(<span class="sbag-small-nb-accessories">0</span>)</sup></div><ul class="sbag-small-accessories"></ul>');
                    }
                    el.find('.sbag-small-accessories').loadTemplate("components/generics/product-small-line.php?id="+product.id,{
                        defaultValue:null
                    },{
                        append:true,
                        overwriteCache: true,
                        success: function(){
                            var nbTotal = parseInt(el.find('.sbag-small-nb-accessories').text());
                            el.find('.sbag-small-nb-accessories').text(nbTotal+1);
                        
                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.qty);

                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(2);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(2));

                            total.text(parseFloat(newPrice*VAT).toFixed(2));
                        },
                        error: function(){
                            console.log('Erreur durant le chargement du Tpl');
                        }
                    });
                }
            });
        }
    }else{
        el.html('<span class="sbag-no-product">Please select at least one product</span>');
    }
}