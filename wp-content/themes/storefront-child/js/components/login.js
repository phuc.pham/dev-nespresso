
/**
 * login.js
 */

(function($) {

	var formLogin = $('form[name="login-form"]');

	formLogin.on('submit', function(e) {

		return true;

		// var loginButtonHtml = formLogin.find('[type="submit"]').html();
		// formLogin.find('[type="submit"]').attr('disabled', 'disabled').html('LOGGING IN');

		var remember = (typeof formLogin.find('[name="rememberme"]:checked').val() !== 'undefined') ? true : false;
		
		$.ajax({
	        type: 'post',
	        dataType: 'json',
	        url: ajaxurl,
	        data: {
	         	action: 'nespresso_customer_login',
	            user_login: formLogin.find('[name="username"]').val(),
	            user_password: formLogin.find('[name="password"]').val(),
	            remember: remember
	        },
	        error: function (jqXHR, textStatus, errorThrown) {
	            // console.log(errorThrown);
	        },
	        success: function (data) {
	           if ( typeof data.error !== 'undefined') {
	           		// formLogin.find('[type="submit"]').attr('disabled', '').html(loginButtonHtml);
	           		sweetAlert('Error', data.error, 'error');
	           }
	           console.log(data);
	           // window.location = login_url;
	        }
	    });

		
		return false;
	});

})(jQuery);