
/**
 * checkout.js
 */
$.prototype.scrollto = function(time=700){
	$('html, body').animate({ scrollTop: $(this).offset().top - 80 }, time);
	return $(this);
};
$(document).ready(function() {
	Checkout.init();
	$('#otheraddress').change(function(){
		if($(this).is(':checked'))
		{
			$('#other_address').show();
		}else
		{
			$('#other_address').hide();
		}
	})
	$('#sredinvoice').change(function(){
		if($(this).is(':checked'))
		{
			$('#redinvoice').show();
		}else
		{
			$('#redinvoice').hide();
		}
	})
	$('#editaddress').click(function(){
		var _that = $(this), _action = _that.data('action');
		if(_action !='true')
		{
			_that.data('action','true');
			_that.text('Cancel');
			if($('.disabled input,.disabled select').length>0)
			{
				$('.disabled input,.disabled select').prop('readonly',false).removeAttr('readonly');			
			}else
			{
				$('.disabled input,.disabled select').prop('readonly',true).attr('readonly','readonly');
			}
			if($('input[value="pick_up"]').is(':checked'))
			{
				var _options  = '';
				$('#pick_up_location>option').each(function(){
					var _v = $(this).attr('value'),_t = $(this).html(),
					_vs = _v.split(' - '),_img = _t.substring(_t.indexOf('|*|')+3),_img = _img?_img:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAACeCAYAAADDhbN7AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4gwDBCkzvIIE5wAAIABJREFUeNrtXXVYlNkXficJQcVAsSilFewEu7tzbV3FtdautVsxaGbA2nD35+6qu66tpAxh0SgMYrcikhPn9wfMCFKfSup3nuc8Ps/MnfGb73t57z3vPedcgLUvsmh/N/YmsFb2dv3CfgBAfKDHpkhfl27sHWGt3EAXF+gxPVEiokSJSBkT4NaFZb5PNw57C5iDrnWfRYgL9Fgo4PP2p6VnorquNhQKJdIysxxadPshgL1LrJUV0y2Qhojp9mVnali/NnVua0V3r3kqE4K9sqL9WeZjrQwsLtBjZqJERLcvO5O2tiYBIADk0MGGHlw/RIkSEUX6ubLgY60UmS7IY6U0REzRfm7UpKG+GnQq79zWipJCxZQQ7JUZ6etqz9451kqD6SYnSkR0/fw+0qmmVQB0eZkvN+CgaH83e5b5WPsSplsnDRFTXIA7GdSrlQ9ols0aE4/L/Yj5LOnhjcOUEOwli/B1cWDvJGufw3RTpCFiCjvrRLX1dPMBrFGDOnTj4gG6/L+tpJNnvQeA7NtbU3yQpzJRIqKYAHcHlvlY+xSmW50oEVFcgDvVqpkfdCaG9SnG340eXD9ESaHe5PfX9kLWfJb05PZRSpSIKOKqS1f2zrLGRDL5XhoipvDz+wpMr4aN9Cn0rBMlh/nQyIGdacPSCXQ/3IfO/bqRtDQ1CgQccYEeylyRmWU+1oplusWJEhFF+7pSNW2tAqCLD/Sgx7eO0JC+HdSv79s4k5LDfSj43z3E4XAKgO/xrSM5zMeu+VgrAnRLcsThg9S4Qd3806tRfQo960SPbx2lMUPsC0yt6xaPo/vhPnTm2Hri83j53uvUxpLuBHkqE4K9sqP9WeZjLX8gMTtRIqI71zwLSCaNG9ShWH93uhfmTUP6tC9STnHe+j0lh/uQ5Mxe0hAK8r03oGdrNfNF+bmy4APAZZnOY61AwPPk83jQ1tSA5y5H8Hg5t6WZcQOcOrwWdWpVx9KNh3D6QkiR3zd/jSe8jp2DQT09/H1ojfp1bS0NLJkzHNkyObQ0hdAUCi5G+rl2tXZwZP/iv2GmmyYNEVHYOSfS1BSSj9MCuhfmTa7b51DjBnUo4oozJUpENLRfhyKZ7mM/sHk23Q/3oaDTu0m/Tk0K+W8v3Q/3ofU/jieH9jaUEKwWmbuyzPdtMt0GaYiYYgPcqV5dPQJAHA6HRHvm0+NbRyjiijO9jPmVhg/oxBh0Kv/px3GUHO5Dsf7ulBzuQ7Mn9VW/16G1Ra7ILJJHsPl83xzTTZeGiCn0v735dDoOJ+dfn30L6V6YD434DNCpfOfaqfTg+iHasHRCoXu78TlSC8UEuHdjme8bWdMlSkQU6+9GNavrFAoaLpdDndpYfjboVOzZ3NKoyPc7trbIK7V0Z5/Q1wy6QI950hAxhZ/bR/X19b4IWKXhHVtbUFyAuzIhh/m6s8z3lep0iRIRRV51IW0tjQoHnRp8bSzp0c0c5mNrOL4+0K2Qhojp1qWD1MigTqUBncrbtzLPKzKzzPeVBBJzEyUiunHxAGlqCEsEQVmwoVDAL3FM57ZWlBzmrRKZWfBVZaaLz5VMonxdqWH92iU+/B5dbCn8/D7q3M6q1EA3ZUwPCji5g4yb1CuZ+VqaqzKZsyNzwfe12le5c5FbDTZTwOetf/kqBe0HLMGjp69K/Fx1XS1oCgXQ0dYstWupWUMHNarrQEtTo8SxITfjMWmeE4gg0NYQXonxd+vBMl/VYrrN0hAxxfi7Ud3aNUpkmk5tLcm+vTUBKJNot7quNgGgzcsnUkODkpm3XUszepAjMisifF16sE+2aqzpZktDxCQ5s5f0augwAsb18/vpye0jpF+nZpkFEC2sjCk53Id+c19WIH2qOKklV2TuyTJf5Wa69VKJiGL83NQsU5wL+DkpTHbWJmTZrHGZR68zJ/RRp9F/nD5V1Jovj8jck33SlVMyWaiqkVDtvZY0vd4J8qSxQ+3LXT7ZvmYKhZ51otq1qjMCX+wHkfmrYb4qH1y07rMIcUEeywQ83v73aRmwH74Sz168KfFz8QkPQUSIufOg3K85+cEzvHufjlev3zEKOKYtPMDRFAqgIeBfivR17WHt4MhOu5WA6dYkhYjp1qUD1ICBZNLT3pbWLBpDAEhDQ1BhorEqWXTn2qnU4KPajsK8rZ0Z3QnypIRgL1m0vxu75qvgQGKeqtj646zfwpzP59HF3zdTXKAHNTNuUOE7FgN6tqGHNw7TycNrGe/tJoXmisws+CoskNiSIw67kIF+yYxh2Cin9USD+rWpawebSrNd1tvBjvh8HvF5XEYic1vbZpQUIqaEYC9ZpJ8rG3CUM9N9nyOZ7CFtLc0SH5Z+nZr05PZR2rxiUqXbp1Ux8emj6yj8nBOjLbuOrS3oTpAn5Ugtbr2qIvNxqyDTbRMK+B6ZmdkYOGkT0jMyS/xs6vt0BIXF4q9/r1XK36aQK3D2cjhux9xDVrasxPHB1+MwwXEPhEI+hALBhQhfl15sDUfZMp2jNERMwf/uoZoMxOF2Lc3o4JbZJBQKKiXTFea19XTJeescRlk07VuaU6y/WmTuza75yobpNkpzi62L69qU1/8Ur6LkMG8ybFyvygBv8uge9Db+ODlOGcBofFvbZup8vghfl94sYkpXMlmSFCKm0LNOjLa1una0oWq5zXS+NIUdFZSfB4DMTBsyyqppa9uMYv3VIjPLfKUyvQZ5rMjpxHmQkWRi2Fif4gI96Myx9SRgkAdXWZ3D5VD4+X0U4+/GaIejXUuzvJ1JWfB94fS6PilUTDcvHijQQKcoFwj4tHf9DOrZxbbKgk7ljlMH0NSxvRiPb92iaV6RmQXfZzLdgkRJTrE1k+DAzLQhnT++iYT88mE5Ho9bbgA0NaxPkVddqF7dmoymaWmImKQ5ReOVFnzcSsp0O4R8/oH0jCwM+m4TshlIDJ3aWKB1C1MYG9Yvl2sdO9QBtfV0y+X/6tzOCvq1a6Bxg7oljg25EY9RM3cAHA40BPz/Iv1ce7NSyydJJrtJi4GYamttrN7++riJYpmtvzigI86L6cfZw8qN9azMmhAAsjZvwijAat/SnOIDPXJFZve+7LRbPNPtlIaIKdrXlfRqlqzTWTRrRHeCPElyZm+5rr0EAj5Jzuwhv7+2E5fLKbf/16ixPj2+dZTuBHkQOCWPb9W8qTrgiPB16VOZnnmlOdknLshjgZDPP/D0+RsMmLgBb9+lFTu+tp4uXr1JhePUgUhMeozzfjfL7Np0qmmhpY0xsrLlICLo164Bz90/ICU1DUs3+uDVm9Scm8kBwm4lQKlUltm1TBnTE1Fx93A9IhG1aurg9dv3xY5va9sMRw4uJg0NASdLJu9vZT/3XLS/G77p6TdfjYS62LrkvdeWzU3pTpAXNbcwLMcIcyAlSkR095oXJQR7qY8WuBvsRXevedHDG4eoX4/W5XY9Q/q2p1h/N0Y6X+sWTenhzcOqovG+33RwoTobLD7IY4VQwF/74vU79Bi1htHeq4G+HjgcoH69WuV2vW6Hz2D6ogN4+vwNOJwPEwWfy0Xs3QfoPGQ5zl25Xm7XU69uTRA4qFOresn3OiIBQyZvQVa2jLQ0Nc7FBLj3+6bXfPFBHqsSJSK6efEAo6LnpsYN1I1w+HxehaUxqXLiEiUieh71c4W1xBDkSkfD+3cgw0Z1Ge1w3A/3URWNV2jAwa0IplNNr0IBf1tKajr6T9iAbJm82M8J+DxcPL4Jpw+vBY/Hg1yuqJAb1r1zCxARXrx+Bx6Pi6xsGSaPrpgKRJlcDltrY/jsW4StqyaXOD7s9l0MnboVcrkCmkLBmWh/t77fVBp9XJDHIqlERCH/7WW0raWqzDI3bUh21iYVupsQcHIHnfl5PWlpCslAX4/8/95Bp4+sK9fo9mP/bnR3atygDnG5HPUedXHezs6MEiUilcj8dU+7eZhutzRETBFXnBkVW/fv0Zqi/dxo/HCHCt/CsjJrTLMm9iVOnte0NDXIcepARuWUZe07106l6+f3MUrrt7MxIWlOJrM80s+139fOdAukIWIK+mdXgQNJioxgbUwo9L+91L1ziwp/sFwut1Lv7c7+ri9d+mML1WD4R9DW1oziAtxVInP/r4r58jDdHmmImKKuulCN6tVKvCkOHWxowczBBIB0GebffevOydOZ6je3ZQXO6yiU+axN6L4qq8XXpf/XFr3+KA0RU9DpXYxAB4Au/r6FHt48TCZN6rOg+oxOow9uHCK3HXPVfZ1L0vmi/VyViRIRxQa4D6jSzJe3RkIqEVHEFWfS1Cy5P111XW3icjjUyKAOo6or1gv3EQM6Ejc3g4bJH3tLG1N6eEMlMrv2r+qgW5sUKqaQM3uZdW1qY0nJYT60asEYFjylUb3G49EJ0Sq6ffkgo2jXztqEov3clAnBIoqpqswXH+SxJlEiousX9hOfQY6cSo64emIbWZVDA51vxaeP70Weu+blu8clTbvJYWqRuWoEHHmYbntSqJhuXNjPKIVnQM82JN47v0qnqldm53I4pKUppF/clqoL24tzWytjig/0oIRgL3m0v1vVYL74II8l0hARSc7sYcR0PC6Xjnsso/hAD7I2b8ICpYx87FB7Sg73IdftcxmNb9OiGSUEe5E0RESVFnx5mG5fUq44zKRApWXznF0IbW0NsrU2ZgFSxt7Cykjd9dTctCGjZpK5IrMi0s91QGVlusXSEDEFntrJKHqtpadLj24eoROilSwoytG1NIUkObOHQs86kRaD59S6RVOK9VeLzIMqBfPlZTppiJgir7qQro42412A3T9NJ3PTRiwgytlnTexDyxxHMNL4VMynymqJ9HUdWFmYbnlSaA7T1dAtWS/q3rkF/e2zhtFU/KXOK2GLS7OI/nhFPRAtLWGZXg+TumEej8sYMCW5tbkhnTy0hkwN6zPS+aJ81SLz4C9lPu7nMt3iWcMRH+SxQ8Dj7XyXmo6eo9ciJTWtxM8O6NkGrZqboHo1rTL9g+BwODh5eG2xY5y3zSn0GAAiIPjfvdixemq+10+IVqGhQe1Cv+u4x3Icc1kCLrfoW/rPsZ+KvZ7/iVaCz+cV+b6ujjaOHFicLxH1S6y1rSm6tLNCk8b6JY69GZWIifP2coQCPoQC/ulIP9eBX5I+z/0c0LXuswhzpvbdIBTwVzx/lYKeo9eW2OVoUO+2MG5SD8s2+aDfhA1IevCsTIHXsrkpmlsaYsyQLkWOMWqkj/PHN8GwUcEbb2ZqgAb182c48/l8nD6yDlZmTQp+V2N9GDXSL7KIxc7GBJbNGmPCiK5FXk9bu2b4zX0ZdHUK/lE2NTKA/8nt0NbSyOGgUrCjf1xBp8HLcDUwAvYdrNGkYfHlk7ejpRj43SZkZmaTllD4b8wXMN8nAy83XX0dn8db/+p1KjoPWYGXJfTyrVdXD3vXz8Ax5x+hpSlE7N2y7zu8esFoKJWEyaN7FMlCCoUCDevXwsnDa1BbL38KuVJJICr4hGvV0MG/x9ahhZVx/vFEUFLRiFjuOBJEhEkju4PLLRyecrkCrZqb4m+fNdDUFKpftzJvAr+/d6C6jnah1/Qldkf6GFpaGji0byHO/bYRNapXK3Z8REwSJv3gxOHzudAQ8E9H5TLfp4KP+ylMl7um2y3g8zcBwNkr16FQlJwJ/PzFG+wXncbKrUeQkZld5qDr2tEG9u2t8DYlDS1tTNCxjUWRY5VKJerUqgHfv7ahjW2zEr87IysbXC4Xf3gux6DebRldT8fWFuhpb4u379JgZ22Mrh2bFzn2bUoarMwa49Lvm1G/rh4cOtjgT/EqKJRKyMoo6zojIwubnI5j1hJnpLxLg6CY6V417cYnPAKPx4WWhvB0tL/boE8FH/cTmW65gM9feu/BMzx9/gazv+uHNQvHFPkZGwtDRPu6on69WnA/8h8CQ2PKHHQ8Hher5o/Gy9fv0H3kamRmy+G2Y25RK0HIFUrMXuoCTU0h/vBageaWRsV+//u0THQatAzZMjkObvkevexti7/BXC7W/TgOr9+kwmHYCqSlZ8Jl2/dFjo25+wCOq9xRX18PF37fjOMey0BEGD9nl7qMsizs6P+uICgsFr0dWiIu0AMGRRRSCQR8/OK6FOZNG0JyPR4ZmdlcLQ3BPzH+boM/BXzcT2C6g0I+f2fq+3SMnLEd/SdsQGTsPcyY0KdI8NWoXg01a1RDdR0tlJc1NW4Ai6aNcMHvFlJS07B1/+/QraaFYf07Fg5ULhdnr4RjysL94PO4+FO8Cp3aWhY5pfG4XDx+9hoDJm5EdrYMor3zMXti3yLXXSaG9WHZrBEu+N3E+7RMbDvwP2hpamDUoM6FjufzeTh5VoLpiw9Cr0Y1vE/LxMBJG3ErOgk8btmXQevXqQ6BgA8NoaDQ93/3XI6unWwQFBqLCXN3Y9ycXSAChALBqUg/10FMAw4uQ6ZbKuDz5z948hLtByzFm5T3ePc+HUOmbMabt+8xf8ZgrF5QEHxBoTEw7TAb8YmPyg14TuunAxygZ5cWuH3ZGYtmDQUAzJxQfM/Ca6ExmLxgHxRKJX5zX4b3acWXWT588hIdBi7Fw8cvsXrRGOjXqVHouF3rpoLD4aB77vUsnjMs93qKL+z3l0Rh5lJndBi0FEn3n4HHLZ+6rF/+8kObvotxr5DgT7x3PlramOBKYAQmztsDJRGi4pIxauYOZGXLoK0h/Cc2wH0ok4CDy4TpBHz+7vdpGeg/YQMysz6s0UyNDKBXUwdcDgchN+4U+j15x5e19enaErbWJnj+IgXPX6bg5esUPH/5Fi9epaB1i6bo061VsZ/3vRaJPmPXIfV9BqP/7+27NPQYtRZJ958VGsB079Qc7Vqa4fnLFDx/8Tbnel68xfOXKbC1NsbgPu2K/f7zV2/gdRlOr0WuyV++LfT16DsPwOFwYG7aENw8ks7NqESM+34Xh8/nQSjgn4z0dR38RZ0K4oM8VieFiing5M4CxSxWZk0o4oozJUpE1Ltry0pQD8Ghvw+tobgA90Jz/6J8XenW5YP56ncvHN9ECRKvQnuyhJ51omPOS/JnRf+xhW5ePFjINpQG/eWzmiRn9qhFYi6XQ396r6K4QA/Sr1PweiKuOFPEFed8h7w8vX2UfvdcUXiCrI42hZ51or991hCXU3EVbUvnDqf7130o6PSuAocU2loZU+RVF5XIXCzzcQtjuqt/bkN8kMcuAY+39W1KGnqMXoN3qenqMdYWhvjvl/WoU6s6Zi5xwcUy7FvC1PRq6ELA5+Ov/yR48SqlwPv7vE4i8d7TfMFDzN2HuBkhLTA27u5D9B67Ds9e5P/Lj73zABGxSQWjwswsjJi+DZcDI9SvVdfVhlAgwKlzIXj+svDruZv0BM0tDNWvhd1OwB3p48KlH6USkbHJiE98BKrA+7zH/W94HD2Hhga1cfnEVgj4/A86X0wSJjru5QhUzOfHkPnyHl6SFJrTKkxV06ry5pZGdPuyMz24foj6dGv1dRTJcD7/SPeyvobK6j9MH0T3w33I96/tBZqh21gYUpSvqyqTmdGaD/FBHusTJSIK/W9vgXI+K7PGlBQqpnth3tTL3o7dcP/GfePSCZQc5kO3LztTtY+aLdlZ59TtSkNEFOXvNqRQ8OXLpwsVU/j5fVTno018W2sTivJ1pacRx6jvV8J0rH+5z5ncn5LDcphPKBB8lITQhOICPCgh2EsRXRT44oM8Vqny6XgfHeJradZY3Z6rJ8t0rBfGfOE+dOvSwQIBh521CcUHepA0REQxAe7D8oEvPsjDTRoipluXDhY4MaelTc4Hn0f9XCmiV9Yrp8+dOoCSQr0p8PSuAulfVmZNKCHYixKCRRTl5zoEADix/m62QqHgFo/LpZ5j1nIS7z1Rs6CmhgAXft8Mw4b6OHriCtbt/LnCoqm2dmbo170lGjesiyfP3uD81Ru4Fh5b6FhTIwP0drBDCysjpGdkw/daJK4GRSAtPb8oPHl0D9SsUQ0Hxf8U+I45k/tDJlfA+9cL6temjOmJ2rV0kXdTQ6lUIu7uQ/hei0BW9oeOV0aN9WFt1gRnLocX+ZvsrE2gqSmE5HocAEBXRwtTxvSEQMAvuLnH4eCXP6/i2Yu36OVgB1trYyiVRce3fB4PnkfP4t379HJ7RgEnd8LEqD78JdEYP2dXvvfmzxiMFfNGIitbhnfv0g04ABDr7zZdQ0PgLZMp0H/CBkjvP1V/oGb1agg+swcaQgF2uf4Jj6Nnyx104r3z0aNLCwgEfHC5XCiVhKysbASGxGD6jwfyPYAhfdphz/oZ0NAQgM/jgUCQZcvx7OVbOAxfiew84Dj/20YYNtKHhX3Bvdzw8/uQkZEF+2Er1a9dOL4Z5qYN8uXMEQC5TIHnr1LQceBSKHLb0Joa1se1f3dj8Xpv/PqXb4Hv7+1gB7cdjvhhtQfO+94AABjUq4WLxzdBp5oWeB9t1PO4XPSfsB63Y5Kwc900TB3dA9lyBTgA5AoFFAolhEI+OLnNkbU0BbDruQAPn7wql2e0ZeVkjB9mj1vRUoyetSPfM+nYxhKH9i+EgM9VZGZmW1p3++EuYnPn2/hAj9mJEhGFnXUi44/O/mpQrxZdP7+fksN9aNLIbuVK4YtmDaH71w/R4QOLyKJpI+JwOWRqWJ88d8+j6eN7EyePmNqhtTm9iP6F/P7aTn262pFQwKfaerq0aNZQkoaI6do/u/Nl+Z77bSPF+rsX+v+Gn9tH/id35Hvt/PHNFHBqJ+nqaJNeTV3Sq6lDdWrVoHlTB5I0REyrFozOdzbFo5tH6F6oN00d0zPf9wzr10Hd2DFvoGZQrxZFXnWhQ/sXkamRAZka1v/gRgbqa69buwY1zX2/cYM6tPunafQq5leyMmtMJrnjmxoZlFvzym2rptCrmF/p5KE1BQrHeznYqep0FXEB7j0A4NalA+Bb5gp85l3meMX6u8lq6en6nD++Cb3HrkPyw+cAgMfPXsN+2AqEnnXCpuWTwOPxcOSPy+XylzRjQh+8ep2KGT8ehEKRwyaJyU/x/TLXjzI7OFgwYzCevXiLifP24uGTlwCAV29SsV90CkqlEgtnDUHXjja4wFDwLizTV6lUIvWj6ev4KX/MmtQXpkYG4HA4HxIMOIBcocTG5RPRqEFtbNn/B8YNc8DWld+VkAGTAWny0yITFV68Ssknkr99lwYej4uk+8/KJe0sr61fMgHjhtkjMCwGw6Ztzfdey+amcN8xFzK5PDsrK7u5dbcf7ty6dAB2vRZ+2LmI9XeDpYPjIblc8T2Px8Wf3qvQ1MhA/SXpGVnoP2E93qdlYPPyiZg9qexbqhk1rocmDevC9dC/UCiU4PG4mDGhN5Y5jsAyxxFYvXAMdKpp5qZD8WBiWB9Pn79Rgy6vnTwngYDPQ7tW5oz/f6Yp5j26tEA1bQ08ffYmH1g0NQRw8jyJmPj7mP1df4j2zMfe9dPx7MVb7HA5UWSCahvbZhDtmQ/x3hw/cnAxendticpmu9dNxw/TBuJ6RALGz9md773+PdrgL+9V4HK5kMvlvfKCLt+WWV7my86WTderoYN/jv6ULx36weOX6DBgKV6+TsWq+aMwdqh9mf4wbS0hiAjv0tJzWY2LEQM6Ydq4XpgxvjcWzBgMHW0tFbmAX0yLWlWrWw1hnoU7EYrCFofDgayY9rhjhnTB3WueeHTrCPaunwElEQ6IT+X/DnDw5m0q+k/cgKjYe+hpb4vYuw/RafAyPHj0osg0+Vp6OmjbshnatjRD25ZmaN/KHPXq1qxUoFu7aBzGDrNHyM07GPv9LqRnZOULmly2fQ+FQpmRlZ1ta2HvGJAXdIXu1X5gPvksDQ0BTh9Zh2bGDT4wX2YWBk/ehIysbOxaNw3zZwwusx8Xe/cB3qWmY8a4nBQiuVyBmUuc0WfsT/hhjSe08qSHK5RKPH72GvXq1kQNXe2CmSKdW0CuUOJWdFKe1KZXqF5dO6eOIY9pCAUwqKeHJ89eF3ltf5wOxG9/+0MhV+J2zD10GbICL14VXQIwfPo2+Px2EcM/mo4Kz5KJQveRq9F9RI53HrwMJ/4JqjSg27N+Bhyn9se1sFiMmrkt37ke/Xu0xgnxKgCATCZ3sHKYF/Ex6AoFnqWDI2L93WDeZa44Kyt7hk41LfzpvSof8z16+grt+y/BsxdvseT7YUUmNX6pEQF//ReMZiYNMGNCH2hqCPDk2Ws8f/UWxk3qQUmk3jBXKJT4+2ww9OvUxOYVk6Cro61e+5mZNsTqBaORlp4J/+Ao9ff/djIAWVlybF89RT1la2tpYOfaqciWyXH0jyvFXt/Wg38g6f5TNDU2KADejy0rW4Yt+39HalrJKVdyuQJpaZl4n57jqWmZICLweBV/9NxPi8dj0shuuBElxeT5TsjKkuWTvNx3OgKgzGyZrIOlg2N4YaADAH5hX64Cn6WDo09cgDt0qml5n/ttI4ZO2YK7STnZE6lpGRg4aSMuHN+E3T9NR5OGdeHkebLUf+jqbUfRqY0FfvpxHGZO6I30jCxoaWrAoJ4eYuLv4/XbD/lqh45fgrFhfcwc3wdd2lnhTUoa+HweDPT1oKUpRP+J6/Mtyq8E3kbYrTsYObAT2rcyR1p6Bqppa6Jh/doIvh6HC/7FByFZWTLMWHIQwf/uwQnxSrTt92Op/OZunWxw5cTWAsGT+NeL+XTF8janDTMxYmBHXPC7iemLD+Rb1vS0t4X7DkcoFEpkZctaWXedF1sU6AplvI/BZ2E/1yc7WzZTKBTgd8/lMMpTgykU8pGekQU+jwslUaGLcVOjLztNMT0zC+0GLMHh3y8hJTUdWpoaeJ+Wgd9PBaDXmHUF1mE/7fwZq7cfxf1HL6GpIQAplQgKjUWPUWsQFXf/I0YljJm9E06ep/DmbSq0NDWQ8i4dLj7/YszsnQUEWmnyE+QV2AEgIekJ5q/2xKvXqZgypmcehpMjKj4Zb1IKrzVOfZ9ZTiLgAAAO80lEQVSB2LsP8kXIMpkc8YmPcO/+c7xNScvnb1LSikyqff4yBVFxycUKyp9ith9V0OUFP5fLhUwmB5/3QWfs2tEGPvsWgsvlyGQyedeSQMdsnZWr88X6u02VhojozjVPMjU0IC0tDQo/v4+kIWIaMaBjofrOvGmD6NHNIzRqUOfSaTTI55FQKCABA32Kx+OSUMgnoYCfT+v7ku/m8biFnlPL4eR8Pu97qteKStrkcDjE5/MKXBufxyM+v3Avqr8dl8stFc2Ow+HQ5hWT6E6QJ3VobVHomN3rptGDG4fo0h9bSCjgU+e2VrmnRHrJonxdTFU6Xeks8j+IzDMTJSIKPLWTrp/fT49vHaFZE/sW22ftT+/V1KRhXXY/s4p4t07N6b9fNlB9fb1Cm3sDoD0/Tacnt4/ScY/luUK4lyIuwL1rqYLuY4vxc52iUt2nju3J+Ac5b51TJDOyXvG+fsl4Gty7HePx7jvmUlKomBKCvTKjfV2afirouJ/KfFZd5x2RyxUzAcBx6kA0M25Y4udqVq+Gnl2aY+bEvoVugLNWscbj8TCsXwesXTQWQgbPZ3DvduhpbweFQgm5XN7DutsPCV+8pvsEEE5OlIgoLsCdjBi0N21hZURCoYA4HA5ZNGVbk1UWVzVoNDNpQBwGPZJbNW+qataYFuPnalOm02tRa764APdpquOhmHSYBEA/uyyhB9cPUee2VuyDr2DfvGISPbx5mOZ815/R+IG92lJCcM4yK9bfza5cQVdItDslIVhEkVdcyKhxyedSWDRtRL+4Lc099pLHAqBCips4xONxSaeaJp0QrWLU9LxDawu6F+ZNCcFe6bH+bu0qBHSFgU8aIqa4AHcyMymZ+VRFRCfEq2jEQDbgKG/fvW4aHdgymzgcDqMjCHo7tKQ7QZ6UKPFSRvu6mpUG6L5oDybPDseR7GzZNL6Aj+Oey2HSpHjRWKlUwqhxPdiYN8GQPu3Z1X05mk41TTS3MoKtlTG0NIUlis49uthC7DQfXC4nWyaTO1h3m3en3AKJT2C+7xIlIkoI9qKmDI6urFm9GnE4HNLW0qDhrNRS5j4oVy7R1BBSjeol96ru0t6a7gR5UkKwlyzqqotRhU6vDETmaYkSEV0/v1/JBHxcLoeOeyyn5DAfampswAKkjLx75xb07u7vtHXVZMaBRE7msJciLsCtS6UEXSEis5r5TBg0drZs1phct89Rb0uxQCndo0R5XC5xORzGh6u0tTPLrQrzyij1bbCyZz73qYkSEd28eIDxkQImhvXpyomtNHaoPQuaUopet6yYRP/zWklCIbN2HEP7dqCkUDElSrwoNsCtfVmBrtQTvD5kMs89nJ0t+05XVxunjqyFcZOSs1RUKUm1auiyUUApGBHBqHE91NOvCQ5KTuNva2eGfZtmQqFQpmdlyWws7R1DKk0g8Rki8+Sck7ldyaJpyacy1qqpo24ONLRfe5a5PtO3r56iPqeWyXm1g/u0yxWHvSjG37V5lZhemUS7CcEiirzqwvjEbf+/d9DTiGPUjEGAwnp+t29vTcnhPiTeu4BRSliXdlaU/EEcblOlQVcI+CZJQ8QUH+RBFgzWfNbmTdTbatpaGiygPnFtt2jm0ALNNAvzvt1a5YjDwV6KKhNIfM60mxNwHKSmRsykkw6tzOnBjUM0uHdbFlQl+DLHERR2zokM9GsxGt+na0u6H+5DCcFeWXEBbp3KE3TlUj2SJ43+aFZW9qTqulo4f3wTzExKTql68fodFHIFnhXSVZO1/JZ47ymUSsKTF69LHOvQwQYu2+YgWyaXZWRmNbOwd7xW5QKJz5BaJidKRHTjwgElk71dVQer/t1b0+A+7Vh2K4TpPuW83yF92lNymA8lBHvJ4wLcOn5V0ysDkXmCKpOZybRbs3o1enD9ED26eZh0dbRYwKk38O0oKVRM537byGh8+1bmKnE4PcrXxfibAl0e5vsuUSKiW5cPkmWzkqUW48b1qJlJTpRrZ2PyzYOuuaWhuk6isM7yH/uIAR1V4rAy1t+t9TcFuoLM5zYhIVhE8YEeZMow4OjXvRU9j/qZBvRs882Cbt7UgfTk9jH1ce8lB2kWqhqJ9zF+rlYVDTpuRTOfVVfHX+Vy+XcCQe6RnM2alPi5pPvPkHT/GW5FSb/ZQOJWtBTS5CdIfviixLFD+3XAz65LoFQqKTtb1s6q67yYrzaQ+Aydb0JCsBdF+boyllq4XA4tmj2Exg9z+Gb0uXnTBlIve1vGn+nasbkqkEiL9Xdr9U1Pr8WBTxoiprvXPBmt+Qzq6VFSqJjO/rrhmwBedV1tuh9+iIJO72K4JGlNd655UkKwl7zS5tNVIpF5YqJERLcuHaRmDKSWZsYNqJFB7ZwKqBamXy3oejnksJxR43okFApKHN+/R5u84nB7FnQMwBfj5zouUSIiaYiYcUqV28659DTi2FeZybx15XeUFCqmEQM6Me4GcDeH6TIjrjg3YkH3aVLLxESJiG5eOqhkktVibtqQ9m+aRVqawq8OeKaG9emYy48FDr0pzIf376gWh2NZpvtskTmX+URq/a4kF/B5dObn9TR6SOcqD7jNyyfRhqUTGI/v1MaScjO/0yr7mo5bmZnPquu843K5fBIR8Kd4FazNSpZaqutqw8SwPvp2bVVkj+GqYu1aNkNvBzto5ul8WpSNGtQZv7gtAYhIJpN3tun+w71vXjIpBZF5bEKwiOKDPBnl55kYGuSeIysk+/bWVY7pOre1Ii6XS7UZTK2q8Umh3pQQ7JUa4+dqwU6vpRvtjpeGiCnW352szQ0ZFY1f+mMLPbp5hKzNm1QZ0K38YTQ9jThG2xhWgw0f0JESgkWUEOyljPYrnWJr1grqfOMSgr2UUb6ujDoWNDUyoK0rvyMABQ7+qIyuusb9m2Yxqrbr0cWWksPV4nBLFnRlC76xud2KyNqMGZMJBXwK+c+Jxg93qLQ7Egc2zaK/fdYw6mMCgAb0bKOSTGSRV50bVzXQVZnVd552Gb9nZ8smcDgc/Oq+jMxNG5X4WaMm9SAU8mBqaFApf5tQwIeOjhZq1qiW/xyOImxw73bw2OkIDoeTJZPLOzXvPv8BG0iUn8g8JlEioqRQb7JkoPOpegQbN6lHk0d3rzRMN3fKhxZhTGpfu3duoTo7OCPi8sEG7PRaMSLz+Nx8PqUVg2mXx+PS2V83UFKomBo1qFPhwGvZ3JRex/5GPk4LGY0fNahzXnG4DQu6ihWZx3zK9ppF00bkOGVA7mJeo8KYTrXDsnzeSEaf6dLOSiUOp0ZedW7Cgq5ySC3jEiU5dbs2FkYMN93t6PYVZ5oypkc59zLJCSSu/bOb9OvUZPSZsUPsVUmcihh/t+ZfA+iqtLSvapdhYT/3eFZW9hgtTQ385b0K5qYlV689fPwSSoUSyY9elOs1Ewgv36TifXom3qS8L3G8Qwcb7Fw7FQqFMjUrW2Zl5eAYyQYSlY/5xkhDxBQX6EHNGTCfSjfr271VuTQKct8xlyzNcgIhLQZF6qMGdVZNr4ror63Y+ivU+UbnqviMu8tfP7+P7oV5k0G9WmUGuhZWxnQvzJt+cVvKqK1En64tVeLw+1h/N1sWdFUEfNKQnKwWGwbba02NDahB/ZxkUkMGRyd8inO5HDWzDujVhtEOyqDe7VTicHbElYMNWdBVsWk3USKiiCvOSiZp9Kqp7WnEMXXL1tLwVfNH070wb7KzZlaOOaxfB1XmcGZswDdegliFpZZRiRIR3QvzJiY6n1HjehQf6EGGjUrv7DU7GxPy/XM7o7E97W3Vxda3Lx+szz7BKsx88YEfmM/GouRpl2nnzNL2sUMdVOKwjN3w/8qYLylUTEyn3fJ0hw42JA0RUUKw17uquOHPWvFrvtGJEhFF+7pSC0vjSgO68cO7fhCHK0GFP2tlw3wjE4K9lHcY1u2WtXfv3EJ1TFMKm8T59UstI6UhYroT5Em2VsZFJhMscxzxxcCaPLro7bgxg7uoxGF5lC9bbP3NgC8h2EsZ4+dGVh8xn6aGgE4dXkvJ4T40e1K/z9TuuOS0cSa9jv2N9m6YUVyx9fvYr2TvlTWG4Ivxcx2hCjhaWBqpARN4aiclhXrTvTBvuhfqTesWj/2MrbF5JA0R070wb5KGiGnv+hn5miLm5tNlqSQTFnTfXsAxSiW1tG7RlP4nWknJ4T60ffUU0tXRpoCTO+l+uA/9MH0QI8DxuFxy3T6XXsX8SsdcllCdWtXpD68V9CzyZ9rz03QaN8zhgzj8jUsmnG8dhDF+riM0NIR/crkccDgcXPS/hVlLnAEAfB4PoWedUEtPB+6Hz2KX2wlQMYcdumybg/49WiMoLBaT5zsBAITCnPZrqn7PSqUy/X1ahrFdr4XPv+X7zv2Wf3xu0fhfMpl8VO7xmeTkcVL9vlyhQO+xa/H0+VvMmz4Qy+eNKvwmcjnwdlqAcUPt4SeJwswfD6rfy86W49S5EAj4PCiVSrlMJu9o12vhc3Z6ZU3FfMNVNRwfV69xOVySnNlD98K8acHMIQWm2H2bZlFymA/94rq0UMkktyouJZJtoMNaEWu+EYkSEcX4uxXY1K+tp0s3Lhyg++E++fqZHD6wiJ7cPkqiPfOJz8t/1P2kkd0pKQd08hg/V3MWdKwVy3wJwV7Ku9cK1u3yeTzyP7mD7oV504wJfWjvhhn0+NYR+tVtqfqITuRJrc8Vh99G+7k2ZUHHWonMF+vvNlwaIqK717wKMF91XW26fdmZpCEiSg73Ia89PxSYXscNdVCJwzJ275W1TwZfQrCXMsbfrUAyqZaGkAJO7aRfXJcSj8vNx3SD+7RTSSapsf5uNizoWPtk8MX4uQ5T5fN9vL2mU02TuNz8KexD+3Wguzn5dJm3Lh3QZ0HH2pcEHMNzSyeVzS2Nismns1cxXQZbI8FaaQUcwxIlIrp//RAVBr6+3VqpWoW9v3lxf132jrFW6swX5euqzBtwTB7dQ3XIsIxd07FWVsw3VLXmszZrQj272KrE4bcRV5zZBjqfYBz2FjBnPksHR8QFuA8TCPh/v0tNh46OFjiAIitbZmHddV4CW+HPWlkz35BEiUiZEOz1JtrPla3wZ61c13wjo7+1815L0f4PPMCym2hf8hQAAAAASUVORK5CYII=';
					_options+='<div class="item-option" data-value="'+_v+'">';
					_options+='	<div>';
					_options+='		<span class="item-title">'+_v.substring(_v.indexOf(' ')+1,_v.indexOf(' - '))+'</span>';
					_options+='		<span>'+_vs[1].replace(/\|/g,', ')+'</span>';
					_options+='	</div>';
					_options+='	<div style="text-align:right"><img src="'+_img+'"/></div>';
					_options+='<button class="selectlocal">Select</button></div>';
				});
				$('.box-content').html(_options);
				//$('.box-pupop-content').height(100);
				$('.box-pupop').show();
				$('#shipping_first_name').data('old-value',$('#shipping_first_name').val());
				$('#shipping_last_name').data('old-value',$('#shipping_last_name').val());
				$('#shipping_first_name,#shipping_last_name').val('');
			}else{
				$('.disabled input,.disabled select').each(function(){
					$(this).data('old-value',$(this).val());
					$(this).val('');
				})
			}
		}else{
			_that.data('action','');
			_that.text('Edit address');
			$('.disabled input,.disabled select').prop('readonly',true).attr('readonly','readonly');
			$('.disabled input,.disabled select').each(function(){
				$(this).val($(this).data('old-value'));
			})
		}
	})
});

var Checkout = {

	that: null,

	init : function() {
		this.validation = new Validation;

		this.form_actions = $('#form_actions');
		this.checkout_next_step = this.form_actions.find('[name="checkout_next_step"]');
		this.checkout_prev_step = this.form_actions.find('[name="checkout_prev_step"]');
		this.customer_billing_details = $('#customer_billing_details');
		//
		this.redinvoice = $('#redinvoice');
		this.red_name = this.redinvoice.find('[name="red_name"]');
		this.red_email_invoice = this.redinvoice.find('[name="red_email_invoice"]');
		this.red_address = this.redinvoice.find('[name="red_address"]');
		this.vat_number = this.redinvoice.find('[name="vat_number"]');
		
		this.billing_first_name = this.customer_billing_details.find('[name="billing_first_name"]');
		this.billing_last_name = this.customer_billing_details.find('[name="billing_last_name"]');
		// under customer_billing_details div
		this.title = this.customer_billing_details.find('[name="title"]');
		this.billing_first_name = this.customer_billing_details.find('[name="billing_first_name"]');
		this.billing_last_name = this.customer_billing_details.find('[name="billing_last_name"]');
		this.billing_phone = this.customer_billing_details.find('[name="billing_phone"]');
		this.billing_address_1 = this.customer_billing_details.find('[name="billing_address_1"]');
		this.billing_address_2 = this.customer_billing_details.find('[name="billing_address_2"]');
		this.billing_city = this.customer_billing_details.find('[name="billing_city"]');
		this.billing_state = this.customer_billing_details.find('[name="billing_state"]');
		// this.billing_postcode = this.customer_billing_details.find('[name="billing_postcode"]');
		this.billing_country= this.customer_billing_details.find('[name="billing_country"]');
		this.billing_email = this.customer_billing_details.find('[name="billing_email"]');
		this.billing_mobile = this.customer_billing_details.find('[name="billing_mobile"]');

		this.customer_shipping_details = $('#customer_shipping_details');
		this.shipping_first_name = this.customer_shipping_details.find('[name="shipping_first_name"]');
		this.shipping_last_name = this.customer_shipping_details.find('[name="shipping_last_name"]');
		this.shipping_address_1 = this.customer_shipping_details.find('[name="shipping_address_1"]');
		this.shipping_address_2 = this.customer_shipping_details.find('[name="shipping_address_2"]');
		this.shipping_city = this.customer_shipping_details.find('[name="shipping_city"]');
		this.shipping_state = this.customer_shipping_details.find('[name="shipping_state"]');
		// this.shipping_postcode = this.customer_shipping_details.find('[name="shipping_postcode"]');
		this.shipping_country= this.customer_shipping_details.find('[name="shipping_country"]');
		this.shipping_mobile= this.customer_shipping_details.find('[name="shipping_mobile"]');

		that = this;

		this.ready();
	}, // init()

	ready : function() {
		var current_city = $('#shipping_city').val();
		setTimeout(function(){
			$('#shipping_city').val(current_city)
		}, 5000);
		//hide optional
		$('.optional').hide();
		// $('.woocommerce-shipping-totals').css('display', 'none');

		var selected_delivery = $('input[name=delivery_option]:checked').val();
		that.deliveryOption(selected_delivery, 'initial');
		$("input[name='delivery_option']").on('click', function () {
			that.deliveryOption($(this).val(), 'onchange');
		});

		$('#shipping_state').on('change', function (e) {
			if($(this).val()=='HO-CHI-MINH')
			{
				$('#express-option').show();
			}else{
				$('#express-option').hide();
				$('#stand_option').prop('checked',true);
			}
			$("[name='checkout_next_step']").attr("disabled", true);
			$("[name='shipping_city']").attr("disabled", true);
			that.getProvinceDistrict($(this).val(), 'shipping_city');
			setTimeout(function(){
				$("[name='shipping_city']").attr("disabled", false);
				$("[name='checkout_next_step']").attr("disabled", false);
			}, 500);
		});

		$('#billing_state').on('change', function (e) {
			$("[name='checkout_next_step']").attr("disabled", true);
			$("[name='billing_city']").attr("disabled", true);
			that.getProvinceDistrict($(this).val(), 'billing_city');
			setTimeout(function(){
				$("[name='billing_city']").attr("disabled", false);
				$("[name='checkout_next_step']").attr("disabled", false);
			}, 500);
		});

		// if((typeof window.sleeve_checker != 'undefined' && !window.sleeve_checker) || (typeof window.capsule_checker != 'undefined' && !window.capsule_checker)) {
		// 	swal({
		// 		html:true,
		// 		title: "Error",
		// 		text: "To validate your basket, your order should be composed of a minimum 50 capsules and should be a multiple of 50 capsules (i.e. 50, 100, 150, etc.) . You can mix the variety of capsules as you choose, as long as the total number is a multiple of 50 (e.g. 20 Roma, 10 Arpeggio, 10 Livanto, 10 Dharkan). Please add <b>"+window.missing_capsule+"</b> capsules in your basket below or return to the shop to add other varieties.",
		// 		type: "warning"
		// 	},
		// 	function(isConfirm){
		// 		if (isConfirm) {
		// 			var pathArray = window.location.pathname.split('/');
		// 			if ( $.inArray( 'vi', pathArray ) == 1) {
		// 				window.location = "vi/coffee-list";
		// 			} else {
		// 				window.location = "/coffee-list";
		// 			}
		//
		// 		}
		// 	});
		// };


		if((typeof window.sleeve_checker != 'undefined' && !window.sleeve_checker) || (typeof window.capsule_checker != 'undefined' && !window.capsule_checker)) {
			$(".basket-dialog-product-discount").html('<p style="text-transform: uppercase;color: red;margin: 20px;font-size: large;">giam 20% neu mua 30 vien do , vo mua di !!! .</p>');
		};

		//gtm checkout
		var checkout_step = 1;
		var checkout_step_two = false;
		var checkout_step_three = false;
		var checkout_step_four = false;
		this.checkout_prev_step.on('click', function(e) {
			checkout_step--;
			if(checkout_step == 1) {
				checkout_step_1();
				$('.step1').addClass('active');
				$('input[name="checkout_prev_step"]').hide();
			} else if(checkout_step == 2) {
				checkout_step_2();
				$('.step1,.step2').addClass('active');
			} else if(checkout_step == 3) {
				checkout_step_3();
				$('.step1,.step2,.step3').addClass('active');
			}
			$('#checkout_timeline').scrollto();
			 //else if(checkout_step == 4) {
				//checkout_step_4();
				//$('.step1,.step2,.step3,.step4').addClass('active');
			//}
		});

		this.checkout_next_step.on('click', function(e) {
			//gtm checkout
			checkout_step++;

			if(checkout_step == 2 && !checkout_step_two) {
				// checkout_step_two = true;
				checkout_step_2();
				$('.step1,.step2').addClass('active');
			} else if(checkout_step == 3 && !checkout_step_three) {
				// checkout_step_three = true;
				$('.step1,.step2,.step3').addClass('active');
				
				checkout_step_3();
				if($('#shipping_state').val()=='HO-CHI-MINH' && $('#express_option').is(':checked'))
				{
					$('label[for="payment_method_cod"],.payment_box.payment_method_cod').hide();
					$('#payment_method_onepayvn').prop('checked',true);
				}else{
					$('label[for="payment_method_cod"],.payment_box.payment_method_cod').show();
					$('#payment_method_cod').prop('checked',true);
				}
			} //else if(checkout_step == 4 && !checkout_step_four) {
				// checkout_step_four = true;
				//checkout_step_4();
				//$('.step1,.step2,.step3,.step4').addClass('active');
			//}
			$('#checkout_timeline').scrollto();
			if ( that.customer_billing_details.is(':visible') && that.customer_shipping_details.is(':visible') ) {

				setTimeout(function(){
				 	var selected_delivery = $('input[name=delivery_option]:checked').val();
					that.deliveryOption(selected_delivery, 'next');
				 }, 1000);
				if ( !that.validate_customer_shipping_details() || !that.validate_customer_billing_details() 
					|| (that.redinvoice.is(':visible') && !that.validate_redinvoice())) {
					that.checkout_prev_step.trigger('click');
					$('#order_info').addClass('hide');
				} else {
					$('#order_info').removeClass('hide');
				}
			
				//that.customer_shipping_details.removeClass('hide');

				//if ( !that.validate_customer_billing_details() ) {
					//that.checkout_prev_step.trigger('click');
					//that.customer_shipping_details.addClass('hide');
				//} else {
					//that.customer_shipping_details.removeClass('hide');
				//}
			} // endif

			//if ( that.customer_shipping_details.is(':visible') ) {
				/* setTimeout(function(){
				 	var selected_delivery = $('input[name=delivery_option]:checked').val();
					that.deliveryOption(selected_delivery, 'next');
				 }, 1000);
				if ( !that.validate_customer_shipping_details() ) {
					that.checkout_prev_step.trigger('click');
					$('#order_info').addClass('hide');
				} else {
					$('#order_info').removeClass('hide');
				}
				*/
			//} // endif

		}); // this.checkout_next_step

		this.populateShippingForm();

	}, // ready()
	validate_redinvoice : function() {

		if ( !this.validation.validate_red_name(this.red_name) )
			return false;

		if ( !this.validation.validate_red_email_invoice(this.red_email_invoice) )
			return false;

		if ( !this.validation.validate_red_address(this.red_address) )
			return false;

		if ( !this.validation.validate_vat_number(this.vat_number) )
			return false;
		return true;

	},
	validate_customer_billing_details : function() {


		if ( !this.validation.validate_title(this.title) )
			return false;

		if ( !this.validation.validate_email(this.billing_email) )
			return false;

		if ( !this.validation.validate_first_name(this.billing_first_name) )
			return false;

		if ( !this.validation.validate_last_name(this.billing_last_name) )
			return false;

		if ( !this.validation.validate_billing_address_1(this.billing_address_1) )
			return false;

		if ( !this.validation.validate_billing_city(this.billing_city) )
			return false;

		if ( !this.validation.validate_billing_state(this.billing_state) )
			return false;

		if ( !this.validation.validate_mobile(this.billing_mobile) )
			return false;

		// if ( !this.validation.validate_postcode(this.billing_postcode) )
		// 	return false;

		return true;

	}, // validate_customer_billing_details()

	validate_customer_shipping_details : function() {

		if ( !this.validation.validate_first_name(this.shipping_first_name) )
			return false;

		if ( !this.validation.validate_last_name(this.shipping_last_name) )
			return false;

		if ( !this.validation.validate_billing_address_1(this.shipping_address_1) )
			return false;

		if ( !this.validation.validate_billing_city(this.shipping_city) )
			return false;

		if ( !this.validation.validate_billing_state(this.shipping_state) )
			return false;

		if ( !this.validation.validate_mobile(this.shipping_mobile) )
			return false;

		return true;

	}, // validate_customer_shipping_details()

	populateShippingForm : function() {

		this.shipping_first_name.val( window.user.shipping_first_name );
		if ( !this.shipping_first_name.val().trim() )
			this.shipping_first_name.val( this.billing_first_name.val() );

		this.shipping_last_name.val( window.user.shipping_last_name );
		if ( !this.shipping_last_name.val().trim() )
			this.shipping_last_name.val( this.billing_last_name.val() );

		this.shipping_address_1.val( window.user.shipping_address_1 );
		if ( !this.shipping_address_1.val().trim() )
			this.shipping_address_1.val( this.billing_address_1.val() );

		this.shipping_address_2.val( window.user.shipping_address_2 );
		if ( !this.shipping_address_2.val().trim() )
			this.shipping_address_2.val( this.billing_address_2.val() );

		this.shipping_city.val( window.user.shipping_city );
		if ( !this.shipping_city.val() )
			this.shipping_city.val( this.billing_city.val() );

		// this.shipping_postcode.val( window.user.shipping_postcode );
		// if ( !this.shipping_postcode.val().trim() )
		// 	this.shipping_postcode.val( this.billing_postcode.val() );

		this.shipping_state.val( window.user.shipping_state );
		if ( !this.shipping_state.val().trim() )
			this.shipping_state.val( this.billing_state.val() );

		this.shipping_country.val( window.user.shipping_country );
		if ( !this.shipping_country.val().trim() )
			this.shipping_country.val( this.billing_country.val() );

	}, // populateShippingForm()

	getProvinceDistrict : function(province_code, field_section) {
		var data = {
            action: 'get_province_district',
            province_code: province_code
        };
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: ajaxurl,
            data: data,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Something went wrong. Please ask the administrator for more information.');
            },
            success: function (data) {
                if ( data.status != 'failed') {
                	var district_select = $('#'+field_section);
                	$('option', district_select).remove();
                	district_select.append(new Option('Please select district', '', true, true));
					$.each(data.districts, function(index, value) {
						district_select.append(new Option(value, index, true, true));
					});
					$('.current_district').html('Please select district')
					district_select.val('');
                } else {
                	console.log(data.message);
                }
            } // success()

        }); // ajax()

	}, // getProvinceDistrict()

	deliveryOption : function(delivery_option, action) {
		$("input[name='checkout_next_step']").attr('disabled','disabled');
		document.body.style.cursor='wait';
		var basket_price = $('.basket-price').html();
		if($('#editaddress').data('action')=='true')
				$('#editaddress').trigger('click');
		if (delivery_option == 'pick_up') {			
			if (basket_price  > 1500000) {
				$('.payment_method_cod').hide();
			}

			$('.local_pickup').click();
			$(".local_pickup_label").show();
			$(".express_label,.flat_rate_label").hide();		
			// pickup
			//$('#pick_up_location_field').show();
			var _options  = '';
			$('#pick_up_location>option').each(function(){
				var _v = $(this).attr('value'),_t = $(this).html(),
				_vs = _v.split(' - '),_img = _t.substring(_t.indexOf('|*|')+3),_img = _img?_img:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAACeCAYAAADDhbN7AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4gwDBCkzvIIE5wAAIABJREFUeNrtXXVYlNkXficJQcVAsSilFewEu7tzbV3FtdautVsxaGbA2nD35+6qu66tpAxh0SgMYrcikhPn9wfMCFKfSup3nuc8Ps/MnfGb73t57z3vPedcgLUvsmh/N/YmsFb2dv3CfgBAfKDHpkhfl27sHWGt3EAXF+gxPVEiokSJSBkT4NaFZb5PNw57C5iDrnWfRYgL9Fgo4PP2p6VnorquNhQKJdIysxxadPshgL1LrJUV0y2Qhojp9mVnali/NnVua0V3r3kqE4K9sqL9WeZjrQwsLtBjZqJERLcvO5O2tiYBIADk0MGGHlw/RIkSEUX6ubLgY60UmS7IY6U0REzRfm7UpKG+GnQq79zWipJCxZQQ7JUZ6etqz9451kqD6SYnSkR0/fw+0qmmVQB0eZkvN+CgaH83e5b5WPsSplsnDRFTXIA7GdSrlQ9ols0aE4/L/Yj5LOnhjcOUEOwli/B1cWDvJGufw3RTpCFiCjvrRLX1dPMBrFGDOnTj4gG6/L+tpJNnvQeA7NtbU3yQpzJRIqKYAHcHlvlY+xSmW50oEVFcgDvVqpkfdCaG9SnG340eXD9ESaHe5PfX9kLWfJb05PZRSpSIKOKqS1f2zrLGRDL5XhoipvDz+wpMr4aN9Cn0rBMlh/nQyIGdacPSCXQ/3IfO/bqRtDQ1CgQccYEeylyRmWU+1oplusWJEhFF+7pSNW2tAqCLD/Sgx7eO0JC+HdSv79s4k5LDfSj43z3E4XAKgO/xrSM5zMeu+VgrAnRLcsThg9S4Qd3806tRfQo960SPbx2lMUPsC0yt6xaPo/vhPnTm2Hri83j53uvUxpLuBHkqE4K9sqP9WeZjLX8gMTtRIqI71zwLSCaNG9ShWH93uhfmTUP6tC9STnHe+j0lh/uQ5Mxe0hAK8r03oGdrNfNF+bmy4APAZZnOY61AwPPk83jQ1tSA5y5H8Hg5t6WZcQOcOrwWdWpVx9KNh3D6QkiR3zd/jSe8jp2DQT09/H1ojfp1bS0NLJkzHNkyObQ0hdAUCi5G+rl2tXZwZP/iv2GmmyYNEVHYOSfS1BSSj9MCuhfmTa7b51DjBnUo4oozJUpENLRfhyKZ7mM/sHk23Q/3oaDTu0m/Tk0K+W8v3Q/3ofU/jieH9jaUEKwWmbuyzPdtMt0GaYiYYgPcqV5dPQJAHA6HRHvm0+NbRyjiijO9jPmVhg/oxBh0Kv/px3GUHO5Dsf7ulBzuQ7Mn9VW/16G1Ra7ILJJHsPl83xzTTZeGiCn0v735dDoOJ+dfn30L6V6YD434DNCpfOfaqfTg+iHasHRCoXu78TlSC8UEuHdjme8bWdMlSkQU6+9GNavrFAoaLpdDndpYfjboVOzZ3NKoyPc7trbIK7V0Z5/Q1wy6QI950hAxhZ/bR/X19b4IWKXhHVtbUFyAuzIhh/m6s8z3lep0iRIRRV51IW0tjQoHnRp8bSzp0c0c5mNrOL4+0K2Qhojp1qWD1MigTqUBncrbtzLPKzKzzPeVBBJzEyUiunHxAGlqCEsEQVmwoVDAL3FM57ZWlBzmrRKZWfBVZaaLz5VMonxdqWH92iU+/B5dbCn8/D7q3M6q1EA3ZUwPCji5g4yb1CuZ+VqaqzKZsyNzwfe12le5c5FbDTZTwOetf/kqBe0HLMGjp69K/Fx1XS1oCgXQ0dYstWupWUMHNarrQEtTo8SxITfjMWmeE4gg0NYQXonxd+vBMl/VYrrN0hAxxfi7Ud3aNUpkmk5tLcm+vTUBKJNot7quNgGgzcsnUkODkpm3XUszepAjMisifF16sE+2aqzpZktDxCQ5s5f0augwAsb18/vpye0jpF+nZpkFEC2sjCk53Id+c19WIH2qOKklV2TuyTJf5Wa69VKJiGL83NQsU5wL+DkpTHbWJmTZrHGZR68zJ/RRp9F/nD5V1Jovj8jck33SlVMyWaiqkVDtvZY0vd4J8qSxQ+3LXT7ZvmYKhZ51otq1qjMCX+wHkfmrYb4qH1y07rMIcUEeywQ83v73aRmwH74Sz168KfFz8QkPQUSIufOg3K85+cEzvHufjlev3zEKOKYtPMDRFAqgIeBfivR17WHt4MhOu5WA6dYkhYjp1qUD1ICBZNLT3pbWLBpDAEhDQ1BhorEqWXTn2qnU4KPajsK8rZ0Z3QnypIRgL1m0vxu75qvgQGKeqtj646zfwpzP59HF3zdTXKAHNTNuUOE7FgN6tqGHNw7TycNrGe/tJoXmisws+CoskNiSIw67kIF+yYxh2Cin9USD+rWpawebSrNd1tvBjvh8HvF5XEYic1vbZpQUIqaEYC9ZpJ8rG3CUM9N9nyOZ7CFtLc0SH5Z+nZr05PZR2rxiUqXbp1Ux8emj6yj8nBOjLbuOrS3oTpAn5Ugtbr2qIvNxqyDTbRMK+B6ZmdkYOGkT0jMyS/xs6vt0BIXF4q9/r1XK36aQK3D2cjhux9xDVrasxPHB1+MwwXEPhEI+hALBhQhfl15sDUfZMp2jNERMwf/uoZoMxOF2Lc3o4JbZJBQKKiXTFea19XTJeescRlk07VuaU6y/WmTuza75yobpNkpzi62L69qU1/8Ur6LkMG8ybFyvygBv8uge9Db+ODlOGcBofFvbZup8vghfl94sYkpXMlmSFCKm0LNOjLa1una0oWq5zXS+NIUdFZSfB4DMTBsyyqppa9uMYv3VIjPLfKUyvQZ5rMjpxHmQkWRi2Fif4gI96Myx9SRgkAdXWZ3D5VD4+X0U4+/GaIejXUuzvJ1JWfB94fS6PilUTDcvHijQQKcoFwj4tHf9DOrZxbbKgk7ljlMH0NSxvRiPb92iaV6RmQXfZzLdgkRJTrE1k+DAzLQhnT++iYT88mE5Ho9bbgA0NaxPkVddqF7dmoymaWmImKQ5ReOVFnzcSsp0O4R8/oH0jCwM+m4TshlIDJ3aWKB1C1MYG9Yvl2sdO9QBtfV0y+X/6tzOCvq1a6Bxg7oljg25EY9RM3cAHA40BPz/Iv1ce7NSyydJJrtJi4GYamttrN7++riJYpmtvzigI86L6cfZw8qN9azMmhAAsjZvwijAat/SnOIDPXJFZve+7LRbPNPtlIaIKdrXlfRqlqzTWTRrRHeCPElyZm+5rr0EAj5Jzuwhv7+2E5fLKbf/16ixPj2+dZTuBHkQOCWPb9W8qTrgiPB16VOZnnmlOdknLshjgZDPP/D0+RsMmLgBb9+lFTu+tp4uXr1JhePUgUhMeozzfjfL7Np0qmmhpY0xsrLlICLo164Bz90/ICU1DUs3+uDVm9Scm8kBwm4lQKlUltm1TBnTE1Fx93A9IhG1aurg9dv3xY5va9sMRw4uJg0NASdLJu9vZT/3XLS/G77p6TdfjYS62LrkvdeWzU3pTpAXNbcwLMcIcyAlSkR095oXJQR7qY8WuBvsRXevedHDG4eoX4/W5XY9Q/q2p1h/N0Y6X+sWTenhzcOqovG+33RwoTobLD7IY4VQwF/74vU79Bi1htHeq4G+HjgcoH69WuV2vW6Hz2D6ogN4+vwNOJwPEwWfy0Xs3QfoPGQ5zl25Xm7XU69uTRA4qFOresn3OiIBQyZvQVa2jLQ0Nc7FBLj3+6bXfPFBHqsSJSK6efEAo6LnpsYN1I1w+HxehaUxqXLiEiUieh71c4W1xBDkSkfD+3cgw0Z1Ge1w3A/3URWNV2jAwa0IplNNr0IBf1tKajr6T9iAbJm82M8J+DxcPL4Jpw+vBY/Hg1yuqJAb1r1zCxARXrx+Bx6Pi6xsGSaPrpgKRJlcDltrY/jsW4StqyaXOD7s9l0MnboVcrkCmkLBmWh/t77fVBp9XJDHIqlERCH/7WW0raWqzDI3bUh21iYVupsQcHIHnfl5PWlpCslAX4/8/95Bp4+sK9fo9mP/bnR3atygDnG5HPUedXHezs6MEiUilcj8dU+7eZhutzRETBFXnBkVW/fv0Zqi/dxo/HCHCt/CsjJrTLMm9iVOnte0NDXIcepARuWUZe07106l6+f3MUrrt7MxIWlOJrM80s+139fOdAukIWIK+mdXgQNJioxgbUwo9L+91L1ziwp/sFwut1Lv7c7+ri9d+mML1WD4R9DW1oziAtxVInP/r4r58jDdHmmImKKuulCN6tVKvCkOHWxowczBBIB0GebffevOydOZ6je3ZQXO6yiU+axN6L4qq8XXpf/XFr3+KA0RU9DpXYxAB4Au/r6FHt48TCZN6rOg+oxOow9uHCK3HXPVfZ1L0vmi/VyViRIRxQa4D6jSzJe3RkIqEVHEFWfS1Cy5P111XW3icjjUyKAOo6or1gv3EQM6Ejc3g4bJH3tLG1N6eEMlMrv2r+qgW5sUKqaQM3uZdW1qY0nJYT60asEYFjylUb3G49EJ0Sq6ffkgo2jXztqEov3clAnBIoqpqswXH+SxJlEiousX9hOfQY6cSo64emIbWZVDA51vxaeP70Weu+blu8clTbvJYWqRuWoEHHmYbntSqJhuXNjPKIVnQM82JN47v0qnqldm53I4pKUppF/clqoL24tzWytjig/0oIRgL3m0v1vVYL74II8l0hARSc7sYcR0PC6Xjnsso/hAD7I2b8ICpYx87FB7Sg73IdftcxmNb9OiGSUEe5E0RESVFnx5mG5fUq44zKRApWXznF0IbW0NsrU2ZgFSxt7Cykjd9dTctCGjZpK5IrMi0s91QGVlusXSEDEFntrJKHqtpadLj24eoROilSwoytG1NIUkObOHQs86kRaD59S6RVOK9VeLzIMqBfPlZTppiJgir7qQro42412A3T9NJ3PTRiwgytlnTexDyxxHMNL4VMynymqJ9HUdWFmYbnlSaA7T1dAtWS/q3rkF/e2zhtFU/KXOK2GLS7OI/nhFPRAtLWGZXg+TumEej8sYMCW5tbkhnTy0hkwN6zPS+aJ81SLz4C9lPu7nMt3iWcMRH+SxQ8Dj7XyXmo6eo9ciJTWtxM8O6NkGrZqboHo1rTL9g+BwODh5eG2xY5y3zSn0GAAiIPjfvdixemq+10+IVqGhQe1Cv+u4x3Icc1kCLrfoW/rPsZ+KvZ7/iVaCz+cV+b6ujjaOHFicLxH1S6y1rSm6tLNCk8b6JY69GZWIifP2coQCPoQC/ulIP9eBX5I+z/0c0LXuswhzpvbdIBTwVzx/lYKeo9eW2OVoUO+2MG5SD8s2+aDfhA1IevCsTIHXsrkpmlsaYsyQLkWOMWqkj/PHN8GwUcEbb2ZqgAb182c48/l8nD6yDlZmTQp+V2N9GDXSL7KIxc7GBJbNGmPCiK5FXk9bu2b4zX0ZdHUK/lE2NTKA/8nt0NbSyOGgUrCjf1xBp8HLcDUwAvYdrNGkYfHlk7ejpRj43SZkZmaTllD4b8wXMN8nAy83XX0dn8db/+p1KjoPWYGXJfTyrVdXD3vXz8Ax5x+hpSlE7N2y7zu8esFoKJWEyaN7FMlCCoUCDevXwsnDa1BbL38KuVJJICr4hGvV0MG/x9ahhZVx/vFEUFLRiFjuOBJEhEkju4PLLRyecrkCrZqb4m+fNdDUFKpftzJvAr+/d6C6jnah1/Qldkf6GFpaGji0byHO/bYRNapXK3Z8REwSJv3gxOHzudAQ8E9H5TLfp4KP+ylMl7um2y3g8zcBwNkr16FQlJwJ/PzFG+wXncbKrUeQkZld5qDr2tEG9u2t8DYlDS1tTNCxjUWRY5VKJerUqgHfv7ahjW2zEr87IysbXC4Xf3gux6DebRldT8fWFuhpb4u379JgZ22Mrh2bFzn2bUoarMwa49Lvm1G/rh4cOtjgT/EqKJRKyMoo6zojIwubnI5j1hJnpLxLg6CY6V417cYnPAKPx4WWhvB0tL/boE8FH/cTmW65gM9feu/BMzx9/gazv+uHNQvHFPkZGwtDRPu6on69WnA/8h8CQ2PKHHQ8Hher5o/Gy9fv0H3kamRmy+G2Y25RK0HIFUrMXuoCTU0h/vBageaWRsV+//u0THQatAzZMjkObvkevexti7/BXC7W/TgOr9+kwmHYCqSlZ8Jl2/dFjo25+wCOq9xRX18PF37fjOMey0BEGD9nl7qMsizs6P+uICgsFr0dWiIu0AMGRRRSCQR8/OK6FOZNG0JyPR4ZmdlcLQ3BPzH+boM/BXzcT2C6g0I+f2fq+3SMnLEd/SdsQGTsPcyY0KdI8NWoXg01a1RDdR0tlJc1NW4Ai6aNcMHvFlJS07B1/+/QraaFYf07Fg5ULhdnr4RjysL94PO4+FO8Cp3aWhY5pfG4XDx+9hoDJm5EdrYMor3zMXti3yLXXSaG9WHZrBEu+N3E+7RMbDvwP2hpamDUoM6FjufzeTh5VoLpiw9Cr0Y1vE/LxMBJG3ErOgk8btmXQevXqQ6BgA8NoaDQ93/3XI6unWwQFBqLCXN3Y9ycXSAChALBqUg/10FMAw4uQ6ZbKuDz5z948hLtByzFm5T3ePc+HUOmbMabt+8xf8ZgrF5QEHxBoTEw7TAb8YmPyg14TuunAxygZ5cWuH3ZGYtmDQUAzJxQfM/Ca6ExmLxgHxRKJX5zX4b3acWXWT588hIdBi7Fw8cvsXrRGOjXqVHouF3rpoLD4aB77vUsnjMs93qKL+z3l0Rh5lJndBi0FEn3n4HHLZ+6rF/+8kObvotxr5DgT7x3PlramOBKYAQmztsDJRGi4pIxauYOZGXLoK0h/Cc2wH0ok4CDy4TpBHz+7vdpGeg/YQMysz6s0UyNDKBXUwdcDgchN+4U+j15x5e19enaErbWJnj+IgXPX6bg5esUPH/5Fi9epaB1i6bo061VsZ/3vRaJPmPXIfV9BqP/7+27NPQYtRZJ958VGsB079Qc7Vqa4fnLFDx/8Tbnel68xfOXKbC1NsbgPu2K/f7zV2/gdRlOr0WuyV++LfT16DsPwOFwYG7aENw8ks7NqESM+34Xh8/nQSjgn4z0dR38RZ0K4oM8VieFiing5M4CxSxWZk0o4oozJUpE1Ltry0pQD8Ghvw+tobgA90Jz/6J8XenW5YP56ncvHN9ECRKvQnuyhJ51omPOS/JnRf+xhW5ePFjINpQG/eWzmiRn9qhFYi6XQ396r6K4QA/Sr1PweiKuOFPEFed8h7w8vX2UfvdcUXiCrI42hZ51or991hCXU3EVbUvnDqf7130o6PSuAocU2loZU+RVF5XIXCzzcQtjuqt/bkN8kMcuAY+39W1KGnqMXoN3qenqMdYWhvjvl/WoU6s6Zi5xwcUy7FvC1PRq6ELA5+Ov/yR48SqlwPv7vE4i8d7TfMFDzN2HuBkhLTA27u5D9B67Ds9e5P/Lj73zABGxSQWjwswsjJi+DZcDI9SvVdfVhlAgwKlzIXj+svDruZv0BM0tDNWvhd1OwB3p48KlH6USkbHJiE98BKrA+7zH/W94HD2Hhga1cfnEVgj4/A86X0wSJjru5QhUzOfHkPnyHl6SFJrTKkxV06ry5pZGdPuyMz24foj6dGv1dRTJcD7/SPeyvobK6j9MH0T3w33I96/tBZqh21gYUpSvqyqTmdGaD/FBHusTJSIK/W9vgXI+K7PGlBQqpnth3tTL3o7dcP/GfePSCZQc5kO3LztTtY+aLdlZ59TtSkNEFOXvNqRQ8OXLpwsVU/j5fVTno018W2sTivJ1pacRx6jvV8J0rH+5z5ncn5LDcphPKBB8lITQhOICPCgh2EsRXRT44oM8Vqny6XgfHeJradZY3Z6rJ8t0rBfGfOE+dOvSwQIBh521CcUHepA0REQxAe7D8oEvPsjDTRoipluXDhY4MaelTc4Hn0f9XCmiV9Yrp8+dOoCSQr0p8PSuAulfVmZNKCHYixKCRRTl5zoEADix/m62QqHgFo/LpZ5j1nIS7z1Rs6CmhgAXft8Mw4b6OHriCtbt/LnCoqm2dmbo170lGjesiyfP3uD81Ru4Fh5b6FhTIwP0drBDCysjpGdkw/daJK4GRSAtPb8oPHl0D9SsUQ0Hxf8U+I45k/tDJlfA+9cL6temjOmJ2rV0kXdTQ6lUIu7uQ/hei0BW9oeOV0aN9WFt1gRnLocX+ZvsrE2gqSmE5HocAEBXRwtTxvSEQMAvuLnH4eCXP6/i2Yu36OVgB1trYyiVRce3fB4PnkfP4t379HJ7RgEnd8LEqD78JdEYP2dXvvfmzxiMFfNGIitbhnfv0g04ABDr7zZdQ0PgLZMp0H/CBkjvP1V/oGb1agg+swcaQgF2uf4Jj6Nnyx104r3z0aNLCwgEfHC5XCiVhKysbASGxGD6jwfyPYAhfdphz/oZ0NAQgM/jgUCQZcvx7OVbOAxfiew84Dj/20YYNtKHhX3Bvdzw8/uQkZEF+2Er1a9dOL4Z5qYN8uXMEQC5TIHnr1LQceBSKHLb0Joa1se1f3dj8Xpv/PqXb4Hv7+1gB7cdjvhhtQfO+94AABjUq4WLxzdBp5oWeB9t1PO4XPSfsB63Y5Kwc900TB3dA9lyBTgA5AoFFAolhEI+OLnNkbU0BbDruQAPn7wql2e0ZeVkjB9mj1vRUoyetSPfM+nYxhKH9i+EgM9VZGZmW1p3++EuYnPn2/hAj9mJEhGFnXUi44/O/mpQrxZdP7+fksN9aNLIbuVK4YtmDaH71w/R4QOLyKJpI+JwOWRqWJ88d8+j6eN7EyePmNqhtTm9iP6F/P7aTn262pFQwKfaerq0aNZQkoaI6do/u/Nl+Z77bSPF+rsX+v+Gn9tH/id35Hvt/PHNFHBqJ+nqaJNeTV3Sq6lDdWrVoHlTB5I0REyrFozOdzbFo5tH6F6oN00d0zPf9wzr10Hd2DFvoGZQrxZFXnWhQ/sXkamRAZka1v/gRgbqa69buwY1zX2/cYM6tPunafQq5leyMmtMJrnjmxoZlFvzym2rptCrmF/p5KE1BQrHeznYqep0FXEB7j0A4NalA+Bb5gp85l3meMX6u8lq6en6nD++Cb3HrkPyw+cAgMfPXsN+2AqEnnXCpuWTwOPxcOSPy+XylzRjQh+8ep2KGT8ehEKRwyaJyU/x/TLXjzI7OFgwYzCevXiLifP24uGTlwCAV29SsV90CkqlEgtnDUHXjja4wFDwLizTV6lUIvWj6ev4KX/MmtQXpkYG4HA4HxIMOIBcocTG5RPRqEFtbNn/B8YNc8DWld+VkAGTAWny0yITFV68Ssknkr99lwYej4uk+8/KJe0sr61fMgHjhtkjMCwGw6Ztzfdey+amcN8xFzK5PDsrK7u5dbcf7ty6dAB2vRZ+2LmI9XeDpYPjIblc8T2Px8Wf3qvQ1MhA/SXpGVnoP2E93qdlYPPyiZg9qexbqhk1rocmDevC9dC/UCiU4PG4mDGhN5Y5jsAyxxFYvXAMdKpp5qZD8WBiWB9Pn79Rgy6vnTwngYDPQ7tW5oz/f6Yp5j26tEA1bQ08ffYmH1g0NQRw8jyJmPj7mP1df4j2zMfe9dPx7MVb7HA5UWSCahvbZhDtmQ/x3hw/cnAxendticpmu9dNxw/TBuJ6RALGz9md773+PdrgL+9V4HK5kMvlvfKCLt+WWV7my86WTderoYN/jv6ULx36weOX6DBgKV6+TsWq+aMwdqh9mf4wbS0hiAjv0tJzWY2LEQM6Ydq4XpgxvjcWzBgMHW0tFbmAX0yLWlWrWw1hnoU7EYrCFofDgayY9rhjhnTB3WueeHTrCPaunwElEQ6IT+X/DnDw5m0q+k/cgKjYe+hpb4vYuw/RafAyPHj0osg0+Vp6OmjbshnatjRD25ZmaN/KHPXq1qxUoFu7aBzGDrNHyM07GPv9LqRnZOULmly2fQ+FQpmRlZ1ta2HvGJAXdIXu1X5gPvksDQ0BTh9Zh2bGDT4wX2YWBk/ehIysbOxaNw3zZwwusx8Xe/cB3qWmY8a4nBQiuVyBmUuc0WfsT/hhjSe08qSHK5RKPH72GvXq1kQNXe2CmSKdW0CuUOJWdFKe1KZXqF5dO6eOIY9pCAUwqKeHJ89eF3ltf5wOxG9/+0MhV+J2zD10GbICL14VXQIwfPo2+Px2EcM/mo4Kz5KJQveRq9F9RI53HrwMJ/4JqjSg27N+Bhyn9se1sFiMmrkt37ke/Xu0xgnxKgCATCZ3sHKYF/Ex6AoFnqWDI2L93WDeZa44Kyt7hk41LfzpvSof8z16+grt+y/BsxdvseT7YUUmNX6pEQF//ReMZiYNMGNCH2hqCPDk2Ws8f/UWxk3qQUmk3jBXKJT4+2ww9OvUxOYVk6Cro61e+5mZNsTqBaORlp4J/+Ao9ff/djIAWVlybF89RT1la2tpYOfaqciWyXH0jyvFXt/Wg38g6f5TNDU2KADejy0rW4Yt+39HalrJKVdyuQJpaZl4n57jqWmZICLweBV/9NxPi8dj0shuuBElxeT5TsjKkuWTvNx3OgKgzGyZrIOlg2N4YaADAH5hX64Cn6WDo09cgDt0qml5n/ttI4ZO2YK7STnZE6lpGRg4aSMuHN+E3T9NR5OGdeHkebLUf+jqbUfRqY0FfvpxHGZO6I30jCxoaWrAoJ4eYuLv4/XbD/lqh45fgrFhfcwc3wdd2lnhTUoa+HweDPT1oKUpRP+J6/Mtyq8E3kbYrTsYObAT2rcyR1p6Bqppa6Jh/doIvh6HC/7FByFZWTLMWHIQwf/uwQnxSrTt92Op/OZunWxw5cTWAsGT+NeL+XTF8janDTMxYmBHXPC7iemLD+Rb1vS0t4X7DkcoFEpkZctaWXedF1sU6AplvI/BZ2E/1yc7WzZTKBTgd8/lMMpTgykU8pGekQU+jwslUaGLcVOjLztNMT0zC+0GLMHh3y8hJTUdWpoaeJ+Wgd9PBaDXmHUF1mE/7fwZq7cfxf1HL6GpIQAplQgKjUWPUWsQFXf/I0YljJm9E06ep/DmbSq0NDWQ8i4dLj7/YszsnQUEWmnyE+QV2AEgIekJ5q/2xKvXqZgypmcehpMjKj4Zb1IKrzVOfZ9ZTiLgAAAO80lEQVSB2LsP8kXIMpkc8YmPcO/+c7xNScvnb1LSikyqff4yBVFxycUKyp9ith9V0OUFP5fLhUwmB5/3QWfs2tEGPvsWgsvlyGQyedeSQMdsnZWr88X6u02VhojozjVPMjU0IC0tDQo/v4+kIWIaMaBjofrOvGmD6NHNIzRqUOfSaTTI55FQKCABA32Kx+OSUMgnoYCfT+v7ku/m8biFnlPL4eR8Pu97qteKStrkcDjE5/MKXBufxyM+v3Avqr8dl8stFc2Ow+HQ5hWT6E6QJ3VobVHomN3rptGDG4fo0h9bSCjgU+e2VrmnRHrJonxdTFU6Xeks8j+IzDMTJSIKPLWTrp/fT49vHaFZE/sW22ftT+/V1KRhXXY/s4p4t07N6b9fNlB9fb1Cm3sDoD0/Tacnt4/ScY/luUK4lyIuwL1rqYLuY4vxc52iUt2nju3J+Ac5b51TJDOyXvG+fsl4Gty7HePx7jvmUlKomBKCvTKjfV2afirouJ/KfFZd5x2RyxUzAcBx6kA0M25Y4udqVq+Gnl2aY+bEvoVugLNWscbj8TCsXwesXTQWQgbPZ3DvduhpbweFQgm5XN7DutsPCV+8pvsEEE5OlIgoLsCdjBi0N21hZURCoYA4HA5ZNGVbk1UWVzVoNDNpQBwGPZJbNW+qataYFuPnalOm02tRa764APdpquOhmHSYBEA/uyyhB9cPUee2VuyDr2DfvGISPbx5mOZ815/R+IG92lJCcM4yK9bfza5cQVdItDslIVhEkVdcyKhxyedSWDRtRL+4Lc099pLHAqBCips4xONxSaeaJp0QrWLU9LxDawu6F+ZNCcFe6bH+bu0qBHSFgU8aIqa4AHcyMymZ+VRFRCfEq2jEQDbgKG/fvW4aHdgymzgcDqMjCHo7tKQ7QZ6UKPFSRvu6mpUG6L5oDybPDseR7GzZNL6Aj+Oey2HSpHjRWKlUwqhxPdiYN8GQPu3Z1X05mk41TTS3MoKtlTG0NIUlis49uthC7DQfXC4nWyaTO1h3m3en3AKJT2C+7xIlIkoI9qKmDI6urFm9GnE4HNLW0qDhrNRS5j4oVy7R1BBSjeol96ru0t6a7gR5UkKwlyzqqotRhU6vDETmaYkSEV0/v1/JBHxcLoeOeyyn5DAfampswAKkjLx75xb07u7vtHXVZMaBRE7msJciLsCtS6UEXSEis5r5TBg0drZs1phct89Rb0uxQCndo0R5XC5xORzGh6u0tTPLrQrzyij1bbCyZz73qYkSEd28eIDxkQImhvXpyomtNHaoPQuaUopet6yYRP/zWklCIbN2HEP7dqCkUDElSrwoNsCtfVmBrtQTvD5kMs89nJ0t+05XVxunjqyFcZOSs1RUKUm1auiyUUApGBHBqHE91NOvCQ5KTuNva2eGfZtmQqFQpmdlyWws7R1DKk0g8Rki8+Sck7ldyaJpyacy1qqpo24ONLRfe5a5PtO3r56iPqeWyXm1g/u0yxWHvSjG37V5lZhemUS7CcEiirzqwvjEbf+/d9DTiGPUjEGAwnp+t29vTcnhPiTeu4BRSliXdlaU/EEcblOlQVcI+CZJQ8QUH+RBFgzWfNbmTdTbatpaGiygPnFtt2jm0ALNNAvzvt1a5YjDwV6KKhNIfM60mxNwHKSmRsykkw6tzOnBjUM0uHdbFlQl+DLHERR2zokM9GsxGt+na0u6H+5DCcFeWXEBbp3KE3TlUj2SJ43+aFZW9qTqulo4f3wTzExKTql68fodFHIFnhXSVZO1/JZ47ymUSsKTF69LHOvQwQYu2+YgWyaXZWRmNbOwd7xW5QKJz5BaJidKRHTjwgElk71dVQer/t1b0+A+7Vh2K4TpPuW83yF92lNymA8lBHvJ4wLcOn5V0ysDkXmCKpOZybRbs3o1enD9ED26eZh0dbRYwKk38O0oKVRM537byGh8+1bmKnE4PcrXxfibAl0e5vsuUSKiW5cPkmWzkqUW48b1qJlJTpRrZ2PyzYOuuaWhuk6isM7yH/uIAR1V4rAy1t+t9TcFuoLM5zYhIVhE8YEeZMow4OjXvRU9j/qZBvRs882Cbt7UgfTk9jH1ce8lB2kWqhqJ9zF+rlYVDTpuRTOfVVfHX+Vy+XcCQe6RnM2alPi5pPvPkHT/GW5FSb/ZQOJWtBTS5CdIfviixLFD+3XAz65LoFQqKTtb1s6q67yYrzaQ+Aydb0JCsBdF+boyllq4XA4tmj2Exg9z+Gb0uXnTBlIve1vGn+nasbkqkEiL9Xdr9U1Pr8WBTxoiprvXPBmt+Qzq6VFSqJjO/rrhmwBedV1tuh9+iIJO72K4JGlNd655UkKwl7zS5tNVIpF5YqJERLcuHaRmDKSWZsYNqJFB7ZwKqBamXy3oejnksJxR43okFApKHN+/R5u84nB7FnQMwBfj5zouUSIiaYiYcUqV28659DTi2FeZybx15XeUFCqmEQM6Me4GcDeH6TIjrjg3YkH3aVLLxESJiG5eOqhkktVibtqQ9m+aRVqawq8OeKaG9emYy48FDr0pzIf376gWh2NZpvtskTmX+URq/a4kF/B5dObn9TR6SOcqD7jNyyfRhqUTGI/v1MaScjO/0yr7mo5bmZnPquu843K5fBIR8Kd4FazNSpZaqutqw8SwPvp2bVVkj+GqYu1aNkNvBzto5ul8WpSNGtQZv7gtAYhIJpN3tun+w71vXjIpBZF5bEKwiOKDPBnl55kYGuSeIysk+/bWVY7pOre1Ii6XS7UZTK2q8Umh3pQQ7JUa4+dqwU6vpRvtjpeGiCnW352szQ0ZFY1f+mMLPbp5hKzNm1QZ0K38YTQ9jThG2xhWgw0f0JESgkWUEOyljPYrnWJr1grqfOMSgr2UUb6ujDoWNDUyoK0rvyMABQ7+qIyuusb9m2Yxqrbr0cWWksPV4nBLFnRlC76xud2KyNqMGZMJBXwK+c+Jxg93qLQ7Egc2zaK/fdYw6mMCgAb0bKOSTGSRV50bVzXQVZnVd552Gb9nZ8smcDgc/Oq+jMxNG5X4WaMm9SAU8mBqaFApf5tQwIeOjhZq1qiW/xyOImxw73bw2OkIDoeTJZPLOzXvPv8BG0iUn8g8JlEioqRQb7JkoPOpegQbN6lHk0d3rzRMN3fKhxZhTGpfu3duoTo7OCPi8sEG7PRaMSLz+Nx8PqUVg2mXx+PS2V83UFKomBo1qFPhwGvZ3JRex/5GPk4LGY0fNahzXnG4DQu6ihWZx3zK9ppF00bkOGVA7mJeo8KYTrXDsnzeSEaf6dLOSiUOp0ZedW7Cgq5ySC3jEiU5dbs2FkYMN93t6PYVZ5oypkc59zLJCSSu/bOb9OvUZPSZsUPsVUmcihh/t+ZfA+iqtLSvapdhYT/3eFZW9hgtTQ385b0K5qYlV689fPwSSoUSyY9elOs1Ewgv36TifXom3qS8L3G8Qwcb7Fw7FQqFMjUrW2Zl5eAYyQYSlY/5xkhDxBQX6EHNGTCfSjfr271VuTQKct8xlyzNcgIhLQZF6qMGdVZNr4ror63Y+ivU+UbnqviMu8tfP7+P7oV5k0G9WmUGuhZWxnQvzJt+cVvKqK1En64tVeLw+1h/N1sWdFUEfNKQnKwWGwbba02NDahB/ZxkUkMGRyd8inO5HDWzDujVhtEOyqDe7VTicHbElYMNWdBVsWk3USKiiCvOSiZp9Kqp7WnEMXXL1tLwVfNH070wb7KzZlaOOaxfB1XmcGZswDdegliFpZZRiRIR3QvzJiY6n1HjehQf6EGGjUrv7DU7GxPy/XM7o7E97W3Vxda3Lx+szz7BKsx88YEfmM/GouRpl2nnzNL2sUMdVOKwjN3w/8qYLylUTEyn3fJ0hw42JA0RUUKw17uquOHPWvFrvtGJEhFF+7pSC0vjSgO68cO7fhCHK0GFP2tlw3wjE4K9lHcY1u2WtXfv3EJ1TFMKm8T59UstI6UhYroT5Em2VsZFJhMscxzxxcCaPLro7bgxg7uoxGF5lC9bbP3NgC8h2EsZ4+dGVh8xn6aGgE4dXkvJ4T40e1K/z9TuuOS0cSa9jv2N9m6YUVyx9fvYr2TvlTWG4Ivxcx2hCjhaWBqpARN4aiclhXrTvTBvuhfqTesWj/2MrbF5JA0R070wb5KGiGnv+hn5miLm5tNlqSQTFnTfXsAxSiW1tG7RlP4nWknJ4T60ffUU0tXRpoCTO+l+uA/9MH0QI8DxuFxy3T6XXsX8SsdcllCdWtXpD68V9CzyZ9rz03QaN8zhgzj8jUsmnG8dhDF+riM0NIR/crkccDgcXPS/hVlLnAEAfB4PoWedUEtPB+6Hz2KX2wlQMYcdumybg/49WiMoLBaT5zsBAITCnPZrqn7PSqUy/X1ahrFdr4XPv+X7zv2Wf3xu0fhfMpl8VO7xmeTkcVL9vlyhQO+xa/H0+VvMmz4Qy+eNKvwmcjnwdlqAcUPt4SeJwswfD6rfy86W49S5EAj4PCiVSrlMJu9o12vhc3Z6ZU3FfMNVNRwfV69xOVySnNlD98K8acHMIQWm2H2bZlFymA/94rq0UMkktyouJZJtoMNaEWu+EYkSEcX4uxXY1K+tp0s3Lhyg++E++fqZHD6wiJ7cPkqiPfOJz8t/1P2kkd0pKQd08hg/V3MWdKwVy3wJwV7Ku9cK1u3yeTzyP7mD7oV504wJfWjvhhn0+NYR+tVtqfqITuRJrc8Vh99G+7k2ZUHHWonMF+vvNlwaIqK717wKMF91XW26fdmZpCEiSg73Ia89PxSYXscNdVCJwzJ275W1TwZfQrCXMsbfrUAyqZaGkAJO7aRfXJcSj8vNx3SD+7RTSSapsf5uNizoWPtk8MX4uQ5T5fN9vL2mU02TuNz8KexD+3Wguzn5dJm3Lh3QZ0HH2pcEHMNzSyeVzS2Nismns1cxXQZbI8FaaQUcwxIlIrp//RAVBr6+3VqpWoW9v3lxf132jrFW6swX5euqzBtwTB7dQ3XIsIxd07FWVsw3VLXmszZrQj272KrE4bcRV5zZBjqfYBz2FjBnPksHR8QFuA8TCPh/v0tNh46OFjiAIitbZmHddV4CW+HPWlkz35BEiUiZEOz1JtrPla3wZ61c13wjo7+1815L0f4PPMCym2hf8hQAAAAASUVORK5CYII=';
				_options+='<div class="item-option" data-value="'+_v+'">';
				_options+='	<div>';
				_options+='		<span class="item-title">'+_v.substring(_v.indexOf(' ')+1,_v.indexOf(' - '))+'</span>';
				_options+='		<span>'+_vs[1].replace(/\|/g,', ')+'</span>';
				_options+='	</div>';
				_options+='	<div style="text-align:right"><img src="'+_img+'"/></div>';
				_options+='<button class="selectlocal">Select</button></div>';
			});
			$('.box-content').html(_options);
			//$('.box-pupop-content').height($(window).height()*(1-0.25));
			$('.box-pupop').show();
			$(document).on('click','.item-option',function(){
				var _that = $(this);
				$('.item-option').removeClass('active');
				$(this).addClass('active');
				$('#pick_up_location').val(_that.data('value'));
				$('#pickselect').html('<div class="item-option active">'+_that.html().replace('<button class="selectlocal">Select</button>','')+'</div>');
			})
			$(document).on('click','#selectlocal,.selectlocal',function(){
				if($('.box-content .item-option.active').length!=1)
				{
						alert('Please select a boutique');
				}else{
					$('.box-pupop').hide();
				}
			})
			// customer
			$('#shipping_company_field,#shipping_mobile_field').hide();
			$('#shipping_address_1_field').hide();
			$('#shipping_address_2_field').hide();
			$('#shipping_state_field').hide();
			$('#shipping_city_field').hide();

			//reset customer address
			if (action == 'onchange' && !window.customer_province == !$('#shipping_state').val() && !window.customer_city == $('#shipping_city').val()) {
				$('#shipping_address_1').val(window.customer_address_1);
				$('#shipping_state').val(window.customer_province).trigger('change');
				setTimeout(function(){
				 	$('#shipping_city').val(window.customer_city).trigger('change');
				 	$("input[name='checkout_next_step']").removeAttr('disabled');
				 	document.body.style.cursor='default';
				 }, 2000);
			} else {
				$("input[name='checkout_next_step']").removeAttr('disabled');
				document.body.style.cursor='default';
			}

		} else	if (delivery_option == 'customer_address') {
			$('#shipping_company_field,#shipping_mobile_field').show();
			$('#pickselect').empty();
			$('.payment_method_cod').show();
			$('.flat_rate').click();
			$(".express_label,.local_pickup_label").hide();
			$(".flat_rate_label").show();
			// pickup
			$('#pick_up_location_field').hide();
			// customer
			$('#shipping_address_1_field').show();
			$('#shipping_address_2_field').show();
			$('#shipping_state_field').show();
			$('#shipping_city_field').show();
			$("input[name='checkout_next_step']").removeAttr('disabled');
			document.body.style.cursor='default';
		}else{
			$('#shipping_company_field,#shipping_mobile_field').show();
			$('#pickselect').empty();
			$('.payment_method_cod').hide();
			$('.express_label').click();
			$(".flat_rate_label,.local_pickup_label").hide();
			$(".express_label").show();
			// pickup
			$('#pick_up_location_field').hide();
			// customer
			$('#shipping_address_1_field').show();
			$('#shipping_address_2_field').show();
			$('#shipping_state_field').show();
			$('#shipping_city_field').show();
			$("input[name='checkout_next_step']").removeAttr('disabled');
			document.body.style.cursor='default';
		}

	}, // deliveryOption()

} // Checkout
