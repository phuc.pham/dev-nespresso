
/**
 * my-infos.js
 */

$(document).ready(function() {
	MyInfos.init();
});

var MyInfos = {

	that: null,

	init : function() {

		this.form = $('#form-my-infos');
		this.year = this.form.find('[name="year"]');
		this.month = this.form.find('[name="month"]');
		this.day = this.form.find('[name="day"]');
		this.first_name = this.form.find('[name="first_name"]');
		this.title = this.form.find('[name="title"]');
		this.middle_name = this.form.find('[name="middle_name"]');
		this.last_name = this.form.find('[name="last_name"]');
		this.date_of_birth = this.form.find('[name="date_of_birth"]');
		// this.email = this.form.find('[name="email"]');
		// this.email_confirmation = this.form.find('[name="email_confirmation"]');
		this.mobile = this.form.find('[name="mobile"]');
		this.country_code = this.form.find('[name="country_code"]');
		this.phone = this.form.find('[name="phone"]');

		this.btn_change_pass = $('#btn-change-password');

		that = this;

		this.ready();

	}, // init()

	ready : function() {

		$('[name="billing_state"]').parent().parent().parent().addClass('is-dirty');
		$('[name="billing_city"]').parent().parent().parent().addClass('is-dirty');

		this.form.on('submit', function(e) {

			that.addDateOfBirthValue();

			// check if form my infos is valid
			$validForm = that.validFormMyInfos();

			if ( !$validForm ) {
				e.preventDefault();
				return false;
			}

			return true;
		});

		this.btn_change_pass.on('click', function() {

			// check if form my infos password is valid
			$validForm = that.validFormMyInfosPassword();

			if ( $validForm  ) {
				var current_url = window.location.href;
				if (current_url.includes('vi/my-account/personal-information')) {
					var alert_text = "Bạn phải đăng nhập trở lại sau khi thay đổi mật khẩu. Thực hiện?";
				} else {
					var alert_text = "You'll need to relogin after changing your password. Proceed?";
				}
				swal({
					title: "Password update warning",
					text: alert_text,
					type: "warning",
					showCancelButton: true,
					confirmButtonText: "Yes, proceed",
					cancelButtonText: "No",
				},
				function(isConfirm){
					if (isConfirm) {
						$('#form-my-infos-password').submit();
					}
				});
			} // endif

		}); // this.btn_change_pass

	}, // ready()

	/**
	 * add date of birth value to hidden input
	 */
	addDateOfBirthValue : function() {

		var year = this.year.val();
		var month = parseInt( this.month.val() ) < 10 ? '0' + this.month.val() : this.month.val();
		var day = parseInt( this.day.val() ) < 10 ? '0' + this.day.val() : this.day.val();

		this.date_of_birth.val( year + '-' + month + '-' + day );

	}, // addDateOfBirthValue()

	/**
	 * check if form my infos is valid
	 * aka personal information form
	 *
	 * @return {[type]} [description]
	 */
	validFormMyInfos : function() {

		$validation = new Validation();

		if ( !$validation.validate_title( this.title ) )
			return false;

		if ( !$validation.validate_first_name( this.first_name ) )
			return false;

		if ( !$validation.validate_last_name( this.last_name ) )
			return false;

		if ( !$validation.validate_date_of_birth( this.date_of_birth ) )
			return false;

		// if ( !$validation.validate_email( this.email ) )
		// 	return false;

		// if ( !$validation.validate_email_confirmation( this.email, this.email_confirmation ) )
		// 	return false;

		if ( !$validation.validate_mobile( this.mobile ) )
			return false;

		return true;

	}, // validFormMyInfos()

	/**
	 * check if form my infos password is valid
	 * aka personal information form
	 *
	 * @return {[type]} [description]
	 */
	validFormMyInfosPassword : function() {

		$password_current = $('#form-my-infos-password').find('[name="password_current"]');
		$password = $('#form-my-infos-password').find('[name="password"]');
		$password_confirmation = $('#form-my-infos-password').find('[name="password_confirmation"]');

		$validation = new Validation();

		if ( !$validation.validate_password_current($password_current) )
			return false;

		if ( !$validation.validate_password($password) )
			return false;

		if ( !$validation.validate_password_confirmation($password_confirmation, $password) )
			return false;

		return true;
	}, // validFormPassword()

} // MyInfos

