$(document).ready(function(){
    initLoading();
});
function initLoading(){

    if(Products.loader){
        generateMinibasket();
    }else{
        setTimeout(function(){ //waiting during the api call
            initLoading();
        },1000);
    }
}
function generateMinibasket(){

    var elBasket = $(".basket");
    var activateQtyItemInBasket = false;

    if(!activateQtyItemInBasket){
        var totalItems = 0;
        for(var k in Products.list){
            totalItems+=parseInt(Products.list[k].quantity);
        }
    } else {
        var totalItems = Object.keys(Products.list).length;
    }

    elBasket.find('.nb-item-in-basket, .btn-add-qty').text(totalItems);

    if(totalItems) {
        elBasket.find('.btn-basket').addClass('btn-basket--active');
    } else {
        elBasket.find('.btn-basket').removeClass('btn-basket--active');
    }

    var el = $('.basket-dialog-product > ul');
    el.empty();

    if(Object.keys(Products.list).length>0){
        var machines = 0, accessoires = 0, gwp = 0, discovery_gifts = 0;  coys_gift = 0;

        var footerEl = $('.basket-dialog-footer');
        footerEl.removeClass('hidden');
        $('.basket-number.nb-item-in-basket').removeClass('hidden');

        //add total line
        var subtotal = footerEl.find('.basket-subtotal-price .price-int');
        var total = footerEl.find('.basket-total-price .price-int');
        var tax = footerEl.find('.basket-vat-price .price-int');

        var subtotal_formatted = footerEl.find('.basket-subtotal-price .price-int-formatted');
        var total_formatted = footerEl.find('.basket-total-price .price-int-formatted');
        var tax_formatted = footerEl.find('.basket-vat-price .price-int-formatted');

        //reinit
        subtotal.text(0);
        total.text(0);
        tax.text(0);

        //for capsules
        var headerCapsule = false;

        $.each(Products.list,function(index,product){

            // add te currency symbol
            product.currency_symbol = window.currency_symbol;
            product.currency = window.currency;
            if(product.type==='capsules' || product.type==='Coffee'){
                if(product.woosb_parent_id)
                {
                    return;
                }
                if(!headerCapsule){
                    headerCapsule = true;
                    el.append('<li><div class="basket-product-header"><p>Coffee <span>(<span class="minibasket-nb-capsules">0</span>)</span></p></div><ul class="sub-dialog-product minibasket-capsules"></ul></li>');
                }

                // add total price & formatted price
                product.total_price = new Intl.NumberFormat('en-US', {}).format(product.price * product.quantity);
                product.price_formatted = new Intl.NumberFormat('en-US', {}).format(product.price);
                el.find('.minibasket-capsules').loadTemplate($("#minibasket-line"), product,{
                    append:true,
                    overwriteCache: true,
                    success: function(){

                        var nbTotal = parseInt(el.find('.minibasket-nb-capsules').text());
                        el.find('.minibasket-nb-capsules').text(nbTotal+1);

                        var oldPrice = parseFloat(subtotal.text());
                        var newPrice = oldPrice + ( product.price * product.quantity );

                        var vatTax = parseFloat(newPrice*VAT).toFixed(0);
                        tax.text(vatTax);

                        subtotal.text(parseFloat(newPrice).toFixed(0));

                        total.text(parseFloat(newPrice*VAT).toFixed(0));
                        total_formatted.text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));

                        elBasket.find('.basket-price').text(parseFloat(newPrice*VAT).toFixed(0));
                        elBasket.find('.basket-price-formatted').text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));
                    },
                    error: function(){
                        console.log('Something went wrong. Please ask the administrator for more information.');
                    }
                });
            }else if(product.type==='Machine'){
                machines++;
            }else if(product.type==='Accessory'){
                accessoires++;
            }else if(product.type==='GWP Gift' || product.type==='Others'){
                gwp++;
            }else if(product.type==='Discovery Gift'){
                discovery_gifts++;
            }else if(product.type==='COYS Gift'){
                coys_gift++;
            }

        });

        if(machines > 0){ //quick fix
            var headerMachine = false;
            $.each(Products.list,function(index,product){
                if(product.woosb_parent_id)
                {
                    return;
                }
                if(product.type==='Machine'){
                    if(!headerMachine){
                        headerMachine = true;
                        el.append('<li><div class="basket-product-header"><p>Machines <span>(<span class="minibasket-nb-machines">0</span>)</span></p></div><ul class="sub-dialog-product minibasket-machines"></ul></li>');
                    }
                    // add total price & formatted price
                    product.total_price = new Intl.NumberFormat('en-US', {}).format(product.price * product.quantity);
                    product.price_formatted = new Intl.NumberFormat('en-US', {}).format(product.price);
                    el.find('.minibasket-machines').loadTemplate($("#minibasket-line"), product,{
                        append:true,
                        overwriteCache: true,
                        success: function(){
                            var nbTotal = parseInt(el.find('.minibasket-nb-machines').text());
                            el.find('.minibasket-nb-machines').text(nbTotal+1);

                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.quantity);

                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(0);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(0));

                            total.text(parseFloat(newPrice*VAT).toFixed(0));
                            total_formatted.text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));

                            elBasket.find('.basket-price').text(parseFloat(newPrice*VAT).toFixed(0));
                            elBasket.find('.basket-price-formatted').text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));
                        },
                        error: function(){
                            console.log('Something went wrong. Please ask the administrator for more information.');
                        }
                    });
                }
            });
        }

        if(accessoires > 0){//quick fix
            var headerAccessories = false;
            $.each(Products.list,function(index,product){
                if(product.woosb_parent_id)
                {
                    return;
                }
                if(product.type==='Accessory'){
                    if(!headerAccessories){
                        headerAccessories = true;
                        el.append('<li><div class="basket-product-header"><p>Accessories <span>(<span class="minibasket-nb-accessories">0</span>)</span></p></div><ul class="sub-dialog-product minibasket-accessories"></ul></li>');
                    }
                    // add total price & formatted price
                    product.total_price = new Intl.NumberFormat('en-US', {}).format(product.price * product.quantity);
                    product.price_formatted = new Intl.NumberFormat('en-US', {}).format(product.price);
                    el.find('.minibasket-accessories').loadTemplate($("#minibasket-line"), product,{
                        append:true,
                        overwriteCache: true,
                        success: function(){
                            var nbTotal = parseInt(el.find('.minibasket-nb-accessories').text());
                            el.find('.minibasket-nb-accessories').text(nbTotal+1);

                            var oldPrice = parseFloat(subtotal.text());

                            var newPrice = oldPrice + (product.price*product.quantity);

                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(0);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(0));

                            total.text(parseFloat(newPrice*VAT).toFixed(0));
                            total_formatted.text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));

                            elBasket.find('.basket-price').text(parseFloat(newPrice*VAT).toFixed());
                            elBasket.find('.basket-price-formatted').text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));
                        },
                        error: function(){
                            console.log('Something went wrong. Please ask the administrator for more information.');
                        }
                    });
                }
            });
        }

        //for gwp (gifts)
        if(gwp > 0){ //quick fix
            var headergwp = false;
            $.each(Products.list,function(index,product){
                if(product.woosb_parent_id)
                {
                    return;
                }
                if(product.type==='GWP Gift' || product.type==='Others'){
                    gwp_gift_id = product.id;
                    if(!headergwp){
                        headergwp = true;
                        el.append('<li><div class="basket-product-header"><p>GIFT FROM NESPRESSO <span>(<span class="minibasket-nb-gwp">0</span>)</span></p></div><ul class="sub-dialog-product minibasket-gwp"></ul></li>');
                    }
                    // add total price & formatted price
                    product.total_price = new Intl.NumberFormat('en-US', {}).format(product.price * product.quantity);
                    product.price_formatted = new Intl.NumberFormat('en-US', {}).format(product.price);
                    el.find('.minibasket-gwp').loadTemplate($("#minibasket-line"), product,{
                        append:true,
                        overwriteCache: true,
                        success: function(){
                            var nbTotal = parseInt(el.find('.minibasket-nb-gwp').text());
                            el.find('.minibasket-nb-gwp').text(nbTotal+1);

                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.quantity);
                            final_sub_amount = newPrice;
                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(0);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(0));

                            total.text(parseFloat(newPrice*VAT).toFixed(0));
                            total_formatted.text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));

                            elBasket.find('.basket-price').text(parseFloat(newPrice*VAT).toFixed(0));
                            elBasket.find('.basket-price-formatted').text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));
                        },
                        error: function(){
                            console.log('Something went wrong. Please ask the administrator for more information.');
                        }
                    });
                }
            });
            //hide nespresso free info in shopping bag
        }

        //for coys (gifts)
        if(coys_gift > 0){ //quick fix
            var headergwp = false;
            $.each(Products.list,function(index,product){
                if(product.woosb_parent_id)
                {
                    return ;
                }
                if(product.type==='COYS Gift'){
                    gwp_gift_id = product.id;
                    if(!headergwp){
                        headergwp = true;
                        el.append('<li><div class="basket-product-header"><p>GIFT FROM NESPRESSO <span>(<span class="minibasket-nb-coys">0</span>)</span></p></div><ul class="sub-dialog-product minibasket-coys"></ul></li>');
                    }
                    // add total price & formatted price
                    product.total_price = new Intl.NumberFormat('en-US', {}).format(product.price * product.quantity);
                    product.price_formatted = new Intl.NumberFormat('en-US', {}).format(product.price);
                    el.find('.minibasket-coys').loadTemplate($("#minibasket-line"), product,{
                        append:true,
                        overwriteCache: true,
                        success: function(){
                            var nbTotal = parseInt(el.find('.minibasket-nb-coys').text());
                            el.find('.minibasket-nb-coys').text(nbTotal+1);

                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.quantity);
                            final_sub_amount = newPrice;
                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(0);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(0));

                            total.text(parseFloat(newPrice*VAT).toFixed(0));
                            total_formatted.text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));

                            elBasket.find('.basket-price').text(parseFloat(newPrice*VAT).toFixed(0));
                            elBasket.find('.basket-price-formatted').text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));
                        },
                        error: function(){
                            console.log('Something went wrong. Please ask the administrator for more information.');
                        }
                    });
                }
            });
            //hide nespresso free info in shopping bag
        }

        //for discovery (gifts)
        if(discovery_gifts > 0){ //quick fix
            var headerOther = false;
            $.each(Products.list,function(index,product){
                if(product.woosb_parent_id)
                {
                    return ;
                }
                if(product.type==='Discovery Gift'){
                    if(!headerOther){
                        headerOther = true;
                        el.append('<li><div class="basket-product-header"><p>Discovery Gift <span>(<span class="minibasket-nb-gift">0</span>)</span></p></div><ul class="sub-dialog-product minibasket-gift"></ul></li>');
                    }
                    // add total price & formatted price
                    product.total_price = new Intl.NumberFormat('en-US', {}).format(product.price * product.quantity);
                    product.price_formatted = new Intl.NumberFormat('en-US', {}).format(product.price);
                    el.find('.minibasket-gift').loadTemplate($("#minibasket-line"), product,{
                        append:true,
                        overwriteCache: true,
                        success: function(){
                            var nbTotal = parseInt(el.find('.minibasket-nb-gift').text());
                            el.find('.minibasket-nb-gift').text(nbTotal+1);

                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.quantity);

                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(0);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(0));

                            total.text(parseFloat(newPrice*VAT).toFixed(0));
                            total_formatted.text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));

                            elBasket.find('.basket-price').text(parseFloat(newPrice*VAT).toFixed(0));
                            elBasket.find('.basket-price-formatted').text(new Intl.NumberFormat('en-US', {}).format(parseFloat(newPrice*VAT).toFixed(0)));
                        },
                        error: function(){
                            console.log('Something went wrong. Please ask the administrator for more information.');
                        }
                    });
                }
            });
            //hide nespresso free info in shopping bag
        }

    }else{

        var shopping_url = $('.basket-dialog').attr('data-start-shopping-url');
        var reorder_url = $('.basket-dialog').attr('data-reorder-url');
        var text = ''
                +'<div class="sbag-no-product sbag-no-product-center"> <div class="title">Your basket is empty</div> <a href="'+ shopping_url +'" title="Start shopping" class="btn btn-secondary btn-green btn-block">Start Shopping</a><a title="Re-order" class="btn btn-secondary btn-green btn-block" id="btn-reorder" data-val="'+check_last_order+'">Reorder</a></p>'
                + '<div class="basket-100"><p><b>FREE <i>Nespresso</i> Touch Travel Mug</b> for the first 100 orders worth ₫1,500,000 and up.</p><p>Add the item to your shopping bag and use code: <b>FIRST100</b> upon checkout. </p></div>';
        el.html(text);
        $('.basket-dialog-footer').addClass('hidden');
    }
} //generateMinibasket()
