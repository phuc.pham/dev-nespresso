/**
 * discover-nespress.js
 */

$(document).ready(function() {

	var init_translateX = 0;

	if($(window).width() > '740') {
		$("html").addClass("desktop");	
	} else {
		$("html").addClass("mobile");
	}
	
	
	$('a[href^="#"]').on('click', function(event) {

	    var target = $(this.getAttribute('href'));
	    target.slideDown();
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top - 79
	        }, 500);
	    }

	});

	//reset

	$('#productToggle0').prop('checked', true);
	$('#productToggle1').prop('checked', false);

	$("input[name='colorToggle0']").removeAttr('checked');
	$("input[name='colorToggle1']").removeAttr('checked');
	$('#radio_colorToggle1_0').prop('checked', true);
	$('#radio_colorToggle1_0').prop('checked', true);

	$("input[name='productToggle']").on('click',function () {
		var coffee_color = $(this).val();
		
		$('.citiz-main-product-color').removeClass('v_bullets0');
		$('.citiz-main-product-color').addClass('hide');
		$('.citiz-main-product').addClass('hide');

		$('.citiz-price').addClass('hide');
		$('.citiz-price-'+coffee_color).removeClass('hide');

		$('.citiz-variation-'+coffee_color).removeClass('hide');
		
		$('.citiz-main-product-color-'+coffee_color).removeClass('hide');
		$('.citiz-main-product-color-'+coffee_color).addClass('v_bullets0');
	});

	//black variation 1
	$("input[name='colorToggle0']").on('click',function () {
		var variation_color = $(this).val();
		
		$('.variation-color-1').removeClass('v_activeColor');
		$('.variation-1-color-'+variation_color).addClass('v_activeColor');
		
	});

	//black variation 1
	$("input[name='colorToggle1']").on('click',function () {
		var variation_color = $(this).val();
		$('.variation-color-2').removeClass('v_activeColor');
		$('.variation-2-color-'+variation_color).addClass('v_activeColor');
		
	});

    $('.faq-item').on('click', function() {
    	if($(this).attr('aria-expanded') == 'false') {
    		$(this).attr('aria-expanded', true);
    		$(this).addClass('v_open');
    	} else {
    		$(this).attr('aria-expanded', false);
    		$(this).removeClass('v_open');
    	}
    	
    });


    $('.addyourmilk-item').on('click', function() {

    	$('.addyourmilk-item').removeClass('v_open');
    	$('.addyourmilk-item').attr('aria-expanded', true);
    	$(this).addClass('v_open');
    	$(this).attr('aria-expanded', true);

    	var v_lens = $(this).data('touch');
    	$('#v_lens').removeClass();
	    $('#v_lens').addClass('v_lens ' + v_lens);
	    	
    });

    $('.v_sliderNext').on('click', function () {
		init_translateX = init_translateX + (-199.2);
		var total_machine = $('.nepresso-machine-product').length;
		$('.v_slide').css( 'transform','translateX('+init_translateX+'px)');
	});

	$('.v_sliderPrev').on('click', function () {
		init_translateX = init_translateX - (-199.2);
		$('.v_slide').css( 'transform','translateX('+init_translateX+'px)');

	});

	$('.view_buy').on('click', function () {
		var win = window.open($(this).data('link'), '_blank');
	});


	var modal = $('#modal-container');
	var modal_img = $('#modal_img');
	// Get the image and insert it inside the modal - use its "alt" text as a caption
	$('.image-modal').on('click', function (){
		modal_img.removeClass('out');
		modal.css('display','block');
		modal_img.prop('src',$(this).data('img'));
	});

	// When the user clicks on <span> (x), close the modal
	$('#modal-container').on('click', function (){
	    modal_img.addClass('out');
	    setTimeout(function() {
	       modal.css('display','none');
	       modal_img.addClass('modal-content');
	     }, 400);
	    
	 });


	var modal_v = $('#video-modal-container');
	var modal_video = $('#modal_video');
	// Get the image and insert it inside the modal - use its "alt" text as a caption
	$('.video-modal').on('click', function (){
		modal_video.removeClass('out');
		modal_v.css('display','block');
		
		play_video('playVideo');
	});

	// When the user clicks on <span> (x), close the modal
	$('#video-modal-container').on('click', function (){
	    modal_img.addClass('out');
	    setTimeout(function() {
	       modal_v.css('display','none');
	       modal_img.addClass('modal-content');
	     }, 400);

	    play_video('pauseVideo');
	    

	 });

	var nespresso_machine_count = $('.v_imageContainer').length;
	console.log(nespresso_machine_count);
	var max_next = nespresso_machine_count-2;
	var max_prev = -1;
	var nespresso_machine_counter = 0;
	$('.v_sliderNext').on('click', function () {
		nespresso_machine_counter++;
		if(nespresso_machine_counter == (max_next)) {
			$('.v_sliderNext').css('display', 'none');
		}
		if(max_prev < nespresso_machine_counter) {
			$('.v_sliderPrev').css('display', 'block');
		}
	});
	$('.v_sliderPrev').on('click', function () {
		nespresso_machine_counter--;
		if(max_next > nespresso_machine_counter) {
			$('.v_sliderNext').css('display', 'block');
		}
		if(max_prev == nespresso_machine_counter) {
			$('.v_sliderPrev').css('display', 'none');
		}

	});
    
});

function play_video(func) {
	var div = document.getElementById("video-modal-container");
    var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
    iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
}
