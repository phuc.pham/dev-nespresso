$(document).ready(function(){
    initLoading();
});
function initLoading(){
    if(!Products.loader){
        generateShoppingBag();
    }else{
        setTimeout(function(){ //waiting during the api call
            initLoading();
        },1000);
    }
}
function generateShoppingBag(data){
    var el = $(".sbag-list-content");
    el.empty();
    el.removeClass('sbag-loading');

    if(Object.keys(Products.list).length>0){
        var machines = 0, accessoires = 0;
            
        $('.sbag-resume-content').removeClass('hidden');
        $('.sbag-footer').removeClass('hidden');
        $('.sbag-before-load').removeClass('hidden');
        
        var subtotal = $('.sbag-subtotal-price-int');
        var total = $('.sbag-total-price-int');
        var tax = $('.sbag-val-price-int');
        //reinit
        subtotal.text(0);
        total.text(0);
        tax.text(0);
        
        //for capsules
        var headerCapsule = false;
        $.each(Products.list,function(index,product){
            if(product.type==='capsules' || product.type==='Coffee'){
                if(!headerCapsule){
                    headerCapsule = true;
                    el.append('<div class="sbag-list-title">Capsules <sup>(<span class="sbag-nb-capsules">0</span>)</sup><div class="sbag-list-title-col"><span class="sbag-head-unit-price">Unit Price</span><span class="sbag-head-qty">Quantity</span><span class="sbag-head-total">Total</span></div></div><div class="sbag-capsules"></div>');
                }
                el.find('.sbag-capsules').loadTemplate($("#minibasket-line"),{
                    defaultValue:null
                },{
                    append:true,
                    overwriteCache: true,
                    success: function(){
                        var nbTotal = parseInt(el.find('.sbag-nb-capsules').text());
                        el.find('.sbag-nb-capsules').text(nbTotal+1);
                        
                        var oldPrice = parseFloat(subtotal.text());
                        var newPrice = oldPrice + (product.price*product.qty);
                        
                        var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(2);
                        tax.text(vatTax);
                        
                        subtotal.text(parseFloat(newPrice).toFixed(2));
                        
                        total.text(parseFloat(newPrice*VAT).toFixed(2));
                    },
                    error: function(){
                        console.log('Erreur durant le chargement du Tpl');
                    }
                });
            }else if(product.type==='machines'){
                machines++;
            }else if(product.type==='accessories'){
                accessoires++;
            }
        });

        if(machines > 0){ //quick fix 
            var headerMachine = false;
            $.each(Products.list,function(index,product){
                if(product.type==='machines'){
                    if(!headerMachine){
                        headerMachine = true;
                        el.append('<div class="sbag-list-title">Machines <sup>(<span class="sbag-nb-machines">0</span>)</sup><div class="sbag-list-title-col"><span class="sbag-head-unit-price">Unit Price</span><span class="sbag-head-qty">Quantity</span><span class="sbag-head-total">Total</span></div></div><div class="sbag-machines"></div>');
                    }
                    el.find('.sbag-machines').loadTemplate($("#minibasket-line"),{
                        defaultValue:null
                    },{
                        append:true,
                        overwriteCache: true,
                        success: function(){   
                            var nbTotal = parseInt(el.find('.sbag-nb-machines').text());
                            el.find('.sbag-nb-machines').text(nbTotal+1);
                        
                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.qty);

                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(2);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(2));

                            total.text(parseFloat(newPrice*VAT).toFixed(2));
                        },
                        error: function(){
                            console.log('Erreur durant le chargement du Tpl');
                        }
                    });
                }
            });
        }

        if(accessoires > 0){//quick fix
            var headerAccessories = false;
            $.each(Products.list,function(index,product){
                if(product.type==='accessories'){
                    if(!headerAccessories){
                        headerAccessories = true;
                        el.append('<div class="sbag-list-title">Accessories <sup>(<span class="sbag-nb-accessories">0</span>)</sup><div class="sbag-list-title-col"><span class="sbag-head-unit-price">Unit Price</span><span class="sbag-head-qty">Quantity</span><span class="sbag-head-total">Total</span></div></div><div class="sbag-accessories"></div>');
                    }
                    el.find('.sbag-accessories').loadTemplate($("#minibasket-line"),{
                        defaultValue:null
                    },{
                        append:true,
                        overwriteCache: true,
                        success: function(){
                            var nbTotal = parseInt(el.find('.sbag-nb-accessories').text());
                            el.find('.sbag-nb-accessories').text(nbTotal+1);
                        
                            var oldPrice = parseFloat(subtotal.text());
                            var newPrice = oldPrice + (product.price*product.qty);

                            var vatTax = parseFloat(newPrice*VATREDUCE).toFixed(2);
                            tax.text(vatTax);

                            subtotal.text(parseFloat(newPrice).toFixed(2));

                            total.text(parseFloat(newPrice*VAT).toFixed(2));
                        },
                        error: function(){
                            console.log('Erreur durant le chargement du Tpl');
                        }
                    });
                }
            });
        }
    }else{
        el.html('<span class="sbag-no-product">Please select at least one product</span>');
    }
    
}