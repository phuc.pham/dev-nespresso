/**
 * discover-nespress.js
 */

$(document).ready(function() {

	var view_demo1 = false;
	var view_demo2 = false;
	var view_demo3 = false;
	var view_demo4 = false;
	var init_translateX = 0;

	if($(window).width() > '740') {
		$("html").addClass("desktop");	
	} else {
		$("html").addClass("mobile");
	}
	
	
	$('a[href^="#"]').on('click', function(event) {

	    var target = $(this.getAttribute('href'));
	    target.slideDown();
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top - 79
	        }, 500);
	    }

	});

	//reset
	$('#productToggle0').prop('checked', true);
	$('#productToggle1').prop('checked', false);

	$("input[name='colorToggle0']").removeAttr('checked');
	$("input[name='colorToggle1']").removeAttr('checked');
	$('#radio_colorToggle1_0').prop('checked', true);
	$('#radio_colorToggle1_0').prop('checked', true);


	$("input[name='productToggle']").on('click',function () {
		var coffee_color = $(this).val();
		
		$('.essenza-main-product-color').removeClass('v_bullets0');
		$('.essenza-main-product-color').addClass('hide');
		$('.essenza-main-product').addClass('hide');

		$('.essenza-variation-'+coffee_color).removeClass('hide');
		
		$('.essenza-main-product-color-'+coffee_color).removeClass('hide');
		$('.essenza-main-product-color-'+coffee_color).addClass('v_bullets0');
	});

	//black variation 1
	$("input[name='colorToggle0']").on('click',function () {
		var variation_color = $(this).val();
		
		$('.variation-color-1').removeClass('v_activeColor');
		$('.variation-1-color-'+variation_color).addClass('v_activeColor');
		
	});

	//black variation 1
	$("input[name='colorToggle1']").on('click',function () {
		var variation_color = $(this).val();
		$('.variation-color-2').removeClass('v_activeColor');
		$('.variation-2-color-'+variation_color).addClass('v_activeColor');
		
	});

	$(window).on('scroll', function () {
		if($('.description_demo').is(':within-viewport')) {
			var demo_number = $('.description_demo').withinviewport().data('demo');
			
			switch(demo_number) {
				case 1 :
					if(!view_demo1) {
						view_demo1 = true;
				        play_video(demo_number);
					}
				break;
				case 2 :
					if(!view_demo2) {
						view_demo2 = true;
				        play_video(demo_number);
					}
				break;
				case 3 :
					if(!view_demo3) {
						view_demo3 = true;
				        play_video(demo_number);
					}
				break;
				case 4 :
					if(!view_demo4) {
						view_demo4 = true;
				        play_video(demo_number);
					}
				break;
			}
		}
        
    });

    $('.faq-item').on('click', function() {
    	if($(this).attr('aria-expanded') == 'false') {
    		$(this).attr('aria-expanded', true);
    		$(this).addClass('v_open');
    	} else {
    		$(this).attr('aria-expanded', false);
    		$(this).removeClass('v_open');
    	}
    	
    });

    $('.v_sliderNext').on('click', function () {
		init_translateX = init_translateX + (-199.2);
		var total_machine = $('.nepresso-machine-product').length;
		$('.v_slide').css( 'transform','translateX('+init_translateX+'px)');

	});

	$('.v_sliderPrev').on('click', function () {
		init_translateX = init_translateX - (-199.2);
		$('.v_slide').css( 'transform','translateX('+init_translateX+'px)');
	});

	$('.view_buy').on('click', function () {
		var win = window.open($(this).data('link'), '_blank');
	});

	var nespresso_machine_count = $('.v_imageContainer').length;
	var max_next = nespresso_machine_count-2;
	var max_prev = -1;
	var nespresso_machine_counter = 0;
	$('.v_sliderNext').on('click', function () {
		nespresso_machine_counter++;
		if(nespresso_machine_counter == (max_next)) {
			$('.v_sliderNext').css('display', 'none');
		}
		if(max_prev < nespresso_machine_counter) {
			$('.v_sliderPrev').css('display', 'block');
		}
	});
	$('.v_sliderPrev').on('click', function () {
		nespresso_machine_counter--;
		if(max_next > nespresso_machine_counter) {
			$('.v_sliderNext').css('display', 'block');
		}
		if(max_prev == nespresso_machine_counter) {
			$('.v_sliderPrev').css('display', 'none');
		}

	});
    
});

function play_video(demo_number) {
	$('#video_scene_'+demo_number).get(0).play();
	var video_duration = $('#video_scene_'+demo_number).get(0).duration % 60 * 1000 + 100;
	
	setTimeout( function () {
		$('.image_demo_'+demo_number).removeClass('hide');
		$('.description_demo_'+demo_number).css('opacity', 1);
	}, video_duration);
}