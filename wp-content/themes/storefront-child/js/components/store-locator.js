/**
 * Init
 */
$(document).ready(function() {

    StoreLocator.init();

});

/**************************************************
 * StoreLocator (singleton)
 ***************************************************/
var StoreLocator = {

    init : function() {

        this.$container = $('.slocator');
        this.$resultsList = $('.slocator-list', this.$container);
        this.$switchers = $('.slocator-switchers', this.$container);

        // Create a map object and specify the DOM element for display.
        var _map = new google.maps.Map(document.getElementById('slocator-map'), {
            // center: {lat: -34.397, lng: 150.644},
            center: {lat: 15.040821, lng: 120.678039},
            scrollwheel: false,
            zoom: 20
        });

        this.initList();
        this.addEventsSwitcher();
        this.addEventsDetail();

    },


    initList : function () {

        var _$locationContainer = $('.slocator-locations', this.$resultsList);
        _$locationContainer.niceScroll();
    },

    addEventsSwitcher : function () {

        this.$tabs = $('.slocator-switchers__tab', this.$switchers);


        this.$tabs.each(function() {

            var _$linkedElement = $($(this).data('show'));

            if(_$linkedElement.length > 0 ) {
                _$linkedElement.addClass('slocator-switchers__view');
            }
        });

        this.$views = $('.slocator-switchers__view');

        var _self = this;

        //listener click
        this.$tabs.on('click', function () {
            _self.showTab($(this));
        });

        //show first
        this.showTab(this.$tabs.first());
    },

    addEventsDetail : function () {
        var _$details = $('.slocator-locations__details', this.$container);

        _$details.on('click', function () {
            if($(this).hasClass('active')){
                $(this).removeClass('active');
            }
            else {
                $(this).addClass('active');
            }
        });

    },

    showTab : function ($tab) {
        this.$views.removeClass('active');
        this.$tabs.removeClass('active');

        $tab.addClass('active');
        this.$views.filter($tab.data('show')).addClass('active');
    }


};
