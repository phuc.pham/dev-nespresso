
/**
 * register.js
 */

(function($) {

	$(document).ready(function() {
		$("input[name='has_machine_registered']").change(function(){
			custom_registration_step_3();
		});
		$('.product__info__colours__list')
			.on('click', 'a', function() {
				var selected_machine = machine_products[$(this).attr('data-key-name')];
				$(this).selectColorVariant(selected_machine); // select a color variant
			})
		;

		var registration_step = 1;
		var registration_step_two = false;
		var registration_step_three = false;
		var registration_step_four = false;

		// going for the next step of registration
		$('.btn-next-step').click(function() {

			// validate the current step of registration
			$validStep = $(this).validateThisStep();

			if ( $validStep ) {
				//gtm
				registration_step++;
				if(registration_step == 2 && !registration_step_two) {
					// registration_step_two = true;
					registration_step_2();
				} else if(registration_step == 3 && !registration_step_three) {
					// registration_step_three = true;
					registration_step_3();
				} else if(registration_step == 4 && !registration_step_four) {
					// registration_step_four = true;
					registration_step_4();
				}
				$(this).nextStep();
			}
		});

		// going for the previous step of registration
		$('.btn-previous-step').click(function() {
			registration_step--;
			$(this).previousStep();
			if(registration_step == 1 && !registration_step_two) {
				// registration_step_two = true;
				registration_step_1();
			}else if(registration_step == 2 && !registration_step_two) {
				// registration_step_two = true;
				registration_step_2();
			} else if(registration_step == 3 && !registration_step_three) {
				// registration_step_three = true;
				registration_step_3();
			} else if(registration_step == 4 && !registration_step_four) {
				// registration_step_four = true;
				registration_step_4();
			}
		});

		// show hide the machine selector for step 3
		$('input[name="has_machine_registered"]').on('click', function() {
			if ( $(this).val() == '1') {
				$('#has_machine_registered_yes').removeClass('hide');
			} else {
				$('#has_machine_registered_yes').addClass('hide');
			}
		});

		// show the machine details on click
		// step 3
		var previous_selected_machine = '';
		$('.my-expresscheckout-machine-table td').on('click', function() {
			if (previous_selected_machine != '') {
				previous_selected_machine.css('background-color', '#fbfbfb');
			}
			previous_selected_machine = $(this);
			$(this).css('background-color', '#ccc');
			$(this).showMachineDetails();
		});

		// show registration error
		// just get the first one
		if ( typeof registration_errors !== 'undefined') {
			if (typeof registration_errors['username_exists'] !== 'undefined' ||
				typeof registration_errors['email_exists'] !== 'undefined'
			) {
				sweetAlert('Error', 'Email is already registered', 'error');
			}
		}

		$('input[name=first_name]').on('keyup', function () {
			$(this).val($(this).val().charAt(0).toUpperCase() + $(this).val().slice(1));
		});

		$('input[name=middle_name]').on('keyup', function () {
			$(this).val($(this).val().charAt(0).toUpperCase() + $(this).val().slice(1));
		});

		$('input[name=last_name]').on('keyup', function () {
			$(this).val($(this).val().charAt(0).toUpperCase() + $(this).val().slice(1));
		});

		$('#billing_state').on('change', function () {
			$(this).getProvinceDistrict($(this).val());
		});

	}); // $(document)

	/**
	 * show the machine details on step 3
	 */
	$.fn.showMachineDetails = function() {

		var key_name = $(this).attr('data-key-name');

		// machine_products obj json from step3 script
		if ( typeof key_name === 'undefined' || typeof machine_products === 'undefined') {
			return false;
		}

		// show the machine details form
		$('#machine-details').removeClass('hide');

		$('[name="product_id"]').val(machine_products[key_name].post_id);
		$('[name="product_sku"]').val(machine_products[key_name].sku);

		var html_color_list = '';

		if ( typeof machine_products[key_name].product_color_variant !== 'undefined' &&
			machine_products[key_name].product_color_variant.length != 0
		) {
			// unhide the color container
			$('#container-product-color-variant').removeClass('hide');

			$.each( machine_products[key_name].product_color_variant, function(i, v) {

				html_color_list += `
					<li>
						<a href="javascript:void(0)"
							title="'+ v.color +'"
							class=""
							style="background: `+ v.color +`"
							data-image-id="`+ v.image_id +`"
							data-key-name="`+ key_name +`"
							data-product-color-variant-index="`+ i +`"
						>
							<input type="radio" name="color_variant_image" value="`+ v.image_id +`" class="hide">
						</a>
					</li>
				`;
			});

			$('.product__info__colours__list')
				.html( html_color_list )
			;

			var selected_machine = machine_products[key_name];
			$('.product__info__colours__list').find('a').first().selectColorVariant( selected_machine ); // initiate the first selection

		} else {
			$('img.step3-selected-img').attr('src', machine_products[key_name].image[0]); // show the product featured image instead of colored images
			$('#container-product-color-variant').addClass('hide'); // hide the color container
			$('#container-colors').remove(); // remove the colors
		}

		$('#container-product-color-variant .container-color').html(html_color_list);

	}; // showMachineDetails()

	/**
	 * select a color variant
	 * @param  {object} selected_machine
	 */
	$.fn.selectColorVariant = function(selected_machine) {

		if ( typeof selected_machine === 'undefined' || typeof selected_machine['product_color_variant'] == 'undefined' )
			return false;

		var self = $(this);
		var image_id = self.attr('data-image-id');
		var product_color_variant_index = self.attr('data-product-color-variant-index');

		$('#container-product-color-variant').find('input').removeAttr('checked');
		$('#container-product-color-variant').find('a').removeClass('active');
		$('img.step3-selected-img').attr('src', selected_machine['product_color_variant'][image_id]['image_url']);

		if ( $('[name="product_color_variant_index"]').length ) {
			$('[name="product_color_variant_index"]').val(product_color_variant_index);
		} else {
			$('#container-product-color-variant').prepend('<input type="hidden" name="product_color_variant_index" value="'+ product_color_variant_index +'">')
		}

		self.addClass('active');
		self.find('input').attr('checked', 'checked');

	} // selectColorVariant()

	/**
	 * hide current step, show next step
	 */
	$.fn.nextStep = function() {

		var currentStep = parseInt($(this).attr('data-step'));

		var currentStepDiv = $(this).parents('div[data-step="'+ currentStep +'"]');

		if ( $('div[data-step="'+ (currentStep+1) +'"]').length < 1 )
			return false;

		currentStepDiv.addClass('hide');

		$('li[data-step="'+ currentStep +'"]').addClass('completed');

		$('div[data-step="'+ (currentStep+1) +'"]').removeClass('hide');

		$('li[data-step="'+ (currentStep+1) +'"]').addClass('active');

		if ( (currentStep + 1) == 2 )
			$().copyValuesFromStep1ToStep2();

		$(window).scrollTop(100);

	}; // nextStep()

	/**
	 * hide current step, show previous step
	 */
	$.fn.previousStep = function() {

		var currentStep = parseInt($(this).attr('data-step'));

		var currentStepDiv = $(this).parents('div[data-step="'+ currentStep +'"]');

		currentStepDiv.addClass('hide');

		$('li[data-step="'+ (currentStep-1) +'"]').removeClass('completed');

		$('div[data-step="'+ (currentStep-1) +'"]').removeClass('hide');

		$('li[data-step="'+ $currentStep +'"]').removeClass('active');

		$(window).scrollTop(100);

	}; // previousStep()

	/**
	 * validate the current step of the registration
	 */
	$.fn.validateThisStep = function() {

		$currentStepDiv = $(this).parents('div[data-step]');

		$currentStep = parseInt($currentStepDiv.attr('data-step'));

		$validStep = false;

		switch ( parseInt($currentStep) ) {
			case 1:
				$().addDateOfBirthValue()
				$validStep = $(this).validateStep1();
			break;
			case 2:
				$validStep = $(this).validateStep2();
			break;
			case 3:
				$validStep = $(this).validateStep3();

				// if step is valid and this is the last step, submit the form
				if ( $validStep ) {

					$email = $('#register-form').find('[name="email"]').val();

					// add default login credentials
					$('#register-form').find('[name="user_email"]').val($email);
					$('#register-form').find('[name="user_login"]').val($email);

					// copy the billing text inputs to shipping inputs to process in backend
					$().copyBillingInputsToShippingInputs();

					$('#register-form').submit();
				}
			break;
		} // endswitch

		return $validStep;

	}; //validateThisStep()

	$.fn.copyValuesFromStep1ToStep2 = function() {

		$register_form = $('#register-form');

		$first_name = $register_form.find('[name="first_name"]');
		$last_name = $register_form.find('[name="last_name"]');
		$phone = $register_form.find('[name="phone"]');
		$email = $register_form.find('[name="email"]');
		$mobile = $register_form.find('[name="mobile"]');
		$country_code = $register_form.find('[name="country_code"]');

		$billing_first_name = $register_form.find('[name="billing_first_name"]');
		$billing_last_name = $register_form.find('[name="billing_last_name"]');
		$billing_phone = $register_form.find('[name="billing_phone"]');
		$billing_email = $register_form.find('[name="billing_email"]');

		$billing_mobile = $register_form.find('[name="billing_mobile"]');
		$billing_country_code = $register_form.find('[name="billing_country_code"]');

		if ( !$billing_first_name.val().trim() ) {
			$billing_first_name.val( $first_name.val());
			$billing_first_name.parent().parent().addClass('is-dirty');
		}

		if ( !$billing_last_name.val().trim() ) {
			$billing_last_name.val( $last_name.val() );
			$billing_last_name.parent().parent().addClass('is-dirty');
		}

		if ( !$billing_mobile.val().trim() ) {
			$billing_mobile.parent().parent().addClass('is-dirty');
		}

		$('[name="billing_state"]').parent().parent().parent().addClass('is-dirty');
		$('[name="billing_city"]').parent().parent().parent().addClass('is-dirty');

		$billing_phone.val( $phone.val() );
		$billing_email.val( $email.val() );

		$billing_mobile.val( $mobile.val() );
		$billing_country_code.val( $country_code.val() );

	}, // copyValuesFromStep1ToStep2()

	/**
	 * add date of birth value to hidden input
	 */
	$.fn.addDateOfBirthValue = function() {

		$year = $('[name="year"]');
		$month = $('[name="month"]');
		$day = $('[name="day"]');
		$date_of_birth = $('[name="date_of_birth"]');

		var year = $year.val();
		var month = parseInt( $month.val() ) < 10 ? '0' + $month.val() : $month.val();
		var day = parseInt( $day.val() ) < 10 ? '0' + $day.val() : $day.val();

		$date_of_birth.val( year + '-' + month + '-' + day );

	}, // addDateOfBirthValue()

	/**
	 * copy the billing text inputs to shipping inputs to process in backend
	 */
	$.fn.copyBillingInputsToShippingInputs = function() {

		$('[name="shipping_first_name"]').val( $('[name="billing_first_name"]').val() );
		$('[name="shipping_last_name"]').val( $('[name="billing_last_name"]').val() );
		$('[name="shipping_company"]').val( $('[name="billing_company"]').val() );
		$('[name="shipping_address_1"]').val( $('[name="billing_address_1"]').val() );
		$('[name="shipping_address_2"]').val( $('[name="billing_address_2"]').val() );
		$('[name="shipping_city"]').val( $('[name="billing_city"]').val() );
		$('[name="shipping_postcode"]').val( $('[name="billing_postcode"]').val() );
		$('[name="shipping_country"]').val( $('[name="billing_country"]').val() );
		$('[name="shipping_state"]').val( $('[name="billing_state"]').val() );
		$('[name="shipping_country_code"]').val( $('[name="billing_country_code"]').val() );
		$('[name="shipping_mobile"]').val( $('[name="billing_mobile"]').val() );
		// $('[name="shipping_ward"]').val( $('[name="billing_ward"]').val() );
		// $('[name="shipping_district"]').val( $('[name="billing_district"]').val() );

	}; // copyBillingInputsToShippingInputs()

	/**
	 * validate the first step form data
	 *
	 * @return {bool} true if valid, else false
	 */
	$.fn.validateStep1 = function() {

		$title = $('[name="title"]');
		$first_name = $('#register-form').find('[name="first_name"]');
		$middle_name = $('#register-form').find('[name="middle_name"]');
		$last_name = $('#register-form').find('[name="last_name"]');
		$date_of_birth = $('#register-form').find('[name="date_of_birth"]');
		$email = $('#register-form').find('[name="email"]');
		$mobile = $('#register-form').find('[name="mobile"]');
		$phone = $('#register-form').find('[name="phone"]');
		$password = $('#register-form').find('[name="password"]');
		$password_confirmation = $('#register-form').find('[name="password_confirmation"]');
		$has_club_membership_no = $('#register-form').find('[name="has_club_membership_no"]:checked');
		$club_membership_no = $('#register-form').find('[name="club_membership_no"]');
		$postal_code = $('#register-form').find('[name="postal_code"]');

		$validation = new Validation;

		if ( !$validation.validate_title( $title ) )
			return false;

		if ( !$validation.validate_first_name( $first_name ) )
			return false;

		if ( !$validation.validate_last_name( $last_name ) )
			return false;

		if ( !$validation.validate_date_of_birth( $date_of_birth ) )
			return false;

		if ( !$validation.validate_email( $email ) )
			return false;

		if ( !$validation.validate_mobile( $mobile ) )
			return false;

		if ( !$validation.validate_phone( $phone ) )
			return false;

		if ( !$validation.validate_password( $password ) )
			return false;

		if ( !$validation.validate_password_confirmation( $password, $password_confirmation ) )
			return false;

		// check if club membership is checked, and run some validation
		if ( parseInt($has_club_membership_no.val()) == 1 ) {

			if ( $club_membership_no.val() )
				if ( !$validation.validate_club_membership_no( $club_membership_no ) )
					return false;

			if ( !$validation.validate_billing_postcode( $postal_code ) )
				return false;

		}

        return true;

	}; // validateStep1()

	/**
	 * validate the second step form data
 	 * @return {bool} true if valid, else false
	 */
	$.fn.validateStep2 = function() {

		$billing_address_1 = $('[name="billing_address_1"]');
		$billing_postcode = $('[name="billing_postcode"]');
		$billing_city = $('[name="billing_city"]');
		$billing_state = $('[name="billing_state"]');
		$mobile = $('[name="billing_mobile"]');

		$validation = new Validation;

		if ( !$validation.validate_mobile( $mobile ) )
			return false;

		if ( !$validation.validate_billing_address_1( $billing_address_1 ) )
			return false;

		if ( !$validation.validate_billing_city( $billing_city ) )
			return false;

		if ( !$validation.validate_billing_state( $billing_state ) )
			return false;

		if ( !$validation.validate_billing_postcode( $billing_postcode ) )
			return false;

		return true;

	}; // validateStep2()

	/**
	 * validate the third step form data
	 */
	$.fn.validateStep3 = function() {

		var has_machine_registered = $('input[name="has_machine_registered"]:checked');

		if ( ! has_machine_registered.val() == 0 )
			return true;

		$serial_no = $('[name="serial_no"]');

		$validation = new Validation;

		if ( !$validation.validate_serial_no( $serial_no ) )
			return false;

		return true;

	}; // validateStep3()


	$.fn.showErrorMessage = function(msg) {
		swal('Registration error', msg, 'error');
	};

	$.fn.getProvinceDistrict = function(province_code) {
		var data = {
            action: 'get_province_district',
            province_code: province_code
        };
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: ajaxurl,
            data: data,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Something went wrong. Please ask the administrator for more information.');
            },
            success: function (data) {
                if ( data.status != 'failed') {
                	var district_select = $('#billing_city');
                	$('option', district_select).remove();
                	district_select.append(new Option('Please select district', '', true, true));
					$.each(data.districts, function(index, value) {
						district_select.append(new Option(value, value, true, true));
					});
					$('.current_district').html('Please select district')
					district_select.val('');
                } else {
                	console.log(data.message);
                }
            } // success()

        }); // ajax()

	};


})(jQuery);
