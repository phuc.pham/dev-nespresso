/**
 * Init
 */
$(document).ready(function() {

    MyEasyOrderHistory.init();

    MyEasyOrder.init();

});

var MyEasyOrderHistory = {
   
    that : null,

    init : function() {

        that = this;

        this.el = $('#my-order-history');
        this.btn_view = this.el.find('.btn-view');
        this.product_list = this.el.find('#product-list');
        this.order_details = this.el.find('#order-details');
        this.order_details_date = this.order_details.find('#order-details-date');

        if ($("#order-details").data('section') == 'order-details') {
            $('html, body').animate({
                scrollTop: $("#order-details").offset().top-100
            }, 500);
        }

        this.fireEvents();

    }, // init()

    fireEvents : function() {

        // this.btn_view.on('click', function() {
        //     that.showOrderDetails($(this));
        // });

    }, // fireEvents()

    showOrderDetails : function(btn_view_obj) {

        var order_id = btn_view_obj.attr('data-order-id');

        var date = btn_view_obj.parents('.order').find('time').html();

        // if ( typeof recent_products[order_id] === 'undefined') {
        //     this.hideOrderDetails();
        //     return;
        // }

        // this.order_details.removeClass('hide');

        // this.order_details.removeClass('hide');

        // this.order_details_date.html('Order details - ' + date);

    }, //showOrderDetails()

    hideOrderDetails : function() {

        this.order_details.addClass('hide');

        this.order_details_date.html(null);

    }, //hideOrderDetails()

}; // MyEasyOrderHistory


var MyEasyOrder = {

    init : function() {

        this.$container = $('.my-order-history');
        this.accountSidebar = $('.account-sidebar');
        this.$containerHistory = $('.my-order-history-list-container');
        this.$containerList = $('.my-order-history-list');
        this.$btnsMoreOrder = $('.more-order', this.$container);
        this.$containerHistory = $('.my-order-history-list-container');
        this.$btnsOpenDetail = $('.btn-open-detail', this.$container);
        this.$btnsCloseDetail = $('.btn-close-detail', this.$container);
        this.$containerMobile = $('.my-order-mobile', this.$container);
        this.$detailFakeAjax = $('#my-order-history-fake-ajax', this.$container).clone().removeAttr('id');
        this.$containerDetailMobile = $('.my-order-mobile-detail', this.$container);
        this.$dynamicContainer = $('.my-order-mobile-detail__dynamic', this.$container);


        this.addEventsDetail();
    },

    addEventsDetail : function () {

        var _self = this;

        this.$btnsMoreOrder.on('click', function(e) {
            e.preventDefault();
            _self.$containerList.toggleClass('active');
            _self.$containerMobile.toggleClass('active');
            $(this).toggleClass('active');
        });

        this.$btnsOpenDetail.on('click', function() {

            _self.openDetail();

            return false;
        });

        this.$btnsCloseDetail.on('click', function() {

            _self.closeDetail();

        });
    },

    openDetail : function () {
        this.$dynamicContainer.html(this.$detailFakeAjax);
        this.$containerDetailMobile.show();
        this.$containerHistory.hide();
	    this.accountSidebar.hide();
    },

    closeDetail : function () {
        this.$dynamicContainer.empty();
        this.$containerDetailMobile.hide();
        this.$containerHistory.show();
	    this.accountSidebar.show();
    }

};