
/**
 * my-contact-preferences.js
 */

$(document).ready(function() {

	$('#form-contact-preferences').on('submit', function(e) {

		$validForm = $(this).validateForm();

		if ( ! $validForm ) {
			e.preventDefault();
			return false;
		}

		return true;
	}); // $('#form-contact-preferences')()

});

(function($) {

	/**
	 * validate the edit addresses form
	 * 
	 * @return {boolean} - true if valid, else false
	 */
	$.fn.validateForm = function() {

		var self = $(this);

		$validation = new Validation();

		$subscribed_in_nesspresso_club = $(this).find('[name="subscribed_in_nesspresso_club"]:checked');
		$contact_preferences = $(this).find('[name="contact_preferences[]"]:checked');
		$subscribed_in_nesspresso_news = $(this).find('[name="subscribed_in_nesspresso_news"]:checked');

		if ( !$validation.validate_subscribed_in_nesspresso_club($subscribed_in_nesspresso_club) )
			return false;

		if ( !$validation.validate_contact_preferences($contact_preferences) )
			return false;

		if ( !$validation.validate_subscribed_in_nesspresso_news($subscribed_in_nesspresso_news) )
			return false;

		return true;

	}; // validateForm()

})(jQuery);