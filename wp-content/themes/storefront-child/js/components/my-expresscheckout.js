
/**
 * Express Checkout
 */
$(document).ready(function() {
	
    $('.my-expresscheckout-step a.toggler').click(function(e)
    {
				
		//état actif du bouton
		var currentToggler = $(this).find("i");
        if(currentToggler.is('.fa-angle-down')){
			$('.my-expresscheckout-step a.toggler').find("i").removeClass('fa-angle-up').addClass('fa-angle-down'); //init tous les boutons
			currentToggler.removeClass('fa-angle-down').addClass('fa-angle-up'); //laisse actif le bouton courant
		}else{
			currentToggler.removeClass('fa-angle-up').addClass('fa-angle-down'); //desactive le bouton courant
		}
		
		//fermeture des panneaux et ouverture du panneau correspondant
		var currentToggle = $(this).parents(".my-expresscheckout-step").find(".step-toggle");
		if(currentToggle.is('.active')){
			currentToggle.removeClass('active');
		}else{
			$('.my-expresscheckout-step .step-toggle').removeClass('active');
			currentToggle.addClass('active');
		}
					   
		e.preventDefault();
        return false;
    });


	$('#form-express-checkout').on('submit', function(e) {
		
		$validForm = $(this).validateForm();

		if ( ! $validForm ) {
			e.preventDefault();
			return false;
		}

		return true;
	}); // $('#form-express-checkout')()

});

(function($) {

	/**
	 * validate the edit addresses form
	 * 
	 * @return {boolean} - true if valid, else false
	 */
	$.fn.validateForm = function() {

		var self = $(this);

		$validation = new Validation();

		$shipping_mode = $(this).find('[name="shipping_mode"]:checked');
		$shipping_mode_standard_options = $(this).find('[name="shipping_mode_standard_options"]:checked');
		$payment_mode = $(this).find('[name="payment_mode"]:checked');

		if ( !$validation.validate_shipping_mode($shipping_mode) )
			return false;

		if ( $shipping_mode.val() == 'standard' && !$validation.validate_shipping_mode_standard_options($shipping_mode_standard_options) )
			return false;

		if ( !$validation.validate_payment_mode($payment_mode) )
			return false;

		return true;

	}; // validateForm()

})(jQuery);
