/**
 * discover-nespress.js
 */

$(document).ready(function() {
	var total_clicks = $('.v_slide_item').length;
	if($(window).width() > '740') {
		$("html").addClass("desktop");
		$('.bg-small').css('display', 'none');
		$('.bg-xl').css('display', 'block');
	} else {
		$("html").addClass("mobile");
		$('.bg-small').css('display', 'block');
		$('.bg-xl').css('display', 'none');
	}

	$('a[href^="#"]').on('click', function(event) {

	    var target = $(this.getAttribute('href'));
	    target.slideDown();
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top - 55
	        }, 500);
	    }

	});

	var init_translateX = 0;
	var capsule_counter = 9;
	var capsule_caret_position = '55px';
	var position_1 = Math.round($('.capsule-sequence-1').offset().left);
	var position_2 = Math.round($('.capsule-sequence-2').offset().left);
	var position_3 = Math.round($('.capsule-sequence-3').offset().left);
	var position_4 = Math.round($('.capsule-sequence-4').offset().left);
	var position_5 = Math.round($('.capsule-sequence-5').offset().left);
	var position_6 = Math.round($('.capsule-sequence-6').offset().left);
	var position_7 = Math.round($('.capsule-sequence-7').offset().left);
	var position_8 = Math.round($('.capsule-sequence-8').offset().left);
	var position_9 = Math.round($('.capsule-sequence-9').offset().left);

    $('.bg_full').hide();
    setTimeout( function(){ $('.bg_full').show(); }, 2000 )
	$('a[href^="#"]').on('click', function(event) {

	    var target = $(this.getAttribute('href'));
	    target.slideDown();
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top - 55
	        }, 500);
	    }

	});

	//next
	$('.v_sliderNext').on('click', function () {
		init_translateX = init_translateX + (-110.667);
		$('.v_slide').css( 'transform','translateX('+init_translateX+'px)');
		$('.v_sliderPrev').prop("disabled", false);
		$('.v_sliderPrev').removeClass("hide");
		capsule_counter++;
		if (capsule_counter === total_clicks) {
			$('.v_sliderNext').prop("disabled", true);
			$('.v_sliderNext').addClass("hide");
		}

		//hide active capsule
		$('.capsule-maker').animate(
	        {
	        	width: 0,
	        },
	        {
	            duration: 300,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.map_text').css('fill-opacity', '0');
	            }
	        }
        );
		$('.v_caret').hide();

		//remove line
		$('.map-path').hide();

		//remove actie map
		Countries.reset_map();

	});

	//previous
	$('.v_sliderPrev').on('click', function () {
		capsule_counter--;
		init_translateX = init_translateX - (-110.667);
		$('.v_slide').css( 'transform','translateX('+init_translateX+'px)');
		$('.v_sliderNext').prop("disabled", false);
		$('.v_sliderNext').removeClass("hide");
		if (capsule_counter === 9) {
			$('.v_sliderPrev').prop("disabled", true);
			$('.v_sliderPrev').addClass("hide");
		}

		//hide active capsule
		$('.capsule-maker').animate(
	        {
	        	width: 0,
	        },
	        {
	            duration: 300,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.map_text').css('fill-opacity', '0');
	            }
	        }
        );

		$('.v_caret').hide();

		//remove line
		$('.map-path').hide();

		//remove active map
		Countries.reset_map();


	});

	//click capsule
	$('.v_slide_item').on('click', function () {

		$('.v_slide_item').removeClass('v_active');

		var active_capsule = $(this).data('product');
		var capsule_offset = Math.round($(this).offset().left);
		var capsule_position = 0;
		$(this).addClass('v_active');

		if (position_1 === capsule_offset) {
			capsule_position = 1;
			capsule_caret_position = '55px';
		} else if (position_2 === capsule_offset) {
			capsule_position = 2;
			capsule_caret_position = '166px';
		} else if (position_3 === capsule_offset) {
			capsule_position = 3;
			capsule_caret_position = '276px';
		} else if (position_4 === capsule_offset) {
			capsule_position = 4;
			capsule_caret_position = '387px';
		} else if (position_5 === capsule_offset) {
			capsule_position = 5;
			capsule_caret_position = '498px';
		} else if (position_6 === capsule_offset) {
			capsule_position = 6;
			capsule_caret_position = '608px';
		} else if (position_7 === capsule_offset) {
			capsule_position = 7;
			capsule_caret_position = '719px';
		} else if (position_8 === capsule_offset) {
			capsule_position = 8;
			capsule_caret_position = '830px';
		} else if (position_9 === capsule_offset) {
			capsule_position = 9;
			capsule_caret_position = '940px';
		}

		$('.v_caret').css('left', capsule_caret_position);
		$('.v_caret').show();
		$('.capsule-maker').css('left', capsule_caret_position);

		$('.capsule-maker').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 500,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.map_text').css('fill-opacity', '0');
	            }
	        }
        );

		//remove active map
		Countries.reset_map();

		animate_map(active_capsule, capsule_position);

		//show description

		$('.capsule-section').addClass('hide');
		$('.' + active_capsule).removeClass('hide');

	});

	var modal = $('#modal-container');
	var modal_img = $('#modal_img');
	// Get the image and insert it inside the modal - use its "alt" text as a caption
	$('.image-modal').on('click', function (){
		modal_img.removeClass('out');
		modal.css('display','block');
		modal_img.prop('src',$(this).data('img'));
	});

	// When the user clicks on <span> (x), close the modal
	$('#modal-container').on('click', function (){

	    modal_img.addClass('out');
	    setTimeout(function() {
	       modal.css('display','none');
	       modal_img.addClass('modal-content');
	     }, 400);

	 });

	// Configure/customize these variables.
    var showChar = 250;  // How many characters are shown by default
    var ellipsestext = "...";

    $('.show-less').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var html = c;

            $(this).html(html+ellipsestext);
        }

    });
});

function animate_map (active_capsule, capsule_position) {
	//active map capsule
	switch (active_capsule) {
		case 'kazaar':
		case 'arpeggio':
		case 'livanto':
		case 'ciocattino':
		case 'vanilio':
		case 'caramelito':
			//brazil
			Countries.brazil(capsule_position);
			//guatemala
			Countries.guatemala(capsule_position);

		break;

		case 'dharkan':
			//costa rica
			Countries.costa_rica(capsule_position);
			//indonesia
			Countries.indonesia(capsule_position);
		break;

		case 'ristretto':
		case 'capriccio':
		case 'volluto':
		case 'linizio_lungo':
		case 'ristretto_decaffeinato':
		case 'volluto_decaffeinato':
			//colombia
			Countries.colombia(capsule_position);
			//brazil
			Countries.brazil(capsule_position);
		break;

		case 'roma':
			//brazil
			Countries.brazil(capsule_position);
			//mexico
			Countries.mexico(capsule_position);

		break;

		case 'cosi':
			//costa_rica
			Countries.costa_rica(capsule_position);
			//kenya
			Countries.kenya(capsule_position);
		break;

		case 'indriya_from_india':
			//india
			Countries.india(capsule_position);
		break;

		case 'rosabaya_de_colombia':
			//colombia
			Countries.colombia(capsule_position);
		break;

		case 'dulsÃo_do_brazil':
			//brazil
			Countries.brazil(capsule_position);
		break;

		case 'bukeela_ka_ethiopia':
			//ethiopia
			Countries.ethiopia(capsule_position);
		break;

		case 'envivo_lungo':
			//mexico
			Countries.mexico(capsule_position);
			//india
			Countries.india(capsule_position);
		break;

		case 'fortissio_lungo':
			//india
			Countries.india(capsule_position);
			//colombia
			Countries.colombia(capsule_position);
		break;

		case 'vivalto_lungo':
		case 'vivalto_lungo_decaffeinato':
			//colombia
			Countries.colombia(capsule_position);
			//ethiopia
			Countries.ethiopia(capsule_position);
		break;

		case 'arpeggio_decaffeinato':
			//costa_rica
			Countries.costa_rica(capsule_position);
			//brazil
			Countries.brazil(capsule_position);
		break;
	}
}

var Countries = {

    brazil:function(capsule_position){

        $('.mark_on_brazil').addClass('marker-active');
		$('.first_g_brazil').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_brazil').css('fill', 'rgb(0, 0, 0)');
		$('.rect_brazil').css('fill', 'rgb(0, 0, 0)');

		$('.rect_brazil').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_brazil').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_brazil').css('fill', 'rgb(255, 255, 255)');
		$('.path-brazil-' + capsule_position).show();
    },
    guatemala:function(capsule_position){

        $('.mark_on_guatemala').addClass('marker-active');
		$('.first_g_guatemala').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_guatemala').css('fill', 'rgb(0, 0, 0)');
		$('.rect_guatemala').css('fill', 'rgb(0, 0, 0)');
		$('.rect_guatemala').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_guatemala').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_guatemala').css('fill', 'rgb(255, 255, 255)');
		$('.path-guatemala-' + capsule_position).show();
    },
    costa_rica:function(capsule_position){

        $('.mark_on_costa_rica').addClass('marker-active');
		$('.first_g_costa_rica').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_costa_rica').css('fill', 'rgb(0, 0, 0)');
		$('.rect_costa_rica').css('fill', 'rgb(0, 0, 0)');
		$('.rect_costa_rica').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_costa_rica').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_costa_rica').css('fill', 'rgb(255, 255, 255)');
		$('.path-costa_rica-' + capsule_position).show();
    },
    indonesia:function(capsule_position){

        $('.mark_on_indonesia').addClass('marker-active');
		$('.first_g_indonesia').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_indonesia').css('fill', 'rgb(0, 0, 0)');
		$('.rect_indonesia').css('fill', 'rgb(0, 0, 0)');
		$('.rect_indonesia').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_indonesia').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_indonesia').css('fill', 'rgb(255, 255, 255)');
		$('.path-indonesia-' + capsule_position).show();
    },
    colombia:function(capsule_position){

        $('.mark_on_colombia').addClass('marker-active');
		$('.first_g_colombia').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_colombia').css('fill', 'rgb(0, 0, 0)');
		$('.rect_colombia').css('fill', 'rgb(0, 0, 0)');
		$('.rect_colombia').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_colombia').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_colombia').css('fill', 'rgb(255, 255, 255)');
		$('.path-colombia-' + capsule_position).show();
    },
    mexico:function(capsule_position){

        $('.mark_on_mexico').addClass('marker-active');
		$('.first_g_mexico').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_mexico').css('fill', 'rgb(0, 0, 0)');
		$('.rect_mexico').css('fill', 'rgb(0, 0, 0)');
		$('.rect_mexico').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_mexico').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_mexico').css('fill', 'rgb(255, 255, 255)');
		$('.path-mexico-' + capsule_position).show();
    },
    kenya:function(capsule_position){

        $('.mark_on_kenya').addClass('marker-active');
		$('.first_g_kenya').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_kenya').css('fill', 'rgb(0, 0, 0)');
		$('.rect_kenya').css('fill', 'rgb(0, 0, 0)');
		$('.rect_kenya').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_kenya').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_kenya').css('fill', 'rgb(255, 255, 255)');
		$('.path-kenya-' + capsule_position).show();
    },
    india:function(capsule_position){

        $('.mark_on_india').addClass('marker-active');
		$('.first_g_india').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_india').css('fill', 'rgb(0, 0, 0)');
		$('.rect_india').css('fill', 'rgb(0, 0, 0)');
		$('.rect_india').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_india').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_india').css('fill', 'rgb(255, 255, 255)');
		$('.path-india-' + capsule_position).show();
    },
    ethiopia:function(capsule_position){

        $('.mark_on_ethiopia').addClass('marker-active');
		$('.first_g_ethiopia').attr('transform', 'matrix(1,0,0,1,0,0)');
		$('.second_g_ethiopia').css('fill', 'rgb(0, 0, 0)');
		$('.rect_ethiopia').css('fill', 'rgb(0, 0, 0)');
		$('.rect_ethiopia').animate(
	        {
	        	width: 200,
	        },
	        {
	            duration: 1000,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.text_ethiopia').css('fill-opacity', '1');
	            }
	        }
        );
		$('.N_ethiopia').css('fill', 'rgb(255, 255, 255)');
		$('.path-ethiopia-' + capsule_position).show();
    },

    reset_map:function(){
        $('.map_mark_on').removeClass('marker-active');
		$('.map_first_g').attr('transform', 'matrix(0.75,0,0,0.75,7.5,16.913015365600586)');
		$('.map_second_g').css('fill', 'rgb(255, 255, 255)');
		$('.map_rect').css('fill', 'rgb(255, 255, 255)');
		$('.map_N').css('fill', 'rgb(0, 0, 0)');
		$('.map_text').css('fill-opacity', '0');
		$('.map-path').hide();
		// $('.v_caret').hide();
		// $('.v_slide_item').removeClass('v_active');

		$('.map_rect').animate(
	        {
	        	width: 60,
	        },
	        {
	            duration: 500,
	            step: function(now) {
	            	$(this).attr("width", now);
	            },
	            complete: function () {
	            	$('.map_text').css('fill-opacity', '0');
	            }
	        }
        );
    },



}; // Countries
