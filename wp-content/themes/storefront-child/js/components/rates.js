$(function () {
    var $panel = $('#r2').parent();
    if ($(window).width() > 996) {
        $panel.hover(function () {
            if ($panel.hasClass('hover-panel')) {
                $(this).addClass('open-hover-panel');
                $(this).find('.rating__panel').stop(true, true).slideDown();
            }
        }, function () {
            $(this).removeClass('open-hover-panel');
            $(this).find('.rating__panel').stop(true, true).slideUp();
        });
    } else {
        //for mobile accept click
        $panel.click(function () {
            if ($panel.hasClass('hover-panel')) {
                if ($(this).find('.rating__panel').is(':visible')) {
                    $(this).removeClass('open-hover-panel');
                    $(this).find('.rating__panel').stop(true, true).slideUp();
                } else {
                    $(this).addClass('open-hover-panel');
                    $(this).find('.rating__panel').stop(true, true).slideDown();
                }
            }
        });
    }
});
