
/**
 * reset-password.js
 */

$(document).ready(function() {

	$('#form-reset-password').on('submit', function(e) {

		// check if form reset password is valid
		$validForm = $(this).validFormResetPassword();

		if ( !$validForm ) {
			e.preventDefault();
			return false;
		}

		return true;

	}); // $('#form-reset-password')()


});

(function($) {

	/**
	 * check if form reset password is valid
	 * 
	 * @return {boolean}
	 */
	$.fn.validFormResetPassword = function() {

		$password = $(this).find('[name="password"]');
		$password_confirmation = $(this).find('[name="password_confirmation"]');

		$validation = new Validation();
		
		if ( !$validation.validate_password($password) )
			return false;

		if ( !$validation.validate_password_confirmation($password, $password_confirmation) )
			return false;

		return true;

	}; // validFormResetPassword()


})(jQuery);
