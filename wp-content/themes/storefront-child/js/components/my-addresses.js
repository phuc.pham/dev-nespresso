
/**
 * my-addresses.js
 */

$(document).ready(function() {
	MyAddresses.init();
}); // $(document)

var MyAddresses = {

	that: null,

	init : function() {

		this.address_list = $('#container-my-address-list');
		this.btn_edit = $('.btn-edit-my-address');
		this.btn_cancel_edit = $('#btn-cancel-edit-my-address');
		this.validation = new Validation();

		this.form_container = $('#form-addresses');
		this.btn_submit = this.form_container.find('.btn-submit');

		this.form_billing = this.form_container.find('#form-edit-addresses-billing');
		this.billing_first_name = this.form_billing.find('[name="billing_first_name"]');
		this.billing_last_name = this.form_billing.find('[name="billing_last_name"]');
		this.billing_company = this.form_billing.find('[name="billing_company"]');
		this.billing_address_1 = this.form_billing.find('[name="billing_address_1"]');
		this.billing_address_2 = this.form_billing.find('[name="billing_address_2"]');
		this.billing_city = this.form_billing.find('[name="billing_city"]');
		this.billing_postcode = this.form_billing.find('[name="billing_postcode"]');
		this.billing_country = this.form_billing.find('[name="billing_country"]');
		this.billing_state = this.form_billing.find('[name="billing_state"]');
		this.billing_phone = this.form_billing.find('[name="billing_phone"]');
		this.billing_mobile = this.form_billing.find('[name="billing_mobile"]');
		// this.billing_email_address = this.form_billing.find('[name="billing_home_office"]');
		// this.billing_ward = this.form_billing.find('[name="billing_ward"]');

		this.form_shipping = this.form_container.find('#form-edit-addresses-shipping');
		this.shipping_first_name = this.form_shipping.find('[name="shipping_first_name"]');
		this.shipping_last_name = this.form_shipping.find('[name="shipping_last_name"]');
		this.shipping_company = this.form_shipping.find('[name="shipping_company"]');
		this.shipping_address_1 = this.form_shipping.find('[name="shipping_address_1"]');
		this.shipping_address_2 = this.form_shipping.find('[name="shipping_address_2"]');
		this.shipping_city = this.form_shipping.find('[name="shipping_city"]');
		this.shipping_postcode = this.form_shipping.find('[name="shipping_postcode"]');
		this.shipping_country = this.form_shipping.find('[name="shipping_country"]');
		this.shipping_state = this.form_shipping.find('[name="shipping_state"]');
		this.shipping_mobile = this.form_shipping.find('[name="shipping_mobile"]');
		// this.shipping_ward = this.form_billing.find('[name="shipping_ward"]');
		// this.shipping_district = this.form_billing.find('[name="shipping_district"]');

		that = this;

		this.ready();

	}, // init()

	ready : function() {

		$('[name="billing_state"]').parent().removeClass('dropdown--init');
		$('[name="billing_city"]').parent().removeClass('dropdown--init');

		$('[name="shipping_state"]').parent().removeClass('dropdown--init');
		$('[name="shipping_city"]').parent().removeClass('dropdown--init');

		this.btn_edit.on('click', function() {
			that.showEditForm( $(this) );
		});

		this.btn_cancel_edit.on('click', function() {
			that.hideEditForm();
		});

		this.btn_submit.on('click', function(e) {

			e.preventDefault();

			var address_type = $(this).attr('data-address-type');

			$validForm = false;
			if ( address_type == 'billing' )
				$validForm = that.validateBillingForm();
			if ( address_type == 'shipping' )
				$validForm = that.validateShippingForm();

			if ( $validForm ) {
				if ( address_type == 'billing' )
					that.form_billing.submit();
				if ( address_type == 'shipping' )
					that.form_shipping.submit();
			}
		}); // this.btn_submit

	}, // ready()

	/**
	 * validate the edit addresses shipping form
	 *
	 * @return {boolean} - true if valid, else false
	 */
	validateShippingForm : function() {
		// if ( this.shipping_first_name.val().trim() )
			if ( !this.validation.validate_first_name( this.shipping_first_name ) )
				return false;

		// if ( this.shipping_last_name.val().trim() )
			if ( !this.validation.validate_last_name( this.shipping_last_name ) )
				return false;

		// if ( this.shipping_company.val().trim() )
			// if ( !this.validation.validate_billing_company( this.shipping_company ) )
			// 	return false;

		if ( !this.validation.validate_billing_address_1( this.shipping_address_1 ) )
			return false;

		// if ( this.shipping_address_2.val().trim() )
		// 	if ( !this.validation.validate_billing_address_2( this.shipping_address_2 ) )
		// 		return false;


		if ( !this.validation.validate_billing_city( this.shipping_city ) )
			return false;

		// if ( !this.validation.validate_billing_postcode( this.shipping_postcode ) )
		// 	return false;

		if ( !this.validation.validate_billing_state( this.shipping_state ) )
			return false;

		if ( !this.validation.validate_mobile( this.shipping_mobile ) )
			return false;

		return true;
	}, // validateBillingForm()

	/**
	 * validate the edit addresses billing form
	 *
	 * @return {boolean} - true if valid, else false
	 */
	validateBillingForm : function() {

		// if ( this.billing_first_name.val().trim() )
			if ( !this.validation.validate_first_name( this.billing_first_name ) )
				return false;

		// if ( this.billing_last_name.val().trim() )
			if ( !this.validation.validate_last_name( this.billing_last_name ) )
				return false;

		// if ( this.billing_company.val().trim() )
			// if ( !this.validation.validate_billing_company( this.billing_company ) )
			// 	return false;

		if ( !this.validation.validate_billing_address_1( this.billing_address_1 ) )
			return false;

		// if ( this.billing_address_2.val().trim() )
		// 	if ( !this.validation.validate_billing_address_2( this.billing_address_2 ) && this.billing_address_2.val() )
		// 		return false;

		if ( !this.validation.validate_billing_city( this.billing_city ) )
			return false;

		// if ( !this.validation.validate_billing_postcode( this.billing_postcode ) )
		// 	return false;

		if ( !this.validation.validate_billing_state( this.billing_state ) )
			return false;

		if ( !this.validation.validate_mobile( this.billing_mobile ) )
			return false;

		return true;

	}, // validateBillingForm()

	/**
	 * show the edit form
	 * hide the my address list container
	 *
	 * @param {object} [obj]
	 */
	showEditForm : function(obj) {

		var data_address_type = obj.attr('data-address-type');

		this.btn_submit.attr('data-address-type', data_address_type);

		this.form_container.removeClass('hide');

		if ( data_address_type == 'billing')
			this.form_billing.removeClass('hide');

		if ( data_address_type == 'shipping')
			this.form_shipping.removeClass('hide');

		this.address_list.addClass('hide');

	}, // showEditForm()

	/**
	 * hide the edit form
	 * show the my address list container
	 */
	hideEditForm : function() {

		this.form_container.addClass('hide');
		this.form_shipping.addClass('hide');
		this.form_billing.addClass('hide');
		this.address_list.removeClass('hide');

	}, // hideEditForm()

} // MyAddresses
