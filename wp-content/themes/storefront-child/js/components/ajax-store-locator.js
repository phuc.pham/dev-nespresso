
/**
 * Will need for handling store categories
 */
// jQuery("#recycling-points-btn").click(function(){
// $("#nav-btn-handler li").addClass('slocator-stores__element--desactivated');
//     $("#recycling-points-btn").removeClass("slocator-stores__element--desactivated");
// });
// jQuery("#nespresso-boutiques-btn").click(function(){
// $("#nav-btn-handler li").addClass('slocator-stores__element--desactivated');
//     $("#nespresso-boutiques-btn").removeClass("slocator-stores__element--desactivated");
// });
// jQuery("#pickup-points-btn").click(function(){
// $("#nav-btn-handler li").addClass('slocator-stores__element--desactivated');
//     $("#pickup-points-btn").removeClass("slocator-stores__element--desactivated");
// });
// jQuery("#retailers-btn").click(function(){
// $("#nav-btn-handler li").addClass('slocator-stores__element--desactivated');
//     $("#retailers-btn").removeClass("slocator-stores__element--desactivated");
// });

$(".store-reseller-rustan-shangrila").on('click', function (e) {
    $("#tabs-content").find('.tab').css('display','none');
    $("#tab-2").css('display','block');
});

$(".tab-1").on('click', function (e) {
    console.log('here');
    $("#tabs-content").find('.tab').css('display','none');
    setTimeout(function(){
            $("#tab-1").css('display','block');
    }, 150);
});

$.ajax({
    method : "GET",
    url : ajaxurl,
    dataType: 'json',
    data : {
        action : 'fetch_stores',
    },
    success : function(res) {
        console.log(res);

        postsHtml = '';
        $.each(res, function(key, value) {
            postsHtml += '<li class="slocator-locations__element">'+
                            '<span class="slocator-locations__icon icon icon-Boutique_on"></span>'+
                            '<div class="slocator-locations_text-content">'+
                                '<span class="slocator-locations__title">'+ value.store_title +'</span>'+
                                '<span class="slocator-locations__line-text">Rue du Cornavin 6</span>'+
                                '<span class="slocator-locations__line-text">Genève</span>'+
                                '<span class="slocator-locations__icon-recycled icon icon-Recycling_on"></span>'+
                            '</div>'+
                        '</li>';
        });

        $('#slocator-locations').html(postsHtml);

    },
    error : function(res) {
        console.log(res);
    },
    timeout : 3000
});
