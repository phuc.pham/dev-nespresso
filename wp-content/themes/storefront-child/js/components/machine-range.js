$(function () {
	$('.machine-list-item').mouseover(function(){ 
		var machine_id = $(this).data('id');
		$('.machine-list-item-description-' + machine_id).css('display', 'none');
		$('.rollover-' + machine_id).css('display', 'block');
	});
	$('.machine-list-item').mouseout(function(){ 
		var machine_id = $(this).data('id');
		$('.machine-list-item-description-' + machine_id).css('display', 'block');
		$('.rollover-' + machine_id).css('display', 'none');
	});
});