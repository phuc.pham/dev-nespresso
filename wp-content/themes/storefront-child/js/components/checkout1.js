
/**
 * checkout.js
 */

$(document).ready(function() {
	Checkout.init();
});

var Checkout = {

	that: null,

	init : function() {
		this.validation = new Validation;

		this.form_actions = $('#form_actions');
		this.checkout_next_step = this.form_actions.find('[name="checkout_next_step"]');
		this.checkout_prev_step = this.form_actions.find('[name="checkout_prev_step"]');
		this.customer_billing_details = $('#customer_billing_details');

		// under customer_billing_details div
		this.title = this.customer_billing_details.find('[name="title"]');
		this.billing_first_name = this.customer_billing_details.find('[name="billing_first_name"]');
		this.billing_last_name = this.customer_billing_details.find('[name="billing_last_name"]');
		this.billing_phone = this.customer_billing_details.find('[name="billing_phone"]');
		this.billing_address_1 = this.customer_billing_details.find('[name="billing_address_1"]');
		this.billing_address_2 = this.customer_billing_details.find('[name="billing_address_2"]');
		this.billing_city = this.customer_billing_details.find('[name="billing_city"]');
		this.billing_state = this.customer_billing_details.find('[name="billing_state"]');
		// this.billing_postcode = this.customer_billing_details.find('[name="billing_postcode"]');
		this.billing_country= this.customer_billing_details.find('[name="billing_country"]');
		this.billing_email = this.customer_billing_details.find('[name="billing_email"]');
		this.billing_mobile = this.customer_billing_details.find('[name="billing_mobile"]');

		this.customer_shipping_details = $('#customer_shipping_details');
		this.shipping_first_name = this.customer_shipping_details.find('[name="shipping_first_name"]');
		this.shipping_last_name = this.customer_shipping_details.find('[name="shipping_last_name"]');
		this.shipping_address_1 = this.customer_shipping_details.find('[name="shipping_address_1"]');
		this.shipping_address_2 = this.customer_shipping_details.find('[name="shipping_address_2"]');
		this.shipping_city = this.customer_shipping_details.find('[name="shipping_city"]');
		this.shipping_state = this.customer_shipping_details.find('[name="shipping_state"]');
		// this.shipping_postcode = this.customer_shipping_details.find('[name="shipping_postcode"]');
		this.shipping_country= this.customer_shipping_details.find('[name="shipping_country"]');
		this.shipping_mobile= this.customer_shipping_details.find('[name="shipping_mobile"]');

		that = this;

		this.ready();
	}, // init()

	ready : function() {
		var current_city = $('#shipping_city').val();
		setTimeout(function(){
			$('#shipping_city').val(current_city)
		}, 5000);
		//hide optional
		$('.optional').hide();
		// $('.woocommerce-shipping-totals').css('display', 'none');

		var selected_delivery = $('input[name=delivery_option]:checked').val();
		that.deliveryOption(selected_delivery, 'initial');
		$("input[name='delivery_option']").on('click', function () {
			that.deliveryOption($(this).val(), 'onchange');
		});

		$('#shipping_state').on('change', function (e) {
			$("[name='checkout_next_step']").attr("disabled", true);
			$("[name='shipping_city']").attr("disabled", true);
			that.getProvinceDistrict($(this).val(), 'shipping_city');
			setTimeout(function(){
				$("[name='shipping_city']").attr("disabled", false);
				$("[name='checkout_next_step']").attr("disabled", false);
			}, 500);
		});

		$('#billing_state').on('change', function (e) {
			$("[name='checkout_next_step']").attr("disabled", true);
			$("[name='billing_city']").attr("disabled", true);
			that.getProvinceDistrict($(this).val(), 'billing_city');
			setTimeout(function(){
				$("[name='billing_city']").attr("disabled", false);
				$("[name='checkout_next_step']").attr("disabled", false);
			}, 500);
		});

		if((typeof window.sleeve_checker != 'undefined' && !window.sleeve_checker) || (typeof window.capsule_checker != 'undefined' && !window.capsule_checker)) {
			swal({
				html:true,
				title: "Error",
				text: "To validate your basket, your order should be composed of a minimum 50 capsules and should be a multiple of 50 capsules (i.e. 50, 100, 150, etc.) . You can mix the variety of capsules as you choose, as long as the total number is a multiple of 50 (e.g. 20 Roma, 10 Arpeggio, 10 Livanto, 10 Dharkan). Please add <b>"+window.missing_capsule+"</b> capsules in your basket below or return to the shop to add other varieties.",
				type: "warning"
			},
			function(isConfirm){
				if (isConfirm) {
					var pathArray = window.location.pathname.split('/');
					if ( $.inArray( 'vi', pathArray ) == 1) {
						window.location = "vi/coffee-list";
					} else {
						window.location = "/coffee-list";
					}

				}
			});
		};

		//gtm checkout
		var checkout_step = 1;
		var checkout_step_two = false;
		var checkout_step_three = false;
		var checkout_step_four = false;
		this.checkout_prev_step.on('click', function(e) {
			checkout_step--;
			if(checkout_step == 1) {
				checkout_step_1();
			} else if(checkout_step == 2) {
				checkout_step_2();
			} else if(checkout_step == 3) {
				checkout_step_3();
			}
			 else if(checkout_step == 4) {
				checkout_step_4();
			}
		});

		this.checkout_next_step.on('click', function(e) {
			//gtm checkout
			checkout_step++;

			if(checkout_step == 2 && !checkout_step_two) {
				// checkout_step_two = true;
				checkout_step_2();
			} else if(checkout_step == 3 && !checkout_step_three) {
				// checkout_step_three = true;
				checkout_step_3();
			} else if(checkout_step == 4 && !checkout_step_four) {
				// checkout_step_four = true;
				checkout_step_4();
			}

			if ( that.customer_billing_details.is(':visible') ) {

				that.customer_shipping_details.removeClass('hide');

				if ( !that.validate_customer_billing_details() ) {
					that.checkout_prev_step.trigger('click');
					that.customer_shipping_details.addClass('hide');
				} else {
					that.customer_shipping_details.removeClass('hide');
				}
			} // endif

			if ( that.customer_shipping_details.is(':visible') ) {
				 setTimeout(function(){
				 	var selected_delivery = $('input[name=delivery_option]:checked').val();
					that.deliveryOption(selected_delivery, 'next');
				 }, 1000);
				if ( !that.validate_customer_shipping_details() ) {
					that.checkout_prev_step.trigger('click');
					$('#order_info').addClass('hide');
				} else {
					$('#order_info').removeClass('hide');
				}
			} // endif

		}); // this.checkout_next_step

		this.populateShippingForm();

	}, // ready()

	validate_customer_billing_details : function() {


		if ( !this.validation.validate_title(this.title) )
			return false;

		if ( !this.validation.validate_email(this.billing_email) )
			return false;

		if ( !this.validation.validate_first_name(this.billing_first_name) )
			return false;

		if ( !this.validation.validate_last_name(this.billing_last_name) )
			return false;

		if ( !this.validation.validate_billing_address_1(this.billing_address_1) )
			return false;

		if ( !this.validation.validate_billing_city(this.billing_city) )
			return false;

		if ( !this.validation.validate_billing_state(this.billing_state) )
			return false;

		if ( !this.validation.validate_mobile(this.billing_mobile) )
			return false;

		// if ( !this.validation.validate_postcode(this.billing_postcode) )
		// 	return false;

		return true;

	}, // validate_customer_billing_details()

	validate_customer_shipping_details : function() {

		if ( !this.validation.validate_first_name(this.shipping_first_name) )
			return false;

		if ( !this.validation.validate_last_name(this.shipping_last_name) )
			return false;

		if ( !this.validation.validate_billing_address_1(this.shipping_address_1) )
			return false;

		if ( !this.validation.validate_billing_city(this.shipping_city) )
			return false;

		if ( !this.validation.validate_billing_state(this.shipping_state) )
			return false;

		if ( !this.validation.validate_mobile(this.shipping_mobile) )
			return false;

		return true;

	}, // validate_customer_shipping_details()

	populateShippingForm : function() {

		this.shipping_first_name.val( window.user.shipping_first_name );
		if ( !this.shipping_first_name.val().trim() )
			this.shipping_first_name.val( this.billing_first_name.val() );

		this.shipping_last_name.val( window.user.shipping_last_name );
		if ( !this.shipping_last_name.val().trim() )
			this.shipping_last_name.val( this.billing_last_name.val() );

		this.shipping_address_1.val( window.user.shipping_address_1 );
		if ( !this.shipping_address_1.val().trim() )
			this.shipping_address_1.val( this.billing_address_1.val() );

		this.shipping_address_2.val( window.user.shipping_address_2 );
		if ( !this.shipping_address_2.val().trim() )
			this.shipping_address_2.val( this.billing_address_2.val() );

		this.shipping_city.val( window.user.shipping_city );
		if ( !this.shipping_city.val().trim() )
			this.shipping_city.val( this.billing_city.val() );

		// this.shipping_postcode.val( window.user.shipping_postcode );
		// if ( !this.shipping_postcode.val().trim() )
		// 	this.shipping_postcode.val( this.billing_postcode.val() );

		this.shipping_state.val( window.user.shipping_state );
		if ( !this.shipping_state.val().trim() )
			this.shipping_state.val( this.billing_state.val() );

		this.shipping_country.val( window.user.shipping_country );
		if ( !this.shipping_country.val().trim() )
			this.shipping_country.val( this.billing_country.val() );

	}, // populateShippingForm()

	getProvinceDistrict : function(province_code, field_section) {
		var data = {
            action: 'get_province_district',
            province_code: province_code
        };
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: ajaxurl,
            data: data,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Something went wrong. Please ask the administrator for more information.');
            },
            success: function (data) {
                if ( data.status != 'failed') {
                	var district_select = $('#'+field_section);
                	$('option', district_select).remove();
                	district_select.append(new Option('Please select district', '', true, true));
					$.each(data.districts, function(index, value) {
						district_select.append(new Option(value, value, true, true));
					});
					$('.current_district').html('Please select district')
					district_select.val('');
                } else {
                	console.log(data.message);
                }
            } // success()

        }); // ajax()

	}, // getProvinceDistrict()

	deliveryOption : function(delivery_option, action) {
		$("input[name='checkout_next_step']").attr('disabled','disabled');
		document.body.style.cursor='wait';
		var basket_price = $('.basket-price').html();
		if (delivery_option == 'pick_up') {

			if (basket_price  > 1500000) {
				$('.payment_method_cod').hide();
			}

			$('.local_pickup').click();
			$(".local_pickup_label").show();

			setTimeout(function(){
				if ($(".flat_rate_label").html() != 'Free shipping')
					$(".flat_rate_label").hide();
			}, 4000);

			// pickup
			$('#pick_up_location_field').show();
			// customer
			$('#shipping_address_1_field').hide();
			$('#shipping_address_2_field').hide();
			$('#shipping_state_field').hide();
			$('#shipping_city_field').hide();

			//reset customer address
			if (action == 'onchange' && !window.customer_province == !$('#shipping_state').val() && !window.customer_city == $('#shipping_city').val()) {
				$('#shipping_address_1').val(window.customer_address_1);
				$('#shipping_state').val(window.customer_province).trigger('change');
				setTimeout(function(){
				 	$('#shipping_city').val(window.customer_city).trigger('change');
				 	$("input[name='checkout_next_step']").removeAttr('disabled');
				 	document.body.style.cursor='default';
				 }, 2000);
			} else {
				$("input[name='checkout_next_step']").removeAttr('disabled');
				document.body.style.cursor='default';
			}

		} else {
			$('.payment_method_cod').show();
			$('.flat_rate').click();
			$(".local_pickup_label").hide();
			setTimeout(function(){
				$(".flat_rate_label").show();
			}, 3500);
			$(".flat_rate_label").show();
			// pickup
			$('#pick_up_location_field').hide();
			// customer
			$('#shipping_address_1_field').show();
			$('#shipping_address_2_field').show();
			$('#shipping_state_field').show();
			$('#shipping_city_field').show();
			$("input[name='checkout_next_step']").removeAttr('disabled');
			document.body.style.cursor='default';
		}

	}, // deliveryOption()

} // Checkout
