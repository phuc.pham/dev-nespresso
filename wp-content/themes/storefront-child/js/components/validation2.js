
/**
 * validation.js
 *
 * list of custom and extendable js validations to all front-end forms
 * with message, require sweetalert and jqeury
 */

function Validation() {

	/**
	 * the object element holder being validated
	 *
	 * @type {HTMLElement}
	 */
	this.obj = null;

	/**
	 * overule valid validation container
	 *
	 * @type {Boolean}
	 */
	this.valid = true;

	/**
	 * initiate class
	 */
	this.init = function() {


	}; // init()

	/**
	 * valid HTMLElement object
	 *
	 * @param  {HtmlElement} obj
	 * @return {boolean}
	 */
	this.valid_object = function(obj) {

		try {
			return obj instanceof jQuery;
		} catch (e) {
			try {
				return this.obj instanceof jQuery;
			} catch (e) {
				return false;
			}
		}
	}; // valid_object()

	/**
	 * overule that validation
	 *
	 * @param {Boolean} valid
	 */
	this.set_valid = function(valid) {

		this.valid = valid;

		return this;
	} // set_valid

	/**
	 * set the object used by the validation
	 *
	 * @param {object} obj [description]
	 */
	this.set_object = function(obj) {

		if ( !this.valid_object(obj) )
			throw "Nespresso Validation Error: Ivalid HTMLElement";

		this.obj = obj;

		return this;
	}; // set_object()

	/**
	 * validate first name
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_first_name = function(obj) {

		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'First name is invalid', 'error');
			return false;
		}

		if ( !this.valid_object(obj) )
			throw 'Nespresso Validation Error: HTMLElement object not found.';

		if ( !obj.val() ) {
			obj.focus();
			sweetAlert('Error', 'First name is required', 'error');
			return false;
		}

		return true;
	}; // valid_first_name()

	/**
	 * validate middle name
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_middle_name = function(obj) {

		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Middle name is invalid', 'error');
			return false;
		}

		if ( !this.valid_object(obj) )
			throw "Nespresso Validation Error: HTMLElement object not found.";

		if ( !obj.val() ) {
			obj.focus();
			sweetAlert('Error', 'First name is required', 'error');
			return false;
		}

		return true
	}; // valid_middle_name()

	/**
	 * validate last name
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_last_name = function(obj) {

		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Last name is invalid', 'error');
			return false;
		}

		if ( !this.valid_object(obj) )
			throw "Nespresso Validation Error: HTMLElement object not found.";

		if ( !obj.val() ) {
			obj.focus();
			sweetAlert('Error', 'Last name is required', 'error');
			return false;
		}

		return true
	}; // validate_last_name()


	/**
	 * validate date_of_birth
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_date_of_birth = function(obj) {

		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Date of birth is invalid', 'error');
			return false;
		}

		if ( !this.valid_object(obj) )
			throw "Nespresso Validation Error: HTMLElement object not found.";

		if ( !obj.val() || obj.val() == '--') {
			obj.focus();
			sweetAlert('Error', 'Date of birth is required', 'error');
			return false;
		}

		// yyyy-mm-dd
		var date_of_birth_pattern = /^\d{4}\-\d{1,2}\-\d{1,2}$/;

		if ( !date_of_birth_pattern.test( obj.val() )  ) {
			obj.focus();
			sweetAlert('Error', 'Date of birth is invalid', 'error');
			return false;
		}


		var new_birthdate = new Date(obj.val());
		var str = ""+new_birthdate+"";

		if ($('[name="month"]').val() == 2 && str.includes("Mar")) {
			sweetAlert('Error', 'Invalid Date', 'error');
			return false
		}

		if ($('[name="month"]').val() == 6 && str.includes("Jul")) {
			sweetAlert('Error', 'Invalid Date', 'error');
			return false
		}

		if ($('[name="month"]').val() == 4 && str.includes("May")) {
			sweetAlert('Error', 'Invalid Date', 'error');
			return false
		}

		if ($('[name="month"]').val() == 9 && str.includes("Oct")) {
			sweetAlert('Error', 'Invalid Date', 'error');
			return false
		}

		if ($('[name="month"]').val() == 11 && str.includes("Dec")) {
			sweetAlert('Error', 'Invalid Date', 'error');
			return false
		}

		if (new_birthdate == 'Invalid Date') {
			sweetAlert('Error', 'Invalid Date', 'error');
			return false
		}
		var ageDifMs = Date.now() - new_birthdate.getTime();
	    var ageDate = new Date(ageDifMs); // miliseconds from epoch
	    var age = Math.abs(ageDate.getUTCFullYear() - 1970);
	    if (age > 14) {
	    	return true;
	    } else {
	    	sweetAlert('Error', 'You must be 15 or Older to create account', 'error');
	    	return false;
	    }

		return true
	}; // validate_date_of_birth()

	/**
	 * validate mobile
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_mobile = function(obj) {

		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Mobile is invalid', 'error');
			return false;
		}

		if ( !this.valid_object(obj) )
			throw "Nespresso Validation Error: HTMLElement object not found.";

		if ( !obj.val() ) {
			obj.focus();
			sweetAlert('Error', 'Mobile is required', 'error');
			return false;
		}

		var mobile = obj.val();
		var start_mobile = parseInt(mobile.charAt(0));
		if (!$.isNumeric(mobile) || mobile.length < 9 || start_mobile) {
			obj.focus();
	    	sweetAlert('Error', 'Invalid mobile number', 'error');
	    	return false;
	    }

		return true
	}; // validate_mobile()

	/**
	 * validate email
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_email = function(obj) {
		var current_url = window.location.pathname;
		if (!current_url.includes("personal-information") ) {

			var current_url = window.location.href;
			if (current_url.includes('/vi/')) {
				var alert_title = 'LỖI';
				var alert_text = 'Email không hợp lệ';
				// var alert_button = 'được';
			} else {
				var alert_title = 'Error';
				var alert_text = 'Email is Invalid';
				// var alert_button = 'OK';
			}

			if ( !this.valid ) {
				try {
					obj.focus();
				} catch (e) {
					// do nothing
				}
				sweetAlert(alert_title, alert_text, 'error');
				return false;
			}

			if ( !this.valid_object(obj) )
				throw "Nespresso Validation Error: HTMLElement object not found.";

			if ( !obj.val() ) {
				obj.focus();
				sweetAlert('Error', 'Email is required', 'error');
				return false;
			}

			//var emailPattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if ( !emailPattern.test(obj.val() )) {
				obj.focus();
				sweetAlert(alert_title, alert_text, 'error');
				// sweetAlert('Error', 'Email is invalid', 'error');
				return false;
			}

			return true;
		}
	}; // validate_email()

	this.validate_email_exists_2 = function(obj) {

		var current_url = window.location.pathname;
		if (current_url.includes("register") ) {
			$.ajax({
	            method : "POST",
	            url : ajaxurl,
	            dataType: 'json',
	            data : {
	                action : 'check_email_exist',
	                email : obj.val()
	            },
	    		error: function (jqXHR, textStatus, errorThrown) {
	                console.log(errorThrown);
	                // return false;
	                // console.log(errorThrown);
	                console.log('Something went wrong. Please ask the administrator for more information.');
	            },
		    	success : function(result) {
			        if (result) {
			        	obj.focus();
						sweetAlert('Error', 'Email is already registered', 'error');
						return false;
			        } else {
			        	return true;
			        }

	            }
	        });
		}


	}; // validate_email()

	/**
	 * validate email is uniquer
	 * can't really, just for additional function
	 *
	 * @return {boolean}
	 */
	this.validate_email_is_unique = function() {
		if ( !this.valid ) {
			try {
				email_obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Email is already registered', 'error');
			return false;
		}

		return true;
	};

	/**
	 * validate email confirmation
	 *
	 * @param  {HTMLElement} email_obj
	 * @param  {HTMLElement} email_confirmation_obj
	 * @return {boolean}
	 */
	this.validate_email_confirmation = function(email_obj, email_confirmation_obj) {

		if ( !this.valid ) {
			try {
				email_obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Emails must match', 'error');
			return false;
		}

		if ( !this.valid_object(email_obj) || !this.valid_object(email_confirmation_obj) )
			throw "Nespresso Validation Error: HTMLElement email or email confirmation object not found.";

		if ( email_obj.val() != email_confirmation_obj.val() ) {
			email_obj.focus();
			sweetAlert('Error', 'Emails must match', 'error');
			return false;
		}

		return true
	}; // validate_email_confirmation()

	/**
	 * validate email exists
	 * can't really, just for additional function
	 *
	 * @return {boolean}
	 */
	this.validate_email_exists = function() {
		if ( !this.valid ) {
			try {
				email_obj.focus();
			} catch (e) {
				// do nothing
			}
			var current_url = window.location.href;
			if (current_url.includes('/vi/')) {
				var alert_title = 'LỖI';
				var alert_text = 'Email chưa được đăng ký';
				// var alert_button = 'được';
			} else {
				var alert_title = 'Error';
				var alert_text = 'Email is not yet registered';
				// var alert_button = 'OK';
			}
			sweetAlert(alert_title, alert_text, 'error');
			return false;
		}

		return true;
	};

	this.validate_phone = function(obj) {

		if ( obj.val() ) {

			if (isNaN(obj.val())) {
				obj.focus();
				sweetAlert('Error', 'Phone is invalid', 'error');
				return false;
			}
		}
		return true;
	}; // validate_phone()

	/**
	 * validate password
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_password = function(obj) {

		var current_url = window.location.href;
		if (current_url.includes('vi/reset-password')) {
			var alert_title = 'LỖI';
			var alert_text = 'Mật khẩu không hợp lệ';
			// var alert_button = 'được';
		} else {
			var alert_title = 'Error';
			var alert_text = 'Password is invalid';
			// var alert_button = 'OK';
		}

		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}

			sweetAlert(alert_title, alert_text, 'error');
			return false;
		}

		if ( !this.valid_object(obj) )
			throw "Nespresso Validation Error: HTMLElement object not found.";

		if ( !obj.val() ) {
			obj.focus();
			sweetAlert('Error', 'Password is required', 'error');
			return false;
		}
		var $s = /^(?=(.*[a-z]){1,})(?=(.*[A-Z]){1,})(?=(.*[\d]){1,})(?=(.*[\!\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\`\{\|\}\~]){1,})(?!.*\s).{6,30}$/;
		var passwordPattern = $s;//new RegExp($s)//"^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!~@$%^&*-]).{4,}$");
		if ( !passwordPattern.test( obj.val() )  ||
			( typeof valid !== 'undefined' && valid == false )
		) {
			obj.focus();
			sweetAlert('Error', alert_text, 'error');
			return false;
		}

		return true
	}; // validate_email()

	/**
	 * validate password confirmation
	 *
	 * @param  {HTMLElement} password_obj
	 * @param  {HTMLElement} password_confirmation_obj
	 * @return {boolean}
	 */
	this.validate_password_confirmation = function(password_obj, password_confirmation_obj) {

		if ( !this.valid ) {
			try {
				password_obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Passwords must match', 'error');
			return false;
		}

		if ( !this.valid_object(password_obj) || !this.valid_object(password_confirmation_obj) )
			throw "Nespresso Validation Error: HTMLElement password or password confirmation object not found.";

		if ( password_obj.val() != password_confirmation_obj.val() ) {
			password_obj.focus();
			sweetAlert('Error', 'Passwords must match', 'error');
			return false;
		}

		return true
	}; // validate_email_confirmation()

	/**
	 * validate password current
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_password_current = function(obj) {

		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Current password is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			obj.focus();
			sweetAlert('Error', 'Current password is required', 'error');
			return false;
		}

		return true
	}; // validate_email()

	/**
	 * validate password same
	 * with the same password given
	 *
	 * @param  {HTMLElement} $obj
	 * @return {boolean}
	 */
	this.validate_password_same = function(password_current, password) {

		if ( !this.valid ) {
			try {
				password_current.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Current password and new password are the same', 'error');
			return false;
		}

		if ( !password_current.val() ==  password.val() ) {
			obj.focus();
			sweetAlert('Error', 'Current password and new password are the same', 'error');
			return false;
		}

		return true
	}; // validate_password_same()

	/**
	 * validate billing_address_type
	 *
	 * @return {boolean}
	 */
	this.validate_billing_address_type = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Address type is invalid', 'error');
			return false;
		}

		// for now only company and private values are accepted
		if ( obj.val() != 'company' && obj.val() != 'private' ) {
			sweetAlert('Error', 'Delivery address is invalid', 'error');
			return false;
		}

		return true;
	}; // validate_billing_address_type()

	/**
	 * validate billing title
	 *
	 * @return {boolean}
	 */
	this.validate_title = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Title is invalid', 'error');
			return false;
		}

		if (!obj.val() ) {
			sweetAlert('Error', 'Title is required', 'error');
			return false;
		}

		return true;
	}; // validate_title()

	/**
	 * validate billing address line 1
	 *
	 * @return {boolean}
	 */
	this.validate_billing_address_1 = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Address line 1 is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Address line 1 is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_address_1()

	/**
	 * validate billing company
	 *
	 * @return {boolean}
	 */
	this.validate_billing_company = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Company is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Company is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_company()

	/**
	 * validate billing address line 2
	 *
	 * @return {boolean}
	 */
	this.validate_billing_address_2 = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Address line 2 is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Address line 2 is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_address_2()

	/**
	 * validate billing city
	 *
	 * @return {boolean}
	 */
	this.validate_billing_city = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'District is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'District is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_city()

	/**
	 * validate billing state
	 *
	 * @return {boolean}
	 */
	this.validate_billing_state = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Province is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Province is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_state()

	/**
	 * validate billing country
	 *
	 * @return {boolean}
	 */
	this.validate_billing_country = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Country is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Country is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_country()

	/**
	 * validate billing postcode
	 *
	 * @return {boolean}
	 */
	this.validate_billing_postcode = function(obj) {
		return true;
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Postcode is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Postcode is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_postcode()

	/**
	 * validate postcode
	 *
	 * @return {boolean}
	 */
	this.validate_postcode = function(obj) {
		return this.validate_billing_postcode(obj);
	}

	/**
	 * validate billing mobile
	 *
	 * @return {boolean}
	 */
	this.validate_billing_mobile = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Mobile number is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Mobile number is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_mobile()

	/**
	 * validate billing phone
	 *
	 * @return {boolean}
	 */
	this.validate_billing_phone = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Phone number is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Phone number is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_phone()

	/**
	 * validate billing home/office
	 *
	 * @return {boolean}
	 */
	this.validate_billing_home_office = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Home or office is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Home or office is required', 'error');
			return false;
		}

		return true;
	}; // validate_billing_home_office()

	/**
	 * validate shipping mode
	 * @return {boolean}
	 */
	this.validate_shipping_mode = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Shipping mode is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Shipping mode is required', 'error');
			return false;
		}

		return true;
	}; // validate_shipping_mode()

	/**
	 * validate shipping mode standard options
	 * @return {boolean}
	 */
	this.validate_shipping_mode_standard_options = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Shipping mode standard delivery options is invalid', 'error');
			return false;
		}

		if ( obj.val() != 'Saturday Delivery' && obj.val() != 'Recycling at home' && obj.val() != 'Signature' ) {
			sweetAlert('Error', 'Shipping mode standard delivery options is required', 'error');
			return false;
		}

		return true;
	}; // validate_shipping_mode_standard_options()

	/**
	 * validate payment mode
	 * @return {boolean}
	 */
	this.validate_payment_mode = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Payment mode is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Payment mode is required', 'error');
			return false;
		}

		return true;
	}; // validate_payment_mode()

	/**
	 * validate subscribed_in_nesspresso_club
	 * @return {boolean}
	 */
	this.subscribed_in_nesspresso_club = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Subscription in Nesspresso Club mode is invalid', 'error');
			return false;
		}

		if ( obj.length && obj.val() != '1' ) {
			sweetAlert('Error', 'Subscription in Nesspresso Club is required', 'error');
			return false;
		}

		return true;
	}; // validate_payment_mode()

	/**
	 * validate subscribed_in_nesspresso_club
	 * @return {boolean}
	 */
	this.validate_subscribed_in_nesspresso_club = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Subscription in Nesspresso Club mode is invalid', 'error');
			return false;
		}

		if ( obj.length > 0 && obj.val() != '1' ) {
			sweetAlert('Error', 'Subscription in Nesspresso Club is required', 'error');
			return false;
		}

		return true;
	}; // subscribed_in_nesspresso_club()

	/**
	 * validate validate_contact_preferences
	 * @return {boolean}
	 */
	this.validate_contact_preferences = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Subscription in Nesspresso Club mode is invalid', 'error');
			return false;
		}

		if ( obj.length > 0) {

			$validValues = ['phone', 'post', 'sms'];

			$valid_contact_preferneces = true;

			$.each(obj, function(i,v) {

				var value = $(v).val();

				if ( $.inArray(value, $validValues) == -1 ) {
					$valid_contact_preferneces = false;
					return false;
				}
			});

			if ( !$valid_contact_preferneces ) {
				sweetAlert('Error', 'Subscription in Nesspresso Club is required', 'error');
				return false;
			}
		}

		return true;
	}; // validate_contact_preferences()

	/**
	 * validate validate_subscribed_in_nesspresso_news
	 * @return {boolean}
	 */
	this.validate_subscribed_in_nesspresso_news = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Subscription in Nesspresso News mode is invalid', 'error');
			return false;
		}

		if ( obj.length && obj.val() != '1' ) {
			sweetAlert('Error', 'Subscription in Nesspresso News is required', 'error');
			return false;
		}

		return true;
	}; // validate_subscribed_in_nesspresso_news()

	/**
	 * validate express checkout active
	 * @return {boolean}
	 */
	this.validate_express_checkout_active = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Express checkout is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Express checkout is required', 'error');
			return false;
		}

		return true;
	}; // validate_express_checkout_active()

	/**
	 * validate serial no
	 * @return {boolean}
	 */
	this.validate_serial_no = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Serial number is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Serial number is required', 'error');
			return false;
		}

		return true;
	}; // validate_serial_no()

	/**
	 * validate club_membership no
	 * @return {boolean}
	 */
	this.validate_club_membership_no = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Club Membership Number is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', 'Club Membership Number is required', 'error');
			return false;
		}

		return true;
	}; // validate_club_membership_no()

	/**
	 * validate obtained_by
	 * @return {boolean}
	 */
	this.validate_obtained_by = function(obj) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', '"HOW DID YOU OBTAIN YOUR MACHINE" is invalid', 'error');
			return false;
		}

		if ( !obj.val() ) {
			sweetAlert('Error', '"HOW DID YOU OBTAIN YOUR MACHINE" is required', 'error');
			return false;
		}

		return true;
	}; // validate_obtained_by()

	/**
	 * validate date
	 * @return {boolean}
	 */
	this.validate_date_string = function(date_string) {
		if ( !this.valid ) {
			try {
				obj.focus();
			} catch (e) {
				// do nothing
			}
			sweetAlert('Error', 'Date is invalid', 'error');
			return false;
		}

		if ( typeof date_string === 'undefined' || !date_string ) {
			sweetAlert('Error', 'Date is required', 'error');
			return false;
		}

		if ( !Date.parse(date_string))  {
		   sweetAlert('Error', 'Date is invalid', 'error');
		   return false;
		}

		return true;
	}; // validate_date_string()


	// alert message
	this.success_information_updated = function() {
		var current_url = window.location.href;
		if (current_url.includes('vi/my-account')) {
			var alert_title = 'Thành công';
			var alert_text = 'Thông tin của bạn đã được cập nhật';
			var alert_button = 'được';
		} else {
			sweetAlert('Success', 'Your information is updated', 'success');
			var alert_title = 'Success';
			var alert_text = 'Your information is updated';
			var alert_button = 'OK';
		}

		swal({
		  title: alert_title,
		  text: alert_text,
		  confirmButtonText: alert_button
		});
	}; // success_information_updated()

	// alert message with reload
	this.success_password_updated = function() {

		swal({
			title: "Success",
			text: "Password updated. You will be now redirected to login page",
			type: "success",
			showCancelButton: false,
			confirmButtonText: "Continue",
		},
		function(isConfirm){
			location.reload();
		});
	}; // success_password_updated()

	// alert message with redirect
	this.success_password_email_token_send = function() {
		var current_url = window.location.href;
		if (current_url.includes('vi/lost-password')) {
			var alert_title = 'Email đã được gửi';
			var alert_text = 'Một email đính kèm với đường link tạo lại mật khẩu đã được gửi đến hộp thư của bạn';
			var alert_button = 'Chuyển đến trang đăng nhập';
		} else {
			var alert_title = 'Email sent';
			var alert_text = 'An email is sent to you containing a link to reset your password.';
			var alert_button = 'Go to Login page';
		}
		swal({
			title: alert_title,
			text: alert_text,
			type: "info",
			showCancelButton: false,
			confirmButtonText: alert_button,
		},
		function(){
			window.location = window.host_url + '/login';
		});
	}; // success_password_email_token_send()

	// alert message
	this.success_password_reset = function() {
		var current_url = window.location.href;
		if (current_url.includes('vi/reset-password')) {
			var alert_title = 'Đặt lại mật khẩu';
			var alert_text = 'Bạn đã cập nhật mật khẩu thành công';
			var alert_button = 'Chuyển đến trang đăng nhập';
		} else {
			var alert_title = 'Success';
			var alert_text = 'Password is invalid';
			var alert_button = 'Go to Login page';
		}

		swal({
			showCancelButton: false,
			type: "success",
			title: alert_title,
		  	text: alert_text,
		  	confirmButtonText: alert_button
		},
		function(){
			window.location = window.host_url + '/login';
		});
	}; // success_password_reset()

	// error message for invalid token
	this.error_invalid_token = function() {
		swal({
			title: "Invalid Token",
			text: "The token provided is invalid or no longer active.",
			type: "error",
			showCancelButton: true,
			confirmButtonText: "Go to lost password page",
			cancelButtonText: "Go homepage",
		},
		function(isConfirm){
			if ( isConfirm )
				window.location = window.host_url + '/lost-password';
			else
				window.location = window.host_url;
		});
	} // error_invalid_token()

	// initialize class
	this.init();

}// Validation()
