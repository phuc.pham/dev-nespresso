(function($) {
    "use strict";
    var Nespresso = {
        desktopScreen: 996,
        mobileScreen: 768,
        mobilePortrait: 480,
        screenWidth: $(window).width(),
        statusMove: false,
        speedAnimation: 300,
        menuIconWidth: 50,
        menuWidth: 0,
        sliderV: 0,
        mobileType: '',
        init: function() {
            this.slider();
            this.sliderMachine();
            this.sliderViews();
            this.menuSlide();
            this.zoomMachine();
            this.sidebarHeight();
            this.checkBasket();
            //this.rating();
            this.mobileEvent();
            this.signInTooltip();
            this.lightbox();
            this.currentOpened = -1;
        },

        reset: function(){
            // $("body").removeClass("nav-fixed");
            // $(".main-nav").css("height", "auto");
            $('#main-wrapper, .mobile-menu').css({'left': 0});
            // $("#mask").hide();
            
            if( $('.mobile-menu').hasClass('arrow-expand') ){
                $('.mobile-menu').trigger("click");
            }
        },

        menuSlide: function(){
            var self = this;
            var count = 1;
            var lastidx = -1;
            
            if ( Modernizr.touch ){ //Mobile + Tablet

                var menuclick = $('.menu-item > a').on("click", function(e){
                    var idx = $('.menu-item').index($(this).parent());

                    if( $(this).parent().find('.sub-menu').length ){
                        e.preventDefault();
                        var $submenu = $(this).parent().find('.sub-menu');

                        if( $submenu.is(":visible") && lastidx === idx ){
                            $(this).removeClass('active');
                            $submenu.slideUp('fast');
                            idx = -1;
                        }else{
                            $('.menu-container .sub-menu').hide();
                            $('.menu-item > a').removeClass('active');
                            $(this).addClass('active');
                            $(this).parent().find('.sub-menu').slideDown('fast');
                        }
                    }

                    lastidx = idx;
                });

            }else{ //Desktop

                var menuslidedown = $('.menu-item > a').bind('mouseover focus',function(){

                    if (count === 1 && $(this).parent().find('.sub-menu').length){
                        $(this).parent().find('.sub-menu').slideDown('fast');
                        count = 0;
                    }
                    else if(count === 0 && !$(this).parent().find('.sub-menu').length){
                        $('.menu-container .sub-menu').hide();
                    }
                    else{
                        $('.menu-container .sub-menu').hide();
                        $(this).parent().find('.sub-menu').show();
                    }
                });

                var menuleave = $('nav.main-nav').mouseleave(function(){
                    if (Modernizr.touch) return;

                    count = 1;
                    $(this).find('.sub-menu').slideUp('fast');
                });
            }
        },
        mobileEvent: function(){
            var self = this;

            //Menu Slide on device
            this.menuWidth = $('.menu-container').width();
            $('.menu-container').css('left', -this.menuWidth);

            $('.mobile-menu').click(function(e) {
                if( self.statusMove || self.screenWidth > self.desktopScreen) return;
                self.statusMove = true;

                // self.moveWrapper($(this),self.menuWidth);
                self.moveMenu($(this), $('.menu-container'));
                $('.menu-container').show();
                
                if( $('.mobile-menu').hasClass('arrow-expand') ){
                    $("body").addClass("nav-fixed");
                    $(".main-nav")
                        .height($(window).height())
                        .addClass("overflow");
                }else{
                    $("body").removeClass("nav-fixed");
                    $(".main-nav")
                        .css("height", "auto")
                        .removeClass("overflow");
                }

                if(self.screenWidth > self.mobilePortrait) {
                    self.mobileType = 'tablet';
                }else{
                    self.mobileType = 'mobile';
                }

                $('.menu-container .sub-menu').hide();
            });

            $('#footer h3').click(function(e){
                $(this).toggleClass('active').next().slideToggle();
            });

            $('#mask, .main-nav').click(function(e){

                if( $(e.target).closest(".menu-container").length ) return;

                $(".main-nav")
                    .css("height", "auto")
                    .removeClass("overflow");

                $("body").removeClass("nav-fixed");
                $('.mobile-menu').animate({'left':0}, self.speedAnimation);
                $('.menu-container-inner').animate({'left': -self.menuWidth, opacity: 0}, self.speedAnimation, function() {
                    $("#mask").hide();
                    $('.mobile-menu').removeClass('arrow-expand');
                });
                
                // if($('.clone-menu').length) {
                //     $('.clone-menu').animate({'left': -self.menuWidth}, self.speedAnimation, function() {
                //         $(this).hide();
                //     });
                    $('#main-wrapper').animate({'left': 0}, self.speedAnimation);
                // }
                $("#mask").fadeOut();
            });
        },
        signInTooltip: function() {
            var self = this;

            $('.drop__sign__mask, #drop__sign__close').on('click', function (e) {
                e.preventDefault();
                $('#drop__sign__close').fadeOut();
                $('.drop__sign__tooltip').fadeOut();
                $('.drop__sign__mask').slideUp();
            });
            $('#sign__tooltip').on('click', function (e) {
                e.preventDefault();
                if(self.screenWidth >= self.mobileScreen) {
                    $('#drop__sign__close').hide();
                } else{
                    $('#drop__sign__close').fadeToggle();
                }
                $('.drop__sign__tooltip').slideToggle();
                $('.drop__sign__mask').fadeToggle();
            });

            $(window).resize(function () {
                var sW = self.screenWidth;
                setTimeout(function () {
                    if( (self.screenWidth >= self.mobileScreen &&  sW < self.mobileScreen) || (sW >= self.mobileScreen &&  self.screenWidth < self.mobileScreen) ) {
                        $('#drop__sign__close').hide();
                        $('.drop__sign__tooltip').hide();
                        $('.drop__sign__mask').hide();
                    }
                },2);
            });

            
        },
        lightbox: function() {

	        $('.show-lightbox').on('click', function (e) {
	            e.preventDefault();
		        var $this = $(this);
		        $this.next().show();
	            setTimeout( function () {
		            $this.next().addClass('lightbox--active');
	            }, 100);
	        });

	        $('.lightbox').on('click', '.lightbox__close', function (e) {
	            e.preventDefault();
		        $('.lightbox--active').removeClass('lightbox--active');
	            setTimeout( function () {
		            $('.lightbox').hide();
	            }, 800);
	        });

	        $(document).on('click', function(e) {
	            if($($(e.target).context).hasClass('lightbox--active')) {
	                $('.lightbox__close').click();
                }
	        });

        },
        moveMenu: function(element, boxWrap) {
            var self = this;

            if(!element.hasClass('arrow-expand')) {
                element.addClass('arrow-expand');
                $('#mask').fadeIn(100);
                self.expandEvent(element, boxWrap);
            }
            else {
                element.removeClass('arrow-expand');
                self.collapseEvent(element, $('.menu-container'));
                $('#mask').fadeOut(100);
            }
        },
        moveWrapper: function(element, moveWidth) {
            var self = this;
            if(!element.hasClass('arrow-expand')) {
                $('#main-wrapper').animate({'left': moveWidth}, self.speedAnimation);
            }
            else {
                $('#main-wrapper').animate({'left': 0}, self.speedAnimation);
            }
        },
        positionMenu: function() {

            if(this.screenWidth >= this.desktopScreen) {
                $('#mask').hide();
                $('.mobile-menu').removeClass('arrow-expand');
                $('.mobile-menu').removeAttr('style');
                $('.menu-container').removeAttr('style');
            } else {
                this.menuWidth = $('.menu-container').width();
                if(this.mobileType == '') {
                    $('.menu-container').css('left', -this.menuWidth);
                }

                var leftPos = this.screenWidth - this.menuIconWidth;
                if($('.mobile-menu').length && $('.mobile-menu').hasClass('arrow-expand')) {
                    $('.mobile-menu').css({'left': 'inherit', 'right': 0});
                }
                if(!$('.mobile-menu').hasClass('arrow-expand')) {
                    $('.menu-container-inner').css('left', -leftPos);
                    $('.menu-container').hide();
                }
                
                $('#main-wrapper').css('left', this.menuWidth);
                if(!$('.mobile-menu').hasClass('arrow-expand')) {
                    $('.menu-container').css('left', -this.menuWidth);
                }
            }

        },
        expandEvent: function(element,boxWrap) {
            var self = this;
            var boxWrap = boxWrap || element.next();
            element.animate({'left': self.menuWidth}, self.speedAnimation, function() {
            });
            boxWrap.animate({'left': 0, opacity: 1}, self.speedAnimation, function() {
                self.statusMove = false;
            });
        },
        collapseEvent: function(element,boxWrap) {
            var self = this;
            var boxWrap = boxWrap || element.next();
            element.animate({'left': 0}, self.speedAnimation, function() {
            });
            boxWrap.animate({'left': -self.menuWidth, opacity: 0}, self.speedAnimation, function() {
                $(this).hide();
                self.statusMove = false;
            });
        },
        slider: function() {
            var self = this;
            var slider = $('.slider-list').bxSlider({
                auto: true,
                autoControls: true,
                adaptiveHeight: true,
                onSliderLoad: function() {
                    if (self.screenWidth <= self.desktopScreen) {
                        Nespresso.arrowResponsive();
                    }
                },
                onSliderResize: function() {
                    if ($(window).width() <= self.desktopScreen) {
                        Nespresso.arrowResponsive();
                    } else {
                        $('.slider .bx-controls .bx-controls-auto').removeAttr("style");
                        $('.slider .bx-controls .bx-pager').removeAttr("style");
                        $('.slider .bx-controls .bx-controls-direction').removeAttr("style");
                    }
                }
            });
        },
        sliderMachine: function() {
            var self = this;
            var slider = $('.slider-list-machine').bxSlider({
                auto: false,
                controls: false,
                autoControls: false,
                adaptiveHeight: true,
                mode: 'fade',
                onSliderLoad: function() {
                    if (self.screenWidth <= self.desktopScreen) {
                        Nespresso.arrowResponsive();
                    }
                },
                onSliderResize: function() {
                    if ($(window).width() <= self.desktopScreen) {
                        Nespresso.arrowResponsive();
                    } else {
                        $('.slider .bx-controls .bx-controls-auto').removeAttr("style");
                        $('.slider .bx-controls .bx-pager').removeAttr("style");
                        $('.slider .bx-controls .bx-controls-direction').removeAttr("style");
                    }
                }
            });
        },
        sliderViews: function() {

            var mode, minSlides, slideWidth;
            if ( Nespresso.screenWidth >= Nespresso.mobileScreen ) {
                mode = 'vertical';
                minSlides = 3;
                slideWidth = 0;
            } else {
                mode = 'horizontal';
                minSlides = 2;
                slideWidth = 500;
            }

            if(!$('.slider__visual .bx-wrapper').length) {
                Nespresso.sliderV = $('.slider-face').bxSlider({
                    mode: mode, auto: false, pager: false, minSlides: minSlides, maxSlides: minSlides, slideWidth: slideWidth, moveSlides: 1, slideMargin: 20
                });
            } else {
                Nespresso.sliderV.reloadSlider({mode: mode, auto: false, pager: false, minSlides: minSlides, slideWidth: slideWidth, maxSlides: minSlides, moveSlides: 1, slideMargin: 20});
            }

        },
        arrowResponsive: function() {
            var imgH = $('.slider-list img').height();
            var arrowH = $('.slider .bx-wrapper .bx-controls-direction a').height();
            var arrowMid = (imgH - arrowH) / 2;
            var bxPagerLength = $('.slider .bx-pager-item').length;
            var bxPagerWidth = $('.slider .bx-pager-item').width() * bxPagerLength;
            $('.slider .bx-controls .bx-controls-auto').css({
                right: (($(".slider").width() - bxPagerWidth - 40)/2),
                top: imgH - 40
            });
            $(".slider .bx-controls .bx-pager").css("top", imgH - 40);
            $('.slider .bx-controls .bx-controls-direction').css("top", arrowMid);
        },
        sidebarHeight: function() {
            if (this.screenWidth > this.mobileScreen) {
                if ($("#page-content").length) {
                    var $h = $("#page-content").height();
                    $("#sidebar .sidebar-inner").css("height", $h + "px");
                }
            }
        },
        basketHeight: function(){

            if( $(".basket-dialog").length ){
                $(".basket-dialog").height($(window).height());
            }
        },
        checkBasket: function(){
            var self = this,
                $cartIcon = $(".custom-dropdown.basket .btn-basket"),
                $basketDialog = $(".basket-dialog"),
                $mask = $('#mask-mini-basket'),
                opened = false;

            $("#mask-mini-basket, .basket .close").on("click", function(e){
                e.preventDefault();
                self.closeBasket($basketDialog, $cartIcon, $mask, function(){
                    opened = false;
                });
            });

            $cartIcon.click(function(e){
                e.preventDefault();
                if(opened) {
                    self.closeBasket($basketDialog, $cartIcon, $mask, function(){
                        opened = false;
                    });
                }else {
                    $basketDialog.show();
                    
                    opened = true;

                    if (self.screenWidth <= self.desktopScreen) {
                        setTimeout(function(){    
                            $cartIcon.closest(".basket").addClass("opened");
                        }, 10);
                    }

                    /*
                    setTimeout(function(){
                        $('body').addClass('fixed');
                    }, 500);
                    */

                    $mask.fadeIn(200);
                    setTimeout(function(){    
                        $basketDialog.addClass("opened");
                    }, 10);
                }
            });

        },
        closeBasket: function($basketDialog, $cartIcon, $mask, cb){
            $mask.fadeOut(200);

            // $('body').removeClass('fixed');

            $basketDialog.removeClass("opened");
            $cartIcon
                .closest(".basket")
                .removeClass("opened");

            setTimeout(function(){
                $basketDialog.hide();

                if( cb !== undefined )
                    cb();
            }, 500);
        },
        rating: function() {
            $('.ratings-stars').hover(
                function() {
                    $(this).prevAll().andSelf().addClass('ratings-over');
                    $(this).nextAll().removeClass('ratings-vote');
                },
                function() {
                    $(this).prevAll().andSelf().removeClass('ratings-over');
                }
            );
        },
        zoomMachine: function() {
            var $dots = $('.product__slider__dots');
            var $header = $('.product__header');
            var animated = false;

            $('.product__slider__list img, .product__slider__close').on('click', function (e) {
                e.preventDefault();

                // disable zoom if on mobile
                if (/Mobi/.test(navigator.userAgent)) {
                    return false;
                }

                if(!animated) {
                    animated = true;
                    var mtop = Nespresso.screenWidth <= Nespresso.mobileScreen ? 50 : 79;
                    $('html,body').animate({scrollTop: $("#product__header").offset().top - mtop}, 400).focus();
                    if($header.hasClass('active')) {
                        if(Nespresso.screenWidth < Nespresso.mobileScreen) {
                            $header.removeClass('active').css({height: 'auto'});
                        } else {
                            var h = $('.product__info > div').outerHeight() > 600 ? $('.product__info > div').outerHeight() : 600;
                            $header.removeClass('active').animate({height: h}, 400);
                        }
                        $dots.addClass('pending');
                        setTimeout(function () {
                            $dots.removeClass('zoomed');
                        }, 400);
                        setTimeout(function () {
                            $dots.removeClass('pending');
	                        animated = false;
                        }, 800);
                    } else {
                        $header.addClass('active').animate({height: $(window).height() - mtop}, 400);
                        $dots.addClass('pending');
                        setTimeout(function () {
                            $dots.removeClass('pending').addClass('zoomed');
	                        animated = false;
                        }, 400);
                    }

                    setTimeout(function () {
                        $('body').toggleClass('no-scroll');
                    }, 400);

                }
            })
        }
    };

    jQuery(document).ready(function() {
        Nespresso.init();
        $(window).resize(function () {
            Nespresso.sidebarHeight();
            Nespresso.basketHeight();
            Nespresso.screenWidth = $(window).width();
            Nespresso.positionMenu();
            Nespresso.reset();
            Nespresso.sliderViews();
        });
    });
	$('form[name="checkout"]').addClass('needs-validation ');
	$('form[name="checkout"] >div').addClass('gray-bgcolor');
	var vi =location.pathname.substring(0,4)=='/vi/'?'vi':'';
	$(document).on('keyup', '.needs-validation input,.needs-validation select,.needs-validation textarea', function () {
		var _that = $(this);
		if (_that.is(':required')) {
		  if (!_that.val().trim()) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi));
			_that.addClass('is-invalid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
		if (_that.data('regix')) {
		  var regix = new RegExp(_that.data('regix'));
		  if (!regix.test(_that.val().trim())) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
		if (_that.data('match')) {
		  var match = _that.data('match');
		   if ($('.needs-validation #'+match).val().trim() != _that.val().trim()) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
	  });
	  $(document).on('change', '.needs-validation input,.needs-validation select,.needs-validation textarea', function () {
		var _that = $(this);
		if (_that.is(':required')) {
		  if (!_that.val().trim()) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
		if (_that.data('regix')) {
		  var regix = new RegExp(_that.data('regix'));
		  if (!regix.test(_that.val().trim())) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
		if (_that.data('match')) {
		  var match = _that.data('match');
		   if ($('.needs-validation #'+match).val().trim() != _that.val().trim()) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		  }
	  });
	  $(document).on('blur', '.needs-validation input,.needs-validation select,.needs-validation textarea', function () {
		var _that = $(this);
		if (_that.is(':required')) {
		  if (!_that.val().trim()) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
		if (_that.data('regix')) {
		  var regix = new RegExp(_that.data('regix'));
		  if (!regix.test(_that.val().trim())) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
		if (_that.data('match')) {
		  var match = _that.data('match');
		  if ($('.needs-validation #'+match).val().trim() != _that.val().trim()) {
			var $m = _that.parent('div').children('[data-msg'+vi+']');
			$m.addClass('text-danger').removeClass('form-text text-muted').html($m.data('msg'+vi)).show();
			$(_that).addClass('is-invalid').removeClass('is-valid');
		  }else
		  {
			_that.addClass('is-valid').removeClass('is-invalid');
			_that.parent('div').children('[data-msg'+vi+']').hide();
		  }
		}
	  });
})(jQuery);
