<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 * From: plugins/woocommerce/single-product.php
 *
 *
 * @link https://minionsolutions.com
 *
 * @since 1.6.4
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php get_header(); ?>

<!-- content -->
<?php

	global $post;

	$product_type = get_field( 'product_type', $post->ID );

	switch ( $product_type ) :

		case 'Coffee' :
			include __DIR__.'/../components/coffee-page.php';
			break;

		case 'Machine' :
			include __DIR__.'/../components/machine-page.php';
			break;

		case 'Accessory' :
			include __DIR__.'/../components/accessory-page.php';
			break;

	endswitch;

?>
<!-- /content -->


<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		// do_action( 'woocommerce_before_main_content' );
	?>

		<?php //while ( have_posts() ) : the_post(); ?>

			<?php //wc_get_template_part( 'content', 'single-product' ); ?>

		<?php //endwhile; // end of the loop. ?>

<!-- get_footer -->
<?php get_footer(); ?>
