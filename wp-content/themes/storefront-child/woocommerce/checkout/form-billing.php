<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//checkout validation
chekout_validation();

/** @global WC_Checkout $checkout */

$user = nespresso_get_user_data();

// $country = nespresso_get_country($user->billing_country);

$states = nespresso_get_states();
asort($states);
$cities = nespresso_get_cities($user->billing_state);
$country_codes = get_country_codes();
?>
<div class="woocommerce-billing-fields">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3  class="c-header" style="margin-top: 20px;"><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3  class="c-header"  style="margin-top: 20px;"><?php _e( 'Billing address', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="woocommerce-billing-fields__field-wrapper ">
        <input type="hidden" id="first_name" name="first_name" value="<?= isset($user->first_name) ? $user->first_name : '' ?>">
        <input type="hidden" id="last_name" name="last_name" value="<?= isset($user->last_name) ? $user->last_name : '' ?>">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="input-group input-group-generic company-hidden hide">
                    <label class="desktop-label col-sm-3 col-md-4" for="title">Title<span class="">*</span></label>
                    <div class="col-sm-6">
                        <div class="dropdown dropdown--input dropdown--init">
                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                <span class="mobile-label">Title<span class=>*</span></span>
                                <span class="current">Please select</span>
                            </button>
                            <select tabindex="-1" name="title">
                                <option value="">Please select</option>
                                <option value="Mr/Ms" <?= isset($user->title) && $user->title == 'Mr/Ms' ? 'selected="selected"' : '' ?> >Mr/Ms</option>
                                <option value="Mr" <?= isset($user->title) && $user->title == 'Mr' ? 'selected="selected"' : '' ?>>Mr</option>
                                <option value="Ms" <?= isset($user->title) && $user->title == 'Ms' ? 'selected="selected"' : '' ?>>Ms</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="input-group input-group-generic" style="display: none;">
                    <label class="desktop-label col-sm-3 col-md-4" for="billing_email">Email address<span class="required">*</span></label>
                    <div class="col-sm-6">
                        <input type="text"
                            title="Email address"
                            id="billing_email"
                            name="billing_email"
                            placeholder="billing_email address"
                            class="form-control col-sm-12 col-md-9"
                            value="<?= isset($user->email) ? $user->email : '' ?>"
                        >
                        <span class="mobile-label">Email address<span class="required">*</span></span>
                    </div>
                </div>
				<div class="input-group col-sm-12" style="display: none;">
					<label class="desktop-label col-sm-12"> <input type="checkbox" id="otheraddress"/> Use different address for Billing</label>
				</div>
				<div class="input-group col-sm-12">
					<label class="desktop-label col-sm-12"> <input type="checkbox" id="sredinvoice"/> I need VAT invoice (Your invoice will be sent electronically via email after your order is completed) </label>
				</div>
                <div class="input-group col-sm-12">
                    <div class="desktop-label col-sm-12">
                        <span
                                style="font-family: bold;text-decoration: underline;font-family: 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;-webkit-font-smoothing: antialiased;"> Note:</span>
                        Any VAT invoice request without filling below information will not be accepted. </div>
                </div>
				<div id="redinvoice"  style="display:none" >
					<div class="input-group input-group-generic col-sm-6">
						<label class="desktop-label col-sm-12" for="red_name">Billing name<span class="required">*</span></label>
						<div class="col-sm-12">
							<input type="text"
								title="Billing name"
								id="red_name"
								name="red_name"
								placeholder="Billing name"
								class="form-control"
								value=""
								required
							>
							<small id="helpred_name" data-msgvi="Vui lòng nhập tên hóa đơn" data-msg="Please provide a billing name" class="text-danger"></small>
							<span class="mobile-label">Billing name<span class="required">*</span></span>
						</div>
					</div>

					<div class="input-group input-group-generic col-sm-6">
						<label class="desktop-label col-sm-12" for="vat_number">VAT number<span class="required">*</span></label>
						<div class="col-sm-12">
							<input type="text"
								title="VAT number"
								id="vat_number"
								name="vat_number"
								placeholder="VAT number"
								class="form-control "
								value=""
								required
							>
							<small id="helpvat_number" data-msgvi="Vui lòng nhập mã số thuế" data-msg="Please provide a VAT number" class="text-danger"></small>
							<span class="mobile-label">VAT number<span class="required">*</span></span>
						</div>
					</div>
					<div class="input-group input-group-generic col-sm-12">
						<label class="desktop-label col-sm-12" for="red_address">Billing address<span class="required">*</span></label>
						<div class="col-sm-12">
							<textarea rows="3"
								title="Billing address"
								id="red_address"
								name="red_address"
								placeholder="Billing address"
								class="form-control "
								value=""
								required
							></textarea>
							<small id="helpred_address" data-msgvi="Vui lòng nhập địa chỉ công ty" data-msg="Please provide a billing address" class="text-danger"></small>
							<span class="mobile-label">Billing address<span class="required">*</span></span>
						</div>
					</div>
                    <div class="input-group input-group-generic col-sm-12">
						<label class="desktop-label col-sm-12" for="red_email_invoice">Email address<span class="required">*</span></label>
						<div class="col-sm-12">
							<input  type="email"
								title="Email address"
								id="red_email_invoice"
								name="red_email_invoice"
								placeholder="Email address"
								class="form-control "
								value=""
								required
							/>
							<small id="helpred_email_invoice" data-msgvi="Vui lòng nhập email" data-msg="Please provide a email address" class="text-danger"></small>
							<span class="mobile-label">Email address<span class="required">*</span></span>
						</div>
					</div>
				</div>
				<div id="other_address"  style="display:none" >
                <div class="input-group input-group-generic col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_first_name">First name<span class="required">*</span></label>
                    <div class="col-sm-12">
                        <input type="text"
                            title="First name"
                            id="billing_first_name"
                            name="billing_first_name"
                            placeholder="First name"
                            class="form-control"
                            value="<?= isset($user->billing_first_name) ? $user->billing_first_name : '' ?>"
							required
                        >
						<small id="helpfirst_name2" data-msgvi="Vui lòng nhập tên của bạn" data-msg="Please provide a first name" class="text-danger"></small>
                        <span class="mobile-label">First name<span class="required">*</span></span>
                    </div>
                </div>

                <div class="input-group input-group-generic col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_last_name">Last name<span class="required">*</span></label>
                    <div class="col-sm-12">
                        <input type="text"
                            title="Last name"
                            id="billing_last_name"
                            name="billing_last_name"
                            placeholder="Last name"
                            class="form-control "
                            value="<?= isset($user->billing_last_name) ? $user->billing_last_name : '' ?>"
							required
                        >
						<small id="helplast_name2" data-msgvi="Vui lòng nhập họ của bạn" data-msg="Please provide a last name" class="text-danger"></small>
                        <span class="mobile-label">Last name<span class="required">*</span></span>
                    </div>
                </div>

                <div class="input-group input-group-generic col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_company">Company</label>
                    <div class="col-sm-12">
                        <input type="text"
                            title="Company"
                            id="billing_company"
                            name="billing_company"
                            placeholder="Company"
                            class="form-control "
                            value="<?= isset($user->billing_company) ? $user->billing_company : '' ?>"
                        >
                        <span class="mobile-label">Company</span>
                    </div>
                </div>


                <div class="input-group input-group-generic is-focused col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_last_name">Mobile<span class="required">*</span></label>
                    <div class="col-sm-2 hide">
                        <select name="billing_country_code" id="billing_country_code" class="form-control pdtp10">
                            <?php foreach ($country_codes as $c_code): ?>
                               <option value="<?=$c_code?>" <?= $user->billing_country_code == $c_code ? 'selected="selected"' : '' ?> ><?=$c_code?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                    <div class="col-sm-12">
                        <input type="text"
                            title="Mobile"
                            id="billing_mobile"
                            name="billing_mobile"
                            class="form-control "
                            value="<?= isset($user->billing_mobile) ? $user->billing_mobile : '' ?>"
                            data-regix="^(086|096|097|098|032|033|034|035|036|037|038|039|091|094|088|083|084|085|081|082|089|090|093|070|079|077|076|078|092|056|058|099|059)([0-9]{7})$"                                            
											required
                        >
						<small id="helpmobile2" data-msgvi="Vui lòng nhập đúng số điện thoại của bạn (e.g. 0987654321)" data-msg="Please enter a valid phone number (e.g. 0987654321)" class="text-danger"></small>
                        <span class="mobile-label">Mobile<span class="required">*</span></span>
                        <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both;"><small>Example: 0912345678</small></p>
                    </div>
                </div>

                <div class="input-group input-group-generic col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_address_1">Address line 1<span class="required">*</span></label>
                    <div class="col-sm-12">
                        <input type="text"
                        	title="Address line 1"
                        	id="billing_address_1"
                        	name="billing_address_1"
                        	placeholder="Address line 1"
                        	class="form-control "
                            value="<?= isset($user->billing_address_1) ? $user->billing_address_1 : '' ?>"
							required
                		>
						<small id="helpbilling_address_1" data-msgvi="Vui lòng nhập địa chỉ của bạn" data-msg="Please provide a address" class="text-danger"></small>
                        <span class="mobile-label">Address line 1<span class="required">*</span></span>
                    </div>
                </div>

                <div class="input-group input-group-generic col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_address_2">Address line 2</label>
                    <div class="col-sm-12">
                        <input type="text"
                        	title="Address line 2"
                        	id="billing_address_2"
                        	name="billing_address_2"
                        	placeholder="Address line 2"
                        	class="form-control "
                            value="<?= isset($user->billing_address_2) ? $user->billing_address_2 : '' ?>"
                    	>
						<small id="helpbilling_address_2" class="text-danger">&nbsp;</small>
                        <span class="mobile-label">Address line 2</span>
                    </div>
                </div>

                <div class="input-group input-group-generic col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_state">Province<span class="required">*</span></label>
                    <div class="col-sm-12">
                        <div class="dropdown dropdown--input dropdown--init">
                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                 <?php if ($user->billing_state): ?>
                                <span class="mobile-label active">Province</span>
                                <span class="current active"><?= $user->billing_state ?></span>
                                <?php else: ?>
                                <span class="mobile-label">Province</span>
                                <span class="current">Please select</span>
                                <?php endif ?>
                            </button>
                            <select tabindex="-1" name="billing_state" id="billing_state">
                                <?php foreach ( $states as $code => $name ) : ?>
                                    <option value="<?= $code ?>" <?= $user->billing_state == $code ? 'selected="selected"': '' ?>><?= $name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <!-- <input type="text" tabindex="-1" name="billing_state" id="billing_state" value="<?php echo $user->billing_state ?>" placeholder="ex. Metro Manila" class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"> -->
                        </div>
                    </div>
                </div>


                <div class="input-group input-group-generic col-sm-6">
                    <label class="desktop-label col-sm-12" for="billing_city">District<span class="required">*</span></label>
                    <div class="col-sm-12">
                        <div class="dropdown dropdown--input">
                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
                                <?php if ($user->billing_city): ?>
                                <span class="mobile-label active">District</span>
                                <span class="current current_district active"><?= $user->billing_city ?></span>
                                <?php else: ?>
                                <span class="mobile-label">District</span>
                                <span class="current current_district">Please select</span>
                                <?php endif ?>
                            </button>
                            <select tabindex="-1" name="billing_city" id="billing_city">
                                <?php foreach ( $cities as  $city ) : ?>
                                    <option value="<?= $city ?>" <?= $user->billing_city == $city ? 'selected="selected"': '' ?>><?= $city ?></option>
                                <?php endforeach; ?>
                            </select>
                            <!-- <input type="text" tabindex="-1" name="billing_city" id="billing_city" value="<?php echo $user->billing_city ?>" placeholder="ex. Metro Manila" class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"> -->
                        </div>
                    </div>
                </div>
				</div>
                <!-- country start -->

                <input type="hidden" name="billing_country" value="VN">
                <!-- country end -->

                <!-- <div class="input-group input-group-generic is-focused">
                    <label class="desktop-label col-sm-3 col-md-4" for="billing_postcode">Post code</label>
                    <div class="col-sm-3">
                        <input type="text"
                        	title="Postal code"
                        	id="billing_postcode"
                        	name="billing_postcode"
                        	placeholder="Ex: 1221"
                        	class="form-control col-sm-12 col-md-9 form-control--withPlaceholder"
                            value="<?= isset($user->billing_postcode) ? $user->billing_postcode : '' ?>"
                    	>
                        <span class="mobile-label">Postal code</span>
                    </div>
                </div> -->

            	<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
					<!-- <div class="woocommerce-account-fields hide">
						<?php if ( ! $checkout->is_registration_required() ) : ?>

							<p class="form-row form-row-wide create-account">
								<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
									<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ) ?> type="checkbox" name="createaccount" value="1" /> <span><?php _e( 'Create an account?', 'woocommerce' ); ?></span>
								</label>
							</p> -->

						<?php endif; ?>

						<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

						<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

						<!-- 	<div class="create-account">
								<?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) : ?>
									<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
								<?php endforeach; ?>
								<div class="clear"></div>
							</div> -->

						<?php endif; ?>

						<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
					</div>

                <?php endif; ?>

            </div>
        </div>

		<?php /*
			$fields = $checkout->get_checkout_fields( 'billing' );

			foreach ( $fields as $key => $field ) {
				if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
					$field['country'] = $checkout->get_value( $field['country_field'] );
				}
				woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			}*/
		?>
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>
