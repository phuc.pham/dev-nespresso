<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$email_header   = get_nespresso_email_header();
$image_url      = (isset($email_header['email_header_image_url']) && $email_header['email_header_image_url']) ? $email_header['email_header_image_url'] : 'https://www.nespresso.com/shared_res/newsletter/transactional/img/shipping_confirmation.jpg';
$label          = (isset($email_header['email_header_button_label']) && $email_header['email_header_button_label']) ? $email_header['email_header_button_label'] : 'Order Coffee';
$button_url     = (isset($email_header['email_header_button_link']) && $email_header['email_header_button_link']) ? $email_header['email_header_button_link'] : 'http://www.nespresso.vn/coffee-list/';
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
	<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
	<style type="text/css">
		/* Client-specific Styles */
		#outlook a {
		  padding: 0;
		}

		.ReadMsgBody {
		  width: 100%;
		}

		.ExternalClass {
		  width: 100%;
		}

		body {
		  -webkit-text-size-adjust: 100%;
		  -ms-text-size-adjust: 100%;
		  -webkit-font-smoothing: antialiased;
		  font-family: Trebuchet MS, Helvetica, arial, sans-serif;
		}

		.yshortcuts,
		.yshortcuts a,
		.yshortcuts a:link,
		.yshortcuts a:visited,
		.yshortcuts a:hover,
		.yshortcuts a span {
		  text-decoration: none !important;
		  border-bottom: none !important;
		  background: none !important;
		}

		/*link style*/

		td {
		  font-family: Trebuchet MS, Helvetica, arial, sans-serif;
		}

		td[class~=fullCol] {
		  padding: 40px 50px;
		}

		td[class~=pre] {
		  padding: 10px 50px;
		}

		p {
		  margin: 0;
		  padding: 0;
		}

		a {
		  color: #666666;
		  text-decoration: none;
		  outline: none;
		}

		a:hover {
		  text-decoration: none
		}

		#addresses {
			border: 0;
			color: #fff;
		}

		.wrap.dark, .wrap.dark span, .wrap.dark p {
			font-size: 14px;
			color: #fff;
		}

		.wrap.dark h2, .wrap.dark h3 {
			text-align: center;
			text-transform: uppercase;
			padding: 0 0 10px;
			border-bottom: 1px solid #333;
		}

		.wrap.dark h2 {
			margin: 25px 0 15px;
			font-size: 16px;
			color: #fff;
		}

		.wrap.dark h3 {
			margin: 10px 0 15px;
			font-size: 15px;
			color: #a3a6a8;
		}

		.wrap.dark ul {
			list-style-type: none;
			margin: 15px 0;
			padding: 0;
		}

		.wrap.dark li {
			font-size: 14px;
			color: #fff;
		}

		.wrap.dark td {
			border: 0;
		}

		th {
			font-size: 15px;
		}

		td {
			font-size: 14px;
		}

		@media only screen and (max-width: 599px) {
		  td[class~=module-td] {
		    padding: 20px !important;
		  }

		  td[class~=fullCol],
		  td[class~=pre] {
		    padding: 30px !important;
		  }

		  table[class~=wrap] {
		    width: 100% !important;
		  }

		  table[class~=row] {
		    width: 440px !important;
		  }

		  img {
		    height: auto !important;
		  }

		  img[class~=line] {
		    height: 4px !important;
		  }

		  img[class~=img] {
		    width: 100% !important;
		    height: auto !important;
		    max-width: 100% !important;
		    display: block !important;
		  }

		  img[class~=title-border-img] {
		    height: 1px !important;
		  }

		  table[class~=logo] {
		    width: 100% !important;
		    margin-bottom: 10px !important;
		  }

		  table[class~=menu-tb] {
		    float: none !important;
		    margin: 0 auto !important;
		  }

		  td[class~=menu-box] {
		    height: 40px !important;
		  }

		  table[class~=menu] .content {
		    text-align: center !important;
		  }

		  td[class~=header] {
		    padding-bottom: 10px !important;
		  }

		  td[class~=top-bar] {
		    padding: 5px 10px !important;
		  }

		  table[class=footer-left] {
		    width: 100% !important;
		  }

		  table[class=footer-left] td {
		    text-align: center !important;
		  }

		  table[class=footer-right] {
		    width: 100% !important;
		  }

		  table[class=footer-right] td {
		    text-align: center !important;
		  }

		  td[class~=col-1-3] {
		    width: 50% !important;
		  }

		  td[class~=col-2-3] {
		    width: 50% !important;
		  }

		  table[class~=col3] {
		    width: 33% !important;
		  }

		  table[class~=col3_5] {
		    width: 40% !important;
		  }

		  table[class~=col4] {
		    width: 50% !important;
		  }

		  table[class~=col4double] {
		    width: 100% !important;
		  }

		  table[class~=col3] .general-img-td {
		    padding-right: 10px !important;
		  }

		  table[class~=col3] .general-td {
		    padding-right: 10px !important;
		  }

		  table[class~=col4] .general-img-td {
		    padding-right: 10px !important;
		  }

		  table[class~=col4] .general-td {
		    padding-right: 10px !important;
		  }

		  td[class~=general-img-td][class~=p-l-r-20] {

		    padding-left: 20px !important;
		    padding-right: 20px !important;
		  }

		  td[class~=full] {
		    width: 100% !important;
		    float: left !important;
		  }

		  table[class~=full] {
		    width: 100% !important;
		    float: left !important;
		  }

		  td[class~=full] .center {
		    text-align: center !important;
		    padding: 10px !important;
		  }

		  td[class~=center] {
		    text-align: center !important;
		    padding: 10px !important;

		  }

		  table[class~=banner] td {
		    text-align: center !important;
		  }

		  td[class~=hide480] {
		    display: none !important;
		  }

		  span[class~=hide480] {
		    display: none !important;
		  }

		  img[class~=hide-pc] {
		    display: inline !important;
		    width: 19px !important;
		    height: auto !important;
		    visibility: visible !important;

		  }

		  table[class~=menu2] .content {
		    padding-right: 0 !important;
		  }

		  table[class~=col2_5] {
		    width: 49% !important;
		  }
		}

		@media only screen and (max-width: 439px) {
		  td.responsive_row, td[class~=responsive_row] {
		    width: 100% !important;
		    float: left !important;
		    display: block !important;
		    padding: 0 !important;
		    margin: 0 !important;
		  }
		  table[class~=col2] {
		    width: 100% !important;
		  }

		  .noBlueLinks a {
		    color: #999999 !important;
		    text-decoration: none !important;
		  }

		  table[class~=col3_5] {
		    width: 100% !important;
		  }

		  table[class~=col3fix] {
		    width: 100% !important;
		  }

		  td [class~=footer] {
		    text-align: center !important;
		  }

		  td [class~=footer] div {
		    text-align: center !important;
		  }

		  td[class=coffeTable] {
		    text-align: center !important;
		  }

		  table[class~=zebra] > tbody > tr {
		    height: auto !important;
		  }

		  table[class~=zebra] td {
		    padding: 0 10px !important;
		  }

		  td[class~=module-td] {
		    padding: 20px !important;
		  }

		  table[class~=row] {
		    width: 100% !important;
		  }

		  table[class~=logo] img {
		    max-width: 100% !important;
		  }

		  table[class~=menu-tb] {
		    width: 100% !important;
		  }

		  table[class~=menu] .content {
		    padding: 4px 5px !important;
		  }

		  table[class~=top-bar] {
		    margin-bottom: 0 !important;
		  }

		  td[class~=col-1-3] {
		    width: 100% !important;
		    padding-bottom: 0 !important;
		    float: left !important;
		    display: block !important;

		  }

		  td[class~=p-d0] {
		    padding-bottom: 0 !important;
		  }

		  td[class~=col-2-3] {
		    width: 100% !important;
		  }

		  table[class~=col3] {
		    width: 100% !important;
		  }

		  td[class~=p-l-r-20] {

		    padding-left: 10px !important;
		    padding-right: 10px !important;
		  }

		  table[class~=col2_5] {
		    width: 49% !important;

		  }
		}


		@media only screen and (max-width: 339px) {
		  td.responsive_row, td[class~=responsive_row] {
		    width: 100% !important;
		    float: left !important;
		    display: block !important;
		    padding: 0 !important;
		    margin: 0 !important;
		  }
		  .noBlueLinks a {
		    color: #999999 !important;
		    text-decoration: none !important;
		  }

		  img[class~=capsule] {
		    display: none !important;
		  }

		  td[class~=module-td] {
		    padding: 20px !important;
		  }

		  table[class~=menu] .h4 {
		    font-size: 14px !important;
		    line-height: 21px !important;

		  }

		  td[class~=top-bar] {
		    padding: 5px 10px !important;
		  }

		  table[class~=menu-tb] {
		    width: 100% !important;
		  }

		  table[class~=logo] img {
		    max-width: 260px !important;
		  }
		}
	</style>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" class="BGtable" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #000000;height: 100%;width: 100%;">
  <tr>
    <td valign="top" class="BGtable-inner">
      <!-- Row -->
      <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
        <tr>
          <td>
            <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
              <tr>
                <td class="module-td noT_D" style="background-color: #000000; color: #ffffff; ">
                  <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
                    <caption style="display:none!important;mso-hide: all;max-height: 0;font-size: 0;line-height: 0;"> </caption>
                    <tr>
                      <!-- wrap -->
                      <td valign="top" class="responsive_row" style="width: 100%;">
                        <!-- Block -->
                        <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                          <tr>
                            <td style="undefined">
                              <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                <tbody>
                                  <tr>
                                    <td valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                      <table role="presentation" width="50%" class="col2" border="0" cellpadding="0" align="left" cellspacing="0" style="border-collapse: collapse; width: 50%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border: none;">
                                        <tbody>
                                          <tr>
                                            <td class="footer" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 20px 9px 10px 10px;">
                                              <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="left" class="logo">
                                                <tbody>
                                                  <tr>
                                                    <td width="140" height="30" align="left" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                                      <div class="imgpop" align="center" style="margin: 0; padding: 0;">
                                                        <a target="_blank" href="https://www.nespresso.vn" style="color: #666666; text-decoration: none; outline: none;"><span style=""><img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/nespressoLogo2.png" alt="" border="0" style="display: block; outline: none; text-decoration: none; border: none;"></span></a>
                                                      </div>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <table role="presentation" width="49%" class="col2" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 49%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border: none;">
                                        <tbody>
                                          <tr>
                                            <td class="footer" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 20px 9px 10px 10px;">
                                              <div class="t1" style="font-size: 13px; font-weight: bold; color: #ffffff; text-align: right; line-height: 24px; text-transform: uppercase; margin: 0; padding: 0;" align="right">
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" class="button" style="border-collapse: collapse; display: inline-block; border-radius: 4px; background-clip: padding-box; background: #308C11;" bgcolor="#308C11">
                                                  <tbody>
                                                    <tr>
                                                      <td style="font-size: 12px; line-height: 25px; font-weight: 400; letter-spacing: -0.2px; -webkit-font-smoothing: antialiased; display: block; font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 3px 18px;">
                                                        <a href="<?= $button_url?> " title="<?= $label ?>" style="text-decoration: none !important; color: #666666; outline: none;"><span style="color: #ffffff; text-align: center;"><?= $label ?></span></a>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </div>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
			</table>

    	<!-- Row -->
    	<table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
      	<tr>
        	<td>
          	<table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;width: 600px;margin: 0 auto; ">
           		<tr>
             		<td class="module-td noT_D" style="background-color: #000000; color: #ffffff; ">
               		<table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
                 		<caption style="display:none!important;mso-hide: all;max-height: 0;font-size: 0;line-height: 0;"> </caption>
                 		<tr>
            					<!-- wrap -->
            					<td valign="top" class="responsive_row" style="width: 100%;">
      									<!-- Block -->
      									<table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
									        <tr>
									          <td style="undefined"><img src="<?= $image_url ?>" alt="Nespresso" title="Nespresso" hspace="0" vspace="0" border="0" style="border: 0; display: block; max-width: 100%;  border-radius: 0;"/></td>
									        </tr>
									      </table>
					            </td>
                 		</tr>
               		</table>
             		</td>
            	</tr>
          	</table>
        	</td>
      	</tr>
    	</table>
