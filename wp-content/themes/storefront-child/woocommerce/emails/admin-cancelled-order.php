<?php
/**
 * Admin cancelled order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/admin-cancelled-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see       https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.5.0
 */

 if ( ! defined( 'ABSPATH' ) ) {
  exit;
 }

 /**
  * @hooked WC_Emails::email_header() Output the email header
  */
 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

      <!-- Row -->
      <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse:collapse; width:100%; margin:0 auto;">
        <tr>
          <td>
            <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse:collapse;width: 600px;margin:0 auto;">
              <tr>
                <td class="module-td noT_D" style="background-color:#f3f5f7; color:#000000; padding-left:40px; padding-right:40px;">
                  <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; width:100%">
                    <caption style="display:none!important; mso-hide:all; max-height:0; font-size:0; line-height:0;">Information about the shopping order</caption>
                    <tr>
                      <!-- wrap -->
                      <td valign="top" class="responsive_row" style="width: 100%;">
                        <!-- Block -->
                        <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                          <tr>
                            <td style="undefined">
                              <table role="presentation" class="spacer bar" style="border-collapse:collapse; border-spacing:0; content:''; display:block; height:40px; padding:0; text-align:left;vertical-align:top; width:100%">
                                <tbody>
                                  <tr style="padding:0; text-align:left; vertical-align:middle">
                                    <td height="40" valign="middle" style="-moz-hyphens:auto; -webkit-hyphens:auto; margin:0; border-collapse:collapse!important; color:#0a0a0a; font-family:Trebuchet MS,Helvetica,arial,sans-serif; font-weight:400; hyphens:auto; mso-line-height-rule:exactly; padding:0; text-align:center; vertical-align:middle;word-wrap:break-word">
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <p class="classes" aria-level="1" role="heading" style="font-size:22px; font-weight:bold; text-align:center; line-height:32px; text-transform:uppercase; color:#2c2c2c;">CANCELLED ORDER NOTIFICATION</p>
                              <table role="presentation" class="spacer bar" style="border-collapse:collapse; border-spacing:0; content:''; display:block; height:43px; padding:0; text-align:left;vertical-align:top; width:100%">
                                <tbody>
                                  <tr style="padding:0;text-align:left;vertical-align:middle">
                                    <td height="43" valign="middle" style="-moz-hyphens:auto; -webkit-hyphens:auto; margin:0; border-collapse:collapse!important; color:#0a0a0a; font-family:Trebuchet MS,Helvetica,arial,sans-serif; font-weight:400; hyphens:auto; mso-line-height-rule:exactly; padding:0; text-align:center; vertical-align:middle; word-wrap:break-word"><img src="https://www.nespresso.com/shared_res/newsletter/transactional/img/hr_light_560_triangle.png" role="presentation" alt="" hspace="0" vspace="0" border="0" width="560" style="border: 0; display: block; max-width: 100%; width: 100%; border-radius: 0;">
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <p class="classes" style="margin-bottom:10px; font-size:16px; color:#2c2c2c; text-align:left; color:#2c2c2c;">Dear Admin,</p>
                              <p>&nbsp;</p>
                              <p class="classes" style="margin-bottom:10px; font-size:14px; color:#2c2c2c; text-align:left; color:#2c2c2c;"><?php printf( __( 'The order #%1$d from %2$s has been cancelled. The order was as follows:', 'woocommerce' ), $order->get_order_number(), $order->get_formatted_billing_full_name() ); ?></p>
                              <table role="presentation" class="spacer bar" style="border-collapse:collapse; border-spacing:0; content:''; display:block; padding:0; text-align:left; vertical-align:top; width:100%">
                                <tbody>
                                  <tr style="padding:0;text-align:left;vertical-align:middle">
                                    <td height="23" valign="middle" style="-moz-hyphens:auto; -webkit-hyphens:auto; margin:0; border-collapse:collapse!important; color:#0a0a0a; font-family:Trebuchet MS,Helvetica,arial,sans-serif;font-weight:400;hyphens:auto; mso-line-height-rule:exactly; padding:0; text-align:center;vertical-align:middle;word-wrap:break-word"></td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <?php
                                        /**
                                        * @hooked WC_Emails::order_details() Shows the order details table.
                                        * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
                                        * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
                                        * @since 2.5.0
                                        */
                                        do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

                                        /**
                                        * @hooked WC_Emails::order_meta() Shows order meta data.
                                        */
                                        do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
                                      ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <p>&nbsp;</p>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <!-- Row -->
      <table role="presentation" width="900" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap dark" style="border-collapse:collapse; width:900px; margin:0 auto;">
        <tr>
          <td>
            <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse; width:100%; margin:0 auto; ">
              <tr>
                <td class="module-td noT_D" style="background-color:#1a1a1a; color:#ffffff; padding-left:40px; padding-right:40px;">
                  <?php
                    /**
                     * @hooked WC_Emails::customer_details() Shows customer details
                     * @hooked WC_Emails::email_address() Shows email address
                     */
                    do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
                  ?>
                  <p>&nbsp;</p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <!-- Row -->
      <table role="presentation" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="wrap light" style="border-collapse:collapse; width:100%; margin:0 auto;">
        <tr>
          <td>
            <table role="presentation" width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse:collapse; width:600px; margin:0 auto; ">
             <tr>
               <td class="module-td noT_D" style="background-color: #000000; color: #ffffff; ">
                 <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; width:100%">
                   <caption style="display:none!important; mso-hide:all; max-height:0; font-size:0; line-height:0;">Legal Information and Social Media</caption>
                   <tr>
                      <!-- wrap -->
                      <td valign="top" class="responsive_row" style="width: 100%;">
                        <!-- Block -->
                        <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                          <tr>
                            <td style="undefined">
                              <table role="presentation" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
                                <tbody>
                                  <tr>
                                    <td valign="top" class="col2" style="width: 50%; font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                                        <tbody>
                                          <tr>
                                            <td class="twoColumns" style="text-align: left; font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 30px 9px 10px 10px;" align="left">
                                              <table role="presentation" width="112" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                  <tr>
                                                    <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                                      <a href="http://www.facebook.com/nespresso" title="Facebook Nespresso" style="color:#666666; text-decoration:none; outline:none;">
                                                        <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/fb.png" width="28" height="28" alt="Facebook Nespresso" border="0" class="img3" style="display:block;">
                                                      </a>
                                                    </td>
                                                    <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                                      <a href="https://twitter.com/nespresso" title="Twitter Nespresso" style="color:#666666; text-decoration:none; outline:none;">
                                                        <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/tw.png" width="28" height="28" alt="Twitter" border="0" class="img3" style="display:block;">
                                                      </a>
                                                    </td>
                                                    <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                                      <a href="https://www.pinterest.com/nespresso/" title="Pinterest Nespresso" style="color:#666666; text-decoration:none; outline:none;">
                                                        <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/pn.png" width="28" height="28" alt="Pinterest" border="0" class="img3" style="display:block;">
                                                      </a>
                                                    </td>
                                                    <td align="left" valign="top" style="font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                                      <a href="https://plus.google.com/+nespresso/" title="Google+" style="color:#666666; text-decoration:none; outline:none;">
                                                        <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/gplus.png" width="28" height="28" alt="Google Plus" border="0" class="img3" style="display:block;">
                                                      </a>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                    <td valign="top" class="col2" style="width: 50%; font-family: Trebuchet MS, Helvetica, arial, sans-serif;">
                                      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                                        <tbody>
                                          <tr>
                                            <td class="twoColumns" style="text-align: right; font-family: Trebuchet MS, Helvetica, arial, sans-serif; padding: 30px 9px 10px 10px;" align="right">
                                              <a href="https://www.nespresso.vn" style="color: #666666; text-decoration: none; outline: none;">
                                                <span style="">
                                                  <img src="http://www.nespresso.com/shared_res/newsletter/transactional/img/nLogo2.png" alt="Nespresso" width="35" height="34" hspace="0" vspace="0" border="0" class="style=&quot;border:" max-width:="" width:="" border-radius:="">
                                                </span>
                                              </a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>

                              <table role="contentinfo" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
                                <tbody>
                                  <tr>
                                    <td class="1c" style="font-family:Trebuchet MS, Helvetica, arial, sans-serif; padding:0 9px 0 10px;">
                                      <div class="appleLinkGrey" style="font-size: 11px; color: #a3a6a8; text-align: justify; margin: 0; padding: 0;" align="justify">Please do not respond to this email. You may contact our Coffee Specialist at 1900 633 474 from 10am to 6pm, Mondays to Saturdays.</div>
                                      <div class="appleLinkGrey" style="font-size: 11px; color: #a3a6a8; text-align: justify; margin: 0; padding: 0;" align="justify"></div>
                                      <table role="presentation" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                        <tbody>
                                          <tr>
                                            <td valign="top" width="100%" height="10" style="font-size: 1px; line-height: 10px; font-family: Trebuchet MS, Helvetica, arial, sans-serif;" dir="ltr">&nbsp;</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <div class="appleLinkGrey" style="font-size: 11px; color: #a3a6a8; text-align: justify; margin: 0; padding: 0;" align="justify">For more information, visit our website at <a href="https://www.nespresso.vn" style="color: #a3a6a8; text-decoration: underline; outline: none;"><span style="color: #a3a6a8; text-decoration: underline;">Nespresso.vn</span></a></div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table role="presentation" class="spacer bar" style="border-collapse:collapse;border-spacing:0;content:'';display:block;height:23px;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                  <tr style="padding:0;text-align:left;vertical-align:middle">
                                    <td height="23" valign="middle" style="-moz-hyphens:auto; -webkit-hyphens:auto; margin:0; border-collapse:collapse!important; color:#0a0a0a; font-family:Trebuchet MS,Helvetica,arial,sans-serif; font-weight:400; hyphens:auto; mso-line-height-rule:exactly; padding:0; text-align:center; vertical-align:middle; word-wrap:break-word"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
