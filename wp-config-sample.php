<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'database_name_here');

/** MySQL database username */
define('DB_USER', 'username_here');

/** MySQL database password */
define('DB_PASSWORD', 'password_here');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V-qm,Abnz:]zK<aT<(I}Q,rAN)/nKi;5G168l&.CY7GL|_n|@nZeL>sRc,El>m^`');
define('SECURE_AUTH_KEY',  'Erf`7D8|;Yt6!?CA<PdZ*~)JLV<Rk7l*e$:4-~1xf_n@7gmb~&_z;V|2yw_#f}*$');
define('LOGGED_IN_KEY',    '#}g*Ewi4z~trEk1:TBZm-<3)hOi6H+-=yP[uA]zrbjw~H$u2rH,dAU(|er+N~ZhP');
define('NONCE_KEY',        '-#H2-~Xh-;Pydd=}r@;}0-k6YN[o=_C@jz2B3w/Y#<;I?-.(sRP-8Fq+K^r0v$bL');
define('AUTH_SALT',        'L9zpb{4Nc~6B`yuf`$eT@5;6SH^CcylGZD*W+l0Vh=O%}j/CpKd>W[/!^)*zH`rC');
define('SECURE_AUTH_SALT', '}jL!7kEbA2!H*sCS+*pvkyPO|;t.F0Cck[E<7dvki[}iS3|X*,a%,g|yBmtKk|h=');
define('LOGGED_IN_SALT',   'j)V*p9gS0`o4@$iH1I_5skn?aw:#~WmeU&]u>^Jk=5+7(H+tOdaBJ}Nenm73uW-d');
define('NONCE_SALT',       'VD3|7O=>+=M(;4I_-R-o^L_h87U]J.nOQ2Ah&LsuGq<+Vsx^J*g%L31O<a(B.s/1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
