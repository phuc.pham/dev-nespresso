
<!-- /content -->

<!-- get_footer -->


		
       	<div id="footer" class="footer container">

		    <!-- <h3 class="hidden-xs"><i class="fa fa-angle-down"></i> Nespresso Club Services</h3> -->
		    <ul class="footer__list-table hidden-xs">
		        <li>
		            <!-- <i class="icon icon-phone"></i> -->
		            <h3 style="font-size: 14;">CALL US</h3>
		            <p>Our lines are open from</p>
		            <p>Monday - Saturday, 10am to 6pm.</p>
		            <p>1900 633 474</p>
		            <br><br>
		            <h3 style="font-size: 14;">EMAIL US</h3>
		            <p>Share your Nespresso Experience <a href="/contact-us">here.</a></p>
		            <br><br>
		            <h3 style="font-size: 14;">MAIL US</h3>
		            <p>Nespresso Vietnam</p>
		            <p>ÂN NAM FINE FOOD CO., LTD.</p>
		            <p>322 Dien Bien Phu street</p>
		            <p>Ward 22, Binh Thanh District,</p>
		            <p>Ho Chi Minh City, Vietnam</p>
		        </li>
		        <!-- <li>
		            <h3 style="font-size: 14;">Nespresso Boutique</h3>
					<p>Visit us at Binh Thanh District,</p>
		            <p>Ho Chi Minh City, Vietnam</p>
					<br><br>
					<h3 style="font-size: 14;">MACHINE RETAILERS</h3>
					<p>Shop for Nespresso Machines near you at</p>
					<p>our partner retailers.</p>
					<br><br> -->
					<!-- <p><a href="/store-locator">Store Locator</a></p> -->
		        <!-- </li> -->
		        <li>
		            <!-- <i class="icon icon-bag"></i> -->
		            <h3 style="font-size: 14;">SECURE PAYMENT</h3>
		            <p>Secure Credit Card payment option for VISA, MASTERCARD, CUP and AMEX</p>
		            <br>
		            <p>Cash on Delivery.</p>
		            <br><br>
					<h3 style="font-size: 14;">FREE DELIVERY FOR MINIMUM &#8363;1,500,000</h3>
					<!-- <br>
					<h3 style="font-size: 14;">BE A NESPRESSO CLUB MEMBER</h3>
					<p>Experience a personalized shopping experience and get invited to exclusive events.</p>
					<br><br>
					<p><a href="/register">Register Now</a></p> -->
		        </li>
		    </ul>

		    <div class="footer__bar clearfix">
		    	<a href="#" title="Vietnam" class="lang"><span id="lang_flag_1" class="lang-vn"></span></a>
		    	<div class="dropdown dropdown--input dropdown--language">
				    <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current" id="lang_btn_flag">English</span></button>
				    <select tabindex="-1" name="language" id="language">
					    <option value="en">English</option>
					    <option value="vn">Ti&#7871;ng Vi&#7879;t</option>
				    </select>
			    </div>
		        <ul class="footer__bar__links">
		            <li><a href="http://www.nestle-nespresso.com/" title="About" target="_blank">About</a></li>
		            <li><a href="/legal" title="Legal">Legal</a></li>
		            <li><a href="/conditions-of-sale/" title="Conditions of Sale">Conditions of Sale</a></li>
		            <li><a href="/contact-us/" title="Contact Us">Contact Us</a></li>
		            <li><a href="/pro" title="Professional">Professional</a></li>
		            <li><a href="/faq" title="FAQ">FAQ</a></li>
		            <li><a href='/vi/legal?q=privacy-policy' title='CHÍNH SÁCH BẢO MẬT'>CHÍNH SÁCH BẢO MẬT</a></li>		        </ul>
		    </div>

		    <div class="footer__share clearfix">
		        <ul class="pull-left">
		            <li><a href="https://www.facebook.com/nespresso"  target="_blank" title="Facebook"><i class="icon icon-facebook"></i></a></li>
		            <li><a href="https://twitter.com/nespresso"  target="_blank" title="Twitter"><i class="icon icon-twitter"></i></a></li>
		            <li><a href="https://www.pinterest.com/nespresso/"  target="_blank" title="Pinterest"><i class="icon icon-pinterest"></i></a></li>
		            <li><a href="https://www.instagram.com/nespresso/?hl=en"  target="_blank" title="Instagram"><i class="icon icon-instagram"></i></a></li>
		            <li><a href=" https://www.youtube.com/user/nespresso"  target="_blank" title="Youtube"><i class="icon icon-youtube"></i></a></li>
		        </ul>
		        <p style="float: left; padding-left:40px;text-align: center;line-height:30px;"><a href="#" title="Top of the page" style="color:#fff;">Top of the page <i class="icon icon-top_page"></i></a>
				<br><a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=55244" target="_blank"><img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/nespresso-inform.png"></a></p
		        <p style="float: right;text-align: left;">
			    	Nespresso is exclusively distributed in Vietnam<br>
	 				by ÂN NAM FINE FOOD CO., LTD.<br>
					41 Thao Dien, Thao Dien Ward, District 2, Ho Chi Minh City, Vietnam<br>
	 				Business Registration Certificate N° 0302314179 dated 24/05/2001<br>
				    by the department of planning and investment of HCMC
	 			</p>
		    </div>

		    <!-- div class="footer__share clearfix" style="padding-top: 0px; margin-top: -10px;">
		        <p style="float: right;text-align: left;">

				</p>

		    </div> -->
		</div><!-- #footer -->

	</div><!-- end of main-wrapper -->

<!-- <script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/libs/jquery-1.8.1.min.js"></script> -->
<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/libs/jquery-2.1.4.min.js"></script>

<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/libs/jquery-ui.min.js"></script>
