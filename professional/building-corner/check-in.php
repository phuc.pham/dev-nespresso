<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Nespresso Vietnam brings luxury coffee and espresso machine straight from the café and into your kitchen.">
	<meta name="author" content="Koodi">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<meta name="google-site-verification" content="Ihzx1tBV53gOkNIrs2JJCElahIqsxA6tiG6pMorjWdA" />
	<title>Nespresso Vietnam | Ispirazione Italiana</title>
	<link rel="icon" type="image/x-icon" href="https://www.nespresso.vn/wp-content/themes/storefront-child/images/favicon.ico">

	<!-- wp_head -->
	<title>Nespresso Vietnam &#8211; Nespresso Vietnam brings luxury coffee and espresso machine straight from the café and into your kitchen.</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	<link rel='dns-prefetch' href='//s.w.org' />
	<link rel="alternate" type="application/rss+xml" title="Nespresso Vietnam &raquo; Feed" href="https://www.nespresso.vn/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Nespresso Vietnam &raquo; Comments Feed" href="https://www.nespresso.vn/comments/feed/" />
	<link rel='stylesheet' id='axapta-api-css' href='https://www.nespresso.vn/wp-content/plugins/axapta-api/public/css/axapta-api-public.css?ver=1.0.0' type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css' href='https://www.nespresso.vn/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3' type='text/css' media='all' />
	<link rel='stylesheet' id='nespresso-css' href='https://www.nespresso.vn/wp-content/plugins/nespresso/public/css/nespresso-public.css?ver=1.0.0' type='text/css' media='all' />
	<link rel='stylesheet' id='smart-search-css' href='https://www.nespresso.vn/wp-content/plugins/smart-woocommerce-search/assets/css/general.css?ver=ysm-1.5.5' type='text/css' media='all' />
	<style id='smart-search-inline-css' type='text/css'>
		.widget_search.ysm-active .smart-search-suggestions .smart-search-post-icon {
			width: 50px;
		}

		.widget_product_search.ysm-active .smart-search-suggestions .smart-search-post-icon {
			width: 50px;
		}
	</style>
	<style id='woocommerce-inline-inline-css' type='text/css'>
		.woocommerce form .form-row .required {
			visibility: visible;
		}
	</style>
	<link rel='stylesheet' id='fpf_front-css' href='https://www.nespresso.vn/wp-content/plugins/flexible-product-fields/assets/css/front.min.css?ver=39' type='text/css' media='all' />
	<link rel='stylesheet' id='trp-language-switcher-style-css' href='https://www.nespresso.vn/wp-content/plugins/translatepress-multilingual/assets/css/trp-language-switcher.css?ver=1.3.8' type='text/css' media='all' />
	<link rel='stylesheet' id='storefront-style-css' href='https://www.nespresso.vn/wp-content/themes/storefront/style.css?ver=2.1.8' type='text/css' media='all' />
	<style id='storefront-style-inline-css' type='text/css'>
		@media (min-width: 768px) {

			.woocommerce-checkout.yith-wcms #order_review,
			.woocommerce-checkout.yith-wcms #order_review_heading {
				width: 100%;
				float: none;
			}
		}

		.main-navigation ul li a,
		.site-title a,
		ul.menu li a,
		.site-branding h1 a,
		.site-footer .storefront-handheld-footer-bar a:not(.button),
		button.menu-toggle,
		button.menu-toggle:hover {
			color: #d5d9db;
		}

		button.menu-toggle,
		button.menu-toggle:hover {
			border-color: #d5d9db;
		}

		.main-navigation ul li a:hover,
		.main-navigation ul li:hover>a,
		.site-title a:hover,
		a.cart-contents:hover,
		.site-header-cart .widget_shopping_cart a:hover,
		.site-header-cart:hover>li>a,
		.site-header ul.menu li.current-menu-item>a {
			color: #ffffff;
		}

		table th {
			background-color: #f8f8f8;
		}

		table tbody td {
			background-color: #fdfdfd;
		}

		table tbody tr:nth-child(2n) td {
			background-color: #fbfbfb;
		}

		.site-header,
		.secondary-navigation ul ul,
		.main-navigation ul.menu>li.menu-item-has-children:after,
		.secondary-navigation ul.menu ul,
		.storefront-handheld-footer-bar,
		.storefront-handheld-footer-bar ul li>a,
		.storefront-handheld-footer-bar ul li.search .site-search,
		button.menu-toggle,
		button.menu-toggle:hover {
			background-color: #2c2d33;
		}

		p.site-description,
		.site-header,
		.storefront-handheld-footer-bar {
			color: #9aa0a7;
		}

		.storefront-handheld-footer-bar ul li.cart .count,
		button.menu-toggle:after,
		button.menu-toggle:before,
		button.menu-toggle span:before {
			background-color: #d5d9db;
		}

		.storefront-handheld-footer-bar ul li.cart .count {
			color: #2c2d33;
		}

		.storefront-handheld-footer-bar ul li.cart .count {
			border-color: #2c2d33;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6 {
			color: #484c51;
		}

		.widget h1 {
			border-bottom-color: #484c51;
		}

		body,
		.secondary-navigation a,
		.onsale,
		.pagination .page-numbers li .page-numbers:not(.current),
		.woocommerce-pagination .page-numbers li .page-numbers:not(.current) {
			color: #43454b;
		}

		.widget-area .widget a,
		.hentry .entry-header .posted-on a,
		.hentry .entry-header .byline a {
			color: #75777d;
		}

		a {
			color: #96588a;
		}

		a:focus,
		.button:focus,
		.button.alt:focus,
		.button.added_to_cart:focus,
		.button.wc-forward:focus,
		button:focus,
		input[type="button"]:focus,
		input[type="reset"]:focus,
		input[type="submit"]:focus {
			outline-color: #96588a;
		}

		button,
		input[type="button"],
		input[type="reset"],
		input[type="submit"],
		.button,
		.added_to_cart,
		.widget a.button,
		.site-header-cart .widget_shopping_cart a.button {
			background-color: #96588a;
			border-color: #96588a;
			color: #ffffff;
		}

		button:hover,
		input[type="button"]:hover,
		input[type="reset"]:hover,
		input[type="submit"]:hover,
		.button:hover,
		.added_to_cart:hover,
		.widget a.button:hover,
		.site-header-cart .widget_shopping_cart a.button:hover {
			background-color: #7d3f71;
			border-color: #7d3f71;
			color: #ffffff;
		}

		button.alt,
		input[type="button"].alt,
		input[type="reset"].alt,
		input[type="submit"].alt,
		.button.alt,
		.added_to_cart.alt,
		.widget-area .widget a.button.alt,
		.added_to_cart,
		.pagination .page-numbers li .page-numbers.current,
		.woocommerce-pagination .page-numbers li .page-numbers.current,
		.widget a.button.checkout {
			background-color: #2c2d33;
			border-color: #2c2d33;
			color: #ffffff;
		}

		button.alt:hover,
		input[type="button"].alt:hover,
		input[type="reset"].alt:hover,
		input[type="submit"].alt:hover,
		.button.alt:hover,
		.added_to_cart.alt:hover,
		.widget-area .widget a.button.alt:hover,
		.added_to_cart:hover,
		.widget a.button.checkout:hover {
			background-color: #13141a;
			border-color: #13141a;
			color: #ffffff;
		}

		#comments .comment-list .comment-content .comment-text {
			background-color: #f8f8f8;
		}

		.site-footer {
			background-color: #f0f0f0;
			color: #61656b;
		}

		.site-footer a:not(.button) {
			color: #2c2d33;
		}

		.site-footer h1,
		.site-footer h2,
		.site-footer h3,
		.site-footer h4,
		.site-footer h5,
		.site-footer h6 {
			color: #494c50;
		}

		#order_review,
		#payment .payment_methods>li .payment_box {
			background-color: #ffffff;
		}

		#payment .payment_methods>li {
			background-color: #fafafa;
		}

		#payment .payment_methods>li:hover {
			background-color: #f5f5f5;
		}

		@media screen and (min-width: 768px) {
			.secondary-navigation ul.menu a:hover {
				color: #b3b9c0;
			}

			.secondary-navigation ul.menu a {
				color: #9aa0a7;
			}

			.site-header-cart .widget_shopping_cart,
			.main-navigation ul.menu ul.sub-menu,
			.main-navigation ul.nav-menu ul.children {
				background-color: #24252b;
			}
		}
	</style>
	<link rel='stylesheet' id='storefront-fonts-css' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,700,900&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
	<link rel='stylesheet' id='woo-viet-provinces-style-css' href='https://www.nespresso.vn/wp-content/plugins/woo-viet/assets/provinces.css?ver=4.9.8' type='text/css' media='all' />
	<link rel='stylesheet' id='tawcvs-frontend-css' href='https://www.nespresso.vn/wp-content/plugins/variation-swatches-for-woocommerce/assets/css/frontend.css?ver=20160615' type='text/css' media='all' />
	<link rel='stylesheet' id='storefront-woocommerce-style-css' href='https://www.nespresso.vn/wp-content/themes/storefront/assets/sass/woocommerce/woocommerce.css?ver=4.9.8' type='text/css' media='all' />
	<style id='storefront-woocommerce-style-inline-css' type='text/css'>
		a.cart-contents,
		.site-header-cart .widget_shopping_cart a {
			color: #d5d9db;
		}

		table.cart td.product-remove,
		table.cart td.actions {
			border-top-color: #ffffff;
		}

		.woocommerce-tabs ul.tabs li.active a,
		ul.products li.product .price,
		.onsale,
		.widget_search form:before,
		.widget_product_search form:before {
			color: #43454b;
		}

		.woocommerce-breadcrumb a,
		a.woocommerce-review-link,
		.product_meta a {
			color: #75777d;
		}

		.onsale {
			border-color: #43454b;
		}

		.star-rating span:before,
		.quantity .plus,
		.quantity .minus,
		p.stars a:hover:after,
		p.stars a:after,
		.star-rating span:before,
		#payment .payment_methods li input[type=radio]:first-child:checked+label:before {
			color: #96588a;
		}

		.widget_price_filter .ui-slider .ui-slider-range,
		.widget_price_filter .ui-slider .ui-slider-handle {
			background-color: #96588a;
		}

		.woocommerce-breadcrumb,
		#reviews .commentlist li .comment_container {
			background-color: #f8f8f8;
		}

		.order_details {
			background-color: #f8f8f8;
		}

		.order_details>li {
			border-bottom: 1px dotted #e3e3e3;
		}

		.order_details:before,
		.order_details:after {
			background: -webkit-linear-gradient(transparent 0, transparent 0), -webkit-linear-gradient(135deg, #f8f8f8 33.33%, transparent 33.33%), -webkit-linear-gradient(45deg, #f8f8f8 33.33%, transparent 33.33%)
		}

		p.stars a:before,
		p.stars a:hover~a:before,
		p.stars.selected a.active~a:before {
			color: #43454b;
		}

		p.stars.selected a.active:before,
		p.stars:hover a:before,
		p.stars.selected a:not(.active):before,
		p.stars.selected a.active:before {
			color: #96588a;
		}

		.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {
			background-color: #96588a;
			color: #ffffff;
		}

		.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {
			background-color: #7d3f71;
			border-color: #7d3f71;
			color: #ffffff;
		}

		@media screen and (min-width: 768px) {

			.site-header-cart .widget_shopping_cart,
			.site-header .product_list_widget li .quantity {
				color: #9aa0a7;
			}
		}
	</style>
	<link rel='stylesheet' id='storefront-child-style-css' href='https://www.nespresso.vn/wp-content/themes/storefront-child/style.css?ver=4.9.8' type='text/css' media='all' />
	<script type='text/javascript' src='https://www.nespresso.vn/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/axapta-api/public/js/axapta-api-public.js?ver=1.0.0'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/html5-responsive-faq/js/hrf-script.js?ver=4.9.8'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/nespresso/public/js/nespresso-public.js?ver=1.0.0'></script>
	<link rel='https://api.w.org/' href='https://www.nespresso.vn/wp-json/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.nespresso.vn/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.nespresso.vn/wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 4.9.8" />
	<meta name="generator" content="WooCommerce 3.5.2" />
	<link rel="canonical" href="https://www.nespresso.vn/" />
	<link rel='shortlink' href='https://www.nespresso.vn/' />
	<link rel="alternate" type="application/json+oembed" href="https://www.nespresso.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nespresso.vn%2F" />
	<link rel="alternate" type="text/xml+oembed" href="https://www.nespresso.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nespresso.vn%2F&#038;format=xml" />
	<!-- start Simple Custom CSS and JS -->
	<style type="text/css">
		.select2-results__options li {
			color: #333 !important;
		}

		.select2-results__options li.select2-results__option--highlighted {
			color: #fff !important;
		}
	</style>
	<!-- end Simple Custom CSS and JS -->

	<!-- This website runs the Product Feed PRO for WooCommerce by AdTribes.io plugin -->
	<!--noptimize-->
	<!-- Global site tag (gtag.js) - Google Ads: 770954263 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-770954263"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'AW-770954263');
	</script>
	<!--/noptimize-->

	<link rel="alternate" hreflang="en-US" href="https://www.nespresso.vn/" />
	<link rel="alternate" hreflang="vi" href="https://www.nespresso.vn/vi/" />
	<script type="text/javascript">
		var ajaxurl = "https://www.nespresso.vn/wp-admin/admin-ajax.php";
		var login_url = "https://www.nespresso.vn/my-account/";
	</script> <noscript>
		<style>
			.woocommerce-product-gallery {
				opacity: 1 !important;
			}
		</style>
	</noscript>

	<!-- Facebook Pixel Code -->
	<script type='text/javascript'>
		! function(f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window,
			document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
	</script>
	<!-- End Facebook Pixel Code -->
	<script type='text/javascript'>
		fbq('init', '2387221091407358', [], {
			"agent": "wordpress-4.9.8-1.7.25"
		});
	</script>
	<script type='text/javascript'>
		fbq('track', 'PageView', []);
	</script>
	<!-- Facebook Pixel Code -->
	<noscript>
		<img height="1" width="1" style="display:none" alt="fbpx" src="https://www.facebook.com/tr?id=2387221091407358&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->
	<script type="text/javascript">
		var cli_flush_cache = 2;
	</script>
	<style type="text/css" id="custom-background-css">
		body.custom-background {
			background-color: ##ffffff;
		}
	</style>
	<link rel="alternate" type="application/rss+xml" title="RSS" href="https://www.nespresso.vn/rsslatest.xml" />
	<style type="text/css" id="wp-custom-css">
		/*
You can add your own CSS here.

Click the help icon above to learn more.
*/

		.site-control .btn {
			line-height: 40px !important;
		}

		.account-sidebar {
			padding: 20px 0 0;
			background-color: #f9f9f9;
			margin-right: 0 !important;
		}

		div.wpcf7-validation-errors {
			border: 2px solid #f7002e;
			color: #3D8705;
		}


		div.wpcf7-mail-sent-ok {
			border: 2px solid #398f14;
			color: red;
		}

		.contactus .contactus-main textarea {
			height: 200px !important;
		}

		.contactus .contactus-main {
			padding: 0 20px !important;
		}

		.contactus .contactus-main .col-md-6 .input-text,
		.contactus .contactus-main .col-md-6 input[type="text"],
		.contactus .contactus-main .col-md-6 input[type="email"],
		.contactus .contactus-main .col-md-6 input[type="url"],
		.contactus .contactus-main .col-md-6 input[type="password"],
		.contactus .contactus-main .col-md-6 input[type="search"],
		.contactus .contactus-main .col-md-6 textarea {
			width: 100%;
		}

		.input-group.input-group-generic input::-moz-placeholder {
			color: #666 !important;
		}

		.input-group.input-group-generic input::-webkit-placeholder {
			color: #666 !important;
		}

		/*
For contact-us desktop page
start
*/

		.page-id-41 .wpcf7-form-control {
			height: 38px !important;
		}

		.page-id-41 .input-group.input-group-generic .desktop-label {
			padding-top: 21px !important;
		}

		.page-id-41 .desktop-label {
			width: 185px !important;
		}

		.page-id-41 .dropdown {
			margin-bottom: 0px !important;
		}

		/*
For contact-us desktop page
end
*/
	</style>
	<!-- /wp_head -->

	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/vendors.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/libs/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/libs/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/css-helpers.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/style.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/custom.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/override.css">



	<script type="text/javascript">
		//Please execute the following dataLayer initialization on page load with the required site or page level parameters and attributes.
		// **NOTE** If any value is not obtainable or not relevant to the page, please leave the value blank/empty so nothing is passed into Google Analytics.
		var page_category = document.getElementById('product_category');
		gtmDataObject = [{
			'isEnvironmentProd': 'Yes',
			'pageName': 'Home | Nespresso',
			'pageType': 'Home', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
			'pageCategory': 'home', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
			'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
			'pageTechnology': 'OriginalLine', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
			'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
			'market': 'VN',
			'version': 'VN - WooCommerce V1',
			'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
			'segmentBusiness': 'B2C',
			'currency': 'VND',
			'clubMemberID': '', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
			'clubMemberStatus': 'false', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
			'clubMemberLevel': '', //market level name for user level that is a club member
			'clubMemberTierID': '', //Global HQ level ID for club membership level
			'clubMemberTitle': '',
			'clubMemberLoginStatus': 'false', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
			'machineOwner': '', //If user owns a machine or not, if known
			'machineOwned': '', //If multiple machines owned, separate with "|||" symbol
			'preferredTechnology': 'Espresso',
			'event': 'event_pageView',
			'persistentBasketLoaded': 'false', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
			'language': 'EN'
		}];
	</script>
	<script type="text/javascript">
		function removeFromCartGTM(cart_item_key) {
			var data = {
				action: 'gtm_get_basket',
				cart_item_key: cart_item_key
			};
			$.ajax({
				dataType: 'json',
				type: 'post',
				data: data,
				url: ajaxurl,
				dataType: 'json',
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
					// return false;
				},
				success: function(data) {
					if (data.status) {
						gtmDataObject.push({
							'event': 'removeFromCart',
							'landscape': 'Wordpress',
							'segmentBusiness': 'B2C',
							'currencyCode': 'VND',
							'ecommerce': {
								'remove': {
									'actionField': {

									},
									'products': data.products
								},
							},
						});
					}

				} // success()

			});
		}

		function addToCartGTM(item) {
			var data = {
				action: 'gtm_get_basket',
			};
			var metric10 = ''
			var metric11 = ''
			var metric12 = ''

			var dimension57 = '';

			switch (item.type) {
				case 'Coffee':
					dimension57 = 'Single Product - Capsule';
					metric10 = item.quantity;
					break;
				case 'Machine':
					dimension57 = 'Variable Product - Machine';
					metric11 = item.quantity;
					break;
				case 'Accessory':
					dimension57 = 'Single Product - Accessory';
					metric12 = item.quantity;
					break;

			}


			gtmDataObject.push({
				'event': 'addToCart',
				'landscape': 'Wordpress',
				'segmentBusiness': 'B2C',
				'currencyCode': 'VND',
				'ecommerce': {
					'add': {
						'actionField': {

						},
						'products': [{
							'name': item.name,
							'id': item.hqid,
							'dimension54': item.name,
							'dimension55': item.range,
							'dimension53': item.sku,
							'quantity': item.quantity,
							'price': item.price,
							'category': item.type.toLowerCase(),
							'dimension56': item.technology,
							'brand': 'Nespresso',
							'metric10': metric10,
							'metric11': metric11,
							'metric12': metric12,
							'dimension57': dimension57
						}]
					},
				},
			});
		}
	</script>
	<!--  Google Tag Manager  -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].unshift({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'gtmDataObject', 'GTM-KMPTP65');
	</script>
	<!-- End Google Tag Manager -->

	<script type="text/javascript">
		window.host_url = 'https://www.nespresso.vn';
		window.nespresso_token = '032da4cc0e26717ca42f91b2ab071669';
		window.currency_symbol = '&#8363;';
		window.currency = 'VND';
		window.user = null;
	</script>
	<link rel="stylesheet" href="owl/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="owl/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/mycss.css?a=<?= time() ?>">
</head>

<body class="home page-template-default page page-id-262 custom-background woocommerce-no-js translatepress-en_US yith-wcms show_checkout_login_reminder storefront-full-width-content right-sidebar woocommerce-active">
	<?php include 'header.php' ?>
	<main id="content">
		<div id="barista" class="checkin">
			<section id="form">
				<div class="wrapper">
					<h2>CHECK-IN TO GET A CHANCE TO WIN<br>OUR MACHINES!</h2>
					<div class="t-img1"><img src="img/i3.png" /></div>
					<div class="content-ci">
						<ul style="list-style:none;padding:20px 0">
							<li>1. Receive a lottery number after you register for the event</li>
							<li>2. Visit our experience corner and submit your number to get a prize from Nespresso</li>
							<li>3. Prizes can be one of the followings: a 25%OFF gift card, a Pixie, an Inissia, or a Essenza Mini.</li>
							<li>4. For further support, please contact our hotline at 1900 633 474. Our lines are open from Monday - Saturday, 10am to 6pm.</li>
							<li>5. The event starts from November 3rd, 2020 – November 5th, 2020</li>
							<li>6. After November 5th, we will stop receiving any code submission.</li>
						</ul>
						<p data-modal="coffee1" class="pop-up"><strong>TERMS AND CONDITIONS</strong></p>
					</div>
				</div>
			</section>
			<section id="sixsimage">
				<div class="wrapper">
					<h3>NESPRESSO MOMENTS</h3>
					<div class="t-img">
						<div class="t-img1"><img src="img/gallery/s13.png" /></div>
						<div class="t-img1"><img src="img/gallery/s14.png"></div>
						<div class="t-img1"><img src="img/gallery/s14.png"></div>
					</div>
					<div class="t-img">
						<div class="t-img1"><img src="img/gallery/s13.png" /></div>
						<div class="t-img1"><img src="img/gallery/s14.png"></div>
						<div class="t-img1"><img src="img/gallery/s14.png"></div>
					</div>
				</div>
			</section>

			<!--coffee pop-up-->
			<div id="coffee1" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="pop-content">
						<h2>A CHANCE TO WIN A NESPRESSO PRIZE</h2>
						<ul style="list-style:none;padding:20px 0">
							<li>1. Receive a lottery number after you register for the event</li>
							<li>2. Visit our experience corner and submit your number to get a prize from Nespresso</li>
							<li>3. Prizes can be one of the followings: a 25%OFF gift card, a Pixie, an Inissia, or a Essenza Mini.</li>
							<li>4. For further support, please contact our hotline at 1900 633 474. Our lines are open from Monday - Saturday, 10am to 6pm.</li>
							<li>5. The event starts from November 3rd, 2020 – November 5th, 2020</li>
							<li>6. After November 5th, we will stop receiving any code submission.</li>
							<li>7. Click here to read more about <strong>TERMS AND CONDITIONS</strong></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="extra-info">
				<div class="promo-content">
					<h2>
						Discover your coffee experience with
						<span class="bold">nespresso</span>
					</h2>
					<p>
						Come into this world of exquisite coffee and discover an experience
						that will take you out of the everyday each time you take a sip.
					</p>
					<ul>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#coffee" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-coffee.svg"></a>
							<h5>Exceptional coffee choice</h5>
							<p>
								A range of 29 coffee capsules of the highest quality, each with
								its own distinct individual character and aroma.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#sustainability" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-leaf.svg"></a>
							<h5>Sustainable coffee quality</h5>
							<p>
								Nespresso AAA Sustainable Quality Program supports 75,000
								farmers to grow the highest quality coffee.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#machine" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-machine.svg"></a>
							<h5>Innovative nespresso machines</h5>
							<p>
								Nespresso machines are simple and easy to use. Enjoy delicious
								coffees with a simple touch of a button.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#accessories" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-cup.svg"></a>
							<h5>Variety of accessories</h5>
							<p>
								Complete your coffee experience with a wide range of
								accessories.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#services" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-hand.svg"></a>
							<h5>Personalised services</h5>
							<p>
								Enjoy exclusive services from ordering, delivery, customer care
								to recycling.
							</p>
						</li>
					</ul>
				</div>
			</div>

		</div>
		</div>
		<?php include 'footer.php'; ?>
		<script src="owl/owl.carousel.min.js"></script>
		<!-- nespresso scripts -->

		<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/withinviewport.js"></script>

		<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/jquery.withinviewport.js"></script>

		<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/gtm_function.js"></script>

		<!-- /wp_footer -->
		<script>		
			$(document).ready(function() {
				// Pop-up
				$(".pop-up").click(function(e) {
					e.preventDefault();
					var modalID = $(this).data("modal");
					$("#" + modalID).addClass("active");
					$("body").addClass("modal-opened");
				});
				$(".btn-close, .modal").click(function(e) {
					e.preventDefault();
					$(".modal").removeClass("active");
					$("body").removeClass("modal-opened");
				});
				$('.modal-content').on('click', function(event) {
					console.log("content");
					event.stopPropagation();
				});
			})
		</script>
</body>

</html>

</div><!-- .entry-content -->
</div><!-- #post-## -->

<!--</main>-->
<!-- #main -->
<!--</div>-->
<!-- #primary -->


<!-- get_footer -->