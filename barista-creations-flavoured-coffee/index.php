<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Nespresso Vietnam brings luxury coffee and espresso machine straight from the café and into your kitchen.">
	<meta name="author" content="Koodi">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
	<meta name="google-site-verification" content="Ihzx1tBV53gOkNIrs2JJCElahIqsxA6tiG6pMorjWdA" />
	<title>Nespresso Vietnam | Ispirazione Italiana</title>
	<link rel="icon" type="image/x-icon" href="https://www.nespresso.vn/wp-content/themes/storefront-child/images/favicon.ico">

	<!-- wp_head -->
	<title>Nespresso Vietnam &#8211; Nespresso Vietnam brings luxury coffee and espresso machine straight from the café and into your kitchen.</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	<link rel='dns-prefetch' href='//s.w.org' />
	<link rel="alternate" type="application/rss+xml" title="Nespresso Vietnam &raquo; Feed" href="https://www.nespresso.vn/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Nespresso Vietnam &raquo; Comments Feed" href="https://www.nespresso.vn/comments/feed/" />
	<link rel='stylesheet' id='axapta-api-css' href='https://www.nespresso.vn/wp-content/plugins/axapta-api/public/css/axapta-api-public.css?ver=1.0.0' type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css' href='https://www.nespresso.vn/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.3' type='text/css' media='all' />
	<link rel='stylesheet' id='nespresso-css' href='https://www.nespresso.vn/wp-content/plugins/nespresso/public/css/nespresso-public.css?ver=1.0.0' type='text/css' media='all' />
	<link rel='stylesheet' id='smart-search-css' href='https://www.nespresso.vn/wp-content/plugins/smart-woocommerce-search/assets/css/general.css?ver=ysm-1.5.5' type='text/css' media='all' />
	<style id='smart-search-inline-css' type='text/css'>
		.widget_search.ysm-active .smart-search-suggestions .smart-search-post-icon {
			width: 50px;
		}

		.widget_product_search.ysm-active .smart-search-suggestions .smart-search-post-icon {
			width: 50px;
		}
	</style>
	<style id='woocommerce-inline-inline-css' type='text/css'>
		.woocommerce form .form-row .required {
			visibility: visible;
		}
	</style>
	<link rel='stylesheet' id='fpf_front-css' href='https://www.nespresso.vn/wp-content/plugins/flexible-product-fields/assets/css/front.min.css?ver=39' type='text/css' media='all' />
	<link rel='stylesheet' id='trp-language-switcher-style-css' href='https://www.nespresso.vn/wp-content/plugins/translatepress-multilingual/assets/css/trp-language-switcher.css?ver=1.3.8' type='text/css' media='all' />
	<link rel='stylesheet' id='storefront-style-css' href='https://www.nespresso.vn/wp-content/themes/storefront/style.css?ver=2.1.8' type='text/css' media='all' />
	<style id='storefront-style-inline-css' type='text/css'>
		@media (min-width: 768px) {

			.woocommerce-checkout.yith-wcms #order_review,
			.woocommerce-checkout.yith-wcms #order_review_heading {
				width: 100%;
				float: none;
			}
		}

		.main-navigation ul li a,
		.site-title a,
		ul.menu li a,
		.site-branding h1 a,
		.site-footer .storefront-handheld-footer-bar a:not(.button),
		button.menu-toggle,
		button.menu-toggle:hover {
			color: #d5d9db;
		}

		button.menu-toggle,
		button.menu-toggle:hover {
			border-color: #d5d9db;
		}

		.main-navigation ul li a:hover,
		.main-navigation ul li:hover>a,
		.site-title a:hover,
		a.cart-contents:hover,
		.site-header-cart .widget_shopping_cart a:hover,
		.site-header-cart:hover>li>a,
		.site-header ul.menu li.current-menu-item>a {
			color: #ffffff;
		}

		table th {
			background-color: #f8f8f8;
		}

		table tbody td {
			background-color: #fdfdfd;
		}

		table tbody tr:nth-child(2n) td {
			background-color: #fbfbfb;
		}

		.site-header,
		.secondary-navigation ul ul,
		.main-navigation ul.menu>li.menu-item-has-children:after,
		.secondary-navigation ul.menu ul,
		.storefront-handheld-footer-bar,
		.storefront-handheld-footer-bar ul li>a,
		.storefront-handheld-footer-bar ul li.search .site-search,
		button.menu-toggle,
		button.menu-toggle:hover {
			background-color: #2c2d33;
		}

		p.site-description,
		.site-header,
		.storefront-handheld-footer-bar {
			color: #9aa0a7;
		}

		.storefront-handheld-footer-bar ul li.cart .count,
		button.menu-toggle:after,
		button.menu-toggle:before,
		button.menu-toggle span:before {
			background-color: #d5d9db;
		}

		.storefront-handheld-footer-bar ul li.cart .count {
			color: #2c2d33;
		}

		.storefront-handheld-footer-bar ul li.cart .count {
			border-color: #2c2d33;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6 {
			color: #484c51;
		}

		.widget h1 {
			border-bottom-color: #484c51;
		}

		body,
		.secondary-navigation a,
		.onsale,
		.pagination .page-numbers li .page-numbers:not(.current),
		.woocommerce-pagination .page-numbers li .page-numbers:not(.current) {
			color: #43454b;
		}

		.widget-area .widget a,
		.hentry .entry-header .posted-on a,
		.hentry .entry-header .byline a {
			color: #75777d;
		}

		a {
			color: #96588a;
		}

		a:focus,
		.button:focus,
		.button.alt:focus,
		.button.added_to_cart:focus,
		.button.wc-forward:focus,
		button:focus,
		input[type="button"]:focus,
		input[type="reset"]:focus,
		input[type="submit"]:focus {
			outline-color: #96588a;
		}

		button,
		input[type="button"],
		input[type="reset"],
		input[type="submit"],
		.button,
		.added_to_cart,
		.widget a.button,
		.site-header-cart .widget_shopping_cart a.button {
			background-color: #96588a;
			border-color: #96588a;
			color: #ffffff;
		}

		button:hover,
		input[type="button"]:hover,
		input[type="reset"]:hover,
		input[type="submit"]:hover,
		.button:hover,
		.added_to_cart:hover,
		.widget a.button:hover,
		.site-header-cart .widget_shopping_cart a.button:hover {
			background-color: #7d3f71;
			border-color: #7d3f71;
			color: #ffffff;
		}

		button.alt,
		input[type="button"].alt,
		input[type="reset"].alt,
		input[type="submit"].alt,
		.button.alt,
		.added_to_cart.alt,
		.widget-area .widget a.button.alt,
		.added_to_cart,
		.pagination .page-numbers li .page-numbers.current,
		.woocommerce-pagination .page-numbers li .page-numbers.current,
		.widget a.button.checkout {
			background-color: #2c2d33;
			border-color: #2c2d33;
			color: #ffffff;
		}

		button.alt:hover,
		input[type="button"].alt:hover,
		input[type="reset"].alt:hover,
		input[type="submit"].alt:hover,
		.button.alt:hover,
		.added_to_cart.alt:hover,
		.widget-area .widget a.button.alt:hover,
		.added_to_cart:hover,
		.widget a.button.checkout:hover {
			background-color: #13141a;
			border-color: #13141a;
			color: #ffffff;
		}

		#comments .comment-list .comment-content .comment-text {
			background-color: #f8f8f8;
		}

		.site-footer {
			background-color: #f0f0f0;
			color: #61656b;
		}

		.site-footer a:not(.button) {
			color: #2c2d33;
		}

		.site-footer h1,
		.site-footer h2,
		.site-footer h3,
		.site-footer h4,
		.site-footer h5,
		.site-footer h6 {
			color: #494c50;
		}

		#order_review,
		#payment .payment_methods>li .payment_box {
			background-color: #ffffff;
		}

		#payment .payment_methods>li {
			background-color: #fafafa;
		}

		#payment .payment_methods>li:hover {
			background-color: #f5f5f5;
		}

		@media screen and (min-width: 768px) {
			.secondary-navigation ul.menu a:hover {
				color: #b3b9c0;
			}

			.secondary-navigation ul.menu a {
				color: #9aa0a7;
			}

			.site-header-cart .widget_shopping_cart,
			.main-navigation ul.menu ul.sub-menu,
			.main-navigation ul.nav-menu ul.children {
				background-color: #24252b;
			}
		}
	</style>
	<link rel='stylesheet' id='storefront-fonts-css' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,700,900&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
	<link rel='stylesheet' id='woo-viet-provinces-style-css' href='https://www.nespresso.vn/wp-content/plugins/woo-viet/assets/provinces.css?ver=4.9.8' type='text/css' media='all' />
	<link rel='stylesheet' id='tawcvs-frontend-css' href='https://www.nespresso.vn/wp-content/plugins/variation-swatches-for-woocommerce/assets/css/frontend.css?ver=20160615' type='text/css' media='all' />
	<link rel='stylesheet' id='storefront-woocommerce-style-css' href='https://www.nespresso.vn/wp-content/themes/storefront/assets/sass/woocommerce/woocommerce.css?ver=4.9.8' type='text/css' media='all' />
	<style id='storefront-woocommerce-style-inline-css' type='text/css'>
		a.cart-contents,
		.site-header-cart .widget_shopping_cart a {
			color: #d5d9db;
		}

		table.cart td.product-remove,
		table.cart td.actions {
			border-top-color: #ffffff;
		}

		.woocommerce-tabs ul.tabs li.active a,
		ul.products li.product .price,
		.onsale,
		.widget_search form:before,
		.widget_product_search form:before {
			color: #43454b;
		}

		.woocommerce-breadcrumb a,
		a.woocommerce-review-link,
		.product_meta a {
			color: #75777d;
		}

		.onsale {
			border-color: #43454b;
		}

		.star-rating span:before,
		.quantity .plus,
		.quantity .minus,
		p.stars a:hover:after,
		p.stars a:after,
		.star-rating span:before,
		#payment .payment_methods li input[type=radio]:first-child:checked+label:before {
			color: #96588a;
		}

		.widget_price_filter .ui-slider .ui-slider-range,
		.widget_price_filter .ui-slider .ui-slider-handle {
			background-color: #96588a;
		}

		.woocommerce-breadcrumb,
		#reviews .commentlist li .comment_container {
			background-color: #f8f8f8;
		}

		.order_details {
			background-color: #f8f8f8;
		}

		.order_details>li {
			border-bottom: 1px dotted #e3e3e3;
		}

		.order_details:before,
		.order_details:after {
			background: -webkit-linear-gradient(transparent 0, transparent 0), -webkit-linear-gradient(135deg, #f8f8f8 33.33%, transparent 33.33%), -webkit-linear-gradient(45deg, #f8f8f8 33.33%, transparent 33.33%)
		}

		p.stars a:before,
		p.stars a:hover~a:before,
		p.stars.selected a.active~a:before {
			color: #43454b;
		}

		p.stars.selected a.active:before,
		p.stars:hover a:before,
		p.stars.selected a:not(.active):before,
		p.stars.selected a.active:before {
			color: #96588a;
		}

		.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {
			background-color: #96588a;
			color: #ffffff;
		}

		.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {
			background-color: #7d3f71;
			border-color: #7d3f71;
			color: #ffffff;
		}

		@media screen and (min-width: 768px) {

			.site-header-cart .widget_shopping_cart,
			.site-header .product_list_widget li .quantity {
				color: #9aa0a7;
			}
		}
	</style>
	<link rel='stylesheet' id='storefront-child-style-css' href='https://www.nespresso.vn/wp-content/themes/storefront-child/style.css?ver=4.9.8' type='text/css' media='all' />
	<script type='text/javascript' src='https://www.nespresso.vn/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/axapta-api/public/js/axapta-api-public.js?ver=1.0.0'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/html5-responsive-faq/js/hrf-script.js?ver=4.9.8'></script>
	<script type='text/javascript' src='https://www.nespresso.vn/wp-content/plugins/nespresso/public/js/nespresso-public.js?ver=1.0.0'></script>
	<link rel='https://api.w.org/' href='https://www.nespresso.vn/wp-json/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.nespresso.vn/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.nespresso.vn/wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 4.9.8" />
	<meta name="generator" content="WooCommerce 3.5.2" />
	<link rel="canonical" href="https://www.nespresso.vn/" />
	<link rel='shortlink' href='https://www.nespresso.vn/' />
	<link rel="alternate" type="application/json+oembed" href="https://www.nespresso.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nespresso.vn%2F" />
	<link rel="alternate" type="text/xml+oembed" href="https://www.nespresso.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.nespresso.vn%2F&#038;format=xml" />
	<!-- start Simple Custom CSS and JS -->
	<style type="text/css">
		.select2-results__options li {
			color: #333 !important;
		}

		.select2-results__options li.select2-results__option--highlighted {
			color: #fff !important;
		}
	</style>
	<!-- end Simple Custom CSS and JS -->

	<!-- This website runs the Product Feed PRO for WooCommerce by AdTribes.io plugin -->
	<!--noptimize-->
	<!-- Global site tag (gtag.js) - Google Ads: 770954263 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-770954263"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'AW-770954263');
	</script>
	<!--/noptimize-->

	<link rel="alternate" hreflang="en-US" href="https://www.nespresso.vn/" />
	<link rel="alternate" hreflang="vi" href="https://www.nespresso.vn/vi/" />
	<script type="text/javascript">
		var ajaxurl = "https://www.nespresso.vn/wp-admin/admin-ajax.php";
		var login_url = "https://www.nespresso.vn/my-account/";
	</script> <noscript>
		<style>
			.woocommerce-product-gallery {
				opacity: 1 !important;
			}
		</style>
	</noscript>

	<!-- Facebook Pixel Code -->
	<script type='text/javascript'>
		! function(f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window,
			document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
	</script>
	<!-- End Facebook Pixel Code -->
	<script type='text/javascript'>
		fbq('init', '2387221091407358', [], {
			"agent": "wordpress-4.9.8-1.7.25"
		});
	</script>
	<script type='text/javascript'>
		fbq('track', 'PageView', []);
	</script>
	<!-- Facebook Pixel Code -->
	<noscript>
		<img height="1" width="1" style="display:none" alt="fbpx" src="https://www.facebook.com/tr?id=2387221091407358&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->
	<script type="text/javascript">
		var cli_flush_cache = 2;
	</script>
	<style type="text/css" id="custom-background-css">
		body.custom-background {
			background-color: ##ffffff;
		}
	</style>
	<link rel="alternate" type="application/rss+xml" title="RSS" href="https://www.nespresso.vn/rsslatest.xml" />
	<style type="text/css" id="wp-custom-css">
		/*
You can add your own CSS here.

Click the help icon above to learn more.
*/

		.site-control .btn {
			line-height: 40px !important;
		}

		.account-sidebar {
			padding: 20px 0 0;
			background-color: #f9f9f9;
			margin-right: 0 !important;
		}

		div.wpcf7-validation-errors {
			border: 2px solid #f7002e;
			color: #3D8705;
		}


		div.wpcf7-mail-sent-ok {
			border: 2px solid #398f14;
			color: red;
		}

		.contactus .contactus-main textarea {
			height: 200px !important;
		}

		.contactus .contactus-main {
			padding: 0 20px !important;
		}

		.contactus .contactus-main .col-md-6 .input-text,
		.contactus .contactus-main .col-md-6 input[type="text"],
		.contactus .contactus-main .col-md-6 input[type="email"],
		.contactus .contactus-main .col-md-6 input[type="url"],
		.contactus .contactus-main .col-md-6 input[type="password"],
		.contactus .contactus-main .col-md-6 input[type="search"],
		.contactus .contactus-main .col-md-6 textarea {
			width: 100%;
		}

		.input-group.input-group-generic input::-moz-placeholder {
			color: #666 !important;
		}

		.input-group.input-group-generic input::-webkit-placeholder {
			color: #666 !important;
		}

		/*
For contact-us desktop page
start
*/

		.page-id-41 .wpcf7-form-control {
			height: 38px !important;
		}

		.page-id-41 .input-group.input-group-generic .desktop-label {
			padding-top: 21px !important;
		}

		.page-id-41 .desktop-label {
			width: 185px !important;
		}

		.page-id-41 .dropdown {
			margin-bottom: 0px !important;
		}

		/*
For contact-us desktop page
end
*/
	</style>
	<!-- /wp_head -->

	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/vendors.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/libs/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/libs/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/css-helpers.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/style.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/custom.css">
	<link rel="stylesheet" type="text/css" href="https://www.nespresso.vn/wp-content/themes/storefront-child/css/override.css">



	<script type="text/javascript">
		//Please execute the following dataLayer initialization on page load with the required site or page level parameters and attributes.
		// **NOTE** If any value is not obtainable or not relevant to the page, please leave the value blank/empty so nothing is passed into Google Analytics.
		var page_category = document.getElementById('product_category');
		gtmDataObject = [{
			'isEnvironmentProd': 'Yes',
			'pageName': 'Home | Nespresso',
			'pageType': 'Home', //Other values include “Product List”, “Product Details”, “Checkout”, “User Account” etc.
			'pageCategory': 'home', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
			'pageSubCategory': '', //If available, please populate. Reference the dimension description in sections 2.B and 2.C for further detail.
			'pageTechnology': 'OriginalLine', //Other values include “VertuoLine”, “Original Line”, “Pro”, or “None”
			'pageVariant': '', //Leave blank for now, this is for setting A/B Test Variations
			'market': 'VN',
			'version': 'VN - WooCommerce V1',
			'landscape': 'WooCommerce-Standard_Agent', //For mobile website, the value is ‘Woocommerce-mobilesite’
			'segmentBusiness': 'B2C',
			'currency': 'VND',
			'clubMemberID': '', //If this is not known, add null or do not include the attribute at all. Or at least, keep the string empty as a last resort
			'clubMemberStatus': 'false', //Customer membership status, True or False. Better defined as user.clubMember.clubMemberStatus: specifies if a user is a The Club Member (he already bought some coffee capsules) or not yet. Boolean value.
			'clubMemberLevel': '', //market level name for user level that is a club member
			'clubMemberTierID': '', //Global HQ level ID for club membership level
			'clubMemberTitle': '',
			'clubMemberLoginStatus': 'false', //user.isLoggedIn : Designation whether the customer is logged in or not on any page/screen
			'machineOwner': '', //If user owns a machine or not, if known
			'machineOwned': '', //If multiple machines owned, separate with "|||" symbol
			'preferredTechnology': 'Espresso',
			'event': 'event_pageView',
			'persistentBasketLoaded': 'false', //If persistent basket loaded on this page, set this value to TRUE. This only needs to be set once per session, and can otherwise be excluded from the gtmDataObject.
			'language': 'EN'
		}];
	</script>
	<script type="text/javascript">
		function removeFromCartGTM(cart_item_key) {
			var data = {
				action: 'gtm_get_basket',
				cart_item_key: cart_item_key
			};
			$.ajax({
				dataType: 'json',
				type: 'post',
				data: data,
				url: ajaxurl,
				dataType: 'json',
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
					// return false;
				},
				success: function(data) {
					if (data.status) {
						gtmDataObject.push({
							'event': 'removeFromCart',
							'landscape': 'Wordpress',
							'segmentBusiness': 'B2C',
							'currencyCode': 'VND',
							'ecommerce': {
								'remove': {
									'actionField': {

									},
									'products': data.products
								},
							},
						});
					}

				} // success()

			});
		}

		function addToCartGTM(item) {
			var data = {
				action: 'gtm_get_basket',
			};
			var metric10 = ''
			var metric11 = ''
			var metric12 = ''

			var dimension57 = '';

			switch (item.type) {
				case 'Coffee':
					dimension57 = 'Single Product - Capsule';
					metric10 = item.quantity;
					break;
				case 'Machine':
					dimension57 = 'Variable Product - Machine';
					metric11 = item.quantity;
					break;
				case 'Accessory':
					dimension57 = 'Single Product - Accessory';
					metric12 = item.quantity;
					break;

			}


			gtmDataObject.push({
				'event': 'addToCart',
				'landscape': 'Wordpress',
				'segmentBusiness': 'B2C',
				'currencyCode': 'VND',
				'ecommerce': {
					'add': {
						'actionField': {

						},
						'products': [{
							'name': item.name,
							'id': item.hqid,
							'dimension54': item.name,
							'dimension55': item.range,
							'dimension53': item.sku,
							'quantity': item.quantity,
							'price': item.price,
							'category': item.type.toLowerCase(),
							'dimension56': item.technology,
							'brand': 'Nespresso',
							'metric10': metric10,
							'metric11': metric11,
							'metric12': metric12,
							'dimension57': dimension57
						}]
					},
				},
			});
		}
	</script>
	<!--  Google Tag Manager  -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].unshift({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'gtmDataObject', 'GTM-KMPTP65');
	</script>
	<!-- End Google Tag Manager -->

	<script type="text/javascript">
		window.host_url = 'https://www.nespresso.vn';
		window.nespresso_token = '032da4cc0e26717ca42f91b2ab071669';
		window.currency_symbol = '&#8363;';
		window.currency = 'VND';
		window.user = null;
	</script>
	<link rel="stylesheet" href="owl/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="owl/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/mycss.css?a=<?= time() ?>">
</head>

<body class="home page-template-default page page-id-262 custom-background woocommerce-no-js translatepress-en_US yith-wcms show_checkout_login_reminder storefront-full-width-content right-sidebar woocommerce-active">
	<?php include 'header.php' ?>
	<main id="content">
		<div id="barista">
			<ul id="barista-nav">
				<li class=""><a href="#home" class="barista-icon-home"><span>Home</span></a></li>
				<li class=""><a href="#coffee" class="barista-icon-coffee"><span>Coffee</span></a></li>
				<li class=""><a href="#recipes" class="barista-icon-recipes"><span>Recipes</span></a></li>
				<!--<li class="current"><a href="#BaristaRelay" class="barista-icon-relay"><span>Barista Relay</span></a></li>-->
			</ul>
			<div class="barista-masthead">
				<section id="home" class="barista-banner">
					<div class="barista-banner-title wrapper">
						<span class="new">NEW</span>
						<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/barista-creations.png" alt="barista-creation">
						<h2>Flavours of Indulgence</h2>
						<p>Inspired by the creativity and expertise of the world’s finest baristas.<br>
							Discover a coffee range made for you to create delicious coffee recipes at home.<br>
							When you fancy an indulgent treat, our new flavoured coffees are an ideal choice.
						</p>
					</div>
				</section><!-- barista-banner -->

				<section id="coffee" class="new-barista-creation wrapper">
					<h2>INTRODUCING THE NEW<br>BARISTA CREATIONS FLAVOURED COFFEES</h2>
					<div class="types-of-coffee">
						<div class="coffee-info">
							<div class="coffee-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/vanilla.png" alt="Vanilla Éclair">
							</div>
							<div class="coffee-description">
								<h4>Vanilla Éclair</h4>
								<p>Velvety vanilla flavour wraps around smooth espresso</p>
								<button data-modal="coffee1" class="pop-up view-more">View More</button>
								<div class="barista-price">19,000 VND</div>
								<button class="btn btn-icon btn-icon-right btn-block btn-green" data-id="306901" data-hqid="306901" data-sku="B121911" data-range="" data-technology="" data-cart="true" data-name="Ispirazione Roma" data-price="14700" data-image-url="https://www.nespresso.vn/wp-content/uploads/2017/04/roma.png" data-type="Coffee" data-vat="1" data-qty-step="10" data-url="https://www.nespresso.vn/ispirazione-roma/" data-aromatic-profile="Balanced">
        <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
        </button>
							</div>
						</div>

						<div class="coffee-info">
							<div class="coffee-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/caramel.png" alt="Caramel crÈme brÛlÉe">
							</div>
							<div class="coffee-description">
								<h4>Caramel crÈme brÛlÉe</h4>
								<p>Rich, creamy caramel flavoured espresso and light toasted notes</p>
								<button data-modal="coffee2" class="pop-up view-more">View More</button>
								<div class="barista-price">19,000 VND</div>
								<button class="btn btn-icon btn-icon-right btn-block btn-green" data-id="306901" data-hqid="306901" data-sku="B121911" data-range="" data-technology="" data-cart="true" data-name="Ispirazione Roma" data-price="14700" data-image-url="https://www.nespresso.vn/wp-content/uploads/2017/04/roma.png" data-type="Coffee" data-vat="1" data-qty-step="10" data-url="https://www.nespresso.vn/ispirazione-roma/" data-aromatic-profile="Balanced">
        <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
        </button>
							</div>
						</div>

						<div class="coffee-info">
							<div class="coffee-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/cocoa.png" alt="Cocoa truffle">
							</div>
							<div class="coffee-description">
								<h4>Cocoa truffle</h4>
								<p>A vibrant, dark, cocoa flavour added to Arabica espresso</p>
								<button data-modal="coffee3" class="pop-up view-more">View More</button>
								<div class="barista-price">19,000 VND</div>
								<!--br>
						<h5>Temporarily Unavailable</h5-->
						<button class="btn btn-icon btn-icon-right btn-block btn-green" data-id="306901" data-hqid="306901" data-sku="B121911" data-range="" data-technology="" data-cart="true" data-name="Ispirazione Roma" data-price="14700" data-image-url="https://www.nespresso.vn/wp-content/uploads/2017/04/roma.png" data-type="Coffee" data-vat="1" data-qty-step="10" data-url="https://www.nespresso.vn/ispirazione-roma/" data-aromatic-profile="Balanced">
        <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
        </button>
							</div>
						</div>
					</div>
				</section><!-- new-barista-creation -->
			</div>

			<section id="recipes" class="coffee-recipes">
				<div class="wrapper">
					<h2>TRY OUT THESE COFFEE RECIPES</h2>
					<ul class="recipes-info">
						<li>
							<a data-modal="recipe2" class="pop-up" href="#">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/caramel-creme-brulee-latte.jpg" alt="Caramel Creme Brulee">
							</a>
							<a class="watch-video" href="#">Watch the recipe video!<span></span></a>
						</li>
						<li>
							<a data-modal="recipe1" class="pop-up" href="#">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/ice-vanilla.png" alt=" Vanilla Eclair">
							</a>
						</li>
						<li>
							<a data-modal="recipe3" class="pop-up" href="#">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/dark-tiramisu-mocha2.png" alt="Cocoa Truffle">
							</a>
						</li>
					</ul>
				</div>
			</section>
			<!-- 
			<section id="BaristaRelay" class="barista-social-media">
				<div class="wrapper">
					<div class="barista-social-media-title">
						<h2>DISCOVER THE #BARISTARELAY RECIPES</h2>
						<p>Indulgent and decadent coffee recipes created by six home baristas.</p>
						<p>Now it’s your turn. What will you create?</p>
						<p>Tag us <a href="https://www.instagram.com/nespresso.vn/?hl=en" target="_blank">@Nespresso.VN</a> with your coffee creations!</p>
					</div>

					<ul class="social-media-info">
						<li>
							<a href="#" data-modal="social-media1" class="pop-up"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/melissa-koh.jpg"></a>
							<h5>Blueberry Crème Brûlée Latte</h5>
							<div class="social-media-author">By Melissa Koh @MelissaCKoh</div>
							<div class="social-media-position">Entrepreneur &amp; Content Creator</div>
						</li>
						<li>
							<a href="#" data-modal="social-media2" class="pop-up"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/jon-chua.jpg"></a>
							<h5>Jon Atas Drink</h5>
							<div class="social-media-author">By Jon Chua @JonChuaJX</div>
							<div class="social-media-position">Artist</div>
						</li>
						<li>
							<a href="#" data-modal="social-media3" class="pop-up"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/nicholas-tan.jpg"></a>
							<h5>The Flipping Cup</h5>
							<div class="social-media-author">By Nicholas Tan @StormScape</div>
							<div class="social-media-position">Content Creator</div>
						</li>
						<li>
							<a href="#" data-modal="social-media4" class="pop-up"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/victoria-cheng.jpg"></a>
							<h5>Chocolatey Truffle with Peanut Butter Clouds</h5>
							<div class="social-media-author">By Victoria Cheng @VictoriaCheng</div>
							<div class="social-media-position">Food Writer &amp; TV Presenter</div>
						</li>
						<li>
							<a href="#" data-modal="social-media5" class="pop-up"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/julie-tan.jpg"></a>
							<h5>Violet Vanilla Eclair Latte</h5>
							<div class="social-media-author">By Julie Tan @JulieTan_cxq</div>
							<div class="social-media-position">Actress &amp; Entrepreneur</div>
						</li>
						<li>
							<a href="#" data-modal="social-media6" class="pop-up"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/edward-russell.jpg"></a>
							<h5>Truffle Triple Choc</h5>
							<div class="social-media-author">By Edward Russell @EdRussell</div>
							<div class="social-media-position">TV Presenter</div>
						</li>
					</ul>
				</div>
			</section>
			-->
			<div class="extra-info">
				<div class="promo-content">
					<h2>
						Discover your coffee experience with
						<span class="bold">nespresso</span>
					</h2>
					<p>
						Come into this world of exquisite coffee and discover an experience
						that will take you out of the everyday each time you take a sip.
					</p>
					<p>
						<a href="https://www.nespresso.com/sg/en/coffee-experience" target="_blank" class="welcome-find-out-more bold">Find out more &gt;</a>
					</p>
					<ul>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#coffee" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-coffee.svg"></a>
							<h5>Exceptional coffee choice</h5>
							<p>
								A range of 29 coffee capsules of the highest quality, each with
								its own distinct individual character and aroma.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#sustainability" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-leaf.svg"></a>
							<h5>Sustainable coffee quality</h5>
							<p>
								Nespresso AAA Sustainable Quality Program supports 75,000
								farmers to grow the highest quality coffee.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#machine" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-machine.svg"></a>
							<h5>Innovative nespresso machines</h5>
							<p>
								Nespresso machines are simple and easy to use. Enjoy delicious
								coffees with a simple touch of a button.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#accessories" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-cup.svg"></a>
							<h5>Variety of accessories</h5>
							<p>
								Complete your coffee experience with a wide range of
								accessories.
							</p>
						</li>
						<li>
							<a href="https://www.nespresso.com/sg/en/coffee-experience#services" target="_blank"><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/welcome-offer/welcome/img/icn-hand.svg"></a>
							<h5>Personalised services</h5>
							<p>
								Enjoy exclusive services from ordering, delivery, customer care
								to recycling.
							</p>
						</li>
					</ul>
				</div>
			</div>

			<!--coffee pop-up-->
			<div id="coffee1" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="coffee-capsule">
						<div class="left-column">
							<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/vanilla-eclair-capsule.png" alt="Vanilla Éclair">
							<div class="barista-price">19,000 VND</div>
							<!-- <div class="add-to-bag" data-product-id="7553.40" data-button-size="large">
                        <span>ADD TO BASKET</span>
                    </div> -->
						</div>

						<div class="right-column">
							<h3>Vanilla Éclair</h3>
							<h4><i>Walk in the Clouds</i> </h4>
							<p>Inspired by French pastries, this lavish flavor oozes sweetness. Creamy vanilla flavour wraps
								around smooth Latin American Arabica espresso to create a velvety coffee.
							</p>
							<p>
								The way milk blends with BARISTA CREATIONS Vanilla Éclair is pure luxury. Add milk froth to
								make
								a cappuccino with the Nespresso BARISTA CREATIONS Vanilla Éclair pod, and you’ll taste how
								it
								brings out the creamy custard taste and gives it a touch of sweet almond. Here’s a tip –
								want to
								splash out? Try BARISTA CREATIONS Vanilla Éclair for your vanilla ice cream Affogato.
							</p>
							<div class="coffee-capsule-spec">
								<table>
									<tbody>
										<tr>
											<td>Recommended cup sizes</td>
											<td><b>40ml</b></td>
										</tr>
										<tr>
											<td>Recommended recipe</td>
											<td><b>Cappuccino</b></td>
										</tr>
										<tr>
											<td>Main aromatic note</td>
											<td><b>Vanilla</b></td>
										</tr>

									</tbody>
								</table>
							</div>

							<div class="coffee-capsule-details">
								<table>
									<tbody>
										<tr>
											<td>Bitterness</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Acidity</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Roastiness</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Body</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="coffee2" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="coffee-capsule">
						<div class="left-column">
							<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/caramel-capsule.png" alt="Caramel crÈme brÛlÉe">
							<div class="barista-price">19,000 VND</div>
							<!-- <div class="add-to-bag" data-product-id="7554.40" data-button-size="large">
                        <span>ADD TO BASKET</span>
                    </div> -->

						</div>

						<div class="right-column">
							<h3>Caramel crÈme brÛlÉe</h3>
							<h4><i>A Sweetheart</i> </h4>
							<p>A classic favourite recreated by carefully adding a sweet flavour to our Latin American
								Arabica espresso, resulting in an irresistibly rich and creamy experience.
							</p>
							<p>
								Try the BARISTA CREATIONS Crème Brûlée flavoured coffee with milk – it draws out the creamy
								caramel character even more and has subtle notes of vanilla and coconut that appear. Adding
								milk gets you a delicious caramel flavoured cappuccino recipe for the perfect afternoon
								treat.
							</p>

							<div class="coffee-capsule-spec">
								<table>
									<tbody>
										<tr>
											<td>Recommended cup sizes</td>
											<td><b>40ml</b></td>
										</tr>
										<tr>
											<td>Recommended recipe</td>
											<td><b>Latte</b></td>
										</tr>
										<tr>
											<td>Main aromatic note</td>
											<td><b>Caramel</b></td>
										</tr>

									</tbody>
								</table>
							</div>


							<div class="coffee-capsule-details">
								<table>
									<tbody>
										<tr>
											<td>Bitterness</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Acidity</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Roastiness</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Body</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="coffee3" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="coffee-capsule">
						<div class="left-column">
							<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/cocoa-capsule2.png" alt="Cocoa truffle">
							<div class="barista-price">19,000 VND</div>
							<!-- <div class="add-to-bag" data-product-id="7579.40" data-button-size="large">
                        <span>ADD TO BASKET</span>
                    </div> -->
						</div>

						<div class="right-column">
							<h3>Cocoa truffle</h3>
							<h4><i>Cocoa kiss</i></h4>
							<p>A delectably rich, smooth coffee, created by introducing dark cocoa flavour to our Latin
								American Arabica espresso, resulting in an indulgent flavour.
							</p>
							<p>
								BARISTA CREATIONS Cocoa Truffle’s vibrant character can push through milk. You’ll recognize
								it in a cappuccino by its distinct cocoa notes and subtle almond sweetness. You may even
								catch a hint of vanilla when you add milk froth to this flavoured coffee capsule.
							</p>

							<div class="coffee-capsule-spec">
								<table>
									<tbody>
										<tr>
											<td>Recommended cup sizes</td>
											<td><b>40ml</b></td>
										</tr>
										<tr>
											<td>Recommended recipe</td>
											<td><b>Cappuccino</b></td>
										</tr>
										<tr>
											<td>Main aromatic note</td>
											<td><b>Dark Chocolate</b></td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="coffee-capsule-details">
								<table>
									<tbody>
										<tr>
											<td>Bitterness</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Acidity</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Roastiness</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
										<tr>
											<td>Body</td>
											<td>
												<div class="circle" data-circle="3"><span class="circle-highlighted"></span><span class="circle-highlighted"></span><span class="circle-highlighted"></span></div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="coffee4" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="capsule-assortment">
						<div class="left-column">
							<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/capsules-barista-creation-assortment-30.png" alt="Barista Creations 30 Capsules Assortment">
							<div class="barista-price">SGD 25.50</div>
							<!-- <div class="add-to-bag" data-product-id="110642" data-button-size="large">
                        <span>ADD TO BASKET</span>
                    </div> -->

						</div>

						<div class="right-column">
							<h3>BARISTA CREATIONS 30 CAPSULES ASSORTMENT</h3>
							<p>Inspired by signature coffee shop favorites, our new range of Barista Creations flavoured
								coffees were specially designed to hit perfect harmony. We hope you enjoy bringing your new
								coffee shop style creations to life
							</p>


							<div class="capsule-assortment-details">
								<table>
									<thead>
										<tr>
											<th>Coffee</th>
											<th>Qty</th>
											<th>Aromatic Profile</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/vanilla-capsule-small.png" alt="Vanilla Éclair"><span>Vanilla
													Éclair</span> </td>
											<td>x10</td>
											<td>Velvety vanilla flavour</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/caramel-capsule-small.png" alt="Caramel Crème Brûlée"><span>Caramel Crème Brûlée</span></td>
											<td>x10</td>
											<td>Creamy caramel flavour</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/cocoa-capsule-small.png" alt="Cocoa Truffle"><span>Cocoa
													Truffle</span></td>
											<td>x10</td>
											<td>Dark cocoa flavour</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="coffee5" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="capsule-assortment">
						<div class="left-column">
							<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/capsules-barista-creation-assortment-50.png" alt="Barista Creations 50 Capsules Assortment">
							<div class="barista-price">SGD 41.50</div>
							<!-- <div class="add-to-bag" data-product-id="114116" data-button-size="large">
                        <span>ADD TO BASKET</span>
                    </div> -->

						</div>

						<div class="right-column">
							<h3>BARISTA CREATIONS 50 CAPSULES ASSORTMENT</h3>
							<p>Inspired by the expertise of some of the world’s finest baristas, our new Barista Creations
								range was specially designed for the perfect harmony of flavors. Whether adding milk or
								enjoying a flavoured coffee, we think you’ll love bringing these coffee shop style creations
								to life.
							</p>

							<div class="capsule-assortment-details">
								<table>
									<thead>
										<tr>
											<th>Coffee</th>
											<th>Qty</th>
											<th>Aromatic Profile</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/vanilla-capsule-small.png" alt="Vanilla Éclair"><span>Vanilla
													Éclair</span> </td>
											<td>x10</td>
											<td>Velvety vanilla flavour</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/caramel-capsule-small.png" alt="Caramel Crème Brûlée"><span>Caramel Crème Brûlée</span></td>
											<td>x10</td>
											<td>Creamy caramel flavour</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/cocoa-capsule-small.png" alt="Cocoa Truffle"><span>Cocoa
													Truffle</span></td>
											<td>x10</td>
											<td>Dark cocoa flavour</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/corto-capsule-small.png" alt="Corto"><span>Corto</span></td>
											<td>x10</td>
											<td>Spicy and roasted</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/scuro-capsule-small.png" alt="Scuro"><span>Scuro</span></td>
											<td>x10</td>
											<td>Roasted and balanced</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--Recipe pop-up-->
			<div id="recipe1" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="recipe-modal-box">
						<h3>Iced Vanilla Cheesecake</h3>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/ice-vanilla.png" alt="Iced Vanilla Cheesecake">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks active" data-link="Ingredients">Ingredients &amp; Materials</button>
									<button class="tablinks" data-link="Preparations">Preparation</button>
								</div>

								<div id="Ingredients" class="tabcontent active">
									<ul class="list-check">
										<li>1 Nespresso Vanilla Éclair capsule</li>
										<li>5ml cheesecake syrup</li>
										<li>100ml milk</li>
										<li>Ice Cubes</li>
										<li>Cocoa Powder</li>
										<li>Caramel Biscuit Crumbs</li>
										<li>Nespresso Coffee Machine</li>
										<li><a href="https://www.nespresso.com/sg/en/order/accessories#types=Milk%20Solution" target="_blank">Nespresso Aeroccino Milk Frother</a>
										</li>
										<li><a href="https://www.nespresso.com/sg/en/order/accessories/view-mug" target="_blank">Nespresso VIEW Mug</a></li>
									</ul>
								</div>

								<div id="Preparations" class="tabcontent">
									<ol class="step-description">
										<li>Add cheesecake syrup into mug with 4 ice cubes</li>
										<li>Prepare the milk froth by pouring milk into the Aeroccino milk frother and
											select cold milk froth</li>
										<li>Once milk froth is ready, pour into the mug</li>
										<li>Extract 40ml of Vanilla Éclair Coffee into the mug</li>
										<li>Dust coffee with cocoa powder and top off with some caramel biscuit crumbs</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="recipe2" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="recipe-modal-box">
						<h3>Caramel Creme Brulee Latte</h3>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/caramel-creme-brulee-latte.jpg" alt="Caramel Creme Brulee Latte">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks" data-link="Ingredients2">Ingredients &amp;
										Materials</button>
									<button class="tablinks active" data-link="Preparations2">Preparation</button>
									<!--<button class="tablinks" data-link="Video2">Video</button>--->
								</div>

								<div id="Ingredients2" class="tabcontent">
									<ul class="list-check">
										<li>1 Nespresso Caramel Crème Brulee capsule</li>
										<li>2.5ml Monin Salted Caramel Syrup</li>
										<li>50g Caramel Custard Pudding</li>
										<li>100ml Milk</li>
										<li>Ice cubes</li>
										<li>Whipped Cream</li>
										<li>Caramel Syrup</li>
										<li>Nespresso Coffee Machine</li>
										<li><a href="https://www.nespresso.com/sg/en/order/machines/barista-recipe-maker" target="_blank">Nespresso Barista Device</a>
										</li>
										<li><a href="https://www.nespresso.com/sg/en/order/accessories/new-view-recipe-glass" target="_blank">View Recipe Glass</a>
										</li>
									</ul>
								</div>

								<div id="Preparations2" class="tabcontent active">
									<ol class="step-description">
										<div class="video">
											<iframe width="560" height="315" src="https://www.youtube.com/embed/uBSBE-ZK2Pg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
											</iframe>
										</div>
										<li>Add 1/2 cup caramel custard pudding your of choice into a Recipe Glass</li>
										<li>Drizzle 2.5ml of Monin salted caramel syrup</li>
										<li>Add 3 ice cubes</li>
										<li>Prepare milk froth by adding in 100ml of milk and 1 ice cube in your Barista –
											using the Caffe Vennois setting</li>
										<li>Pour mixture into the Recipe Glass</li>
										<li>Extract 40ml of Caramel Crème Brulee coffee over the mixture</li>
										<li>Top it off with whipped cream</li>
										<li>Finish with caramel syrup to taste</li>
									</ol>
								</div>
								<!--<div id="Video2" class="tabcontent">
                            <div class="video"><iframe width="560" height="315"
                                    src="https://www.youtube.com/embed/uBSBE-ZK2Pg" frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                            </div>
                        </div>--->
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="recipe3" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="recipe-modal-box">
						<h3>Dark Tiramisu Mocha</h3>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/dark-tiramisu-mocha.png" alt="Dark Tiramisu Mocha">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks" data-link="Ingredients3">Ingredients &amp;
										Materials</button>
									<button class="tablinks active" data-link="Preparations3">Preparation</button>
								</div>

								<div id="Ingredients3" class="tabcontent">
									<ul class="list-check">
										<li>1 Nespresso Cocoa Truffle capsule</li>
										<li>2.5ml Tiramisu Syrup</li>
										<li>1/2 teaspoon <a href="https://www.nespresso.com/sg/en/order/accessories/cailler-le-chocolat" target="_blank">Cailler Chocolate Powder</a>
										</li>
										<li>100 ml Milk</li>
										<li>Ice cubes</li>
										<li>5ml Dark chocolate sauce</li>
										<li>Chocolate flakes</li>
										<li>Nespresso Coffee Machine</li>
										<li><a href="https://www.nespresso.com/sg/en/order/accessories#types=Milk%20Solution" target="_blank">Nespresso Aeroccino Milk Frother</a>
										</li>
										<li><a href="https://www.nespresso.com/sg/en/order/accessories/new-view-cappuccino-coffee-cups" target="_blank">VIEW Cappuccino Cup</a>
										</li>
									</ul>
								</div>

								<div id="Preparations3" class="tabcontent active">
									<ol class="step-description">
										<li>Drizzle dark chocolate sauce on the side of VIEW Cappuccino Cup</li>
										<li>Add Tiramisu Syrup and Caillier chocolate powder into the cup</li>
										<li>Prepare the milk froth by pouring milk into the Aeroccino milk frother and
											select hot milk froth</li>
										<li>Extract 40ml of Cocoa Truffle Coffee into the cups</li>
										<li>Dust with chocolate flakes</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<!--machine pop-up-->
			<div id="machine1" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="machine-content">
						<div class="left-column">
							<div class="offer-box"><b>$80</b> OFF*</div>
							<div class="machine-image slider">
								<div id="lattissima-one-white-popup" class="slide active">
									<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/lattissima-one-white.png" alt="Lattissima One White">
								</div>

								<div id="lattissima-one-brown-popup" class="slide">
									<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/lattissima-one-brown.png" alt="Lattissima One Brown">
								</div>

								<div id="lattissima-one-black-popup" class="slide">
									<div class="new-badge"><b>NEW</b></div>
									<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/lattissima-one-black.png" alt="Lattissima One Black">
								</div>

								<div class="slider-links">
									<button class="slider-link white active" data-link="lattissima-one-white-popup"></button>
									<button class="slider-link brown" data-link="lattissima-one-brown-popup"></button>
									<button class="slider-link black" data-link="lattissima-one-black-popup"></button>
								</div>
							</div>
							<div class="barista-price">SGD 368.00</div>
							<div class="barista-usual-price">(U.P SGD 448.00)</div>
							<!-- <div class="product-btn">
                        <div class="add-to-bag active" id="btn-lattissima-one-white-popup" data-color="White"
                            data-product-id="F111-HK-WH-NE" data-button-size="large"><span>ADD TO BASKET</span>
                        </div>
                        <div class="add-to-bag" id="btn-lattissima-one-brown-popup" data-color="Mocha Brown"
                            data-product-id="F111-HK-BW-NE" data-button-size="large"><span>ADD TO BASKET</span>
                        </div>
                        <div class="add-to-bag" id="btn-lattissima-one-black-popup" data-color="Black"
                            data-product-id="F111-HK-BK-NE" data-button-size="large"><span>ADD TO BASKET</span>
                        </div>
                    </div> -->
						</div>

						<div class="right-column">
							<h3>Lattissima One</h3>
							<h5>SIMPLICITY TO THE NEXT LEVEL</h5>
							<p>A new and innovative fresh milk system with one single button for all coffee &amp; milk
								drinks.
								Cleaning is also very simple as the milk jug is dishwasher safe.
							</p>

							<h5>COMPACT YET ELEGANT</h5>
							<p>It features a premium and compact design with qualitative finishing and materials such as
								the
								chromed
								lever, subtle gloss and matte line patterns
							</p>

							<h5>NO MILK WASTAGE</h5>
							<p>The machine is equipped with a single-serve system, all the milk in the jug is used for
								the
								beverage preventing any milk wastage.
							</p>

							<div class="machine-details">
								<table>
									<tbody>
										<tr>
											<td><b>Weight</b></td>
											<td>4.2 Kilogram</td>
										</tr>
										<tr>
											<td><b>Dimensions (WxDxH)</b></td>
											<td>15.4 x 32.4 x 25.6 cm</td>
										</tr>
										<tr>
											<td><b>Used capsule container capacity</b></td>
											<td>8</td>
										</tr>
										<tr>
											<td><b>Removable water tank</b></td>
											<td>1 Litre</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div id="machine2" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="machine-content">
						<div class="left-column">
							<div class="offer-box"><b>$100</b> OFF*</div>
							<div class="machine-image slider">
								<div id="lattissima-touch-silver-popup" class="slide active">
									<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/lattissima-touch-silver.png" alt="Lattissima Touch Silver">
								</div>

								<div id="lattissima-touch-black-popup" class="slide">
									<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/lattissima-touch-black.png" alt="Lattissima Touch Black">
								</div>

								<div class="slider-links">
									<button class="slider-link silver active" data-link="lattissima-touch-silver-popup"></button>
									<button class="slider-link black" data-link="lattissima-touch-black-popup"></button>
								</div>
							</div>
							<div class="barista-price">SGD 448.00</div>
							<div class="barista-usual-price">(U.P SGD 548.00)</div>
						</div>

						<div class="right-column">
							<h3>Lattissima Touch</h3>
							<h5>SIMPLICITY TO THE NEXT LEVEL</h5>
							<p>A new and innovative fresh milk system with one single button for all coffee &amp; milk
								drinks.
								Cleaning is also very simple as the milk jug is dishwasher safe.
							</p>

							<h5>6 ONE TOUCH RECIPES</h5>
							<p>Espresso, Lungo, Cappuccino, Creamy Latte, Latte Macchiato or hot milk. Choose between
								six
								coffee selections and milk recipes.
							</p>

							<h5>TACTILE INTERFACE</h5>
							<p>Tactile control panel offering endless possibilities in preparations, including those
								with
								your own touch.
							</p>

							<div class="machine-details">
								<table>
									<tbody>
										<tr>
											<td><b>Weight</b></td>
											<td>4.5 Kilogram</td>
										</tr>
										<tr>
											<td><b>Dimensions (WxDxH)</b></td>
											<td>17.3 x 32 x 25.8 cm</td>
										</tr>
										<tr>
											<td><b>Used capsule container capacity</b></td>
											<td>9</td>
										</tr>
										<tr>
											<td><b>Removable water tank</b></td>
											<td>0.9 Litre</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="machine3" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="machine-content">
						<div class="left-column">
							<div class="offer-box"><b>$160</b> OFF*</div>
							<div class="machine-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/lattissima-pro.png" alt="Lattissima Pro">

							</div>
							<div class="barista-price">SGD 728.00</div>
							<div class="barista-usual-price">(U.P SGD 888.00)</div>
							<!-- <div class="add-to-bag" data-product-id="F456-HK-PR-DL" data-button-size="large">
                        <span>ADD TO BASKET</span>
                    </div> -->
						</div>

						<div class="right-column">
							<h3>Lattissima Pro</h3>
							<h5>SIMPLICITY TO THE NEXT LEVEL</h5>
							<p>A new and innovative fresh milk system with one single button for all coffee &amp; milk
								drinks.
								Cleaning is also very simple as the milk jug is dishwasher safe.
							</p>

							<h5>INTUITIVE TOUCH SCREEN</h5>
							<p>One touch is all it takes. The intuitive touchscreen offers a simple way to program the
								machine and enjoy different types of coffee such as Ristretto, Espresso, and Lungo; as
								well
								as create milk-based recipes such as Latte Macchiatos and Cappuccinos.
							</p>

							<h5>POWERFUL INSIDE AND OUT</h5>
							<p>Inspired by the quality of professional machines with a high level of simplicity,
								presented
								in a sleek, pure and robust design with aluminium finish.
							</p>

							<div class="machine-details">
								<table>
									<tbody>
										<tr>
											<td><b>Weight</b></td>
											<td>6.5 Kilogram</td>
										</tr>
										<tr>
											<td><b>Dimensions (WxDxH)</b></td>
											<td>19.4 x 33.2 x 27.4 cm</td>
										</tr>
										<tr>
											<td><b>Used capsule container capacity</b></td>
											<td>13</td>
										</tr>
										<tr>
											<td><b>Removable water tank</b></td>
											<td>1.3 Litre</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>


			<!--Gift pop-up-->
			<div id="gift" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="gift-content">
						<div class="left-column">
							<div class="gift-image slider">
								<div id="inissia-coffee-red-popup" class="slide active">
									<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/inissia-bundle-red.png" alt="Inissia Coffee Machine Red">
								</div>

								<div id="inissia-coffee-black-popup" class="slide">
									<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/inissia-bundle-black.png" alt="Inissia Coffee Machine Black">
								</div>

								<div class="slider-links">
									<button class="slider-link red active" data-link="inissia-coffee-red-popup"></button>
									<button class="slider-link black" data-link="inissia-coffee-black-popup"></button>
								</div>
							</div>
						</div>

						<div class="right-column">
							<h3>Inissia Bundle</h3>
							<p> Tiny foot print, compact, lightweight and equipped with an ergonomic handle, the Inissia
								coffee
								machine fits perfectly into any house interior. The Aeroccino3 Milk Frother is an
								ultra-simple and fast automatic system for preparation of a light and creamy hot or cold
								milk froth with a touch of a button
							</p>

							<div class="gift-machine-details">
								<table>
									<tbody>
										<tr>
											<td>Key features</td>
											<td>Espresso (40ml), Lungo (110ml), Cappuccino and Latte Macchiato</td>
										</tr>
										<tr>
											<td>Weight</td>
											<td>2.4 Kilogram </td>
										</tr>
										<tr>
											<td>Dimensions (WxDxH)</td>
											<td>12cm x 32.1cm x 23cm</td>
										</tr>
										<tr>
											<td>Used capsule container capacity</td>
											<td>11</td>
										</tr>
										<tr>
											<td>Removable water tank</td>
											<td>0.7 Litre</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="gift-content">
						<div class="left-column">
							<div class="gift-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/capsules-barista-creation-assortment-30.png" alt="Barista Creations 30 Capsules Assortment">
							</div>

						</div>

						<div class="right-column">
							<div class="gift-coffee-capsule-details">
								<h5>BARISTA CREATIONS 30 CAPSULES ASSORTMENT</h5>
								<table>
									<thead>
										<tr>
											<th>Coffee</th>
											<th>Qty</th>
											<th>Aromatic Profile</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/vanilla-capsule-small.png" alt="Vanilla Éclair"><span>Vanilla
													Éclair</span> </td>
											<td>x10</td>
											<td>Velvety vanilla flavour</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/caramel-capsule-small.png" alt="Caramel Crème Brûlée"><span>Caramel Crème Brûlée</span></td>
											<td>x10</td>
											<td>Creamy caramel flavour</td>
										</tr>
										<tr>
											<td><img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/cocoa-capsule-small.png" alt="Cocoa Truffle"><span>Cocoa
													Truffle</span></td>
											<td>x10</td>
											<td>Dark cocoa flavour</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!---barista recipe maker pop up-->
			<div id="barista-recipe-maker-video" class="modal">
				<div class="modal-content barista-recipe-maker-pop-up">
					<span class="btn-close">×</span>
					<div class="barista-recipe-maker-video-title">
						<h3>DISCOVER A WIDE RANGE OF RECIPE POSSIBILITIES WITH BARISTA RECIPE MAKER</h3>
						<h6>The Nespresso Barista recipe maker is an invitation to discover a wide world of recipe
							possibilities. Make anything from a refreshing Iced Nitro to hot chocolate or elaborate latte
							art at
							home.
						</h6>
					</div>

					<div class="barista-recipe-maker-video-content tab">
						<div id="video1" class="tabcontent active">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/CPyjpCeYwvo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
							</iframe>
						</div>

						<div id="video2" class="tabcontent">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/W1HqciWJJ5o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
							</iframe>
						</div>


						<div id="video3" class="tabcontent">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/JfImzikPV88" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
							</iframe>
						</div>

						<div class="tab-links">
							<button class="tablinks active" data-link="video1">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/barista-first-use.jpg" alt="">
								<p>Barista First Use</p>
							</button>
							<button class="tablinks" data-link="video2">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/nespresso-recipes-cafe-viennois.jpg" alt="">
								<p>Make a Café Viennois</p>
							</button>
							<button class="tablinks" data-link="video3">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/barista-prepare-iced-coffee-recipes.jpg" alt="">
								<p>Prepare iced coffee recipes</p>
							</button>

						</div>

					</div>
				</div>
			</div>


			<!--Social Media pop-up-->
			<div id="social-media1" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="social-modal-box">
						<h3>Blueberry Crème Brûlée Latte</h3>
						<div class="social-media-author">By Melissa Koh @MelissaCKoh</div>
						<div class="social-media-position">Entrepreneur &amp; Content Creator</div>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/melissa-koh.jpg" alt="Blueberry Crème Brûlée Latte">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks active" data-link="ingredients-social1">Ingredients &amp; Materials</button>
									<button class="tablinks" data-link="preparations-social1">Preparation</button>
								</div>

								<div id="ingredients-social1" class="tabcontent active">
									<ul class="list-check">
										<li>1 Nespresso Barista Creations Caramel Crème Brûlée Capsule</li>
										<li>1/2 cup of Frozen Blueberries</li>
										<li>1 cup of Water</li>
										<li>1 cup of Sugar</li>
										<li>2 tsp of Lemon Juice</li>
										<li>2 tsp of Toffee Nut Syrup</li>
										<li>3 Coconut Water Ice Cubes</li>
										<li>100ml of Organic Milk</li>
										<li>Hazelnuts</li>
										<li>Whipped Cream</li>
										<li>Torched Marshmallows</li>
										<li>Nespresso Coffee Machine</li>
										<li>Nespresso Aeroccino Milk Frother</li>
										<li>Nespresso View Recipe Glass</li>
									</ul>
								</div>

								<div id="preparations-social1" class="tabcontent">
									<ol class="step-description">
										<li>To prepare the blueberry compote, add blueberries into a pan with the water, sugar and lemon. Stir well over a medium heat for about 10 minutes. Once it is the consistency you like, place aside to cool.</li>
										<li>Add toffee nut syrup into your View Recipe Glass with 2 tsp of blueberry compote. Add the coconut water ice cubes into the glass.</li>
										<li>Prepare the milk using the Aeroccino Milk Frother. Pour hot milk froth into the glass.</li>
										<li>Extract an espresso (40ml) of Nespresso Barista Creations Caramel Crème Brûlée capsule straight into the View Recipe Glass.</li>
										<li>Top off the coffee with a layer of whipped cream. Drizzle blueberry sauce on top of the cream and add the torched marshmallows.</li>
										<li>Crush hazelnuts using a pestle and mortar and finish your creation off by sprinkling some crushed hazelnuts.</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="social-media2" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="social-modal-box">
						<h3>Jon Atas Drink</h3>
						<div class="social-media-author">By Jon Chua @JonChuaJX</div>
						<div class="social-media-position">Artist</div>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/jon-chua.jpg" alt="Jon Atas Drink">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks active" data-link="ingredients-social2">Ingredients &amp; Materials</button>
									<button class="tablinks" data-link="preparations-social2">Preparation</button>
								</div>

								<div id="ingredients-social2" class="tabcontent active">
									<ul class="list-check">
										<li>1 Nespresso Barista Creations Cocoa Truffle Capsule</li>
										<li>2.5ml of Macadamia Syrup</li>
										<li>1/2 tsp of Cocoa Powder</li>
										<li>1 cup of Organic Milk</li>
										<li>Nespresso Coffee Machine</li>
										<li>Nespresso Aeroccino Milk Frother</li>
										<li>Nespresso View Mug</li>
									</ul>
								</div>

								<div id="preparations-social2" class="tabcontent">
									<ol class="step-description">
										<li>Add the macadamia syrup and cocoa powder into your View Mug.</li>
										<li>Prepare the milk using the Aeroccino Milk Frother. Pour the hot milk froth into your View Mug.</li>
										<li>Extract an espresso (40ml) of Nespresso Barista Creations Cocoa Truffle capsule into your View Mug.</li>
										<li>Finish your creation by dusting cocoa powder on top of the freshly brewed coffee.</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="social-media3" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="social-modal-box">
						<h3>The Flipping Cup</h3>
						<div class="social-media-author">By Nicholas Tan @StormScape</div>
						<div class="social-media-position">Content Creator</div>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/nicholas-tan.jpg" alt="The Flipping Cup">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks active" data-link="ingredients-social3">Ingredients &amp; Materials</button>
									<button class="tablinks" data-link="preparations-social3">Preparation</button>
								</div>

								<div id="ingredients-social3" class="tabcontent active">
									<ul class="list-check">
										<li>1 Nespresso Barista Creations Caramel Crème Brûlée Capsule</li>
										<li>25g of Honey</li>
										<li>85g of Sugar</li>
										<li>4 Eggs</li>
										<li>1 Egg Yolk</li>
										<li>30g of Cake Flour</li>
										<li>Nespresso Coffee Machine</li>
										<li>Nespresso View Cappuccino Cup</li>
									</ul>
								</div>

								<div id="preparations-social3" class="tabcontent">
									<ol class="step-description">
										<li>Extract an espresso (40ml) of Nespresso Barista Creations Caramel Crème Brûlée capsule and set aside.</li>
										<li>To create the pudding, mix 15g of honey, 60g of sugar, 3 eggs and 1 egg yolk. Whisk until the mixture turns pale yellow. Sieve the pudding mixture into a bowl and set aside.</li>
										<li>Prepare the Castella dough by adding 1 egg, 10g of honey and 25g of sugar into a bowl.</li>
										<li>Put the bowl in a warm water bath and whisk for 2 minutes. Remove from the water bath and whisk using an electric whisk at a high speed for 3 minutes until it forms a creamy texture. Add 30g of cake flour and mix with a spatula.</li>
										<li>Pour the pudding solution and the extracted espresso that were set aside, into your View Cappuccino Cup.</li>
										<li>Carefully add the Castella dough onto the espresso pudding mixture and place into a water bath. Steam for 45 minutes at 165ºC.</li>
										<li>Cool in the refrigerator overnight and sprinkle cinnamon over the top to serve.</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="social-media4" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="social-modal-box">
						<h3>Chocolatey Truffle with Peanut Butter Clouds</h3>
						<div class="social-media-author">By Victoria Cheng @VictoriaCheng</div>
						<div class="social-media-position">Food Writer &amp; TV Presenter</div>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/victoria-cheng.jpg" alt="Chocolatey Truffle with Peanut Butter Clouds">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks active" data-link="ingredients-social4">Ingredients &amp; Materials</button>
									<button class="tablinks" data-link="preparations-social4">Preparation</button>
								</div>

								<div id="ingredients-social4" class="tabcontent active">
									<ul class="list-check">
										<li>1 Nespresso Barista Creations Cocoa Truffle Capsule</li>
										<li>1-2 Cocoa Ice Cubes (cocoa powder, sugar and milk mix)</li>
										<li>50ml of Organic Milk</li>
										<li>100ml of Whipping Cream</li>
										<li>1.5 tbsp of Peanut Butter</li>
										<li>1 tbsp of Sugar</li>
										<li>Peanut Butter Chips</li>
										<li>Cocoa Powder</li>
										<li>Chocolate Syrup</li>
										<li>Nespresso Coffee Machine</li>
										<li>Nespresso View Cappuccino Cup</li>
									</ul>
								</div>

								<div id="preparations-social4" class="tabcontent">
									<ol class="step-description">
										<li>Extract an espresso (40ml) of Nespresso Barista Creations Cocoa Truffle capsule into your View Cappuccino Cup and stir in 1 teaspoon of peanut butter. Place in the fridge to cool.</li>
										<li>Prepare peanut butter cream by adding the sugar, 1 tablespoon of peanut butter and the whipping cream into a bowl. Whisk until you obtain a light and creamy texture.</li>
										<li>Take your View Cappuccino Cup out of the fridge and line the edges with chocolate syrup.</li>
										<li>Optional: Add 1 cocoa ice cube into the cup. You can prepare this ice cube by pouring the cocoa powder, sugar and milk mixture into an ice tray and freeze.</li>
										<li>Pour cold milk into your View Cappuccino Cup.</li>
										<li>Add the peanut butter cream up until the edge of your View Cappuccino Cup.</li>
										<li>Sprinkle peanut butter chips and cocoa powder to finish.</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="social-media5" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="social-modal-box">
						<h3>Violet Vanilla Eclair Latte</h3>
						<div class="social-media-author">By Julie Tan @JulieTan_cxp</div>
						<div class="social-media-position">Actress &amp; Entrepreneur</div>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/julie-tan.jpg" alt="Violet Vanilla Eclair Latte">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks active" data-link="ingredients-social5">Ingredients &amp; Materials</button>
									<button class="tablinks" data-link="preparations-social5">Preparation</button>
								</div>

								<div id="ingredients-social5" class="tabcontent active">
									<ul class="list-check">
										<li>1 Nespresso Barista Creations Vanilla Éclair Capsule</li>
										<li>50ml of Organic Milk</li>
										<li>White Chocolate Chips</li>
										<li>Mix of Dried Raisins, Cranberries, Almonds, Pumpkin Seeds and Goji Berries</li>
										<li>Blue-pea Flower Extract</li>
										<li>Gingernut Cookies</li>
										<li>Nespresso Coffee Machine</li>
										<li>Nespresso Aeroccino Milk Frother</li>
										<li>Nespresso View Cappuccino Cup</li>
									</ul>
								</div>

								<div id="preparations-social5" class="tabcontent">
									<ol class="step-description">
										<li>Melt the white chocolate chips and use a spoon to coat your View Cappuccino Cup with the melted chocolate.</li>
										<li>Extract an espresso (40ml) of Nespresso Vanilla Éclair capsule into your View Cappuccino Cup.</li>
										<li>Add blue-pea extract into the milk and prepare the milk using the Aeroccino Milk Frother. Pour the hot milk froth into your View Cappuccino Cup.</li>
										<li>Finish off with a sprinkle of berries and nuts and serve with ginger biscuits.</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="social-media6" class="modal">
				<div class="modal-content">
					<span class="btn-close">×</span>
					<div class="social-modal-box">
						<h3>Truffle Triple Choc</h3>
						<div class="social-media-author">By Edward Russell @EdRussell</div>
						<div class="social-media-position">TV Presenter</div>
						<div class="recipe-details">
							<div class="recipe-image">
								<img src="https://www.nespresso.com/shared_res/mos/free_html/sg/baristacreationsflavoured/img/edward-russell.jpg" alt="Truffle Triple Choc">
							</div>

							<div class="recipe-content tab">
								<div class="tab-links">
									<button class="tablinks active" data-link="ingredients-social6">Ingredients &amp; Materials</button>
									<button class="tablinks" data-link="preparations-social6">Preparation</button>
								</div>

								<div id="ingredients-social6" class="tabcontent active">
									<ul class="list-check">
										<li>1 Nespresso Barista Creations Cocoa Truffle Capsule</li>
										<li>50ml of Organic Milk</li>
										<li>Chocolate Sauce</li>
										<li>Cocoa Powder</li>
										<li>Dark Chocolate</li>
										<li>Nespresso Coffee Machine</li>
										<li>Nespresso Aeroccino Milk Frother</li>
										<li>Nespresso View Cappuccino Cup</li>
									</ul>
								</div>

								<div id="preparations-social6" class="tabcontent">
									<ol class="step-description">
										<li>Coat your View Cappuccino Cup with chocolate sauce and cocoa powder.</li>
										<li>Prepare the milk using the Aeroccino Milk Frother. Pour the hot milk froth into your cup.</li>
										<li>Extract an espresso (40ml) of Nespresso Barista Creations Cocoa Truffle capsule into your View Cappuccino Cup.</li>
										<li>Top with extra milk froth.</li>
										<li>Garnish with grated dark chocolate and a sprinkle of cocoa powder.</li>

									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
		</div>
		<?php include 'footer.php'; ?>
		<script src="owl/owl.carousel.min.js"></script>
		<!-- nespresso scripts -->

		<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/withinviewport.js"></script>

		<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/jquery.withinviewport.js"></script>

		<script type="text/javascript" src="https://www.nespresso.vn/wp-content/themes/storefront-child/js/components/gtm_function.js"></script>

		<!-- /wp_footer -->
		<script>
			
	;(function($, window, document, undefined){

// our plugin constructor
var OnePageNav = function(elem, options){
	this.elem = elem;
	this.$elem = $(elem);
	this.options = options;
	this.metadata = this.$elem.data('plugin-options');
	this.$win = $(window);
	this.sections = {};
	this.didScroll = false;
	this.$doc = $(document);
	this.docHeight = this.$doc.height();
};

// the plugin prototype
OnePageNav.prototype = {
	defaults: {
		navItems: 'a',
		currentClass: 'current',
		changeHash: false,
		easing: 'swing',
		filter: '',
		scrollSpeed: 750,
		scrollThreshold: 0.5,
		begin: false,
		end: false,
		scrollChange: false
	},

	init: function() {
		// Introduce defaults that can be extended either
		// globally or using an object literal.
		this.config = $.extend({}, this.defaults, this.options, this.metadata);

		this.$nav = this.$elem.find(this.config.navItems);

		//Filter any links out of the nav
		if(this.config.filter !== '') {
			this.$nav = this.$nav.filter(this.config.filter);
		}

		//Handle clicks on the nav
		this.$nav.on('click.onePageNav', $.proxy(this.handleClick, this));

		//Get the section positions
		this.getPositions();

		//Handle scroll changes
		this.bindInterval();

		//Update the positions on resize too
		this.$win.on('resize.onePageNav', $.proxy(this.getPositions, this));

		return this;
	},

	adjustNav: function(self, $parent) {
		self.$elem.find('.' + self.config.currentClass).removeClass(self.config.currentClass);
		$parent.addClass(self.config.currentClass);
	},

	bindInterval: function() {
		var self = this;
		var docHeight;

		self.$win.on('scroll.onePageNav', function() {
			self.didScroll = true;
		});

		self.t = setInterval(function() {
			docHeight = self.$doc.height();

			//If it was scrolled
			if(self.didScroll) {
				self.didScroll = false;
				self.scrollChange();
			}

			//If the document height changes
			if(docHeight !== self.docHeight) {
				self.docHeight = docHeight;
				self.getPositions();
			}
		}, 250);
	},

	getHash: function($link) {
		return $link.attr('href').split('#')[1];
	},

	getPositions: function() {
		var self = this;
		var linkHref;
		var topPos;
		var $target;

		self.$nav.each(function() {
			linkHref = self.getHash($(this));
			$target = $('#' + linkHref);

			if($target.length) {
				topPos = $target.offset().top;
				self.sections[linkHref] = Math.round(topPos);
			}
		});
	},

	getSection: function(windowPos) {
		var returnValue = null;
		var windowHeight = Math.round(this.$win.height() * this.config.scrollThreshold);

		for(var section in this.sections) {
			if((this.sections[section] - windowHeight) < windowPos) {
				returnValue = section;
			}
		}

		return returnValue;
	},

	handleClick: function(e) {
		var self = this;
		var $link = $(e.currentTarget);
		var $parent = $link.parent();
		var newLoc = '#' + self.getHash($link);

		if(!$parent.hasClass(self.config.currentClass)) {
			//Start callback
			if(self.config.begin) {
				self.config.begin();
			}

			//Change the highlighted nav item
			self.adjustNav(self, $parent);

			//Removing the auto-adjust on scroll
			self.unbindInterval();

			//Scroll to the correct position
			self.scrollTo(newLoc, function() {
				//Do we need to change the hash?
				if(self.config.changeHash) {
					window.location.hash = newLoc;
				}

				//Add the auto-adjust on scroll back in
				self.bindInterval();

				//End callback
				if(self.config.end) {
					self.config.end();
				}
			});
		}

		e.preventDefault();
	},

	scrollChange: function() {
		var windowTop = this.$win.scrollTop();
		var position = this.getSection(windowTop);
		var $parent;

		//If the position is set
		if(position !== null) {
			$parent = this.$elem.find('a[href$="#' + position + '"]').parent();

			//If it's not already the current section
			if(!$parent.hasClass(this.config.currentClass)) {
				//Change the highlighted nav item
				this.adjustNav(this, $parent);

				//If there is a scrollChange callback
				if(this.config.scrollChange) {
					this.config.scrollChange($parent);
				}
			}
		}
	},

	scrollTo: function(target, callback) {
		var offset = $(target).offset().top;

		$('html, body').animate({
			scrollTop: offset
		}, this.config.scrollSpeed, this.config.easing, callback);
	},

	unbindInterval: function() {
		clearInterval(this.t);
		this.$win.unbind('scroll.onePageNav');
	}
};

OnePageNav.defaults = OnePageNav.prototype.defaults;

$.fn.onePageNav = function(options) {
	return this.each(function() {
		new OnePageNav(this, options).init();
	});
};

})( jQuery, window , document );
$(document).ready(function () {
// Pop-up
$(".pop-up").click(function (e) {
	e.preventDefault();
	var modalID = $(this).data("modal");
	$("#" + modalID).addClass("active");
	$("body").addClass("modal-opened");
});
$(".btn-close, .modal").click(function (e) {
	e.preventDefault();
	$(".modal").removeClass("active");
	$("body").removeClass("modal-opened");
});
$('.modal-content').on('click', function (event) {
	console.log("content");
	event.stopPropagation();
});

// Slider
$(".slider-link").click(function (e) {
	e.preventDefault();
	var id = $(this).data("link");
	$(this).siblings('.active').removeClass("active");
	$(this).addClass("active");
	$(this).closest('.slider').children('.slide').removeClass("active");
	$("#" + id).addClass("active")
	$(this).closest('.machine-info').children('.product-btn').children('.add-to-bag').removeClass("active");
	$(this).closest('.left-column').children('.product-btn').children('.add-to-bag').removeClass("active");
	$(this).closest('.mothers-day-machine').children('.mothers-day-machine-description').children('.product-btn').children('.add-to-bag').removeClass("active");
	$("#btn-" + id).addClass("active")
})

// Tab
$(".tab-links button").click(function (e) {
	e.preventDefault();
	var id = $(this).data("link");
	$(this).siblings('.active').removeClass("active");
	$(this).addClass("active");
	$(this).closest('.tab').children('.tabcontent').removeClass("active");
	$("#" + id).addClass("active")
})

// Circle
$(".circle").each(function () {
	var circle = $(this).data("circle");
	for (i = 0; i < circle; i++) {
		$(this).append("<span class='circle-highlighted'></span>");
	}
})

// Navigation
$('#barista-nav').onePageNav();
})

		</script>
</body>

</html>

</div><!-- .entry-content -->
</div><!-- #post-## -->

<!--</main>-->
<!-- #main -->
<!--</div>-->
<!-- #primary -->


<!-- get_footer -->