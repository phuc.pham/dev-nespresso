
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMPTP65" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="wrapper">
        <div id="main-accessibility">
            <ul>
                <li><a href="#JumpContent" class="activateKeyboardNavigation" title="Jump to Content">Jump to Content</a></li>
                <li><a href="#JumpMenu" title="Jump to Menu">Jump to Menu</a></li>
                <li><a href="#JumpContact" title="Jump to Contact">Jump to Contact</a></li>
                <li><a href="#JumpFooter" title="Jump to Footer">Jump to Footer</a></li>
                <li><a href="#" class="activateDyslexia" title="Activate mode Dyslexia"><span class="dyslexia-desactivate hidden">Desactivate</span><span class="dyslexia-activate">Activate</span> mode Dyslexia</a></li>
            </ul>
        </div>
        <div id="main-wrapper">

            <!-- header -->
            

<div class="header-container">
    <header id="header">
        <div class="header__fixelts">
					<div style="background: #a95b5b;text-transform: uppercase;
    color: #ffffff;
    display: block;
    border: 1px solid;
    clear: both;
	text-align: center;padding: 10px;margin:0;
    font-size: 13px;
    position: relative;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="" data-src="https://www.nespresso.com/shared_res/agility/commons/img/services2018/express-delivery.svg" aria-hidden="true" style="
    stroke-width: 4px;
    margin-bottom: -1rem;
    width: 40px;
    height: 40px;
    display: inline-block;
    fill: none;
    stroke: currentColor;
"><g class="icon-stroke" stroke-linejoin="bevel"><path d="M32,64h-5l0-35h44.3c4,0,7.6,2.3,9.3,5.9l3.3,8.9l12,4.3V64h-8"></path><path d="M44.4,64.4c0,3.4-2.8,6.1-6.1,6.1c-3.4,0-6.1-2.8-6.1-6.1c0-3.4,2.7-6.1,6.1-6.1C41.7,58.3,44.4,61,44.4,64.4z"></path><path d="M88,64.4v0.2c0,2.3-1.3,4.5-3.5,5.4c-5.5,2.3-10.7-2.9-8.2-8.4c0.5-1.2,1.5-2.2,2.8-2.7C83.6,56.9,88,60.1,88,64.4z"></path><path d="M58,33v11h19.9l-2.4-6.8c-0.8-2.5-3.2-4.2-5.9-4.2H58z"></path><line x1="76" y1="64" x2="44" y2="64"></line><line x1="24" y1="36" x2="3" y2="36"></line><line x1="24" y1="45" x2="10" y2="45"></line><line x1="24" y1="55" x2="18" y2="55"></line></g></svg> Free Shipping from <font color="yellow">1,500,000 đ</font>
 <a href="/coffee-list" style="    color: #fff;
    border: 1px solid;
    padding: 5px;
    border-radius: 5px;    white-space: nowrap;
    margin-left: 20px;
">Order Coffee</a></div>
		            <div class="container">
                <div class="row bg-m-menu">

                    <div class="site-branding">
                        <h1>
                            <a href="/" title="Nespresso Home Page">Nespresso</a>
                        </h1>
                         <h2 class="mobile-menu">
                            <button class="bars"><span>Menu</span></button>
                        </h2>
                    </div>

                    <div class="site-control">
                        <div class="site-control-wrapper pull-left">
                           <!--  <h2 class="mobile-menu">
                                <button class="bars"><span>Menu</span></button>
                            </h2> -->

                            <div class="drop__sign">
                                <div class="drop__sign__mask"></div>

                                                                            <button id="sign__tooltip" class="btn btn-icon btn-nav">
                                            <i class="icon-user"></i>
                                            <span class="hide-md">Sign in or register</span>
                                        </button>
                                    
                                <button id="drop__sign__close" class="drop__sign__close"><i class="icon icon-Picto_croisrefermer"></i></button>

                                <div class="drop__sign__tooltip fgray-bgcolor">
                                    <div class="drop__sign__title">Sign in</div>
                                    <p>Access your account and place an order: </p>

                                    <form action="https://www.nespresso.vn/my-account/" method="post" name="login-form" role="form">

                                        <input type="hidden" name="login" value="Login">

                                        
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4 hidden" for="your-email">Email<span class="required">*</span></label>
                                            <input title="Holder name" id="username" name="username" placeholder="Email" class="form-control col-sm-12 col-md-9" type="text" value="" />
                                            <span class="mobile-label">Email<span class="required">*</span></span>
                                        </div>
                                        <div class="input-group input-group-generic">
                                            <label class="desktop-label col-sm-3 col-md-4 hidden" for="Password">Password<span class="required">*</span></label>
                                            <input title="Holder name" id="password" name="password" placeholder="Password *" class="form-control col-sm-12 col-md-9" type="password" />
                                            <span class="mobile-label">Password<span class="required">*</span></span>
                                        </div>

                                        
                                        <a href="/lost-password" class="link link--right" title="Forgot your password ?">Forgot your password ?</a>
                                        <div class="input-group input-group-generic">
                                            <div class="checkbox">
                                                <label for="save-card">
                                                    <input value="Remember me" id="save-card" name="rememberme" type="checkbox" value="forever" />
                                                    Remember me
                                                </label>
                                            </div>
                                        </div>

                                        <input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="b86efc5f3a" /><input type="hidden" name="_wp_http_referer" value="/" />
                                        <button type="submit" class="btn btn-primary btn-icon-right btn-block" title="Login" >Login<i class="icon icon-arrow_left"></i></button>

                                        
                                    </form>

                                    <hr>
                                    <p class="text-center">Don't have an account yet ?</p>
                                    <a class="btn btn-primary btn-icon-right btn-block" href="/register" title="Register now">Register now<i class="icon icon-arrow_left"></i></a>
                                </div>

                            </div><!-- .drop__sign -->

                            <div class="custom-dropdown basket">

                                <button class="btn btn-icon btn-nav btn-basket" style="padding-right: 30px;">
                                    <i class="icon-basket"></i>
                                    <span class="btn-add-qty">0</span>
                                    <span class="hide-md basket-empty">Your basket is empty</span>
                                    <span class="hide-md basket-full">
                                        Your basket: &#8363; <span class="basket-price" style="display: none;"></span>
                                        <span class="basket-price-formatted"></span>
                                    </span>
                                </button>

                                <div class="basket-dialog"
                                    data-start-shopping-url="https://www.nespresso.vn/coffee-list"
                                    data-reorder-url="https://www.nespresso.vn/my-account"
                                > <!-- basket-dialog--warning -->
                                    <script>
                                        var check_last_order = "false";
                                    </script>
                                    <div class="basket-dialog__wrapper ">

                                        <div class="basket-dialog-header clearfix">
                                            <div class="shopping-bag" id="basket">SHOPPING BAG</div>
                                            <a href="#" title="Close" class="icon icon-Picto_croisrefermer close"></a>
												                                             <p style="text-transform: uppercase;">Free Shipping from <font color="red">1,500,000 đ</font></p>
											                                        </div>

                                        <div class="basket-dialog-warning hidden">
                                            <span>For packaging purposes, capsules can only be ordered in multiples of 50. You can add 20 more on top of the 30 already selected to reach the next threshold.</span>
                                        </div>

                                        <div class="basket-dialog-product ">
                                            <ul></ul>
                                        </div>

                                        <div class="basket-dialog-footer clearfix">
                                            <div class="basket-100">
                                            <p><b>FREE <i>Nespresso</i> Touch Travel Mug</b> for the first 100 orders worth P5,000 and up.</p><p>Add the item to your shopping bag and use code: <b>FIRST100</b> upon checkout. </p>
                                            </div>
                                            <div class="basket-subtotal-price">
                                                SUBTOTAL
                                                <span style="display: none;">&#8363;<span class="price-int">0</span></span>
                                                <span>&#8363;<span class="price-int-formatted">0</span></span>
                                            </div>

                                            <!-- <div class="basket-vat-price">
                                                Vat (inCL.)
                                                <span>&#8363;<span class="price-int">0</span></span>
                                            </div> -->

                                            <div class="basket-total-price">
                                                TOTAL
                                                <span style="display: none;">&#8363;<span class="price-int">0</span></span>
                                                <span>&#8363;<span class="price-int-formatted">0</span></span>
                                            </div>

                                            <div class="basket-footer-bottom">
                                                <p>(without shipping costs and personalized special offers)</p>
                                                                                                    <a href="/login" class="btn btn-green">Login to checkout</a>
                                                                                                <br>


                                            </div>

                                        </div><!-- .basket-dialog-footer -->

                                    </div><!-- .basket-dialog__wrapper -->

                                </div>

                                <div class="basket-number nb-item-in-basket hidden">0</div>
                            </div><!-- .custom-dropdown .basket -->

                            <!-- add the template for basket list items -->
                            <script type="text/html" id="minibasket-line">

    <li class="basket-list-item">
        <div class="basket-product-image">
            <img data-src="image_url" data-alt="name"/>
        </div>
        <div class="basket-product-title">
            <span data-content="name"></span>
            <span class="span">
                <strong>
                    <span data-content="currency_symbol"></span>
                    <span data-content="total_price"></span>
                </strong>
                &nbsp;
                (<span data-content="quantity"></span> X <span data-content="currency_symbol"></span><span data-content="price_formatted"></span>)
            </span>
        </div>
        <a href="javascript:void(0)"
            class="basket-product-close"
            data-cart-remove="true"
            data-id="cart_item_key"
            data-class="discovery_offer"
        ><i class="icon icon-Picto_croisrefermer"></i> <span class="hidden">close</span></a>
    </li>

</script>
                            <div id="mask-mini-basket"></div>

                        </div><!-- .ite-control-wrapper -->
                            <button id="search-btn" class="btn btn-icon btn-nav btn-basket btn-search" style="min-width: 5px;">
                                <i class="fa fa-search fa-5" style="margin-left: 10px;" aria-hidden="true"></i>
                            </button>
                    </div><!-- .site-control -->

                </div><!-- .row -->
            </div><!-- .container -->

        </div><!-- .header__fixelts -->
        <div class="container"><br><br>
            <div class="search-div hide-me" id="search-div" >
                <div class="widget woocommerce widget_product_search">
                    <form role="" method="get" class="woocommerce-product-search " action="https://www.nespresso.vn">
                        <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products&hellip;" value="" name="s" autocomplete="off" style="height:23px;" />
                        <input type="submit" value="Search" />
                        <input type="hidden" name="post_type" value="product" />
                    </form>
                </div>
            </div>
        </div>
        <nav class="main-nav" role="menubar">
            <div class="container">
                <div class="row">
                    <ul class="menu-container menu-container-inner" id="JumpMenu" tabindex="-1">
                        <li class="menu-item menu-home hidden-md hidden-lg">
                            <a href="/" title="Home"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li class="menu-item menu-coffee" data-menu="coffee">
                            <a href="/coffee-list" accesskey="c" title="Coffee Page"><i class="icon icon-capsule"></i>Coffee</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/coffee-list">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/order-your-capsules.png" alt="coffee-order-your-capsules" class="hidden-xs hidden-sm" style="height: 150%; max-height: 150px; margin-top: -5px;" />
                                        </div>
                                        <div class="menu-description" style="margin-top: 15px;">
                                            <span class="menu-head__title">Order Your Capsules</span>
                                            <!-- <span class="menu-head__sub-title">Free delivery for every P3,000 spent</span> -->
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/coffee-range">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/capsule.jpg" alt="coffee-range" class="hidden-xs hidden-sm" style="height: 100%; max-height: 100px; margin-top: 20px;" />
                                        </div>
                                        <div class="menu-description" style="margin-top: -10px;">
                                            <span class="menu-head__title">Discover the Range</span>
                                            <span class="menu-head__sub-title">Explore <i>Nespresso</i> coffee selection</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <ul class="sub-menu">
                                        <li class="menu-info">
                                            <div class="product-info-wrap">
                                                <a role="menuitem" href="/coffee-origins" class="custom-menu-item" title="Coffee Origins" style="height: 112px;">
                                                    <div class="product-image">
                                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/our-coffee-stories.png" alt="coffee-origins" class="hidden-xs hidden-sm" style="max-width: 140%; margin-left: -27px;" />
                                                    </div>
                                                    <div class="product-info override-css">
                                                        <p class="product-head_title" style="position: absolute; top:45px; margin-left: 4px; color: #000;">Our Coffee Stories</p>
                                                        <p class="product-head_sub-title" style="position: absolute; top:70px; width: 150%; margin-left: -95px; font-size: 10px;">Journey to the perfect cup</p>
                                                    </div>
                                                    <div class="product-info override-vn-css" style="display: none;">
                                                        <p class="product-head_title" style="position: absolute; top:20px; margin-left: 4px; width: 66%; color: #000;">Our Coffee Story</p>
                                                        <p class="product-head_sub-title" style="position: absolute; top:85px; width: 115%; margin-left: -95px; font-size: 10px;">Journey to the perfect cup</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product-info-wrap">
                                                <a role="menuitem" class="custom-menu-item" href="https://www.nespresso.com/recipes/int/en/recipes.html" title="Coffee Recipes" target="_blank" style="margin-top: 10px;">
                                                    <div class="product-image">
                                                        <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/recipes.png" alt="coffee-recipes" class="hidden-xs hidden-sm" style="max-width: 85%; margin-left: -5px;" />
                                                    </div>
                                                    <div class="product-info">
                                                        <div class="product-info" style="margin-top: 0px; color: #000;">
                                                        <p class="product-head_title">Recipes</p>
                                                        <p class="product-head_sub-title" style="font-size: 10px;">Be your own barista</p><br>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="https://www.nespresso.vn/festive5sleeve/" class="noarrow" ><img src="https://www.nespresso.vn/wp-content/uploads/2019/02/coffee-navi-229-x-229-1.jpg" alt="5sleeve" data-promo-name="5sleeve" data-promo-id="Coffee_Navgitation_banner" data-promo-creative="Header Navigation | Coffee Banner" data-promo-position="Coffee Navigation | 4" class="promo-banner coffee-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-machines" data-menu="machine">
                            <a href="/machine-list" accesskey="d" title="Machines"><i class="icon icon-machine"></i>Machines</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/machine-list">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/order-your-machines.png" alt="machines-order-your-machine" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Order Your Machine</span>
                                            <span class="menu-head__sub-title" >Free delivery for every<br>machine purchase</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/machine-range">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/machine-range.png" alt="machines-compare-machine" class="hidden-xs hidden-sm" style="height: 145%; max-height: 220px; margin-top: -15px;"/>
                                        </div>
                                        <div class="menu-description" style="margin-top: 25px;">
                                            <span class="menu-head__title">Discover the range</span>
                                            <span class="menu-head__sub-title" >Find the machine that suits<br>you best</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" target="_blank" href="https://www.youtube.com/user/nespresso/playlists?shelf_id=5&view=50&sort=dd">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/machine-assistance.png" alt="machines-video-machine-assistance" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Video Machine Assistance</span>
                                            <span class="menu-head__sub-title">
                                                Let us guide you through<br>our How-To videos
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="https://www.nespresso.vn/machine-list/" class="noarrow" ><img src="https://www.nespresso.vn/wp-content/uploads/2019/02/machine-navi-229-x-229.jpg" alt="Festive Bundle"  data-promo-name="Festive Bundle" data-promo-id="Machine_Navgitation_banner" data-promo-creative="Header Navigation | Machine Banner" data-promo-position="Machine Navigation | 4" class="promo-banner machine-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-accessories" data-menu="accessory">
                            <a href="/accessory-list" accesskey="e" title="Accessories"><i class="icon icon-accessories"></i>Accessories</a>
                        
                        </li>
                        <li class="menu-item menu-our-services" data-menu="our-services">
                            <a href="/services" title="Our services"><i class="icon icon-services"></i>Our Services</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/services">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/services-order.png" alt="services-order" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">ORDERS</span>
                                            <span class="menu-head__sub-title">Our Coffee Specialists are ready to assist you</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/services/#delivery">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/services-delivery.png" alt="services-delivery" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">DELIVERY</span>
                                            <span class="menu-head__sub-title" >Learn about our delivery terms</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/services/#customer-care">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/services-customer-care.png" alt="services-customer-care" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">CUSTOMER CARE</span>
                                            <span class="menu-head__sub-title" >
                                                Do you need assistance for your machine?
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="/services/" class="noarrow" ><img src="https://www.nespresso.vn/wp-content/uploads/2019/02/delivery-navi-229-x-229.jpg" alt="Our Services" data-promo-name="Our Services" data-promo-id="Our_Services_Navgitation_banner" data-promo-creative="Header Navigation | Our Services Banner" data-promo-position="Our Services Navigation | 4" class="promo-banner our-services-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-become-a-member" style="text-align: center;" title="New to Nespresso?">
                            <a href="/login/"><i class="icon icon-Nespresso_Monogram_2"></i>New to Nespresso?</a>
                        </li>
                       
                        <li class="menu-item menu-customer-service" data-menu="contact-us">
                            <a href="/faq" id="JumpContact" tabindex="-1" title="Contact us"><i class="icon icon-faq"></i>Contact Us</a>
                            <ul class="sub-menu">
                                <li>
                                    <a role="menuitem" href="/faq">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/contact-faq.png" alt="contact-faq" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">FAQs</span>
                                            <span class="menu-head__sub-title" >We answer your frequently<br>asked questions</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/contact-us">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/contact-email.png" alt="contact-email" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Email us</span>
                                            <span class="menu-head__sub-title" >
                                                Our coffee specialists are ready to help you
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a role="menuitem" href="/services#customer-care">
                                        <div class="menu-image">
                                            <img src="https://www.nespresso.vn/wp-content/themes/storefront-child/images/menu/contact-telephone.png" alt="contact-telephone" class="hidden-xs hidden-sm"/>
                                        </div>
                                        <div class="menu-description">
                                            <span class="menu-head__title">Call us at 1900 633 474</span>
                                            <span class="menu-head__sub-title" >
                                                Lines are open Mon - Sat,<br>from 10am to 6pm
                                            </span>
                                        </div>
                                    </a>
                                </li>
                                <li class="last-sub-menu">
                                    <a href="/faq/" class="noarrow" ><img src="https://www.nespresso.vn/wp-content/uploads/2019/02/Accessory-List-Reorder.jpg" alt="Contact Us" data-promo-name="Contact Us" data-promo-id="Contact_Us_Navgitation_banner" data-promo-creative="Header Navigation | Contact Us Banner" data-promo-position="Contact Us Navigation | 4" class="promo-banner contact-us-nav-banner"/></a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-business-solutions last">
                            <a href="/pro" title="Professional"><i class="icon icon-business"></i>Professional</a>
                        </li>
                        <li class="menu-item menu-language visible-xs visible-sm">
                            <form action="/" method="GET">
                                <div class="dropdown menu-language__item  dropdown--language">
                                    <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i> <span class="current">English</span></button>
                                    <select class="select__element" id="header-language" name="">
                                        <option value="1">English</option>
                                        <option value="2">Français</option>
                                    </select>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="mask"></div>

        <!-- Set Gift Information -->
                        <input type="hidden" id="gwp_gift_status" value="Inactive">
        <input type="hidden" id="gwp_gift_by_product_status" value="Inactive">
    </header>
</div>
            <!-- /header -->



<!-- <div id="primary" class="content-area"> -->
	<!-- <main id="main" class="site-main" role="main"> -->

		
<div id="post-262" class="post-262 page type-page status-publish hentry category-home">
			<div class="entry-content">
			

<!-- quantity selector for add to basket when clicked -->

<script type="text/html" id="template-qty-selector">
    <div class="qty-selector">
        <span class="qty-selection machine-qty-select qty-nopad">0</span>
        <span class="qty-selection machine-qty-select">1</span>
        <span class="qty-selection machine-qty-select">2</span>
        <span class="qty-selection machine-qty-select">3</span>
        <span class="qty-selection machine-qty-select">4</span>
        <span class="qty-selection qty-nopad">10</span>
        <span class="qty-selection">20</span>
        <span class="qty-selection">30</span>
        <span class="qty-selection">40</span>
        <span class="qty-selection">50</span>
        <span class="qty-selection qty-nopad">60</span>
        <span class="qty-selection">70</span>
        <span class="qty-selection">80</span>
        <span class="qty-selection">90</span>
        <span class="qty-selection hide-sm">100</span>
        <span class="qty-selection qty-nopad hide-sm">150</span>
        <span class="qty-selection hide-sm">200</span>
        <span class="qty-selection hide-sm">300</span>
        <span class="qty-selection hide-sm">400</span>
        <span class="qty-selection hide-sm">500</span>
        <div class="qty-input" style="display: none;">
            <form id="select-your-qty" name="select-your-qty">
                <input class="qty-input-custom" data-value="defaultValue" name="qty-input-custom" type="number" step="1" placeholder="Other Quantity">
                <input class="qty-input-submit" type="submit" value="OK">
            </form>
        </div>
    </div>
</script>

<!-- content -->

<style type="text/css">
    .qty-selector::after {
        left: 85% !important;
    }
</style>